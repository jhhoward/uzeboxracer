// The Module object: Our interface to the outside world. We import
// and export values on it, and do the work to get that through
// closure compiler if necessary. There are various ways Module can be used:
// 1. Not defined. We create it here
// 2. A function parameter, function(Module) { ..generated code.. }
// 3. pre-run appended it, var Module = {}; ..generated code..
// 4. External script tag defines var Module.
// We need to do an eval in order to handle the closure compiler
// case, where this code here is minified but Module was defined
// elsewhere (e.g. case 4 above). We also need to check if Module
// already exists (e.g. case 3 above).
// Note that if you want to run closure, and also to use Module
// after the generated code, you will need to define   var Module = {};
// before the code. Then that object will be used in the code, and you
// can continue to use Module afterwards as well.
var Module;
if (!Module) Module = (typeof Module !== 'undefined' ? Module : null) || {};

// Sometimes an existing Module object exists with properties
// meant to overwrite the default module functionality. Here
// we collect those properties and reapply _after_ we configure
// the current environment's defaults to avoid having to be so
// defensive during initialization.
var moduleOverrides = {};
for (var key in Module) {
  if (Module.hasOwnProperty(key)) {
    moduleOverrides[key] = Module[key];
  }
}

// The environment setup code below is customized to use Module.
// *** Environment setup code ***
var ENVIRONMENT_IS_NODE = typeof process === 'object' && typeof require === 'function';
var ENVIRONMENT_IS_WEB = typeof window === 'object';
var ENVIRONMENT_IS_WORKER = typeof importScripts === 'function';
var ENVIRONMENT_IS_SHELL = !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER;

if (ENVIRONMENT_IS_NODE) {
  // Expose functionality in the same simple way that the shells work
  // Note that we pollute the global namespace here, otherwise we break in node
  if (!Module['print']) Module['print'] = function print(x) {
    process['stdout'].write(x + '\n');
  };
  if (!Module['printErr']) Module['printErr'] = function printErr(x) {
    process['stderr'].write(x + '\n');
  };

  var nodeFS = require('fs');
  var nodePath = require('path');

  Module['read'] = function read(filename, binary) {
    filename = nodePath['normalize'](filename);
    var ret = nodeFS['readFileSync'](filename);
    // The path is absolute if the normalized version is the same as the resolved.
    if (!ret && filename != nodePath['resolve'](filename)) {
      filename = path.join(__dirname, '..', 'src', filename);
      ret = nodeFS['readFileSync'](filename);
    }
    if (ret && !binary) ret = ret.toString();
    return ret;
  };

  Module['readBinary'] = function readBinary(filename) { return Module['read'](filename, true) };

  Module['load'] = function load(f) {
    globalEval(read(f));
  };

  if (!Module['thisProgram']) {
    if (process['argv'].length > 1) {
      Module['thisProgram'] = process['argv'][1].replace(/\\/g, '/');
    } else {
      Module['thisProgram'] = 'unknown-program';
    }
  }

  Module['arguments'] = process['argv'].slice(2);

  if (typeof module !== 'undefined') {
    module['exports'] = Module;
  }

  process['on']('uncaughtException', function(ex) {
    // suppress ExitStatus exceptions from showing an error
    if (!(ex instanceof ExitStatus)) {
      throw ex;
    }
  });
}
else if (ENVIRONMENT_IS_SHELL) {
  if (!Module['print']) Module['print'] = print;
  if (typeof printErr != 'undefined') Module['printErr'] = printErr; // not present in v8 or older sm

  if (typeof read != 'undefined') {
    Module['read'] = read;
  } else {
    Module['read'] = function read() { throw 'no read() available (jsc?)' };
  }

  Module['readBinary'] = function readBinary(f) {
    if (typeof readbuffer === 'function') {
      return new Uint8Array(readbuffer(f));
    }
    var data = read(f, 'binary');
    assert(typeof data === 'object');
    return data;
  };

  if (typeof scriptArgs != 'undefined') {
    Module['arguments'] = scriptArgs;
  } else if (typeof arguments != 'undefined') {
    Module['arguments'] = arguments;
  }

}
else if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
  Module['read'] = function read(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.send(null);
    return xhr.responseText;
  };

  if (typeof arguments != 'undefined') {
    Module['arguments'] = arguments;
  }

  if (typeof console !== 'undefined') {
    if (!Module['print']) Module['print'] = function print(x) {
      console.log(x);
    };
    if (!Module['printErr']) Module['printErr'] = function printErr(x) {
      console.log(x);
    };
  } else {
    // Probably a worker, and without console.log. We can do very little here...
    var TRY_USE_DUMP = false;
    if (!Module['print']) Module['print'] = (TRY_USE_DUMP && (typeof(dump) !== "undefined") ? (function(x) {
      dump(x);
    }) : (function(x) {
      // self.postMessage(x); // enable this if you want stdout to be sent as messages
    }));
  }

  if (ENVIRONMENT_IS_WORKER) {
    Module['load'] = importScripts;
  }

  if (typeof Module['setWindowTitle'] === 'undefined') {
    Module['setWindowTitle'] = function(title) { document.title = title };
  }
}
else {
  // Unreachable because SHELL is dependant on the others
  throw 'Unknown runtime environment. Where are we?';
}

function globalEval(x) {
  eval.call(null, x);
}
if (!Module['load'] && Module['read']) {
  Module['load'] = function load(f) {
    globalEval(Module['read'](f));
  };
}
if (!Module['print']) {
  Module['print'] = function(){};
}
if (!Module['printErr']) {
  Module['printErr'] = Module['print'];
}
if (!Module['arguments']) {
  Module['arguments'] = [];
}
if (!Module['thisProgram']) {
  Module['thisProgram'] = './this.program';
}

// *** Environment setup code ***

// Closure helpers
Module.print = Module['print'];
Module.printErr = Module['printErr'];

// Callbacks
Module['preRun'] = [];
Module['postRun'] = [];

// Merge back in the overrides
for (var key in moduleOverrides) {
  if (moduleOverrides.hasOwnProperty(key)) {
    Module[key] = moduleOverrides[key];
  }
}



// === Preamble library stuff ===

// Documentation for the public APIs defined in this file must be updated in: 
//    site/source/docs/api_reference/preamble.js.rst
// A prebuilt local version of the documentation is available at: 
//    site/build/text/docs/api_reference/preamble.js.txt
// You can also build docs locally as HTML or other formats in site/
// An online HTML version (which may be of a different version of Emscripten)
//    is up at http://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html

//========================================
// Runtime code shared with compiler
//========================================

var Runtime = {
  setTempRet0: function (value) {
    tempRet0 = value;
  },
  getTempRet0: function () {
    return tempRet0;
  },
  stackSave: function () {
    return STACKTOP;
  },
  stackRestore: function (stackTop) {
    STACKTOP = stackTop;
  },
  getNativeTypeSize: function (type) {
    switch (type) {
      case 'i1': case 'i8': return 1;
      case 'i16': return 2;
      case 'i32': return 4;
      case 'i64': return 8;
      case 'float': return 4;
      case 'double': return 8;
      default: {
        if (type[type.length-1] === '*') {
          return Runtime.QUANTUM_SIZE; // A pointer
        } else if (type[0] === 'i') {
          var bits = parseInt(type.substr(1));
          assert(bits % 8 === 0);
          return bits/8;
        } else {
          return 0;
        }
      }
    }
  },
  getNativeFieldSize: function (type) {
    return Math.max(Runtime.getNativeTypeSize(type), Runtime.QUANTUM_SIZE);
  },
  STACK_ALIGN: 16,
  getAlignSize: function (type, size, vararg) {
    // we align i64s and doubles on 64-bit boundaries, unlike x86
    if (!vararg && (type == 'i64' || type == 'double')) return 8;
    if (!type) return Math.min(size, 8); // align structures internally to 64 bits
    return Math.min(size || (type ? Runtime.getNativeFieldSize(type) : 0), Runtime.QUANTUM_SIZE);
  },
  dynCall: function (sig, ptr, args) {
    if (args && args.length) {
      assert(args.length == sig.length-1);
      if (!args.splice) args = Array.prototype.slice.call(args);
      args.splice(0, 0, ptr);
      assert(('dynCall_' + sig) in Module, 'bad function pointer type - no table for sig \'' + sig + '\'');
      return Module['dynCall_' + sig].apply(null, args);
    } else {
      assert(sig.length == 1);
      assert(('dynCall_' + sig) in Module, 'bad function pointer type - no table for sig \'' + sig + '\'');
      return Module['dynCall_' + sig].call(null, ptr);
    }
  },
  functionPointers: [],
  addFunction: function (func) {
    for (var i = 0; i < Runtime.functionPointers.length; i++) {
      if (!Runtime.functionPointers[i]) {
        Runtime.functionPointers[i] = func;
        return 2*(1 + i);
      }
    }
    throw 'Finished up all reserved function pointers. Use a higher value for RESERVED_FUNCTION_POINTERS.';
  },
  removeFunction: function (index) {
    Runtime.functionPointers[(index-2)/2] = null;
  },
  getAsmConst: function (code, numArgs) {
    // code is a constant string on the heap, so we can cache these
    if (!Runtime.asmConstCache) Runtime.asmConstCache = {};
    var func = Runtime.asmConstCache[code];
    if (func) return func;
    var args = [];
    for (var i = 0; i < numArgs; i++) {
      args.push(String.fromCharCode(36) + i); // $0, $1 etc
    }
    var source = Pointer_stringify(code);
    if (source[0] === '"') {
      // tolerate EM_ASM("..code..") even though EM_ASM(..code..) is correct
      if (source.indexOf('"', 1) === source.length-1) {
        source = source.substr(1, source.length-2);
      } else {
        // something invalid happened, e.g. EM_ASM("..code($0)..", input)
        abort('invalid EM_ASM input |' + source + '|. Please use EM_ASM(..code..) (no quotes) or EM_ASM({ ..code($0).. }, input) (to input values)');
      }
    }
    try {
      // Module is the only 'upvar', which we provide directly. We also provide FS for legacy support.
      var evalled = eval('(function(Module, FS) { return function(' + args.join(',') + '){ ' + source + ' } })')(Module, typeof FS !== 'undefined' ? FS : null);
    } catch(e) {
      Module.printErr('error in executing inline EM_ASM code: ' + e + ' on: \n\n' + source + '\n\nwith args |' + args + '| (make sure to use the right one out of EM_ASM, EM_ASM_ARGS, etc.)');
      throw e;
    }
    return Runtime.asmConstCache[code] = evalled;
  },
  warnOnce: function (text) {
    if (!Runtime.warnOnce.shown) Runtime.warnOnce.shown = {};
    if (!Runtime.warnOnce.shown[text]) {
      Runtime.warnOnce.shown[text] = 1;
      Module.printErr(text);
    }
  },
  funcWrappers: {},
  getFuncWrapper: function (func, sig) {
    assert(sig);
    if (!Runtime.funcWrappers[sig]) {
      Runtime.funcWrappers[sig] = {};
    }
    var sigCache = Runtime.funcWrappers[sig];
    if (!sigCache[func]) {
      sigCache[func] = function dynCall_wrapper() {
        return Runtime.dynCall(sig, func, arguments);
      };
    }
    return sigCache[func];
  },
  getCompilerSetting: function (name) {
    throw 'You must build with -s RETAIN_COMPILER_SETTINGS=1 for Runtime.getCompilerSetting or emscripten_get_compiler_setting to work';
  },
  stackAlloc: function (size) { var ret = STACKTOP;STACKTOP = (STACKTOP + size)|0;STACKTOP = (((STACKTOP)+15)&-16);(assert((((STACKTOP|0) < (STACK_MAX|0))|0))|0); return ret; },
  staticAlloc: function (size) { var ret = STATICTOP;STATICTOP = (STATICTOP + (assert(!staticSealed),size))|0;STATICTOP = (((STATICTOP)+15)&-16); return ret; },
  dynamicAlloc: function (size) { var ret = DYNAMICTOP;DYNAMICTOP = (DYNAMICTOP + (assert(DYNAMICTOP > 0),size))|0;DYNAMICTOP = (((DYNAMICTOP)+15)&-16); if (DYNAMICTOP >= TOTAL_MEMORY) { var success = enlargeMemory(); if (!success) return 0; }; return ret; },
  alignMemory: function (size,quantum) { var ret = size = Math.ceil((size)/(quantum ? quantum : 16))*(quantum ? quantum : 16); return ret; },
  makeBigInt: function (low,high,unsigned) { var ret = (unsigned ? ((+((low>>>0)))+((+((high>>>0)))*4294967296.0)) : ((+((low>>>0)))+((+((high|0)))*4294967296.0))); return ret; },
  GLOBAL_BASE: 8,
  QUANTUM_SIZE: 4,
  __dummy__: 0
}


Module['Runtime'] = Runtime;









//========================================
// Runtime essentials
//========================================

var __THREW__ = 0; // Used in checking for thrown exceptions.

var ABORT = false; // whether we are quitting the application. no code should run after this. set in exit() and abort()
var EXITSTATUS = 0;

var undef = 0;
// tempInt is used for 32-bit signed values or smaller. tempBigInt is used
// for 32-bit unsigned values or more than 32 bits. TODO: audit all uses of tempInt
var tempValue, tempInt, tempBigInt, tempInt2, tempBigInt2, tempPair, tempBigIntI, tempBigIntR, tempBigIntS, tempBigIntP, tempBigIntD, tempDouble, tempFloat;
var tempI64, tempI64b;
var tempRet0, tempRet1, tempRet2, tempRet3, tempRet4, tempRet5, tempRet6, tempRet7, tempRet8, tempRet9;

function assert(condition, text) {
  if (!condition) {
    abort('Assertion failed: ' + text);
  }
}

var globalScope = this;

// Returns the C function with a specified identifier (for C++, you need to do manual name mangling)
function getCFunc(ident) {
  var func = Module['_' + ident]; // closure exported function
  if (!func) {
    try {
      func = eval('_' + ident); // explicit lookup
    } catch(e) {}
  }
  assert(func, 'Cannot call unknown function ' + ident + ' (perhaps LLVM optimizations or closure removed it?)');
  return func;
}

var cwrap, ccall;
(function(){
  var JSfuncs = {
    // Helpers for cwrap -- it can't refer to Runtime directly because it might
    // be renamed by closure, instead it calls JSfuncs['stackSave'].body to find
    // out what the minified function name is.
    'stackSave': function() {
      Runtime.stackSave()
    },
    'stackRestore': function() {
      Runtime.stackRestore()
    },
    // type conversion from js to c
    'arrayToC' : function(arr) {
      var ret = Runtime.stackAlloc(arr.length);
      writeArrayToMemory(arr, ret);
      return ret;
    },
    'stringToC' : function(str) {
      var ret = 0;
      if (str !== null && str !== undefined && str !== 0) { // null string
        // at most 4 bytes per UTF-8 code point, +1 for the trailing '\0'
        ret = Runtime.stackAlloc((str.length << 2) + 1);
        writeStringToMemory(str, ret);
      }
      return ret;
    }
  };
  // For fast lookup of conversion functions
  var toC = {'string' : JSfuncs['stringToC'], 'array' : JSfuncs['arrayToC']};

  // C calling interface. 
  ccall = function ccallFunc(ident, returnType, argTypes, args) {
    var func = getCFunc(ident);
    var cArgs = [];
    var stack = 0;
    assert(returnType !== 'array', 'Return type should not be "array".');
    if (args) {
      for (var i = 0; i < args.length; i++) {
        var converter = toC[argTypes[i]];
        if (converter) {
          if (stack === 0) stack = Runtime.stackSave();
          cArgs[i] = converter(args[i]);
        } else {
          cArgs[i] = args[i];
        }
      }
    }
    var ret = func.apply(null, cArgs);
    if (returnType === 'string') ret = Pointer_stringify(ret);
    if (stack !== 0) Runtime.stackRestore(stack);
    return ret;
  }

  var sourceRegex = /^function\s*\(([^)]*)\)\s*{\s*([^*]*?)[\s;]*(?:return\s*(.*?)[;\s]*)?}$/;
  function parseJSFunc(jsfunc) {
    // Match the body and the return value of a javascript function source
    var parsed = jsfunc.toString().match(sourceRegex).slice(1);
    return {arguments : parsed[0], body : parsed[1], returnValue: parsed[2]}
  }
  var JSsource = {};
  for (var fun in JSfuncs) {
    if (JSfuncs.hasOwnProperty(fun)) {
      // Elements of toCsource are arrays of three items:
      // the code, and the return value
      JSsource[fun] = parseJSFunc(JSfuncs[fun]);
    }
  }

  
  cwrap = function cwrap(ident, returnType, argTypes) {
    argTypes = argTypes || [];
    var cfunc = getCFunc(ident);
    // When the function takes numbers and returns a number, we can just return
    // the original function
    var numericArgs = argTypes.every(function(type){ return type === 'number'});
    var numericRet = (returnType !== 'string');
    if ( numericRet && numericArgs) {
      return cfunc;
    }
    // Creation of the arguments list (["$1","$2",...,"$nargs"])
    var argNames = argTypes.map(function(x,i){return '$'+i});
    var funcstr = "(function(" + argNames.join(',') + ") {";
    var nargs = argTypes.length;
    if (!numericArgs) {
      // Generate the code needed to convert the arguments from javascript
      // values to pointers
      funcstr += 'var stack = ' + JSsource['stackSave'].body + ';';
      for (var i = 0; i < nargs; i++) {
        var arg = argNames[i], type = argTypes[i];
        if (type === 'number') continue;
        var convertCode = JSsource[type + 'ToC']; // [code, return]
        funcstr += 'var ' + convertCode.arguments + ' = ' + arg + ';';
        funcstr += convertCode.body + ';';
        funcstr += arg + '=' + convertCode.returnValue + ';';
      }
    }

    // When the code is compressed, the name of cfunc is not literally 'cfunc' anymore
    var cfuncname = parseJSFunc(function(){return cfunc}).returnValue;
    // Call the function
    funcstr += 'var ret = ' + cfuncname + '(' + argNames.join(',') + ');';
    if (!numericRet) { // Return type can only by 'string' or 'number'
      // Convert the result to a string
      var strgfy = parseJSFunc(function(){return Pointer_stringify}).returnValue;
      funcstr += 'ret = ' + strgfy + '(ret);';
    }
    if (!numericArgs) {
      // If we had a stack, restore it
      funcstr += JSsource['stackRestore'].body.replace('()', '(stack)') + ';';
    }
    funcstr += 'return ret})';
    return eval(funcstr);
  };
})();
Module["cwrap"] = cwrap;
Module["ccall"] = ccall;


function setValue(ptr, value, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': HEAP8[((ptr)>>0)]=value; break;
      case 'i8': HEAP8[((ptr)>>0)]=value; break;
      case 'i16': HEAP16[((ptr)>>1)]=value; break;
      case 'i32': HEAP32[((ptr)>>2)]=value; break;
      case 'i64': (tempI64 = [value>>>0,(tempDouble=value,(+(Math_abs(tempDouble))) >= 1.0 ? (tempDouble > 0.0 ? ((Math_min((+(Math_floor((tempDouble)/4294967296.0))), 4294967295.0))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/4294967296.0)))))>>>0) : 0)],HEAP32[((ptr)>>2)]=tempI64[0],HEAP32[(((ptr)+(4))>>2)]=tempI64[1]); break;
      case 'float': HEAPF32[((ptr)>>2)]=value; break;
      case 'double': HEAPF64[((ptr)>>3)]=value; break;
      default: abort('invalid type for setValue: ' + type);
    }
}
Module['setValue'] = setValue;


function getValue(ptr, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': return HEAP8[((ptr)>>0)];
      case 'i8': return HEAP8[((ptr)>>0)];
      case 'i16': return HEAP16[((ptr)>>1)];
      case 'i32': return HEAP32[((ptr)>>2)];
      case 'i64': return HEAP32[((ptr)>>2)];
      case 'float': return HEAPF32[((ptr)>>2)];
      case 'double': return HEAPF64[((ptr)>>3)];
      default: abort('invalid type for setValue: ' + type);
    }
  return null;
}
Module['getValue'] = getValue;

var ALLOC_NORMAL = 0; // Tries to use _malloc()
var ALLOC_STACK = 1; // Lives for the duration of the current function call
var ALLOC_STATIC = 2; // Cannot be freed
var ALLOC_DYNAMIC = 3; // Cannot be freed except through sbrk
var ALLOC_NONE = 4; // Do not allocate
Module['ALLOC_NORMAL'] = ALLOC_NORMAL;
Module['ALLOC_STACK'] = ALLOC_STACK;
Module['ALLOC_STATIC'] = ALLOC_STATIC;
Module['ALLOC_DYNAMIC'] = ALLOC_DYNAMIC;
Module['ALLOC_NONE'] = ALLOC_NONE;

// allocate(): This is for internal use. You can use it yourself as well, but the interface
//             is a little tricky (see docs right below). The reason is that it is optimized
//             for multiple syntaxes to save space in generated code. So you should
//             normally not use allocate(), and instead allocate memory using _malloc(),
//             initialize it with setValue(), and so forth.
// @slab: An array of data, or a number. If a number, then the size of the block to allocate,
//        in *bytes* (note that this is sometimes confusing: the next parameter does not
//        affect this!)
// @types: Either an array of types, one for each byte (or 0 if no type at that position),
//         or a single type which is used for the entire block. This only matters if there
//         is initial data - if @slab is a number, then this does not matter at all and is
//         ignored.
// @allocator: How to allocate memory, see ALLOC_*
function allocate(slab, types, allocator, ptr) {
  var zeroinit, size;
  if (typeof slab === 'number') {
    zeroinit = true;
    size = slab;
  } else {
    zeroinit = false;
    size = slab.length;
  }

  var singleType = typeof types === 'string' ? types : null;

  var ret;
  if (allocator == ALLOC_NONE) {
    ret = ptr;
  } else {
    ret = [_malloc, Runtime.stackAlloc, Runtime.staticAlloc, Runtime.dynamicAlloc][allocator === undefined ? ALLOC_STATIC : allocator](Math.max(size, singleType ? 1 : types.length));
  }

  if (zeroinit) {
    var ptr = ret, stop;
    assert((ret & 3) == 0);
    stop = ret + (size & ~3);
    for (; ptr < stop; ptr += 4) {
      HEAP32[((ptr)>>2)]=0;
    }
    stop = ret + size;
    while (ptr < stop) {
      HEAP8[((ptr++)>>0)]=0;
    }
    return ret;
  }

  if (singleType === 'i8') {
    if (slab.subarray || slab.slice) {
      HEAPU8.set(slab, ret);
    } else {
      HEAPU8.set(new Uint8Array(slab), ret);
    }
    return ret;
  }

  var i = 0, type, typeSize, previousType;
  while (i < size) {
    var curr = slab[i];

    if (typeof curr === 'function') {
      curr = Runtime.getFunctionIndex(curr);
    }

    type = singleType || types[i];
    if (type === 0) {
      i++;
      continue;
    }
    assert(type, 'Must know what type to store in allocate!');

    if (type == 'i64') type = 'i32'; // special case: we have one i32 here, and one i32 later

    setValue(ret+i, curr, type);

    // no need to look up size unless type changes, so cache it
    if (previousType !== type) {
      typeSize = Runtime.getNativeTypeSize(type);
      previousType = type;
    }
    i += typeSize;
  }

  return ret;
}
Module['allocate'] = allocate;

function Pointer_stringify(ptr, /* optional */ length) {
  if (length === 0 || !ptr) return '';
  // TODO: use TextDecoder
  // Find the length, and check for UTF while doing so
  var hasUtf = 0;
  var t;
  var i = 0;
  while (1) {
    assert(ptr + i < TOTAL_MEMORY);
    t = HEAPU8[(((ptr)+(i))>>0)];
    hasUtf |= t;
    if (t == 0 && !length) break;
    i++;
    if (length && i == length) break;
  }
  if (!length) length = i;

  var ret = '';

  if (hasUtf < 128) {
    var MAX_CHUNK = 1024; // split up into chunks, because .apply on a huge string can overflow the stack
    var curr;
    while (length > 0) {
      curr = String.fromCharCode.apply(String, HEAPU8.subarray(ptr, ptr + Math.min(length, MAX_CHUNK)));
      ret = ret ? ret + curr : curr;
      ptr += MAX_CHUNK;
      length -= MAX_CHUNK;
    }
    return ret;
  }
  return Module['UTF8ToString'](ptr);
}
Module['Pointer_stringify'] = Pointer_stringify;

// Given a pointer 'ptr' to a null-terminated ASCII-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

function AsciiToString(ptr) {
  var str = '';
  while (1) {
    var ch = HEAP8[((ptr++)>>0)];
    if (!ch) return str;
    str += String.fromCharCode(ch);
  }
}
Module['AsciiToString'] = AsciiToString;

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in ASCII form. The copy will require at most str.length+1 bytes of space in the HEAP.

function stringToAscii(str, outPtr) {
  return writeAsciiToMemory(str, outPtr, false);
}
Module['stringToAscii'] = stringToAscii;

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the a given array that contains uint8 values, returns
// a copy of that string as a Javascript String object.

function UTF8ArrayToString(u8Array, idx) {
  var u0, u1, u2, u3, u4, u5;

  var str = '';
  while (1) {
    // For UTF8 byte structure, see http://en.wikipedia.org/wiki/UTF-8#Description and https://www.ietf.org/rfc/rfc2279.txt and https://tools.ietf.org/html/rfc3629
    u0 = u8Array[idx++];
    if (!u0) return str;
    if (!(u0 & 0x80)) { str += String.fromCharCode(u0); continue; }
    u1 = u8Array[idx++] & 63;
    if ((u0 & 0xE0) == 0xC0) { str += String.fromCharCode(((u0 & 31) << 6) | u1); continue; }
    u2 = u8Array[idx++] & 63;
    if ((u0 & 0xF0) == 0xE0) {
      u0 = ((u0 & 15) << 12) | (u1 << 6) | u2;
    } else {
      u3 = u8Array[idx++] & 63;
      if ((u0 & 0xF8) == 0xF0) {
        u0 = ((u0 & 7) << 18) | (u1 << 12) | (u2 << 6) | u3;
      } else {
        u4 = u8Array[idx++] & 63;
        if ((u0 & 0xFC) == 0xF8) {
          u0 = ((u0 & 3) << 24) | (u1 << 18) | (u2 << 12) | (u3 << 6) | u4;
        } else {
          u5 = u8Array[idx++] & 63;
          u0 = ((u0 & 1) << 30) | (u1 << 24) | (u2 << 18) | (u3 << 12) | (u4 << 6) | u5;
        }
      }
    }
    if (u0 < 0x10000) {
      str += String.fromCharCode(u0);
    } else {
      var ch = u0 - 0x10000;
      str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
    }
  }
}
Module['UTF8ArrayToString'] = UTF8ArrayToString;

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

function UTF8ToString(ptr) {
  return UTF8ArrayToString(HEAPU8, ptr);
}
Module['UTF8ToString'] = UTF8ToString;

// Copies the given Javascript String object 'str' to the given byte array at address 'outIdx',
// encoded in UTF8 form and null-terminated. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outU8Array: the array to copy to. Each index in this array is assumed to be one 8-byte element.
//   outIdx: The starting offset in the array to begin the copying.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null 
//                    terminator, i.e. if maxBytesToWrite=1, only the null terminator will be written and nothing else.
//                    maxBytesToWrite=0 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8Array(str, outU8Array, outIdx, maxBytesToWrite) {
  if (!(maxBytesToWrite > 0)) // Parameter maxBytesToWrite is not optional. Negative values, 0, null, undefined and false each don't write out any bytes.
    return 0;

  var startIdx = outIdx;
  var endIdx = outIdx + maxBytesToWrite - 1; // -1 for string null terminator.
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    // For UTF8 byte structure, see http://en.wikipedia.org/wiki/UTF-8#Description and https://www.ietf.org/rfc/rfc2279.txt and https://tools.ietf.org/html/rfc3629
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) u = 0x10000 + ((u & 0x3FF) << 10) | (str.charCodeAt(++i) & 0x3FF);
    if (u <= 0x7F) {
      if (outIdx >= endIdx) break;
      outU8Array[outIdx++] = u;
    } else if (u <= 0x7FF) {
      if (outIdx + 1 >= endIdx) break;
      outU8Array[outIdx++] = 0xC0 | (u >> 6);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0xFFFF) {
      if (outIdx + 2 >= endIdx) break;
      outU8Array[outIdx++] = 0xE0 | (u >> 12);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0x1FFFFF) {
      if (outIdx + 3 >= endIdx) break;
      outU8Array[outIdx++] = 0xF0 | (u >> 18);
      outU8Array[outIdx++] = 0x80 | ((u >> 12) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0x3FFFFFF) {
      if (outIdx + 4 >= endIdx) break;
      outU8Array[outIdx++] = 0xF8 | (u >> 24);
      outU8Array[outIdx++] = 0x80 | ((u >> 18) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 12) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else {
      if (outIdx + 5 >= endIdx) break;
      outU8Array[outIdx++] = 0xFC | (u >> 30);
      outU8Array[outIdx++] = 0x80 | ((u >> 24) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 18) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 12) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    }
  }
  // Null-terminate the pointer to the buffer.
  outU8Array[outIdx] = 0;
  return outIdx - startIdx;
}
Module['stringToUTF8Array'] = stringToUTF8Array;

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF8 form. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8(str, outPtr, maxBytesToWrite) {
  assert(typeof maxBytesToWrite == 'number', 'stringToUTF8(str, outPtr, maxBytesToWrite) is missing the third parameter that specifies the length of the output buffer!');
  return stringToUTF8Array(str, HEAPU8, outPtr, maxBytesToWrite);
}
Module['stringToUTF8'] = stringToUTF8;

// Returns the number of bytes the given Javascript string takes if encoded as a UTF8 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF8(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) u = 0x10000 + ((u & 0x3FF) << 10) | (str.charCodeAt(++i) & 0x3FF);
    if (u <= 0x7F) {
      ++len;
    } else if (u <= 0x7FF) {
      len += 2;
    } else if (u <= 0xFFFF) {
      len += 3;
    } else if (u <= 0x1FFFFF) {
      len += 4;
    } else if (u <= 0x3FFFFFF) {
      len += 5;
    } else {
      len += 6;
    }
  }
  return len;
}
Module['lengthBytesUTF8'] = lengthBytesUTF8;

// Given a pointer 'ptr' to a null-terminated UTF16LE-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

function UTF16ToString(ptr) {
  var i = 0;

  var str = '';
  while (1) {
    var codeUnit = HEAP16[(((ptr)+(i*2))>>1)];
    if (codeUnit == 0)
      return str;
    ++i;
    // fromCharCode constructs a character from a UTF-16 code unit, so we can pass the UTF16 string right through.
    str += String.fromCharCode(codeUnit);
  }
}
Module['UTF16ToString'] = UTF16ToString;

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF16 form. The copy will require at most str.length*4+2 bytes of space in the HEAP.
// Use the function lengthBytesUTF16() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null 
//                    terminator, i.e. if maxBytesToWrite=2, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<2 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF16(str, outPtr, maxBytesToWrite) {
  assert(typeof maxBytesToWrite == 'number', 'stringToUTF16(str, outPtr, maxBytesToWrite) is missing the third parameter that specifies the length of the output buffer!');
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 2) return 0;
  maxBytesToWrite -= 2; // Null terminator.
  var startPtr = outPtr;
  var numCharsToWrite = (maxBytesToWrite < str.length*2) ? (maxBytesToWrite / 2) : str.length;
  for (var i = 0; i < numCharsToWrite; ++i) {
    // charCodeAt returns a UTF-16 encoded code unit, so it can be directly written to the HEAP.
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    HEAP16[((outPtr)>>1)]=codeUnit;
    outPtr += 2;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP16[((outPtr)>>1)]=0;
  return outPtr - startPtr;
}
Module['stringToUTF16'] = stringToUTF16;

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF16(str) {
  return str.length*2;
}
Module['lengthBytesUTF16'] = lengthBytesUTF16;

function UTF32ToString(ptr) {
  var i = 0;

  var str = '';
  while (1) {
    var utf32 = HEAP32[(((ptr)+(i*4))>>2)];
    if (utf32 == 0)
      return str;
    ++i;
    // Gotcha: fromCharCode constructs a character from a UTF-16 encoded code (pair), not from a Unicode code point! So encode the code point to UTF-16 for constructing.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    if (utf32 >= 0x10000) {
      var ch = utf32 - 0x10000;
      str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
    } else {
      str += String.fromCharCode(utf32);
    }
  }
}
Module['UTF32ToString'] = UTF32ToString;

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF32 form. The copy will require at most str.length*4+4 bytes of space in the HEAP.
// Use the function lengthBytesUTF32() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null 
//                    terminator, i.e. if maxBytesToWrite=4, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<4 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF32(str, outPtr, maxBytesToWrite) {
  assert(typeof maxBytesToWrite == 'number', 'stringToUTF32(str, outPtr, maxBytesToWrite) is missing the third parameter that specifies the length of the output buffer!');
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 4) return 0;
  var startPtr = outPtr;
  var endPtr = startPtr + maxBytesToWrite - 4;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) {
      var trailSurrogate = str.charCodeAt(++i);
      codeUnit = 0x10000 + ((codeUnit & 0x3FF) << 10) | (trailSurrogate & 0x3FF);
    }
    HEAP32[((outPtr)>>2)]=codeUnit;
    outPtr += 4;
    if (outPtr + 4 > endPtr) break;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP32[((outPtr)>>2)]=0;
  return outPtr - startPtr;
}
Module['stringToUTF32'] = stringToUTF32;

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF32(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i);
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) ++i; // possibly a lead surrogate, so skip over the tail surrogate.
    len += 4;
  }

  return len;
}
Module['lengthBytesUTF32'] = lengthBytesUTF32;

function demangle(func) {
  var hasLibcxxabi = !!Module['___cxa_demangle'];
  if (hasLibcxxabi) {
    try {
      var buf = _malloc(func.length);
      writeStringToMemory(func.substr(1), buf);
      var status = _malloc(4);
      var ret = Module['___cxa_demangle'](buf, 0, 0, status);
      if (getValue(status, 'i32') === 0 && ret) {
        return Pointer_stringify(ret);
      }
      // otherwise, libcxxabi failed, we can try ours which may return a partial result
    } catch(e) {
      // failure when using libcxxabi, we can try ours which may return a partial result
    } finally {
      if (buf) _free(buf);
      if (status) _free(status);
      if (ret) _free(ret);
    }
  }
  var i = 3;
  // params, etc.
  var basicTypes = {
    'v': 'void',
    'b': 'bool',
    'c': 'char',
    's': 'short',
    'i': 'int',
    'l': 'long',
    'f': 'float',
    'd': 'double',
    'w': 'wchar_t',
    'a': 'signed char',
    'h': 'unsigned char',
    't': 'unsigned short',
    'j': 'unsigned int',
    'm': 'unsigned long',
    'x': 'long long',
    'y': 'unsigned long long',
    'z': '...'
  };
  var subs = [];
  var first = true;
  function dump(x) {
    //return;
    if (x) Module.print(x);
    Module.print(func);
    var pre = '';
    for (var a = 0; a < i; a++) pre += ' ';
    Module.print (pre + '^');
  }
  function parseNested() {
    i++;
    if (func[i] === 'K') i++; // ignore const
    var parts = [];
    while (func[i] !== 'E') {
      if (func[i] === 'S') { // substitution
        i++;
        var next = func.indexOf('_', i);
        var num = func.substring(i, next) || 0;
        parts.push(subs[num] || '?');
        i = next+1;
        continue;
      }
      if (func[i] === 'C') { // constructor
        parts.push(parts[parts.length-1]);
        i += 2;
        continue;
      }
      var size = parseInt(func.substr(i));
      var pre = size.toString().length;
      if (!size || !pre) { i--; break; } // counter i++ below us
      var curr = func.substr(i + pre, size);
      parts.push(curr);
      subs.push(curr);
      i += pre + size;
    }
    i++; // skip E
    return parts;
  }
  function parse(rawList, limit, allowVoid) { // main parser
    limit = limit || Infinity;
    var ret = '', list = [];
    function flushList() {
      return '(' + list.join(', ') + ')';
    }
    var name;
    if (func[i] === 'N') {
      // namespaced N-E
      name = parseNested().join('::');
      limit--;
      if (limit === 0) return rawList ? [name] : name;
    } else {
      // not namespaced
      if (func[i] === 'K' || (first && func[i] === 'L')) i++; // ignore const and first 'L'
      var size = parseInt(func.substr(i));
      if (size) {
        var pre = size.toString().length;
        name = func.substr(i + pre, size);
        i += pre + size;
      }
    }
    first = false;
    if (func[i] === 'I') {
      i++;
      var iList = parse(true);
      var iRet = parse(true, 1, true);
      ret += iRet[0] + ' ' + name + '<' + iList.join(', ') + '>';
    } else {
      ret = name;
    }
    paramLoop: while (i < func.length && limit-- > 0) {
      //dump('paramLoop');
      var c = func[i++];
      if (c in basicTypes) {
        list.push(basicTypes[c]);
      } else {
        switch (c) {
          case 'P': list.push(parse(true, 1, true)[0] + '*'); break; // pointer
          case 'R': list.push(parse(true, 1, true)[0] + '&'); break; // reference
          case 'L': { // literal
            i++; // skip basic type
            var end = func.indexOf('E', i);
            var size = end - i;
            list.push(func.substr(i, size));
            i += size + 2; // size + 'EE'
            break;
          }
          case 'A': { // array
            var size = parseInt(func.substr(i));
            i += size.toString().length;
            if (func[i] !== '_') throw '?';
            i++; // skip _
            list.push(parse(true, 1, true)[0] + ' [' + size + ']');
            break;
          }
          case 'E': break paramLoop;
          default: ret += '?' + c; break paramLoop;
        }
      }
    }
    if (!allowVoid && list.length === 1 && list[0] === 'void') list = []; // avoid (void)
    if (rawList) {
      if (ret) {
        list.push(ret + '?');
      }
      return list;
    } else {
      return ret + flushList();
    }
  }
  var parsed = func;
  try {
    // Special-case the entry point, since its name differs from other name mangling.
    if (func == 'Object._main' || func == '_main') {
      return 'main()';
    }
    if (typeof func === 'number') func = Pointer_stringify(func);
    if (func[0] !== '_') return func;
    if (func[1] !== '_') return func; // C function
    if (func[2] !== 'Z') return func;
    switch (func[3]) {
      case 'n': return 'operator new()';
      case 'd': return 'operator delete()';
    }
    parsed = parse();
  } catch(e) {
    parsed += '?';
  }
  if (parsed.indexOf('?') >= 0 && !hasLibcxxabi) {
    Runtime.warnOnce('warning: a problem occurred in builtin C++ name demangling; build with  -s DEMANGLE_SUPPORT=1  to link in libcxxabi demangling');
  }
  return parsed;
}

function demangleAll(text) {
  return text.replace(/__Z[\w\d_]+/g, function(x) { var y = demangle(x); return x === y ? x : (x + ' [' + y + ']') });
}

function jsStackTrace() {
  var err = new Error();
  if (!err.stack) {
    // IE10+ special cases: It does have callstack info, but it is only populated if an Error object is thrown,
    // so try that as a special-case.
    try {
      throw new Error(0);
    } catch(e) {
      err = e;
    }
    if (!err.stack) {
      return '(no stack trace available)';
    }
  }
  return err.stack.toString();
}

function stackTrace() {
  return demangleAll(jsStackTrace());
}
Module['stackTrace'] = stackTrace;

// Memory management

var PAGE_SIZE = 4096;

function alignMemoryPage(x) {
  if (x % 4096 > 0) {
    x += (4096 - (x % 4096));
  }
  return x;
}

var HEAP;
var HEAP8, HEAPU8, HEAP16, HEAPU16, HEAP32, HEAPU32, HEAPF32, HEAPF64;

var STATIC_BASE = 0, STATICTOP = 0, staticSealed = false; // static area
var STACK_BASE = 0, STACKTOP = 0, STACK_MAX = 0; // stack area
var DYNAMIC_BASE = 0, DYNAMICTOP = 0; // dynamic area handled by sbrk

function enlargeMemory() {
  abort('Cannot enlarge memory arrays. Either (1) compile with -s TOTAL_MEMORY=X with X higher than the current value ' + TOTAL_MEMORY + ', (2) compile with ALLOW_MEMORY_GROWTH which adjusts the size at runtime but prevents some optimizations, or (3) set Module.TOTAL_MEMORY before the program runs.');
}


var TOTAL_STACK = Module['TOTAL_STACK'] || 5242880;
var TOTAL_MEMORY = Module['TOTAL_MEMORY'] || 16777216;


var totalMemory = 64*1024;
while (totalMemory < TOTAL_MEMORY || totalMemory < 2*TOTAL_STACK) {
  if (totalMemory < 16*1024*1024) {
    totalMemory *= 2;
  } else {
    totalMemory += 16*1024*1024
  }
}
if (totalMemory !== TOTAL_MEMORY) {
  Module.printErr('increasing TOTAL_MEMORY to ' + totalMemory + ' to be compliant with the asm.js spec (and given that TOTAL_STACK=' + TOTAL_STACK + ')');
  TOTAL_MEMORY = totalMemory;
}



// Initialize the runtime's memory
// check for full engine support (use string 'subarray' to avoid closure compiler confusion)
assert(typeof Int32Array !== 'undefined' && typeof Float64Array !== 'undefined' && !!(new Int32Array(1)['subarray']) && !!(new Int32Array(1)['set']),
       'JS engine does not provide full typed array support');


var buffer = new ArrayBuffer(TOTAL_MEMORY);

HEAP8 = new Int8Array(buffer);
HEAP16 = new Int16Array(buffer);
HEAP32 = new Int32Array(buffer);
HEAPU8 = new Uint8Array(buffer);
HEAPU16 = new Uint16Array(buffer);
HEAPU32 = new Uint32Array(buffer);
HEAPF32 = new Float32Array(buffer);
HEAPF64 = new Float64Array(buffer);

// Endianness check (note: assumes compiler arch was little-endian)
HEAP32[0] = 255;
assert(HEAPU8[0] === 255 && HEAPU8[3] === 0, 'Typed arrays 2 must be run on a little-endian system');

Module['HEAP'] = HEAP;
Module['buffer'] = buffer;
Module['HEAP8'] = HEAP8;
Module['HEAP16'] = HEAP16;
Module['HEAP32'] = HEAP32;
Module['HEAPU8'] = HEAPU8;
Module['HEAPU16'] = HEAPU16;
Module['HEAPU32'] = HEAPU32;
Module['HEAPF32'] = HEAPF32;
Module['HEAPF64'] = HEAPF64;

function callRuntimeCallbacks(callbacks) {
  while(callbacks.length > 0) {
    var callback = callbacks.shift();
    if (typeof callback == 'function') {
      callback();
      continue;
    }
    var func = callback.func;
    if (typeof func === 'number') {
      if (callback.arg === undefined) {
        Runtime.dynCall('v', func);
      } else {
        Runtime.dynCall('vi', func, [callback.arg]);
      }
    } else {
      func(callback.arg === undefined ? null : callback.arg);
    }
  }
}

var __ATPRERUN__  = []; // functions called before the runtime is initialized
var __ATINIT__    = []; // functions called during startup
var __ATMAIN__    = []; // functions called when main() is to be run
var __ATEXIT__    = []; // functions called during shutdown
var __ATPOSTRUN__ = []; // functions called after the runtime has exited

var runtimeInitialized = false;
var runtimeExited = false;

function preRun() {
  // compatibility - merge in anything from Module['preRun'] at this time
  if (Module['preRun']) {
    if (typeof Module['preRun'] == 'function') Module['preRun'] = [Module['preRun']];
    while (Module['preRun'].length) {
      addOnPreRun(Module['preRun'].shift());
    }
  }
  callRuntimeCallbacks(__ATPRERUN__);
}

function ensureInitRuntime() {
  if (runtimeInitialized) return;
  runtimeInitialized = true;
  callRuntimeCallbacks(__ATINIT__);
}

function preMain() {
  callRuntimeCallbacks(__ATMAIN__);
}

function exitRuntime() {
  callRuntimeCallbacks(__ATEXIT__);
  runtimeExited = true;
}

function postRun() {
  // compatibility - merge in anything from Module['postRun'] at this time
  if (Module['postRun']) {
    if (typeof Module['postRun'] == 'function') Module['postRun'] = [Module['postRun']];
    while (Module['postRun'].length) {
      addOnPostRun(Module['postRun'].shift());
    }
  }
  callRuntimeCallbacks(__ATPOSTRUN__);
}

function addOnPreRun(cb) {
  __ATPRERUN__.unshift(cb);
}
Module['addOnPreRun'] = Module.addOnPreRun = addOnPreRun;

function addOnInit(cb) {
  __ATINIT__.unshift(cb);
}
Module['addOnInit'] = Module.addOnInit = addOnInit;

function addOnPreMain(cb) {
  __ATMAIN__.unshift(cb);
}
Module['addOnPreMain'] = Module.addOnPreMain = addOnPreMain;

function addOnExit(cb) {
  __ATEXIT__.unshift(cb);
}
Module['addOnExit'] = Module.addOnExit = addOnExit;

function addOnPostRun(cb) {
  __ATPOSTRUN__.unshift(cb);
}
Module['addOnPostRun'] = Module.addOnPostRun = addOnPostRun;

// Tools


function intArrayFromString(stringy, dontAddNull, length /* optional */) {
  var len = length > 0 ? length : lengthBytesUTF8(stringy)+1;
  var u8array = new Array(len);
  var numBytesWritten = stringToUTF8Array(stringy, u8array, 0, u8array.length);
  if (dontAddNull) u8array.length = numBytesWritten;
  return u8array;
}
Module['intArrayFromString'] = intArrayFromString;

function intArrayToString(array) {
  var ret = [];
  for (var i = 0; i < array.length; i++) {
    var chr = array[i];
    if (chr > 0xFF) {
        assert(false, 'Character code ' + chr + ' (' + String.fromCharCode(chr) + ')  at offset ' + i + ' not in 0x00-0xFF.');
      chr &= 0xFF;
    }
    ret.push(String.fromCharCode(chr));
  }
  return ret.join('');
}
Module['intArrayToString'] = intArrayToString;

function writeStringToMemory(string, buffer, dontAddNull) {
  var array = intArrayFromString(string, dontAddNull);
  var i = 0;
  while (i < array.length) {
    var chr = array[i];
    HEAP8[(((buffer)+(i))>>0)]=chr;
    i = i + 1;
  }
}
Module['writeStringToMemory'] = writeStringToMemory;

function writeArrayToMemory(array, buffer) {
  for (var i = 0; i < array.length; i++) {
    HEAP8[((buffer++)>>0)]=array[i];
  }
}
Module['writeArrayToMemory'] = writeArrayToMemory;

function writeAsciiToMemory(str, buffer, dontAddNull) {
  for (var i = 0; i < str.length; ++i) {
    assert(str.charCodeAt(i) === str.charCodeAt(i)&0xff);
    HEAP8[((buffer++)>>0)]=str.charCodeAt(i);
  }
  // Null-terminate the pointer to the HEAP.
  if (!dontAddNull) HEAP8[((buffer)>>0)]=0;
}
Module['writeAsciiToMemory'] = writeAsciiToMemory;

function unSign(value, bits, ignore) {
  if (value >= 0) {
    return value;
  }
  return bits <= 32 ? 2*Math.abs(1 << (bits-1)) + value // Need some trickery, since if bits == 32, we are right at the limit of the bits JS uses in bitshifts
                    : Math.pow(2, bits)         + value;
}
function reSign(value, bits, ignore) {
  if (value <= 0) {
    return value;
  }
  var half = bits <= 32 ? Math.abs(1 << (bits-1)) // abs is needed if bits == 32
                        : Math.pow(2, bits-1);
  if (value >= half && (bits <= 32 || value > half)) { // for huge values, we can hit the precision limit and always get true here. so don't do that
                                                       // but, in general there is no perfect solution here. With 64-bit ints, we get rounding and errors
                                                       // TODO: In i64 mode 1, resign the two parts separately and safely
    value = -2*half + value; // Cannot bitshift half, as it may be at the limit of the bits JS uses in bitshifts
  }
  return value;
}

// check for imul support, and also for correctness ( https://bugs.webkit.org/show_bug.cgi?id=126345 )
if (!Math['imul'] || Math['imul'](0xffffffff, 5) !== -5) Math['imul'] = function imul(a, b) {
  var ah  = a >>> 16;
  var al = a & 0xffff;
  var bh  = b >>> 16;
  var bl = b & 0xffff;
  return (al*bl + ((ah*bl + al*bh) << 16))|0;
};
Math.imul = Math['imul'];


if (!Math['clz32']) Math['clz32'] = function(x) {
  x = x >>> 0;
  for (var i = 0; i < 32; i++) {
    if (x & (1 << (31 - i))) return i;
  }
  return 32;
};
Math.clz32 = Math['clz32']

var Math_abs = Math.abs;
var Math_cos = Math.cos;
var Math_sin = Math.sin;
var Math_tan = Math.tan;
var Math_acos = Math.acos;
var Math_asin = Math.asin;
var Math_atan = Math.atan;
var Math_atan2 = Math.atan2;
var Math_exp = Math.exp;
var Math_log = Math.log;
var Math_sqrt = Math.sqrt;
var Math_ceil = Math.ceil;
var Math_floor = Math.floor;
var Math_pow = Math.pow;
var Math_imul = Math.imul;
var Math_fround = Math.fround;
var Math_min = Math.min;
var Math_clz32 = Math.clz32;

// A counter of dependencies for calling run(). If we need to
// do asynchronous work before running, increment this and
// decrement it. Incrementing must happen in a place like
// PRE_RUN_ADDITIONS (used by emcc to add file preloading).
// Note that you can add dependencies in preRun, even though
// it happens right before run - run will be postponed until
// the dependencies are met.
var runDependencies = 0;
var runDependencyWatcher = null;
var dependenciesFulfilled = null; // overridden to take different actions when all run dependencies are fulfilled
var runDependencyTracking = {};

function addRunDependency(id) {
  runDependencies++;
  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }
  if (id) {
    assert(!runDependencyTracking[id]);
    runDependencyTracking[id] = 1;
    if (runDependencyWatcher === null && typeof setInterval !== 'undefined') {
      // Check for missing dependencies every few seconds
      runDependencyWatcher = setInterval(function() {
        if (ABORT) {
          clearInterval(runDependencyWatcher);
          runDependencyWatcher = null;
          return;
        }
        var shown = false;
        for (var dep in runDependencyTracking) {
          if (!shown) {
            shown = true;
            Module.printErr('still waiting on run dependencies:');
          }
          Module.printErr('dependency: ' + dep);
        }
        if (shown) {
          Module.printErr('(end of list)');
        }
      }, 10000);
    }
  } else {
    Module.printErr('warning: run dependency added without ID');
  }
}
Module['addRunDependency'] = addRunDependency;
function removeRunDependency(id) {
  runDependencies--;
  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }
  if (id) {
    assert(runDependencyTracking[id]);
    delete runDependencyTracking[id];
  } else {
    Module.printErr('warning: run dependency removed without ID');
  }
  if (runDependencies == 0) {
    if (runDependencyWatcher !== null) {
      clearInterval(runDependencyWatcher);
      runDependencyWatcher = null;
    }
    if (dependenciesFulfilled) {
      var callback = dependenciesFulfilled;
      dependenciesFulfilled = null;
      callback(); // can add another dependenciesFulfilled
    }
  }
}
Module['removeRunDependency'] = removeRunDependency;

Module["preloadedImages"] = {}; // maps url to image data
Module["preloadedAudios"] = {}; // maps url to audio data


var memoryInitializer = null;

// === Body ===





STATIC_BASE = 8;

STATICTOP = STATIC_BASE + 37472;
  /* global initializers */ __ATINIT__.push();
  

/* memory initializer */ allocate([0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,120,0,0,0,0,1,0,0,122,0,0,0,1,0,0,0,115,0,0,0,0,2,0,0,97,0,0,0,2,0,0,0,113,0,0,0,0,4,0,0,119,0,0,0,0,8,0,0,80,4,0,0,64,0,0,0,79,4,0,0,128,0,0,0,82,4,0,0,16,0,0,0,81,4,0,0,32,0,0,0,13,0,0,0,8,0,0,0,9,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,2,8,0,8,85,4,8,90,0,0,248,0,9,144,0,10,30,50,5,0,0,255,0,0,0,0,0,0,0,0,0,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,73,0,0,0,0,0,73,82,82,0,0,0,73,82,83,28,37,0,73,82,82,36,37,37,27,82,82,92,45,45,28,18,18,0,0,0,0,0,0,0,0,0,0,0,0,64,73,73,73,0,64,73,73,82,91,91,91,74,82,82,91,28,28,19,18,91,28,36,28,18,9,9,9,36,27,18,9,9,9,9,10,10,10,10,10,10,10,10,19,10,10,18,10,10,18,19,10,0,0,0,0,0,0,0,0,146,82,82,82,82,82,82,73,19,18,19,9,0,28,27,19,10,18,18,64,27,101,100,28,9,10,0,9,36,27,28,101,18,0,0,37,28,28,28,28,1,0,37,36,28,28,28,28,0,36,37,36,36,36,28,28,0,0,0,0,0,0,0,0,73,64,0,0,0,0,0,0,82,73,73,0,0,0,0,0,28,91,74,72,0,0,0,0,28,37,82,73,0,0,0,0,101,36,100,73,0,0,0,0,101,37,100,73,0,0,0,0,109,46,92,73,0,0,0,0,0,0,0,0,0,0,64,82,0,0,0,0,0,73,82,92,0,0,0,0,83,82,36,37,0,0,0,83,82,37,37,37,0,0,74,82,37,37,27,0,0,0,91,37,19,0,0,27,0,91,83,0,10,28,55,55,9,91,28,101,127,55,46,46,82,46,46,28,19,19,18,0,46,37,27,19,18,0,0,18,28,28,18,0,0,18,37,28,18,0,0,18,46,55,28,19,0,19,46,55,54,28,19,27,55,55,46,46,37,19,28,28,46,46,46,46,27,28,28,28,110,110,110,28,28,28,28,28,9,19,19,18,18,19,19,0,19,19,19,19,19,28,0,9,19,19,19,19,28,1,0,55,19,19,19,28,19,0,46,46,27,27,28,28,0,28,119,46,27,27,37,0,19,127,37,28,28,28,1,10,55,36,28,28,28,19,10,46,37,37,9,0,19,46,36,37,37,37,37,37,46,37,37,37,37,37,37,37,37,37,45,46,37,27,28,28,46,46,37,28,19,37,27,19,37,28,28,28,28,1,9,82,28,28,19,0,1,28,91,92,10,0,1,101,119,92,27,0,1,110,127,110,82,27,0,0,109,28,82,73,0,0,0,0,28,19,83,0,0,0,0,0,28,82,74,0,0,0,0,0,91,91,0,0,0,0,0,0,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,19,91,127,119,110,110,110,110,19,101,119,118,110,110,110,110,27,101,119,118,118,118,110,119,27,91,191,118,182,118,118,118,9,19,109,191,183,182,183,101,0,27,18,92,118,191,118,109,0,0,18,27,18,91,91,92,0,0,0,0,9,10,18,19,110,119,36,28,28,28,28,28,119,37,28,28,28,28,36,37,110,28,28,28,36,37,37,27,28,28,36,37,37,19,0,0,100,109,109,27,0,9,110,191,109,91,9,18,110,191,109,91,91,18,91,100,91,18,19,18,19,27,18,18,9,1,0,0,28,36,46,28,9,0,9,110,37,36,1,0,9,118,127,127,0,0,18,119,127,127,100,18,100,191,191,119,91,18,27,9,191,109,91,18,27,9,0,0,18,19,27,9,0,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,127,127,83,19,19,0,0,0,100,18,27,1,0,0,0,0,27,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,38,0,0,0,38,0,38,38,47,0,0,0,47,0,0,0,55,0,0,0,55,0,0,0,255,0,0,0,255,0,0,0,191,0,0,0,191,0,0,191,55,0,0,55,0,0,55,0,0,38,38,0,0,0,38,38,0,0,0,0,0,0,0,0,38,38,38,0,38,38,38,38,0,0,47,0,47,0,0,0,0,55,0,0,55,0,0,0,255,0,0,0,255,255,255,255,0,0,0,0,191,0,0,0,0,0,0,0,55,0,0,0,38,38,38,0,38,38,38,38,0,0,0,0,0,0,0,0,38,0,38,38,38,38,0,0,0,0,47,0,0,0,47,0,0,0,55,0,0,0,55,0,0,0,255,255,255,255,0,0,0,0,191,0,0,0,191,0,0,0,55,0,0,0,55,0,38,0,38,38,38,38,0,0,0,0,0,0,0,0,0,0,0,0,38,38,38,0,38,0,0,47,0,0,47,0,47,0,55,0,0,0,55,0,0,55,255,0,0,0,255,0,0,0,191,0,0,0,191,0,0,191,55,0,0,55,0,0,55,0,38,38,38,0,0,0,38,0,0,0,0,0,0,0,0,0,0,0,38,0,0,0,0,0,0,0,47,0,0,0,0,0,0,55,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,191,0,0,0,0,0,0,0,0,55,0,0,0,0,0,0,0,38,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,228,0,0,0,0,9,155,237,246,0,0,0,155,164,173,191,191,0,82,155,164,118,127,127,183,91,155,109,119,127,118,110,183,0,0,0,0,0,0,0,0,0,0,0,0,91,182,246,246,0,91,246,255,255,255,255,191,246,255,255,255,255,191,191,174,255,191,255,255,255,183,173,101,191,191,255,255,183,182,173,101,183,191,255,255,191,183,174,110,183,191,255,255,191,183,183,101,0,0,0,0,0,0,0,0,246,246,237,237,173,164,164,155,182,109,109,92,82,110,101,100,101,101,101,82,101,118,109,37,92,92,82,83,110,36,101,110,101,82,82,110,37,37,36,37,91,82,119,45,37,37,37,36,82,110,110,45,37,37,37,37,0,0,0,0,0,0,0,0,155,73,0,0,0,0,0,0,164,155,146,0,0,0,0,0,37,100,147,73,0,0,0,0,101,46,155,82,0,0,0,0,109,37,101,82,0,0,0,0,109,45,101,82,0,0,0,0,110,46,100,82,0,0,0,0,0,0,0,0,0,0,73,91,0,0,0,0,0,74,91,101,0,0,0,0,91,155,109,45,0,0,0,91,91,45,46,37,0,0,82,91,46,46,28,9,0,1,91,45,28,1,9,28,0,92,91,1,74,101,119,127,74,91,28,110,127,119,119,118,155,119,119,109,101,110,110,164,119,110,101,101,101,91,164,183,37,37,92,82,82,109,191,191,19,9,73,100,127,191,191,191,9,92,127,127,191,191,183,191,127,127,119,127,127,183,191,191,119,119,127,127,110,183,191,191,119,127,127,110,119,191,191,191,182,255,255,255,191,191,183,91,191,255,255,255,191,191,164,100,191,255,255,255,191,173,164,127,191,255,255,255,191,164,191,127,255,255,255,191,173,191,191,191,255,255,255,182,191,191,191,119,255,255,247,191,191,191,119,119,255,255,255,191,191,191,100,82,109,127,110,46,45,45,37,110,127,118,110,110,110,46,37,101,119,119,119,118,46,28,37,36,127,119,110,37,28,37,28,27,119,101,37,37,28,9,10,91,110,110,28,9,73,37,100,100,91,73,74,110,127,100,92,0,83,119,191,119,91,92,0,0,110,37,91,74,0,0,0,0,28,27,91,0,0,0,0,0,36,91,83,0,0,0,0,0,91,92,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,91,92,127,191,119,118,119,119,92,109,127,183,119,119,119,119,92,109,127,191,119,119,119,191,92,100,191,191,191,191,191,127,10,92,110,191,191,191,191,118,0,101,100,109,191,191,191,191,0,0,91,109,100,109,173,182,0,0,0,0,18,91,100,109,119,127,110,110,119,191,191,191,127,118,109,118,119,191,191,191,119,109,110,118,127,191,191,191,109,110,118,127,191,183,173,246,118,191,191,118,164,173,255,255,191,182,100,173,255,255,255,255,182,173,191,255,255,255,255,246,173,182,173,100,91,82,0,0,255,255,191,191,173,91,100,191,255,255,182,173,173,191,255,191,255,246,255,255,255,255,191,100,255,255,255,255,191,182,118,91,255,255,255,191,191,91,0,0,255,255,255,92,0,0,0,0,164,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,191,191,100,92,92,0,0,0,110,92,101,73,0,0,0,0,109,91,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,4,0,1,2,3,4,5,6,7,8,9,10,11,12,13,0,14,15,16,17,18,0,0,5,3,0,19,20,21,22,23,24,25,26,27,28,29,30,31], "i8", ALLOC_NONE, Runtime.GLOBAL_BASE);
/* memory initializer */ allocate([73,110,105,116,10], "i8", ALLOC_NONE, Runtime.GLOBAL_BASE+8624);
/* memory initializer */ allocate([16,16,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,58,59,59,60,25,27,26,25,25,27,26,25,25,27,26,25,67,68,68,69,25,27,28,29,29,30,26,25,25,27,26,25,43,44,44,45,25,27,40,41,41,42,26,25,25,27,26,25,25,25,25,25,25,27,52,53,53,54,26,25,25,27,28,29,29,29,29,29,29,30,52,53,53,54,28,29,29,30,58,59,59,59,59,59,59,60,52,53,53,54,64,65,65,66,61,40,41,41,41,41,42,63,55,56,56,57,26,25,25,27,61,55,56,56,56,56,57,63,49,50,50,51,26,25,25,27,61,49,50,50,50,50,51,63,64,65,65,66,26,25,25,27,67,68,68,68,68,68,68,69,26,25,25,27,26,25,25,27,64,65,65,65,65,65,65,66,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,27,5,2,17,18,13,8,3,1,26,25,25,27,26,25,25,27,5,3,17,18,13,8,2,3,26,25,25,27,28,29,29,30,5,1,17,18,13,8,3,3,26,25,25,27,40,41,41,42,5,2,17,18,13,8,3,1,26,25,25,27,52,53,53,54,5,1,17,18,13,8,3,1,28,29,29,30,52,53,53,54,5,2,17,18,13,8,1,2,37,38,38,39,52,53,53,54,5,1,17,18,13,8,2,3,37,38,38,39,55,56,56,57,5,3,17,18,13,8,3,1,37,38,38,39,31,32,32,33,5,2,17,18,13,8,2,1,58,59,59,60,49,50,50,51,5,1,17,18,13,8,1,1,61,62,62,63,64,65,65,66,5,3,17,18,13,8,2,2,61,62,62,63,26,25,25,27,5,1,17,18,13,8,3,3,67,68,68,69,26,25,25,27,6,3,17,18,13,8,2,3,64,65,65,66,26,25,25,27,5,1,17,18,13,8,1,2,26,25,25,27,26,25,25,27,5,2,17,18,13,8,3,2,26,25,25,27,26,25,25,27,5,1,17,18,13,8,2,2,26,25,25,27,0,0,0,0,0,0,16,16,28,29,29,30,26,25,25,25,25,25,25,27,28,29,29,30,37,38,38,39,28,29,29,29,29,29,29,30,31,32,32,33,37,38,38,38,38,38,38,39,31,32,32,32,32,32,32,33,43,44,44,44,44,44,44,45,49,50,50,50,50,50,50,51,5,5,5,5,5,5,5,5,6,5,5,6,6,6,5,5,1,1,1,1,1,1,3,2,1,2,1,2,3,2,1,3,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,1,3,2,1,3,1,2,2,1,1,1,1,3,3,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,64,65,65,66,64,65,65,65,65,65,65,66,64,65,65,66,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,28,29,29,30,26,25,25,27,28,29,29,29,29,29,29,30,34,35,35,36,26,25,25,27,43,44,44,44,44,44,44,45,46,47,47,48,26,25,25,27,5,5,5,6,5,6,5,5,5,5,6,5,26,25,25,27,6,1,3,3,1,2,2,3,1,1,3,1,26,25,25,27,5,1,14,15,15,15,15,15,15,15,15,15,26,25,25,27,5,1,17,18,18,18,18,18,18,18,18,18,26,25,25,27,5,2,17,18,12,12,12,12,12,12,12,12,26,25,25,27,5,2,17,18,13,18,10,10,10,10,10,10,26,25,25,27,5,1,17,18,13,8,3,3,3,2,3,3,26,25,25,27,5,2,17,18,13,8,2,1,1,3,1,3,26,25,25,27,5,2,17,18,13,8,2,2,64,65,65,66,26,25,25,27,5,3,17,18,13,8,1,2,26,25,25,27,26,25,25,27,5,1,17,18,13,8,2,1,26,25,25,27,26,25,25,27,5,1,17,18,13,8,1,2,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,27,5,1,17,18,13,8,3,2,26,25,25,27,26,25,25,27,6,2,17,18,13,8,2,2,28,29,29,30,26,25,25,27,5,3,17,18,13,8,1,3,34,35,35,36,26,25,25,27,6,2,17,18,13,8,2,1,46,47,47,48,26,25,25,27,5,1,17,18,13,8,1,1,4,6,6,6,26,25,25,27,6,3,17,18,13,8,3,3,2,1,1,2,26,25,25,27,5,3,17,18,13,18,15,15,15,15,15,15,26,25,25,27,5,2,17,18,13,18,18,18,18,18,18,18,26,25,25,27,6,1,17,18,12,12,12,12,12,12,12,12,26,25,25,27,5,2,9,10,10,10,10,10,10,10,10,10,26,25,25,27,6,3,3,2,1,3,2,1,1,2,2,3,26,25,25,27,5,1,2,1,1,1,3,2,1,2,2,1,26,25,25,27,64,65,65,65,65,65,65,66,64,65,65,66,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,0,0,0,0,0,0,16,16,28,29,29,30,26,25,25,25,25,25,25,27,26,25,25,27,31,32,32,33,28,29,29,29,29,29,29,30,26,25,25,27,31,32,32,33,37,38,38,38,38,38,38,39,26,25,25,27,49,50,50,51,43,44,44,44,44,44,44,45,26,25,25,27,6,5,6,5,6,6,5,5,5,5,5,5,26,25,25,27,1,2,1,1,2,2,2,3,3,1,2,1,26,25,25,27,15,15,15,15,15,15,15,15,15,16,3,3,26,25,25,27,18,18,18,18,18,18,18,18,18,8,3,1,26,25,25,27,12,12,12,12,12,12,12,12,13,8,2,3,26,25,25,27,10,10,10,10,10,10,18,18,13,8,2,3,26,25,25,27,1,2,2,2,1,3,17,18,13,8,1,2,26,25,25,27,3,3,3,1,2,1,17,18,13,8,2,3,26,25,25,27,64,65,65,66,2,1,17,18,13,8,3,2,26,25,25,27,26,25,25,27,7,2,17,18,13,8,1,2,26,25,25,27,26,25,25,27,6,1,17,18,13,8,2,2,26,25,25,27,26,25,25,27,6,1,17,18,13,8,2,3,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,27,6,3,17,18,13,8,2,2,26,25,25,27,26,25,25,27,6,1,17,18,13,8,3,2,26,25,25,27,28,29,29,30,6,1,17,18,13,8,1,3,26,25,25,27,43,44,44,45,6,3,17,18,13,8,3,3,26,25,25,27,6,6,6,6,6,2,17,18,13,8,2,1,26,25,25,27,2,2,2,2,3,2,17,18,13,8,1,2,26,25,25,27,15,15,15,15,15,15,18,18,13,8,2,1,26,25,25,27,18,18,18,18,18,18,18,18,13,8,2,3,26,25,25,27,12,12,12,12,12,12,12,12,18,8,3,3,26,25,25,27,10,10,10,10,10,10,10,10,10,11,3,2,26,25,25,27,1,2,3,3,2,2,3,1,1,1,3,3,26,25,25,27,3,2,3,2,2,2,1,2,3,1,2,3,26,25,25,27,64,65,65,66,64,65,65,65,65,65,65,66,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,27,5,3,17,18,13,8,1,1,26,25,25,27,28,29,29,30,6,2,17,18,13,8,3,2,28,29,29,30,37,38,38,39,5,1,17,18,13,8,2,1,34,35,35,36,43,44,44,45,5,3,17,18,13,8,1,3,46,47,47,48,6,5,5,5,6,2,17,18,13,8,2,2,4,6,6,6,1,2,1,2,1,3,17,18,13,8,2,1,2,3,2,1,15,15,15,15,15,15,18,18,18,18,15,15,15,15,15,15,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,12,12,12,12,12,12,18,18,18,18,12,12,12,12,12,12,10,10,10,10,10,10,18,18,18,18,10,10,10,10,10,10,3,2,3,1,1,1,17,18,18,8,3,1,3,3,2,2,1,3,3,1,2,1,17,18,13,8,3,3,3,1,3,3,64,65,65,66,7,3,17,18,13,8,1,2,64,65,65,66,26,25,25,27,5,2,17,18,13,8,3,1,26,25,25,27,26,25,25,27,5,2,17,18,13,8,3,1,26,25,25,27,26,25,25,27,5,3,17,18,13,8,2,3,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,27,28,29,29,29,29,29,29,30,26,25,25,27,28,29,29,30,31,32,32,32,32,32,32,33,28,29,29,30,37,38,38,39,31,32,32,32,32,32,32,33,37,38,38,39,43,44,44,45,49,50,50,50,50,50,50,51,43,44,44,45,5,5,5,5,5,6,5,6,5,5,5,6,5,5,5,5,1,2,1,3,3,1,1,3,2,1,1,2,3,2,2,2,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,10,10,10,10,10,10,18,18,18,18,10,10,10,10,10,10,2,1,2,1,1,1,17,18,13,8,2,3,1,2,1,1,3,3,1,3,2,1,17,18,13,8,1,1,1,1,2,2,64,65,65,66,2,1,17,18,13,8,3,3,64,65,65,66,26,25,25,27,7,2,17,18,13,8,1,1,26,25,25,27,26,25,25,27,5,2,17,18,13,8,1,2,26,25,25,27,26,25,25,27,5,1,17,18,13,8,1,1,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,27,6,2,17,18,13,8,2,2,26,25,25,27,28,29,29,30,5,2,17,18,13,8,2,3,28,29,29,30,31,32,32,33,5,3,17,18,13,8,3,3,58,59,59,60,49,50,50,51,5,3,17,18,13,8,1,1,61,62,62,63,5,5,6,5,5,1,17,18,13,8,1,2,61,62,62,63,2,2,1,3,3,1,17,18,13,8,2,3,61,62,62,63,15,15,15,15,15,15,18,18,13,8,1,1,61,62,62,63,18,18,18,18,18,18,18,18,13,8,1,3,61,62,62,63,12,12,12,12,12,12,18,18,13,8,3,2,61,62,62,63,10,10,10,10,10,10,18,18,13,8,3,2,61,62,62,63,1,3,3,3,2,1,17,18,13,8,1,3,61,62,62,63,1,1,2,3,1,2,17,18,13,8,3,3,67,68,68,69,64,65,65,66,2,2,17,18,13,8,1,3,64,65,65,66,26,25,25,27,7,1,17,18,13,8,2,2,26,25,25,27,26,25,25,27,5,3,17,18,13,8,2,3,26,25,25,27,26,25,25,27,5,1,17,18,13,8,1,1,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,27,6,1,17,18,13,8,2,1,26,25,25,27,28,29,29,30,5,1,17,18,13,8,1,1,28,29,29,30,34,35,35,36,6,1,17,18,13,8,2,1,37,38,38,39,46,47,47,48,5,3,17,18,13,8,1,1,43,44,44,45,5,5,5,5,6,3,17,18,13,8,1,3,4,5,5,6,2,3,2,1,2,1,17,18,13,8,3,3,3,1,2,1,15,15,15,15,15,15,18,18,18,18,15,15,15,15,15,15,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,3,2,1,1,3,3,3,1,3,2,2,3,2,3,3,2,1,3,1,1,1,2,2,3,1,3,1,1,3,3,3,1,64,65,65,66,64,65,65,65,65,65,65,66,64,65,65,66,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,0,0,0,0,0,0,16,16,64,65,65,66,5,1,17,18,13,8,2,3,28,29,29,30,26,25,25,27,5,1,17,18,13,8,2,2,37,38,38,39,26,25,25,27,5,2,17,18,13,8,2,1,37,38,38,39,26,25,25,27,6,1,17,18,13,8,3,3,43,44,44,45,26,25,25,27,5,2,17,18,13,8,1,3,4,5,5,5,26,25,25,27,6,2,17,18,13,8,1,1,1,1,1,3,26,25,25,27,5,3,17,18,13,18,15,15,15,15,15,15,26,25,25,27,5,3,17,18,13,18,18,18,18,18,18,18,26,25,25,27,6,3,17,18,13,18,12,12,12,12,12,12,26,25,25,27,5,3,17,18,13,18,10,10,10,10,10,10,26,25,25,27,6,2,17,18,13,8,1,1,3,2,2,1,28,29,29,30,5,1,17,18,13,8,3,3,2,3,1,3,64,65,65,66,5,2,17,18,13,8,3,2,64,65,65,66,26,25,25,27,5,2,17,18,13,8,1,2,26,25,25,27,26,25,25,27,6,1,17,18,13,8,1,2,26,25,25,27,26,25,25,27,5,1,17,18,13,8,2,3,26,25,25,27,0,0,0,0,0,0,16,16,28,29,29,30,28,29,29,29,29,29,29,30,28,29,29,30,37,38,38,39,37,38,38,38,38,38,38,39,37,38,38,39,43,44,44,45,43,44,44,44,44,44,44,45,43,44,44,45,5,5,5,5,5,5,5,5,6,5,5,6,5,5,5,6,70,70,70,70,70,70,70,70,70,70,70,70,70,70,70,70,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,71,64,65,65,66,64,65,65,65,65,65,65,66,64,65,65,66,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,28,29,29,29,30,72,71,71,71,71,71,28,29,29,29,30,64,65,65,65,66,72,71,71,71,71,71,64,65,65,65,66,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,0,0,0,0,0,0,16,16,28,29,29,30,28,29,29,29,29,29,29,30,28,29,29,30,64,65,65,65,66,35,35,35,35,35,35,36,34,35,35,36,26,25,25,25,27,47,47,47,47,47,47,48,46,47,47,48,26,25,25,25,27,6,5,5,5,6,5,6,5,5,5,5,26,25,25,25,27,70,70,70,70,70,70,70,70,70,70,70,26,25,25,25,27,72,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,72,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,72,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,72,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,72,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,72,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,72,71,71,71,71,71,64,65,65,65,66,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,0,0,0,0,0,0,16,16,28,29,29,30,28,29,29,29,29,29,29,30,28,29,29,30,34,35,35,36,34,35,35,35,35,35,35,64,65,65,65,66,46,47,47,48,46,47,47,47,47,47,47,26,25,25,25,27,5,5,5,6,5,5,6,6,6,6,6,26,25,25,25,27,70,70,70,70,70,70,70,70,70,70,70,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,64,65,65,65,66,71,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,0,0,0,0,0,0,16,16,28,29,29,29,30,72,71,71,71,71,71,26,25,25,25,27,37,38,38,38,39,72,71,71,71,71,71,26,25,25,25,27,43,44,44,44,45,72,71,71,71,71,71,26,25,25,25,27,5,6,5,5,5,72,71,71,71,71,71,26,25,25,25,27,70,70,70,70,70,71,71,71,71,71,71,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,71,71,71,71,71,71,71,71,71,71,71,28,29,29,29,30,64,65,65,66,64,65,65,65,65,65,65,66,64,65,65,66,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,25,27,72,71,71,71,71,71,28,29,29,29,30,26,25,25,25,27,72,71,71,71,71,71,37,38,38,38,39,26,25,25,25,27,72,71,71,71,71,71,43,44,44,44,45,26,25,25,25,27,72,71,71,71,71,71,6,6,6,5,6,26,25,25,25,27,72,71,71,71,71,71,70,70,70,70,70,26,25,25,25,27,72,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,72,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,72,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,72,71,71,71,71,71,71,71,71,71,71,26,25,25,25,27,72,71,71,71,71,71,71,71,71,71,71,28,29,29,29,30,72,71,71,71,71,71,71,71,71,71,71,64,65,65,66,64,65,65,65,65,65,65,66,64,65,65,66,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,25,27,72,71,71,71,71,71,28,29,29,29,30,28,29,29,29,30,72,71,71,71,71,71,31,32,32,32,33,37,38,38,38,39,72,71,71,71,71,71,31,32,32,32,33,43,44,44,44,45,75,75,75,75,75,75,49,50,50,50,51,6,5,5,5,5,6,2,2,2,2,2,4,6,6,6,5,2,1,2,3,1,2,2,2,2,2,2,1,3,1,2,2,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,2,2,1,2,3,2,2,2,2,2,2,3,1,3,1,2,3,3,2,1,1,2,2,2,2,2,2,2,2,1,3,2,64,65,65,65,66,75,75,75,75,75,75,64,65,65,65,66,26,25,25,25,27,72,72,72,72,72,72,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,26,25,25,25,27,72,71,71,71,71,71,26,25,25,25,27,0,0,0,0,0,0,16,16,28,29,29,30,5,3,17,18,13,8,2,3,28,29,29,30,37,38,38,39,5,1,17,18,13,8,3,2,34,35,35,36,43,44,44,73,5,2,17,18,13,8,1,3,73,47,74,48,6,5,6,73,6,3,17,18,13,8,2,2,73,5,5,5,70,70,70,73,1,3,17,18,13,8,1,1,73,70,70,70,71,71,71,73,2,2,17,18,13,8,2,2,73,72,71,71,71,71,71,73,2,1,17,18,13,8,3,3,73,72,71,71,71,71,71,73,1,2,17,18,13,8,1,2,73,72,71,71,71,71,71,73,3,1,17,18,13,8,3,1,73,72,71,71,71,71,71,73,2,1,17,18,13,8,3,1,73,72,71,71,71,71,71,73,3,1,17,18,13,8,2,2,73,72,71,71,64,65,65,66,7,3,17,18,13,8,3,1,64,65,65,66,26,25,25,27,6,1,17,18,13,8,1,3,26,25,25,27,26,25,25,27,6,2,17,18,13,8,2,1,26,25,25,27,26,25,25,27,6,1,17,18,13,8,3,3,26,25,25,27,26,25,25,27,6,1,17,18,13,8,3,2,26,25,25,27,0,0,0,0,0,0,16,16,28,29,29,30,28,29,29,29,29,29,29,30,28,29,29,30,37,38,38,39,50,50,50,77,50,50,50,50,37,38,38,39,37,38,38,39,5,6,6,5,5,5,5,5,37,38,38,39,43,44,44,45,6,14,15,15,15,15,16,2,43,44,44,45,5,5,5,5,5,17,18,18,18,18,8,2,4,6,5,6,1,1,1,1,2,9,10,18,18,10,11,2,3,2,1,3,15,15,15,15,15,15,15,12,12,15,15,15,15,15,15,15,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,1,3,2,3,3,3,2,2,3,1,3,2,3,3,2,1,3,3,2,3,1,1,3,2,3,3,2,2,1,3,2,1,64,65,65,66,64,65,65,65,65,65,65,66,64,65,65,66,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,0,0,0,0,0,0,16,16,26,25,25,27,26,25,25,25,25,25,25,27,28,29,29,30,28,29,29,30,28,29,29,29,29,29,29,30,37,38,38,39,34,35,35,36,31,32,32,33,37,38,38,38,38,38,38,39,46,47,74,48,49,50,50,51,43,44,44,44,44,44,44,45,5,5,5,5,5,6,5,5,5,6,5,5,6,5,5,5,1,1,1,1,1,1,3,2,1,2,1,2,3,2,1,3,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,2,3,2,3,3,1,2,2,2,1,1,2,3,3,2,3,3,2,3,3,1,2,2,3,3,1,3,1,2,1,2,1,58,59,59,60,58,59,60,7,3,40,41,42,40,41,41,42,61,62,62,63,61,62,63,5,1,52,53,54,52,53,53,54,61,62,62,63,61,62,63,5,2,52,53,54,52,53,53,54,61,62,62,63,61,62,63,6,1,52,53,54,52,53,53,54,0,0,0,0,0,0,16,16,67,68,68,69,61,62,63,5,1,52,53,54,55,56,56,57,34,35,35,36,67,68,69,5,2,55,56,57,37,38,38,39,34,35,35,35,35,35,36,6,2,37,38,38,38,38,38,39,46,47,47,47,47,47,48,5,2,43,44,44,44,44,44,45,6,5,5,6,5,5,5,5,3,4,5,5,5,5,5,5,1,1,1,1,1,1,3,2,1,2,1,2,3,2,1,3,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,2,3,2,3,3,1,2,2,3,2,1,2,3,3,2,2,1,3,1,1,1,3,1,1,2,2,3,1,2,2,2,3,64,65,65,66,64,65,65,65,65,65,65,66,64,65,65,66,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,0,0,0,0,0,0,16,16,5,2,17,8,3,3,55,56,56,56,56,56,56,56,56,57,5,2,17,8,1,2,37,38,38,38,38,38,38,38,38,39,6,2,17,8,2,2,37,38,38,44,78,44,44,38,38,39,5,2,17,8,2,3,43,44,45,6,5,5,6,43,44,45,6,2,17,8,3,1,4,5,5,5,2,2,2,4,5,5,1,1,17,8,1,1,3,2,1,2,1,2,3,2,1,3,15,15,18,18,15,15,15,15,15,15,15,15,15,15,15,15,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,1,3,2,3,3,1,2,2,2,3,1,2,3,3,2,2,1,2,2,1,3,3,3,3,3,3,2,3,2,3,2,1,64,65,65,66,64,65,65,65,65,65,65,66,64,65,65,66,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,26,25,25,27,26,25,25,25,25,25,25,27,26,25,25,27,0,0,0,0,0,0,16,16,28,29,29,30,26,25,25,25,25,25,25,27,28,29,29,30,34,35,35,36,28,29,29,29,29,29,29,30,34,35,35,36,46,47,47,47,74,47,47,47,48,46,47,47,47,47,47,48,6,5,6,6,5,5,5,5,6,5,6,5,5,6,5,5,6,14,15,15,15,15,15,15,15,16,1,3,3,3,1,3,6,17,12,18,12,18,12,18,12,8,2,3,1,3,3,3,5,17,12,18,12,18,12,18,12,8,1,3,1,3,2,2,5,17,12,18,12,18,12,18,12,8,1,1,2,3,3,2,5,17,12,18,12,18,12,18,12,8,2,2,1,2,1,1,5,17,12,18,12,18,12,18,12,8,2,2,3,1,3,1,6,9,18,18,10,10,10,10,10,11,1,3,2,2,3,1,6,2,17,8,3,2,1,2,2,3,2,3,3,3,1,1,6,2,17,8,2,2,40,41,41,41,41,41,41,41,41,42,6,2,17,8,3,3,52,53,53,53,53,53,53,53,53,54,5,2,17,8,2,3,52,53,53,53,53,53,53,53,53,54,5,2,17,8,1,1,52,53,53,53,53,53,53,53,53,54,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,82,82,82,82,82,82,82,82,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,82,82,82,82,82,82,82,82,91,91,91,91,91,91,91,82,91,91,91,91,91,91,91,82,91,91,91,91,91,91,91,82,82,82,82,82,82,82,82,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,82,82,82,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,82,82,82,82,82,82,82,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,91,91,91,82,82,82,82,82,82,82,82,82,9,9,9,82,9,9,9,82,91,9,9,82,9,9,9,82,91,91,9,82,9,9,9,82,82,82,82,82,82,82,82,82,91,91,91,91,9,9,9,82,91,91,91,91,91,9,9,82,91,91,91,91,91,91,9,82,82,82,82,82,82,82,82,82,9,9,9,82,9,9,9,82,9,9,9,82,9,9,9,82,9,9,9,82,9,9,9,82,9,9,9,82,82,82,82,82,9,9,9,82,9,9,9,82,9,9,9,82,9,9,9,82,9,9,9,82,9,9,9,82,82,82,82,82,82,82,82,82,9,9,9,82,9,9,9,82,9,9,9,82,9,9,9,82,9,9,9,82,9,9,9,82,82,82,82,82,82,82,82,82,9,9,9,9,9,9,9,82,9,9,9,9,9,9,9,82,9,9,9,9,9,9,9,82,82,82,82,82,82,82,82,82,9,91,91,82,91,91,91,82,9,9,91,82,91,91,91,82,9,9,9,82,91,91,91,82,9,9,9,82,91,91,91,82,9,9,9,82,9,91,91,82,9,9,9,82,9,9,91,82,9,9,9,82,9,9,9,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,82,164,164,164,164,164,164,82,82,82,82,82,82,82,82,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,82,82,82,82,82,82,82,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,82,82,82,82,82,82,82,82,82,82,164,255,255,255,255,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,255,164,164,164,164,164,164,164,255,164,164,164,164,164,164,164,255,164,164,164,164,164,164,164,255,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,82,82,82,82,82,82,82,82,82,82,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,82,82,82,82,82,82,82,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,82,82,82,82,82,82,82,82,164,164,164,164,164,164,82,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,82,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,82,82,82,82,82,82,82,82,173,173,173,173,173,173,173,173,63,63,1,1,63,63,1,1,1,63,63,1,1,63,63,1,173,173,173,173,173,173,173,173,173,173,173,173,173,173,173,173,82,82,82,82,82,82,82,82,82,82,82,82,82,82,82,82,82,173,173,63,1,173,173,82,82,173,173,63,63,173,173,82,82,173,173,1,63,173,173,82,82,173,173,1,1,173,173,82,82,173,173,63,1,173,173,82,82,173,173,63,63,173,173,82,82,173,173,1,63,173,173,82,82,173,173,1,1,173,173,82,7,7,7,7,7,7,7,7,7,31,31,31,31,31,31,7,7,31,63,63,63,63,31,7,7,31,63,127,127,63,31,7,7,31,63,127,127,63,31,7,7,31,63,63,63,63,31,7,7,31,31,31,31,31,31,7,7,7,7,7,7,7,7,7,31,31,31,31,31,31,31,31,31,63,63,63,63,63,63,31,31,63,127,127,127,127,63,31,31,63,127,7,7,127,63,31,31,63,127,7,7,127,63,31,31,63,127,127,127,127,63,31,31,63,63,63,63,63,63,31,31,31,31,31,31,31,31,31,63,63,63,63,63,63,63,63,63,127,127,127,127,127,127,63,63,127,7,7,7,7,127,63,63,127,7,31,31,7,127,63,63,127,7,31,31,7,127,63,63,127,7,7,7,7,127,63,63,127,127,127,127,127,127,63,63,63,63,63,63,63,63,63,127,127,127,127,127,127,127,127,127,7,7,7,7,7,7,127,127,7,31,31,31,31,7,127,127,7,31,63,63,31,7,127,127,7,31,63,63,31,7,127,127,7,31,31,31,31,7,127,127,7,7,7,7,7,7,127,127,127,127,127,127,127,127,127,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,82,82,82,82,82,9,172,172,172,172,172,172,172,9,9,9,9,9,9,9,9,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,82,82,82,82,82,82,82,82,172,172,172,172,172,172,172,172,9,9,9,9,9,9,9,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,82,82,82,82,82,82,172,9,172,172,172,172,172,172,172,9,9,9,9,9,9,9,9,9,9,81,81,81,81,81,81,81,9,162,162,162,162,162,162,81,9,162,245,235,235,235,162,81,9,162,235,245,235,235,162,81,9,162,235,235,245,235,162,81,9,162,235,235,235,245,162,81,9,162,162,162,162,162,162,81,9,81,81,81,81,81,81,81,81,81,81,81,81,81,81,81,81,162,162,162,162,162,162,81,81,162,245,235,235,235,162,81,81,162,235,245,235,235,162,81,81,162,235,235,245,235,162,81,81,162,235,235,235,245,162,81,81,162,162,162,162,162,162,81,81,81,81,81,81,81,81,81,81,81,81,81,81,81,81,9,81,162,162,162,162,162,162,9,81,162,245,235,235,235,162,9,81,162,235,245,235,235,162,9,81,162,235,235,245,235,162,9,81,162,235,235,235,245,162,9,81,162,162,162,162,162,162,9,81,81,81,81,81,81,81,9,9,102,102,102,92,102,102,102,9,102,102,102,92,102,102,102,9,92,92,92,92,92,92,92,9,102,92,102,102,102,92,102,9,102,92,102,102,102,92,102,9,92,92,92,92,92,92,92,9,102,102,102,92,102,102,102,9,102,102,102,92,102,102,102,92,102,102,102,92,102,102,102,92,102,102,102,92,102,102,102,92,92,92,92,92,92,92,92,102,102,92,102,102,102,92,102,102,102,92,102,102,102,92,102,92,92,92,92,92,92,92,92,92,102,102,102,92,102,102,102,92,102,102,102,92,102,102,102,92,102,102,102,92,102,102,9,92,102,102,102,92,102,102,9,92,92,92,92,92,92,92,9,102,102,92,102,102,102,92,9,102,102,92,102,102,102,92,9,92,92,92,92,92,92,92,9,92,102,102,102,92,102,102,9,92,102,102,102,92,102,102,9,9,164,164,164,164,164,164,164,9,164,235,235,235,235,235,164,9,164,162,162,162,162,162,164,9,164,164,164,164,164,164,164,9,164,164,164,164,164,164,164,9,164,235,235,235,235,235,164,9,164,162,162,162,162,162,164,9,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,235,235,235,235,235,235,164,164,162,162,162,162,162,162,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,235,235,235,235,235,235,164,164,162,162,162,162,162,162,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,9,164,235,235,235,235,235,164,9,164,162,162,162,162,162,164,9,164,164,164,164,164,164,164,9,164,164,164,164,164,164,164,9,164,235,235,235,235,235,164,9,164,162,162,162,162,162,164,9,164,164,164,164,164,164,164,9,9,9,9,9,9,9,9,9,9,162,162,162,162,162,162,162,9,162,82,82,82,82,82,82,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,9,9,9,9,9,9,9,162,162,162,162,162,162,162,162,82,82,82,82,82,82,82,82,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,9,9,9,9,9,9,9,9,162,162,162,162,162,162,162,9,82,82,82,82,82,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,9,164,164,164,164,164,164,164,9,164,235,235,235,235,235,164,9,164,162,162,162,162,162,164,9,164,164,164,164,164,164,164,9,164,164,164,164,164,164,164,9,164,235,235,235,235,235,164,9,164,162,162,162,162,162,164,9,9,9,9,9,9,9,9,164,164,164,164,164,164,164,164,164,235,235,235,235,235,235,164,164,162,162,162,162,162,162,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,235,235,235,235,235,235,164,164,162,162,162,162,162,162,164,9,9,9,9,9,9,9,9,164,164,164,164,164,164,164,9,164,235,235,235,235,235,164,9,164,162,162,162,162,162,164,9,164,164,164,164,164,164,164,9,164,164,164,164,164,164,164,9,164,235,235,235,235,235,164,9,164,162,162,162,162,162,164,9,9,9,9,9,9,9,9,9,9,92,92,92,92,92,92,92,9,102,92,102,102,102,92,102,9,102,92,102,102,102,92,102,9,92,92,92,92,92,92,92,9,102,102,102,92,102,102,102,9,102,102,102,92,102,102,102,9,92,92,92,92,92,92,92,9,9,9,9,9,9,9,9,92,92,92,92,92,92,92,92,102,102,92,102,102,102,92,102,102,102,92,102,102,102,92,102,92,92,92,92,92,92,92,92,92,102,102,102,92,102,102,102,92,102,102,102,92,102,102,102,92,92,92,92,92,92,92,92,9,9,9,9,9,9,9,9,92,92,92,92,92,92,92,9,102,102,92,102,102,102,102,9,102,102,92,102,102,102,102,9,92,92,92,92,92,92,92,9,92,102,102,102,92,102,102,9,92,102,102,102,92,102,102,9,92,92,92,92,92,92,92,9,9,9,9,9,9,9,9,9,9,81,81,81,81,81,81,81,9,162,162,162,162,162,162,81,9,162,245,235,235,235,162,81,9,162,235,245,235,235,162,81,9,162,235,235,245,235,162,81,9,162,235,235,235,245,162,81,9,162,162,162,162,162,162,81,9,9,9,9,9,9,9,9,81,81,81,81,81,81,81,81,81,162,162,162,162,162,162,81,81,162,245,235,235,235,162,81,81,162,235,245,235,235,162,81,81,162,235,235,245,235,162,81,81,162,235,235,235,245,162,81,81,162,162,162,162,162,162,81,9,9,9,9,9,9,9,9,81,81,81,81,81,81,81,9,81,162,162,162,162,162,162,9,81,162,245,235,235,235,162,9,81,162,235,245,235,235,162,9,81,162,235,235,245,235,162,9,81,162,235,235,235,245,162,9,81,162,162,162,162,162,162,9,9,9,9,9,9,9,9,9,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,162,162,162,162,162,9,162,82,82,82,82,82,82,9,162,162,162,162,162,162,162,9,9,9,9,9,9,9,9,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,162,82,82,82,82,82,82,82,82,162,162,162,162,162,162,162,162], "i8", ALLOC_NONE, Runtime.GLOBAL_BASE+10736);
/* memory initializer */ allocate([9,9,9,9,9,9,9,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,162,162,162,162,162,82,162,9,82,82,82,82,82,82,162,9,162,162,162,162,162,162,162,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,171,171,171,171,171,171,171,9,171,82,82,82,82,82,82,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,9,9,9,9,9,9,9,171,171,171,171,171,171,171,171,82,82,82,82,82,82,82,82,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,9,9,9,9,9,9,9,9,171,171,171,171,171,171,171,9,82,82,82,82,82,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,9,9,9,9,9,9,9,9,9,172,172,172,172,172,172,172,9,172,82,82,82,82,82,82,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,172,82,172,172,172,172,172,9,9,9,9,9,9,9,9,172,172,172,172,172,172,172,172,82,82,82,82,82,82,82,82,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,9,9,9,9,9,9,9,9,172,172,172,172,172,172,172,9,82,82,82,82,82,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,172,172,172,172,172,82,172,9,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,171,171,171,171,171,9,171,82,82,82,82,82,82,9,171,171,171,171,171,171,171,9,9,9,9,9,9,9,9,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,171,82,82,82,82,82,82,82,82,171,171,171,171,171,171,171,171,9,9,9,9,9,9,9,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,171,171,171,171,171,82,171,9,82,82,82,82,82,82,171,9,171,171,171,171,171,171,171,9,9,9,9,9,9,9,9,9,82,82,82,82,82,82,82,82,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,29,30,29,29,29,29,30,29,29,30,29,30,29,29,30,30,219,219,219,219,219,219,219,219,218,218,218,218,218,218,218,218,218,243,243,243,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,243,243,243,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,145,145,145,145,145,145,145,145,145,89,89,89,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,89,89,89,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,145,82,173,82,173,82,82,173,82,82,173,82,82,173,82,173,82,82,173,82,173,82,82,173,82,82,173,82,82,173,82,173,82,82,173,82,173,82,82,173,82,82,173,82,82,173,82,173,82,82,173,82,173,82,82,173,82,82,173,82,82,173,82,173,82,92,92,92,92,92,92,92,92,102,82,82,82,82,82,82,102,102,82,9,9,9,9,82,102,92,82,9,9,9,9,82,92,92,82,9,9,9,9,82,102,92,82,1,1,1,1,82,102,92,82,1,1,1,1,82,92,9,82,1,1,1,1,82,9,82,82,82,82,82,82,82,82,173,173,173,173,173,173,173,173,82,82,82,82,82,82,82,82,82,173,173,173,82,173,173,173,173,82,173,82,173,82,173,82,173,173,82,173,173,173,82,173,82,82,82,82,82,82,82,82,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,81,81,81,81,81,81,81,81,81,162,162,162,162,162,162,81,81,162,9,9,9,9,162,81,81,162,9,9,9,9,162,81,81,162,9,9,9,9,162,81,81,162,1,1,1,1,162,81,81,162,1,1,1,1,162,81,9,9,1,1,1,1,9,9,164,164,164,164,164,164,164,164,164,82,82,82,82,82,82,164,164,82,9,9,9,9,82,164,164,82,9,9,9,9,82,164,164,82,9,9,9,9,82,164,164,82,1,1,1,1,82,164,164,82,1,1,1,1,82,164,9,82,1,1,1,1,82,9,16,16,3,4,0,0,0,0,0,0,0,12,3,2,4,0,0,0,1,5,22,20,2,8,2,4,0,12,1,24,11,4,0,0,11,20,21,8,2,6,0,1,0,12,5,23,6,11,8,4,1,24,0,1,3,4,0,1,24,16,13,15,0,1,5,6,11,23,4,11,9,5,2,7,23,8,2,18,8,10,4,0,1,0,5,9,1,0,0,1,0,1,0,12,1,0,1,0,5,8,2,6,11,2,20,6,24,1,0,12,1,0,1,0,0,5,2,8,6,0,0,3,23,6,0,12,1,3,9,0,0,0,0,1,0,14,13,19,13,13,13,17,5,9,5,4,0,0,3,7,20,18,2,7,2,4,0,0,24,1,0,1,0,24,11,9,0,12,0,5,4,5,22,2,23,6,0,1,3,23,7,9,0,12,0,0,11,2,21,4,0,3,8,6,1,0,11,10,4,12,0,0,1,0,0,5,8,6,1,0,5,20,9,24,1,16,13,13,19,15,0,0,1,24,1,0,0,0,5,23,10,2,20,2,10,18,2,2,10,23,6,0,0,0,0,0,0,0,0,0,0,12,0,0,0,0,0,0,0,0,0,0,0,0,28,1,94,90,90,90,90,90,90,90,90,95,25,25,25,25,25,25,25,25,25,25,25,25,25,91,91,91,91,25,0,0,16,6,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,0,0,0,0,0,0,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,164,164,164,164,164,172,172,172,164,164,164,164,164,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,164,164,164,164,164,172,172,172,164,164,164,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,164,164,164,172,172,172,164,164,164,164,164,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,164,164,164,164,164,172,172,172,164,164,164,164,164,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,164,164,164,164,164,172,172,172,164,164,164,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,164,164,164,172,172,172,164,164,164,164,164,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,218,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,218,218,218,218,218,218,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,218,218,218,218,172,172,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,218,218,218,164,164,218,218,218,218,218,218,164,164,218,218,218,218,218,218,164,164,218,218,218,218,218,218,164,164,218,218,218,172,172,172,164,164,172,172,172,172,172,172,164,164,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,164,164,164,164,172,172,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,90,172,172,172,172,172,172,172,90,172,172,172,172,172,172,172,90,172,172,172,172,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,172,172,172,90,172,172,172,172,172,172,172,90,172,172,172,172,172,172,172,90,172,172,172,172,172,172,90,172,172,172,172,172,172,172,90,172,172,172,172,172,172,172,90,172,172,172,172,172,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,164,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,172,90,90,90,90,90,172,172,172,90,90,90,90,90,172,172,172,90,90,90,90,90,172,172,172,90,90,90,90,90,172,172,172,90,90,90,90,90,172,172,172,172,90,172,172,172,172,172,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,0,0,0,0,0,255,255,255,0,0,0,0,0,255,255,255,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,255,0,0,0,0,0,255,0,255,0,0,0,0,0,255,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,255,0,0,255,255,255,255,255,255,255,0,0,255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,255,255,255,255,255,255,255,0,0,255,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,0,0,255,255,255,255,255,255,0,255,0,0,255,0,0,0,0,0,255,255,255,255,255,0,0,0,0,0,255,0,0,255,0,255,255,255,255,255,255,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,255,0,0,255,0,0,0,0,255,255,0,255,0,0,0,0,255,255,255,0,0,0,0,255,0,0,255,255,0,255,0,255,0,0,0,255,255,0,0,0,255,255,255,255,0,255,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,0,0,255,0,255,0,255,0,0,0,0,255,255,255,0,0,0,0,255,0,255,0,255,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,255,255,255,255,255,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,0,0,0,0,255,0,0,255,255,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,0,255,255,0,0,255,0,0,0,0,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,255,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,0,0,255,255,0,0,0,255,255,0,0,0,0,0,255,255,255,0,0,0,255,255,255,255,0,0,0,255,255,255,255,0,0,0,255,255,255,0,0,0,0,0,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,0,0,0,0,0,255,255,0,0,0,0,0,255,255,0,0,0,0,0,255,255,255,255,0,0,0,0,0,0,0,255,255,0,255,255,0,0,0,255,255,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,0,0,0,0,255,255,255,255,0,0,0,255,255,0,255,255,0,0,255,255,0,0,255,255,0,0,255,255,255,255,255,255,255,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,0,0,255,255,0,0,0,0,0,0,255,255,255,255,255,255,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,255,255,0,0,0,255,255,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,0,0,0,255,255,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,255,255,255,255,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,0,255,255,0,0,0,255,255,0,0,0,0,0,255,255,0,0,0,0,0,255,255,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,0,0,0,255,255,0,0,0,255,0,0,255,255,255,0,0,255,0,0,0,255,255,255,255,0,0,0,255,0,0,255,255,255,255,0,255,0,0,0,0,255,255,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,0,255,255,255,255,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,255,255,0,0,0,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,255,255,0,0,0,0,0,255,255,255,0,0,0,0,255,255,255,255,0,0,0,0,0,255,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,255,0,0,0,0,0,255,255,255,255,0,0,0,0,255,255,255,0,0,0,0,0,255,255,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,0,0,255,255,255,255,255,255,255,0,255,255,0,0,0,255,255,0,0,0,0,0,255,255,0,0,0,0,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,0,0,0,0,255,0,0,0,255,0,0,255,0,0,255,0,255,0,0,255,0,255,0,255,255,0,0,255,0,0,255,255,0,0,0,0,255,0,0,0,0,0,0,0,0,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,0,0,0,0,255,255,0,255,255,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,255,255,255,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,255,255,255,255,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,0,0,0,255,255,0,0,255,255,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,255,255,0,0,255,255,0,0,0,255,255,255,255,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,0,0,0,255,255,0,0,255,255,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,255,255,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,255,255,255,255,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,255,255,255,255,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,0,0,255,255,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,255,255,255,0,255,255,0,0,0,255,255,0,0,255,255,0,0,255,255,0,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,255,255,255,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,255,0,0,0,255,255,0,0,0,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,255,255,0,255,255,0,0,255,255,0,0,255,255,0,255,255,0,0,0,255,255,255,255,0,0,0,0,255,255,255,255,255,0,0,0,255,255,0,255,255,255,0,0,255,255,0,0,255,255,255,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,255,255,0,0,0,255,255,0,255,255,255,0,255,255,255,0,255,255,255,255,255,255,255,0,255,255,255,255,255,255,255,0,255,255,0,255,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,0,0,0,0,0,0,0,0,255,255,0,0,0,255,255,0,255,255,255,0,0,255,255,0,255,255,255,255,0,255,255,0,255,255,255,255,255,255,255,0,255,255,0,255,255,255,255,0,255,255,0,0,255,255,255,0,255,255,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,255,255,255,255,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,255,255,255,255,0,255,255,0,0,255,255,0,0,0,255,255,255,255,0,255,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,255,255,255,0,255,255,255,255,255,0,0,0,255,255,0,255,255,255,0,0,255,255,0,0,255,255,255,0,0,0,0,0,0,0,0,0,0,255,255,255,255,0,0,0,255,255,0,0,255,255,0,0,255,255,0,0,0,0,0,0,0,255,255,255,255,255,0,0,0,0,0,0,0,255,255,0,255,255,0,0,0,255,255,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,0,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,255,0,255,255,255,0,0,255,255,255,255,255,0,0,0,0,255,255,255,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,255,255,0,255,255,0,0,0,255,255,0,255,255,0,255,0,255,255,0,255,255,255,255,255,255,255,0,255,255,255,255,255,255,255,0,255,255,255,0,255,255,255,0,255,255,0,0,0,255,255,0,0,0,0,0,0,0,0,0,255,255,0,0,0,255,255,0,255,255,255,0,255,255,255,0,0,255,255,255,255,255,0,0,0,0,255,255,255,0,0,0,0,255,255,255,255,255,0,0,255,255,255,0,255,255,255,0,255,255,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,255,255,0,0,255,255,0,0,255,255,0,0,255,255,0,0,255,255,0,0,255,255,0,0,0,255,255,255,255,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,0,0,0,0,0,255,255,255,0,0,0,0,255,255,255,0,0,0,0,255,255,255,0,0,0,0,255,255,255,0,0,0,0,255,255,255,0,0,0,0,0,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,0,0,0,255,0,0,0,0,255,0,255,0,0,255,255,0,0,255,255,0,255,0,0,0,0,255,255,0,255,0,0,0,0,255,255,0,0,255,255,0,0,255,0,255,0,0,0,0,255,0,0,0,255,255,255,255,0,0,0,0,255,255,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,0,0,255,255,0,0,0,0,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,255,0,255,0,0,0,0,255,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,255,15,15,15,15,15,15,15,15,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,5,5,5,5,5,5,5,5,255,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,9,0,0,0,0,0,0,0,9,0,0,0,0,9,9,9,9,9,9,9,0,0,0,9,9,9,0,0,0,0,0,9,9,9,0,0,0,0,9,9,0,9,9,0,0,0,9,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,54,0,0,0,0,0,0,0,54,0,0,0,0,54,54,54,54,54,54,54,0,0,0,54,54,54,0,0,0,0,0,54,54,54,0,0,0,0,54,54,0,54,54,0,0,0,54,0,0,0,54,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,255,57,57,57,57,57,57,57,57,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,24,24,24,24,24,24,24,24,255,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,23,0,0,0,0,0,0,0,251,252,0,0,254,252,1,0,250,251,2,0,254,253,3,0,250,250,4,0,254,254,5,0,251,250,6,0,253,254,7,0,252,251,8,0,252,254,9,0,253,250,6,1,251,254,7,1,254,250,4,1,250,254,5,1,254,251,2,1,250,253,3,1,254,251,0,3,251,251,1,3,254,253,2,3,250,251,3,3,254,254,4,3,250,250,5,3,253,254,6,3,251,250,7,3,252,253,8,3,252,250,9,3,251,254,6,2,253,250,7,2,250,253,4,2,254,249,5,2,250,252,2,2,254,250,3,2,2,0,0,0,0,0,0,0,6,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,132,148,164,180,196,197,198,199,200,216,217,218,202,186,170,154,153,137,121,105,89,73,57,56,55,71,70,86,102,118,117,116,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,83,84,79,82,89,0,0,0,70,82,69,69,32,82,79,65,77,0,0,0,0,0,0,0,65,82,67,65,68,69,0,0,49,32,80,76,65,89,69,82,0,0,0,0,0,0,0,0,50,32,80,76,65,89,69,82,0,0,0,0,0,0,0,0,83,85,82,86,73,86,65,76,0,0,0,0,0,0,0,0,80,85,82,83,85,73,84,0,67,79,78,84,73,78,85,69,0,0,0,0,0,0,0,0,82,69,83,84,65,82,84,0,69,88,73,84,0,0,0,0,66,65,78,75,32,74,79,66,0,0,0,0,0,0,0,0,67,79,76,76,69,67,84,79,82,0,0,0,0,0,0,0,83,84,82,69,69,84,32,82,65,67,69,0,0,0,0,0,56,115,0,0,1,0,0,0,120,142,0,0,72,115,0,0,1,0,0,0,0,143,0,0,88,115,0,0,1,0,0,0,184,143,0,0,255,255,0,0,0,115,0,0,1,0,0,0,0,142,0,0,16,115,0,0,1,0,0,0,32,142,0,0,255,255,0,0,0,0,0,0,16,115,0,0,1,0,0,0,80,142,0,0,255,255,0,0,224,114,0,0,2,0,0,0,144,115,0,0,240,114,0,0,2,0,0,0,176,115,0,0,255,255,0,0,0,0,0,0,192,114,0,0,2,0,0,0,104,115,0,0,200,114,0,0,1,0,0,0,232,141,0,0,216,114,0,0,2,0,0,0,192,115,0,0,255,255,0,0,24,115,0,0,3,0,0,0,0,0,0,0,40,115,0,0,4,0,0,0,0,0,0,0,48,115,0,0,5,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,74,79,89,82,73,68,69,82,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,6,0,0,251,0,255,0,0,244,5,3,12,5,4,12,5,3,12,5,4,12,5,5,0,0,255,0,0,0,0,0,255,0,0,0,0,0,0,0,2,9,0,0,241,4,0,249,0,255,0,0,0,0,0,0,1,10,0,0,226,2,1,5,1,1,3,1,1,1,2,5,0,0,255,0,0,0,0,0,0,231,2,3,12,4,5,0,0,255,0,0,0,0,0,0,0,241,0,8,90,2,4,3,2,4,4,2,4,5,2,4,6,2,5,0,0,255,0,0,1,1,0,7,144,0,0,226,8,5,0,0,255,0,0,0,2,8,1,7,200,1,6,0,1,0,231,0,255,0,0,0,1,0,0,7,160,0,0,236,1,0,254,0,1,1,0,255,0,0,0,0,0,0,0,0,1,3,0,0,226,3,1,6,4,1,9,3,5,0,0,255,0,0,0,0,0,0,0,0,2,1,1,7,200,1,6,0,1,0,236,0,255,0,0,0,2,4,0,0,241,4,0,249,0,255,0,0,0,0,0,0,2,1,0,0,241,0,255,0,0,237,0,1,6,2,1,1,0,255,0,0,0,0,0,0,0,236,0,1,2,1,7,0,2,7,255,0,1,3,0,255,0,0,0,0,0,0,0,0,2,8,0,7,100,1,0,20,5,0,247,0,255,0,0,0,2,9,0,0,241,0,255,0,2,4,0,0,248,0,8,80,3,8,78,3,8,84,2,8,76,2,8,88,2,8,74,1,8,92,0,0,252,3,8,96,3,8,90,3,8,96,3,8,90,3,8,96,3,8,90,1,5,0,0,255,0,0,0,0,2,4,0,0,251,0,8,62,1,3,2,1,3,2,1,3,2,1,3,2,1,3,2,1,4,10,1,3,2,1,3,2,1,3,2,1,3,2,1,3,2,1,4,2,1,3,2,1,3,2,1,3,2,1,3,2,1,3,2,1,4,10,1,3,2,1,3,2,1,3,2,1,3,2,1,3,2,1,4,5,1,3,2,1,3,2,1,3,2,1,3,2,1,3,2,1,4,5,1,3,2,1,3,2,1,3,2,1,3,2,1,3,2,1,4,5,1,3,2,1,3,2,1,3,2,1,3,2,1,3,2,1,5,0,0,255,0,0,0,0,0,0,0,0,2,3,0,8,86,2,4,10,2,4,8,2,4,6,2,4,4,2,4,2,0,8,66,2,3,5,2,4,2,2,3,5,2,3,5,2,4,2,2,3,5,0,0,248,4,3,5,4,4,5,4,3,5,4,4,5,4,3,5,4,4,5,4,3,5,1,5,0,0,255,0,0,2,4,0,0,251,0,8,52,1,3,2,1,3,4,1,3,6,1,4,10,1,3,2,1,3,4,1,3,6,1,4,10,1,3,2,1,3,4,1,3,6,1,4,5,1,3,2,1,3,4,1,3,6,2,3,2,2,3,4,2,3,6,3,4,5,3,3,2,3,3,4,3,3,6,4,4,5,4,3,2,4,3,4,4,3,6,1,5,0,0,255,0,0,0,0,0,7,0,4,2,4,0,7,255,0,9,144,0,10,60,0,0,251,1,8,80,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,5,0,0,255,0,0,0,0,0,0,0,0,7,0,4,2,4,0,7,255,0,9,144,0,10,60,0,0,251,1,8,83,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,5,0,0,255,0,0,0,0,0,0,0,0,7,0,4,2,4,0,7,255,0,9,144,0,10,60,0,0,251,1,8,86,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,5,0,0,255,0,0,0,0,0,0,0,0,7,0,4,2,4,0,7,255,0,9,144,0,10,60,0,0,251,1,8,89,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,4,5,3,3,5,3,5,0,0,255,0,0,0,0,0,0,0,0,2,2,1,6,0,1,0,224,0,255,0,0,0,0,0,0,2,1,1,6,0,1,0,224,0,255,0,0,0,0,0,0,2,8,1,6,0,1,0,224,0,255,0,0,0,0,0,0,2,5,1,6,0,1,0,224,0,255,0,0,0,0,0,0,2,9,1,6,0,1,0,224,0,255,0,0,0,0,0,0,1,3,0,0,226,3,1,6,4,1,9,3,5,0,0,0,7,255,0,1,9,0,0,246,2,7,255,0,0,241,0,1,6,2,1,1,0,255,0,0,2,1,0,8,90,0,0,224,0,4,26,1,3,3,1,3,3,1,3,3,1,3,4,1,3,4,1,4,16,1,5,0,0,255,0,0,0,0,0,0,2,1,0,8,90,0,0,224,0,4,29,1,3,3,1,3,3,1,3,3,1,3,4,1,3,4,1,4,13,1,5,0,0,255,0,0,0,0,0,0,2,4,0,7,112,0,0,216,0,8,60,1,5,0,0,255,0,0,0,0,0,0,0,0,2,9,0,7,208,0,0,236,0,8,60,5,3,12,5,5,0,0,255,0,0,0,0,0,2,8,0,7,200,0,0,246,0,8,90,5,7,100,0,0,246,0,8,82,4,7,200,0,0,246,0,8,90,5,7,150,0,0,246,0,8,82,0,255,0,0,0,0,0,0,0,0,2,2,0,4,16,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,3,1,1,4,1,1,4,1,1,4,1,1,4,1,1,4,1,1,4,1,1,4,1,1,4,1,1,4,1,1,4,1,1,4,1,1,4,1,1,4,1,1,4,1,1,4,1,1,5,0,0,255,0,2,6,3,0,246,0,255,0,2,4,0,9,127,0,10,64,0,8,80,4,255,0,0,0,0,0,0,0,0,0,0,88,116,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,116,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,120,116], "i8", ALLOC_NONE, Runtime.GLOBAL_BASE+20976);
/* memory initializer */ allocate([128,116,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,144,116,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,168,116,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,184,116,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,208,116,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,116,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,240,116,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,8,117,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,117,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,48,117,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,117,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,72,117,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,88,117,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,112,117,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,117,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,136,117,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,192,117,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,118,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,152,118,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,248,118,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,119,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,136,119,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,208,119,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,120,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40,120,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,56,120,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,72,120,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,88,120,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,104,120,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,120,120,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,144,120,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,184,120,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,120,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,248,120,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,121,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,121,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,168,121,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,176,121,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,2,3,4,5,5,6,7,8,9,9,10,11,12,12,13,14,14,15,16,16,17,18,18,19,20,20,21,21,22,23,23,24,24,25,25,26,26,27,27,27,28,28,29,29,29,30,30,30,30,31,31,31,31,31,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,31,31,31,31,31,30,30,30,30,29,29,29,28,28,27,27,27,26,26,25,25,24,24,23,23,22,21,21,20,20,19,18,18,17,16,16,15,14,14,13,12,12,11,10,9,9,8,7,6,5,5,4,3,2,2,1,0,0,255,255,254,253,252,252,251,250,249,248,248,247,246,245,245,244,243,243,242,241,241,240,239,239,238,237,237,236,236,235,234,234,233,233,232,232,231,231,230,230,230,229,229,228,228,228,227,227,227,227,226,226,226,226,226,225,225,225,225,225,225,225,225,225,225,225,225,225,225,225,226,226,226,226,226,227,227,227,227,228,228,228,229,229,230,230,230,231,231,232,232,233,233,234,234,235,236,236,237,237,238,239,239,240,241,241,242,243,243,244,245,245,246,247,248,248,249,250,251,252,252,253,254,255,255,0,0,0,1,0,2,0,2,0,3,0,4,0,5,0,6,0,6,0,7,0,8,0,9,0,10,0,11,0,11,0,12,0,13,0,14,0,15,0,16,0,17,0,18,0,19,0,20,0,21,0,23,0,24,0,25,0,26,0,28,0,29,0,30,0,32,0,34,0,35,0,37,0,39,0,41,0,43,0,45,0,48,0,51,0,53,0,56,0,60,0,64,0,68,0,72,0,77,0,83,0,89,0,97,0,105,0,116,0,128,0,143,0,161,0,184,0,216,0,3,1,69,1,178,1,139,2,24,5,0,0,233,250,118,253,79,254,188,254,254,254,41,255,73,255,96,255,114,255,129,255,141,255,152,255,160,255,168,255,174,255,180,255,185,255,189,255,193,255,197,255,201,255,204,255,206,255,209,255,212,255,214,255,216,255,218,255,220,255,222,255,223,255,225,255,227,255,228,255,229,255,231,255,232,255,233,255,234,255,236,255,237,255,238,255,239,255,240,255,241,255,242,255,243,255,244,255,245,255,246,255,246,255,247,255,248,255,249,255,250,255,251,255,251,255,252,255,253,255,254,255,255,255,255,255,0,0,188,10,0,0,0,0,0,0,254,0,0,0,0,0,254,254,0,255,1,255,255,255,254,254,0,255,1,255,255,255,254,254,0,255,1,255,255,255,254,254,0,255,1,255,255,255,254,254,0,255,1,255,255,255,254,254,254,0,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,254,254,0,0,0,0,254,254,254,254,1,255,255,255,0,254,254,254,1,173,173,173,0,254,254,254,1,255,255,255,0,254,254,254,1,173,173,173,0,254,254,254,1,255,255,255,0,254,254,254,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,0,0,254,254,254,254,254,254,0,255,0,0,254,254,254,0,255,1,254,254,254,254,254,0,255,1,254,254,254,254,0,255,1,255,254,254,254,254,0,0,1,255,254,254,254,254,254,254,0,0,254,254,254,254,254,254,254,254,254,254,254,254,255,255,0,0,254,254,254,254,255,255,255,255,0,0,254,254,255,255,255,1,173,255,0,0,255,255,255,1,255,173,255,0,255,255,1,173,255,255,0,254,0,0,1,255,173,255,0,254,254,254,0,0,255,0,254,254,254,254,254,254,0,0,254,254,254,254,254,0,0,254,254,254,254,254,0,255,255,0,254,254,254,0,255,255,1,255,0,254,0,255,255,1,255,255,255,0,0,255,1,255,254,254,254,254,254,0,255,255,254,254,254,254,254,254,0,255,254,254,254,254,254,254,254,0,254,254,254,254,255,255,255,255,0,254,254,254,255,255,255,255,1,0,254,254,255,255,255,1,255,255,0,254,255,255,1,255,173,255,255,0,0,1,255,173,255,173,255,0,254,0,255,255,173,255,0,254,254,254,0,255,255,0,254,254,254,254,254,0,0,254,254,254,254,254,254,254,0,0,254,254,254,254,0,0,255,0,254,254,0,0,255,255,1,1,0,254,0,255,1,1,255,255,0,254,254,0,254,254,254,254,254,254,254,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,255,255,255,255,255,0,254,254,255,255,255,255,255,0,254,254,0,255,255,255,1,1,0,254,0,255,1,1,173,255,0,254,254,0,173,255,255,173,255,0,254,0,255,173,255,255,0,0,254,254,0,255,0,0,254,254,254,254,0,0,254,254,254,254,254,0,0,0,0,0,254,254,0,255,255,255,255,255,0,254,0,1,1,1,1,1,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,255,255,255,255,255,0,254,0,255,255,255,255,255,0,254,0,255,255,255,255,255,0,254,0,1,1,1,1,1,0,254,0,255,173,255,173,255,0,254,0,255,173,255,173,255,0,254,0,255,173,255,173,255,0,254,254,0,0,0,0,0,254,254,254,0,0,0,0,0,254,254,0,128,0,255,5,255,254,254,0,255,0,255,5,255,254,254,0,255,0,255,255,255,254,254,0,255,0,255,128,255,254,254,0,128,0,255,128,255,254,254,254,0,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,254,254,0,0,0,0,254,254,254,254,0,128,128,128,0,254,254,254,0,255,255,173,0,254,254,254,0,255,255,173,0,254,254,254,0,255,255,173,0,254,254,254,0,128,128,128,0,254,254,254,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,0,0,254,254,254,254,254,254,0,128,0,0,254,254,254,0,255,0,254,254,254,254,254,0,255,0,254,254,254,254,0,128,0,255,254,254,254,254,0,0,0,255,254,254,254,254,254,254,0,0,254,254,254,254,254,254,254,254,254,254,254,254,255,5,0,0,254,254,254,254,255,5,255,255,0,0,254,254,255,255,255,0,255,128,0,0,128,255,255,0,255,255,128,0,128,255,0,255,255,173,0,254,0,0,0,128,255,173,0,254,254,254,0,0,128,0,254,254,254,254,254,254,0,0,254,254,254,254,254,0,0,254,254,254,254,254,0,255,128,0,254,254,254,0,255,255,0,255,0,254,0,255,255,0,255,255,5,0,0,128,0,255,254,254,254,254,254,0,255,255,254,254,254,254,254,254,0,128,254,254,254,254,254,254,254,0,254,254,254,254,255,5,255,255,0,254,254,254,128,255,255,255,0,0,254,254,255,255,255,0,255,128,0,254,255,255,0,255,255,255,128,0,0,0,255,255,255,255,173,0,254,0,128,255,255,173,0,254,254,254,0,128,173,0,254,254,254,254,254,0,0,254,254,254,254,254,254,254,0,0,254,254,254,254,0,0,128,0,254,254,0,0,255,255,0,0,0,254,0,128,0,0,255,255,0,254,254,0,254,254,254,254,254,254,254,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,255,255,255,5,5,0,254,254,128,128,255,255,255,0,254,254,0,255,255,255,0,0,0,254,0,255,0,0,255,128,0,254,254,0,255,255,255,255,128,0,254,0,128,255,173,173,0,0,254,254,0,128,0,0,254,254,254,254,0,0,254,254,254,254,254,0,0,0,0,0,254,254,0,128,255,255,255,128,0,254,0,0,0,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,255,255,255,255,255,0,254,0,128,128,255,5,5,0,254,0,255,255,255,255,255,0,254,0,0,0,0,0,0,0,254,0,128,255,255,255,128,0,254,0,128,255,255,255,128,0,254,0,128,173,173,173,128,0,254,254,0,0,0,0,0,254,254,254,0,0,0,5,0,254,254,0,128,0,5,7,5,254,254,0,255,0,5,7,5,254,254,0,255,0,255,5,255,254,254,0,255,0,255,128,255,254,254,0,128,0,255,128,255,254,254,254,0,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,254,254,0,0,0,0,254,254,254,254,0,128,128,128,0,254,254,254,0,255,255,173,8,254,254,254,0,255,255,173,0,254,254,254,0,255,255,173,0,254,254,254,0,128,128,128,0,254,254,254,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,0,0,254,254,254,254,254,254,0,128,0,5,254,254,254,0,255,0,254,254,254,254,254,0,255,0,254,254,254,254,0,128,0,255,254,254,254,254,0,0,0,255,254,254,254,254,254,254,0,0,254,254,254,254,254,254,254,254,254,254,254,254,5,7,5,0,254,254,254,254,5,7,5,255,0,0,254,254,255,5,255,0,255,128,0,0,128,255,255,0,255,255,128,0,128,255,0,255,255,173,0,254,0,0,0,128,255,173,0,254,254,254,0,0,128,0,254,254,254,254,254,254,0,0,254,254,254,254,254,0,0,254,254,254,254,254,0,255,128,0,254,254,254,0,255,255,0,255,5,254,0,255,255,0,255,5,7,5,0,128,0,255,254,254,254,254,254,0,255,255,254,254,254,254,254,254,0,128,254,254,254,254,254,254,254,0,254,254,254,254,5,7,5,255,0,254,254,254,128,5,255,255,0,0,254,254,255,255,255,0,255,128,0,254,255,255,0,255,255,255,128,0,0,0,255,255,255,255,173,0,254,0,128,255,255,173,0,254,254,254,0,128,173,0,254,254,254,254,254,0,0,254,254,254,254,254,254,254,0,0,254,254,254,254,0,0,128,0,254,254,0,0,255,255,0,0,0,254,0,128,0,0,255,5,5,254,254,0,254,254,254,254,254,254,254,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,255,255,5,7,7,5,254,254,128,128,255,5,5,0,254,254,0,255,255,255,0,0,0,254,0,255,0,0,255,128,0,254,254,0,255,255,255,255,128,0,254,0,128,255,173,173,0,0,254,254,0,128,0,0,254,254,254,254,0,0,254,254,254,254,254,0,0,0,8,0,254,254,0,128,255,255,255,128,0,254,0,0,0,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,255,255,255,5,5,0,254,0,128,128,5,7,7,5,254,0,255,255,255,5,5,0,254,0,0,0,0,0,0,0,254,0,128,255,255,255,128,0,254,0,128,255,255,255,128,0,254,0,128,173,173,173,128,0,254,254,0,0,0,0,0,254,254,254,0,0,0,0,0,254,254,0,128,0,255,5,255,254,254,0,255,0,255,5,255,254,254,0,255,0,255,152,255,254,254,0,255,0,152,224,152,254,254,0,128,0,152,224,152,254,254,254,0,0,0,152,0,254,254,254,254,254,254,254,254,254,254,254,254,254,0,0,0,0,254,254,254,254,0,128,128,128,0,254,254,254,0,255,255,173,0,254,254,254,0,255,255,173,8,254,254,254,0,255,255,173,0,254,254,254,0,128,128,128,0,254,254,254,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,0,0,254,254,254,254,254,254,0,128,0,0,254,254,254,0,255,0,254,254,254,254,254,0,255,0,254,254,254,254,0,128,0,255,254,254,254,254,0,0,0,152,254,254,254,254,254,254,0,152,254,254,254,254,254,254,254,254,254,254,254,254,255,5,0,0,254,254,254,254,255,5,255,255,0,0,254,254,152,255,255,0,255,128,0,0,224,152,255,0,255,255,128,0,224,152,0,255,255,173,0,254,152,0,0,128,255,173,0,254,254,254,0,0,128,0,254,254,254,254,254,254,0,0,254,254,254,254,254,0,0,254,254,254,254,254,0,255,128,0,254,254,254,0,255,255,0,255,0,254,0,255,255,0,255,255,5,0,0,128,0,255,254,254,254,254,254,0,255,152,254,254,254,254,254,254,152,224,254,254,254,254,254,254,254,152,254,254,254,254,152,5,255,255,0,254,254,254,224,152,255,255,0,0,254,254,152,255,255,0,255,128,0,254,255,255,0,255,255,255,128,0,0,0,255,255,255,255,173,0,254,0,128,255,255,173,0,254,254,254,0,128,173,0,254,254,254,254,254,0,0,254,254,254,254,254,254,254,0,0,254,254,254,254,0,0,128,0,254,254,0,0,255,255,0,0,0,254,0,128,0,0,255,255,0,254,254,0,254,254,254,254,254,254,254,152,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,152,152,255,5,5,0,254,254,224,224,152,255,255,0,254,254,152,152,255,255,0,0,0,254,0,255,0,0,255,128,0,254,254,0,255,255,255,255,128,0,254,0,128,255,173,173,0,0,254,254,0,128,0,0,254,254,254,254,0,0,254,254,254,254,254,0,8,0,0,0,254,254,0,128,255,255,255,128,0,254,0,0,0,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,152,152,255,255,255,0,254,152,224,224,152,5,5,0,254,0,152,152,255,255,255,0,254,0,0,0,0,0,0,0,254,0,128,255,255,255,128,0,254,0,128,255,255,255,128,0,254,0,128,173,173,173,128,0,254,254,0,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,255,0,255,0,254,254,254,254,0,0,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,254,254,254,254,0,255,0,255,0,254,254,254,254,0,0,0,254,254,254,254,254,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,254,254,254,254,254,254,0,255,0,255,0,254,254,254,254,0,0,0,254,254,254,254,254,254,254,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,254,254,254,254,254,254,0,255,254,254,254,254,254,254,0,0,254,254,254,254,254,254,0,255,254,254,254,254,254,254,254,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,254,254,254,254,254,0,0,255,254,254,254,254,254,254,0,0,254,254,254,254,254,254,0,255,0,254,254,254,254,254,254,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,254,254,254,254,254,254,0,255,0,254,254,254,254,254,0,0,254,254,254,254,254,0,0,255,254,254,254,254,254,254,254,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,0,254,0,254,254,254,254,0,255,0,254,254,254,254,254,254,0,255,0,0,0,254,254,0,254,254,0,254,254,254,254,254,254,254,0,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,254,254,254,254,254,254,254,0,0,254,254,0,0,0,0,0,255,0,254,0,255,255,255,255,255,255,0,0,0,0,0,0,255,0,254,254,254,254,254,0,0,254,254,254,254,254,254,0,254,254,254,254,254,254,254,254,254,254,254,254,0,254,254,254,254,254,254,0,255,0,254,254,254,254,254,254,0,255,0,254,0,0,254,254,254,0,255,0,255,0,254,254,254,254,0,255,255,0,254,254,254,0,255,255,255,0,254,254,254,0,0,0,0,0,254,254,254,254,254,254,254,254,254,254,254,0,0,0,254,254,254,254,254,0,255,0,254,254,254,254,254,0,255,0,254,254,254,254,254,0,255,0,254,254,254,0,0,0,255,0,0,0,254,254,0,255,255,255,0,254,254,254,254,0,255,0,254,254,254,254,254,254,0,254,254,254,254,0,0,0,254,254,0,0,0,0,255,255,254,254,255,255,0,0,255,254,254,254,254,255,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,255,254,254,254,254,255,0,0,255,255,254,254,255,255,0,0,0,0,254,254,0,0,0,254,254,254,254,254,254,254,254,254,0,0,254,254,0,0,254,254,0,255,254,254,255,0,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,254,0,255,254,254,255,0,254,254,0,0,254,254,0,0,254,254,254,254,254,254,254,254,254,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,73,67,75,32,85,80,32,84,72,69,32,67,82,69,87,0,0,0,0,0,0,0,0,71,69,84,32,84,79,32,84,72,69,32,66,65,78,75,0,71,69,84,32,84,79,32,84,72,69,32,83,65,70,69,72,79,85,83,69,0,0,0,0,77,73,83,83,73,79,78,32,67,79,77,80,76,69,84,69,33,0,0,0,0,0,0,0,71,79,32,67,79,76,76,69,67,84,32,84,72,69,32,80,65,89,77,69,78,84,83,0,68,79,78,39,84,32,76,69,84,32,72,73,77,32,71,69,84,32,65,87,65,89,33,0,73,84,39,83,32,84,72,69,32,67,79,80,83,33,32,69,83,67,65,80,69,33,0,0,71,79,33,0,0,0,0,0,62,32,82,69,84,82,89,32,60,0,0,0,0,0,0,0,62,32,67,79,78,84,73,78,85,69,32,60,0,0,0,0,71,69,84,32,66,65,67,75,32,84,79,32,84,72,69,32,66,79,83,83,0,0,0,0,70,79,76,76,79,87,32,72,73,77,33,0,0,0,0,0,71,69,84,32,82,69,65,68,89,0,0,0,0,0,0,0,152,140,0,0,176,140,0,0,192,140,0,0,216,140,0,0,240,140,0,0,8,141,0,0,32,141,0,0,56,141,0,0,64,141,0,0,80,141,0,0,96,141,0,0,120,141,0,0,136,141,0,0,0,0,0,0,8,8,1,8,1,1,1,3,0,90,8,7,1,2,9,4,0,90,8,7,1,2,8,4,6,1,19,53,64,6,8,1,1,10,20,50,1,128,8,1,0,4,0,0,0,0,0,0,6,1,140,70,0,6,8,1,1,10,138,66,1,128,8,1,0,1,6,8,5,1,0,60,7,4,4,0,0,0,0,0,6,1,140,73,0,255,6,6,145,73,0,48,8,6,1,8,1,1,8,2,1,8,11,1,0,1,1,5,8,5,1,0,60,8,10,1,14,7,8,1,0,8,11,0,4,0,0,0,6,1,140,73,0,255,6,2,145,73,0,48,8,6,1,8,1,1,8,11,1,0,1,1,5,8,5,1,0,60,8,1,0,8,11,0,4,0,0,0,6,1,19,53,64,6,8,1,1,0,5,10,20,50,1,128,8,1,0,1,0,11,104,99,3,8,1,1,10,103,97,1,6,10,103,97,1,48,10,103,97,1,54,8,1,0,1,1,11,138,68,3,8,1,1,9,1,138,66,54,9,1,138,66,6,9,1,138,66,48,0,40,18,5,17,32,255,1,0,20,10,138,66,1,6,10,138,66,1,48,10,138,66,1,54,8,1,0,1,2,0,60,7,2,11,211,213,3,8,1,1,9,1,212,210,54,9,1,212,210,6,9,1,212,210,48,9,1,212,210,128,5,0,0,0,0,6,1,211,213,64,6,8,1,1,10,212,210,1,128,8,1,0,0,1,1,4,11,59,228,3,8,1,1,9,1,58,226,128,0,20,18,4,17,32,255,1,0,20,18,4,17,31,255,1,0,20,10,58,226,1,128,8,1,0,6,6,22,181,64,128,14,0,1,4,11,27,180,3,8,1,1,9,1,26,178,128,0,20,18,4,17,32,255,1,0,40,10,26,178,6,0,10,26,178,1,128,13,24,36,14,8,8,2,1,8,1,0,1,11,8,10,1,15,14,0,9,6,23,33,0,8,2,0,8,10,0,11,22,36,3,8,1,1,9,1,23,33,128,0,20,18,4,17,32,255,1,0,20,18,4,17,31,255,1,0,20,10,23,33,1,128,8,1,0,1,10,11,56,19,8,1,1,9,1,55,17,128,5,0,8,11,1,6,1,136,71,0,6,6,6,136,73,0,48,8,1,1,0,1,1,12,0,180,1,7,8,9,1,14,3,8,1,0,8,11,0,16,1,8,1,1,5,0,0,0,0,0,89,79,85,32,87,82,69,67,75,69,68,32,89,79,85,82,32,67,65,82,33,0,0,0,84,72,69,89,32,71,79,84,32,65,87,65,89,33,0,0,89,79,85,32,76,79,83,84,32,84,72,69,32,82,65,67,69,33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], "i8", ALLOC_NONE, Runtime.GLOBAL_BASE+31228);




var tempDoublePtr = Runtime.alignMemory(allocate(12, "i8", ALLOC_STATIC), 8);

assert(tempDoublePtr % 8 == 0);

function copyTempFloat(ptr) { // functions, because inlining this code increases code size too much

  HEAP8[tempDoublePtr] = HEAP8[ptr];

  HEAP8[tempDoublePtr+1] = HEAP8[ptr+1];

  HEAP8[tempDoublePtr+2] = HEAP8[ptr+2];

  HEAP8[tempDoublePtr+3] = HEAP8[ptr+3];

}

function copyTempDouble(ptr) {

  HEAP8[tempDoublePtr] = HEAP8[ptr];

  HEAP8[tempDoublePtr+1] = HEAP8[ptr+1];

  HEAP8[tempDoublePtr+2] = HEAP8[ptr+2];

  HEAP8[tempDoublePtr+3] = HEAP8[ptr+3];

  HEAP8[tempDoublePtr+4] = HEAP8[ptr+4];

  HEAP8[tempDoublePtr+5] = HEAP8[ptr+5];

  HEAP8[tempDoublePtr+6] = HEAP8[ptr+6];

  HEAP8[tempDoublePtr+7] = HEAP8[ptr+7];

}


  
  
  var ___errno_state=0;function ___setErrNo(value) {
      // For convenient setting and returning of errno.
      HEAP32[((___errno_state)>>2)]=value;
      return value;
    }
  
  var ERRNO_CODES={EPERM:1,ENOENT:2,ESRCH:3,EINTR:4,EIO:5,ENXIO:6,E2BIG:7,ENOEXEC:8,EBADF:9,ECHILD:10,EAGAIN:11,EWOULDBLOCK:11,ENOMEM:12,EACCES:13,EFAULT:14,ENOTBLK:15,EBUSY:16,EEXIST:17,EXDEV:18,ENODEV:19,ENOTDIR:20,EISDIR:21,EINVAL:22,ENFILE:23,EMFILE:24,ENOTTY:25,ETXTBSY:26,EFBIG:27,ENOSPC:28,ESPIPE:29,EROFS:30,EMLINK:31,EPIPE:32,EDOM:33,ERANGE:34,ENOMSG:42,EIDRM:43,ECHRNG:44,EL2NSYNC:45,EL3HLT:46,EL3RST:47,ELNRNG:48,EUNATCH:49,ENOCSI:50,EL2HLT:51,EDEADLK:35,ENOLCK:37,EBADE:52,EBADR:53,EXFULL:54,ENOANO:55,EBADRQC:56,EBADSLT:57,EDEADLOCK:35,EBFONT:59,ENOSTR:60,ENODATA:61,ETIME:62,ENOSR:63,ENONET:64,ENOPKG:65,EREMOTE:66,ENOLINK:67,EADV:68,ESRMNT:69,ECOMM:70,EPROTO:71,EMULTIHOP:72,EDOTDOT:73,EBADMSG:74,ENOTUNIQ:76,EBADFD:77,EREMCHG:78,ELIBACC:79,ELIBBAD:80,ELIBSCN:81,ELIBMAX:82,ELIBEXEC:83,ENOSYS:38,ENOTEMPTY:39,ENAMETOOLONG:36,ELOOP:40,EOPNOTSUPP:95,EPFNOSUPPORT:96,ECONNRESET:104,ENOBUFS:105,EAFNOSUPPORT:97,EPROTOTYPE:91,ENOTSOCK:88,ENOPROTOOPT:92,ESHUTDOWN:108,ECONNREFUSED:111,EADDRINUSE:98,ECONNABORTED:103,ENETUNREACH:101,ENETDOWN:100,ETIMEDOUT:110,EHOSTDOWN:112,EHOSTUNREACH:113,EINPROGRESS:115,EALREADY:114,EDESTADDRREQ:89,EMSGSIZE:90,EPROTONOSUPPORT:93,ESOCKTNOSUPPORT:94,EADDRNOTAVAIL:99,ENETRESET:102,EISCONN:106,ENOTCONN:107,ETOOMANYREFS:109,EUSERS:87,EDQUOT:122,ESTALE:116,ENOTSUP:95,ENOMEDIUM:123,EILSEQ:84,EOVERFLOW:75,ECANCELED:125,ENOTRECOVERABLE:131,EOWNERDEAD:130,ESTRPIPE:86};function _sysconf(name) {
      // long sysconf(int name);
      // http://pubs.opengroup.org/onlinepubs/009695399/functions/sysconf.html
      switch(name) {
        case 30: return PAGE_SIZE;
        case 132:
        case 133:
        case 12:
        case 137:
        case 138:
        case 15:
        case 235:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
        case 149:
        case 13:
        case 10:
        case 236:
        case 153:
        case 9:
        case 21:
        case 22:
        case 159:
        case 154:
        case 14:
        case 77:
        case 78:
        case 139:
        case 80:
        case 81:
        case 79:
        case 82:
        case 68:
        case 67:
        case 164:
        case 11:
        case 29:
        case 47:
        case 48:
        case 95:
        case 52:
        case 51:
        case 46:
          return 200809;
        case 27:
        case 246:
        case 127:
        case 128:
        case 23:
        case 24:
        case 160:
        case 161:
        case 181:
        case 182:
        case 242:
        case 183:
        case 184:
        case 243:
        case 244:
        case 245:
        case 165:
        case 178:
        case 179:
        case 49:
        case 50:
        case 168:
        case 169:
        case 175:
        case 170:
        case 171:
        case 172:
        case 97:
        case 76:
        case 32:
        case 173:
        case 35:
          return -1;
        case 176:
        case 177:
        case 7:
        case 155:
        case 8:
        case 157:
        case 125:
        case 126:
        case 92:
        case 93:
        case 129:
        case 130:
        case 131:
        case 94:
        case 91:
          return 1;
        case 74:
        case 60:
        case 69:
        case 70:
        case 4:
          return 1024;
        case 31:
        case 42:
        case 72:
          return 32;
        case 87:
        case 26:
        case 33:
          return 2147483647;
        case 34:
        case 1:
          return 47839;
        case 38:
        case 36:
          return 99;
        case 43:
        case 37:
          return 2048;
        case 0: return 2097152;
        case 3: return 65536;
        case 28: return 32768;
        case 44: return 32767;
        case 75: return 16384;
        case 39: return 1000;
        case 89: return 700;
        case 71: return 256;
        case 40: return 255;
        case 2: return 100;
        case 180: return 64;
        case 25: return 20;
        case 5: return 16;
        case 6: return 6;
        case 73: return 4;
        case 84: {
          if (typeof navigator === 'object') return navigator['hardwareConcurrency'] || 1;
          return 1;
        }
      }
      ___setErrNo(ERRNO_CODES.EINVAL);
      return -1;
    }

  
  function _emscripten_set_main_loop_timing(mode, value) {
      Browser.mainLoop.timingMode = mode;
      Browser.mainLoop.timingValue = value;
  
      if (!Browser.mainLoop.func) {
        console.error('emscripten_set_main_loop_timing: Cannot set timing mode for main loop since a main loop does not exist! Call emscripten_set_main_loop first to set one up.');
        return 1; // Return non-zero on failure, can't set timing mode when there is no main loop.
      }
  
      if (mode == 0 /*EM_TIMING_SETTIMEOUT*/) {
        Browser.mainLoop.scheduler = function Browser_mainLoop_scheduler() {
          setTimeout(Browser.mainLoop.runner, value); // doing this each time means that on exception, we stop
        };
        Browser.mainLoop.method = 'timeout';
      } else if (mode == 1 /*EM_TIMING_RAF*/) {
        Browser.mainLoop.scheduler = function Browser_mainLoop_scheduler() {
          Browser.requestAnimationFrame(Browser.mainLoop.runner);
        };
        Browser.mainLoop.method = 'rAF';
      }
      return 0;
    }function _emscripten_set_main_loop(func, fps, simulateInfiniteLoop, arg, noSetTiming) {
      Module['noExitRuntime'] = true;
  
      assert(!Browser.mainLoop.func, 'emscripten_set_main_loop: there can only be one main loop function at once: call emscripten_cancel_main_loop to cancel the previous one before setting a new one with different parameters.');
  
      Browser.mainLoop.func = func;
      Browser.mainLoop.arg = arg;
  
      var thisMainLoopId = Browser.mainLoop.currentlyRunningMainloop;
  
      Browser.mainLoop.runner = function Browser_mainLoop_runner() {
        if (ABORT) return;
        if (Browser.mainLoop.queue.length > 0) {
          var start = Date.now();
          var blocker = Browser.mainLoop.queue.shift();
          blocker.func(blocker.arg);
          if (Browser.mainLoop.remainingBlockers) {
            var remaining = Browser.mainLoop.remainingBlockers;
            var next = remaining%1 == 0 ? remaining-1 : Math.floor(remaining);
            if (blocker.counted) {
              Browser.mainLoop.remainingBlockers = next;
            } else {
              // not counted, but move the progress along a tiny bit
              next = next + 0.5; // do not steal all the next one's progress
              Browser.mainLoop.remainingBlockers = (8*remaining + next)/9;
            }
          }
          console.log('main loop blocker "' + blocker.name + '" took ' + (Date.now() - start) + ' ms'); //, left: ' + Browser.mainLoop.remainingBlockers);
          Browser.mainLoop.updateStatus();
          setTimeout(Browser.mainLoop.runner, 0);
          return;
        }
  
        // catch pauses from non-main loop sources
        if (thisMainLoopId < Browser.mainLoop.currentlyRunningMainloop) return;
  
        // Implement very basic swap interval control
        Browser.mainLoop.currentFrameNumber = Browser.mainLoop.currentFrameNumber + 1 | 0;
        if (Browser.mainLoop.timingMode == 1/*EM_TIMING_RAF*/ && Browser.mainLoop.timingValue > 1 && Browser.mainLoop.currentFrameNumber % Browser.mainLoop.timingValue != 0) {
          // Not the scheduled time to render this frame - skip.
          Browser.mainLoop.scheduler();
          return;
        }
  
        // Signal GL rendering layer that processing of a new frame is about to start. This helps it optimize
        // VBO double-buffering and reduce GPU stalls.
  
        if (Browser.mainLoop.method === 'timeout' && Module.ctx) {
          Module.printErr('Looks like you are rendering without using requestAnimationFrame for the main loop. You should use 0 for the frame rate in emscripten_set_main_loop in order to use requestAnimationFrame, as that can greatly improve your frame rates!');
          Browser.mainLoop.method = ''; // just warn once per call to set main loop
        }
  
        Browser.mainLoop.runIter(function() {
          if (typeof arg !== 'undefined') {
            Runtime.dynCall('vi', func, [arg]);
          } else {
            Runtime.dynCall('v', func);
          }
        });
  
        // catch pauses from the main loop itself
        if (thisMainLoopId < Browser.mainLoop.currentlyRunningMainloop) return;
  
        // Queue new audio data. This is important to be right after the main loop invocation, so that we will immediately be able
        // to queue the newest produced audio samples.
        // TODO: Consider adding pre- and post- rAF callbacks so that GL.newRenderingFrameStarted() and SDL.audio.queueNewAudioData()
        //       do not need to be hardcoded into this function, but can be more generic.
        if (typeof SDL === 'object' && SDL.audio && SDL.audio.queueNewAudioData) SDL.audio.queueNewAudioData();
  
        Browser.mainLoop.scheduler();
      }
  
      if (!noSetTiming) {
        if (fps && fps > 0) _emscripten_set_main_loop_timing(0/*EM_TIMING_SETTIMEOUT*/, 1000.0 / fps);
        else _emscripten_set_main_loop_timing(1/*EM_TIMING_RAF*/, 1); // Do rAF by rendering each frame (no decimating)
  
        Browser.mainLoop.scheduler();
      }
  
      if (simulateInfiniteLoop) {
        throw 'SimulateInfiniteLoop';
      }
    }

   
  Module["_memset"] = _memset;

  function _abort() {
      Module['abort']();
    }

  
  
  
  
  var ERRNO_MESSAGES={0:"Success",1:"Not super-user",2:"No such file or directory",3:"No such process",4:"Interrupted system call",5:"I/O error",6:"No such device or address",7:"Arg list too long",8:"Exec format error",9:"Bad file number",10:"No children",11:"No more processes",12:"Not enough core",13:"Permission denied",14:"Bad address",15:"Block device required",16:"Mount device busy",17:"File exists",18:"Cross-device link",19:"No such device",20:"Not a directory",21:"Is a directory",22:"Invalid argument",23:"Too many open files in system",24:"Too many open files",25:"Not a typewriter",26:"Text file busy",27:"File too large",28:"No space left on device",29:"Illegal seek",30:"Read only file system",31:"Too many links",32:"Broken pipe",33:"Math arg out of domain of func",34:"Math result not representable",35:"File locking deadlock error",36:"File or path name too long",37:"No record locks available",38:"Function not implemented",39:"Directory not empty",40:"Too many symbolic links",42:"No message of desired type",43:"Identifier removed",44:"Channel number out of range",45:"Level 2 not synchronized",46:"Level 3 halted",47:"Level 3 reset",48:"Link number out of range",49:"Protocol driver not attached",50:"No CSI structure available",51:"Level 2 halted",52:"Invalid exchange",53:"Invalid request descriptor",54:"Exchange full",55:"No anode",56:"Invalid request code",57:"Invalid slot",59:"Bad font file fmt",60:"Device not a stream",61:"No data (for no delay io)",62:"Timer expired",63:"Out of streams resources",64:"Machine is not on the network",65:"Package not installed",66:"The object is remote",67:"The link has been severed",68:"Advertise error",69:"Srmount error",70:"Communication error on send",71:"Protocol error",72:"Multihop attempted",73:"Cross mount point (not really error)",74:"Trying to read unreadable message",75:"Value too large for defined data type",76:"Given log. name not unique",77:"f.d. invalid for this operation",78:"Remote address changed",79:"Can   access a needed shared lib",80:"Accessing a corrupted shared lib",81:".lib section in a.out corrupted",82:"Attempting to link in too many libs",83:"Attempting to exec a shared library",84:"Illegal byte sequence",86:"Streams pipe error",87:"Too many users",88:"Socket operation on non-socket",89:"Destination address required",90:"Message too long",91:"Protocol wrong type for socket",92:"Protocol not available",93:"Unknown protocol",94:"Socket type not supported",95:"Not supported",96:"Protocol family not supported",97:"Address family not supported by protocol family",98:"Address already in use",99:"Address not available",100:"Network interface is not configured",101:"Network is unreachable",102:"Connection reset by network",103:"Connection aborted",104:"Connection reset by peer",105:"No buffer space available",106:"Socket is already connected",107:"Socket is not connected",108:"Can't send after socket shutdown",109:"Too many references",110:"Connection timed out",111:"Connection refused",112:"Host is down",113:"Host is unreachable",114:"Socket already connected",115:"Connection already in progress",116:"Stale file handle",122:"Quota exceeded",123:"No medium (in tape drive)",125:"Operation canceled",130:"Previous owner died",131:"State not recoverable"};
  
  var PATH={splitPath:function (filename) {
        var splitPathRe = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
        return splitPathRe.exec(filename).slice(1);
      },normalizeArray:function (parts, allowAboveRoot) {
        // if the path tries to go above the root, `up` ends up > 0
        var up = 0;
        for (var i = parts.length - 1; i >= 0; i--) {
          var last = parts[i];
          if (last === '.') {
            parts.splice(i, 1);
          } else if (last === '..') {
            parts.splice(i, 1);
            up++;
          } else if (up) {
            parts.splice(i, 1);
            up--;
          }
        }
        // if the path is allowed to go above the root, restore leading ..s
        if (allowAboveRoot) {
          for (; up--; up) {
            parts.unshift('..');
          }
        }
        return parts;
      },normalize:function (path) {
        var isAbsolute = path.charAt(0) === '/',
            trailingSlash = path.substr(-1) === '/';
        // Normalize the path
        path = PATH.normalizeArray(path.split('/').filter(function(p) {
          return !!p;
        }), !isAbsolute).join('/');
        if (!path && !isAbsolute) {
          path = '.';
        }
        if (path && trailingSlash) {
          path += '/';
        }
        return (isAbsolute ? '/' : '') + path;
      },dirname:function (path) {
        var result = PATH.splitPath(path),
            root = result[0],
            dir = result[1];
        if (!root && !dir) {
          // No dirname whatsoever
          return '.';
        }
        if (dir) {
          // It has a dirname, strip trailing slash
          dir = dir.substr(0, dir.length - 1);
        }
        return root + dir;
      },basename:function (path) {
        // EMSCRIPTEN return '/'' for '/', not an empty string
        if (path === '/') return '/';
        var lastSlash = path.lastIndexOf('/');
        if (lastSlash === -1) return path;
        return path.substr(lastSlash+1);
      },extname:function (path) {
        return PATH.splitPath(path)[3];
      },join:function () {
        var paths = Array.prototype.slice.call(arguments, 0);
        return PATH.normalize(paths.join('/'));
      },join2:function (l, r) {
        return PATH.normalize(l + '/' + r);
      },resolve:function () {
        var resolvedPath = '',
          resolvedAbsolute = false;
        for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
          var path = (i >= 0) ? arguments[i] : FS.cwd();
          // Skip empty and invalid entries
          if (typeof path !== 'string') {
            throw new TypeError('Arguments to path.resolve must be strings');
          } else if (!path) {
            return ''; // an invalid portion invalidates the whole thing
          }
          resolvedPath = path + '/' + resolvedPath;
          resolvedAbsolute = path.charAt(0) === '/';
        }
        // At this point the path should be resolved to a full absolute path, but
        // handle relative paths to be safe (might happen when process.cwd() fails)
        resolvedPath = PATH.normalizeArray(resolvedPath.split('/').filter(function(p) {
          return !!p;
        }), !resolvedAbsolute).join('/');
        return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
      },relative:function (from, to) {
        from = PATH.resolve(from).substr(1);
        to = PATH.resolve(to).substr(1);
        function trim(arr) {
          var start = 0;
          for (; start < arr.length; start++) {
            if (arr[start] !== '') break;
          }
          var end = arr.length - 1;
          for (; end >= 0; end--) {
            if (arr[end] !== '') break;
          }
          if (start > end) return [];
          return arr.slice(start, end - start + 1);
        }
        var fromParts = trim(from.split('/'));
        var toParts = trim(to.split('/'));
        var length = Math.min(fromParts.length, toParts.length);
        var samePartsLength = length;
        for (var i = 0; i < length; i++) {
          if (fromParts[i] !== toParts[i]) {
            samePartsLength = i;
            break;
          }
        }
        var outputParts = [];
        for (var i = samePartsLength; i < fromParts.length; i++) {
          outputParts.push('..');
        }
        outputParts = outputParts.concat(toParts.slice(samePartsLength));
        return outputParts.join('/');
      }};
  
  var TTY={ttys:[],init:function () {
        // https://github.com/kripken/emscripten/pull/1555
        // if (ENVIRONMENT_IS_NODE) {
        //   // currently, FS.init does not distinguish if process.stdin is a file or TTY
        //   // device, it always assumes it's a TTY device. because of this, we're forcing
        //   // process.stdin to UTF8 encoding to at least make stdin reading compatible
        //   // with text files until FS.init can be refactored.
        //   process['stdin']['setEncoding']('utf8');
        // }
      },shutdown:function () {
        // https://github.com/kripken/emscripten/pull/1555
        // if (ENVIRONMENT_IS_NODE) {
        //   // inolen: any idea as to why node -e 'process.stdin.read()' wouldn't exit immediately (with process.stdin being a tty)?
        //   // isaacs: because now it's reading from the stream, you've expressed interest in it, so that read() kicks off a _read() which creates a ReadReq operation
        //   // inolen: I thought read() in that case was a synchronous operation that just grabbed some amount of buffered data if it exists?
        //   // isaacs: it is. but it also triggers a _read() call, which calls readStart() on the handle
        //   // isaacs: do process.stdin.pause() and i'd think it'd probably close the pending call
        //   process['stdin']['pause']();
        // }
      },register:function (dev, ops) {
        TTY.ttys[dev] = { input: [], output: [], ops: ops };
        FS.registerDevice(dev, TTY.stream_ops);
      },stream_ops:{open:function (stream) {
          var tty = TTY.ttys[stream.node.rdev];
          if (!tty) {
            throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
          }
          stream.tty = tty;
          stream.seekable = false;
        },close:function (stream) {
          // flush any pending line data
          stream.tty.ops.flush(stream.tty);
        },flush:function (stream) {
          stream.tty.ops.flush(stream.tty);
        },read:function (stream, buffer, offset, length, pos /* ignored */) {
          if (!stream.tty || !stream.tty.ops.get_char) {
            throw new FS.ErrnoError(ERRNO_CODES.ENXIO);
          }
          var bytesRead = 0;
          for (var i = 0; i < length; i++) {
            var result;
            try {
              result = stream.tty.ops.get_char(stream.tty);
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
            if (result === undefined && bytesRead === 0) {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
            if (result === null || result === undefined) break;
            bytesRead++;
            buffer[offset+i] = result;
          }
          if (bytesRead) {
            stream.node.timestamp = Date.now();
          }
          return bytesRead;
        },write:function (stream, buffer, offset, length, pos) {
          if (!stream.tty || !stream.tty.ops.put_char) {
            throw new FS.ErrnoError(ERRNO_CODES.ENXIO);
          }
          for (var i = 0; i < length; i++) {
            try {
              stream.tty.ops.put_char(stream.tty, buffer[offset+i]);
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
          }
          if (length) {
            stream.node.timestamp = Date.now();
          }
          return i;
        }},default_tty_ops:{get_char:function (tty) {
          if (!tty.input.length) {
            var result = null;
            if (ENVIRONMENT_IS_NODE) {
              // we will read data by chunks of BUFSIZE
              var BUFSIZE = 256;
              var buf = new Buffer(BUFSIZE);
              var bytesRead = 0;
  
              var fd = process.stdin.fd;
              // Linux and Mac cannot use process.stdin.fd (which isn't set up as sync)
              var usingDevice = false;
              try {
                fd = fs.openSync('/dev/stdin', 'r');
                usingDevice = true;
              } catch (e) {}
  
              bytesRead = fs.readSync(fd, buf, 0, BUFSIZE, null);
  
              if (usingDevice) { fs.closeSync(fd); }
              if (bytesRead > 0) {
                result = buf.slice(0, bytesRead).toString('utf-8');
              } else {
                result = null;
              }
  
            } else if (typeof window != 'undefined' &&
              typeof window.prompt == 'function') {
              // Browser.
              result = window.prompt('Input: ');  // returns null on cancel
              if (result !== null) {
                result += '\n';
              }
            } else if (typeof readline == 'function') {
              // Command line.
              result = readline();
              if (result !== null) {
                result += '\n';
              }
            }
            if (!result) {
              return null;
            }
            tty.input = intArrayFromString(result, true);
          }
          return tty.input.shift();
        },put_char:function (tty, val) {
          if (val === null || val === 10) {
            Module['print'](UTF8ArrayToString(tty.output, 0));
            tty.output = [];
          } else {
            if (val != 0) tty.output.push(val); // val == 0 would cut text output off in the middle.
          }
        },flush:function (tty) {
          if (tty.output && tty.output.length > 0) {
            Module['print'](UTF8ArrayToString(tty.output, 0));
            tty.output = [];
          }
        }},default_tty1_ops:{put_char:function (tty, val) {
          if (val === null || val === 10) {
            Module['printErr'](UTF8ArrayToString(tty.output, 0));
            tty.output = [];
          } else {
            if (val != 0) tty.output.push(val);
          }
        },flush:function (tty) {
          if (tty.output && tty.output.length > 0) {
            Module['printErr'](UTF8ArrayToString(tty.output, 0));
            tty.output = [];
          }
        }}};
  
  var MEMFS={ops_table:null,mount:function (mount) {
        return MEMFS.createNode(null, '/', 16384 | 511 /* 0777 */, 0);
      },createNode:function (parent, name, mode, dev) {
        if (FS.isBlkdev(mode) || FS.isFIFO(mode)) {
          // no supported
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (!MEMFS.ops_table) {
          MEMFS.ops_table = {
            dir: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr,
                lookup: MEMFS.node_ops.lookup,
                mknod: MEMFS.node_ops.mknod,
                rename: MEMFS.node_ops.rename,
                unlink: MEMFS.node_ops.unlink,
                rmdir: MEMFS.node_ops.rmdir,
                readdir: MEMFS.node_ops.readdir,
                symlink: MEMFS.node_ops.symlink
              },
              stream: {
                llseek: MEMFS.stream_ops.llseek
              }
            },
            file: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr
              },
              stream: {
                llseek: MEMFS.stream_ops.llseek,
                read: MEMFS.stream_ops.read,
                write: MEMFS.stream_ops.write,
                allocate: MEMFS.stream_ops.allocate,
                mmap: MEMFS.stream_ops.mmap
              }
            },
            link: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr,
                readlink: MEMFS.node_ops.readlink
              },
              stream: {}
            },
            chrdev: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr
              },
              stream: FS.chrdev_stream_ops
            }
          };
        }
        var node = FS.createNode(parent, name, mode, dev);
        if (FS.isDir(node.mode)) {
          node.node_ops = MEMFS.ops_table.dir.node;
          node.stream_ops = MEMFS.ops_table.dir.stream;
          node.contents = {};
        } else if (FS.isFile(node.mode)) {
          node.node_ops = MEMFS.ops_table.file.node;
          node.stream_ops = MEMFS.ops_table.file.stream;
          node.usedBytes = 0; // The actual number of bytes used in the typed array, as opposed to contents.buffer.byteLength which gives the whole capacity.
          // When the byte data of the file is populated, this will point to either a typed array, or a normal JS array. Typed arrays are preferred
          // for performance, and used by default. However, typed arrays are not resizable like normal JS arrays are, so there is a small disk size
          // penalty involved for appending file writes that continuously grow a file similar to std::vector capacity vs used -scheme.
          node.contents = null; 
        } else if (FS.isLink(node.mode)) {
          node.node_ops = MEMFS.ops_table.link.node;
          node.stream_ops = MEMFS.ops_table.link.stream;
        } else if (FS.isChrdev(node.mode)) {
          node.node_ops = MEMFS.ops_table.chrdev.node;
          node.stream_ops = MEMFS.ops_table.chrdev.stream;
        }
        node.timestamp = Date.now();
        // add the new node to the parent
        if (parent) {
          parent.contents[name] = node;
        }
        return node;
      },getFileDataAsRegularArray:function (node) {
        if (node.contents && node.contents.subarray) {
          var arr = [];
          for (var i = 0; i < node.usedBytes; ++i) arr.push(node.contents[i]);
          return arr; // Returns a copy of the original data.
        }
        return node.contents; // No-op, the file contents are already in a JS array. Return as-is.
      },getFileDataAsTypedArray:function (node) {
        if (!node.contents) return new Uint8Array;
        if (node.contents.subarray) return node.contents.subarray(0, node.usedBytes); // Make sure to not return excess unused bytes.
        return new Uint8Array(node.contents);
      },expandFileStorage:function (node, newCapacity) {
  
        // If we are asked to expand the size of a file that already exists, revert to using a standard JS array to store the file
        // instead of a typed array. This makes resizing the array more flexible because we can just .push() elements at the back to
        // increase the size.
        if (node.contents && node.contents.subarray && newCapacity > node.contents.length) {
          node.contents = MEMFS.getFileDataAsRegularArray(node);
          node.usedBytes = node.contents.length; // We might be writing to a lazy-loaded file which had overridden this property, so force-reset it.
        }
  
        if (!node.contents || node.contents.subarray) { // Keep using a typed array if creating a new storage, or if old one was a typed array as well.
          var prevCapacity = node.contents ? node.contents.buffer.byteLength : 0;
          if (prevCapacity >= newCapacity) return; // No need to expand, the storage was already large enough.
          // Don't expand strictly to the given requested limit if it's only a very small increase, but instead geometrically grow capacity.
          // For small filesizes (<1MB), perform size*2 geometric increase, but for large sizes, do a much more conservative size*1.125 increase to
          // avoid overshooting the allocation cap by a very large margin.
          var CAPACITY_DOUBLING_MAX = 1024 * 1024;
          newCapacity = Math.max(newCapacity, (prevCapacity * (prevCapacity < CAPACITY_DOUBLING_MAX ? 2.0 : 1.125)) | 0);
          if (prevCapacity != 0) newCapacity = Math.max(newCapacity, 256); // At minimum allocate 256b for each file when expanding.
          var oldContents = node.contents;
          node.contents = new Uint8Array(newCapacity); // Allocate new storage.
          if (node.usedBytes > 0) node.contents.set(oldContents.subarray(0, node.usedBytes), 0); // Copy old data over to the new storage.
          return;
        }
        // Not using a typed array to back the file storage. Use a standard JS array instead.
        if (!node.contents && newCapacity > 0) node.contents = [];
        while (node.contents.length < newCapacity) node.contents.push(0);
      },resizeFileStorage:function (node, newSize) {
        if (node.usedBytes == newSize) return;
        if (newSize == 0) {
          node.contents = null; // Fully decommit when requesting a resize to zero.
          node.usedBytes = 0;
          return;
        }
  
        if (!node.contents || node.contents.subarray) { // Resize a typed array if that is being used as the backing store.
          var oldContents = node.contents;
          node.contents = new Uint8Array(new ArrayBuffer(newSize)); // Allocate new storage.
          if (oldContents) {
            node.contents.set(oldContents.subarray(0, Math.min(newSize, node.usedBytes))); // Copy old data over to the new storage.
          }
          node.usedBytes = newSize;
          return;
        }
        // Backing with a JS array.
        if (!node.contents) node.contents = [];
        if (node.contents.length > newSize) node.contents.length = newSize;
        else while (node.contents.length < newSize) node.contents.push(0);
        node.usedBytes = newSize;
      },node_ops:{getattr:function (node) {
          var attr = {};
          // device numbers reuse inode numbers.
          attr.dev = FS.isChrdev(node.mode) ? node.id : 1;
          attr.ino = node.id;
          attr.mode = node.mode;
          attr.nlink = 1;
          attr.uid = 0;
          attr.gid = 0;
          attr.rdev = node.rdev;
          if (FS.isDir(node.mode)) {
            attr.size = 4096;
          } else if (FS.isFile(node.mode)) {
            attr.size = node.usedBytes;
          } else if (FS.isLink(node.mode)) {
            attr.size = node.link.length;
          } else {
            attr.size = 0;
          }
          attr.atime = new Date(node.timestamp);
          attr.mtime = new Date(node.timestamp);
          attr.ctime = new Date(node.timestamp);
          // NOTE: In our implementation, st_blocks = Math.ceil(st_size/st_blksize),
          //       but this is not required by the standard.
          attr.blksize = 4096;
          attr.blocks = Math.ceil(attr.size / attr.blksize);
          return attr;
        },setattr:function (node, attr) {
          if (attr.mode !== undefined) {
            node.mode = attr.mode;
          }
          if (attr.timestamp !== undefined) {
            node.timestamp = attr.timestamp;
          }
          if (attr.size !== undefined) {
            MEMFS.resizeFileStorage(node, attr.size);
          }
        },lookup:function (parent, name) {
          throw FS.genericErrors[ERRNO_CODES.ENOENT];
        },mknod:function (parent, name, mode, dev) {
          return MEMFS.createNode(parent, name, mode, dev);
        },rename:function (old_node, new_dir, new_name) {
          // if we're overwriting a directory at new_name, make sure it's empty.
          if (FS.isDir(old_node.mode)) {
            var new_node;
            try {
              new_node = FS.lookupNode(new_dir, new_name);
            } catch (e) {
            }
            if (new_node) {
              for (var i in new_node.contents) {
                throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
              }
            }
          }
          // do the internal rewiring
          delete old_node.parent.contents[old_node.name];
          old_node.name = new_name;
          new_dir.contents[new_name] = old_node;
          old_node.parent = new_dir;
        },unlink:function (parent, name) {
          delete parent.contents[name];
        },rmdir:function (parent, name) {
          var node = FS.lookupNode(parent, name);
          for (var i in node.contents) {
            throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
          }
          delete parent.contents[name];
        },readdir:function (node) {
          var entries = ['.', '..']
          for (var key in node.contents) {
            if (!node.contents.hasOwnProperty(key)) {
              continue;
            }
            entries.push(key);
          }
          return entries;
        },symlink:function (parent, newname, oldpath) {
          var node = MEMFS.createNode(parent, newname, 511 /* 0777 */ | 40960, 0);
          node.link = oldpath;
          return node;
        },readlink:function (node) {
          if (!FS.isLink(node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          return node.link;
        }},stream_ops:{read:function (stream, buffer, offset, length, position) {
          var contents = stream.node.contents;
          if (position >= stream.node.usedBytes) return 0;
          var size = Math.min(stream.node.usedBytes - position, length);
          assert(size >= 0);
          if (size > 8 && contents.subarray) { // non-trivial, and typed array
            buffer.set(contents.subarray(position, position + size), offset);
          } else
          {
            for (var i = 0; i < size; i++) buffer[offset + i] = contents[position + i];
          }
          return size;
        },write:function (stream, buffer, offset, length, position, canOwn) {
          if (!length) return 0;
          var node = stream.node;
          node.timestamp = Date.now();
  
          if (buffer.subarray && (!node.contents || node.contents.subarray)) { // This write is from a typed array to a typed array?
            if (canOwn) { // Can we just reuse the buffer we are given?
              assert(position === 0, 'canOwn must imply no weird position inside the file');
              node.contents = buffer.subarray(offset, offset + length);
              node.usedBytes = length;
              return length;
            } else if (node.usedBytes === 0 && position === 0) { // If this is a simple first write to an empty file, do a fast set since we don't need to care about old data.
              node.contents = new Uint8Array(buffer.subarray(offset, offset + length));
              node.usedBytes = length;
              return length;
            } else if (position + length <= node.usedBytes) { // Writing to an already allocated and used subrange of the file?
              node.contents.set(buffer.subarray(offset, offset + length), position);
              return length;
            }
          }
          // Appending to an existing file and we need to reallocate, or source data did not come as a typed array.
          MEMFS.expandFileStorage(node, position+length);
          if (node.contents.subarray && buffer.subarray) node.contents.set(buffer.subarray(offset, offset + length), position); // Use typed array write if available.
          else
            for (var i = 0; i < length; i++) {
             node.contents[position + i] = buffer[offset + i]; // Or fall back to manual write if not.
            }
          node.usedBytes = Math.max(node.usedBytes, position+length);
          return length;
        },llseek:function (stream, offset, whence) {
          var position = offset;
          if (whence === 1) {  // SEEK_CUR.
            position += stream.position;
          } else if (whence === 2) {  // SEEK_END.
            if (FS.isFile(stream.node.mode)) {
              position += stream.node.usedBytes;
            }
          }
          if (position < 0) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          return position;
        },allocate:function (stream, offset, length) {
          MEMFS.expandFileStorage(stream.node, offset + length);
          stream.node.usedBytes = Math.max(stream.node.usedBytes, offset + length);
        },mmap:function (stream, buffer, offset, length, position, prot, flags) {
          if (!FS.isFile(stream.node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
          }
          var ptr;
          var allocated;
          var contents = stream.node.contents;
          // Only make a new copy when MAP_PRIVATE is specified.
          if ( !(flags & 2) &&
                (contents.buffer === buffer || contents.buffer === buffer.buffer) ) {
            // We can't emulate MAP_SHARED when the file is not backed by the buffer
            // we're mapping to (e.g. the HEAP buffer).
            allocated = false;
            ptr = contents.byteOffset;
          } else {
            // Try to avoid unnecessary slices.
            if (position > 0 || position + length < stream.node.usedBytes) {
              if (contents.subarray) {
                contents = contents.subarray(position, position + length);
              } else {
                contents = Array.prototype.slice.call(contents, position, position + length);
              }
            }
            allocated = true;
            ptr = _malloc(length);
            if (!ptr) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOMEM);
            }
            buffer.set(contents, ptr);
          }
          return { ptr: ptr, allocated: allocated };
        }}};
  
  var IDBFS={dbs:{},indexedDB:function () {
        if (typeof indexedDB !== 'undefined') return indexedDB;
        var ret = null;
        if (typeof window === 'object') ret = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        assert(ret, 'IDBFS used, but indexedDB not supported');
        return ret;
      },DB_VERSION:21,DB_STORE_NAME:"FILE_DATA",mount:function (mount) {
        // reuse all of the core MEMFS functionality
        return MEMFS.mount.apply(null, arguments);
      },syncfs:function (mount, populate, callback) {
        IDBFS.getLocalSet(mount, function(err, local) {
          if (err) return callback(err);
  
          IDBFS.getRemoteSet(mount, function(err, remote) {
            if (err) return callback(err);
  
            var src = populate ? remote : local;
            var dst = populate ? local : remote;
  
            IDBFS.reconcile(src, dst, callback);
          });
        });
      },getDB:function (name, callback) {
        // check the cache first
        var db = IDBFS.dbs[name];
        if (db) {
          return callback(null, db);
        }
  
        var req;
        try {
          req = IDBFS.indexedDB().open(name, IDBFS.DB_VERSION);
        } catch (e) {
          return callback(e);
        }
        req.onupgradeneeded = function(e) {
          var db = e.target.result;
          var transaction = e.target.transaction;
  
          var fileStore;
  
          if (db.objectStoreNames.contains(IDBFS.DB_STORE_NAME)) {
            fileStore = transaction.objectStore(IDBFS.DB_STORE_NAME);
          } else {
            fileStore = db.createObjectStore(IDBFS.DB_STORE_NAME);
          }
  
          if (!fileStore.indexNames.contains('timestamp')) {
            fileStore.createIndex('timestamp', 'timestamp', { unique: false });
          }
        };
        req.onsuccess = function() {
          db = req.result;
  
          // add to the cache
          IDBFS.dbs[name] = db;
          callback(null, db);
        };
        req.onerror = function(e) {
          callback(this.error);
          e.preventDefault();
        };
      },getLocalSet:function (mount, callback) {
        var entries = {};
  
        function isRealDir(p) {
          return p !== '.' && p !== '..';
        };
        function toAbsolute(root) {
          return function(p) {
            return PATH.join2(root, p);
          }
        };
  
        var check = FS.readdir(mount.mountpoint).filter(isRealDir).map(toAbsolute(mount.mountpoint));
  
        while (check.length) {
          var path = check.pop();
          var stat;
  
          try {
            stat = FS.stat(path);
          } catch (e) {
            return callback(e);
          }
  
          if (FS.isDir(stat.mode)) {
            check.push.apply(check, FS.readdir(path).filter(isRealDir).map(toAbsolute(path)));
          }
  
          entries[path] = { timestamp: stat.mtime };
        }
  
        return callback(null, { type: 'local', entries: entries });
      },getRemoteSet:function (mount, callback) {
        var entries = {};
  
        IDBFS.getDB(mount.mountpoint, function(err, db) {
          if (err) return callback(err);
  
          var transaction = db.transaction([IDBFS.DB_STORE_NAME], 'readonly');
          transaction.onerror = function(e) {
            callback(this.error);
            e.preventDefault();
          };
  
          var store = transaction.objectStore(IDBFS.DB_STORE_NAME);
          var index = store.index('timestamp');
  
          index.openKeyCursor().onsuccess = function(event) {
            var cursor = event.target.result;
  
            if (!cursor) {
              return callback(null, { type: 'remote', db: db, entries: entries });
            }
  
            entries[cursor.primaryKey] = { timestamp: cursor.key };
  
            cursor.continue();
          };
        });
      },loadLocalEntry:function (path, callback) {
        var stat, node;
  
        try {
          var lookup = FS.lookupPath(path);
          node = lookup.node;
          stat = FS.stat(path);
        } catch (e) {
          return callback(e);
        }
  
        if (FS.isDir(stat.mode)) {
          return callback(null, { timestamp: stat.mtime, mode: stat.mode });
        } else if (FS.isFile(stat.mode)) {
          // Performance consideration: storing a normal JavaScript array to a IndexedDB is much slower than storing a typed array.
          // Therefore always convert the file contents to a typed array first before writing the data to IndexedDB.
          node.contents = MEMFS.getFileDataAsTypedArray(node);
          return callback(null, { timestamp: stat.mtime, mode: stat.mode, contents: node.contents });
        } else {
          return callback(new Error('node type not supported'));
        }
      },storeLocalEntry:function (path, entry, callback) {
        try {
          if (FS.isDir(entry.mode)) {
            FS.mkdir(path, entry.mode);
          } else if (FS.isFile(entry.mode)) {
            FS.writeFile(path, entry.contents, { encoding: 'binary', canOwn: true });
          } else {
            return callback(new Error('node type not supported'));
          }
  
          FS.chmod(path, entry.mode);
          FS.utime(path, entry.timestamp, entry.timestamp);
        } catch (e) {
          return callback(e);
        }
  
        callback(null);
      },removeLocalEntry:function (path, callback) {
        try {
          var lookup = FS.lookupPath(path);
          var stat = FS.stat(path);
  
          if (FS.isDir(stat.mode)) {
            FS.rmdir(path);
          } else if (FS.isFile(stat.mode)) {
            FS.unlink(path);
          }
        } catch (e) {
          return callback(e);
        }
  
        callback(null);
      },loadRemoteEntry:function (store, path, callback) {
        var req = store.get(path);
        req.onsuccess = function(event) { callback(null, event.target.result); };
        req.onerror = function(e) {
          callback(this.error);
          e.preventDefault();
        };
      },storeRemoteEntry:function (store, path, entry, callback) {
        var req = store.put(entry, path);
        req.onsuccess = function() { callback(null); };
        req.onerror = function(e) {
          callback(this.error);
          e.preventDefault();
        };
      },removeRemoteEntry:function (store, path, callback) {
        var req = store.delete(path);
        req.onsuccess = function() { callback(null); };
        req.onerror = function(e) {
          callback(this.error);
          e.preventDefault();
        };
      },reconcile:function (src, dst, callback) {
        var total = 0;
  
        var create = [];
        Object.keys(src.entries).forEach(function (key) {
          var e = src.entries[key];
          var e2 = dst.entries[key];
          if (!e2 || e.timestamp > e2.timestamp) {
            create.push(key);
            total++;
          }
        });
  
        var remove = [];
        Object.keys(dst.entries).forEach(function (key) {
          var e = dst.entries[key];
          var e2 = src.entries[key];
          if (!e2) {
            remove.push(key);
            total++;
          }
        });
  
        if (!total) {
          return callback(null);
        }
  
        var errored = false;
        var completed = 0;
        var db = src.type === 'remote' ? src.db : dst.db;
        var transaction = db.transaction([IDBFS.DB_STORE_NAME], 'readwrite');
        var store = transaction.objectStore(IDBFS.DB_STORE_NAME);
  
        function done(err) {
          if (err) {
            if (!done.errored) {
              done.errored = true;
              return callback(err);
            }
            return;
          }
          if (++completed >= total) {
            return callback(null);
          }
        };
  
        transaction.onerror = function(e) {
          done(this.error);
          e.preventDefault();
        };
  
        // sort paths in ascending order so directory entries are created
        // before the files inside them
        create.sort().forEach(function (path) {
          if (dst.type === 'local') {
            IDBFS.loadRemoteEntry(store, path, function (err, entry) {
              if (err) return done(err);
              IDBFS.storeLocalEntry(path, entry, done);
            });
          } else {
            IDBFS.loadLocalEntry(path, function (err, entry) {
              if (err) return done(err);
              IDBFS.storeRemoteEntry(store, path, entry, done);
            });
          }
        });
  
        // sort paths in descending order so files are deleted before their
        // parent directories
        remove.sort().reverse().forEach(function(path) {
          if (dst.type === 'local') {
            IDBFS.removeLocalEntry(path, done);
          } else {
            IDBFS.removeRemoteEntry(store, path, done);
          }
        });
      }};
  
  var NODEFS={isWindows:false,staticInit:function () {
        NODEFS.isWindows = !!process.platform.match(/^win/);
      },mount:function (mount) {
        assert(ENVIRONMENT_IS_NODE);
        return NODEFS.createNode(null, '/', NODEFS.getMode(mount.opts.root), 0);
      },createNode:function (parent, name, mode, dev) {
        if (!FS.isDir(mode) && !FS.isFile(mode) && !FS.isLink(mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var node = FS.createNode(parent, name, mode);
        node.node_ops = NODEFS.node_ops;
        node.stream_ops = NODEFS.stream_ops;
        return node;
      },getMode:function (path) {
        var stat;
        try {
          stat = fs.lstatSync(path);
          if (NODEFS.isWindows) {
            // On Windows, directories return permission bits 'rw-rw-rw-', even though they have 'rwxrwxrwx', so
            // propagate write bits to execute bits.
            stat.mode = stat.mode | ((stat.mode & 146) >> 1);
          }
        } catch (e) {
          if (!e.code) throw e;
          throw new FS.ErrnoError(ERRNO_CODES[e.code]);
        }
        return stat.mode;
      },realPath:function (node) {
        var parts = [];
        while (node.parent !== node) {
          parts.push(node.name);
          node = node.parent;
        }
        parts.push(node.mount.opts.root);
        parts.reverse();
        return PATH.join.apply(null, parts);
      },flagsToPermissionStringMap:{0:"r",1:"r+",2:"r+",64:"r",65:"r+",66:"r+",129:"rx+",193:"rx+",514:"w+",577:"w",578:"w+",705:"wx",706:"wx+",1024:"a",1025:"a",1026:"a+",1089:"a",1090:"a+",1153:"ax",1154:"ax+",1217:"ax",1218:"ax+",4096:"rs",4098:"rs+"},flagsToPermissionString:function (flags) {
        if (flags in NODEFS.flagsToPermissionStringMap) {
          return NODEFS.flagsToPermissionStringMap[flags];
        } else {
          return flags;
        }
      },node_ops:{getattr:function (node) {
          var path = NODEFS.realPath(node);
          var stat;
          try {
            stat = fs.lstatSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          // node.js v0.10.20 doesn't report blksize and blocks on Windows. Fake them with default blksize of 4096.
          // See http://support.microsoft.com/kb/140365
          if (NODEFS.isWindows && !stat.blksize) {
            stat.blksize = 4096;
          }
          if (NODEFS.isWindows && !stat.blocks) {
            stat.blocks = (stat.size+stat.blksize-1)/stat.blksize|0;
          }
          return {
            dev: stat.dev,
            ino: stat.ino,
            mode: stat.mode,
            nlink: stat.nlink,
            uid: stat.uid,
            gid: stat.gid,
            rdev: stat.rdev,
            size: stat.size,
            atime: stat.atime,
            mtime: stat.mtime,
            ctime: stat.ctime,
            blksize: stat.blksize,
            blocks: stat.blocks
          };
        },setattr:function (node, attr) {
          var path = NODEFS.realPath(node);
          try {
            if (attr.mode !== undefined) {
              fs.chmodSync(path, attr.mode);
              // update the common node structure mode as well
              node.mode = attr.mode;
            }
            if (attr.timestamp !== undefined) {
              var date = new Date(attr.timestamp);
              fs.utimesSync(path, date, date);
            }
            if (attr.size !== undefined) {
              fs.truncateSync(path, attr.size);
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },lookup:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          var mode = NODEFS.getMode(path);
          return NODEFS.createNode(parent, name, mode);
        },mknod:function (parent, name, mode, dev) {
          var node = NODEFS.createNode(parent, name, mode, dev);
          // create the backing node for this in the fs root as well
          var path = NODEFS.realPath(node);
          try {
            if (FS.isDir(node.mode)) {
              fs.mkdirSync(path, node.mode);
            } else {
              fs.writeFileSync(path, '', { mode: node.mode });
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          return node;
        },rename:function (oldNode, newDir, newName) {
          var oldPath = NODEFS.realPath(oldNode);
          var newPath = PATH.join2(NODEFS.realPath(newDir), newName);
          try {
            fs.renameSync(oldPath, newPath);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },unlink:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          try {
            fs.unlinkSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },rmdir:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          try {
            fs.rmdirSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },readdir:function (node) {
          var path = NODEFS.realPath(node);
          try {
            return fs.readdirSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },symlink:function (parent, newName, oldPath) {
          var newPath = PATH.join2(NODEFS.realPath(parent), newName);
          try {
            fs.symlinkSync(oldPath, newPath);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },readlink:function (node) {
          var path = NODEFS.realPath(node);
          try {
            path = fs.readlinkSync(path);
            path = NODEJS_PATH.relative(NODEJS_PATH.resolve(node.mount.opts.root), path);
            return path;
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        }},stream_ops:{open:function (stream) {
          var path = NODEFS.realPath(stream.node);
          try {
            if (FS.isFile(stream.node.mode)) {
              stream.nfd = fs.openSync(path, NODEFS.flagsToPermissionString(stream.flags));
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },close:function (stream) {
          try {
            if (FS.isFile(stream.node.mode) && stream.nfd) {
              fs.closeSync(stream.nfd);
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },read:function (stream, buffer, offset, length, position) {
          if (length === 0) return 0; // node errors on 0 length reads
          // FIXME this is terrible.
          var nbuffer = new Buffer(length);
          var res;
          try {
            res = fs.readSync(stream.nfd, nbuffer, 0, length, position);
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          if (res > 0) {
            for (var i = 0; i < res; i++) {
              buffer[offset + i] = nbuffer[i];
            }
          }
          return res;
        },write:function (stream, buffer, offset, length, position) {
          // FIXME this is terrible.
          var nbuffer = new Buffer(buffer.subarray(offset, offset + length));
          var res;
          try {
            res = fs.writeSync(stream.nfd, nbuffer, 0, length, position);
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          return res;
        },llseek:function (stream, offset, whence) {
          var position = offset;
          if (whence === 1) {  // SEEK_CUR.
            position += stream.position;
          } else if (whence === 2) {  // SEEK_END.
            if (FS.isFile(stream.node.mode)) {
              try {
                var stat = fs.fstatSync(stream.nfd);
                position += stat.size;
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES[e.code]);
              }
            }
          }
  
          if (position < 0) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
  
          return position;
        }}};
  
  var _stdin=allocate(1, "i32*", ALLOC_STATIC);
  
  var _stdout=allocate(1, "i32*", ALLOC_STATIC);
  
  var _stderr=allocate(1, "i32*", ALLOC_STATIC);
  
  function _fflush(stream) {
      // int fflush(FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fflush.html
  
      /*
      // Disabled, see https://github.com/kripken/emscripten/issues/2770
      stream = FS.getStreamFromPtr(stream);
      if (stream.stream_ops.flush) {
        stream.stream_ops.flush(stream);
      }
      */
    }var FS={root:null,mounts:[],devices:[null],streams:[],nextInode:1,nameTable:null,currentPath:"/",initialized:false,ignorePermissions:true,trackingDelegate:{},tracking:{openFlags:{READ:1,WRITE:2}},ErrnoError:null,genericErrors:{},handleFSError:function (e) {
        if (!(e instanceof FS.ErrnoError)) throw e + ' : ' + stackTrace();
        return ___setErrNo(e.errno);
      },lookupPath:function (path, opts) {
        path = PATH.resolve(FS.cwd(), path);
        opts = opts || {};
  
        if (!path) return { path: '', node: null };
  
        var defaults = {
          follow_mount: true,
          recurse_count: 0
        };
        for (var key in defaults) {
          if (opts[key] === undefined) {
            opts[key] = defaults[key];
          }
        }
  
        if (opts.recurse_count > 8) {  // max recursive lookup of 8
          throw new FS.ErrnoError(ERRNO_CODES.ELOOP);
        }
  
        // split the path
        var parts = PATH.normalizeArray(path.split('/').filter(function(p) {
          return !!p;
        }), false);
  
        // start at the root
        var current = FS.root;
        var current_path = '/';
  
        for (var i = 0; i < parts.length; i++) {
          var islast = (i === parts.length-1);
          if (islast && opts.parent) {
            // stop resolving
            break;
          }
  
          current = FS.lookupNode(current, parts[i]);
          current_path = PATH.join2(current_path, parts[i]);
  
          // jump to the mount's root node if this is a mountpoint
          if (FS.isMountpoint(current)) {
            if (!islast || (islast && opts.follow_mount)) {
              current = current.mounted.root;
            }
          }
  
          // by default, lookupPath will not follow a symlink if it is the final path component.
          // setting opts.follow = true will override this behavior.
          if (!islast || opts.follow) {
            var count = 0;
            while (FS.isLink(current.mode)) {
              var link = FS.readlink(current_path);
              current_path = PATH.resolve(PATH.dirname(current_path), link);
  
              var lookup = FS.lookupPath(current_path, { recurse_count: opts.recurse_count });
              current = lookup.node;
  
              if (count++ > 40) {  // limit max consecutive symlinks to 40 (SYMLOOP_MAX).
                throw new FS.ErrnoError(ERRNO_CODES.ELOOP);
              }
            }
          }
        }
  
        return { path: current_path, node: current };
      },getPath:function (node) {
        var path;
        while (true) {
          if (FS.isRoot(node)) {
            var mount = node.mount.mountpoint;
            if (!path) return mount;
            return mount[mount.length-1] !== '/' ? mount + '/' + path : mount + path;
          }
          path = path ? node.name + '/' + path : node.name;
          node = node.parent;
        }
      },hashName:function (parentid, name) {
        var hash = 0;
  
  
        for (var i = 0; i < name.length; i++) {
          hash = ((hash << 5) - hash + name.charCodeAt(i)) | 0;
        }
        return ((parentid + hash) >>> 0) % FS.nameTable.length;
      },hashAddNode:function (node) {
        var hash = FS.hashName(node.parent.id, node.name);
        node.name_next = FS.nameTable[hash];
        FS.nameTable[hash] = node;
      },hashRemoveNode:function (node) {
        var hash = FS.hashName(node.parent.id, node.name);
        if (FS.nameTable[hash] === node) {
          FS.nameTable[hash] = node.name_next;
        } else {
          var current = FS.nameTable[hash];
          while (current) {
            if (current.name_next === node) {
              current.name_next = node.name_next;
              break;
            }
            current = current.name_next;
          }
        }
      },lookupNode:function (parent, name) {
        var err = FS.mayLookup(parent);
        if (err) {
          throw new FS.ErrnoError(err, parent);
        }
        var hash = FS.hashName(parent.id, name);
        for (var node = FS.nameTable[hash]; node; node = node.name_next) {
          var nodeName = node.name;
          if (node.parent.id === parent.id && nodeName === name) {
            return node;
          }
        }
        // if we failed to find it in the cache, call into the VFS
        return FS.lookup(parent, name);
      },createNode:function (parent, name, mode, rdev) {
        if (!FS.FSNode) {
          FS.FSNode = function(parent, name, mode, rdev) {
            if (!parent) {
              parent = this;  // root node sets parent to itself
            }
            this.parent = parent;
            this.mount = parent.mount;
            this.mounted = null;
            this.id = FS.nextInode++;
            this.name = name;
            this.mode = mode;
            this.node_ops = {};
            this.stream_ops = {};
            this.rdev = rdev;
          };
  
          FS.FSNode.prototype = {};
  
          // compatibility
          var readMode = 292 | 73;
          var writeMode = 146;
  
          // NOTE we must use Object.defineProperties instead of individual calls to
          // Object.defineProperty in order to make closure compiler happy
          Object.defineProperties(FS.FSNode.prototype, {
            read: {
              get: function() { return (this.mode & readMode) === readMode; },
              set: function(val) { val ? this.mode |= readMode : this.mode &= ~readMode; }
            },
            write: {
              get: function() { return (this.mode & writeMode) === writeMode; },
              set: function(val) { val ? this.mode |= writeMode : this.mode &= ~writeMode; }
            },
            isFolder: {
              get: function() { return FS.isDir(this.mode); }
            },
            isDevice: {
              get: function() { return FS.isChrdev(this.mode); }
            }
          });
        }
  
        var node = new FS.FSNode(parent, name, mode, rdev);
  
        FS.hashAddNode(node);
  
        return node;
      },destroyNode:function (node) {
        FS.hashRemoveNode(node);
      },isRoot:function (node) {
        return node === node.parent;
      },isMountpoint:function (node) {
        return !!node.mounted;
      },isFile:function (mode) {
        return (mode & 61440) === 32768;
      },isDir:function (mode) {
        return (mode & 61440) === 16384;
      },isLink:function (mode) {
        return (mode & 61440) === 40960;
      },isChrdev:function (mode) {
        return (mode & 61440) === 8192;
      },isBlkdev:function (mode) {
        return (mode & 61440) === 24576;
      },isFIFO:function (mode) {
        return (mode & 61440) === 4096;
      },isSocket:function (mode) {
        return (mode & 49152) === 49152;
      },flagModes:{"r":0,"rs":1052672,"r+":2,"w":577,"wx":705,"xw":705,"w+":578,"wx+":706,"xw+":706,"a":1089,"ax":1217,"xa":1217,"a+":1090,"ax+":1218,"xa+":1218},modeStringToFlags:function (str) {
        var flags = FS.flagModes[str];
        if (typeof flags === 'undefined') {
          throw new Error('Unknown file open mode: ' + str);
        }
        return flags;
      },flagsToPermissionString:function (flag) {
        var accmode = flag & 2097155;
        var perms = ['r', 'w', 'rw'][accmode];
        if ((flag & 512)) {
          perms += 'w';
        }
        return perms;
      },nodePermissions:function (node, perms) {
        if (FS.ignorePermissions) {
          return 0;
        }
        // return 0 if any user, group or owner bits are set.
        if (perms.indexOf('r') !== -1 && !(node.mode & 292)) {
          return ERRNO_CODES.EACCES;
        } else if (perms.indexOf('w') !== -1 && !(node.mode & 146)) {
          return ERRNO_CODES.EACCES;
        } else if (perms.indexOf('x') !== -1 && !(node.mode & 73)) {
          return ERRNO_CODES.EACCES;
        }
        return 0;
      },mayLookup:function (dir) {
        var err = FS.nodePermissions(dir, 'x');
        if (err) return err;
        if (!dir.node_ops.lookup) return ERRNO_CODES.EACCES;
        return 0;
      },mayCreate:function (dir, name) {
        try {
          var node = FS.lookupNode(dir, name);
          return ERRNO_CODES.EEXIST;
        } catch (e) {
        }
        return FS.nodePermissions(dir, 'wx');
      },mayDelete:function (dir, name, isdir) {
        var node;
        try {
          node = FS.lookupNode(dir, name);
        } catch (e) {
          return e.errno;
        }
        var err = FS.nodePermissions(dir, 'wx');
        if (err) {
          return err;
        }
        if (isdir) {
          if (!FS.isDir(node.mode)) {
            return ERRNO_CODES.ENOTDIR;
          }
          if (FS.isRoot(node) || FS.getPath(node) === FS.cwd()) {
            return ERRNO_CODES.EBUSY;
          }
        } else {
          if (FS.isDir(node.mode)) {
            return ERRNO_CODES.EISDIR;
          }
        }
        return 0;
      },mayOpen:function (node, flags) {
        if (!node) {
          return ERRNO_CODES.ENOENT;
        }
        if (FS.isLink(node.mode)) {
          return ERRNO_CODES.ELOOP;
        } else if (FS.isDir(node.mode)) {
          if ((flags & 2097155) !== 0 ||  // opening for write
              (flags & 512)) {
            return ERRNO_CODES.EISDIR;
          }
        }
        return FS.nodePermissions(node, FS.flagsToPermissionString(flags));
      },MAX_OPEN_FDS:4096,nextfd:function (fd_start, fd_end) {
        fd_start = fd_start || 0;
        fd_end = fd_end || FS.MAX_OPEN_FDS;
        for (var fd = fd_start; fd <= fd_end; fd++) {
          if (!FS.streams[fd]) {
            return fd;
          }
        }
        throw new FS.ErrnoError(ERRNO_CODES.EMFILE);
      },getStream:function (fd) {
        return FS.streams[fd];
      },createStream:function (stream, fd_start, fd_end) {
        if (!FS.FSStream) {
          FS.FSStream = function(){};
          FS.FSStream.prototype = {};
          // compatibility
          Object.defineProperties(FS.FSStream.prototype, {
            object: {
              get: function() { return this.node; },
              set: function(val) { this.node = val; }
            },
            isRead: {
              get: function() { return (this.flags & 2097155) !== 1; }
            },
            isWrite: {
              get: function() { return (this.flags & 2097155) !== 0; }
            },
            isAppend: {
              get: function() { return (this.flags & 1024); }
            }
          });
        }
        // clone it, so we can return an instance of FSStream
        var newStream = new FS.FSStream();
        for (var p in stream) {
          newStream[p] = stream[p];
        }
        stream = newStream;
        var fd = FS.nextfd(fd_start, fd_end);
        stream.fd = fd;
        FS.streams[fd] = stream;
        return stream;
      },closeStream:function (fd) {
        FS.streams[fd] = null;
      },getStreamFromPtr:function (ptr) {
        return FS.streams[ptr - 1];
      },getPtrForStream:function (stream) {
        return stream ? stream.fd + 1 : 0;
      },chrdev_stream_ops:{open:function (stream) {
          var device = FS.getDevice(stream.node.rdev);
          // override node's stream ops with the device's
          stream.stream_ops = device.stream_ops;
          // forward the open call
          if (stream.stream_ops.open) {
            stream.stream_ops.open(stream);
          }
        },llseek:function () {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }},major:function (dev) {
        return ((dev) >> 8);
      },minor:function (dev) {
        return ((dev) & 0xff);
      },makedev:function (ma, mi) {
        return ((ma) << 8 | (mi));
      },registerDevice:function (dev, ops) {
        FS.devices[dev] = { stream_ops: ops };
      },getDevice:function (dev) {
        return FS.devices[dev];
      },getMounts:function (mount) {
        var mounts = [];
        var check = [mount];
  
        while (check.length) {
          var m = check.pop();
  
          mounts.push(m);
  
          check.push.apply(check, m.mounts);
        }
  
        return mounts;
      },syncfs:function (populate, callback) {
        if (typeof(populate) === 'function') {
          callback = populate;
          populate = false;
        }
  
        var mounts = FS.getMounts(FS.root.mount);
        var completed = 0;
  
        function done(err) {
          if (err) {
            if (!done.errored) {
              done.errored = true;
              return callback(err);
            }
            return;
          }
          if (++completed >= mounts.length) {
            callback(null);
          }
        };
  
        // sync all mounts
        mounts.forEach(function (mount) {
          if (!mount.type.syncfs) {
            return done(null);
          }
          mount.type.syncfs(mount, populate, done);
        });
      },mount:function (type, opts, mountpoint) {
        var root = mountpoint === '/';
        var pseudo = !mountpoint;
        var node;
  
        if (root && FS.root) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        } else if (!root && !pseudo) {
          var lookup = FS.lookupPath(mountpoint, { follow_mount: false });
  
          mountpoint = lookup.path;  // use the absolute path
          node = lookup.node;
  
          if (FS.isMountpoint(node)) {
            throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
          }
  
          if (!FS.isDir(node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.ENOTDIR);
          }
        }
  
        var mount = {
          type: type,
          opts: opts,
          mountpoint: mountpoint,
          mounts: []
        };
  
        // create a root node for the fs
        var mountRoot = type.mount(mount);
        mountRoot.mount = mount;
        mount.root = mountRoot;
  
        if (root) {
          FS.root = mountRoot;
        } else if (node) {
          // set as a mountpoint
          node.mounted = mount;
  
          // add the new mount to the current mount's children
          if (node.mount) {
            node.mount.mounts.push(mount);
          }
        }
  
        return mountRoot;
      },unmount:function (mountpoint) {
        var lookup = FS.lookupPath(mountpoint, { follow_mount: false });
  
        if (!FS.isMountpoint(lookup.node)) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
  
        // destroy the nodes for this mount, and all its child mounts
        var node = lookup.node;
        var mount = node.mounted;
        var mounts = FS.getMounts(mount);
  
        Object.keys(FS.nameTable).forEach(function (hash) {
          var current = FS.nameTable[hash];
  
          while (current) {
            var next = current.name_next;
  
            if (mounts.indexOf(current.mount) !== -1) {
              FS.destroyNode(current);
            }
  
            current = next;
          }
        });
  
        // no longer a mountpoint
        node.mounted = null;
  
        // remove this mount from the child mounts
        var idx = node.mount.mounts.indexOf(mount);
        assert(idx !== -1);
        node.mount.mounts.splice(idx, 1);
      },lookup:function (parent, name) {
        return parent.node_ops.lookup(parent, name);
      },mknod:function (path, mode, dev) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        if (!name || name === '.' || name === '..') {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var err = FS.mayCreate(parent, name);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.mknod) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return parent.node_ops.mknod(parent, name, mode, dev);
      },create:function (path, mode) {
        mode = mode !== undefined ? mode : 438 /* 0666 */;
        mode &= 4095;
        mode |= 32768;
        return FS.mknod(path, mode, 0);
      },mkdir:function (path, mode) {
        mode = mode !== undefined ? mode : 511 /* 0777 */;
        mode &= 511 | 512;
        mode |= 16384;
        return FS.mknod(path, mode, 0);
      },mkdev:function (path, mode, dev) {
        if (typeof(dev) === 'undefined') {
          dev = mode;
          mode = 438 /* 0666 */;
        }
        mode |= 8192;
        return FS.mknod(path, mode, dev);
      },symlink:function (oldpath, newpath) {
        if (!PATH.resolve(oldpath)) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        var lookup = FS.lookupPath(newpath, { parent: true });
        var parent = lookup.node;
        if (!parent) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        var newname = PATH.basename(newpath);
        var err = FS.mayCreate(parent, newname);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.symlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return parent.node_ops.symlink(parent, newname, oldpath);
      },rename:function (old_path, new_path) {
        var old_dirname = PATH.dirname(old_path);
        var new_dirname = PATH.dirname(new_path);
        var old_name = PATH.basename(old_path);
        var new_name = PATH.basename(new_path);
        // parents must exist
        var lookup, old_dir, new_dir;
        try {
          lookup = FS.lookupPath(old_path, { parent: true });
          old_dir = lookup.node;
          lookup = FS.lookupPath(new_path, { parent: true });
          new_dir = lookup.node;
        } catch (e) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        if (!old_dir || !new_dir) throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        // need to be part of the same mount
        if (old_dir.mount !== new_dir.mount) {
          throw new FS.ErrnoError(ERRNO_CODES.EXDEV);
        }
        // source must exist
        var old_node = FS.lookupNode(old_dir, old_name);
        // old path should not be an ancestor of the new path
        var relative = PATH.relative(old_path, new_dirname);
        if (relative.charAt(0) !== '.') {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        // new path should not be an ancestor of the old path
        relative = PATH.relative(new_path, old_dirname);
        if (relative.charAt(0) !== '.') {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
        }
        // see if the new path already exists
        var new_node;
        try {
          new_node = FS.lookupNode(new_dir, new_name);
        } catch (e) {
          // not fatal
        }
        // early out if nothing needs to change
        if (old_node === new_node) {
          return;
        }
        // we'll need to delete the old entry
        var isdir = FS.isDir(old_node.mode);
        var err = FS.mayDelete(old_dir, old_name, isdir);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        // need delete permissions if we'll be overwriting.
        // need create permissions if new doesn't already exist.
        err = new_node ?
          FS.mayDelete(new_dir, new_name, isdir) :
          FS.mayCreate(new_dir, new_name);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!old_dir.node_ops.rename) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(old_node) || (new_node && FS.isMountpoint(new_node))) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        // if we are going to change the parent, check write permissions
        if (new_dir !== old_dir) {
          err = FS.nodePermissions(old_dir, 'w');
          if (err) {
            throw new FS.ErrnoError(err);
          }
        }
        try {
          if (FS.trackingDelegate['willMovePath']) {
            FS.trackingDelegate['willMovePath'](old_path, new_path);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['willMovePath']('"+old_path+"', '"+new_path+"') threw an exception: " + e.message);
        }
        // remove the node from the lookup hash
        FS.hashRemoveNode(old_node);
        // do the underlying fs rename
        try {
          old_dir.node_ops.rename(old_node, new_dir, new_name);
        } catch (e) {
          throw e;
        } finally {
          // add the node back to the hash (in case node_ops.rename
          // changed its name)
          FS.hashAddNode(old_node);
        }
        try {
          if (FS.trackingDelegate['onMovePath']) FS.trackingDelegate['onMovePath'](old_path, new_path);
        } catch(e) {
          console.log("FS.trackingDelegate['onMovePath']('"+old_path+"', '"+new_path+"') threw an exception: " + e.message);
        }
      },rmdir:function (path) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        var node = FS.lookupNode(parent, name);
        var err = FS.mayDelete(parent, name, true);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.rmdir) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(node)) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        try {
          if (FS.trackingDelegate['willDeletePath']) {
            FS.trackingDelegate['willDeletePath'](path);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['willDeletePath']('"+path+"') threw an exception: " + e.message);
        }
        parent.node_ops.rmdir(parent, name);
        FS.destroyNode(node);
        try {
          if (FS.trackingDelegate['onDeletePath']) FS.trackingDelegate['onDeletePath'](path);
        } catch(e) {
          console.log("FS.trackingDelegate['onDeletePath']('"+path+"') threw an exception: " + e.message);
        }
      },readdir:function (path) {
        var lookup = FS.lookupPath(path, { follow: true });
        var node = lookup.node;
        if (!node.node_ops.readdir) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTDIR);
        }
        return node.node_ops.readdir(node);
      },unlink:function (path) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        var node = FS.lookupNode(parent, name);
        var err = FS.mayDelete(parent, name, false);
        if (err) {
          // POSIX says unlink should set EPERM, not EISDIR
          if (err === ERRNO_CODES.EISDIR) err = ERRNO_CODES.EPERM;
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.unlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(node)) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        try {
          if (FS.trackingDelegate['willDeletePath']) {
            FS.trackingDelegate['willDeletePath'](path);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['willDeletePath']('"+path+"') threw an exception: " + e.message);
        }
        parent.node_ops.unlink(parent, name);
        FS.destroyNode(node);
        try {
          if (FS.trackingDelegate['onDeletePath']) FS.trackingDelegate['onDeletePath'](path);
        } catch(e) {
          console.log("FS.trackingDelegate['onDeletePath']('"+path+"') threw an exception: " + e.message);
        }
      },readlink:function (path) {
        var lookup = FS.lookupPath(path);
        var link = lookup.node;
        if (!link) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        if (!link.node_ops.readlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        return PATH.resolve(FS.getPath(lookup.node.parent), link.node_ops.readlink(link));
      },stat:function (path, dontFollow) {
        var lookup = FS.lookupPath(path, { follow: !dontFollow });
        var node = lookup.node;
        if (!node) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        if (!node.node_ops.getattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return node.node_ops.getattr(node);
      },lstat:function (path) {
        return FS.stat(path, true);
      },chmod:function (path, mode, dontFollow) {
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: !dontFollow });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        node.node_ops.setattr(node, {
          mode: (mode & 4095) | (node.mode & ~4095),
          timestamp: Date.now()
        });
      },lchmod:function (path, mode) {
        FS.chmod(path, mode, true);
      },fchmod:function (fd, mode) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        FS.chmod(stream.node, mode);
      },chown:function (path, uid, gid, dontFollow) {
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: !dontFollow });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        node.node_ops.setattr(node, {
          timestamp: Date.now()
          // we ignore the uid / gid for now
        });
      },lchown:function (path, uid, gid) {
        FS.chown(path, uid, gid, true);
      },fchown:function (fd, uid, gid) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        FS.chown(stream.node, uid, gid);
      },truncate:function (path, len) {
        if (len < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: true });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isDir(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!FS.isFile(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var err = FS.nodePermissions(node, 'w');
        if (err) {
          throw new FS.ErrnoError(err);
        }
        node.node_ops.setattr(node, {
          size: len,
          timestamp: Date.now()
        });
      },ftruncate:function (fd, len) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        FS.truncate(stream.node, len);
      },utime:function (path, atime, mtime) {
        var lookup = FS.lookupPath(path, { follow: true });
        var node = lookup.node;
        node.node_ops.setattr(node, {
          timestamp: Math.max(atime, mtime)
        });
      },open:function (path, flags, mode, fd_start, fd_end) {
        if (path === "") {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        flags = typeof flags === 'string' ? FS.modeStringToFlags(flags) : flags;
        mode = typeof mode === 'undefined' ? 438 /* 0666 */ : mode;
        if ((flags & 64)) {
          mode = (mode & 4095) | 32768;
        } else {
          mode = 0;
        }
        var node;
        if (typeof path === 'object') {
          node = path;
        } else {
          path = PATH.normalize(path);
          try {
            var lookup = FS.lookupPath(path, {
              follow: !(flags & 131072)
            });
            node = lookup.node;
          } catch (e) {
            // ignore
          }
        }
        // perhaps we need to create the node
        var created = false;
        if ((flags & 64)) {
          if (node) {
            // if O_CREAT and O_EXCL are set, error out if the node already exists
            if ((flags & 128)) {
              throw new FS.ErrnoError(ERRNO_CODES.EEXIST);
            }
          } else {
            // node doesn't exist, try to create it
            node = FS.mknod(path, mode, 0);
            created = true;
          }
        }
        if (!node) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        // can't truncate a device
        if (FS.isChrdev(node.mode)) {
          flags &= ~512;
        }
        // check permissions, if this is not a file we just created now (it is ok to
        // create and write to a file with read-only permissions; it is read-only
        // for later use)
        if (!created) {
          var err = FS.mayOpen(node, flags);
          if (err) {
            throw new FS.ErrnoError(err);
          }
        }
        // do truncation if necessary
        if ((flags & 512)) {
          FS.truncate(node, 0);
        }
        // we've already handled these, don't pass down to the underlying vfs
        flags &= ~(128 | 512);
  
        // register the stream with the filesystem
        var stream = FS.createStream({
          node: node,
          path: FS.getPath(node),  // we want the absolute path to the node
          flags: flags,
          seekable: true,
          position: 0,
          stream_ops: node.stream_ops,
          // used by the file family libc calls (fopen, fwrite, ferror, etc.)
          ungotten: [],
          error: false
        }, fd_start, fd_end);
        // call the new stream's open function
        if (stream.stream_ops.open) {
          stream.stream_ops.open(stream);
        }
        if (Module['logReadFiles'] && !(flags & 1)) {
          if (!FS.readFiles) FS.readFiles = {};
          if (!(path in FS.readFiles)) {
            FS.readFiles[path] = 1;
            Module['printErr']('read file: ' + path);
          }
        }
        try {
          if (FS.trackingDelegate['onOpenFile']) {
            var trackingFlags = 0;
            if ((flags & 2097155) !== 1) {
              trackingFlags |= FS.tracking.openFlags.READ;
            }
            if ((flags & 2097155) !== 0) {
              trackingFlags |= FS.tracking.openFlags.WRITE;
            }
            FS.trackingDelegate['onOpenFile'](path, trackingFlags);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['onOpenFile']('"+path+"', flags) threw an exception: " + e.message);
        }
        return stream;
      },close:function (stream) {
        try {
          if (stream.stream_ops.close) {
            stream.stream_ops.close(stream);
          }
        } catch (e) {
          throw e;
        } finally {
          FS.closeStream(stream.fd);
        }
      },llseek:function (stream, offset, whence) {
        if (!stream.seekable || !stream.stream_ops.llseek) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        stream.position = stream.stream_ops.llseek(stream, offset, whence);
        stream.ungotten = [];
        return stream.position;
      },read:function (stream, buffer, offset, length, position) {
        if (length < 0 || position < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 1) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (FS.isDir(stream.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!stream.stream_ops.read) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var seeking = true;
        if (typeof position === 'undefined') {
          position = stream.position;
          seeking = false;
        } else if (!stream.seekable) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        var bytesRead = stream.stream_ops.read(stream, buffer, offset, length, position);
        if (!seeking) stream.position += bytesRead;
        return bytesRead;
      },write:function (stream, buffer, offset, length, position, canOwn) {
        if (length < 0 || position < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (FS.isDir(stream.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!stream.stream_ops.write) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if (stream.flags & 1024) {
          // seek to the end before writing in append mode
          FS.llseek(stream, 0, 2);
        }
        var seeking = true;
        if (typeof position === 'undefined') {
          position = stream.position;
          seeking = false;
        } else if (!stream.seekable) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        var bytesWritten = stream.stream_ops.write(stream, buffer, offset, length, position, canOwn);
        if (!seeking) stream.position += bytesWritten;
        try {
          if (stream.path && FS.trackingDelegate['onWriteToFile']) FS.trackingDelegate['onWriteToFile'](stream.path);
        } catch(e) {
          console.log("FS.trackingDelegate['onWriteToFile']('"+path+"') threw an exception: " + e.message);
        }
        return bytesWritten;
      },allocate:function (stream, offset, length) {
        if (offset < 0 || length <= 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (!FS.isFile(stream.node.mode) && !FS.isDir(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
        }
        if (!stream.stream_ops.allocate) {
          throw new FS.ErrnoError(ERRNO_CODES.EOPNOTSUPP);
        }
        stream.stream_ops.allocate(stream, offset, length);
      },mmap:function (stream, buffer, offset, length, position, prot, flags) {
        // TODO if PROT is PROT_WRITE, make sure we have write access
        if ((stream.flags & 2097155) === 1) {
          throw new FS.ErrnoError(ERRNO_CODES.EACCES);
        }
        if (!stream.stream_ops.mmap) {
          throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
        }
        return stream.stream_ops.mmap(stream, buffer, offset, length, position, prot, flags);
      },ioctl:function (stream, cmd, arg) {
        if (!stream.stream_ops.ioctl) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTTY);
        }
        return stream.stream_ops.ioctl(stream, cmd, arg);
      },readFile:function (path, opts) {
        opts = opts || {};
        opts.flags = opts.flags || 'r';
        opts.encoding = opts.encoding || 'binary';
        if (opts.encoding !== 'utf8' && opts.encoding !== 'binary') {
          throw new Error('Invalid encoding type "' + opts.encoding + '"');
        }
        var ret;
        var stream = FS.open(path, opts.flags);
        var stat = FS.stat(path);
        var length = stat.size;
        var buf = new Uint8Array(length);
        FS.read(stream, buf, 0, length, 0);
        if (opts.encoding === 'utf8') {
          ret = UTF8ArrayToString(buf, 0);
        } else if (opts.encoding === 'binary') {
          ret = buf;
        }
        FS.close(stream);
        return ret;
      },writeFile:function (path, data, opts) {
        opts = opts || {};
        opts.flags = opts.flags || 'w';
        opts.encoding = opts.encoding || 'utf8';
        if (opts.encoding !== 'utf8' && opts.encoding !== 'binary') {
          throw new Error('Invalid encoding type "' + opts.encoding + '"');
        }
        var stream = FS.open(path, opts.flags, opts.mode);
        if (opts.encoding === 'utf8') {
          var buf = new Uint8Array(lengthBytesUTF8(data)+1);
          var actualNumBytes = stringToUTF8Array(data, buf, 0, buf.length);
          FS.write(stream, buf, 0, actualNumBytes, 0, opts.canOwn);
        } else if (opts.encoding === 'binary') {
          FS.write(stream, data, 0, data.length, 0, opts.canOwn);
        }
        FS.close(stream);
      },cwd:function () {
        return FS.currentPath;
      },chdir:function (path) {
        var lookup = FS.lookupPath(path, { follow: true });
        if (!FS.isDir(lookup.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTDIR);
        }
        var err = FS.nodePermissions(lookup.node, 'x');
        if (err) {
          throw new FS.ErrnoError(err);
        }
        FS.currentPath = lookup.path;
      },createDefaultDirectories:function () {
        FS.mkdir('/tmp');
        FS.mkdir('/home');
        FS.mkdir('/home/web_user');
      },createDefaultDevices:function () {
        // create /dev
        FS.mkdir('/dev');
        // setup /dev/null
        FS.registerDevice(FS.makedev(1, 3), {
          read: function() { return 0; },
          write: function() { return 0; }
        });
        FS.mkdev('/dev/null', FS.makedev(1, 3));
        // setup /dev/tty and /dev/tty1
        // stderr needs to print output using Module['printErr']
        // so we register a second tty just for it.
        TTY.register(FS.makedev(5, 0), TTY.default_tty_ops);
        TTY.register(FS.makedev(6, 0), TTY.default_tty1_ops);
        FS.mkdev('/dev/tty', FS.makedev(5, 0));
        FS.mkdev('/dev/tty1', FS.makedev(6, 0));
        // setup /dev/[u]random
        var random_device;
        if (typeof crypto !== 'undefined') {
          // for modern web browsers
          var randomBuffer = new Uint8Array(1);
          random_device = function() { crypto.getRandomValues(randomBuffer); return randomBuffer[0]; };
        } else if (ENVIRONMENT_IS_NODE) {
          // for nodejs
          random_device = function() { return require('crypto').randomBytes(1)[0]; };
        } else {
          // default for ES5 platforms
          random_device = function() { return (Math.random()*256)|0; };
        }
        FS.createDevice('/dev', 'random', random_device);
        FS.createDevice('/dev', 'urandom', random_device);
        // we're not going to emulate the actual shm device,
        // just create the tmp dirs that reside in it commonly
        FS.mkdir('/dev/shm');
        FS.mkdir('/dev/shm/tmp');
      },createStandardStreams:function () {
        // TODO deprecate the old functionality of a single
        // input / output callback and that utilizes FS.createDevice
        // and instead require a unique set of stream ops
  
        // by default, we symlink the standard streams to the
        // default tty devices. however, if the standard streams
        // have been overwritten we create a unique device for
        // them instead.
        if (Module['stdin']) {
          FS.createDevice('/dev', 'stdin', Module['stdin']);
        } else {
          FS.symlink('/dev/tty', '/dev/stdin');
        }
        if (Module['stdout']) {
          FS.createDevice('/dev', 'stdout', null, Module['stdout']);
        } else {
          FS.symlink('/dev/tty', '/dev/stdout');
        }
        if (Module['stderr']) {
          FS.createDevice('/dev', 'stderr', null, Module['stderr']);
        } else {
          FS.symlink('/dev/tty1', '/dev/stderr');
        }
  
        // open default streams for the stdin, stdout and stderr devices
        var stdin = FS.open('/dev/stdin', 'r');
        HEAP32[((_stdin)>>2)]=FS.getPtrForStream(stdin);
        assert(stdin.fd === 0, 'invalid handle for stdin (' + stdin.fd + ')');
  
        var stdout = FS.open('/dev/stdout', 'w');
        HEAP32[((_stdout)>>2)]=FS.getPtrForStream(stdout);
        assert(stdout.fd === 1, 'invalid handle for stdout (' + stdout.fd + ')');
  
        var stderr = FS.open('/dev/stderr', 'w');
        HEAP32[((_stderr)>>2)]=FS.getPtrForStream(stderr);
        assert(stderr.fd === 2, 'invalid handle for stderr (' + stderr.fd + ')');
      },ensureErrnoError:function () {
        if (FS.ErrnoError) return;
        FS.ErrnoError = function ErrnoError(errno, node) {
          this.node = node;
          this.setErrno = function(errno) {
            this.errno = errno;
            for (var key in ERRNO_CODES) {
              if (ERRNO_CODES[key] === errno) {
                this.code = key;
                break;
              }
            }
          };
          this.setErrno(errno);
          this.message = ERRNO_MESSAGES[errno];
          if (this.stack) this.stack = demangleAll(this.stack);
        };
        FS.ErrnoError.prototype = new Error();
        FS.ErrnoError.prototype.constructor = FS.ErrnoError;
        // Some errors may happen quite a bit, to avoid overhead we reuse them (and suffer a lack of stack info)
        [ERRNO_CODES.ENOENT].forEach(function(code) {
          FS.genericErrors[code] = new FS.ErrnoError(code);
          FS.genericErrors[code].stack = '<generic error, no stack>';
        });
      },staticInit:function () {
        FS.ensureErrnoError();
  
        FS.nameTable = new Array(4096);
  
        FS.mount(MEMFS, {}, '/');
  
        FS.createDefaultDirectories();
        FS.createDefaultDevices();
      },init:function (input, output, error) {
        assert(!FS.init.initialized, 'FS.init was previously called. If you want to initialize later with custom parameters, remove any earlier calls (note that one is automatically added to the generated code)');
        FS.init.initialized = true;
  
        FS.ensureErrnoError();
  
        // Allow Module.stdin etc. to provide defaults, if none explicitly passed to us here
        Module['stdin'] = input || Module['stdin'];
        Module['stdout'] = output || Module['stdout'];
        Module['stderr'] = error || Module['stderr'];
  
        FS.createStandardStreams();
      },quit:function () {
        FS.init.initialized = false;
        for (var i = 0; i < FS.streams.length; i++) {
          var stream = FS.streams[i];
          if (!stream) {
            continue;
          }
          FS.close(stream);
        }
      },getMode:function (canRead, canWrite) {
        var mode = 0;
        if (canRead) mode |= 292 | 73;
        if (canWrite) mode |= 146;
        return mode;
      },joinPath:function (parts, forceRelative) {
        var path = PATH.join.apply(null, parts);
        if (forceRelative && path[0] == '/') path = path.substr(1);
        return path;
      },absolutePath:function (relative, base) {
        return PATH.resolve(base, relative);
      },standardizePath:function (path) {
        return PATH.normalize(path);
      },findObject:function (path, dontResolveLastLink) {
        var ret = FS.analyzePath(path, dontResolveLastLink);
        if (ret.exists) {
          return ret.object;
        } else {
          ___setErrNo(ret.error);
          return null;
        }
      },analyzePath:function (path, dontResolveLastLink) {
        // operate from within the context of the symlink's target
        try {
          var lookup = FS.lookupPath(path, { follow: !dontResolveLastLink });
          path = lookup.path;
        } catch (e) {
        }
        var ret = {
          isRoot: false, exists: false, error: 0, name: null, path: null, object: null,
          parentExists: false, parentPath: null, parentObject: null
        };
        try {
          var lookup = FS.lookupPath(path, { parent: true });
          ret.parentExists = true;
          ret.parentPath = lookup.path;
          ret.parentObject = lookup.node;
          ret.name = PATH.basename(path);
          lookup = FS.lookupPath(path, { follow: !dontResolveLastLink });
          ret.exists = true;
          ret.path = lookup.path;
          ret.object = lookup.node;
          ret.name = lookup.node.name;
          ret.isRoot = lookup.path === '/';
        } catch (e) {
          ret.error = e.errno;
        };
        return ret;
      },createFolder:function (parent, name, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(canRead, canWrite);
        return FS.mkdir(path, mode);
      },createPath:function (parent, path, canRead, canWrite) {
        parent = typeof parent === 'string' ? parent : FS.getPath(parent);
        var parts = path.split('/').reverse();
        while (parts.length) {
          var part = parts.pop();
          if (!part) continue;
          var current = PATH.join2(parent, part);
          try {
            FS.mkdir(current);
          } catch (e) {
            // ignore EEXIST
          }
          parent = current;
        }
        return current;
      },createFile:function (parent, name, properties, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(canRead, canWrite);
        return FS.create(path, mode);
      },createDataFile:function (parent, name, data, canRead, canWrite, canOwn) {
        var path = name ? PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name) : parent;
        var mode = FS.getMode(canRead, canWrite);
        var node = FS.create(path, mode);
        if (data) {
          if (typeof data === 'string') {
            var arr = new Array(data.length);
            for (var i = 0, len = data.length; i < len; ++i) arr[i] = data.charCodeAt(i);
            data = arr;
          }
          // make sure we can write to the file
          FS.chmod(node, mode | 146);
          var stream = FS.open(node, 'w');
          FS.write(stream, data, 0, data.length, 0, canOwn);
          FS.close(stream);
          FS.chmod(node, mode);
        }
        return node;
      },createDevice:function (parent, name, input, output) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(!!input, !!output);
        if (!FS.createDevice.major) FS.createDevice.major = 64;
        var dev = FS.makedev(FS.createDevice.major++, 0);
        // Create a fake device that a set of stream ops to emulate
        // the old behavior.
        FS.registerDevice(dev, {
          open: function(stream) {
            stream.seekable = false;
          },
          close: function(stream) {
            // flush any pending line data
            if (output && output.buffer && output.buffer.length) {
              output(10);
            }
          },
          read: function(stream, buffer, offset, length, pos /* ignored */) {
            var bytesRead = 0;
            for (var i = 0; i < length; i++) {
              var result;
              try {
                result = input();
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES.EIO);
              }
              if (result === undefined && bytesRead === 0) {
                throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
              }
              if (result === null || result === undefined) break;
              bytesRead++;
              buffer[offset+i] = result;
            }
            if (bytesRead) {
              stream.node.timestamp = Date.now();
            }
            return bytesRead;
          },
          write: function(stream, buffer, offset, length, pos) {
            for (var i = 0; i < length; i++) {
              try {
                output(buffer[offset+i]);
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES.EIO);
              }
            }
            if (length) {
              stream.node.timestamp = Date.now();
            }
            return i;
          }
        });
        return FS.mkdev(path, mode, dev);
      },createLink:function (parent, name, target, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        return FS.symlink(target, path);
      },forceLoadFile:function (obj) {
        if (obj.isDevice || obj.isFolder || obj.link || obj.contents) return true;
        var success = true;
        if (typeof XMLHttpRequest !== 'undefined') {
          throw new Error("Lazy loading should have been performed (contents set) in createLazyFile, but it was not. Lazy loading only works in web workers. Use --embed-file or --preload-file in emcc on the main thread.");
        } else if (Module['read']) {
          // Command-line.
          try {
            // WARNING: Can't read binary files in V8's d8 or tracemonkey's js, as
            //          read() will try to parse UTF8.
            obj.contents = intArrayFromString(Module['read'](obj.url), true);
            obj.usedBytes = obj.contents.length;
          } catch (e) {
            success = false;
          }
        } else {
          throw new Error('Cannot load without read() or XMLHttpRequest.');
        }
        if (!success) ___setErrNo(ERRNO_CODES.EIO);
        return success;
      },createLazyFile:function (parent, name, url, canRead, canWrite) {
        // Lazy chunked Uint8Array (implements get and length from Uint8Array). Actual getting is abstracted away for eventual reuse.
        function LazyUint8Array() {
          this.lengthKnown = false;
          this.chunks = []; // Loaded chunks. Index is the chunk number
        }
        LazyUint8Array.prototype.get = function LazyUint8Array_get(idx) {
          if (idx > this.length-1 || idx < 0) {
            return undefined;
          }
          var chunkOffset = idx % this.chunkSize;
          var chunkNum = (idx / this.chunkSize)|0;
          return this.getter(chunkNum)[chunkOffset];
        }
        LazyUint8Array.prototype.setDataGetter = function LazyUint8Array_setDataGetter(getter) {
          this.getter = getter;
        }
        LazyUint8Array.prototype.cacheLength = function LazyUint8Array_cacheLength() {
          // Find length
          var xhr = new XMLHttpRequest();
          xhr.open('HEAD', url, false);
          xhr.send(null);
          if (!(xhr.status >= 200 && xhr.status < 300 || xhr.status === 304)) throw new Error("Couldn't load " + url + ". Status: " + xhr.status);
          var datalength = Number(xhr.getResponseHeader("Content-length"));
          var header;
          var hasByteServing = (header = xhr.getResponseHeader("Accept-Ranges")) && header === "bytes";
          var chunkSize = 1024*1024; // Chunk size in bytes
  
          if (!hasByteServing) chunkSize = datalength;
  
          // Function to get a range from the remote URL.
          var doXHR = (function(from, to) {
            if (from > to) throw new Error("invalid range (" + from + ", " + to + ") or no bytes requested!");
            if (to > datalength-1) throw new Error("only " + datalength + " bytes available! programmer error!");
  
            // TODO: Use mozResponseArrayBuffer, responseStream, etc. if available.
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, false);
            if (datalength !== chunkSize) xhr.setRequestHeader("Range", "bytes=" + from + "-" + to);
  
            // Some hints to the browser that we want binary data.
            if (typeof Uint8Array != 'undefined') xhr.responseType = 'arraybuffer';
            if (xhr.overrideMimeType) {
              xhr.overrideMimeType('text/plain; charset=x-user-defined');
            }
  
            xhr.send(null);
            if (!(xhr.status >= 200 && xhr.status < 300 || xhr.status === 304)) throw new Error("Couldn't load " + url + ". Status: " + xhr.status);
            if (xhr.response !== undefined) {
              return new Uint8Array(xhr.response || []);
            } else {
              return intArrayFromString(xhr.responseText || '', true);
            }
          });
          var lazyArray = this;
          lazyArray.setDataGetter(function(chunkNum) {
            var start = chunkNum * chunkSize;
            var end = (chunkNum+1) * chunkSize - 1; // including this byte
            end = Math.min(end, datalength-1); // if datalength-1 is selected, this is the last block
            if (typeof(lazyArray.chunks[chunkNum]) === "undefined") {
              lazyArray.chunks[chunkNum] = doXHR(start, end);
            }
            if (typeof(lazyArray.chunks[chunkNum]) === "undefined") throw new Error("doXHR failed!");
            return lazyArray.chunks[chunkNum];
          });
  
          this._length = datalength;
          this._chunkSize = chunkSize;
          this.lengthKnown = true;
        }
        if (typeof XMLHttpRequest !== 'undefined') {
          if (!ENVIRONMENT_IS_WORKER) throw 'Cannot do synchronous binary XHRs outside webworkers in modern browsers. Use --embed-file or --preload-file in emcc';
          var lazyArray = new LazyUint8Array();
          Object.defineProperty(lazyArray, "length", {
              get: function() {
                  if(!this.lengthKnown) {
                      this.cacheLength();
                  }
                  return this._length;
              }
          });
          Object.defineProperty(lazyArray, "chunkSize", {
              get: function() {
                  if(!this.lengthKnown) {
                      this.cacheLength();
                  }
                  return this._chunkSize;
              }
          });
  
          var properties = { isDevice: false, contents: lazyArray };
        } else {
          var properties = { isDevice: false, url: url };
        }
  
        var node = FS.createFile(parent, name, properties, canRead, canWrite);
        // This is a total hack, but I want to get this lazy file code out of the
        // core of MEMFS. If we want to keep this lazy file concept I feel it should
        // be its own thin LAZYFS proxying calls to MEMFS.
        if (properties.contents) {
          node.contents = properties.contents;
        } else if (properties.url) {
          node.contents = null;
          node.url = properties.url;
        }
        // Add a function that defers querying the file size until it is asked the first time.
        Object.defineProperty(node, "usedBytes", {
            get: function() { return this.contents.length; }
        });
        // override each stream op with one that tries to force load the lazy file first
        var stream_ops = {};
        var keys = Object.keys(node.stream_ops);
        keys.forEach(function(key) {
          var fn = node.stream_ops[key];
          stream_ops[key] = function forceLoadLazyFile() {
            if (!FS.forceLoadFile(node)) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
            return fn.apply(null, arguments);
          };
        });
        // use a custom read function
        stream_ops.read = function stream_ops_read(stream, buffer, offset, length, position) {
          if (!FS.forceLoadFile(node)) {
            throw new FS.ErrnoError(ERRNO_CODES.EIO);
          }
          var contents = stream.node.contents;
          if (position >= contents.length)
            return 0;
          var size = Math.min(contents.length - position, length);
          assert(size >= 0);
          if (contents.slice) { // normal array
            for (var i = 0; i < size; i++) {
              buffer[offset + i] = contents[position + i];
            }
          } else {
            for (var i = 0; i < size; i++) { // LazyUint8Array from sync binary XHR
              buffer[offset + i] = contents.get(position + i);
            }
          }
          return size;
        };
        node.stream_ops = stream_ops;
        return node;
      },createPreloadedFile:function (parent, name, url, canRead, canWrite, onload, onerror, dontCreateFile, canOwn) {
        Browser.init();
        // TODO we should allow people to just pass in a complete filename instead
        // of parent and name being that we just join them anyways
        var fullname = name ? PATH.resolve(PATH.join2(parent, name)) : parent;
        function processData(byteArray) {
          function finish(byteArray) {
            if (!dontCreateFile) {
              FS.createDataFile(parent, name, byteArray, canRead, canWrite, canOwn);
            }
            if (onload) onload();
            removeRunDependency('cp ' + fullname);
          }
          var handled = false;
          Module['preloadPlugins'].forEach(function(plugin) {
            if (handled) return;
            if (plugin['canHandle'](fullname)) {
              plugin['handle'](byteArray, fullname, finish, function() {
                if (onerror) onerror();
                removeRunDependency('cp ' + fullname);
              });
              handled = true;
            }
          });
          if (!handled) finish(byteArray);
        }
        addRunDependency('cp ' + fullname);
        if (typeof url == 'string') {
          Browser.asyncLoad(url, function(byteArray) {
            processData(byteArray);
          }, onerror);
        } else {
          processData(url);
        }
      },indexedDB:function () {
        return window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
      },DB_NAME:function () {
        return 'EM_FS_' + window.location.pathname;
      },DB_VERSION:20,DB_STORE_NAME:"FILE_DATA",saveFilesToDB:function (paths, onload, onerror) {
        onload = onload || function(){};
        onerror = onerror || function(){};
        var indexedDB = FS.indexedDB();
        try {
          var openRequest = indexedDB.open(FS.DB_NAME(), FS.DB_VERSION);
        } catch (e) {
          return onerror(e);
        }
        openRequest.onupgradeneeded = function openRequest_onupgradeneeded() {
          console.log('creating db');
          var db = openRequest.result;
          db.createObjectStore(FS.DB_STORE_NAME);
        };
        openRequest.onsuccess = function openRequest_onsuccess() {
          var db = openRequest.result;
          var transaction = db.transaction([FS.DB_STORE_NAME], 'readwrite');
          var files = transaction.objectStore(FS.DB_STORE_NAME);
          var ok = 0, fail = 0, total = paths.length;
          function finish() {
            if (fail == 0) onload(); else onerror();
          }
          paths.forEach(function(path) {
            var putRequest = files.put(FS.analyzePath(path).object.contents, path);
            putRequest.onsuccess = function putRequest_onsuccess() { ok++; if (ok + fail == total) finish() };
            putRequest.onerror = function putRequest_onerror() { fail++; if (ok + fail == total) finish() };
          });
          transaction.onerror = onerror;
        };
        openRequest.onerror = onerror;
      },loadFilesFromDB:function (paths, onload, onerror) {
        onload = onload || function(){};
        onerror = onerror || function(){};
        var indexedDB = FS.indexedDB();
        try {
          var openRequest = indexedDB.open(FS.DB_NAME(), FS.DB_VERSION);
        } catch (e) {
          return onerror(e);
        }
        openRequest.onupgradeneeded = onerror; // no database to load from
        openRequest.onsuccess = function openRequest_onsuccess() {
          var db = openRequest.result;
          try {
            var transaction = db.transaction([FS.DB_STORE_NAME], 'readonly');
          } catch(e) {
            onerror(e);
            return;
          }
          var files = transaction.objectStore(FS.DB_STORE_NAME);
          var ok = 0, fail = 0, total = paths.length;
          function finish() {
            if (fail == 0) onload(); else onerror();
          }
          paths.forEach(function(path) {
            var getRequest = files.get(path);
            getRequest.onsuccess = function getRequest_onsuccess() {
              if (FS.analyzePath(path).exists) {
                FS.unlink(path);
              }
              FS.createDataFile(PATH.dirname(path), PATH.basename(path), getRequest.result, true, true, true);
              ok++;
              if (ok + fail == total) finish();
            };
            getRequest.onerror = function getRequest_onerror() { fail++; if (ok + fail == total) finish() };
          });
          transaction.onerror = onerror;
        };
        openRequest.onerror = onerror;
      }};
  
  
  
  
  function _mkport() { throw 'TODO' }var SOCKFS={mount:function (mount) {
        // If Module['websocket'] has already been defined (e.g. for configuring
        // the subprotocol/url) use that, if not initialise it to a new object.
        Module['websocket'] = (Module['websocket'] && 
                               ('object' === typeof Module['websocket'])) ? Module['websocket'] : {};
  
        // Add the Event registration mechanism to the exported websocket configuration
        // object so we can register network callbacks from native JavaScript too.
        // For more documentation see system/include/emscripten/emscripten.h
        Module['websocket']._callbacks = {};
        Module['websocket']['on'] = function(event, callback) {
  	    if ('function' === typeof callback) {
  		  this._callbacks[event] = callback;
          }
  	    return this;
        };
  
        Module['websocket'].emit = function(event, param) {
  	    if ('function' === typeof this._callbacks[event]) {
  		  this._callbacks[event].call(this, param);
          }
        };
  
        // If debug is enabled register simple default logging callbacks for each Event.
  
        return FS.createNode(null, '/', 16384 | 511 /* 0777 */, 0);
      },createSocket:function (family, type, protocol) {
        var streaming = type == 1;
        if (protocol) {
          assert(streaming == (protocol == 6)); // if SOCK_STREAM, must be tcp
        }
  
        // create our internal socket structure
        var sock = {
          family: family,
          type: type,
          protocol: protocol,
          server: null,
          error: null, // Used in getsockopt for SOL_SOCKET/SO_ERROR test
          peers: {},
          pending: [],
          recv_queue: [],
          sock_ops: SOCKFS.websocket_sock_ops
        };
  
        // create the filesystem node to store the socket structure
        var name = SOCKFS.nextname();
        var node = FS.createNode(SOCKFS.root, name, 49152, 0);
        node.sock = sock;
  
        // and the wrapping stream that enables library functions such
        // as read and write to indirectly interact with the socket
        var stream = FS.createStream({
          path: name,
          node: node,
          flags: FS.modeStringToFlags('r+'),
          seekable: false,
          stream_ops: SOCKFS.stream_ops
        });
  
        // map the new stream to the socket structure (sockets have a 1:1
        // relationship with a stream)
        sock.stream = stream;
  
        return sock;
      },getSocket:function (fd) {
        var stream = FS.getStream(fd);
        if (!stream || !FS.isSocket(stream.node.mode)) {
          return null;
        }
        return stream.node.sock;
      },stream_ops:{poll:function (stream) {
          var sock = stream.node.sock;
          return sock.sock_ops.poll(sock);
        },ioctl:function (stream, request, varargs) {
          var sock = stream.node.sock;
          return sock.sock_ops.ioctl(sock, request, varargs);
        },read:function (stream, buffer, offset, length, position /* ignored */) {
          var sock = stream.node.sock;
          var msg = sock.sock_ops.recvmsg(sock, length);
          if (!msg) {
            // socket is closed
            return 0;
          }
          buffer.set(msg.buffer, offset);
          return msg.buffer.length;
        },write:function (stream, buffer, offset, length, position /* ignored */) {
          var sock = stream.node.sock;
          return sock.sock_ops.sendmsg(sock, buffer, offset, length);
        },close:function (stream) {
          var sock = stream.node.sock;
          sock.sock_ops.close(sock);
        }},nextname:function () {
        if (!SOCKFS.nextname.current) {
          SOCKFS.nextname.current = 0;
        }
        return 'socket[' + (SOCKFS.nextname.current++) + ']';
      },websocket_sock_ops:{createPeer:function (sock, addr, port) {
          var ws;
  
          if (typeof addr === 'object') {
            ws = addr;
            addr = null;
            port = null;
          }
  
          if (ws) {
            // for sockets that've already connected (e.g. we're the server)
            // we can inspect the _socket property for the address
            if (ws._socket) {
              addr = ws._socket.remoteAddress;
              port = ws._socket.remotePort;
            }
            // if we're just now initializing a connection to the remote,
            // inspect the url property
            else {
              var result = /ws[s]?:\/\/([^:]+):(\d+)/.exec(ws.url);
              if (!result) {
                throw new Error('WebSocket URL must be in the format ws(s)://address:port');
              }
              addr = result[1];
              port = parseInt(result[2], 10);
            }
          } else {
            // create the actual websocket object and connect
            try {
              // runtimeConfig gets set to true if WebSocket runtime configuration is available.
              var runtimeConfig = (Module['websocket'] && ('object' === typeof Module['websocket']));
  
              // The default value is 'ws://' the replace is needed because the compiler replaces '//' comments with '#'
              // comments without checking context, so we'd end up with ws:#, the replace swaps the '#' for '//' again.
              var url = 'ws:#'.replace('#', '//');
  
              if (runtimeConfig) {
                if ('string' === typeof Module['websocket']['url']) {
                  url = Module['websocket']['url']; // Fetch runtime WebSocket URL config.
                }
              }
  
              if (url === 'ws://' || url === 'wss://') { // Is the supplied URL config just a prefix, if so complete it.
                var parts = addr.split('/');
                url = url + parts[0] + ":" + port + "/" + parts.slice(1).join('/');
              }
  
              // Make the WebSocket subprotocol (Sec-WebSocket-Protocol) default to binary if no configuration is set.
              var subProtocols = 'binary'; // The default value is 'binary'
  
              if (runtimeConfig) {
                if ('string' === typeof Module['websocket']['subprotocol']) {
                  subProtocols = Module['websocket']['subprotocol']; // Fetch runtime WebSocket subprotocol config.
                }
              }
  
              // The regex trims the string (removes spaces at the beginning and end, then splits the string by
              // <any space>,<any space> into an Array. Whitespace removal is important for Websockify and ws.
              subProtocols = subProtocols.replace(/^ +| +$/g,"").split(/ *, */);
  
              // The node ws library API for specifying optional subprotocol is slightly different than the browser's.
              var opts = ENVIRONMENT_IS_NODE ? {'protocol': subProtocols.toString()} : subProtocols;
  
              // If node we use the ws library.
              var WebSocket = ENVIRONMENT_IS_NODE ? require('ws') : window['WebSocket'];
              ws = new WebSocket(url, opts);
              ws.binaryType = 'arraybuffer';
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EHOSTUNREACH);
            }
          }
  
  
          var peer = {
            addr: addr,
            port: port,
            socket: ws,
            dgram_send_queue: []
          };
  
          SOCKFS.websocket_sock_ops.addPeer(sock, peer);
          SOCKFS.websocket_sock_ops.handlePeerEvents(sock, peer);
  
          // if this is a bound dgram socket, send the port number first to allow
          // us to override the ephemeral port reported to us by remotePort on the
          // remote end.
          if (sock.type === 2 && typeof sock.sport !== 'undefined') {
            peer.dgram_send_queue.push(new Uint8Array([
                255, 255, 255, 255,
                'p'.charCodeAt(0), 'o'.charCodeAt(0), 'r'.charCodeAt(0), 't'.charCodeAt(0),
                ((sock.sport & 0xff00) >> 8) , (sock.sport & 0xff)
            ]));
          }
  
          return peer;
        },getPeer:function (sock, addr, port) {
          return sock.peers[addr + ':' + port];
        },addPeer:function (sock, peer) {
          sock.peers[peer.addr + ':' + peer.port] = peer;
        },removePeer:function (sock, peer) {
          delete sock.peers[peer.addr + ':' + peer.port];
        },handlePeerEvents:function (sock, peer) {
          var first = true;
  
          var handleOpen = function () {
  
            Module['websocket'].emit('open', sock.stream.fd);
  
            try {
              var queued = peer.dgram_send_queue.shift();
              while (queued) {
                peer.socket.send(queued);
                queued = peer.dgram_send_queue.shift();
              }
            } catch (e) {
              // not much we can do here in the way of proper error handling as we've already
              // lied and said this data was sent. shut it down.
              peer.socket.close();
            }
          };
  
          function handleMessage(data) {
            assert(typeof data !== 'string' && data.byteLength !== undefined);  // must receive an ArrayBuffer
            data = new Uint8Array(data);  // make a typed array view on the array buffer
  
  
            // if this is the port message, override the peer's port with it
            var wasfirst = first;
            first = false;
            if (wasfirst &&
                data.length === 10 &&
                data[0] === 255 && data[1] === 255 && data[2] === 255 && data[3] === 255 &&
                data[4] === 'p'.charCodeAt(0) && data[5] === 'o'.charCodeAt(0) && data[6] === 'r'.charCodeAt(0) && data[7] === 't'.charCodeAt(0)) {
              // update the peer's port and it's key in the peer map
              var newport = ((data[8] << 8) | data[9]);
              SOCKFS.websocket_sock_ops.removePeer(sock, peer);
              peer.port = newport;
              SOCKFS.websocket_sock_ops.addPeer(sock, peer);
              return;
            }
  
            sock.recv_queue.push({ addr: peer.addr, port: peer.port, data: data });
            Module['websocket'].emit('message', sock.stream.fd);
          };
  
          if (ENVIRONMENT_IS_NODE) {
            peer.socket.on('open', handleOpen);
            peer.socket.on('message', function(data, flags) {
              if (!flags.binary) {
                return;
              }
              handleMessage((new Uint8Array(data)).buffer);  // copy from node Buffer -> ArrayBuffer
            });
            peer.socket.on('close', function() {
              Module['websocket'].emit('close', sock.stream.fd);
            });
            peer.socket.on('error', function(error) {
              // Although the ws library may pass errors that may be more descriptive than
              // ECONNREFUSED they are not necessarily the expected error code e.g. 
              // ENOTFOUND on getaddrinfo seems to be node.js specific, so using ECONNREFUSED
              // is still probably the most useful thing to do.
              sock.error = ERRNO_CODES.ECONNREFUSED; // Used in getsockopt for SOL_SOCKET/SO_ERROR test.
              Module['websocket'].emit('error', [sock.stream.fd, sock.error, 'ECONNREFUSED: Connection refused']);
              // don't throw
            });
          } else {
            peer.socket.onopen = handleOpen;
            peer.socket.onclose = function() {
              Module['websocket'].emit('close', sock.stream.fd);
            };
            peer.socket.onmessage = function peer_socket_onmessage(event) {
              handleMessage(event.data);
            };
            peer.socket.onerror = function(error) {
              // The WebSocket spec only allows a 'simple event' to be thrown on error,
              // so we only really know as much as ECONNREFUSED.
              sock.error = ERRNO_CODES.ECONNREFUSED; // Used in getsockopt for SOL_SOCKET/SO_ERROR test.
              Module['websocket'].emit('error', [sock.stream.fd, sock.error, 'ECONNREFUSED: Connection refused']);
            };
          }
        },poll:function (sock) {
          if (sock.type === 1 && sock.server) {
            // listen sockets should only say they're available for reading
            // if there are pending clients.
            return sock.pending.length ? (64 | 1) : 0;
          }
  
          var mask = 0;
          var dest = sock.type === 1 ?  // we only care about the socket state for connection-based sockets
            SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport) :
            null;
  
          if (sock.recv_queue.length ||
              !dest ||  // connection-less sockets are always ready to read
              (dest && dest.socket.readyState === dest.socket.CLOSING) ||
              (dest && dest.socket.readyState === dest.socket.CLOSED)) {  // let recv return 0 once closed
            mask |= (64 | 1);
          }
  
          if (!dest ||  // connection-less sockets are always ready to write
              (dest && dest.socket.readyState === dest.socket.OPEN)) {
            mask |= 4;
          }
  
          if ((dest && dest.socket.readyState === dest.socket.CLOSING) ||
              (dest && dest.socket.readyState === dest.socket.CLOSED)) {
            mask |= 16;
          }
  
          return mask;
        },ioctl:function (sock, request, arg) {
          switch (request) {
            case 21531:
              var bytes = 0;
              if (sock.recv_queue.length) {
                bytes = sock.recv_queue[0].data.length;
              }
              HEAP32[((arg)>>2)]=bytes;
              return 0;
            default:
              return ERRNO_CODES.EINVAL;
          }
        },close:function (sock) {
          // if we've spawned a listen server, close it
          if (sock.server) {
            try {
              sock.server.close();
            } catch (e) {
            }
            sock.server = null;
          }
          // close any peer connections
          var peers = Object.keys(sock.peers);
          for (var i = 0; i < peers.length; i++) {
            var peer = sock.peers[peers[i]];
            try {
              peer.socket.close();
            } catch (e) {
            }
            SOCKFS.websocket_sock_ops.removePeer(sock, peer);
          }
          return 0;
        },bind:function (sock, addr, port) {
          if (typeof sock.saddr !== 'undefined' || typeof sock.sport !== 'undefined') {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);  // already bound
          }
          sock.saddr = addr;
          sock.sport = port || _mkport();
          // in order to emulate dgram sockets, we need to launch a listen server when
          // binding on a connection-less socket
          // note: this is only required on the server side
          if (sock.type === 2) {
            // close the existing server if it exists
            if (sock.server) {
              sock.server.close();
              sock.server = null;
            }
            // swallow error operation not supported error that occurs when binding in the
            // browser where this isn't supported
            try {
              sock.sock_ops.listen(sock, 0);
            } catch (e) {
              if (!(e instanceof FS.ErrnoError)) throw e;
              if (e.errno !== ERRNO_CODES.EOPNOTSUPP) throw e;
            }
          }
        },connect:function (sock, addr, port) {
          if (sock.server) {
            throw new FS.ErrnoError(ERRNO_CODES.EOPNOTSUPP);
          }
  
          // TODO autobind
          // if (!sock.addr && sock.type == 2) {
          // }
  
          // early out if we're already connected / in the middle of connecting
          if (typeof sock.daddr !== 'undefined' && typeof sock.dport !== 'undefined') {
            var dest = SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport);
            if (dest) {
              if (dest.socket.readyState === dest.socket.CONNECTING) {
                throw new FS.ErrnoError(ERRNO_CODES.EALREADY);
              } else {
                throw new FS.ErrnoError(ERRNO_CODES.EISCONN);
              }
            }
          }
  
          // add the socket to our peer list and set our
          // destination address / port to match
          var peer = SOCKFS.websocket_sock_ops.createPeer(sock, addr, port);
          sock.daddr = peer.addr;
          sock.dport = peer.port;
  
          // always "fail" in non-blocking mode
          throw new FS.ErrnoError(ERRNO_CODES.EINPROGRESS);
        },listen:function (sock, backlog) {
          if (!ENVIRONMENT_IS_NODE) {
            throw new FS.ErrnoError(ERRNO_CODES.EOPNOTSUPP);
          }
          if (sock.server) {
             throw new FS.ErrnoError(ERRNO_CODES.EINVAL);  // already listening
          }
          var WebSocketServer = require('ws').Server;
          var host = sock.saddr;
          sock.server = new WebSocketServer({
            host: host,
            port: sock.sport
            // TODO support backlog
          });
          Module['websocket'].emit('listen', sock.stream.fd); // Send Event with listen fd.
  
          sock.server.on('connection', function(ws) {
            if (sock.type === 1) {
              var newsock = SOCKFS.createSocket(sock.family, sock.type, sock.protocol);
  
              // create a peer on the new socket
              var peer = SOCKFS.websocket_sock_ops.createPeer(newsock, ws);
              newsock.daddr = peer.addr;
              newsock.dport = peer.port;
  
              // push to queue for accept to pick up
              sock.pending.push(newsock);
              Module['websocket'].emit('connection', newsock.stream.fd);
            } else {
              // create a peer on the listen socket so calling sendto
              // with the listen socket and an address will resolve
              // to the correct client
              SOCKFS.websocket_sock_ops.createPeer(sock, ws);
              Module['websocket'].emit('connection', sock.stream.fd);
            }
          });
          sock.server.on('closed', function() {
            Module['websocket'].emit('close', sock.stream.fd);
            sock.server = null;
          });
          sock.server.on('error', function(error) {
            // Although the ws library may pass errors that may be more descriptive than
            // ECONNREFUSED they are not necessarily the expected error code e.g. 
            // ENOTFOUND on getaddrinfo seems to be node.js specific, so using EHOSTUNREACH
            // is still probably the most useful thing to do. This error shouldn't
            // occur in a well written app as errors should get trapped in the compiled
            // app's own getaddrinfo call.
            sock.error = ERRNO_CODES.EHOSTUNREACH; // Used in getsockopt for SOL_SOCKET/SO_ERROR test.
            Module['websocket'].emit('error', [sock.stream.fd, sock.error, 'EHOSTUNREACH: Host is unreachable']);
            // don't throw
          });
        },accept:function (listensock) {
          if (!listensock.server) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          var newsock = listensock.pending.shift();
          newsock.stream.flags = listensock.stream.flags;
          return newsock;
        },getname:function (sock, peer) {
          var addr, port;
          if (peer) {
            if (sock.daddr === undefined || sock.dport === undefined) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
            }
            addr = sock.daddr;
            port = sock.dport;
          } else {
            // TODO saddr and sport will be set for bind()'d UDP sockets, but what
            // should we be returning for TCP sockets that've been connect()'d?
            addr = sock.saddr || 0;
            port = sock.sport || 0;
          }
          return { addr: addr, port: port };
        },sendmsg:function (sock, buffer, offset, length, addr, port) {
          if (sock.type === 2) {
            // connection-less sockets will honor the message address,
            // and otherwise fall back to the bound destination address
            if (addr === undefined || port === undefined) {
              addr = sock.daddr;
              port = sock.dport;
            }
            // if there was no address to fall back to, error out
            if (addr === undefined || port === undefined) {
              throw new FS.ErrnoError(ERRNO_CODES.EDESTADDRREQ);
            }
          } else {
            // connection-based sockets will only use the bound
            addr = sock.daddr;
            port = sock.dport;
          }
  
          // find the peer for the destination address
          var dest = SOCKFS.websocket_sock_ops.getPeer(sock, addr, port);
  
          // early out if not connected with a connection-based socket
          if (sock.type === 1) {
            if (!dest || dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
            } else if (dest.socket.readyState === dest.socket.CONNECTING) {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
          }
  
          // create a copy of the incoming data to send, as the WebSocket API
          // doesn't work entirely with an ArrayBufferView, it'll just send
          // the entire underlying buffer
          var data;
          if (buffer instanceof Array || buffer instanceof ArrayBuffer) {
            data = buffer.slice(offset, offset + length);
          } else {  // ArrayBufferView
            data = buffer.buffer.slice(buffer.byteOffset + offset, buffer.byteOffset + offset + length);
          }
  
          // if we're emulating a connection-less dgram socket and don't have
          // a cached connection, queue the buffer to send upon connect and
          // lie, saying the data was sent now.
          if (sock.type === 2) {
            if (!dest || dest.socket.readyState !== dest.socket.OPEN) {
              // if we're not connected, open a new connection
              if (!dest || dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
                dest = SOCKFS.websocket_sock_ops.createPeer(sock, addr, port);
              }
              dest.dgram_send_queue.push(data);
              return length;
            }
          }
  
          try {
            // send the actual data
            dest.socket.send(data);
            return length;
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
        },recvmsg:function (sock, length) {
          // http://pubs.opengroup.org/onlinepubs/7908799/xns/recvmsg.html
          if (sock.type === 1 && sock.server) {
            // tcp servers should not be recv()'ing on the listen socket
            throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
          }
  
          var queued = sock.recv_queue.shift();
          if (!queued) {
            if (sock.type === 1) {
              var dest = SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport);
  
              if (!dest) {
                // if we have a destination address but are not connected, error out
                throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
              }
              else if (dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
                // return null if the socket has closed
                return null;
              }
              else {
                // else, our socket is in a valid state but truly has nothing available
                throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
              }
            } else {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
          }
  
          // queued.data will be an ArrayBuffer if it's unadulterated, but if it's
          // requeued TCP data it'll be an ArrayBufferView
          var queuedLength = queued.data.byteLength || queued.data.length;
          var queuedOffset = queued.data.byteOffset || 0;
          var queuedBuffer = queued.data.buffer || queued.data;
          var bytesRead = Math.min(length, queuedLength);
          var res = {
            buffer: new Uint8Array(queuedBuffer, queuedOffset, bytesRead),
            addr: queued.addr,
            port: queued.port
          };
  
  
          // push back any unread data for TCP connections
          if (sock.type === 1 && bytesRead < queuedLength) {
            var bytesRemaining = queuedLength - bytesRead;
            queued.data = new Uint8Array(queuedBuffer, queuedOffset + bytesRead, bytesRemaining);
            sock.recv_queue.unshift(queued);
          }
  
          return res;
        }}};function _send(fd, buf, len, flags) {
      var sock = SOCKFS.getSocket(fd);
      if (!sock) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      // TODO honor flags
      return _write(fd, buf, len);
    }
  
  function _pwrite(fildes, buf, nbyte, offset) {
      // ssize_t pwrite(int fildes, const void *buf, size_t nbyte, off_t offset);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/write.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      try {
        var slab = HEAP8;
        return FS.write(stream, slab, buf, nbyte, offset);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _write(fildes, buf, nbyte) {
      // ssize_t write(int fildes, const void *buf, size_t nbyte);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/write.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
  
  
      try {
        var slab = HEAP8;
        return FS.write(stream, slab, buf, nbyte);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }
  
  function _fileno(stream) {
      // int fileno(FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fileno.html
      stream = FS.getStreamFromPtr(stream);
      if (!stream) return -1;
      return stream.fd;
    }function _fwrite(ptr, size, nitems, stream) {
      // size_t fwrite(const void *restrict ptr, size_t size, size_t nitems, FILE *restrict stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fwrite.html
      var bytesToWrite = nitems * size;
      if (bytesToWrite == 0) return 0;
      var fd = _fileno(stream);
      var bytesWritten = _write(fd, ptr, bytesToWrite);
      if (bytesWritten == -1) {
        var streamObj = FS.getStreamFromPtr(stream);
        if (streamObj) streamObj.error = true;
        return 0;
      } else {
        return (bytesWritten / size)|0;
      }
    }
  
  
   
  Module["_strlen"] = _strlen;
  
  function __reallyNegative(x) {
      return x < 0 || (x === 0 && (1/x) === -Infinity);
    }function __formatString(format, varargs) {
      var textIndex = format;
      var argIndex = 0;
      function getNextArg(type) {
        // NOTE: Explicitly ignoring type safety. Otherwise this fails:
        //       int x = 4; printf("%c\n", (char)x);
        var ret;
        if (type === 'double') {
          ret = (HEAP32[((tempDoublePtr)>>2)]=HEAP32[(((varargs)+(argIndex))>>2)],HEAP32[(((tempDoublePtr)+(4))>>2)]=HEAP32[(((varargs)+((argIndex)+(4)))>>2)],(+(HEAPF64[(tempDoublePtr)>>3])));
        } else if (type == 'i64') {
          ret = [HEAP32[(((varargs)+(argIndex))>>2)],
                 HEAP32[(((varargs)+(argIndex+4))>>2)]];
  
        } else {
          type = 'i32'; // varargs are always i32, i64, or double
          ret = HEAP32[(((varargs)+(argIndex))>>2)];
        }
        argIndex += Runtime.getNativeFieldSize(type);
        return ret;
      }
  
      var ret = [];
      var curr, next, currArg;
      while(1) {
        var startTextIndex = textIndex;
        curr = HEAP8[((textIndex)>>0)];
        if (curr === 0) break;
        next = HEAP8[((textIndex+1)>>0)];
        if (curr == 37) {
          // Handle flags.
          var flagAlwaysSigned = false;
          var flagLeftAlign = false;
          var flagAlternative = false;
          var flagZeroPad = false;
          var flagPadSign = false;
          flagsLoop: while (1) {
            switch (next) {
              case 43:
                flagAlwaysSigned = true;
                break;
              case 45:
                flagLeftAlign = true;
                break;
              case 35:
                flagAlternative = true;
                break;
              case 48:
                if (flagZeroPad) {
                  break flagsLoop;
                } else {
                  flagZeroPad = true;
                  break;
                }
              case 32:
                flagPadSign = true;
                break;
              default:
                break flagsLoop;
            }
            textIndex++;
            next = HEAP8[((textIndex+1)>>0)];
          }
  
          // Handle width.
          var width = 0;
          if (next == 42) {
            width = getNextArg('i32');
            textIndex++;
            next = HEAP8[((textIndex+1)>>0)];
          } else {
            while (next >= 48 && next <= 57) {
              width = width * 10 + (next - 48);
              textIndex++;
              next = HEAP8[((textIndex+1)>>0)];
            }
          }
  
          // Handle precision.
          var precisionSet = false, precision = -1;
          if (next == 46) {
            precision = 0;
            precisionSet = true;
            textIndex++;
            next = HEAP8[((textIndex+1)>>0)];
            if (next == 42) {
              precision = getNextArg('i32');
              textIndex++;
            } else {
              while(1) {
                var precisionChr = HEAP8[((textIndex+1)>>0)];
                if (precisionChr < 48 ||
                    precisionChr > 57) break;
                precision = precision * 10 + (precisionChr - 48);
                textIndex++;
              }
            }
            next = HEAP8[((textIndex+1)>>0)];
          }
          if (precision < 0) {
            precision = 6; // Standard default.
            precisionSet = false;
          }
  
          // Handle integer sizes. WARNING: These assume a 32-bit architecture!
          var argSize;
          switch (String.fromCharCode(next)) {
            case 'h':
              var nextNext = HEAP8[((textIndex+2)>>0)];
              if (nextNext == 104) {
                textIndex++;
                argSize = 1; // char (actually i32 in varargs)
              } else {
                argSize = 2; // short (actually i32 in varargs)
              }
              break;
            case 'l':
              var nextNext = HEAP8[((textIndex+2)>>0)];
              if (nextNext == 108) {
                textIndex++;
                argSize = 8; // long long
              } else {
                argSize = 4; // long
              }
              break;
            case 'L': // long long
            case 'q': // int64_t
            case 'j': // intmax_t
              argSize = 8;
              break;
            case 'z': // size_t
            case 't': // ptrdiff_t
            case 'I': // signed ptrdiff_t or unsigned size_t
              argSize = 4;
              break;
            default:
              argSize = null;
          }
          if (argSize) textIndex++;
          next = HEAP8[((textIndex+1)>>0)];
  
          // Handle type specifier.
          switch (String.fromCharCode(next)) {
            case 'd': case 'i': case 'u': case 'o': case 'x': case 'X': case 'p': {
              // Integer.
              var signed = next == 100 || next == 105;
              argSize = argSize || 4;
              var currArg = getNextArg('i' + (argSize * 8));
              var origArg = currArg;
              var argText;
              // Flatten i64-1 [low, high] into a (slightly rounded) double
              if (argSize == 8) {
                currArg = Runtime.makeBigInt(currArg[0], currArg[1], next == 117);
              }
              // Truncate to requested size.
              if (argSize <= 4) {
                var limit = Math.pow(256, argSize) - 1;
                currArg = (signed ? reSign : unSign)(currArg & limit, argSize * 8);
              }
              // Format the number.
              var currAbsArg = Math.abs(currArg);
              var prefix = '';
              if (next == 100 || next == 105) {
                if (argSize == 8 && i64Math) argText = i64Math.stringify(origArg[0], origArg[1], null); else
                argText = reSign(currArg, 8 * argSize, 1).toString(10);
              } else if (next == 117) {
                if (argSize == 8 && i64Math) argText = i64Math.stringify(origArg[0], origArg[1], true); else
                argText = unSign(currArg, 8 * argSize, 1).toString(10);
                currArg = Math.abs(currArg);
              } else if (next == 111) {
                argText = (flagAlternative ? '0' : '') + currAbsArg.toString(8);
              } else if (next == 120 || next == 88) {
                prefix = (flagAlternative && currArg != 0) ? '0x' : '';
                if (argSize == 8 && i64Math) {
                  if (origArg[1]) {
                    argText = (origArg[1]>>>0).toString(16);
                    var lower = (origArg[0]>>>0).toString(16);
                    while (lower.length < 8) lower = '0' + lower;
                    argText += lower;
                  } else {
                    argText = (origArg[0]>>>0).toString(16);
                  }
                } else
                if (currArg < 0) {
                  // Represent negative numbers in hex as 2's complement.
                  currArg = -currArg;
                  argText = (currAbsArg - 1).toString(16);
                  var buffer = [];
                  for (var i = 0; i < argText.length; i++) {
                    buffer.push((0xF - parseInt(argText[i], 16)).toString(16));
                  }
                  argText = buffer.join('');
                  while (argText.length < argSize * 2) argText = 'f' + argText;
                } else {
                  argText = currAbsArg.toString(16);
                }
                if (next == 88) {
                  prefix = prefix.toUpperCase();
                  argText = argText.toUpperCase();
                }
              } else if (next == 112) {
                if (currAbsArg === 0) {
                  argText = '(nil)';
                } else {
                  prefix = '0x';
                  argText = currAbsArg.toString(16);
                }
              }
              if (precisionSet) {
                while (argText.length < precision) {
                  argText = '0' + argText;
                }
              }
  
              // Add sign if needed
              if (currArg >= 0) {
                if (flagAlwaysSigned) {
                  prefix = '+' + prefix;
                } else if (flagPadSign) {
                  prefix = ' ' + prefix;
                }
              }
  
              // Move sign to prefix so we zero-pad after the sign
              if (argText.charAt(0) == '-') {
                prefix = '-' + prefix;
                argText = argText.substr(1);
              }
  
              // Add padding.
              while (prefix.length + argText.length < width) {
                if (flagLeftAlign) {
                  argText += ' ';
                } else {
                  if (flagZeroPad) {
                    argText = '0' + argText;
                  } else {
                    prefix = ' ' + prefix;
                  }
                }
              }
  
              // Insert the result into the buffer.
              argText = prefix + argText;
              argText.split('').forEach(function(chr) {
                ret.push(chr.charCodeAt(0));
              });
              break;
            }
            case 'f': case 'F': case 'e': case 'E': case 'g': case 'G': {
              // Float.
              var currArg = getNextArg('double');
              var argText;
              if (isNaN(currArg)) {
                argText = 'nan';
                flagZeroPad = false;
              } else if (!isFinite(currArg)) {
                argText = (currArg < 0 ? '-' : '') + 'inf';
                flagZeroPad = false;
              } else {
                var isGeneral = false;
                var effectivePrecision = Math.min(precision, 20);
  
                // Convert g/G to f/F or e/E, as per:
                // http://pubs.opengroup.org/onlinepubs/9699919799/functions/printf.html
                if (next == 103 || next == 71) {
                  isGeneral = true;
                  precision = precision || 1;
                  var exponent = parseInt(currArg.toExponential(effectivePrecision).split('e')[1], 10);
                  if (precision > exponent && exponent >= -4) {
                    next = ((next == 103) ? 'f' : 'F').charCodeAt(0);
                    precision -= exponent + 1;
                  } else {
                    next = ((next == 103) ? 'e' : 'E').charCodeAt(0);
                    precision--;
                  }
                  effectivePrecision = Math.min(precision, 20);
                }
  
                if (next == 101 || next == 69) {
                  argText = currArg.toExponential(effectivePrecision);
                  // Make sure the exponent has at least 2 digits.
                  if (/[eE][-+]\d$/.test(argText)) {
                    argText = argText.slice(0, -1) + '0' + argText.slice(-1);
                  }
                } else if (next == 102 || next == 70) {
                  argText = currArg.toFixed(effectivePrecision);
                  if (currArg === 0 && __reallyNegative(currArg)) {
                    argText = '-' + argText;
                  }
                }
  
                var parts = argText.split('e');
                if (isGeneral && !flagAlternative) {
                  // Discard trailing zeros and periods.
                  while (parts[0].length > 1 && parts[0].indexOf('.') != -1 &&
                         (parts[0].slice(-1) == '0' || parts[0].slice(-1) == '.')) {
                    parts[0] = parts[0].slice(0, -1);
                  }
                } else {
                  // Make sure we have a period in alternative mode.
                  if (flagAlternative && argText.indexOf('.') == -1) parts[0] += '.';
                  // Zero pad until required precision.
                  while (precision > effectivePrecision++) parts[0] += '0';
                }
                argText = parts[0] + (parts.length > 1 ? 'e' + parts[1] : '');
  
                // Capitalize 'E' if needed.
                if (next == 69) argText = argText.toUpperCase();
  
                // Add sign.
                if (currArg >= 0) {
                  if (flagAlwaysSigned) {
                    argText = '+' + argText;
                  } else if (flagPadSign) {
                    argText = ' ' + argText;
                  }
                }
              }
  
              // Add padding.
              while (argText.length < width) {
                if (flagLeftAlign) {
                  argText += ' ';
                } else {
                  if (flagZeroPad && (argText[0] == '-' || argText[0] == '+')) {
                    argText = argText[0] + '0' + argText.slice(1);
                  } else {
                    argText = (flagZeroPad ? '0' : ' ') + argText;
                  }
                }
              }
  
              // Adjust case.
              if (next < 97) argText = argText.toUpperCase();
  
              // Insert the result into the buffer.
              argText.split('').forEach(function(chr) {
                ret.push(chr.charCodeAt(0));
              });
              break;
            }
            case 's': {
              // String.
              var arg = getNextArg('i8*');
              var argLength = arg ? _strlen(arg) : '(null)'.length;
              if (precisionSet) argLength = Math.min(argLength, precision);
              if (!flagLeftAlign) {
                while (argLength < width--) {
                  ret.push(32);
                }
              }
              if (arg) {
                for (var i = 0; i < argLength; i++) {
                  ret.push(HEAPU8[((arg++)>>0)]);
                }
              } else {
                ret = ret.concat(intArrayFromString('(null)'.substr(0, argLength), true));
              }
              if (flagLeftAlign) {
                while (argLength < width--) {
                  ret.push(32);
                }
              }
              break;
            }
            case 'c': {
              // Character.
              if (flagLeftAlign) ret.push(getNextArg('i8'));
              while (--width > 0) {
                ret.push(32);
              }
              if (!flagLeftAlign) ret.push(getNextArg('i8'));
              break;
            }
            case 'n': {
              // Write the length written so far to the next parameter.
              var ptr = getNextArg('i32*');
              HEAP32[((ptr)>>2)]=ret.length;
              break;
            }
            case '%': {
              // Literal percent sign.
              ret.push(curr);
              break;
            }
            default: {
              // Unknown specifiers remain untouched.
              for (var i = startTextIndex; i < textIndex + 2; i++) {
                ret.push(HEAP8[((i)>>0)]);
              }
            }
          }
          textIndex += 2;
          // TODO: Support a/A (hex float) and m (last error) specifiers.
          // TODO: Support %1${specifier} for arg selection.
        } else {
          ret.push(curr);
          textIndex += 1;
        }
      }
      return ret;
    }function _fprintf(stream, format, varargs) {
      // int fprintf(FILE *restrict stream, const char *restrict format, ...);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/printf.html
      var result = __formatString(format, varargs);
      var stack = Runtime.stackSave();
      var ret = _fwrite(allocate(result, 'i8', ALLOC_STACK), 1, result.length, stream);
      Runtime.stackRestore(stack);
      return ret;
    }function _printf(format, varargs) {
      // int printf(const char *restrict format, ...);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/printf.html
      var stdout = HEAP32[((_stdout)>>2)];
      return _fprintf(stdout, format, varargs);
    }

  
  var GL={counter:1,lastError:0,buffers:[],mappedBuffers:{},programs:[],framebuffers:[],renderbuffers:[],textures:[],uniforms:[],shaders:[],vaos:[],contexts:[],currentContext:null,byteSizeByTypeRoot:5120,byteSizeByType:[1,1,2,2,4,4,4,2,3,4,8],programInfos:{},stringCache:{},packAlignment:4,unpackAlignment:4,init:function () {
        GL.miniTempBuffer = new Float32Array(GL.MINI_TEMP_BUFFER_SIZE);
        for (var i = 0; i < GL.MINI_TEMP_BUFFER_SIZE; i++) {
          GL.miniTempBufferViews[i] = GL.miniTempBuffer.subarray(0, i+1);
        }
      },recordError:function recordError(errorCode) {
        if (!GL.lastError) {
          GL.lastError = errorCode;
        }
      },getNewId:function (table) {
        var ret = GL.counter++;
        for (var i = table.length; i < ret; i++) {
          table[i] = null;
        }
        return ret;
      },MINI_TEMP_BUFFER_SIZE:16,miniTempBuffer:null,miniTempBufferViews:[0],getSource:function (shader, count, string, length) {
        var source = '';
        for (var i = 0; i < count; ++i) {
          var frag;
          if (length) {
            var len = HEAP32[(((length)+(i*4))>>2)];
            if (len < 0) {
              frag = Pointer_stringify(HEAP32[(((string)+(i*4))>>2)]);
            } else {
              frag = Pointer_stringify(HEAP32[(((string)+(i*4))>>2)], len);
            }
          } else {
            frag = Pointer_stringify(HEAP32[(((string)+(i*4))>>2)]);
          }
          source += frag;
        }
        return source;
      },computeImageSize:function (width, height, sizePerPixel, alignment) {
        function roundedToNextMultipleOf(x, y) {
          return Math.floor((x + y - 1) / y) * y
        }
        var plainRowSize = width * sizePerPixel;
        var alignedRowSize = roundedToNextMultipleOf(plainRowSize, alignment);
        return (height <= 0) ? 0 :
                 ((height - 1) * alignedRowSize + plainRowSize);
      },get:function (name_, p, type) {
        // Guard against user passing a null pointer.
        // Note that GLES2 spec does not say anything about how passing a null pointer should be treated.
        // Testing on desktop core GL 3, the application crashes on glGetIntegerv to a null pointer, but
        // better to report an error instead of doing anything random.
        if (!p) {
          GL.recordError(0x0501 /* GL_INVALID_VALUE */);
          return;
        }
        var ret = undefined;
        switch(name_) { // Handle a few trivial GLES values
          case 0x8DFA: // GL_SHADER_COMPILER
            ret = 1;
            break;
          case 0x8DF8: // GL_SHADER_BINARY_FORMATS
            if (type !== 'Integer') {
              GL.recordError(0x0500); // GL_INVALID_ENUM
            }
            return; // Do not write anything to the out pointer, since no binary formats are supported.
          case 0x8DF9: // GL_NUM_SHADER_BINARY_FORMATS
            ret = 0;
            break;
          case 0x86A2: // GL_NUM_COMPRESSED_TEXTURE_FORMATS
            // WebGL doesn't have GL_NUM_COMPRESSED_TEXTURE_FORMATS (it's obsolete since GL_COMPRESSED_TEXTURE_FORMATS returns a JS array that can be queried for length),
            // so implement it ourselves to allow C++ GLES2 code get the length.
            var formats = GLctx.getParameter(0x86A3 /*GL_COMPRESSED_TEXTURE_FORMATS*/);
            ret = formats.length;
            break;
          case 0x8B9A: // GL_IMPLEMENTATION_COLOR_READ_TYPE
            ret = 0x1401; // GL_UNSIGNED_BYTE
            break;
          case 0x8B9B: // GL_IMPLEMENTATION_COLOR_READ_FORMAT
            ret = 0x1908; // GL_RGBA
            break;
        }
  
        if (ret === undefined) {
          var result = GLctx.getParameter(name_);
          switch (typeof(result)) {
            case "number":
              ret = result;
              break;
            case "boolean":
              ret = result ? 1 : 0;
              break;
            case "string":
              GL.recordError(0x0500); // GL_INVALID_ENUM
              return;
            case "object":
              if (result === null) {
                // null is a valid result for some (e.g., which buffer is bound - perhaps nothing is bound), but otherwise
                // can mean an invalid name_, which we need to report as an error
                switch(name_) {
                  case 0x8894: // ARRAY_BUFFER_BINDING
                  case 0x8B8D: // CURRENT_PROGRAM
                  case 0x8895: // ELEMENT_ARRAY_BUFFER_BINDING
                  case 0x8CA6: // FRAMEBUFFER_BINDING
                  case 0x8CA7: // RENDERBUFFER_BINDING
                  case 0x8069: // TEXTURE_BINDING_2D
                  case 0x8514: { // TEXTURE_BINDING_CUBE_MAP
                    ret = 0;
                    break;
                  }
                  default: {
                    GL.recordError(0x0500); // GL_INVALID_ENUM
                    return;
                  }
                }
              } else if (result instanceof Float32Array ||
                         result instanceof Uint32Array ||
                         result instanceof Int32Array ||
                         result instanceof Array) {
                for (var i = 0; i < result.length; ++i) {
                  switch (type) {
                    case 'Integer': HEAP32[(((p)+(i*4))>>2)]=result[i];   break;
                    case 'Float':   HEAPF32[(((p)+(i*4))>>2)]=result[i]; break;
                    case 'Boolean': HEAP8[(((p)+(i))>>0)]=result[i] ? 1 : 0;    break;
                    default: throw 'internal glGet error, bad type: ' + type;
                  }
                }
                return;
              } else if (result instanceof WebGLBuffer ||
                         result instanceof WebGLProgram ||
                         result instanceof WebGLFramebuffer ||
                         result instanceof WebGLRenderbuffer ||
                         result instanceof WebGLTexture) {
                ret = result.name | 0;
              } else {
                GL.recordError(0x0500); // GL_INVALID_ENUM
                return;
              }
              break;
            default:
              GL.recordError(0x0500); // GL_INVALID_ENUM
              return;
          }
        }
  
        switch (type) {
          case 'Integer': HEAP32[((p)>>2)]=ret;    break;
          case 'Float':   HEAPF32[((p)>>2)]=ret;  break;
          case 'Boolean': HEAP8[((p)>>0)]=ret ? 1 : 0; break;
          default: throw 'internal glGet error, bad type: ' + type;
        }
      },getTexPixelData:function (type, format, width, height, pixels, internalFormat) {
        var sizePerPixel;
        var numChannels;
        switch(format) {
          case 0x1906 /* GL_ALPHA */:
          case 0x1909 /* GL_LUMINANCE */:
          case 0x1902 /* GL_DEPTH_COMPONENT */:
          case 0x1903 /* GL_RED */:
            numChannels = 1;
            break;
          case 0x190A /* GL_LUMINANCE_ALPHA */:
          case 0x8227 /* GL_RG */:
            numChannels = 2;
            break;
          case 0x1907 /* GL_RGB */:
            numChannels = 3;
            break;
          case 0x1908 /* GL_RGBA */:
            numChannels = 4;
            break;
          default:
            GL.recordError(0x0500); // GL_INVALID_ENUM
            return {
              pixels: null,
              internalFormat: 0x0
            };
        }
        switch (type) {
          case 0x1401 /* GL_UNSIGNED_BYTE */:
            sizePerPixel = numChannels*1;
            break;
          case 0x1403 /* GL_UNSIGNED_SHORT */:
          case 0x8D61 /* GL_HALF_FLOAT_OES */:
            sizePerPixel = numChannels*2;
            break;
          case 0x1405 /* GL_UNSIGNED_INT */:
          case 0x1406 /* GL_FLOAT */:
            sizePerPixel = numChannels*4;
            break;
          case 0x84FA /* UNSIGNED_INT_24_8_WEBGL */:
            sizePerPixel = 4;
            break;
          case 0x8363 /* GL_UNSIGNED_SHORT_5_6_5 */:
          case 0x8033 /* GL_UNSIGNED_SHORT_4_4_4_4 */:
          case 0x8034 /* GL_UNSIGNED_SHORT_5_5_5_1 */:
            sizePerPixel = 2;
            break;
          default:
            GL.recordError(0x0500); // GL_INVALID_ENUM
            return {
              pixels: null,
              internalFormat: 0x0
            };
        }
        var bytes = GL.computeImageSize(width, height, sizePerPixel, GL.unpackAlignment);
        if (type == 0x1401 /* GL_UNSIGNED_BYTE */) {
          pixels = HEAPU8.subarray((pixels),(pixels+bytes));
        } else if (type == 0x1406 /* GL_FLOAT */) {
          pixels = HEAPF32.subarray((pixels)>>2,(pixels+bytes)>>2);
        } else if (type == 0x1405 /* GL_UNSIGNED_INT */ || type == 0x84FA /* UNSIGNED_INT_24_8_WEBGL */) {
          pixels = HEAPU32.subarray((pixels)>>2,(pixels+bytes)>>2);
        } else {
          pixels = HEAPU16.subarray((pixels)>>1,(pixels+bytes)>>1);
        }
        return {
          pixels: pixels,
          internalFormat: internalFormat
        };
      },validateBufferTarget:function (target) {
        switch (target) {
          case 0x8892: // GL_ARRAY_BUFFER
          case 0x8893: // GL_ELEMENT_ARRAY_BUFFER
          case 0x8F36: // GL_COPY_READ_BUFFER
          case 0x8F37: // GL_COPY_WRITE_BUFFER
          case 0x88EB: // GL_PIXEL_PACK_BUFFER
          case 0x88EC: // GL_PIXEL_UNPACK_BUFFER
          case 0x8C2A: // GL_TEXTURE_BUFFER
          case 0x8C8E: // GL_TRANSFORM_FEEDBACK_BUFFER
          case 0x8A11: // GL_UNIFORM_BUFFER
            return true;
          default:
            return false;
        }
      },createContext:function (canvas, webGLContextAttributes) {
        if (typeof webGLContextAttributes.majorVersion === 'undefined' && typeof webGLContextAttributes.minorVersion === 'undefined') {
          webGLContextAttributes.majorVersion = 1;
          webGLContextAttributes.minorVersion = 0;
        }
        var ctx;
        var errorInfo = '?';
        function onContextCreationError(event) {
          errorInfo = event.statusMessage || errorInfo;
        }
        try {
          canvas.addEventListener('webglcontextcreationerror', onContextCreationError, false);
          try {
            if (webGLContextAttributes.majorVersion == 1 && webGLContextAttributes.minorVersion == 0) {
              ctx = canvas.getContext("webgl", webGLContextAttributes) || canvas.getContext("experimental-webgl", webGLContextAttributes);
            } else if (webGLContextAttributes.majorVersion == 2 && webGLContextAttributes.minorVersion == 0) {
              ctx = canvas.getContext("webgl2", webGLContextAttributes) || canvas.getContext("experimental-webgl2", webGLContextAttributes);
            } else {
              throw 'Unsupported WebGL context version ' + majorVersion + '.' + minorVersion + '!'
            }
          } finally {
            canvas.removeEventListener('webglcontextcreationerror', onContextCreationError, false);
          }
          if (!ctx) throw ':(';
        } catch (e) {
          Module.print('Could not create canvas: ' + [errorInfo, e, JSON.stringify(webGLContextAttributes)]);
          return 0;
        }
        // possible GL_DEBUG entry point: ctx = wrapDebugGL(ctx);
  
        if (!ctx) return 0;
        return GL.registerContext(ctx, webGLContextAttributes);
      },registerContext:function (ctx, webGLContextAttributes) {
        var handle = GL.getNewId(GL.contexts);
        var context = {
          handle: handle,
          version: webGLContextAttributes.majorVersion,
          GLctx: ctx
        };
        // Store the created context object so that we can access the context given a canvas without having to pass the parameters again.
        if (ctx.canvas) ctx.canvas.GLctxObject = context;
        GL.contexts[handle] = context;
        if (typeof webGLContextAttributes['enableExtensionsByDefault'] === 'undefined' || webGLContextAttributes.enableExtensionsByDefault) {
          GL.initExtensions(context);
        }
        return handle;
      },makeContextCurrent:function (contextHandle) {
        var context = GL.contexts[contextHandle];
        if (!context) return false;
        GLctx = Module.ctx = context.GLctx; // Active WebGL context object.
        GL.currentContext = context; // Active Emscripten GL layer context object.
        return true;
      },getContext:function (contextHandle) {
        return GL.contexts[contextHandle];
      },deleteContext:function (contextHandle) {
        if (GL.currentContext === GL.contexts[contextHandle]) GL.currentContext = null;
        if (typeof JSEvents === 'object') JSEvents.removeAllHandlersOnTarget(GL.contexts[contextHandle].GLctx.canvas); // Release all JS event handlers on the DOM element that the GL context is associated with since the context is now deleted.
        if (GL.contexts[contextHandle] && GL.contexts[contextHandle].GLctx.canvas) GL.contexts[contextHandle].GLctx.canvas.GLctxObject = undefined; // Make sure the canvas object no longer refers to the context object so there are no GC surprises.
        GL.contexts[contextHandle] = null;
      },initExtensions:function (context) {
  
        // If this function is called without a specific context object, init the extensions of the currently active context.
        if (!context) context = GL.currentContext;
  
        if (context.initExtensionsDone) return;
        context.initExtensionsDone = true;
  
        var GLctx = context.GLctx;
  
        context.maxVertexAttribs = GLctx.getParameter(GLctx.MAX_VERTEX_ATTRIBS);
  
        // Detect the presence of a few extensions manually, this GL interop layer itself will need to know if they exist. 
        context.compressionExt = GLctx.getExtension('WEBGL_compressed_texture_s3tc') ||
                            GLctx.getExtension('MOZ_WEBGL_compressed_texture_s3tc') ||
                            GLctx.getExtension('WEBKIT_WEBGL_compressed_texture_s3tc');
  
        context.anisotropicExt = GLctx.getExtension('EXT_texture_filter_anisotropic') ||
                            GLctx.getExtension('MOZ_EXT_texture_filter_anisotropic') ||
                            GLctx.getExtension('WEBKIT_EXT_texture_filter_anisotropic');
  
        context.floatExt = GLctx.getExtension('OES_texture_float');
  
        // Extension available from Firefox 26 and Google Chrome 30
        context.instancedArraysExt = GLctx.getExtension('ANGLE_instanced_arrays');
        
        // Extension available from Firefox 25 and WebKit
        context.vaoExt = GLctx.getExtension('OES_vertex_array_object');
  
        if (context.version === 2) {
          // drawBuffers is available in WebGL2 by default.
          context.drawBuffersExt = function(n, bufs) {
            GLctx.drawBuffers(n, bufs);
          };
        } else {
          var ext = GLctx.getExtension('WEBGL_draw_buffers');
          if (ext) {
            context.drawBuffersExt = function(n, bufs) {
              ext.drawBuffersWEBGL(n, bufs);
            };
          }
        }
  
        // These are the 'safe' feature-enabling extensions that don't add any performance impact related to e.g. debugging, and
        // should be enabled by default so that client GLES2/GL code will not need to go through extra hoops to get its stuff working.
        // As new extensions are ratified at http://www.khronos.org/registry/webgl/extensions/ , feel free to add your new extensions
        // here, as long as they don't produce a performance impact for users that might not be using those extensions.
        // E.g. debugging-related extensions should probably be off by default.
        var automaticallyEnabledExtensions = [ "OES_texture_float", "OES_texture_half_float", "OES_standard_derivatives",
                                               "OES_vertex_array_object", "WEBGL_compressed_texture_s3tc", "WEBGL_depth_texture",
                                               "OES_element_index_uint", "EXT_texture_filter_anisotropic", "ANGLE_instanced_arrays",
                                               "OES_texture_float_linear", "OES_texture_half_float_linear", "WEBGL_compressed_texture_atc",
                                               "WEBGL_compressed_texture_pvrtc", "EXT_color_buffer_half_float", "WEBGL_color_buffer_float",
                                               "EXT_frag_depth", "EXT_sRGB", "WEBGL_draw_buffers", "WEBGL_shared_resources",
                                               "EXT_shader_texture_lod" ];
  
        function shouldEnableAutomatically(extension) {
          var ret = false;
          automaticallyEnabledExtensions.forEach(function(include) {
            if (ext.indexOf(include) != -1) {
              ret = true;
            }
          });
          return ret;
        }
  
        var exts = GLctx.getSupportedExtensions();
        if (exts && exts.length > 0) {
          GLctx.getSupportedExtensions().forEach(function(ext) {
            ext = ext.replace('MOZ_', '').replace('WEBKIT_', '');
            if (automaticallyEnabledExtensions.indexOf(ext) != -1) {
              GLctx.getExtension(ext); // Calling .getExtension enables that extension permanently, no need to store the return value to be enabled.
            }
          });
        }
      },populateUniformTable:function (program) {
        var p = GL.programs[program];
        GL.programInfos[program] = {
          uniforms: {},
          maxUniformLength: 0, // This is eagerly computed below, since we already enumerate all uniforms anyway.
          maxAttributeLength: -1 // This is lazily computed and cached, computed when/if first asked, "-1" meaning not computed yet.
        };
  
        var ptable = GL.programInfos[program];
        var utable = ptable.uniforms;
        // A program's uniform table maps the string name of an uniform to an integer location of that uniform.
        // The global GL.uniforms map maps integer locations to WebGLUniformLocations.
        var numUniforms = GLctx.getProgramParameter(p, GLctx.ACTIVE_UNIFORMS);
        for (var i = 0; i < numUniforms; ++i) {
          var u = GLctx.getActiveUniform(p, i);
  
          var name = u.name;
          ptable.maxUniformLength = Math.max(ptable.maxUniformLength, name.length+1);
  
          // Strip off any trailing array specifier we might have got, e.g. "[0]".
          if (name.indexOf(']', name.length-1) !== -1) {
            var ls = name.lastIndexOf('[');
            name = name.slice(0, ls);
          }
  
          // Optimize memory usage slightly: If we have an array of uniforms, e.g. 'vec3 colors[3];', then 
          // only store the string 'colors' in utable, and 'colors[0]', 'colors[1]' and 'colors[2]' will be parsed as 'colors'+i.
          // Note that for the GL.uniforms table, we still need to fetch the all WebGLUniformLocations for all the indices.
          var loc = GLctx.getUniformLocation(p, name);
          var id = GL.getNewId(GL.uniforms);
          utable[name] = [u.size, id];
          GL.uniforms[id] = loc;
  
          for (var j = 1; j < u.size; ++j) {
            var n = name + '['+j+']';
            loc = GLctx.getUniformLocation(p, n);
            id = GL.getNewId(GL.uniforms);
  
            GL.uniforms[id] = loc;
          }
        }
      }};
  
  
  var Browser={mainLoop:{scheduler:null,method:"",currentlyRunningMainloop:0,func:null,arg:0,timingMode:0,timingValue:0,currentFrameNumber:0,queue:[],pause:function () {
          Browser.mainLoop.scheduler = null;
          Browser.mainLoop.currentlyRunningMainloop++; // Incrementing this signals the previous main loop that it's now become old, and it must return.
        },resume:function () {
          Browser.mainLoop.currentlyRunningMainloop++;
          var timingMode = Browser.mainLoop.timingMode;
          var timingValue = Browser.mainLoop.timingValue;
          var func = Browser.mainLoop.func;
          Browser.mainLoop.func = null;
          _emscripten_set_main_loop(func, 0, false, Browser.mainLoop.arg, true /* do not set timing and call scheduler, we will do it on the next lines */);
          _emscripten_set_main_loop_timing(timingMode, timingValue);
          Browser.mainLoop.scheduler();
        },updateStatus:function () {
          if (Module['setStatus']) {
            var message = Module['statusMessage'] || 'Please wait...';
            var remaining = Browser.mainLoop.remainingBlockers;
            var expected = Browser.mainLoop.expectedBlockers;
            if (remaining) {
              if (remaining < expected) {
                Module['setStatus'](message + ' (' + (expected - remaining) + '/' + expected + ')');
              } else {
                Module['setStatus'](message);
              }
            } else {
              Module['setStatus']('');
            }
          }
        },runIter:function (func) {
          if (ABORT) return;
          if (Module['preMainLoop']) {
            var preRet = Module['preMainLoop']();
            if (preRet === false) {
              return; // |return false| skips a frame
            }
          }
          try {
            func();
          } catch (e) {
            if (e instanceof ExitStatus) {
              return;
            } else {
              if (e && typeof e === 'object' && e.stack) Module.printErr('exception thrown: ' + [e, e.stack]);
              throw e;
            }
          }
          if (Module['postMainLoop']) Module['postMainLoop']();
        }},isFullScreen:false,pointerLock:false,moduleContextCreatedCallbacks:[],workers:[],init:function () {
        if (!Module["preloadPlugins"]) Module["preloadPlugins"] = []; // needs to exist even in workers
  
        if (Browser.initted) return;
        Browser.initted = true;
  
        try {
          new Blob();
          Browser.hasBlobConstructor = true;
        } catch(e) {
          Browser.hasBlobConstructor = false;
          console.log("warning: no blob constructor, cannot create blobs with mimetypes");
        }
        Browser.BlobBuilder = typeof MozBlobBuilder != "undefined" ? MozBlobBuilder : (typeof WebKitBlobBuilder != "undefined" ? WebKitBlobBuilder : (!Browser.hasBlobConstructor ? console.log("warning: no BlobBuilder") : null));
        Browser.URLObject = typeof window != "undefined" ? (window.URL ? window.URL : window.webkitURL) : undefined;
        if (!Module.noImageDecoding && typeof Browser.URLObject === 'undefined') {
          console.log("warning: Browser does not support creating object URLs. Built-in browser image decoding will not be available.");
          Module.noImageDecoding = true;
        }
  
        // Support for plugins that can process preloaded files. You can add more of these to
        // your app by creating and appending to Module.preloadPlugins.
        //
        // Each plugin is asked if it can handle a file based on the file's name. If it can,
        // it is given the file's raw data. When it is done, it calls a callback with the file's
        // (possibly modified) data. For example, a plugin might decompress a file, or it
        // might create some side data structure for use later (like an Image element, etc.).
  
        var imagePlugin = {};
        imagePlugin['canHandle'] = function imagePlugin_canHandle(name) {
          return !Module.noImageDecoding && /\.(jpg|jpeg|png|bmp)$/i.test(name);
        };
        imagePlugin['handle'] = function imagePlugin_handle(byteArray, name, onload, onerror) {
          var b = null;
          if (Browser.hasBlobConstructor) {
            try {
              b = new Blob([byteArray], { type: Browser.getMimetype(name) });
              if (b.size !== byteArray.length) { // Safari bug #118630
                // Safari's Blob can only take an ArrayBuffer
                b = new Blob([(new Uint8Array(byteArray)).buffer], { type: Browser.getMimetype(name) });
              }
            } catch(e) {
              Runtime.warnOnce('Blob constructor present but fails: ' + e + '; falling back to blob builder');
            }
          }
          if (!b) {
            var bb = new Browser.BlobBuilder();
            bb.append((new Uint8Array(byteArray)).buffer); // we need to pass a buffer, and must copy the array to get the right data range
            b = bb.getBlob();
          }
          var url = Browser.URLObject.createObjectURL(b);
          assert(typeof url == 'string', 'createObjectURL must return a url as a string');
          var img = new Image();
          img.onload = function img_onload() {
            assert(img.complete, 'Image ' + name + ' could not be decoded');
            var canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            var ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0);
            Module["preloadedImages"][name] = canvas;
            Browser.URLObject.revokeObjectURL(url);
            if (onload) onload(byteArray);
          };
          img.onerror = function img_onerror(event) {
            console.log('Image ' + url + ' could not be decoded');
            if (onerror) onerror();
          };
          img.src = url;
        };
        Module['preloadPlugins'].push(imagePlugin);
  
        var audioPlugin = {};
        audioPlugin['canHandle'] = function audioPlugin_canHandle(name) {
          return !Module.noAudioDecoding && name.substr(-4) in { '.ogg': 1, '.wav': 1, '.mp3': 1 };
        };
        audioPlugin['handle'] = function audioPlugin_handle(byteArray, name, onload, onerror) {
          var done = false;
          function finish(audio) {
            if (done) return;
            done = true;
            Module["preloadedAudios"][name] = audio;
            if (onload) onload(byteArray);
          }
          function fail() {
            if (done) return;
            done = true;
            Module["preloadedAudios"][name] = new Audio(); // empty shim
            if (onerror) onerror();
          }
          if (Browser.hasBlobConstructor) {
            try {
              var b = new Blob([byteArray], { type: Browser.getMimetype(name) });
            } catch(e) {
              return fail();
            }
            var url = Browser.URLObject.createObjectURL(b); // XXX we never revoke this!
            assert(typeof url == 'string', 'createObjectURL must return a url as a string');
            var audio = new Audio();
            audio.addEventListener('canplaythrough', function() { finish(audio) }, false); // use addEventListener due to chromium bug 124926
            audio.onerror = function audio_onerror(event) {
              if (done) return;
              console.log('warning: browser could not fully decode audio ' + name + ', trying slower base64 approach');
              function encode64(data) {
                var BASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
                var PAD = '=';
                var ret = '';
                var leftchar = 0;
                var leftbits = 0;
                for (var i = 0; i < data.length; i++) {
                  leftchar = (leftchar << 8) | data[i];
                  leftbits += 8;
                  while (leftbits >= 6) {
                    var curr = (leftchar >> (leftbits-6)) & 0x3f;
                    leftbits -= 6;
                    ret += BASE[curr];
                  }
                }
                if (leftbits == 2) {
                  ret += BASE[(leftchar&3) << 4];
                  ret += PAD + PAD;
                } else if (leftbits == 4) {
                  ret += BASE[(leftchar&0xf) << 2];
                  ret += PAD;
                }
                return ret;
              }
              audio.src = 'data:audio/x-' + name.substr(-3) + ';base64,' + encode64(byteArray);
              finish(audio); // we don't wait for confirmation this worked - but it's worth trying
            };
            audio.src = url;
            // workaround for chrome bug 124926 - we do not always get oncanplaythrough or onerror
            Browser.safeSetTimeout(function() {
              finish(audio); // try to use it even though it is not necessarily ready to play
            }, 10000);
          } else {
            return fail();
          }
        };
        Module['preloadPlugins'].push(audioPlugin);
  
        // Canvas event setup
  
        var canvas = Module['canvas'];
        function pointerLockChange() {
          Browser.pointerLock = document['pointerLockElement'] === canvas ||
                                document['mozPointerLockElement'] === canvas ||
                                document['webkitPointerLockElement'] === canvas ||
                                document['msPointerLockElement'] === canvas;
        }
        if (canvas) {
          // forced aspect ratio can be enabled by defining 'forcedAspectRatio' on Module
          // Module['forcedAspectRatio'] = 4 / 3;
          
          canvas.requestPointerLock = canvas['requestPointerLock'] ||
                                      canvas['mozRequestPointerLock'] ||
                                      canvas['webkitRequestPointerLock'] ||
                                      canvas['msRequestPointerLock'] ||
                                      function(){};
          canvas.exitPointerLock = document['exitPointerLock'] ||
                                   document['mozExitPointerLock'] ||
                                   document['webkitExitPointerLock'] ||
                                   document['msExitPointerLock'] ||
                                   function(){}; // no-op if function does not exist
          canvas.exitPointerLock = canvas.exitPointerLock.bind(document);
  
  
          document.addEventListener('pointerlockchange', pointerLockChange, false);
          document.addEventListener('mozpointerlockchange', pointerLockChange, false);
          document.addEventListener('webkitpointerlockchange', pointerLockChange, false);
          document.addEventListener('mspointerlockchange', pointerLockChange, false);
  
          if (Module['elementPointerLock']) {
            canvas.addEventListener("click", function(ev) {
              if (!Browser.pointerLock && canvas.requestPointerLock) {
                canvas.requestPointerLock();
                ev.preventDefault();
              }
            }, false);
          }
        }
      },createContext:function (canvas, useWebGL, setInModule, webGLContextAttributes) {
        if (useWebGL && Module.ctx && canvas == Module.canvas) return Module.ctx; // no need to recreate GL context if it's already been created for this canvas.
  
        var ctx;
        var contextHandle;
        if (useWebGL) {
          // For GLES2/desktop GL compatibility, adjust a few defaults to be different to WebGL defaults, so that they align better with the desktop defaults.
          var contextAttributes = {
            antialias: false,
            alpha: false
          };
  
          if (webGLContextAttributes) {
            for (var attribute in webGLContextAttributes) {
              contextAttributes[attribute] = webGLContextAttributes[attribute];
            }
          }
  
          contextHandle = GL.createContext(canvas, contextAttributes);
          if (contextHandle) {
            ctx = GL.getContext(contextHandle).GLctx;
          }
          // Set the background of the WebGL canvas to black
          canvas.style.backgroundColor = "black";
        } else {
          ctx = canvas.getContext('2d');
        }
  
        if (!ctx) return null;
  
        if (setInModule) {
          if (!useWebGL) assert(typeof GLctx === 'undefined', 'cannot set in module if GLctx is used, but we are a non-GL context that would replace it');
  
          Module.ctx = ctx;
          if (useWebGL) GL.makeContextCurrent(contextHandle);
          Module.useWebGL = useWebGL;
          Browser.moduleContextCreatedCallbacks.forEach(function(callback) { callback() });
          Browser.init();
        }
        return ctx;
      },destroyContext:function (canvas, useWebGL, setInModule) {},fullScreenHandlersInstalled:false,lockPointer:undefined,resizeCanvas:undefined,requestFullScreen:function (lockPointer, resizeCanvas, vrDevice) {
        Browser.lockPointer = lockPointer;
        Browser.resizeCanvas = resizeCanvas;
        Browser.vrDevice = vrDevice;
        if (typeof Browser.lockPointer === 'undefined') Browser.lockPointer = true;
        if (typeof Browser.resizeCanvas === 'undefined') Browser.resizeCanvas = false;
        if (typeof Browser.vrDevice === 'undefined') Browser.vrDevice = null;
  
        var canvas = Module['canvas'];
        function fullScreenChange() {
          Browser.isFullScreen = false;
          var canvasContainer = canvas.parentNode;
          if ((document['webkitFullScreenElement'] || document['webkitFullscreenElement'] ||
               document['mozFullScreenElement'] || document['mozFullscreenElement'] ||
               document['fullScreenElement'] || document['fullscreenElement'] ||
               document['msFullScreenElement'] || document['msFullscreenElement'] ||
               document['webkitCurrentFullScreenElement']) === canvasContainer) {
            canvas.cancelFullScreen = document['cancelFullScreen'] ||
                                      document['mozCancelFullScreen'] ||
                                      document['webkitCancelFullScreen'] ||
                                      document['msExitFullscreen'] ||
                                      document['exitFullscreen'] ||
                                      function() {};
            canvas.cancelFullScreen = canvas.cancelFullScreen.bind(document);
            if (Browser.lockPointer) canvas.requestPointerLock();
            Browser.isFullScreen = true;
            if (Browser.resizeCanvas) Browser.setFullScreenCanvasSize();
          } else {
            
            // remove the full screen specific parent of the canvas again to restore the HTML structure from before going full screen
            canvasContainer.parentNode.insertBefore(canvas, canvasContainer);
            canvasContainer.parentNode.removeChild(canvasContainer);
            
            if (Browser.resizeCanvas) Browser.setWindowedCanvasSize();
          }
          if (Module['onFullScreen']) Module['onFullScreen'](Browser.isFullScreen);
          Browser.updateCanvasDimensions(canvas);
        }
  
        if (!Browser.fullScreenHandlersInstalled) {
          Browser.fullScreenHandlersInstalled = true;
          document.addEventListener('fullscreenchange', fullScreenChange, false);
          document.addEventListener('mozfullscreenchange', fullScreenChange, false);
          document.addEventListener('webkitfullscreenchange', fullScreenChange, false);
          document.addEventListener('MSFullscreenChange', fullScreenChange, false);
        }
  
        // create a new parent to ensure the canvas has no siblings. this allows browsers to optimize full screen performance when its parent is the full screen root
        var canvasContainer = document.createElement("div");
        canvas.parentNode.insertBefore(canvasContainer, canvas);
        canvasContainer.appendChild(canvas);
  
        // use parent of canvas as full screen root to allow aspect ratio correction (Firefox stretches the root to screen size)
        canvasContainer.requestFullScreen = canvasContainer['requestFullScreen'] ||
                                            canvasContainer['mozRequestFullScreen'] ||
                                            canvasContainer['msRequestFullscreen'] ||
                                           (canvasContainer['webkitRequestFullScreen'] ? function() { canvasContainer['webkitRequestFullScreen'](Element['ALLOW_KEYBOARD_INPUT']) } : null);
  
        if (vrDevice) {
          canvasContainer.requestFullScreen({ vrDisplay: vrDevice });
        } else {
          canvasContainer.requestFullScreen();
        }
      },nextRAF:0,fakeRequestAnimationFrame:function (func) {
        // try to keep 60fps between calls to here
        var now = Date.now();
        if (Browser.nextRAF === 0) {
          Browser.nextRAF = now + 1000/60;
        } else {
          while (now + 2 >= Browser.nextRAF) { // fudge a little, to avoid timer jitter causing us to do lots of delay:0
            Browser.nextRAF += 1000/60;
          }
        }
        var delay = Math.max(Browser.nextRAF - now, 0);
        setTimeout(func, delay);
      },requestAnimationFrame:function requestAnimationFrame(func) {
        if (typeof window === 'undefined') { // Provide fallback to setTimeout if window is undefined (e.g. in Node.js)
          Browser.fakeRequestAnimationFrame(func);
        } else {
          if (!window.requestAnimationFrame) {
            window.requestAnimationFrame = window['requestAnimationFrame'] ||
                                           window['mozRequestAnimationFrame'] ||
                                           window['webkitRequestAnimationFrame'] ||
                                           window['msRequestAnimationFrame'] ||
                                           window['oRequestAnimationFrame'] ||
                                           Browser.fakeRequestAnimationFrame;
          }
          window.requestAnimationFrame(func);
        }
      },safeCallback:function (func) {
        return function() {
          if (!ABORT) return func.apply(null, arguments);
        };
      },allowAsyncCallbacks:true,queuedAsyncCallbacks:[],pauseAsyncCallbacks:function () {
        Browser.allowAsyncCallbacks = false;
      },resumeAsyncCallbacks:function () { // marks future callbacks as ok to execute, and synchronously runs any remaining ones right now
        Browser.allowAsyncCallbacks = true;
        if (Browser.queuedAsyncCallbacks.length > 0) {
          var callbacks = Browser.queuedAsyncCallbacks;
          Browser.queuedAsyncCallbacks = [];
          callbacks.forEach(function(func) {
            func();
          });
        }
      },safeRequestAnimationFrame:function (func) {
        return Browser.requestAnimationFrame(function() {
          if (ABORT) return;
          if (Browser.allowAsyncCallbacks) {
            func();
          } else {
            Browser.queuedAsyncCallbacks.push(func);
          }
        });
      },safeSetTimeout:function (func, timeout) {
        Module['noExitRuntime'] = true;
        return setTimeout(function() {
          if (ABORT) return;
          if (Browser.allowAsyncCallbacks) {
            func();
          } else {
            Browser.queuedAsyncCallbacks.push(func);
          }
        }, timeout);
      },safeSetInterval:function (func, timeout) {
        Module['noExitRuntime'] = true;
        return setInterval(function() {
          if (ABORT) return;
          if (Browser.allowAsyncCallbacks) {
            func();
          } // drop it on the floor otherwise, next interval will kick in
        }, timeout);
      },getMimetype:function (name) {
        return {
          'jpg': 'image/jpeg',
          'jpeg': 'image/jpeg',
          'png': 'image/png',
          'bmp': 'image/bmp',
          'ogg': 'audio/ogg',
          'wav': 'audio/wav',
          'mp3': 'audio/mpeg'
        }[name.substr(name.lastIndexOf('.')+1)];
      },getUserMedia:function (func) {
        if(!window.getUserMedia) {
          window.getUserMedia = navigator['getUserMedia'] ||
                                navigator['mozGetUserMedia'];
        }
        window.getUserMedia(func);
      },getMovementX:function (event) {
        return event['movementX'] ||
               event['mozMovementX'] ||
               event['webkitMovementX'] ||
               0;
      },getMovementY:function (event) {
        return event['movementY'] ||
               event['mozMovementY'] ||
               event['webkitMovementY'] ||
               0;
      },getMouseWheelDelta:function (event) {
        var delta = 0;
        switch (event.type) {
          case 'DOMMouseScroll': 
            delta = event.detail;
            break;
          case 'mousewheel': 
            delta = event.wheelDelta;
            break;
          case 'wheel': 
            delta = event['deltaY'];
            break;
          default:
            throw 'unrecognized mouse wheel event: ' + event.type;
        }
        return delta;
      },mouseX:0,mouseY:0,mouseMovementX:0,mouseMovementY:0,touches:{},lastTouches:{},calculateMouseEvent:function (event) { // event should be mousemove, mousedown or mouseup
        if (Browser.pointerLock) {
          // When the pointer is locked, calculate the coordinates
          // based on the movement of the mouse.
          // Workaround for Firefox bug 764498
          if (event.type != 'mousemove' &&
              ('mozMovementX' in event)) {
            Browser.mouseMovementX = Browser.mouseMovementY = 0;
          } else {
            Browser.mouseMovementX = Browser.getMovementX(event);
            Browser.mouseMovementY = Browser.getMovementY(event);
          }
          
          // check if SDL is available
          if (typeof SDL != "undefined") {
          	Browser.mouseX = SDL.mouseX + Browser.mouseMovementX;
          	Browser.mouseY = SDL.mouseY + Browser.mouseMovementY;
          } else {
          	// just add the mouse delta to the current absolut mouse position
          	// FIXME: ideally this should be clamped against the canvas size and zero
          	Browser.mouseX += Browser.mouseMovementX;
          	Browser.mouseY += Browser.mouseMovementY;
          }        
        } else {
          // Otherwise, calculate the movement based on the changes
          // in the coordinates.
          var rect = Module["canvas"].getBoundingClientRect();
          var cw = Module["canvas"].width;
          var ch = Module["canvas"].height;
  
          // Neither .scrollX or .pageXOffset are defined in a spec, but
          // we prefer .scrollX because it is currently in a spec draft.
          // (see: http://www.w3.org/TR/2013/WD-cssom-view-20131217/)
          var scrollX = ((typeof window.scrollX !== 'undefined') ? window.scrollX : window.pageXOffset);
          var scrollY = ((typeof window.scrollY !== 'undefined') ? window.scrollY : window.pageYOffset);
          // If this assert lands, it's likely because the browser doesn't support scrollX or pageXOffset
          // and we have no viable fallback.
          assert((typeof scrollX !== 'undefined') && (typeof scrollY !== 'undefined'), 'Unable to retrieve scroll position, mouse positions likely broken.');
  
          if (event.type === 'touchstart' || event.type === 'touchend' || event.type === 'touchmove') {
            var touch = event.touch;
            if (touch === undefined) {
              return; // the "touch" property is only defined in SDL
  
            }
            var adjustedX = touch.pageX - (scrollX + rect.left);
            var adjustedY = touch.pageY - (scrollY + rect.top);
  
            adjustedX = adjustedX * (cw / rect.width);
            adjustedY = adjustedY * (ch / rect.height);
  
            var coords = { x: adjustedX, y: adjustedY };
            
            if (event.type === 'touchstart') {
              Browser.lastTouches[touch.identifier] = coords;
              Browser.touches[touch.identifier] = coords;
            } else if (event.type === 'touchend' || event.type === 'touchmove') {
              Browser.lastTouches[touch.identifier] = Browser.touches[touch.identifier];
              Browser.touches[touch.identifier] = { x: adjustedX, y: adjustedY };
            } 
            return;
          }
  
          var x = event.pageX - (scrollX + rect.left);
          var y = event.pageY - (scrollY + rect.top);
  
          // the canvas might be CSS-scaled compared to its backbuffer;
          // SDL-using content will want mouse coordinates in terms
          // of backbuffer units.
          x = x * (cw / rect.width);
          y = y * (ch / rect.height);
  
          Browser.mouseMovementX = x - Browser.mouseX;
          Browser.mouseMovementY = y - Browser.mouseY;
          Browser.mouseX = x;
          Browser.mouseY = y;
        }
      },xhrLoad:function (url, onload, onerror) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function xhr_onload() {
          if (xhr.status == 200 || (xhr.status == 0 && xhr.response)) { // file URLs can return 0
            onload(xhr.response);
          } else {
            onerror();
          }
        };
        xhr.onerror = onerror;
        xhr.send(null);
      },asyncLoad:function (url, onload, onerror, noRunDep) {
        Browser.xhrLoad(url, function(arrayBuffer) {
          assert(arrayBuffer, 'Loading data file "' + url + '" failed (no arrayBuffer).');
          onload(new Uint8Array(arrayBuffer));
          if (!noRunDep) removeRunDependency('al ' + url);
        }, function(event) {
          if (onerror) {
            onerror();
          } else {
            throw 'Loading data file "' + url + '" failed.';
          }
        });
        if (!noRunDep) addRunDependency('al ' + url);
      },resizeListeners:[],updateResizeListeners:function () {
        var canvas = Module['canvas'];
        Browser.resizeListeners.forEach(function(listener) {
          listener(canvas.width, canvas.height);
        });
      },setCanvasSize:function (width, height, noUpdates) {
        var canvas = Module['canvas'];
        Browser.updateCanvasDimensions(canvas, width, height);
        if (!noUpdates) Browser.updateResizeListeners();
      },windowedWidth:0,windowedHeight:0,setFullScreenCanvasSize:function () {
        // check if SDL is available   
        if (typeof SDL != "undefined") {
        	var flags = HEAPU32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)];
        	flags = flags | 0x00800000; // set SDL_FULLSCREEN flag
        	HEAP32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)]=flags
        }
        Browser.updateResizeListeners();
      },setWindowedCanvasSize:function () {
        // check if SDL is available       
        if (typeof SDL != "undefined") {
        	var flags = HEAPU32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)];
        	flags = flags & ~0x00800000; // clear SDL_FULLSCREEN flag
        	HEAP32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)]=flags
        }
        Browser.updateResizeListeners();
      },updateCanvasDimensions:function (canvas, wNative, hNative) {
        if (wNative && hNative) {
          canvas.widthNative = wNative;
          canvas.heightNative = hNative;
        } else {
          wNative = canvas.widthNative;
          hNative = canvas.heightNative;
        }
        var w = wNative;
        var h = hNative;
        if (Module['forcedAspectRatio'] && Module['forcedAspectRatio'] > 0) {
          if (w/h < Module['forcedAspectRatio']) {
            w = Math.round(h * Module['forcedAspectRatio']);
          } else {
            h = Math.round(w / Module['forcedAspectRatio']);
          }
        }
        if (((document['webkitFullScreenElement'] || document['webkitFullscreenElement'] ||
             document['mozFullScreenElement'] || document['mozFullscreenElement'] ||
             document['fullScreenElement'] || document['fullscreenElement'] ||
             document['msFullScreenElement'] || document['msFullscreenElement'] ||
             document['webkitCurrentFullScreenElement']) === canvas.parentNode) && (typeof screen != 'undefined')) {
           var factor = Math.min(screen.width / w, screen.height / h);
           w = Math.round(w * factor);
           h = Math.round(h * factor);
        }
        if (Browser.resizeCanvas) {
          if (canvas.width  != w) canvas.width  = w;
          if (canvas.height != h) canvas.height = h;
          if (typeof canvas.style != 'undefined') {
            canvas.style.removeProperty( "width");
            canvas.style.removeProperty("height");
          }
        } else {
          if (canvas.width  != wNative) canvas.width  = wNative;
          if (canvas.height != hNative) canvas.height = hNative;
          if (typeof canvas.style != 'undefined') {
            if (w != wNative || h != hNative) {
              canvas.style.setProperty( "width", w + "px", "important");
              canvas.style.setProperty("height", h + "px", "important");
            } else {
              canvas.style.removeProperty( "width");
              canvas.style.removeProperty("height");
            }
          }
        }
      },wgetRequests:{},nextWgetRequestHandle:0,getNextWgetRequestHandle:function () {
        var handle = Browser.nextWgetRequestHandle;
        Browser.nextWgetRequestHandle++;
        return handle;
      }};
  
  
  function _malloc(bytes) {
      /* Over-allocate to make sure it is byte-aligned by 8.
       * This will leak memory, but this is only the dummy
       * implementation (replaced by dlmalloc normally) so
       * not an issue.
       */
      var ptr = Runtime.dynamicAlloc(bytes + 8);
      return (ptr+8) & 0xFFFFFFF8;
    }
  Module["_malloc"] = _malloc;
  
  function _free() {
  }
  Module["_free"] = _free;
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  var _environ=allocate(1, "i32*", ALLOC_STATIC);var ___environ=_environ;function ___buildEnvironment(env) {
      // WARNING: Arbitrary limit!
      var MAX_ENV_VALUES = 64;
      var TOTAL_ENV_SIZE = 1024;
  
      // Statically allocate memory for the environment.
      var poolPtr;
      var envPtr;
      if (!___buildEnvironment.called) {
        ___buildEnvironment.called = true;
        // Set default values. Use string keys for Closure Compiler compatibility.
        ENV['USER'] = 'web_user';
        ENV['PATH'] = '/';
        ENV['PWD'] = '/';
        ENV['HOME'] = '/home/web_user';
        ENV['LANG'] = 'C';
        ENV['_'] = Module['thisProgram'];
        // Allocate memory.
        poolPtr = allocate(TOTAL_ENV_SIZE, 'i8', ALLOC_STATIC);
        envPtr = allocate(MAX_ENV_VALUES * 4,
                          'i8*', ALLOC_STATIC);
        HEAP32[((envPtr)>>2)]=poolPtr;
        HEAP32[((_environ)>>2)]=envPtr;
      } else {
        envPtr = HEAP32[((_environ)>>2)];
        poolPtr = HEAP32[((envPtr)>>2)];
      }
  
      // Collect key=value lines.
      var strings = [];
      var totalSize = 0;
      for (var key in env) {
        if (typeof env[key] === 'string') {
          var line = key + '=' + env[key];
          strings.push(line);
          totalSize += line.length;
        }
      }
      if (totalSize > TOTAL_ENV_SIZE) {
        throw new Error('Environment size exceeded TOTAL_ENV_SIZE!');
      }
  
      // Make new.
      var ptrSize = 4;
      for (var i = 0; i < strings.length; i++) {
        var line = strings[i];
        writeAsciiToMemory(line, poolPtr);
        HEAP32[(((envPtr)+(i * ptrSize))>>2)]=poolPtr;
        poolPtr += line.length + 1;
      }
      HEAP32[(((envPtr)+(strings.length * ptrSize))>>2)]=0;
    }var ENV={};function _getenv(name) {
      // char *getenv(const char *name);
      // http://pubs.opengroup.org/onlinepubs/009695399/functions/getenv.html
      if (name === 0) return 0;
      name = Pointer_stringify(name);
      if (!ENV.hasOwnProperty(name)) return 0;
  
      if (_getenv.ret) _free(_getenv.ret);
      _getenv.ret = allocate(intArrayFromString(ENV[name]), 'i8', ALLOC_NORMAL);
      return _getenv.ret;
    }
  
  function _putenv(string) {
      // int putenv(char *string);
      // http://pubs.opengroup.org/onlinepubs/009695399/functions/putenv.html
      // WARNING: According to the standard (and the glibc implementation), the
      //          string is taken by reference so future changes are reflected.
      //          We copy it instead, possibly breaking some uses.
      if (string === 0) {
        ___setErrNo(ERRNO_CODES.EINVAL);
        return -1;
      }
      string = Pointer_stringify(string);
      var splitPoint = string.indexOf('=')
      if (string === '' || string.indexOf('=') === -1) {
        ___setErrNo(ERRNO_CODES.EINVAL);
        return -1;
      }
      var name = string.slice(0, splitPoint);
      var value = string.slice(splitPoint + 1);
      if (!(name in ENV) || ENV[name] !== value) {
        ENV[name] = value;
        ___buildEnvironment(ENV);
      }
      return 0;
    }
  
  function _SDL_RWFromConstMem(mem, size) {
      var id = SDL.rwops.length; // TODO: recycle ids when they are null
      SDL.rwops.push({ bytes: mem, count: size });
      return id;
    }function _TTF_FontHeight(font) {
      var fontData = SDL.fonts[font];
      return fontData.size;
    }function _TTF_SizeText(font, text, w, h) {
      var fontData = SDL.fonts[font];
      if (w) {
        HEAP32[((w)>>2)]=SDL.estimateTextWidth(fontData, Pointer_stringify(text));
      }
      if (h) {
        HEAP32[((h)>>2)]=fontData.size;
      }
      return 0;
    }function _TTF_RenderText_Solid(font, text, color) {
      // XXX the font and color are ignored
      text = Pointer_stringify(text) || ' '; // if given an empty string, still return a valid surface
      var fontData = SDL.fonts[font];
      var w = SDL.estimateTextWidth(fontData, text);
      var h = fontData.size;
      var color = SDL.loadColorToCSSRGB(color); // XXX alpha breaks fonts?
      var fontString = h + 'px ' + fontData.name;
      var surf = SDL.makeSurface(w, h, 0, false, 'text:' + text); // bogus numbers..
      var surfData = SDL.surfaces[surf];
      surfData.ctx.save();
      surfData.ctx.fillStyle = color;
      surfData.ctx.font = fontString;
      surfData.ctx.textBaseline = 'top';
      surfData.ctx.fillText(text, 0, 0);
      surfData.ctx.restore();
      return surf;
    }function _Mix_HaltMusic() {
      var audio = SDL.music.audio;
      if (audio) {
        audio.src = audio.src; // rewind <media> element
        audio.currentPosition = 0; // rewind Web Audio graph playback.
        audio.pause();
      }
      SDL.music.audio = null;
      if (SDL.hookMusicFinished) {
        Runtime.dynCall('v', SDL.hookMusicFinished);
      }
      return 0;
    }function _Mix_PlayMusic(id, loops) {
      // Pause old music if it exists.
      if (SDL.music.audio) {
        if (!SDL.music.audio.paused) Module.printErr('Music is already playing. ' + SDL.music.source);
        SDL.music.audio.pause();
      }
      var info = SDL.audios[id];
      var audio;
      if (info.webAudio) { // Play via Web Audio API
        // Create an instance of the WebAudio object.
        audio = {};
        audio.resource = info; // This new webAudio object is an instance that refers to this existing resource.
        audio.paused = false;
        audio.currentPosition = 0;
        audio.play = function() { SDL.playWebAudio(this); }
        audio.pause = function() { SDL.pauseWebAudio(this); }
      } else if (info.audio) { // Play via the <audio> element
        audio = info.audio;
      }
      audio['onended'] = function() { if (SDL.music.audio == this) _Mix_HaltMusic(); } // will send callback
      audio.loop = loops != 0; // TODO: handle N loops for finite N
      audio.volume = SDL.music.volume;
      SDL.music.audio = audio;
      audio.play();
      return 0;
    }function _Mix_FreeChunk(id) {
      SDL.audios[id] = null;
    }function _Mix_LoadWAV_RW(rwopsID, freesrc) {
      var rwops = SDL.rwops[rwopsID];
  
      if (rwops === undefined)
        return 0;
  
      var filename = '';
      var audio;
      var webAudio;
      var bytes;
  
      if (rwops.filename !== undefined) {
        filename = PATH.resolve(rwops.filename);
        var raw = Module["preloadedAudios"][filename];
        if (!raw) {
          if (raw === null) Module.printErr('Trying to reuse preloaded audio, but freePreloadedMediaOnUse is set!');
          if (!Module.noAudioDecoding) Runtime.warnOnce('Cannot find preloaded audio ' + filename);
  
          // see if we can read the file-contents from the in-memory FS
          try {
            bytes = FS.readFile(filename);
          } catch (e) {
            Module.printErr('Couldn\'t find file for: ' + filename);
            return 0;
          }
        }
        if (Module['freePreloadedMediaOnUse']) {
          Module["preloadedAudios"][filename] = null;
        }
        audio = raw;
      }
      else if (rwops.bytes !== undefined) {
        // For Web Audio context buffer decoding, we must make a clone of the audio data, but for <media> element,
        // a view to existing data is sufficient.
        if (SDL.webAudioAvailable()) bytes = HEAPU8.buffer.slice(rwops.bytes, rwops.bytes + rwops.count);
        else bytes = HEAPU8.subarray(rwops.bytes, rwops.bytes + rwops.count);
      }
      else {
        return 0;
      }
  
      var arrayBuffer = bytes ? bytes.buffer || bytes : bytes;
  
      // To allow user code to work around browser bugs with audio playback on <audio> elements an Web Audio, enable
      // the user code to hook in a callback to decide on a file basis whether each file should use Web Audio or <audio> for decoding and playback.
      // In particular, see https://bugzilla.mozilla.org/show_bug.cgi?id=654787 and ?id=1012801 for tradeoffs.
      var canPlayWithWebAudio = Module['SDL_canPlayWithWebAudio'] === undefined || Module['SDL_canPlayWithWebAudio'](filename, arrayBuffer);
  
      if (bytes !== undefined && SDL.webAudioAvailable() && canPlayWithWebAudio) {
        audio = undefined;
        webAudio = {};
        // The audio decoding process is asynchronous, which gives trouble if user code plays the audio data back immediately
        // after loading. Therefore prepare an array of callback handlers to run when this audio decoding is complete, which
        // will then start the playback (with some delay).
        webAudio.onDecodeComplete = []; // While this member array exists, decoding hasn't finished yet.
        function onDecodeComplete(data) {
          webAudio.decodedBuffer = data;
          // Call all handlers that were waiting for this decode to finish, and clear the handler list.
          webAudio.onDecodeComplete.forEach(function(e) { e(); });
          webAudio.onDecodeComplete = undefined; // Don't allow more callback handlers since audio has finished decoding.
        }
  
        SDL.audioContext['decodeAudioData'](arrayBuffer, onDecodeComplete);
      } else if (audio === undefined && bytes) {
        // Here, we didn't find a preloaded audio but we either were passed a filepath for
        // which we loaded bytes, or we were passed some bytes
        var blob = new Blob([bytes], {type: rwops.mimetype});
        var url = URL.createObjectURL(blob);
        audio = new Audio();
        audio.src = url;
        audio.mozAudioChannelType = 'content'; // bugzilla 910340
      }
  
      var id = SDL.audios.length;
      // Keep the loaded audio in the audio arrays, ready for playback
      SDL.audios.push({
        source: filename,
        audio: audio, // Points to the <audio> element, if loaded
        webAudio: webAudio // Points to a Web Audio -specific resource object, if loaded
      });
      return id;
    }function _Mix_PlayChannel(channel, id, loops) {
      // TODO: handle fixed amount of N loops. Currently loops either 0 or infinite times.
  
      // Get the audio element associated with the ID
      var info = SDL.audios[id];
      if (!info) return -1;
      if (!info.audio && !info.webAudio) return -1;
  
      // If the user asks us to allocate a channel automatically, get the first
      // free one.
      if (channel == -1) {
        for (var i = SDL.channelMinimumNumber; i < SDL.numChannels; i++) {
          if (!SDL.channels[i].audio) {
            channel = i;
            break;
          }
        }
        if (channel == -1) {
          Module.printErr('All ' + SDL.numChannels + ' channels in use!');
          return -1;
        }
      }
      var channelInfo = SDL.channels[channel];
      var audio;
      if (info.webAudio) {
        // Create an instance of the WebAudio object.
        audio = {};
        audio.resource = info; // This new object is an instance that refers to this existing resource.
        audio.paused = false;
        audio.currentPosition = 0;
        // Make our instance look similar to the instance of a <media> to make api simple.
        audio.play = function() { SDL.playWebAudio(this); }
        audio.pause = function() { SDL.pauseWebAudio(this); }
      } else {
        // We clone the audio node to utilize the preloaded audio buffer, since
        // the browser has already preloaded the audio file.
        audio = info.audio.cloneNode(true);
        audio.numChannels = info.audio.numChannels;
        audio.frequency = info.audio.frequency;
      }
      audio['onended'] = function SDL_audio_onended() { // TODO: cache these
        if (channelInfo.audio == this) { channelInfo.audio.paused = true; channelInfo.audio = null; }
        if (SDL.channelFinished) Runtime.getFuncWrapper(SDL.channelFinished, 'vi')(channel);
      }
      channelInfo.audio = audio;
      // TODO: handle N loops. Behavior matches Mix_PlayMusic
      audio.loop = loops != 0;
      audio.volume = channelInfo.volume;
      audio.play();
      return channel;
    }function _SDL_PauseAudio(pauseOn) {
      if (!SDL.audio) {
        return;
      }
      if (pauseOn) {
        if (SDL.audio.timer !== undefined) {
          clearTimeout(SDL.audio.timer);
          SDL.audio.numAudioTimersPending = 0;
          SDL.audio.timer = undefined;
        }
      } else if (!SDL.audio.timer) {
        // Start the audio playback timer callback loop.
        SDL.audio.numAudioTimersPending = 1;
        SDL.audio.timer = Browser.safeSetTimeout(SDL.audio.caller, 1);
      }
      SDL.audio.paused = pauseOn;
    }function _SDL_CloseAudio() {
      if (SDL.audio) {
        _SDL_PauseAudio(1);
        _free(SDL.audio.buffer);
        SDL.audio = null;
        SDL.allocateChannels(0);
      }
    }function _SDL_LockSurface(surf) {
      var surfData = SDL.surfaces[surf];
  
      surfData.locked++;
      if (surfData.locked > 1) return 0;
  
      if (!surfData.buffer) {
        surfData.buffer = _malloc(surfData.width * surfData.height * 4);
        HEAP32[(((surf)+(20))>>2)]=surfData.buffer;
      }
  
      // Mark in C/C++-accessible SDL structure
      // SDL_Surface has the following fields: Uint32 flags, SDL_PixelFormat *format; int w, h; Uint16 pitch; void *pixels; ...
      // So we have fields all of the same size, and 5 of them before us.
      // TODO: Use macros like in library.js
      HEAP32[(((surf)+(20))>>2)]=surfData.buffer;
  
      if (surf == SDL.screen && Module.screenIsReadOnly && surfData.image) return 0;
  
      if (SDL.defaults.discardOnLock) {
        if (!surfData.image) {
          surfData.image = surfData.ctx.createImageData(surfData.width, surfData.height);
        }
        if (!SDL.defaults.opaqueFrontBuffer) return;
      } else {
        surfData.image = surfData.ctx.getImageData(0, 0, surfData.width, surfData.height);
      }
  
      // Emulate desktop behavior and kill alpha values on the locked surface. (very costly!) Set SDL.defaults.opaqueFrontBuffer = false
      // if you don't want this.
      if (surf == SDL.screen && SDL.defaults.opaqueFrontBuffer) {
        var data = surfData.image.data;
        var num = data.length;
        for (var i = 0; i < num/4; i++) {
          data[i*4+3] = 255; // opacity, as canvases blend alpha
        }
      }
  
      if (SDL.defaults.copyOnLock && !SDL.defaults.discardOnLock) {
        // Copy pixel data to somewhere accessible to 'C/C++'
        if (surfData.isFlagSet(0x00200000 /* SDL_HWPALETTE */)) {
          // If this is neaded then
          // we should compact the data from 32bpp to 8bpp index.
          // I think best way to implement this is use
          // additional colorMap hash (color->index).
          // Something like this:
          //
          // var size = surfData.width * surfData.height;
          // var data = '';
          // for (var i = 0; i<size; i++) {
          //   var color = SDL.translateRGBAToColor(
          //     surfData.image.data[i*4   ], 
          //     surfData.image.data[i*4 +1], 
          //     surfData.image.data[i*4 +2], 
          //     255);
          //   var index = surfData.colorMap[color];
          //   HEAP8[(((surfData.buffer)+(i))>>0)]=index;
          // }
          throw 'CopyOnLock is not supported for SDL_LockSurface with SDL_HWPALETTE flag set' + new Error().stack;
        } else {
        HEAPU8.set(surfData.image.data, surfData.buffer);
        }
      }
  
      return 0;
    }
  
  function _SDL_FreeRW(rwopsID) {
      SDL.rwops[rwopsID] = null;
      while (SDL.rwops.length > 0 && SDL.rwops[SDL.rwops.length-1] === null) {
        SDL.rwops.pop();
      }
    }function _IMG_Load_RW(rwopsID, freeSrc) {
      try {
        // stb_image integration support
        function cleanup() {
          if (rwops && freeSrc) _SDL_FreeRW(rwopsID);
        };
        function addCleanup(func) {
          var old = cleanup;
          cleanup = function added_cleanup() {
            old();
            func();
          }
        }
        function callStbImage(func, params) {
          var x = Module['_malloc'](4);
          var y = Module['_malloc'](4);
          var comp = Module['_malloc'](4);
          addCleanup(function() {
            Module['_free'](x);
            Module['_free'](y);
            Module['_free'](comp);
            if (data) Module['_stbi_image_free'](data);
          });
          var data = Module['_' + func].apply(null, params.concat([x, y, comp, 0]));
          if (!data) return null;
          return {
            rawData: true,
            data: data,
            width: HEAP32[((x)>>2)],
            height: HEAP32[((y)>>2)],
            size: HEAP32[((x)>>2)] * HEAP32[((y)>>2)] * HEAP32[((comp)>>2)],
            bpp: HEAP32[((comp)>>2)]
          };
        }
  
        var rwops = SDL.rwops[rwopsID];
        if (rwops === undefined) {
          return 0;
        }
  
        var filename = rwops.filename;
        if (filename === undefined) {
          Runtime.warnOnce('Only file names that have been preloaded are supported for IMG_Load_RW. Consider using STB_IMAGE=1 if you want synchronous image decoding (see settings.js)');
          return 0;
        }
  
        if (!raw) {
          filename = PATH.resolve(filename);
          var raw = Module["preloadedImages"][filename];
          if (!raw) {
            if (raw === null) Module.printErr('Trying to reuse preloaded image, but freePreloadedMediaOnUse is set!');
            Runtime.warnOnce('Cannot find preloaded image ' + filename);
            Runtime.warnOnce('Cannot find preloaded image ' + filename + '. Consider using STB_IMAGE=1 if you want synchronous image decoding (see settings.js)');
            return 0;
          } else if (Module['freePreloadedMediaOnUse']) {
            Module["preloadedImages"][filename] = null;
          }
        }
  
        var surf = SDL.makeSurface(raw.width, raw.height, 0, false, 'load:' + filename);
        var surfData = SDL.surfaces[surf];
        surfData.ctx.globalCompositeOperation = "copy";
        if (!raw.rawData) {
          surfData.ctx.drawImage(raw, 0, 0, raw.width, raw.height, 0, 0, raw.width, raw.height);
        } else {
          var imageData = surfData.ctx.getImageData(0, 0, surfData.width, surfData.height);
          if (raw.bpp == 4) {
            // rgba
            imageData.data.set(HEAPU8.subarray((raw.data),(raw.data+raw.size)));
          } else if (raw.bpp == 3) {
            // rgb
            var pixels = raw.size/3;
            var data = imageData.data;
            var sourcePtr = raw.data;
            var destPtr = 0;
            for (var i = 0; i < pixels; i++) {
              data[destPtr++] = HEAPU8[((sourcePtr++)>>0)];
              data[destPtr++] = HEAPU8[((sourcePtr++)>>0)];
              data[destPtr++] = HEAPU8[((sourcePtr++)>>0)];
              data[destPtr++] = 255;
            }
          } else if (raw.bpp == 1) {
            // grayscale
            var pixels = raw.size;
            var data = imageData.data;
            var sourcePtr = raw.data;
            var destPtr = 0;
            for (var i = 0; i < pixels; i++) {
              var value = HEAPU8[((sourcePtr++)>>0)];
              data[destPtr++] = value;
              data[destPtr++] = value;
              data[destPtr++] = value;
              data[destPtr++] = 255;
            }
          } else {
            Module.printErr('cannot handle bpp ' + raw.bpp);
            return 0;
          }
          surfData.ctx.putImageData(imageData, 0, 0);
        }
        surfData.ctx.globalCompositeOperation = "source-over";
        // XXX SDL does not specify that loaded images must have available pixel data, in fact
        //     there are cases where you just want to blit them, so you just need the hardware
        //     accelerated version. However, code everywhere seems to assume that the pixels
        //     are in fact available, so we retrieve it here. This does add overhead though.
        _SDL_LockSurface(surf);
        surfData.locked--; // The surface is not actually locked in this hack
        if (SDL.GL) {
          // After getting the pixel data, we can free the canvas and context if we do not need to do 2D canvas blitting
          surfData.canvas = surfData.ctx = null;
        }
        return surf;
      } finally {
        cleanup();
      }
    }
  
  function _SDL_RWFromFile(_name, mode) {
      var id = SDL.rwops.length; // TODO: recycle ids when they are null
      var name = Pointer_stringify(_name)
      SDL.rwops.push({ filename: name, mimetype: Browser.getMimetype(name) });
      return id;
    }function _IMG_Load(filename){
      var rwops = _SDL_RWFromFile(filename);
      var result = _IMG_Load_RW(rwops, 1);
      return result;
    }function _SDL_UpperBlitScaled(src, srcrect, dst, dstrect) {
      return SDL.blitSurface(src, srcrect, dst, dstrect, true);
    }function _SDL_UpperBlit(src, srcrect, dst, dstrect) {
      return SDL.blitSurface(src, srcrect, dst, dstrect, false);
    }function _SDL_GetTicks() {
      return (Date.now() - SDL.startTime)|0;
    }var SDL={defaults:{width:320,height:200,copyOnLock:true,discardOnLock:false,opaqueFrontBuffer:true},version:null,surfaces:{},canvasPool:[],events:[],fonts:[null],audios:[null],rwops:[null],music:{audio:null,volume:1},mixerFrequency:22050,mixerFormat:32784,mixerNumChannels:2,mixerChunkSize:1024,channelMinimumNumber:0,GL:false,glAttributes:{0:3,1:3,2:2,3:0,4:0,5:1,6:16,7:0,8:0,9:0,10:0,11:0,12:0,13:0,14:0,15:1,16:0,17:0,18:0},keyboardState:null,keyboardMap:{},canRequestFullscreen:false,isRequestingFullscreen:false,textInput:false,startTime:null,initFlags:0,buttonState:0,modState:0,DOMButtons:[0,0,0],DOMEventToSDLEvent:{},TOUCH_DEFAULT_ID:0,eventHandler:null,eventHandlerContext:null,eventHandlerTemp:0,keyCodes:{16:1249,17:1248,18:1250,20:1081,33:1099,34:1102,35:1101,36:1098,37:1104,38:1106,39:1103,40:1105,44:316,45:1097,46:127,91:1251,93:1125,96:1122,97:1113,98:1114,99:1115,100:1116,101:1117,102:1118,103:1119,104:1120,105:1121,106:1109,107:1111,109:1110,110:1123,111:1108,112:1082,113:1083,114:1084,115:1085,116:1086,117:1087,118:1088,119:1089,120:1090,121:1091,122:1092,123:1093,124:1128,125:1129,126:1130,127:1131,128:1132,129:1133,130:1134,131:1135,132:1136,133:1137,134:1138,135:1139,144:1107,160:94,161:33,162:34,163:35,164:36,165:37,166:38,167:95,168:40,169:41,170:42,171:43,172:124,173:45,174:123,175:125,176:126,181:127,182:129,183:128,188:44,190:46,191:47,192:96,219:91,220:92,221:93,222:39,224:1251},scanCodes:{8:42,9:43,13:40,27:41,32:44,35:204,39:53,44:54,46:55,47:56,48:39,49:30,50:31,51:32,52:33,53:34,54:35,55:36,56:37,57:38,58:203,59:51,61:46,91:47,92:49,93:48,96:52,97:4,98:5,99:6,100:7,101:8,102:9,103:10,104:11,105:12,106:13,107:14,108:15,109:16,110:17,111:18,112:19,113:20,114:21,115:22,116:23,117:24,118:25,119:26,120:27,121:28,122:29,127:76,305:224,308:226,316:70},loadRect:function (rect) {
        return {
          x: HEAP32[((rect + 0)>>2)],
          y: HEAP32[((rect + 4)>>2)],
          w: HEAP32[((rect + 8)>>2)],
          h: HEAP32[((rect + 12)>>2)]
        };
      },updateRect:function (rect, r) {
        HEAP32[((rect)>>2)]=r.x;
        HEAP32[(((rect)+(4))>>2)]=r.y;
        HEAP32[(((rect)+(8))>>2)]=r.w;
        HEAP32[(((rect)+(12))>>2)]=r.h;
      },intersectionOfRects:function (first, second) {
        var leftX = Math.max(first.x, second.x);
        var leftY = Math.max(first.y, second.y);
        var rightX = Math.min(first.x + first.w, second.x + second.w);
        var rightY = Math.min(first.y + first.h, second.y + second.h);
  
        return {
          x: leftX,
          y: leftY,
          w: Math.max(leftX, rightX) - leftX,
          h: Math.max(leftY, rightY) - leftY
        }
      },checkPixelFormat:function (fmt) {
        // Canvas screens are always RGBA.
        var format = HEAP32[((fmt)>>2)];
        if (format != -2042224636) {
          Runtime.warnOnce('Unsupported pixel format!');
        }
      },loadColorToCSSRGB:function (color) {
        var rgba = HEAP32[((color)>>2)];
        return 'rgb(' + (rgba&255) + ',' + ((rgba >> 8)&255) + ',' + ((rgba >> 16)&255) + ')';
      },loadColorToCSSRGBA:function (color) {
        var rgba = HEAP32[((color)>>2)];
        return 'rgba(' + (rgba&255) + ',' + ((rgba >> 8)&255) + ',' + ((rgba >> 16)&255) + ',' + (((rgba >> 24)&255)/255) + ')';
      },translateColorToCSSRGBA:function (rgba) {
        return 'rgba(' + (rgba&0xff) + ',' + (rgba>>8 & 0xff) + ',' + (rgba>>16 & 0xff) + ',' + (rgba>>>24)/0xff + ')';
      },translateRGBAToCSSRGBA:function (r, g, b, a) {
        return 'rgba(' + (r&0xff) + ',' + (g&0xff) + ',' + (b&0xff) + ',' + (a&0xff)/255 + ')';
      },translateRGBAToColor:function (r, g, b, a) {
        return r | g << 8 | b << 16 | a << 24;
      },makeSurface:function (width, height, flags, usePageCanvas, source, rmask, gmask, bmask, amask) {
        flags = flags || 0;
        var is_SDL_HWSURFACE = flags & 0x00000001;
        var is_SDL_HWPALETTE = flags & 0x00200000;
        var is_SDL_OPENGL = flags & 0x04000000;
  
        var surf = _malloc(60);
        var pixelFormat = _malloc(44);
        //surface with SDL_HWPALETTE flag is 8bpp surface (1 byte)
        var bpp = is_SDL_HWPALETTE ? 1 : 4;
        var buffer = 0;
  
        // preemptively initialize this for software surfaces,
        // otherwise it will be lazily initialized inside of SDL_LockSurface
        if (!is_SDL_HWSURFACE && !is_SDL_OPENGL) {
          buffer = _malloc(width * height * 4);
        }
  
        HEAP32[((surf)>>2)]=flags;
        HEAP32[(((surf)+(4))>>2)]=pixelFormat;
        HEAP32[(((surf)+(8))>>2)]=width;
        HEAP32[(((surf)+(12))>>2)]=height;
        HEAP32[(((surf)+(16))>>2)]=width * bpp;  // assuming RGBA or indexed for now,
                                                                                          // since that is what ImageData gives us in browsers
        HEAP32[(((surf)+(20))>>2)]=buffer;
  
        HEAP32[(((surf)+(36))>>2)]=0;
        HEAP32[(((surf)+(40))>>2)]=0;
        HEAP32[(((surf)+(44))>>2)]=Module["canvas"].width;
        HEAP32[(((surf)+(48))>>2)]=Module["canvas"].height;
  
        HEAP32[(((surf)+(56))>>2)]=1;
  
        HEAP32[((pixelFormat)>>2)]=-2042224636;
        HEAP32[(((pixelFormat)+(4))>>2)]=0;// TODO
        HEAP8[(((pixelFormat)+(8))>>0)]=bpp * 8;
        HEAP8[(((pixelFormat)+(9))>>0)]=bpp;
  
        HEAP32[(((pixelFormat)+(12))>>2)]=rmask || 0x000000ff;
        HEAP32[(((pixelFormat)+(16))>>2)]=gmask || 0x0000ff00;
        HEAP32[(((pixelFormat)+(20))>>2)]=bmask || 0x00ff0000;
        HEAP32[(((pixelFormat)+(24))>>2)]=amask || 0xff000000;
  
        // Decide if we want to use WebGL or not
        SDL.GL = SDL.GL || is_SDL_OPENGL;
        var canvas;
        if (!usePageCanvas) {
          if (SDL.canvasPool.length > 0) {
            canvas = SDL.canvasPool.pop();
          } else {
            canvas = document.createElement('canvas');
          }
          canvas.width = width;
          canvas.height = height;
        } else {
          canvas = Module['canvas'];
        }
  
        var webGLContextAttributes = {
          antialias: ((SDL.glAttributes[13 /*SDL_GL_MULTISAMPLEBUFFERS*/] != 0) && (SDL.glAttributes[14 /*SDL_GL_MULTISAMPLESAMPLES*/] > 1)),
          depth: (SDL.glAttributes[6 /*SDL_GL_DEPTH_SIZE*/] > 0),
          stencil: (SDL.glAttributes[7 /*SDL_GL_STENCIL_SIZE*/] > 0)
        };
        
        var ctx = Browser.createContext(canvas, is_SDL_OPENGL, usePageCanvas, webGLContextAttributes);
              
        SDL.surfaces[surf] = {
          width: width,
          height: height,
          canvas: canvas,
          ctx: ctx,
          surf: surf,
          buffer: buffer,
          pixelFormat: pixelFormat,
          alpha: 255,
          flags: flags,
          locked: 0,
          usePageCanvas: usePageCanvas,
          source: source,
  
          isFlagSet: function(flag) {
            return flags & flag;
          }
        };
  
        return surf;
      },copyIndexedColorData:function (surfData, rX, rY, rW, rH) {
        // HWPALETTE works with palette
        // setted by SDL_SetColors
        if (!surfData.colors) {
          return;
        }
        
        var fullWidth  = Module['canvas'].width;
        var fullHeight = Module['canvas'].height;
  
        var startX  = rX || 0;
        var startY  = rY || 0;
        var endX    = (rW || (fullWidth - startX)) + startX;
        var endY    = (rH || (fullHeight - startY)) + startY;
        
        var buffer  = surfData.buffer;
  
        if (!surfData.image.data32) {
          surfData.image.data32 = new Uint32Array(surfData.image.data.buffer);
        }
        var data32   = surfData.image.data32;
  
        var colors32 = surfData.colors32;
  
        for (var y = startY; y < endY; ++y) {
          var base = y * fullWidth;
          for (var x = startX; x < endX; ++x) {
            data32[base + x] = colors32[HEAPU8[((buffer + base + x)>>0)]];
          }
        }
      },freeSurface:function (surf) {
        var refcountPointer = surf + 56;
        var refcount = HEAP32[((refcountPointer)>>2)];
        if (refcount > 1) {
          HEAP32[((refcountPointer)>>2)]=refcount - 1;
          return;
        }
  
        var info = SDL.surfaces[surf];
        if (!info.usePageCanvas && info.canvas) SDL.canvasPool.push(info.canvas);
        if (info.buffer) _free(info.buffer);
        _free(info.pixelFormat);
        _free(surf);
        SDL.surfaces[surf] = null;
  
        if (surf === SDL.screen) {
          SDL.screen = null;
        }
      },blitSurface__deps:["SDL_LockSurface"],blitSurface:function (src, srcrect, dst, dstrect, scale) {
        var srcData = SDL.surfaces[src];
        var dstData = SDL.surfaces[dst];
        var sr, dr;
        if (srcrect) {
          sr = SDL.loadRect(srcrect);
        } else {
          sr = { x: 0, y: 0, w: srcData.width, h: srcData.height };
        }
        if (dstrect) {
          dr = SDL.loadRect(dstrect);
        } else {
          dr = { x: 0, y: 0, w: srcData.width, h: srcData.height };
        }
        if (dstData.clipRect) {
          var widthScale = (!scale || sr.w === 0) ? 1 : sr.w / dr.w;
          var heightScale = (!scale || sr.h === 0) ? 1 : sr.h / dr.h;
          
          dr = SDL.intersectionOfRects(dstData.clipRect, dr);
          
          sr.w = dr.w * widthScale;
          sr.h = dr.h * heightScale;
          
          if (dstrect) {
            SDL.updateRect(dstrect, dr);
          }
        }
        var blitw, blith;
        if (scale) {
          blitw = dr.w; blith = dr.h;
        } else {
          blitw = sr.w; blith = sr.h;
        }
        if (sr.w === 0 || sr.h === 0 || blitw === 0 || blith === 0) {
          return 0;
        }
        var oldAlpha = dstData.ctx.globalAlpha;
        dstData.ctx.globalAlpha = srcData.alpha/255;
        dstData.ctx.drawImage(srcData.canvas, sr.x, sr.y, sr.w, sr.h, dr.x, dr.y, blitw, blith);
        dstData.ctx.globalAlpha = oldAlpha;
        if (dst != SDL.screen) {
          // XXX As in IMG_Load, for compatibility we write out |pixels|
          Runtime.warnOnce('WARNING: copying canvas data to memory for compatibility');
          _SDL_LockSurface(dst);
          dstData.locked--; // The surface is not actually locked in this hack
        }
        return 0;
      },downFingers:{},savedKeydown:null,receiveEvent:function (event) {
        function unpressAllPressedKeys() {
          // Un-press all pressed keys: TODO
          for (var code in SDL.keyboardMap) {
            SDL.events.push({
              type: 'keyup',
              keyCode: SDL.keyboardMap[code]
            });
          }
        };
        switch(event.type) {
          case 'touchstart': case 'touchmove': {
            event.preventDefault();
  
            var touches = [];
            
            // Clear out any touchstart events that we've already processed
            if (event.type === 'touchstart') {
              for (var i = 0; i < event.touches.length; i++) {
                var touch = event.touches[i];
                if (SDL.downFingers[touch.identifier] != true) {
                  SDL.downFingers[touch.identifier] = true;
                  touches.push(touch);
                }
              }
            } else {
              touches = event.touches;
            }
            
            var firstTouch = touches[0];
            if (event.type == 'touchstart') {
              SDL.DOMButtons[0] = 1;
            }
            var mouseEventType;
            switch(event.type) {
              case 'touchstart': mouseEventType = 'mousedown'; break;
              case 'touchmove': mouseEventType = 'mousemove'; break;
            }
            var mouseEvent = {
              type: mouseEventType,
              button: 0,
              pageX: firstTouch.clientX,
              pageY: firstTouch.clientY
            };
            SDL.events.push(mouseEvent);
  
            for (var i = 0; i < touches.length; i++) {
              var touch = touches[i];
              SDL.events.push({
                type: event.type,
                touch: touch
              });
            };
            break;
          }
          case 'touchend': {
            event.preventDefault();
            
            // Remove the entry in the SDL.downFingers hash
            // because the finger is no longer down.
            for(var i = 0; i < event.changedTouches.length; i++) {
              var touch = event.changedTouches[i];
              if (SDL.downFingers[touch.identifier] === true) {
                delete SDL.downFingers[touch.identifier];
              }
            }
  
            var mouseEvent = {
              type: 'mouseup',
              button: 0,
              pageX: event.changedTouches[0].clientX,
              pageY: event.changedTouches[0].clientY
            };
            SDL.DOMButtons[0] = 0;
            SDL.events.push(mouseEvent);
            
            for (var i = 0; i < event.changedTouches.length; i++) {
              var touch = event.changedTouches[i];
              SDL.events.push({
                type: 'touchend',
                touch: touch
              });
            };
            break;
          }
          case 'DOMMouseScroll': case 'mousewheel': case 'wheel':
            var delta = -Browser.getMouseWheelDelta(event); // Flip the wheel direction to translate from browser wheel direction (+:down) to SDL direction (+:up)
            delta = (delta == 0) ? 0 : (delta > 0 ? Math.max(delta, 1) : Math.min(delta, -1)); // Quantize to integer so that minimum scroll is at least +/- 1.
  
            // Simulate old-style SDL events representing mouse wheel input as buttons
            var button = delta > 0 ? 3 /*SDL_BUTTON_WHEELUP-1*/ : 4 /*SDL_BUTTON_WHEELDOWN-1*/; // Subtract one since JS->C marshalling is defined to add one back.
            SDL.events.push({ type: 'mousedown', button: button, pageX: event.pageX, pageY: event.pageY });
            SDL.events.push({ type: 'mouseup', button: button, pageX: event.pageX, pageY: event.pageY });
  
            // Pass a delta motion event.
            SDL.events.push({ type: 'wheel', deltaX: 0, deltaY: delta });
            event.preventDefault(); // If we don't prevent this, then 'wheel' event will be sent again by the browser as 'DOMMouseScroll' and we will receive this same event the second time.
            break;
          case 'mousemove':
            if (SDL.DOMButtons[0] === 1) {
              SDL.events.push({
                type: 'touchmove',
                touch: {
                  identifier: 0,
                  deviceID: -1,
                  pageX: event.pageX,
                  pageY: event.pageY
                }
              });
            }
            if (Browser.pointerLock) {
              // workaround for firefox bug 750111
              if ('mozMovementX' in event) {
                event['movementX'] = event['mozMovementX'];
                event['movementY'] = event['mozMovementY'];
              }
              // workaround for Firefox bug 782777
              if (event['movementX'] == 0 && event['movementY'] == 0) {
                // ignore a mousemove event if it doesn't contain any movement info
                // (without pointer lock, we infer movement from pageX/pageY, so this check is unnecessary)
                event.preventDefault();
                return;
              }
            }
            // fall through
          case 'keydown': case 'keyup': case 'keypress': case 'mousedown': case 'mouseup':
            // If we preventDefault on keydown events, the subsequent keypress events
            // won't fire. However, it's fine (and in some cases necessary) to
            // preventDefault for keys that don't generate a character. Otherwise,
            // preventDefault is the right thing to do in general.
            if (event.type !== 'keydown' || (!SDL.unicode && !SDL.textInput) || (event.keyCode === 8 /* backspace */ || event.keyCode === 9 /* tab */)) {
              event.preventDefault();
            }
  
            if (event.type == 'mousedown') {
              SDL.DOMButtons[event.button] = 1;
              SDL.events.push({
                type: 'touchstart',
                touch: {
                  identifier: 0,
                  deviceID: -1,
                  pageX: event.pageX,
                  pageY: event.pageY
                }
              });
            } else if (event.type == 'mouseup') {
              // ignore extra ups, can happen if we leave the canvas while pressing down, then return,
              // since we add a mouseup in that case
              if (!SDL.DOMButtons[event.button]) {
                return;
              }
  
              SDL.events.push({
                type: 'touchend',
                touch: {
                  identifier: 0,
                  deviceID: -1,
                  pageX: event.pageX,
                  pageY: event.pageY
                }
              });
              SDL.DOMButtons[event.button] = 0;
            }
  
            // We can only request fullscreen as the result of user input.
            // Due to this limitation, we toggle a boolean on keydown which
            // SDL_WM_ToggleFullScreen will check and subsequently set another
            // flag indicating for us to request fullscreen on the following
            // keyup. This isn't perfect, but it enables SDL_WM_ToggleFullScreen
            // to work as the result of a keypress (which is an extremely
            // common use case).
            if (event.type === 'keydown' || event.type === 'mousedown') {
              SDL.canRequestFullscreen = true;
            } else if (event.type === 'keyup' || event.type === 'mouseup') {
              if (SDL.isRequestingFullscreen) {
                Module['requestFullScreen'](true, true);
                SDL.isRequestingFullscreen = false;
              }
              SDL.canRequestFullscreen = false;
            }
  
            // SDL expects a unicode character to be passed to its keydown events.
            // Unfortunately, the browser APIs only provide a charCode property on
            // keypress events, so we must backfill in keydown events with their
            // subsequent keypress event's charCode.
            if (event.type === 'keypress' && SDL.savedKeydown) {
              // charCode is read-only
              SDL.savedKeydown.keypressCharCode = event.charCode;
              SDL.savedKeydown = null;
            } else if (event.type === 'keydown') {
              SDL.savedKeydown = event;
            }
  
            // Don't push keypress events unless SDL_StartTextInput has been called.
            if (event.type !== 'keypress' || SDL.textInput) {
              SDL.events.push(event);
            }
            break;
          case 'mouseout':
            // Un-press all pressed mouse buttons, because we might miss the release outside of the canvas
            for (var i = 0; i < 3; i++) {
              if (SDL.DOMButtons[i]) {
                SDL.events.push({
                  type: 'mouseup',
                  button: i,
                  pageX: event.pageX,
                  pageY: event.pageY
                });
                SDL.DOMButtons[i] = 0;
              }
            }
            event.preventDefault();
            break;
          case 'focus':
            SDL.events.push(event);
            event.preventDefault();
            break;
          case 'blur':
            SDL.events.push(event);
            unpressAllPressedKeys();
            event.preventDefault();
            break;
          case 'visibilitychange':
            SDL.events.push({
              type: 'visibilitychange',
              visible: !document.hidden
            });
            unpressAllPressedKeys();
            event.preventDefault();
            break;
          case 'unload':
            if (Browser.mainLoop.runner) {
              SDL.events.push(event);
              // Force-run a main event loop, since otherwise this event will never be caught!
              Browser.mainLoop.runner();
            }
            return;
          case 'resize':
            SDL.events.push(event);
            // manually triggered resize event doesn't have a preventDefault member
            if (event.preventDefault) {
              event.preventDefault();
            }
            break;
        }
        if (SDL.events.length >= 10000) {
          Module.printErr('SDL event queue full, dropping events');
          SDL.events = SDL.events.slice(0, 10000);
        }
        // If we have a handler installed, this will push the events to the app
        // instead of the app polling for them.
        SDL.flushEventsToHandler();
        return;
      },lookupKeyCodeForEvent:function (event) {
          var code = event.keyCode;
          if (code >= 65 && code <= 90) {
            code += 32; // make lowercase for SDL
          } else {
            code = SDL.keyCodes[event.keyCode] || event.keyCode;
            // If this is one of the modifier keys (224 | 1<<10 - 227 | 1<<10), and the event specifies that it is
            // a right key, add 4 to get the right key SDL key code.
            if (event.location === KeyboardEvent.DOM_KEY_LOCATION_RIGHT && code >= (224 | 1<<10) && code <= (227 | 1<<10)) {
              code += 4;
            }
          }
          return code;
      },handleEvent:function (event) {
        if (event.handled) return;
        event.handled = true;
  
        switch (event.type) {
          case 'touchstart': case 'touchend': case 'touchmove': {
            Browser.calculateMouseEvent(event);
            break;
          }
          case 'keydown': case 'keyup': {
            var down = event.type === 'keydown';
            var code = SDL.lookupKeyCodeForEvent(event);
            HEAP8[(((SDL.keyboardState)+(code))>>0)]=down;
            // TODO: lmeta, rmeta, numlock, capslock, KMOD_MODE, KMOD_RESERVED
            SDL.modState = (HEAP8[(((SDL.keyboardState)+(1248))>>0)] ? 0x0040 : 0) | // KMOD_LCTRL
              (HEAP8[(((SDL.keyboardState)+(1249))>>0)] ? 0x0001 : 0) | // KMOD_LSHIFT
              (HEAP8[(((SDL.keyboardState)+(1250))>>0)] ? 0x0100 : 0) | // KMOD_LALT
              (HEAP8[(((SDL.keyboardState)+(1252))>>0)] ? 0x0080 : 0) | // KMOD_RCTRL
              (HEAP8[(((SDL.keyboardState)+(1253))>>0)] ? 0x0002 : 0) | // KMOD_RSHIFT
              (HEAP8[(((SDL.keyboardState)+(1254))>>0)] ? 0x0200 : 0); //  KMOD_RALT
            if (down) {
              SDL.keyboardMap[code] = event.keyCode; // save the DOM input, which we can use to unpress it during blur
            } else {
              delete SDL.keyboardMap[code];
            }
  
            break;
          }
          case 'mousedown': case 'mouseup':
            if (event.type == 'mousedown') {
              // SDL_BUTTON(x) is defined as (1 << ((x)-1)).  SDL buttons are 1-3,
              // and DOM buttons are 0-2, so this means that the below formula is
              // correct.
              SDL.buttonState |= 1 << event.button;
            } else if (event.type == 'mouseup') {
              SDL.buttonState &= ~(1 << event.button);
            }
            // fall through
          case 'mousemove': {
            Browser.calculateMouseEvent(event);
            break;
          }
        }
      },flushEventsToHandler:function () {
        if (!SDL.eventHandler) return;
  
        while (SDL.pollEvent(SDL.eventHandlerTemp)) {
          Runtime.dynCall('iii', SDL.eventHandler, [SDL.eventHandlerContext, SDL.eventHandlerTemp]);
        }
      },pollEvent:function (ptr) {
        if (SDL.initFlags & 0x200 && SDL.joystickEventState) {
          // If SDL_INIT_JOYSTICK was supplied AND the joystick system is configured
          // to automatically query for events, query for joystick events.
          SDL.queryJoysticks();
        }
        if (ptr) {
          while (SDL.events.length > 0) {
            if (SDL.makeCEvent(SDL.events.shift(), ptr) !== false) return 1;
          }
          return 0;
        } else {
          // XXX: somewhat risky in that we do not check if the event is real or not (makeCEvent returns false) if no pointer supplied
          return SDL.events.length > 0;
        }
      },makeCEvent:function (event, ptr) {
        if (typeof event === 'number') {
          // This is a pointer to a copy of a native C event that was SDL_PushEvent'ed
          _memcpy(ptr, event, 28);
          _free(event); // the copy is no longer needed
          return;
        }
  
        SDL.handleEvent(event);
  
        switch (event.type) {
          case 'keydown': case 'keyup': {
            var down = event.type === 'keydown';
            //Module.print('Received key event: ' + event.keyCode);
            var key = SDL.lookupKeyCodeForEvent(event);
            var scan;
            if (key >= 1024) {
              scan = key - 1024;
            } else {
              scan = SDL.scanCodes[key] || key;
            }
  
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP8[(((ptr)+(8))>>0)]=down ? 1 : 0;
            HEAP8[(((ptr)+(9))>>0)]=0; // TODO
            HEAP32[(((ptr)+(12))>>2)]=scan;
            HEAP32[(((ptr)+(16))>>2)]=key;
            HEAP16[(((ptr)+(20))>>1)]=SDL.modState;
            // some non-character keys (e.g. backspace and tab) won't have keypressCharCode set, fill in with the keyCode.
            HEAP32[(((ptr)+(24))>>2)]=event.keypressCharCode || key;
  
            break;
          }
          case 'keypress': {
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            // Not filling in windowID for now
            var cStr = intArrayFromString(String.fromCharCode(event.charCode));
            for (var i = 0; i < cStr.length; ++i) {
              HEAP8[(((ptr)+(8 + i))>>0)]=cStr[i];
            }
            break;
          }
          case 'mousedown': case 'mouseup': case 'mousemove': {
            if (event.type != 'mousemove') {
              var down = event.type === 'mousedown';
              HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
              HEAP32[(((ptr)+(4))>>2)]=0;
              HEAP32[(((ptr)+(8))>>2)]=0;
              HEAP32[(((ptr)+(12))>>2)]=0;
              HEAP8[(((ptr)+(16))>>0)]=event.button+1; // DOM buttons are 0-2, SDL 1-3
              HEAP8[(((ptr)+(17))>>0)]=down ? 1 : 0;
              HEAP32[(((ptr)+(20))>>2)]=Browser.mouseX;
              HEAP32[(((ptr)+(24))>>2)]=Browser.mouseY;
            } else {
              HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
              HEAP32[(((ptr)+(4))>>2)]=0;
              HEAP32[(((ptr)+(8))>>2)]=0;
              HEAP32[(((ptr)+(12))>>2)]=0;
              HEAP32[(((ptr)+(16))>>2)]=SDL.buttonState;
              HEAP32[(((ptr)+(20))>>2)]=Browser.mouseX;
              HEAP32[(((ptr)+(24))>>2)]=Browser.mouseY;
              HEAP32[(((ptr)+(28))>>2)]=Browser.mouseMovementX;
              HEAP32[(((ptr)+(32))>>2)]=Browser.mouseMovementY;
            }
            break;
          }
          case 'wheel': {
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP32[(((ptr)+(16))>>2)]=event.deltaX;
            HEAP32[(((ptr)+(20))>>2)]=event.deltaY; 
            break;       
          }
          case 'touchstart': case 'touchend': case 'touchmove': {
            var touch = event.touch;
            if (!Browser.touches[touch.identifier]) break;
            var w = Module['canvas'].width;
            var h = Module['canvas'].height;
            var x = Browser.touches[touch.identifier].x / w;
            var y = Browser.touches[touch.identifier].y / h;
            var lx = Browser.lastTouches[touch.identifier].x / w;
            var ly = Browser.lastTouches[touch.identifier].y / h;
            var dx = x - lx;
            var dy = y - ly;
            if (touch['deviceID'] === undefined) touch.deviceID = SDL.TOUCH_DEFAULT_ID;
            if (dx === 0 && dy === 0 && event.type === 'touchmove') return false; // don't send these if nothing happened
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP32[(((ptr)+(4))>>2)]=_SDL_GetTicks();
            (tempI64 = [touch.deviceID>>>0,(tempDouble=touch.deviceID,(+(Math_abs(tempDouble))) >= 1.0 ? (tempDouble > 0.0 ? ((Math_min((+(Math_floor((tempDouble)/4294967296.0))), 4294967295.0))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/4294967296.0)))))>>>0) : 0)],HEAP32[(((ptr)+(8))>>2)]=tempI64[0],HEAP32[(((ptr)+(12))>>2)]=tempI64[1]);
            (tempI64 = [touch.identifier>>>0,(tempDouble=touch.identifier,(+(Math_abs(tempDouble))) >= 1.0 ? (tempDouble > 0.0 ? ((Math_min((+(Math_floor((tempDouble)/4294967296.0))), 4294967295.0))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/4294967296.0)))))>>>0) : 0)],HEAP32[(((ptr)+(16))>>2)]=tempI64[0],HEAP32[(((ptr)+(20))>>2)]=tempI64[1]);
            HEAPF32[(((ptr)+(24))>>2)]=x;
            HEAPF32[(((ptr)+(28))>>2)]=y;
            HEAPF32[(((ptr)+(32))>>2)]=dx;
            HEAPF32[(((ptr)+(36))>>2)]=dy;
            if (touch.force !== undefined) {
              HEAPF32[(((ptr)+(40))>>2)]=touch.force;
            } else { // No pressure data, send a digital 0/1 pressure.
              HEAPF32[(((ptr)+(40))>>2)]=event.type == "touchend" ? 0 : 1;
            }
            break;
          }
          case 'unload': {
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            break;
          }
          case 'resize': {
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP32[(((ptr)+(4))>>2)]=event.w;
            HEAP32[(((ptr)+(8))>>2)]=event.h;
            break;
          }
          case 'joystick_button_up': case 'joystick_button_down': {
            var state = event.type === 'joystick_button_up' ? 0 : 1;
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP8[(((ptr)+(4))>>0)]=event.index;
            HEAP8[(((ptr)+(5))>>0)]=event.button;
            HEAP8[(((ptr)+(6))>>0)]=state;
            break;
          }
          case 'joystick_axis_motion': {
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP8[(((ptr)+(4))>>0)]=event.index;
            HEAP8[(((ptr)+(5))>>0)]=event.axis;
            HEAP32[(((ptr)+(8))>>2)]=SDL.joystickAxisValueConversion(event.value);
            break;
          }
          case 'focus': {
            var SDL_WINDOWEVENT_FOCUS_GAINED = 12 /* SDL_WINDOWEVENT_FOCUS_GAINED */;
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP32[(((ptr)+(4))>>2)]=0;
            HEAP8[(((ptr)+(8))>>0)]=SDL_WINDOWEVENT_FOCUS_GAINED;
            break;
          }
          case 'blur': {
            var SDL_WINDOWEVENT_FOCUS_LOST = 13 /* SDL_WINDOWEVENT_FOCUS_LOST */;
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP32[(((ptr)+(4))>>2)]=0;
            HEAP8[(((ptr)+(8))>>0)]=SDL_WINDOWEVENT_FOCUS_LOST;
            break;
          }
          case 'visibilitychange': {
            var SDL_WINDOWEVENT_SHOWN  = 1 /* SDL_WINDOWEVENT_SHOWN */;
            var SDL_WINDOWEVENT_HIDDEN = 2 /* SDL_WINDOWEVENT_HIDDEN */;
            var visibilityEventID = event.visible ? SDL_WINDOWEVENT_SHOWN : SDL_WINDOWEVENT_HIDDEN;
            HEAP32[((ptr)>>2)]=SDL.DOMEventToSDLEvent[event.type];
            HEAP32[(((ptr)+(4))>>2)]=0;
            HEAP8[(((ptr)+(8))>>0)]=visibilityEventID;
            break;
          }
          default: throw 'Unhandled SDL event: ' + event.type;
        }
      },estimateTextWidth:function (fontData, text) {
        var h = fontData.size;
        var fontString = h + 'px ' + fontData.name;
        var tempCtx = SDL.ttfContext;
        assert(tempCtx, 'TTF_Init must have been called');
        tempCtx.save();
        tempCtx.font = fontString;
        var ret = tempCtx.measureText(text).width | 0;
        tempCtx.restore();
        return ret;
      },allocateChannels:function (num) { // called from Mix_AllocateChannels and init
        if (SDL.numChannels && SDL.numChannels >= num && num != 0) return;
        SDL.numChannels = num;
        SDL.channels = [];
        for (var i = 0; i < num; i++) {
          SDL.channels[i] = {
            audio: null,
            volume: 1.0
          };
        }
      },setGetVolume:function (info, volume) {
        if (!info) return 0;
        var ret = info.volume * 128; // MIX_MAX_VOLUME
        if (volume != -1) {
          info.volume = Math.min(Math.max(volume, 0), 128) / 128;
          if (info.audio) {
            try {
              info.audio.volume = info.volume; // For <audio> element
              if (info.audio.webAudioGainNode) info.audio.webAudioGainNode['gain']['value'] = info.volume; // For WebAudio playback
            } catch(e) {
              Module.printErr('setGetVolume failed to set audio volume: ' + e);
            }
          }
        }
        return ret;
      },setPannerPosition:function (info, x, y, z) {
        if (!info) return;
        if (info.audio) {
          if (info.audio.webAudioPannerNode) {
            info.audio.webAudioPannerNode['setPosition'](x, y, z);
          }
        }
      },playWebAudio:function (audio) {
        if (!audio) return;
        if (audio.webAudioNode) return; // This instance is already playing, don't start again.
        if (!SDL.webAudioAvailable()) return;
        try {
          var webAudio = audio.resource.webAudio;
          audio.paused = false;
          if (!webAudio.decodedBuffer) {
            if (webAudio.onDecodeComplete === undefined) abort("Cannot play back audio object that was not loaded");
            webAudio.onDecodeComplete.push(function() { if (!audio.paused) SDL.playWebAudio(audio); });
            return;
          }
          audio.webAudioNode = SDL.audioContext['createBufferSource']();
          audio.webAudioNode['buffer'] = webAudio.decodedBuffer;
          audio.webAudioNode['loop'] = audio.loop;
          audio.webAudioNode['onended'] = function() { audio['onended'](); } // For <media> element compatibility, route the onended signal to the instance.
  
          audio.webAudioPannerNode = SDL.audioContext['createPanner']();
          audio.webAudioPannerNode['panningModel'] = 'equalpower';
  
          // Add an intermediate gain node to control volume.
          audio.webAudioGainNode = SDL.audioContext['createGain']();
          audio.webAudioGainNode['gain']['value'] = audio.volume;
  
          audio.webAudioNode['connect'](audio.webAudioPannerNode);
          audio.webAudioPannerNode['connect'](audio.webAudioGainNode);
          audio.webAudioGainNode['connect'](SDL.audioContext['destination']);
  
          audio.webAudioNode['start'](0, audio.currentPosition);
          audio.startTime = SDL.audioContext['currentTime'] - audio.currentPosition;
        } catch(e) {
          Module.printErr('playWebAudio failed: ' + e);
        }
      },pauseWebAudio:function (audio) {
        if (!audio) return;
        if (audio.webAudioNode) {
          try {
            // Remember where we left off, so that if/when we resume, we can restart the playback at a proper place.
            audio.currentPosition = (SDL.audioContext['currentTime'] - audio.startTime) % audio.resource.webAudio.decodedBuffer.duration;
            // Important: When we reach here, the audio playback is stopped by the user. But when calling .stop() below, the Web Audio
            // graph will send the onended signal, but we don't want to process that, since pausing should not clear/destroy the audio
            // channel.
            audio.webAudioNode['onended'] = undefined;
            audio.webAudioNode.stop();
            audio.webAudioNode = undefined;
          } catch(e) {
            Module.printErr('pauseWebAudio failed: ' + e);
          }
        }
        audio.paused = true;
      },openAudioContext:function () {
        // Initialize Web Audio API if we haven't done so yet. Note: Only initialize Web Audio context ever once on the web page,
        // since initializing multiple times fails on Chrome saying 'audio resources have been exhausted'.
        if (!SDL.audioContext) {
          if (typeof(AudioContext) !== 'undefined') SDL.audioContext = new AudioContext();
          else if (typeof(webkitAudioContext) !== 'undefined') SDL.audioContext = new webkitAudioContext();
        }
      },webAudioAvailable:function () { return !!SDL.audioContext; },fillWebAudioBufferFromHeap:function (heapPtr, sizeSamplesPerChannel, dstAudioBuffer) {
        // The input audio data is interleaved across the channels, i.e. [L, R, L, R, L, R, ...] and is either 8-bit or 16-bit as
        // supported by the SDL API. The output audio wave data for Web Audio API must be in planar buffers of [-1,1]-normalized Float32 data,
        // so perform a buffer conversion for the data.
        var numChannels = SDL.audio.channels;
        for(var c = 0; c < numChannels; ++c) {
          var channelData = dstAudioBuffer['getChannelData'](c);
          if (channelData.length != sizeSamplesPerChannel) {
            throw 'Web Audio output buffer length mismatch! Destination size: ' + channelData.length + ' samples vs expected ' + sizeSamplesPerChannel + ' samples!';
          }
          if (SDL.audio.format == 0x8010 /*AUDIO_S16LSB*/) {
            for(var j = 0; j < sizeSamplesPerChannel; ++j) {
              channelData[j] = (HEAP16[(((heapPtr)+((j*numChannels + c)*2))>>1)]) / 0x8000;
            }
          } else if (SDL.audio.format == 0x0008 /*AUDIO_U8*/) {
            for(var j = 0; j < sizeSamplesPerChannel; ++j) {
              var v = (HEAP8[(((heapPtr)+(j*numChannels + c))>>0)]);
              channelData[j] = ((v >= 0) ? v-128 : v+128) /128;
            }
          }
        }
      },debugSurface:function (surfData) {
        console.log('dumping surface ' + [surfData.surf, surfData.source, surfData.width, surfData.height]);
        var image = surfData.ctx.getImageData(0, 0, surfData.width, surfData.height);
        var data = image.data;
        var num = Math.min(surfData.width, surfData.height);
        for (var i = 0; i < num; i++) {
          console.log('   diagonal ' + i + ':' + [data[i*surfData.width*4 + i*4 + 0], data[i*surfData.width*4 + i*4 + 1], data[i*surfData.width*4 + i*4 + 2], data[i*surfData.width*4 + i*4 + 3]]);
        }
      },joystickEventState:1,lastJoystickState:{},joystickNamePool:{},recordJoystickState:function (joystick, state) {
        // Standardize button state.
        var buttons = new Array(state.buttons.length);
        for (var i = 0; i < state.buttons.length; i++) {
          buttons[i] = SDL.getJoystickButtonState(state.buttons[i]);
        }
  
        SDL.lastJoystickState[joystick] = {
          buttons: buttons,
          axes: state.axes.slice(0),
          timestamp: state.timestamp,
          index: state.index,
          id: state.id
        };
      },getJoystickButtonState:function (button) {
        if (typeof button === 'object') {
          // Current gamepad API editor's draft (Firefox Nightly)
          // https://dvcs.w3.org/hg/gamepad/raw-file/default/gamepad.html#idl-def-GamepadButton
          return button.pressed;
        } else {
          // Current gamepad API working draft (Firefox / Chrome Stable)
          // http://www.w3.org/TR/2012/WD-gamepad-20120529/#gamepad-interface
          return button > 0;
        }
      },queryJoysticks:function () {
        for (var joystick in SDL.lastJoystickState) {
          var state = SDL.getGamepad(joystick - 1);
          var prevState = SDL.lastJoystickState[joystick];
          // Check only if the timestamp has differed.
          // NOTE: Timestamp is not available in Firefox.
          if (typeof state.timestamp !== 'number' || state.timestamp !== prevState.timestamp) {
            var i;
            for (i = 0; i < state.buttons.length; i++) {
              var buttonState = SDL.getJoystickButtonState(state.buttons[i]);
              // NOTE: The previous state already has a boolean representation of
              //       its button, so no need to standardize its button state here.
              if (buttonState !== prevState.buttons[i]) {
                // Insert button-press event.
                SDL.events.push({
                  type: buttonState ? 'joystick_button_down' : 'joystick_button_up',
                  joystick: joystick,
                  index: joystick - 1,
                  button: i
                });
              }
            }
            for (i = 0; i < state.axes.length; i++) {
              if (state.axes[i] !== prevState.axes[i]) {
                // Insert axes-change event.
                SDL.events.push({
                  type: 'joystick_axis_motion',
                  joystick: joystick,
                  index: joystick - 1,
                  axis: i,
                  value: state.axes[i]
                });
              }
            }
  
            SDL.recordJoystickState(joystick, state);
          }
        }
      },joystickAxisValueConversion:function (value) {
        // Ensures that 0 is 0, 1 is 32767, and -1 is 32768.
        return Math.ceil(((value+1) * 32767.5) - 32768);
      },getGamepads:function () {
        var fcn = navigator.getGamepads || navigator.webkitGamepads || navigator.mozGamepads || navigator.gamepads || navigator.webkitGetGamepads;
        if (fcn !== undefined) {
          // The function must be applied on the navigator object.
          return fcn.apply(navigator);
        } else {
          return [];
        }
      },getGamepad:function (deviceIndex) {
        var gamepads = SDL.getGamepads();
        if (gamepads.length > deviceIndex && deviceIndex >= 0) {
          return gamepads[deviceIndex];
        }
        return null;
      }};function _SDL_SetVideoMode(width, height, depth, flags) {
      ['touchstart', 'touchend', 'touchmove', 'mousedown', 'mouseup', 'mousemove', 'DOMMouseScroll', 'mousewheel', 'wheel', 'mouseout'].forEach(function(event) {
        Module['canvas'].addEventListener(event, SDL.receiveEvent, true);
      });
  
      var canvas = Module['canvas'];
  
      // (0,0) means 'use fullscreen' in native; in Emscripten, use the current canvas size.
      if (width == 0 && height == 0) {
        width = canvas.width;
        height = canvas.height;
      }
  
      if (!SDL.addedResizeListener) {
        SDL.addedResizeListener = true;
        Browser.resizeListeners.push(function(w, h) {
          if (!SDL.settingVideoMode) {
            SDL.receiveEvent({
              type: 'resize',
              w: w,
              h: h
            });
          }
        });
      }
  
      if (width !== canvas.width || height !== canvas.height) {
        SDL.settingVideoMode = true; // SetVideoMode itself should not trigger resize events
        Browser.setCanvasSize(width, height);
        SDL.settingVideoMode = false;
      }
  
      // Free the old surface first if there is one
      if (SDL.screen) {
        SDL.freeSurface(SDL.screen);
        assert(!SDL.screen);
      }
  
      if (SDL.GL) flags = flags | 0x04000000; // SDL_OPENGL - if we are using GL, then later calls to SetVideoMode may not mention GL, but we do need it. Once in GL mode, we never leave it.
  
      SDL.screen = SDL.makeSurface(width, height, flags, true, 'screen');
  
      return SDL.screen;
    }



  
  function _emscripten_memcpy_big(dest, src, num) {
      HEAPU8.set(HEAPU8.subarray(src, src+num), dest);
      return dest;
    } 
  Module["_memcpy"] = _memcpy;

  function _SDL_PollEvent(ptr) {
      return SDL.pollEvent(ptr);
    }

  function _SDL_UnlockSurface(surf) {
      assert(!SDL.GL); // in GL mode we do not keep around 2D canvases and contexts
  
      var surfData = SDL.surfaces[surf];
  
      if (!surfData.locked || --surfData.locked > 0) {
        return;
      }
  
      // Copy pixel data to image
      if (surfData.isFlagSet(0x00200000 /* SDL_HWPALETTE */)) {
        SDL.copyIndexedColorData(surfData);
      } else if (!surfData.colors) {
        var data = surfData.image.data;
        var buffer = surfData.buffer;
        assert(buffer % 4 == 0, 'Invalid buffer offset: ' + buffer);
        var src = buffer >> 2;
        var dst = 0;
        var isScreen = surf == SDL.screen;
        var num;
        if (typeof CanvasPixelArray !== 'undefined' && data instanceof CanvasPixelArray) {
          // IE10/IE11: ImageData objects are backed by the deprecated CanvasPixelArray,
          // not UInt8ClampedArray. These don't have buffers, so we need to revert
          // to copying a byte at a time. We do the undefined check because modern
          // browsers do not define CanvasPixelArray anymore.
          num = data.length;
          while (dst < num) {
            var val = HEAP32[src]; // This is optimized. Instead, we could do HEAP32[(((buffer)+(dst))>>2)];
            data[dst  ] = val & 0xff;
            data[dst+1] = (val >> 8) & 0xff;
            data[dst+2] = (val >> 16) & 0xff;
            data[dst+3] = isScreen ? 0xff : ((val >> 24) & 0xff);
            src++;
            dst += 4;
          }
        } else {
          var data32 = new Uint32Array(data.buffer);
          if (isScreen && SDL.defaults.opaqueFrontBuffer) {
            num = data32.length;
            // logically we need to do
            //      while (dst < num) {
            //          data32[dst++] = HEAP32[src++] | 0xff000000
            //      }
            // the following code is faster though, because
            // .set() is almost free - easily 10x faster due to
            // native memcpy efficiencies, and the remaining loop
            // just stores, not load + store, so it is faster
            data32.set(HEAP32.subarray(src, src + num));
            var data8 = new Uint8Array(data.buffer);
            var i = 3;
            var j = i + 4*num;
            if (num % 8 == 0) {
              // unrolling gives big speedups
              while (i < j) {
                data8[i] = 0xff;
                i = i + 4 | 0;
                data8[i] = 0xff;
                i = i + 4 | 0;
                data8[i] = 0xff;
                i = i + 4 | 0;
                data8[i] = 0xff;
                i = i + 4 | 0;
                data8[i] = 0xff;
                i = i + 4 | 0;
                data8[i] = 0xff;
                i = i + 4 | 0;
                data8[i] = 0xff;
                i = i + 4 | 0;
                data8[i] = 0xff;
                i = i + 4 | 0;
              }
             } else {
              while (i < j) {
                data8[i] = 0xff;
                i = i + 4 | 0;
              }
            }
          } else {
            data32.set(HEAP32.subarray(src, src + data32.length));
          }
        }
      } else {
        var width = Module['canvas'].width;
        var height = Module['canvas'].height;
        var s = surfData.buffer;
        var data = surfData.image.data;
        var colors = surfData.colors; // TODO: optimize using colors32
        for (var y = 0; y < height; y++) {
          var base = y*width*4;
          for (var x = 0; x < width; x++) {
            // See comment above about signs
            var val = HEAPU8[((s++)>>0)] * 4;
            var start = base + x*4;
            data[start]   = colors[val];
            data[start+1] = colors[val+1];
            data[start+2] = colors[val+2];
          }
          s += width*3;
        }
      }
      // Copy to canvas
      surfData.ctx.putImageData(surfData.image, 0, 0);
      // Note that we save the image, so future writes are fast. But, memory is not yet released
    }

  function _SDL_Init(initFlags) {
      SDL.startTime = Date.now();
      SDL.initFlags = initFlags;
  
      // capture all key events. we just keep down and up, but also capture press to prevent default actions
      if (!Module['doNotCaptureKeyboard']) {
        var keyboardListeningElement = Module['keyboardListeningElement'] || document;
        keyboardListeningElement.addEventListener("keydown", SDL.receiveEvent);
        keyboardListeningElement.addEventListener("keyup", SDL.receiveEvent);
        keyboardListeningElement.addEventListener("keypress", SDL.receiveEvent);
        window.addEventListener("focus", SDL.receiveEvent);
        window.addEventListener("blur", SDL.receiveEvent);
        document.addEventListener("visibilitychange", SDL.receiveEvent);
      }
  
      if (initFlags & 0x200) {
        // SDL_INIT_JOYSTICK
        // Firefox will not give us Joystick data unless we register this NOP
        // callback.
        // https://bugzilla.mozilla.org/show_bug.cgi?id=936104
        addEventListener("gamepadconnected", function() {});
      }
  
      window.addEventListener("unload", SDL.receiveEvent);
      SDL.keyboardState = _malloc(0x10000); // Our SDL needs 512, but 64K is safe for older SDLs
      _memset(SDL.keyboardState, 0, 0x10000);
      // Initialize this structure carefully for closure
      SDL.DOMEventToSDLEvent['keydown']    = 0x300  /* SDL_KEYDOWN */;
      SDL.DOMEventToSDLEvent['keyup']      = 0x301  /* SDL_KEYUP */;
      SDL.DOMEventToSDLEvent['keypress']   = 0x303  /* SDL_TEXTINPUT */;
      SDL.DOMEventToSDLEvent['mousedown']  = 0x401  /* SDL_MOUSEBUTTONDOWN */;
      SDL.DOMEventToSDLEvent['mouseup']    = 0x402  /* SDL_MOUSEBUTTONUP */;
      SDL.DOMEventToSDLEvent['mousemove']  = 0x400  /* SDL_MOUSEMOTION */;
      SDL.DOMEventToSDLEvent['wheel']      = 0x403  /* SDL_MOUSEWHEEL */; 
      SDL.DOMEventToSDLEvent['touchstart'] = 0x700  /* SDL_FINGERDOWN */;
      SDL.DOMEventToSDLEvent['touchend']   = 0x701  /* SDL_FINGERUP */;
      SDL.DOMEventToSDLEvent['touchmove']  = 0x702  /* SDL_FINGERMOTION */;
      SDL.DOMEventToSDLEvent['unload']     = 0x100  /* SDL_QUIT */;
      SDL.DOMEventToSDLEvent['resize']     = 0x7001 /* SDL_VIDEORESIZE/SDL_EVENT_COMPAT2 */;
      SDL.DOMEventToSDLEvent['visibilitychange'] = 0x200 /* SDL_WINDOWEVENT */;
      SDL.DOMEventToSDLEvent['focus']      = 0x200 /* SDL_WINDOWEVENT */;
      SDL.DOMEventToSDLEvent['blur']       = 0x200 /* SDL_WINDOWEVENT */;
  
      // These are not technically DOM events; the HTML gamepad API is poll-based.
      // However, we define them here, as the rest of the SDL code assumes that
      // all SDL events originate as DOM events.
      SDL.DOMEventToSDLEvent['joystick_axis_motion'] = 0x600 /* SDL_JOYAXISMOTION */;
      SDL.DOMEventToSDLEvent['joystick_button_down'] = 0x603 /* SDL_JOYBUTTONDOWN */;
      SDL.DOMEventToSDLEvent['joystick_button_up'] = 0x604 /* SDL_JOYBUTTONUP */;
      return 0; // success
    }


  function _SDL_Flip(surf) {
      // We actually do this in Unlock, since the screen surface has as its canvas
      // backing the page canvas element
    }

  function ___errno_location() {
      return ___errno_state;
    }

  function _SDL_MapRGBA(fmt, r, g, b, a) {
      SDL.checkPixelFormat(fmt);
      // We assume the machine is little-endian.
      return r&0xff|(g&0xff)<<8|(b&0xff)<<16|(a&0xff)<<24;
    }



  function _sbrk(bytes) {
      // Implement a Linux-like 'memory area' for our 'process'.
      // Changes the size of the memory area by |bytes|; returns the
      // address of the previous top ('break') of the memory area
      // We control the "dynamic" memory - DYNAMIC_BASE to DYNAMICTOP
      var self = _sbrk;
      if (!self.called) {
        DYNAMICTOP = alignMemoryPage(DYNAMICTOP); // make sure we start out aligned
        self.called = true;
        assert(Runtime.dynamicAlloc);
        self.alloc = Runtime.dynamicAlloc;
        Runtime.dynamicAlloc = function() { abort('cannot dynamically allocate, sbrk now has control') };
      }
      var ret = DYNAMICTOP;
      if (bytes != 0) {
        var success = self.alloc(bytes);
        if (!success) return -1 >>> 0; // sbrk failure code
      }
      return ret;  // Previous break location.
    }

  function _SDL_FreeSurface(surf) {
      if (surf) SDL.freeSurface(surf);
    }

  function _time(ptr) {
      var ret = (Date.now()/1000)|0;
      if (ptr) {
        HEAP32[((ptr)>>2)]=ret;
      }
      return ret;
    }

  function _SDL_CreateRGBSurface(flags, width, height, depth, rmask, gmask, bmask, amask) {
      return SDL.makeSurface(width, height, flags, false, 'CreateRGBSurface', rmask, gmask, bmask, amask);
    }
___errno_state = Runtime.staticAlloc(4); HEAP32[((___errno_state)>>2)]=0;
FS.staticInit();__ATINIT__.unshift({ func: function() { if (!Module["noFSInit"] && !FS.init.initialized) FS.init() } });__ATMAIN__.push({ func: function() { FS.ignorePermissions = false } });__ATEXIT__.push({ func: function() { FS.quit() } });Module["FS_createFolder"] = FS.createFolder;Module["FS_createPath"] = FS.createPath;Module["FS_createDataFile"] = FS.createDataFile;Module["FS_createPreloadedFile"] = FS.createPreloadedFile;Module["FS_createLazyFile"] = FS.createLazyFile;Module["FS_createLink"] = FS.createLink;Module["FS_createDevice"] = FS.createDevice;
__ATINIT__.unshift({ func: function() { TTY.init() } });__ATEXIT__.push({ func: function() { TTY.shutdown() } });
if (ENVIRONMENT_IS_NODE) { var fs = require("fs"); var NODEJS_PATH = require("path"); NODEFS.staticInit(); }
__ATINIT__.push({ func: function() { SOCKFS.root = FS.mount(SOCKFS, {}, null); } });
var GLctx; GL.init()
Module["requestFullScreen"] = function Module_requestFullScreen(lockPointer, resizeCanvas, vrDevice) { Browser.requestFullScreen(lockPointer, resizeCanvas, vrDevice) };
  Module["requestAnimationFrame"] = function Module_requestAnimationFrame(func) { Browser.requestAnimationFrame(func) };
  Module["setCanvasSize"] = function Module_setCanvasSize(width, height, noUpdates) { Browser.setCanvasSize(width, height, noUpdates) };
  Module["pauseMainLoop"] = function Module_pauseMainLoop() { Browser.mainLoop.pause() };
  Module["resumeMainLoop"] = function Module_resumeMainLoop() { Browser.mainLoop.resume() };
  Module["getUserMedia"] = function Module_getUserMedia() { Browser.getUserMedia() }
___buildEnvironment(ENV);
STACK_BASE = STACKTOP = Runtime.alignMemory(STATICTOP);

staticSealed = true; // seal the static portion of memory

STACK_MAX = STACK_BASE + TOTAL_STACK;

DYNAMIC_BASE = DYNAMICTOP = Runtime.alignMemory(STACK_MAX);

assert(DYNAMIC_BASE < TOTAL_MEMORY, "TOTAL_MEMORY not big enough for stack");



function nullFunc_vi(x) { Module["printErr"]("Invalid function pointer called with signature 'vi'. Perhaps this is an invalid value (e.g. caused by calling a virtual method on a NULL pointer)? Or calling a function with an incorrect type, which will fail? (it is worth building your source files with -Werror (warnings are errors), as warnings can indicate undefined behavior which can cause this)");  Module["printErr"]("Build with ASSERTIONS=2 for more info."); abort(x) }

function nullFunc_v(x) { Module["printErr"]("Invalid function pointer called with signature 'v'. Perhaps this is an invalid value (e.g. caused by calling a virtual method on a NULL pointer)? Or calling a function with an incorrect type, which will fail? (it is worth building your source files with -Werror (warnings are errors), as warnings can indicate undefined behavior which can cause this)");  Module["printErr"]("Build with ASSERTIONS=2 for more info."); abort(x) }

function invoke_vi(index,a1) {
  try {
    Module["dynCall_vi"](index,a1);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}

function invoke_v(index) {
  try {
    Module["dynCall_v"](index);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}

Module.asmGlobalArg = { "Math": Math, "Int8Array": Int8Array, "Int16Array": Int16Array, "Int32Array": Int32Array, "Uint8Array": Uint8Array, "Uint16Array": Uint16Array, "Uint32Array": Uint32Array, "Float32Array": Float32Array, "Float64Array": Float64Array, "NaN": NaN, "Infinity": Infinity };
Module.asmLibraryArg = { "abort": abort, "assert": assert, "nullFunc_vi": nullFunc_vi, "nullFunc_v": nullFunc_v, "invoke_vi": invoke_vi, "invoke_v": invoke_v, "_Mix_LoadWAV_RW": _Mix_LoadWAV_RW, "_putenv": _putenv, "_IMG_Load_RW": _IMG_Load_RW, "_send": _send, "_SDL_SetVideoMode": _SDL_SetVideoMode, "_IMG_Load": _IMG_Load, "_TTF_FontHeight": _TTF_FontHeight, "_SDL_CloseAudio": _SDL_CloseAudio, "__reallyNegative": __reallyNegative, "_SDL_GetTicks": _SDL_GetTicks, "_SDL_Flip": _SDL_Flip, "___buildEnvironment": ___buildEnvironment, "_fflush": _fflush, "_SDL_LockSurface": _SDL_LockSurface, "_Mix_HaltMusic": _Mix_HaltMusic, "_emscripten_set_main_loop_timing": _emscripten_set_main_loop_timing, "_SDL_PollEvent": _SDL_PollEvent, "_SDL_Init": _SDL_Init, "_SDL_FreeSurface": _SDL_FreeSurface, "_Mix_PlayChannel": _Mix_PlayChannel, "_TTF_RenderText_Solid": _TTF_RenderText_Solid, "_fileno": _fileno, "_Mix_FreeChunk": _Mix_FreeChunk, "_sysconf": _sysconf, "___setErrNo": ___setErrNo, "_Mix_PlayMusic": _Mix_PlayMusic, "_emscripten_memcpy_big": _emscripten_memcpy_big, "_pwrite": _pwrite, "_TTF_SizeText": _TTF_SizeText, "_SDL_UpperBlit": _SDL_UpperBlit, "_SDL_PauseAudio": _SDL_PauseAudio, "_write": _write, "_emscripten_set_main_loop": _emscripten_set_main_loop, "___errno_location": ___errno_location, "_SDL_CreateRGBSurface": _SDL_CreateRGBSurface, "_mkport": _mkport, "_SDL_UnlockSurface": _SDL_UnlockSurface, "_abort": _abort, "_fwrite": _fwrite, "_time": _time, "_fprintf": _fprintf, "_SDL_MapRGBA": _SDL_MapRGBA, "_sbrk": _sbrk, "__formatString": __formatString, "_SDL_FreeRW": _SDL_FreeRW, "_SDL_UpperBlitScaled": _SDL_UpperBlitScaled, "_printf": _printf, "_SDL_RWFromConstMem": _SDL_RWFromConstMem, "_getenv": _getenv, "_SDL_RWFromFile": _SDL_RWFromFile, "STACKTOP": STACKTOP, "STACK_MAX": STACK_MAX, "tempDoublePtr": tempDoublePtr, "ABORT": ABORT };
// EMSCRIPTEN_START_ASM
var asm = (function(global, env, buffer) {
  'almost asm';
  
  var HEAP8 = new global.Int8Array(buffer);
  var HEAP16 = new global.Int16Array(buffer);
  var HEAP32 = new global.Int32Array(buffer);
  var HEAPU8 = new global.Uint8Array(buffer);
  var HEAPU16 = new global.Uint16Array(buffer);
  var HEAPU32 = new global.Uint32Array(buffer);
  var HEAPF32 = new global.Float32Array(buffer);
  var HEAPF64 = new global.Float64Array(buffer);


  var STACKTOP=env.STACKTOP|0;
  var STACK_MAX=env.STACK_MAX|0;
  var tempDoublePtr=env.tempDoublePtr|0;
  var ABORT=env.ABORT|0;

  var __THREW__ = 0;
  var threwValue = 0;
  var setjmpId = 0;
  var undef = 0;
  var nan = global.NaN, inf = global.Infinity;
  var tempInt = 0, tempBigInt = 0, tempBigIntP = 0, tempBigIntS = 0, tempBigIntR = 0.0, tempBigIntI = 0, tempBigIntD = 0, tempValue = 0, tempDouble = 0.0;

  var tempRet0 = 0;
  var tempRet1 = 0;
  var tempRet2 = 0;
  var tempRet3 = 0;
  var tempRet4 = 0;
  var tempRet5 = 0;
  var tempRet6 = 0;
  var tempRet7 = 0;
  var tempRet8 = 0;
  var tempRet9 = 0;
  var Math_floor=global.Math.floor;
  var Math_abs=global.Math.abs;
  var Math_sqrt=global.Math.sqrt;
  var Math_pow=global.Math.pow;
  var Math_cos=global.Math.cos;
  var Math_sin=global.Math.sin;
  var Math_tan=global.Math.tan;
  var Math_acos=global.Math.acos;
  var Math_asin=global.Math.asin;
  var Math_atan=global.Math.atan;
  var Math_atan2=global.Math.atan2;
  var Math_exp=global.Math.exp;
  var Math_log=global.Math.log;
  var Math_ceil=global.Math.ceil;
  var Math_imul=global.Math.imul;
  var Math_min=global.Math.min;
  var Math_clz32=global.Math.clz32;
  var abort=env.abort;
  var assert=env.assert;
  var nullFunc_vi=env.nullFunc_vi;
  var nullFunc_v=env.nullFunc_v;
  var invoke_vi=env.invoke_vi;
  var invoke_v=env.invoke_v;
  var _Mix_LoadWAV_RW=env._Mix_LoadWAV_RW;
  var _putenv=env._putenv;
  var _IMG_Load_RW=env._IMG_Load_RW;
  var _send=env._send;
  var _SDL_SetVideoMode=env._SDL_SetVideoMode;
  var _IMG_Load=env._IMG_Load;
  var _TTF_FontHeight=env._TTF_FontHeight;
  var _SDL_CloseAudio=env._SDL_CloseAudio;
  var __reallyNegative=env.__reallyNegative;
  var _SDL_GetTicks=env._SDL_GetTicks;
  var _SDL_Flip=env._SDL_Flip;
  var ___buildEnvironment=env.___buildEnvironment;
  var _fflush=env._fflush;
  var _SDL_LockSurface=env._SDL_LockSurface;
  var _Mix_HaltMusic=env._Mix_HaltMusic;
  var _emscripten_set_main_loop_timing=env._emscripten_set_main_loop_timing;
  var _SDL_PollEvent=env._SDL_PollEvent;
  var _SDL_Init=env._SDL_Init;
  var _SDL_FreeSurface=env._SDL_FreeSurface;
  var _Mix_PlayChannel=env._Mix_PlayChannel;
  var _TTF_RenderText_Solid=env._TTF_RenderText_Solid;
  var _fileno=env._fileno;
  var _Mix_FreeChunk=env._Mix_FreeChunk;
  var _sysconf=env._sysconf;
  var ___setErrNo=env.___setErrNo;
  var _Mix_PlayMusic=env._Mix_PlayMusic;
  var _emscripten_memcpy_big=env._emscripten_memcpy_big;
  var _pwrite=env._pwrite;
  var _TTF_SizeText=env._TTF_SizeText;
  var _SDL_UpperBlit=env._SDL_UpperBlit;
  var _SDL_PauseAudio=env._SDL_PauseAudio;
  var _write=env._write;
  var _emscripten_set_main_loop=env._emscripten_set_main_loop;
  var ___errno_location=env.___errno_location;
  var _SDL_CreateRGBSurface=env._SDL_CreateRGBSurface;
  var _mkport=env._mkport;
  var _SDL_UnlockSurface=env._SDL_UnlockSurface;
  var _abort=env._abort;
  var _fwrite=env._fwrite;
  var _time=env._time;
  var _fprintf=env._fprintf;
  var _SDL_MapRGBA=env._SDL_MapRGBA;
  var _sbrk=env._sbrk;
  var __formatString=env.__formatString;
  var _SDL_FreeRW=env._SDL_FreeRW;
  var _SDL_UpperBlitScaled=env._SDL_UpperBlitScaled;
  var _printf=env._printf;
  var _SDL_RWFromConstMem=env._SDL_RWFromConstMem;
  var _getenv=env._getenv;
  var _SDL_RWFromFile=env._SDL_RWFromFile;
  var tempFloat = 0.0;

// EMSCRIPTEN_START_FUNCS
function stackAlloc(size) {
  size = size|0;
  var ret = 0;
  ret = STACKTOP;
  STACKTOP = (STACKTOP + size)|0;
STACKTOP = (STACKTOP + 15)&-16;
if ((STACKTOP|0) >= (STACK_MAX|0)) abort();

  return ret|0;
}
function stackSave() {
  return STACKTOP|0;
}
function stackRestore(top) {
  top = top|0;
  STACKTOP = top;
}

function setThrew(threw, value) {
  threw = threw|0;
  value = value|0;
  if ((__THREW__|0) == 0) {
    __THREW__ = threw;
    threwValue = value;
  }
}
function copyTempFloat(ptr) {
  ptr = ptr|0;
  HEAP8[tempDoublePtr>>0] = HEAP8[ptr>>0];
  HEAP8[tempDoublePtr+1>>0] = HEAP8[ptr+1>>0];
  HEAP8[tempDoublePtr+2>>0] = HEAP8[ptr+2>>0];
  HEAP8[tempDoublePtr+3>>0] = HEAP8[ptr+3>>0];
}
function copyTempDouble(ptr) {
  ptr = ptr|0;
  HEAP8[tempDoublePtr>>0] = HEAP8[ptr>>0];
  HEAP8[tempDoublePtr+1>>0] = HEAP8[ptr+1>>0];
  HEAP8[tempDoublePtr+2>>0] = HEAP8[ptr+2>>0];
  HEAP8[tempDoublePtr+3>>0] = HEAP8[ptr+3>>0];
  HEAP8[tempDoublePtr+4>>0] = HEAP8[ptr+4>>0];
  HEAP8[tempDoublePtr+5>>0] = HEAP8[ptr+5>>0];
  HEAP8[tempDoublePtr+6>>0] = HEAP8[ptr+6>>0];
  HEAP8[tempDoublePtr+7>>0] = HEAP8[ptr+7>>0];
}
function setTempRet0(value) {
  value = value|0;
  tempRet0 = value;
}
function getTempRet0() {
  return tempRet0|0;
}

function _ClearVram() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 return;
}
function _SetMasterVolume($vol) {
 $vol = $vol|0;
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $vol;
 STACKTOP = sp;return;
}
function _TriggerNote($channel,$patch,$note,$volume) {
 $channel = $channel|0;
 $patch = $patch|0;
 $note = $note|0;
 $volume = $volume|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $channel;
 $1 = $patch;
 $2 = $note;
 $3 = $volume;
 STACKTOP = sp;return;
}
function _TriggerFx($patch,$volume,$retrig) {
 $patch = $patch|0;
 $volume = $volume|0;
 $retrig = $retrig|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $patch;
 $1 = $volume;
 $3 = $retrig&1;
 $2 = $3;
 STACKTOP = sp;return;
}
function _InitMusicPlayer($patchPointersParam) {
 $patchPointersParam = $patchPointersParam|0;
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $patchPointersParam;
 STACKTOP = sp;return;
}
function _SetUserPreVsyncCallback($func) {
 $func = $func|0;
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $func;
 STACKTOP = sp;return;
}
function _ReadJoypad($joypadNo) {
 $joypadNo = $joypadNo|0;
 var $0 = 0, $1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $joypadNo;
 $1 = HEAP32[136>>2]|0;
 STACKTOP = sp;return ($1|0);
}
function _UpdateJoypad($state,$mappings,$numMappings,$eventType,$pressed) {
 $state = $state|0;
 $mappings = $mappings|0;
 $numMappings = $numMappings|0;
 $eventType = $eventType|0;
 $pressed = $pressed|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $n = 0;
 var label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $state;
 $1 = $mappings;
 $2 = $numMappings;
 $3 = $eventType;
 $5 = $pressed&1;
 $4 = $5;
 $n = 0;
 while(1) {
  $6 = $n;
  $7 = $2;
  $8 = ($6|0)<($7|0);
  if (!($8)) {
   break;
  }
  $9 = $n;
  $10 = $9<<1;
  $11 = $1;
  $12 = (($11) + ($10<<2)|0);
  $13 = HEAP32[$12>>2]|0;
  $14 = $3;
  $15 = ($13|0)==($14|0);
  if ($15) {
   $16 = $4;
   $17 = $16&1;
   if ($17) {
    $18 = $n;
    $19 = $18<<1;
    $20 = (($19) + 1)|0;
    $21 = $1;
    $22 = (($21) + ($20<<2)|0);
    $23 = HEAP32[$22>>2]|0;
    $24 = $0;
    $25 = HEAP32[$24>>2]|0;
    $26 = $25 | $23;
    HEAP32[$24>>2] = $26;
   } else {
    $27 = $n;
    $28 = $27<<1;
    $29 = (($28) + 1)|0;
    $30 = $1;
    $31 = (($30) + ($29<<2)|0);
    $32 = HEAP32[$31>>2]|0;
    $33 = $32 ^ -1;
    $34 = $0;
    $35 = HEAP32[$34>>2]|0;
    $36 = $35 & $33;
    HEAP32[$34>>2] = $36;
   }
  }
  $37 = $n;
  $38 = (($37) + 1)|0;
  $n = $38;
 }
 STACKTOP = sp;return;
}
function _Kernel_Init() {
 var $0 = 0, $1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 (_SDL_Init(65535)|0);
 $0 = (_SDL_SetVideoMode(224,216,32,0)|0);
 HEAP32[8>>2] = $0;
 _Initialize();
 _Game_Init();
 $1 = (_SDL_GetTicks()|0);
 HEAP32[24>>2] = $1;
 return;
}
function _Kernel_Update() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $event = 0, $framesUpdated = 0, $ticks = 0, $ticksPassed = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 64|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $event = sp;
 $0 = (_SDL_GetTicks()|0);
 $ticks = $0;
 $1 = $ticks;
 $2 = HEAP32[24>>2]|0;
 $3 = (($1) - ($2))|0;
 $ticksPassed = $3;
 $4 = $ticks;
 HEAP32[24>>2] = $4;
 $5 = $ticksPassed;
 $6 = HEAP32[32>>2]|0;
 $7 = (($6) + ($5))|0;
 HEAP32[32>>2] = $7;
 $framesUpdated = 0;
 while(1) {
  $8 = HEAP32[32>>2]|0;
  $9 = ($8|0)>(0);
  if ($9) {
   $10 = $framesUpdated;
   $11 = ($10|0)<(15);
   $26 = $11;
  } else {
   $26 = 0;
  }
  if (!($26)) {
   break;
  }
  _Game_Update();
  $12 = HEAP32[32>>2]|0;
  $13 = (($12) - 16)|0;
  HEAP32[32>>2] = $13;
  $14 = $framesUpdated;
  $15 = (($14) + 1)|0;
  $framesUpdated = $15;
 }
 while(1) {
  $16 = (_SDL_PollEvent(($event|0))|0);
  $17 = ($16|0)!=(0);
  if (!($17)) {
   break;
  }
  $18 = HEAP32[$event>>2]|0;
  if ((($18|0) == 256)) {
   HEAP8[16>>0] = 0;
  } else if ((($18|0) == 768)) {
   $19 = (($event) + 12|0);
   $20 = (($19) + 4|0);
   $21 = HEAP32[$20>>2]|0;
   _UpdateJoypad(136,40,48,$21,1);
  } else if ((($18|0) == 769)) {
   $22 = (($event) + 12|0);
   $23 = (($22) + 4|0);
   $24 = HEAP32[$23>>2]|0;
   _UpdateJoypad(136,40,48,$24,0);
  } else {
  }
 }
 _Video_Render();
 $25 = HEAP32[8>>2]|0;
 (_SDL_Flip(($25|0))|0);
 STACKTOP = sp;return;
}
function _main() {
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = 0;
 _Kernel_Init();
 _emscripten_set_main_loop((6|0),0,1);
 STACKTOP = sp;return 0;
}
function _DisplayLogo() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 _InitMusicPlayer(168);
 _SetTileTable(192);
 _ClearVram();
 _WaitVsync(15);
 _TriggerFx(0,-1,1);
 _DrawMap2(12,12,2240);
 _WaitVsync(3);
 _DrawMap2(12,12,2264);
 _WaitVsync(2);
 _DrawMap2(12,12,2240);
 _WaitVsync(65);
 _ClearVram();
 _WaitVsync(20);
 return;
}
function _SetTileTable($data) {
 $data = $data|0;
 var $0 = 0, $1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $data;
 $1 = $0;
 _Video_SetBank(7592,$1);
 STACKTOP = sp;return;
}
function _WaitVsync($count) {
 $count = $count|0;
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $count;
 STACKTOP = sp;return;
}
function _DrawMap2($x,$y,$map) {
 $x = $x|0;
 $y = $y|0;
 $map = $map|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $dx = 0, $dy = 0, $mapHeight = 0, $mapWidth = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $x;
 $1 = $y;
 $2 = $map;
 $3 = $2;
 $4 = HEAP8[$3>>0]|0;
 $mapWidth = $4;
 $5 = $2;
 $6 = (($5) + 1|0);
 $7 = HEAP8[$6>>0]|0;
 $mapHeight = $7;
 $dy = 0;
 while(1) {
  $8 = $dy;
  $9 = $8&255;
  $10 = $mapHeight;
  $11 = $10&255;
  $12 = ($9|0)<($11|0);
  if (!($12)) {
   break;
  }
  $dx = 0;
  while(1) {
   $13 = $dx;
   $14 = $13&255;
   $15 = $mapWidth;
   $16 = $15&255;
   $17 = ($14|0)<($16|0);
   if (!($17)) {
    break;
   }
   $18 = $0;
   $19 = $18&255;
   $20 = $dx;
   $21 = $20&255;
   $22 = (($19) + ($21))|0;
   $23 = $22&255;
   $24 = $1;
   $25 = $24&255;
   $26 = $dy;
   $27 = $26&255;
   $28 = (($25) + ($27))|0;
   $29 = $28&255;
   $30 = $dy;
   $31 = $30&255;
   $32 = $mapWidth;
   $33 = $32&255;
   $34 = Math_imul($31, $33)|0;
   $35 = $dx;
   $36 = $35&255;
   $37 = (($34) + ($36))|0;
   $38 = (($37) + 2)|0;
   $39 = $2;
   $40 = (($39) + ($38)|0);
   $41 = HEAP8[$40>>0]|0;
   $42 = $41&255;
   _SetTile($23,$29,$42);
   $43 = $dx;
   $44 = (($43) + 1)<<24>>24;
   $dx = $44;
  }
  $45 = $dy;
  $46 = (($45) + 1)<<24>>24;
  $dy = $46;
 }
 STACKTOP = sp;return;
}
function _SetFont($x,$y,$tileId) {
 $x = $x|0;
 $y = $y|0;
 $tileId = $tileId|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $x;
 $1 = $y;
 $2 = $tileId;
 $3 = $0;
 $4 = $1;
 $5 = $2;
 $6 = $5&255;
 $7 = HEAP8[2400>>0]|0;
 $8 = $7&255;
 $9 = (($6) + ($8))|0;
 _SetTile($3,$4,$9);
 STACKTOP = sp;return;
}
function _SetTile($x,$y,$tileId) {
 $x = $x|0;
 $y = $y|0;
 $tileId = $tileId|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $x;
 $1 = $y;
 $2 = $tileId;
 $3 = $2;
 $4 = $3&255;
 $5 = $1;
 $6 = $5 << 24 >> 24;
 $7 = $6<<5;
 $8 = $0;
 $9 = $8 << 24 >> 24;
 $10 = (($7) + ($9))|0;
 $11 = (2408 + ($10)|0);
 HEAP8[$11>>0] = $4;
 STACKTOP = sp;return;
}
function _SetFontTilesIndex($index) {
 $index = $index|0;
 var $0 = 0, $1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $index;
 $1 = $0;
 HEAP8[2400>>0] = $1;
 STACKTOP = sp;return;
}
function _Video_SetBank($bank,$data) {
 $bank = $bank|0;
 $data = $data|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0;
 var $7 = 0, $8 = 0, $9 = 0, $n = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $bank;
 $1 = $data;
 $2 = $1;
 $3 = $0;
 HEAP32[$3>>2] = $2;
 $n = 0;
 while(1) {
  $4 = $n;
  $5 = ($4|0)<(256);
  if (!($5)) {
   break;
  }
  $6 = $n;
  $7 = $0;
  $8 = (($7) + 4|0);
  $9 = (($8) + ($6<<2)|0);
  $10 = HEAP32[$9>>2]|0;
  $11 = ($10|0)!=(0|0);
  if ($11) {
   $12 = $n;
   $13 = $0;
   $14 = (($13) + 4|0);
   $15 = (($14) + ($12<<2)|0);
   $16 = HEAP32[$15>>2]|0;
   _SDL_FreeSurface(($16|0));
   $17 = $n;
   $18 = $0;
   $19 = (($18) + 4|0);
   $20 = (($19) + ($17<<2)|0);
   HEAP32[$20>>2] = 0;
  }
  $21 = $n;
  $22 = (($21) + 1)|0;
  $n = $22;
 }
 STACKTOP = sp;return;
}
function _Video_InitBank($bank,$transparent,$flip) {
 $bank = $bank|0;
 $transparent = $transparent|0;
 $flip = $flip|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $bank;
 $3 = $transparent&1;
 $1 = $3;
 $2 = $flip;
 $4 = $0;
 _Video_SetBank($4,0);
 $5 = $1;
 $6 = $5&1;
 $7 = $0;
 $8 = (($7) + 1028|0);
 $9 = $6&1;
 HEAP8[$8>>0] = $9;
 $10 = $2;
 $11 = $0;
 $12 = (($11) + 1032|0);
 HEAP32[$12>>2] = $10;
 STACKTOP = sp;return;
}
function _SetSpritesTileTable($data) {
 $data = $data|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $data;
 $1 = $0;
 _Video_SetBank(3432,$1);
 $2 = $0;
 _Video_SetBank(4472,$2);
 $3 = $0;
 _Video_SetBank(5512,$3);
 $4 = $0;
 _Video_SetBank(6552,$4);
 STACKTOP = sp;return;
}
function _InitializeVideoMode() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $i = 0, $vararg_buffer = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $vararg_buffer = sp;
 (_printf((8632|0),($vararg_buffer|0))|0);
 $i = 0;
 while(1) {
  $0 = $i;
  $1 = ($0|0)<(18);
  if (!($1)) {
   break;
  }
  $2 = $i;
  $3 = (2304 + (($2*5)|0)|0);
  HEAP8[$3>>0] = -32;
  $4 = $i;
  $5 = (($4) + 1)|0;
  $i = $5;
 }
 HEAP8[((2288 + 10|0))>>0] = 32;
 HEAP8[2288>>0] = 0;
 _Video_InitBank(7592,0,0);
 _Video_InitBank(8640,0,0);
 _Video_InitBank(3432,1,0);
 _Video_InitBank(4472,1,1);
 _Video_InitBank(5512,1,2);
 _Video_InitBank(6552,1,3);
 $6 = HEAP32[8>>2]|0;
 HEAP32[9680>>2] = $6;
 STACKTOP = sp;return;
}
function _Video_PutPixel($surface,$x,$y,$pixel) {
 $surface = $surface|0;
 $x = $x|0;
 $y = $y|0;
 $pixel = $pixel|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $3 = 0, $4 = 0;
 var $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $bpp = 0, $p = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $surface;
 $1 = $x;
 $2 = $y;
 $3 = $pixel;
 $4 = $0;
 $5 = (($4) + 4|0);
 $6 = HEAP32[$5>>2]|0;
 $7 = (($6) + 9|0);
 $8 = HEAP8[$7>>0]|0;
 $9 = $8&255;
 $bpp = $9;
 $10 = $0;
 $11 = (($10) + 20|0);
 $12 = HEAP32[$11>>2]|0;
 $13 = $2;
 $14 = $0;
 $15 = (($14) + 16|0);
 $16 = HEAP32[$15>>2]|0;
 $17 = Math_imul($13, $16)|0;
 $18 = (($12) + ($17)|0);
 $19 = $1;
 $20 = $bpp;
 $21 = Math_imul($19, $20)|0;
 $22 = (($18) + ($21)|0);
 $p = $22;
 $23 = $3;
 $24 = $p;
 HEAP32[$24>>2] = $23;
 STACKTOP = sp;return;
}
function _Video_GetColour($surface,$i) {
 $surface = $surface|0;
 $i = $i|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $blue = 0, $green = 0, $red = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $surface;
 $1 = $i;
 $2 = $1;
 $3 = $2 >> 0;
 $4 = $3 & 7;
 $5 = ($4*255)|0;
 $6 = (($5|0) / 7)&-1;
 $red = $6;
 $7 = $1;
 $8 = $7 >> 3;
 $9 = $8 & 7;
 $10 = ($9*255)|0;
 $11 = (($10|0) / 7)&-1;
 $green = $11;
 $12 = $1;
 $13 = $12 >> 6;
 $14 = $13 & 3;
 $15 = ($14*255)|0;
 $16 = (($15|0) / 3)&-1;
 $blue = $16;
 $17 = $0;
 $18 = (($17) + 4|0);
 $19 = HEAP32[$18>>2]|0;
 $20 = $red;
 $21 = $20&255;
 $22 = $green;
 $23 = $22&255;
 $24 = $blue;
 $25 = $24&255;
 $26 = (_SDL_MapRGBA(($19|0),($21|0),($23|0),($25|0),-1)|0);
 STACKTOP = sp;return ($26|0);
}
function _Video_CreateTileSurface($bank,$index) {
 $bank = $bank|0;
 $index = $index|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 var $amask = 0, $bmask = 0, $gmask = 0, $i = 0, $j = 0, $pixel = 0, $rmask = 0, $surface = 0, $tileData = 0, $x = 0, $y = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 64|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $bank;
 $1 = $index;
 $rmask = 255;
 $gmask = 65280;
 $bmask = 16711680;
 $amask = -16777216;
 $2 = $rmask;
 $3 = $gmask;
 $4 = $bmask;
 $5 = $amask;
 $6 = (_SDL_CreateRGBSurface(0,8,8,32,($2|0),($3|0),($4|0),($5|0))|0);
 $surface = $6;
 $7 = $surface;
 (_SDL_LockSurface(($7|0))|0);
 $8 = $0;
 $9 = HEAP32[$8>>2]|0;
 $10 = $1;
 $11 = $10<<6;
 $12 = (($9) + ($11)|0);
 $tileData = $12;
 $y = 0;
 while(1) {
  $13 = $y;
  $14 = ($13|0)<(8);
  if (!($14)) {
   break;
  }
  $x = 0;
  while(1) {
   $15 = $x;
   $16 = ($15|0)<(8);
   if (!($16)) {
    break;
   }
   $17 = $x;
   $i = $17;
   $18 = $y;
   $j = $18;
   $19 = $0;
   $20 = (($19) + 1032|0);
   $21 = HEAP32[$20>>2]|0;
   $22 = $21 & 1;
   $23 = ($22|0)!=(0);
   if ($23) {
    $24 = $x;
    $25 = (7 - ($24))|0;
    $i = $25;
   }
   $26 = $0;
   $27 = (($26) + 1032|0);
   $28 = HEAP32[$27>>2]|0;
   $29 = $28 & 2;
   $30 = ($29|0)!=(0);
   if ($30) {
    $31 = $y;
    $32 = (7 - ($31))|0;
    $j = $32;
   }
   $33 = $tileData;
   $34 = HEAP8[$33>>0]|0;
   $pixel = $34;
   $35 = $pixel;
   $36 = $35&255;
   $37 = ($36|0)==(254);
   if ($37) {
    $38 = $0;
    $39 = (($38) + 1028|0);
    $40 = HEAP8[$39>>0]|0;
    $41 = $40&1;
    if ($41) {
     $42 = $surface;
     $43 = $i;
     $44 = $j;
     _Video_PutPixel($42,$43,$44,0);
    } else {
     label = 12;
    }
   } else {
    label = 12;
   }
   if ((label|0) == 12) {
    label = 0;
    $45 = $surface;
    $46 = $i;
    $47 = $j;
    $48 = $surface;
    $49 = $pixel;
    $50 = $49&255;
    $51 = (_Video_GetColour($48,$50)|0);
    _Video_PutPixel($45,$46,$47,$51);
   }
   $52 = $tileData;
   $53 = (($52) + 1|0);
   $tileData = $53;
   $54 = $x;
   $55 = (($54) + 1)|0;
   $x = $55;
  }
  $56 = $y;
  $57 = (($56) + 1)|0;
  $y = $57;
 }
 $58 = $surface;
 _SDL_UnlockSurface(($58|0));
 $59 = $surface;
 STACKTOP = sp;return ($59|0);
}
function _Video_GetBankSurface($bank,$index) {
 $bank = $bank|0;
 $index = $index|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $3 = 0, $4 = 0;
 var $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $bank;
 $2 = $index;
 $3 = $1;
 $4 = HEAP32[$3>>2]|0;
 $5 = ($4|0)==(0|0);
 if ($5) {
  $0 = 0;
  $24 = $0;
  STACKTOP = sp;return ($24|0);
 }
 $6 = $2;
 $7 = $1;
 $8 = (($7) + 4|0);
 $9 = (($8) + ($6<<2)|0);
 $10 = HEAP32[$9>>2]|0;
 $11 = ($10|0)!=(0|0);
 if (!($11)) {
  $12 = $1;
  $13 = $2;
  $14 = (_Video_CreateTileSurface($12,$13)|0);
  $15 = $2;
  $16 = $1;
  $17 = (($16) + 4|0);
  $18 = (($17) + ($15<<2)|0);
  HEAP32[$18>>2] = $14;
 }
 $19 = $2;
 $20 = $1;
 $21 = (($20) + 4|0);
 $22 = (($21) + ($19<<2)|0);
 $23 = HEAP32[$22>>2]|0;
 $0 = $23;
 $24 = $0;
 STACKTOP = sp;return ($24|0);
}
function _Video_DrawTile($targetSurface,$bank,$index,$x,$y) {
 $targetSurface = $targetSurface|0;
 $bank = $bank|0;
 $index = $index|0;
 $x = $x|0;
 $y = $y|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 var $targetRect = 0, $tileRect = 0, $tileSurface = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 64|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $tileRect = sp + 16|0;
 $targetRect = sp + 40|0;
 $0 = $targetSurface;
 $1 = $bank;
 $2 = $index;
 $3 = $x;
 $4 = $y;
 $5 = $1;
 $6 = $2;
 $7 = (_Video_GetBankSurface($5,$6)|0);
 $tileSurface = $7;
 $8 = $tileSurface;
 $9 = ($8|0)!=(0|0);
 if (!($9)) {
  STACKTOP = sp;return;
 }
 HEAP32[$tileRect>>2] = 0;
 $10 = (($tileRect) + 4|0);
 HEAP32[$10>>2] = 0;
 $11 = (($tileRect) + 8|0);
 HEAP32[$11>>2] = 8;
 $12 = (($tileRect) + 12|0);
 HEAP32[$12>>2] = 8;
 $13 = $3;
 HEAP32[$targetRect>>2] = $13;
 $14 = $4;
 $15 = (($targetRect) + 4|0);
 HEAP32[$15>>2] = $14;
 $16 = (($targetRect) + 8|0);
 HEAP32[$16>>2] = 8;
 $17 = (($targetRect) + 12|0);
 HEAP32[$17>>2] = 8;
 $18 = $tileSurface;
 $19 = $0;
 (_SDL_UpperBlit(($18|0),($tileRect|0),($19|0),($targetRect|0))|0);
 STACKTOP = sp;return;
}
function _Video_Render() {
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0;
 var $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0;
 var $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0;
 var $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0;
 var $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0;
 var $97 = 0, $98 = 0, $99 = 0, $bank = 0, $n = 0, $offsetX = 0, $offsetY = 0, $tile = 0, $tile3 = 0, $tileX = 0, $tileY = 0, $x = 0, $x2 = 0, $y = 0, $y1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 48|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = HEAP8[((2288 + 8|0))>>0]|0;
 $1 = $0&255;
 $2 = (($1|0) / 8)&-1;
 $tileX = $2;
 $3 = HEAP8[((2288 + 9|0))>>0]|0;
 $4 = $3&255;
 $5 = (($4|0) / 8)&-1;
 $6 = HEAP8[((2288 + 10|0))>>0]|0;
 $7 = $6&255;
 $8 = (($5|0) % ($7|0))&-1;
 $tileY = $8;
 $9 = HEAP8[((2288 + 8|0))>>0]|0;
 $10 = $9&255;
 $11 = (($10|0) % 8)&-1;
 $offsetX = $11;
 $12 = HEAP8[((2288 + 9|0))>>0]|0;
 $13 = $12&255;
 $14 = (($13|0) % 8)&-1;
 $offsetY = $14;
 $15 = $offsetX;
 $16 = (0 - ($15))|0;
 $x = $16;
 $17 = HEAP8[2288>>0]|0;
 $18 = $17&255;
 $19 = $18<<3;
 $20 = $offsetY;
 $21 = (($19) - ($20))|0;
 $y = $21;
 while(1) {
  $22 = $y;
  $23 = ($22|0)<(216);
  if (!($23)) {
   break;
  }
  $24 = $offsetX;
  $25 = (0 - ($24))|0;
  $x = $25;
  $26 = HEAP8[((2288 + 8|0))>>0]|0;
  $27 = $26&255;
  $28 = (($27|0) / 8)&-1;
  $tileX = $28;
  while(1) {
   $29 = $x;
   $30 = ($29|0)<(224);
   if (!($30)) {
    break;
   }
   $31 = $tileY;
   $32 = $31<<5;
   $33 = $tileX;
   $34 = (($32) + ($33))|0;
   $35 = (2408 + ($34)|0);
   $36 = HEAP8[$35>>0]|0;
   $tile = $36;
   $37 = HEAP32[9680>>2]|0;
   $38 = $tile;
   $39 = $38&255;
   $40 = $x;
   $41 = $y;
   _Video_DrawTile($37,7592,$39,$40,$41);
   $42 = $x;
   $43 = (($42) + 8)|0;
   $x = $43;
   $44 = $tileX;
   $45 = (($44) + 1)|0;
   $tileX = $45;
   $46 = $tileX;
   $47 = ($46|0)>=(32);
   if ($47) {
    $tileX = 0;
   }
  }
  $48 = $y;
  $49 = (($48) + 8)|0;
  $y = $49;
  $50 = $tileY;
  $51 = (($50) + 1)|0;
  $tileY = $51;
  $52 = $tileY;
  $53 = HEAP8[((2288 + 10|0))>>0]|0;
  $54 = $53&255;
  $55 = ($52|0)>=($54|0);
  if ($55) {
   $tileY = 0;
  }
 }
 $n = 0;
 while(1) {
  $56 = $n;
  $57 = ($56|0)<(18);
  if (!($57)) {
   break;
  }
  $58 = $n;
  $59 = (2304 + (($58*5)|0)|0);
  $60 = HEAP8[$59>>0]|0;
  $61 = $60&255;
  $62 = ($61|0)!=(224);
  if ($62) {
   $bank = 3432;
   $63 = $n;
   $64 = (2304 + (($63*5)|0)|0);
   $65 = (($64) + 3|0);
   $66 = HEAP8[$65>>0]|0;
   $67 = $66&255;
   $68 = $67 & 1;
   $69 = ($68|0)!=(0);
   if ($69) {
    $70 = $n;
    $71 = (2304 + (($70*5)|0)|0);
    $72 = (($71) + 3|0);
    $73 = HEAP8[$72>>0]|0;
    $74 = $73&255;
    $75 = $74 & 2;
    $76 = ($75|0)!=(0);
    if ($76) {
     $bank = 6552;
    } else {
     label = 17;
    }
   } else {
    label = 17;
   }
   if ((label|0) == 17) {
    label = 0;
    $77 = $n;
    $78 = (2304 + (($77*5)|0)|0);
    $79 = (($78) + 3|0);
    $80 = HEAP8[$79>>0]|0;
    $81 = $80&255;
    $82 = $81 & 1;
    $83 = ($82|0)!=(0);
    if ($83) {
     $bank = 4472;
    } else {
     $84 = $n;
     $85 = (2304 + (($84*5)|0)|0);
     $86 = (($85) + 3|0);
     $87 = HEAP8[$86>>0]|0;
     $88 = $87&255;
     $89 = $88 & 2;
     $90 = ($89|0)!=(0);
     if ($90) {
      $bank = 5512;
     }
    }
   }
   $91 = HEAP32[9680>>2]|0;
   $92 = $bank;
   $93 = $n;
   $94 = (2304 + (($93*5)|0)|0);
   $95 = (($94) + 2|0);
   $96 = HEAP8[$95>>0]|0;
   $97 = $96&255;
   $98 = $n;
   $99 = (2304 + (($98*5)|0)|0);
   $100 = HEAP8[$99>>0]|0;
   $101 = $100&255;
   $102 = $n;
   $103 = (2304 + (($102*5)|0)|0);
   $104 = (($103) + 1|0);
   $105 = HEAP8[$104>>0]|0;
   $106 = $105&255;
   $107 = HEAP8[2288>>0]|0;
   $108 = $107&255;
   $109 = $108<<3;
   $110 = (($106) + ($109))|0;
   _Video_DrawTile($91,$92,$97,$101,$110);
  }
  $111 = $n;
  $112 = (($111) + 1)|0;
  $n = $112;
 }
 $113 = HEAP32[((2288 + 4|0))>>2]|0;
 $114 = HEAP32[8640>>2]|0;
 $115 = ($113|0)!=($114|0);
 if ($115) {
  $116 = HEAP32[((2288 + 4|0))>>2]|0;
  _Video_SetBank(8640,$116);
 }
 $y1 = 0;
 while(1) {
  $117 = $y1;
  $118 = HEAP8[2288>>0]|0;
  $119 = $118&255;
  $120 = ($117|0)<($119|0);
  if (!($120)) {
   break;
  }
  $x2 = 0;
  while(1) {
   $121 = $x2;
   $122 = ($121|0)<(32);
   if (!($122)) {
    break;
   }
   $123 = $y1;
   $124 = HEAP8[((2288 + 10|0))>>0]|0;
   $125 = $124&255;
   $126 = (($123) + ($125))|0;
   $127 = $126<<5;
   $128 = $x2;
   $129 = (($127) + ($128))|0;
   $130 = (2408 + ($129)|0);
   $131 = HEAP8[$130>>0]|0;
   $tile3 = $131;
   $132 = HEAP32[9680>>2]|0;
   $133 = $tile3;
   $134 = $133&255;
   $135 = $x2;
   $136 = $135<<3;
   $137 = $y1;
   $138 = $137<<3;
   _Video_DrawTile($132,8640,$134,$136,$138);
   $139 = $x2;
   $140 = (($139) + 1)|0;
   $x2 = $140;
  }
  $141 = $y1;
  $142 = (($141) + 1)|0;
  $y1 = $142;
 }
 STACKTOP = sp;return;
}
function _Print($x,$y,$string) {
 $x = $x|0;
 $y = $y|0;
 $string = $string|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0;
 var $8 = 0, $9 = 0, $c = 0, $i = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $x;
 $1 = $y;
 $2 = $string;
 $i = 0;
 while(1) {
  $3 = $i;
  $4 = (($3) + 1)|0;
  $i = $4;
  $5 = $2;
  $6 = (($5) + ($3)|0);
  $7 = HEAP8[$6>>0]|0;
  $c = $7;
  $8 = $c;
  $9 = $8 << 24 >> 24;
  $10 = ($9|0)!=(0);
  if (!($10)) {
   break;
  }
  $11 = $c;
  $12 = $11 << 24 >> 24;
  $13 = $12 & 127;
  $14 = (($13) - 32)|0;
  $15 = $14&255;
  $c = $15;
  $16 = $0;
  $17 = (($16) + 1)|0;
  $0 = $17;
  $18 = $16&255;
  $19 = $1;
  $20 = $19&255;
  $21 = $c;
  _SetFont($18,$20,$21);
 }
 STACKTOP = sp;return;
}
function _PrintChar($x,$y,$c) {
 $x = $x|0;
 $y = $y|0;
 $c = $c|0;
 var $0 = 0, $1 = 0, $10 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $x;
 $1 = $y;
 $2 = $c;
 $3 = $0;
 $4 = $3&255;
 $5 = $1;
 $6 = $5&255;
 $7 = $2;
 $8 = $7 << 24 >> 24;
 $9 = (($8) - 32)|0;
 $10 = $9&255;
 _SetFont($4,$6,$10);
 STACKTOP = sp;return;
}
function _Fill($x,$y,$width,$height,$tile) {
 $x = $x|0;
 $y = $y|0;
 $width = $width|0;
 $height = $height|0;
 $tile = $tile|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $3 = 0, $4 = 0, $5 = 0;
 var $6 = 0, $7 = 0, $8 = 0, $9 = 0, $cx = 0, $cy = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $x;
 $1 = $y;
 $2 = $width;
 $3 = $height;
 $4 = $tile;
 $cy = 0;
 while(1) {
  $5 = $cy;
  $6 = $3;
  $7 = ($5|0)<($6|0);
  if (!($7)) {
   break;
  }
  $cx = 0;
  while(1) {
   $8 = $cx;
   $9 = $2;
   $10 = ($8|0)<($9|0);
   if (!($10)) {
    break;
   }
   $11 = $0;
   $12 = $cx;
   $13 = (($11) + ($12))|0;
   $14 = $13&255;
   $15 = $1;
   $16 = $cy;
   $17 = (($15) + ($16))|0;
   $18 = $17&255;
   $19 = $4;
   _SetTile($14,$18,$19);
   $20 = $cx;
   $21 = (($20) + 1)|0;
   $cx = $21;
  }
  $22 = $cy;
  $23 = (($22) + 1)|0;
  $cy = $23;
 }
 STACKTOP = sp;return;
}
function _FadeIn($speed,$blocking) {
 $speed = $speed|0;
 $blocking = $blocking|0;
 var $0 = 0, $1 = 0, $2 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $speed;
 $2 = $blocking&1;
 $1 = $2;
 STACKTOP = sp;return;
}
function _FadeOut($speed,$blocking) {
 $speed = $speed|0;
 $blocking = $blocking|0;
 var $0 = 0, $1 = 0, $2 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $speed;
 $2 = $blocking&1;
 $1 = $2;
 STACKTOP = sp;return;
}
function _GeneratePalette() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $blue = 0, $green = 0, $i = 0, $red = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $i = 0;
 while(1) {
  $0 = $i;
  $1 = ($0|0)<(256);
  if (!($1)) {
   break;
  }
  $2 = $i;
  $3 = $2 >> 0;
  $4 = $3 & 7;
  $5 = ($4*255)|0;
  $6 = (($5|0) / 7)&-1;
  $red = $6;
  $7 = $i;
  $8 = $7 >> 3;
  $9 = $8 & 7;
  $10 = ($9*255)|0;
  $11 = (($10|0) / 7)&-1;
  $green = $11;
  $12 = $i;
  $13 = $12 >> 6;
  $14 = $13 & 3;
  $15 = ($14*255)|0;
  $16 = (($15|0) / 3)&-1;
  $blue = $16;
  $17 = HEAP32[8>>2]|0;
  $18 = (($17) + 4|0);
  $19 = HEAP32[$18>>2]|0;
  $20 = $red;
  $21 = $20&255;
  $22 = $green;
  $23 = $22&255;
  $24 = $blue;
  $25 = $24&255;
  $26 = (_SDL_MapRGBA(($19|0),($21|0),($23|0),($25|0),-1)|0);
  $27 = $i;
  $28 = (9688 + ($27<<2)|0);
  HEAP32[$28>>2] = $26;
  $29 = $i;
  $30 = (($29) + 1)|0;
  $i = $30;
 }
 STACKTOP = sp;return;
}
function _Initialize() {
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (_isEepromFormatted()|0);
 if (!($0)) {
  _FormatEeprom();
 }
 HEAP8[10712>>0] = -40;
 HEAP8[10728>>0] = -40;
 HEAP8[10720>>0] = 24;
 HEAP8[10736>>0] = 24;
 _GeneratePalette();
 _InitializeVideoMode();
 _DisplayLogo();
 return;
}
function _FormatEeprom() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 return;
}
function _isEepromFormatted() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 return 1;
}
function _World_GetTileAddress($tileIndex) {
 $tileIndex = $tileIndex|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $tileIndex;
 $2 = $1;
 $3 = $2&255;
 do {
  switch ($3|0) {
  case 0:  {
   $0 = 10744;
   break;
  }
  case 1:  {
   $0 = 11008;
   break;
  }
  case 2:  {
   $0 = 11272;
   break;
  }
  case 3:  {
   $0 = 11536;
   break;
  }
  case 4:  {
   $0 = 12064;
   break;
  }
  case 5:  {
   $0 = 11800;
   break;
  }
  case 6:  {
   $0 = 12328;
   break;
  }
  case 7:  {
   $0 = 12592;
   break;
  }
  case 8:  {
   $0 = 12856;
   break;
  }
  case 9:  {
   $0 = 13120;
   break;
  }
  case 10:  {
   $0 = 13384;
   break;
  }
  case 11:  {
   $0 = 13648;
   break;
  }
  case 12:  {
   $0 = 14176;
   break;
  }
  case 13:  {
   $0 = 13912;
   break;
  }
  case 14:  {
   $0 = 14440;
   break;
  }
  case 15:  {
   $0 = 14704;
   break;
  }
  case 16:  {
   $0 = 15232;
   break;
  }
  case 17:  {
   $0 = 14968;
   break;
  }
  case 18:  {
   $0 = 15496;
   break;
  }
  case 19:  {
   $0 = 15760;
   break;
  }
  case 20:  {
   $0 = 16024;
   break;
  }
  case 21:  {
   $0 = 16552;
   break;
  }
  case 22:  {
   $0 = 16288;
   break;
  }
  case 23:  {
   $0 = 16816;
   break;
  }
  case 24:  {
   $0 = 17080;
   break;
  }
  default: {
   $0 = 0;
  }
  }
 } while(0);
 $4 = $0;
 STACKTOP = sp;return ($4|0);
}
function _World_GetLayout() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 return (22400|0);
}
function _World_GetLayoutTiles() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 return (22800|0);
}
function _World_GetHUDLayout() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 return (22664|0);
}
function _World_GetLogoLayout() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 return (22696|0);
}
function _World_Init() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 _SetTileTable(17344);
 HEAP32[((2288 + 4|0))>>2] = 22800;
 HEAP8[((2288 + 10|0))>>0] = 29;
 HEAP8[2288>>0] = 3;
 _World_UpdateAllTiles();
 return;
}
function _World_GetMacroTileExits($x,$y) {
 $x = $x|0;
 $y = $y|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $x;
 $1 = $y;
 $2 = $0;
 $3 = $1;
 $4 = (_World_GetAllMacroTileExits($2,$3)|0);
 $5 = $4&255;
 $6 = $5 & 15;
 $7 = $6&255;
 STACKTOP = sp;return ($7|0);
}
function _World_GetAllMacroTileExits($x,$y) {
 $x = $x|0;
 $y = $y|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $worldTile = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $x;
 $2 = $y;
 $3 = $2;
 $4 = $3 << 24 >> 24;
 $5 = $4<<4;
 $6 = (2 + ($5))|0;
 $7 = $1;
 $8 = $7 << 24 >> 24;
 $9 = (($6) + ($8))|0;
 $10 = (22400 + ($9)|0);
 $11 = HEAP8[$10>>0]|0;
 $worldTile = $11;
 $12 = $worldTile;
 $13 = $12&255;
 do {
  switch ($13|0) {
  case 1:  {
   $0 = 10;
   break;
  }
  case 2:  {
   $0 = 5;
   break;
  }
  case 3:  {
   $0 = 3;
   break;
  }
  case 4:  {
   $0 = 6;
   break;
  }
  case 5:  {
   $0 = 9;
   break;
  }
  case 6:  {
   $0 = 12;
   break;
  }
  case 7:  {
   $0 = 15;
   break;
  }
  case 8:  {
   $0 = 7;
   break;
  }
  case 9:  {
   $0 = 14;
   break;
  }
  case 10:  {
   $0 = 13;
   break;
  }
  case 11:  {
   $0 = 11;
   break;
  }
  case 18:  {
   $0 = 5;
   break;
  }
  case 19:  {
   $0 = 10;
   break;
  }
  case 20:  {
   $0 = 5;
   break;
  }
  case 21:  {
   $0 = 21;
   break;
  }
  case 22:  {
   $0 = 37;
   break;
  }
  case 23:  {
   $0 = 69;
   break;
  }
  case 24:  {
   $0 = -128;
   break;
  }
  default: {
   $0 = 0;
  }
  }
 } while(0);
 $14 = $0;
 STACKTOP = sp;return ($14|0);
}
function _World_GetTile($x,$y) {
 $x = $x|0;
 $y = $y|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $address = 0, $macroTileX = 0, $macroTileY = 0;
 var $result = 0, $u = 0, $v = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $x;
 $2 = $y;
 $3 = $1;
 $4 = $3 << 16 >> 16;
 $5 = $4 >> 4;
 $6 = $5&65535;
 $macroTileX = $6;
 $7 = $2;
 $8 = $7 << 16 >> 16;
 $9 = $8 >> 4;
 $10 = $9&65535;
 $macroTileY = $10;
 $address = 0;
 $11 = $macroTileX;
 $12 = $11 << 16 >> 16;
 $13 = ($12|0)<(0);
 if ($13) {
  label = 5;
 } else {
  $14 = $macroTileY;
  $15 = $14 << 16 >> 16;
  $16 = ($15|0)<(0);
  if ($16) {
   label = 5;
  } else {
   $17 = $macroTileX;
   $18 = $17 << 16 >> 16;
   $19 = ($18|0)>=(16);
   if ($19) {
    label = 5;
   } else {
    $20 = $macroTileY;
    $21 = $20 << 16 >> 16;
    $22 = ($21|0)>=(16);
    if ($22) {
     label = 5;
    }
   }
  }
 }
 if ((label|0) == 5) {
  $address = 10744;
 }
 $23 = $address;
 $24 = ($23|0)!=(0|0);
 if (!($24)) {
  $25 = $macroTileY;
  $26 = $25 << 16 >> 16;
  $27 = $26<<4;
  $28 = (2 + ($27))|0;
  $29 = $macroTileX;
  $30 = $29 << 16 >> 16;
  $31 = (($28) + ($30))|0;
  $32 = (22400 + ($31)|0);
  $33 = HEAP8[$32>>0]|0;
  $34 = (_World_GetTileAddress($33)|0);
  $address = $34;
 }
 $35 = $address;
 $36 = ($35|0)!=(0|0);
 if ($36) {
  $37 = $1;
  $38 = $37 << 16 >> 16;
  $39 = $38 & 15;
  $40 = $39&255;
  $u = $40;
  $41 = $2;
  $42 = $41 << 16 >> 16;
  $43 = $42 & 15;
  $44 = $43&255;
  $v = $44;
  $45 = $v;
  $46 = $45&255;
  $47 = $46 << 4;
  $48 = $u;
  $49 = $48&255;
  $50 = (($47) + ($49))|0;
  $51 = (($50) + 2)|0;
  $52 = $address;
  $53 = (($52) + ($51)|0);
  $54 = HEAP8[$53>>0]|0;
  $result = $54;
  $55 = $result;
  $0 = $55;
  $56 = $0;
  STACKTOP = sp;return ($56|0);
 } else {
  $0 = 0;
  $56 = $0;
  STACKTOP = sp;return ($56|0);
 }
 return (0)|0;
}
function _World_UpdateAllTiles() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 HEAP16[28944>>1] = 255;
 _World_UpdateTiles();
 return;
}
function _World_UpdateTiles() {
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0;
 var $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0;
 var $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0, $202 = 0, $203 = 0, $204 = 0, $205 = 0;
 var $206 = 0, $207 = 0, $208 = 0, $209 = 0, $21 = 0, $210 = 0, $211 = 0, $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0, $219 = 0, $22 = 0, $220 = 0, $221 = 0, $222 = 0, $223 = 0;
 var $224 = 0, $225 = 0, $226 = 0, $227 = 0, $228 = 0, $229 = 0, $23 = 0, $230 = 0, $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0, $241 = 0;
 var $242 = 0, $243 = 0, $244 = 0, $245 = 0, $246 = 0, $247 = 0, $248 = 0, $249 = 0, $25 = 0, $250 = 0, $251 = 0, $252 = 0, $253 = 0, $254 = 0, $255 = 0, $256 = 0, $257 = 0, $258 = 0, $259 = 0, $26 = 0;
 var $260 = 0, $261 = 0, $262 = 0, $263 = 0, $264 = 0, $265 = 0, $266 = 0, $267 = 0, $268 = 0, $269 = 0, $27 = 0, $270 = 0, $271 = 0, $272 = 0, $273 = 0, $274 = 0, $275 = 0, $276 = 0, $277 = 0, $278 = 0;
 var $279 = 0, $28 = 0, $280 = 0, $281 = 0, $282 = 0, $283 = 0, $284 = 0, $285 = 0, $286 = 0, $287 = 0, $288 = 0, $289 = 0, $29 = 0, $290 = 0, $291 = 0, $292 = 0, $293 = 0, $294 = 0, $295 = 0, $296 = 0;
 var $297 = 0, $298 = 0, $299 = 0, $3 = 0, $30 = 0, $300 = 0, $301 = 0, $302 = 0, $303 = 0, $304 = 0, $305 = 0, $306 = 0, $307 = 0, $308 = 0, $309 = 0, $31 = 0, $310 = 0, $311 = 0, $312 = 0, $313 = 0;
 var $314 = 0, $315 = 0, $316 = 0, $317 = 0, $318 = 0, $319 = 0, $32 = 0, $320 = 0, $321 = 0, $322 = 0, $323 = 0, $324 = 0, $325 = 0, $326 = 0, $327 = 0, $328 = 0, $329 = 0, $33 = 0, $330 = 0, $331 = 0;
 var $332 = 0, $333 = 0, $334 = 0, $335 = 0, $336 = 0, $337 = 0, $338 = 0, $339 = 0, $34 = 0, $340 = 0, $341 = 0, $342 = 0, $343 = 0, $344 = 0, $345 = 0, $346 = 0, $347 = 0, $348 = 0, $349 = 0, $35 = 0;
 var $350 = 0, $351 = 0, $352 = 0, $353 = 0, $354 = 0, $355 = 0, $356 = 0, $357 = 0, $358 = 0, $359 = 0, $36 = 0, $360 = 0, $361 = 0, $362 = 0, $363 = 0, $364 = 0, $365 = 0, $366 = 0, $367 = 0, $368 = 0;
 var $369 = 0, $37 = 0, $370 = 0, $371 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0;
 var $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0;
 var $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0;
 var $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $diffX = 0, $diffY = 0, $i = 0, $i11 = 0, $i17 = 0, $i3 = 0, $i8 = 0, $j = 0;
 var $j14 = 0, $j2 = 0, $j20 = 0, $j6 = 0, $outX = 0, $outX10 = 0, $outX16 = 0, $outX22 = 0, $outX4 = 0, $outY = 0, $outY1 = 0, $outY13 = 0, $outY19 = 0, $outY5 = 0, $scrollX = 0, $scrollY = 0, $worldX = 0, $worldX12 = 0, $worldX18 = 0, $worldX9 = 0;
 var $worldY = 0, $worldY15 = 0, $worldY21 = 0, $worldY7 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 48|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = HEAP16[28952>>1]|0;
 $1 = $0&255;
 HEAP8[((2288 + 8|0))>>0] = $1;
 $2 = HEAP16[((28952 + 2|0))>>1]|0;
 $3 = $2 << 16 >> 16;
 $4 = ($3|0)<(0);
 if ($4) {
  $5 = HEAP16[((28952 + 2|0))>>1]|0;
  $6 = $5 << 16 >> 16;
  $7 = (($6) - 24)|0;
  $8 = (($7|0) % 232)&-1;
  $9 = $8&255;
  HEAP8[((2288 + 9|0))>>0] = $9;
 } else {
  $10 = HEAP16[((28952 + 2|0))>>1]|0;
  $11 = $10 << 16 >> 16;
  $12 = (($11|0) % 232)&-1;
  $13 = $12&255;
  HEAP8[((2288 + 9|0))>>0] = $13;
 }
 $14 = HEAP16[28952>>1]|0;
 $15 = $14 << 16 >> 16;
 $16 = $15 >> 3;
 $17 = $16&65535;
 $scrollX = $17;
 $18 = HEAP16[((28952 + 2|0))>>1]|0;
 $19 = $18 << 16 >> 16;
 $20 = $19 >> 3;
 $21 = $20&65535;
 $scrollY = $21;
 $22 = $scrollX;
 $23 = $22 << 16 >> 16;
 $24 = HEAP16[28960>>1]|0;
 $25 = $24 << 16 >> 16;
 $26 = ($23|0)==($25|0);
 if ($26) {
  $27 = $scrollY;
  $28 = $27 << 16 >> 16;
  $29 = HEAP16[28944>>1]|0;
  $30 = $29 << 16 >> 16;
  $31 = ($28|0)==($30|0);
  if ($31) {
   STACKTOP = sp;return;
  }
 }
 $32 = $scrollX;
 $33 = $32 << 16 >> 16;
 $34 = HEAP16[28960>>1]|0;
 $35 = $34 << 16 >> 16;
 $36 = (($35) - 32)|0;
 $37 = ($33|0)<=($36|0);
 if ($37) {
  label = 11;
 } else {
  $38 = $scrollX;
  $39 = $38 << 16 >> 16;
  $40 = HEAP16[28960>>1]|0;
  $41 = $40 << 16 >> 16;
  $42 = (($41) + 32)|0;
  $43 = ($39|0)>=($42|0);
  if ($43) {
   label = 11;
  } else {
   $44 = $scrollY;
   $45 = $44 << 16 >> 16;
   $46 = HEAP16[28944>>1]|0;
   $47 = $46 << 16 >> 16;
   $48 = (($47) - 32)|0;
   $49 = ($45|0)<=($48|0);
   if ($49) {
    label = 11;
   } else {
    $50 = $scrollY;
    $51 = $50 << 16 >> 16;
    $52 = HEAP16[28944>>1]|0;
    $53 = $52 << 16 >> 16;
    $54 = (($53) + 32)|0;
    $55 = ($51|0)>=($54|0);
    if ($55) {
     label = 11;
    } else {
     $diffX = 0;
     $diffY = 0;
     $106 = $scrollY;
     $107 = $106 << 16 >> 16;
     $108 = HEAP16[28944>>1]|0;
     $109 = $108 << 16 >> 16;
     $110 = ($107|0)<($109|0);
     if ($110) {
      $111 = HEAP16[28944>>1]|0;
      $112 = $111 << 16 >> 16;
      $113 = $scrollY;
      $114 = $113 << 16 >> 16;
      $115 = (($112) - ($114))|0;
      $116 = $115&255;
      $diffY = $116;
      $117 = $diffY;
      $118 = $117 << 24 >> 24;
      $119 = HEAP8[28968>>0]|0;
      $120 = $119 << 24 >> 24;
      $121 = (($120) - ($118))|0;
      $122 = $121&255;
      HEAP8[28968>>0] = $122;
      while(1) {
       $123 = HEAP8[28968>>0]|0;
       $124 = $123 << 24 >> 24;
       $125 = ($124|0)<(0);
       if (!($125)) {
        break;
       }
       $126 = HEAP8[28968>>0]|0;
       $127 = $126 << 24 >> 24;
       $128 = (($127) + 29)|0;
       $129 = $128&255;
       HEAP8[28968>>0] = $129;
      }
      $130 = HEAP8[28968>>0]|0;
      $outY1 = $130;
      $j2 = 0;
      while(1) {
       $131 = $j2;
       $132 = $131 << 24 >> 24;
       $133 = $diffY;
       $134 = $133 << 24 >> 24;
       $135 = ($132|0)<($134|0);
       if (!($135)) {
        break;
       }
       $136 = $scrollY;
       $137 = $136 << 16 >> 16;
       $138 = $j2;
       $139 = $138 << 24 >> 24;
       $140 = (($137) + ($139))|0;
       $141 = $140&65535;
       $worldY = $141;
       $i3 = 0;
       while(1) {
        $142 = $i3;
        $143 = $142&255;
        $144 = ($143|0)<(32);
        if (!($144)) {
         break;
        }
        $145 = $scrollX;
        $146 = $145 << 16 >> 16;
        $147 = $i3;
        $148 = $147&255;
        $149 = (($146) + ($148))|0;
        $150 = $149&65535;
        $worldX = $150;
        $151 = $worldX;
        $152 = $151 << 16 >> 16;
        $153 = $152 & 31;
        $154 = $153&255;
        $outX4 = $154;
        $155 = $outX4;
        $156 = $outY1;
        $157 = $worldX;
        $158 = $worldY;
        $159 = (_World_GetTile($157,$158)|0);
        $160 = $159&255;
        _SetTile($155,$156,$160);
        $161 = $i3;
        $162 = (($161) + 1)<<24>>24;
        $i3 = $162;
       }
       $163 = $outY1;
       $164 = (($163) + 1)<<24>>24;
       $outY1 = $164;
       $165 = $outY1;
       $166 = $165&255;
       $167 = ($166|0)>=(29);
       if ($167) {
        $168 = $outY1;
        $169 = $168&255;
        $170 = (($169) - 29)|0;
        $171 = $170&255;
        $outY1 = $171;
       }
       $172 = $j2;
       $173 = (($172) + 1)<<24>>24;
       $j2 = $173;
      }
     } else {
      $174 = $scrollY;
      $175 = $174 << 16 >> 16;
      $176 = HEAP16[28944>>1]|0;
      $177 = $176 << 16 >> 16;
      $178 = ($175|0)>($177|0);
      if ($178) {
       $179 = $scrollY;
       $180 = $179 << 16 >> 16;
       $181 = HEAP16[28944>>1]|0;
       $182 = $181 << 16 >> 16;
       $183 = (($180) - ($182))|0;
       $184 = $183 & 31;
       $185 = $184&255;
       $diffY = $185;
       $186 = $diffY;
       $187 = $186 << 24 >> 24;
       $188 = HEAP8[28968>>0]|0;
       $189 = $188 << 24 >> 24;
       $190 = (($189) + ($187))|0;
       $191 = $190&255;
       HEAP8[28968>>0] = $191;
       while(1) {
        $192 = HEAP8[28968>>0]|0;
        $193 = $192 << 24 >> 24;
        $194 = ($193|0)>=(29);
        if (!($194)) {
         break;
        }
        $195 = HEAP8[28968>>0]|0;
        $196 = $195 << 24 >> 24;
        $197 = (($196) - 29)|0;
        $198 = $197&255;
        HEAP8[28968>>0] = $198;
       }
       $199 = HEAP8[28968>>0]|0;
       $200 = $199 << 24 >> 24;
       $201 = (($200) + 29)|0;
       $202 = $diffY;
       $203 = $202 << 24 >> 24;
       $204 = (($201) - ($203))|0;
       $205 = $204&255;
       $outY5 = $205;
       while(1) {
        $206 = $outY5;
        $207 = $206&255;
        $208 = ($207|0)>=(29);
        if (!($208)) {
         break;
        }
        $209 = $outY5;
        $210 = $209&255;
        $211 = (($210) - 29)|0;
        $212 = $211&255;
        $outY5 = $212;
       }
       $213 = $diffY;
       $214 = $213 << 24 >> 24;
       $215 = (29 - ($214))|0;
       $216 = $215&255;
       $j6 = $216;
       while(1) {
        $217 = $j6;
        $218 = $217 << 24 >> 24;
        $219 = ($218|0)<(29);
        if (!($219)) {
         break;
        }
        $220 = $scrollY;
        $221 = $220 << 16 >> 16;
        $222 = $j6;
        $223 = $222 << 24 >> 24;
        $224 = (($221) + ($223))|0;
        $225 = $224&65535;
        $worldY7 = $225;
        $i8 = 0;
        while(1) {
         $226 = $i8;
         $227 = $226&255;
         $228 = ($227|0)<(32);
         if (!($228)) {
          break;
         }
         $229 = $scrollX;
         $230 = $229 << 16 >> 16;
         $231 = $i8;
         $232 = $231&255;
         $233 = (($230) + ($232))|0;
         $234 = $233&65535;
         $worldX9 = $234;
         $235 = $worldX9;
         $236 = $235 << 16 >> 16;
         $237 = $236 & 31;
         $238 = $237&255;
         $outX10 = $238;
         $239 = $outX10;
         $240 = $outY5;
         $241 = $worldX9;
         $242 = $worldY7;
         $243 = (_World_GetTile($241,$242)|0);
         $244 = $243&255;
         _SetTile($239,$240,$244);
         $245 = $i8;
         $246 = (($245) + 1)<<24>>24;
         $i8 = $246;
        }
        $247 = $outY5;
        $248 = (($247) + 1)<<24>>24;
        $outY5 = $248;
        $249 = $outY5;
        $250 = $249&255;
        $251 = ($250|0)>=(29);
        if ($251) {
         $252 = $outY5;
         $253 = $252&255;
         $254 = (($253) - 29)|0;
         $255 = $254&255;
         $outY5 = $255;
        }
        $256 = $j6;
        $257 = (($256) + 1)<<24>>24;
        $j6 = $257;
       }
      }
     }
     $258 = $scrollX;
     $259 = $258 << 16 >> 16;
     $260 = HEAP16[28960>>1]|0;
     $261 = $260 << 16 >> 16;
     $262 = ($259|0)<($261|0);
     if ($262) {
      $263 = HEAP16[28960>>1]|0;
      $264 = $263 << 16 >> 16;
      $265 = $scrollX;
      $266 = $265 << 16 >> 16;
      $267 = (($264) - ($266))|0;
      $268 = $267&255;
      $diffX = $268;
      $i11 = 0;
      while(1) {
       $269 = $i11;
       $270 = $269&255;
       $271 = $diffX;
       $272 = $271 << 24 >> 24;
       $273 = ($270|0)<($272|0);
       if (!($273)) {
        break;
       }
       $274 = $scrollX;
       $275 = $274 << 16 >> 16;
       $276 = $i11;
       $277 = $276&255;
       $278 = (($275) + ($277))|0;
       $279 = $278&65535;
       $worldX12 = $279;
       $280 = HEAP8[28968>>0]|0;
       $outY13 = $280;
       $j14 = 0;
       while(1) {
        $281 = $j14;
        $282 = $281 << 24 >> 24;
        $283 = ($282|0)<(29);
        if (!($283)) {
         break;
        }
        $284 = $scrollY;
        $285 = $284 << 16 >> 16;
        $286 = $j14;
        $287 = $286 << 24 >> 24;
        $288 = (($285) + ($287))|0;
        $289 = $288&65535;
        $worldY15 = $289;
        $290 = $worldX12;
        $291 = $290 << 16 >> 16;
        $292 = $291 & 31;
        $293 = $292&255;
        $outX16 = $293;
        $294 = $outX16;
        $295 = $outY13;
        $296 = $worldX12;
        $297 = $worldY15;
        $298 = (_World_GetTile($296,$297)|0);
        $299 = $298&255;
        _SetTile($294,$295,$299);
        $300 = $outY13;
        $301 = (($300) + 1)<<24>>24;
        $outY13 = $301;
        $302 = $outY13;
        $303 = $302&255;
        $304 = ($303|0)>=(29);
        if ($304) {
         $305 = $outY13;
         $306 = $305&255;
         $307 = (($306) - 29)|0;
         $308 = $307&255;
         $outY13 = $308;
        }
        $309 = $j14;
        $310 = (($309) + 1)<<24>>24;
        $j14 = $310;
       }
       $311 = $i11;
       $312 = (($311) + 1)<<24>>24;
       $i11 = $312;
      }
     } else {
      $313 = $scrollX;
      $314 = $313 << 16 >> 16;
      $315 = HEAP16[28960>>1]|0;
      $316 = $315 << 16 >> 16;
      $317 = ($314|0)>($316|0);
      if ($317) {
       $318 = $scrollX;
       $319 = $318 << 16 >> 16;
       $320 = HEAP16[28960>>1]|0;
       $321 = $320 << 16 >> 16;
       $322 = (($319) - ($321))|0;
       $323 = $322&255;
       $diffX = $323;
       $324 = $diffX;
       $325 = $324 << 24 >> 24;
       $326 = (32 - ($325))|0;
       $327 = $326&255;
       $i17 = $327;
       while(1) {
        $328 = $i17;
        $329 = $328&255;
        $330 = ($329|0)<(32);
        if (!($330)) {
         break;
        }
        $331 = $scrollX;
        $332 = $331 << 16 >> 16;
        $333 = $i17;
        $334 = $333&255;
        $335 = (($332) + ($334))|0;
        $336 = $335&65535;
        $worldX18 = $336;
        $337 = HEAP8[28968>>0]|0;
        $outY19 = $337;
        $j20 = 0;
        while(1) {
         $338 = $j20;
         $339 = $338 << 24 >> 24;
         $340 = ($339|0)<(29);
         if (!($340)) {
          break;
         }
         $341 = $scrollY;
         $342 = $341 << 16 >> 16;
         $343 = $j20;
         $344 = $343 << 24 >> 24;
         $345 = (($342) + ($344))|0;
         $346 = $345&65535;
         $worldY21 = $346;
         $347 = $worldX18;
         $348 = $347 << 16 >> 16;
         $349 = $348 & 31;
         $350 = $349&255;
         $outX22 = $350;
         $351 = $outX22;
         $352 = $outY19;
         $353 = $worldX18;
         $354 = $worldY21;
         $355 = (_World_GetTile($353,$354)|0);
         $356 = $355&255;
         _SetTile($351,$352,$356);
         $357 = $outY19;
         $358 = (($357) + 1)<<24>>24;
         $outY19 = $358;
         $359 = $outY19;
         $360 = $359&255;
         $361 = ($360|0)>=(29);
         if ($361) {
          $362 = $outY19;
          $363 = $362&255;
          $364 = (($363) - 29)|0;
          $365 = $364&255;
          $outY19 = $365;
         }
         $366 = $j20;
         $367 = (($366) + 1)<<24>>24;
         $j20 = $367;
        }
        $368 = $i17;
        $369 = (($368) + 1)<<24>>24;
        $i17 = $369;
       }
      }
     }
    }
   }
  }
 }
 if ((label|0) == 11) {
  $56 = $scrollY;
  $57 = $56 << 16 >> 16;
  $58 = ($57|0)>=(0);
  if ($58) {
   $59 = $scrollY;
   $60 = $59 << 16 >> 16;
   $61 = (($60|0) % 29)&-1;
   $62 = $61&255;
   HEAP8[28968>>0] = $62;
  } else {
   $63 = $scrollY;
   $64 = $63 << 16 >> 16;
   $65 = (($64) + 29)|0;
   $66 = (($65|0) % 29)&-1;
   $67 = $66&255;
   HEAP8[28968>>0] = $67;
  }
  $68 = HEAP8[28968>>0]|0;
  $outY = $68;
  $69 = $scrollY;
  $j = $69;
  while(1) {
   $70 = $j;
   $71 = $70 << 16 >> 16;
   $72 = $scrollY;
   $73 = $72 << 16 >> 16;
   $74 = (($73) + 29)|0;
   $75 = ($71|0)<($74|0);
   if (!($75)) {
    break;
   }
   $76 = $scrollX;
   $i = $76;
   while(1) {
    $77 = $i;
    $78 = $77 << 16 >> 16;
    $79 = $scrollX;
    $80 = $79 << 16 >> 16;
    $81 = (($80) + 32)|0;
    $82 = ($78|0)<($81|0);
    if (!($82)) {
     break;
    }
    $83 = $i;
    $84 = $83 << 16 >> 16;
    $85 = $84 & 31;
    $86 = $85&255;
    $outX = $86;
    $87 = $outX;
    $88 = $outY;
    $89 = $i;
    $90 = $j;
    $91 = (_World_GetTile($89,$90)|0);
    $92 = $91&255;
    _SetTile($87,$88,$92);
    $93 = $i;
    $94 = (($93) + 1)<<16>>16;
    $i = $94;
   }
   $95 = $outY;
   $96 = (($95) + 1)<<24>>24;
   $outY = $96;
   $97 = $outY;
   $98 = $97&255;
   $99 = ($98|0)>=(29);
   if ($99) {
    $100 = $outY;
    $101 = $100&255;
    $102 = (($101) - 29)|0;
    $103 = $102&255;
    $outY = $103;
   }
   $104 = $j;
   $105 = (($104) + 1)<<16>>16;
   $j = $105;
  }
 }
 $370 = $scrollX;
 HEAP16[28960>>1] = $370;
 $371 = $scrollY;
 HEAP16[28944>>1] = $371;
 STACKTOP = sp;return;
}
function _World_SetCustomTile($worldX,$worldY,$tile) {
 $worldX = $worldX|0;
 $worldY = $worldY|0;
 $tile = $tile|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $outX = 0, $outY = 0, $scrollX = 0;
 var $scrollY = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $worldX;
 $1 = $worldY;
 $2 = $tile;
 $3 = HEAP16[28952>>1]|0;
 $4 = $3 << 16 >> 16;
 $5 = $4 >> 3;
 $6 = $5&65535;
 $scrollX = $6;
 $7 = HEAP16[((28952 + 2|0))>>1]|0;
 $8 = $7 << 16 >> 16;
 $9 = $8 >> 3;
 $10 = $9&65535;
 $scrollY = $10;
 $11 = $0;
 $12 = $11&255;
 $13 = $scrollX;
 $14 = $13 << 16 >> 16;
 $15 = ($12|0)>=($14|0);
 if (!($15)) {
  STACKTOP = sp;return;
 }
 $16 = $1;
 $17 = $16&255;
 $18 = $scrollY;
 $19 = $18 << 16 >> 16;
 $20 = ($17|0)>=($19|0);
 if (!($20)) {
  STACKTOP = sp;return;
 }
 $21 = $0;
 $22 = $21&255;
 $23 = $scrollX;
 $24 = $23 << 16 >> 16;
 $25 = (($24) + 32)|0;
 $26 = ($22|0)<($25|0);
 if (!($26)) {
  STACKTOP = sp;return;
 }
 $27 = $1;
 $28 = $27&255;
 $29 = $scrollY;
 $30 = $29 << 16 >> 16;
 $31 = (($30) + 29)|0;
 $32 = ($28|0)<($31|0);
 if (!($32)) {
  STACKTOP = sp;return;
 }
 $33 = $0;
 $34 = $33&255;
 $35 = $34 & 31;
 $36 = $35&255;
 $outX = $36;
 $37 = HEAP8[28968>>0]|0;
 $38 = $37 << 24 >> 24;
 $39 = $1;
 $40 = $39&255;
 $41 = $scrollY;
 $42 = $41 << 16 >> 16;
 $43 = (($40) - ($42))|0;
 $44 = (($38) + ($43))|0;
 $45 = $44&255;
 $outY = $45;
 $46 = $outY;
 $47 = $46&255;
 $48 = ($47|0)>=(29);
 if ($48) {
  $49 = $outY;
  $50 = $49&255;
  $51 = (($50) - 29)|0;
  $52 = $51&255;
  $outY = $52;
 }
 $53 = $outX;
 $54 = $outY;
 $55 = $2;
 $56 = $55&255;
 _SetTile($53,$54,$56);
 STACKTOP = sp;return;
}
function _World_ResetCustomTile($worldX,$worldY) {
 $worldX = $worldX|0;
 $worldY = $worldY|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $tile = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $worldX;
 $1 = $worldY;
 $2 = $0;
 $3 = $2&255;
 $4 = $1;
 $5 = $4&255;
 $6 = (_World_GetTile($3,$5)|0);
 $tile = $6;
 $7 = $0;
 $8 = $1;
 $9 = $tile;
 _World_SetCustomTile($7,$8,$9);
 STACKTOP = sp;return;
}
function _Player_CommitOffence($wantedRaiseChance) {
 $wantedRaiseChance = $wantedRaiseChance|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $3 = 0, $4 = 0, $5 = 0;
 var $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $wantedRaiseChance;
 $1 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $2 = ($1&65535) >>> 6;
 $3 = $2 & 1;
 $4 = $3&1;
 if (!($4)) {
  $5 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
  $6 = ($5&65535) >>> 12;
  $7 = $6 & 1;
  $8 = $7&1;
  if (!($8)) {
   $9 = (_Math_Random()|0);
   $10 = $9&65535;
   $11 = $0;
   $12 = $11&255;
   $13 = $10 & $12;
   $14 = ($13|0)==(0);
   if (!($14)) {
    STACKTOP = sp;return;
   }
   $15 = HEAP8[29104>>0]|0;
   $16 = $15&255;
   $17 = ($16|0)<(4);
   if ($17) {
    $18 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
    $19 = ($18&65535) >>> 1;
    $20 = $19 & 1;
    $21 = $20&1;
    if (!($21)) {
     $22 = HEAP8[29104>>0]|0;
     $23 = (($22) + 1)<<24>>24;
     HEAP8[29104>>0] = $23;
     _UI_UpdateWanted();
    }
   }
   STACKTOP = sp;return;
  }
 }
 STACKTOP = sp;return;
}
function _Camera_Shake($duration) {
 $duration = $duration|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $duration;
 $1 = $0;
 $2 = $1&255;
 $3 = HEAP8[29152>>0]|0;
 $4 = $3&255;
 $5 = ($2|0)>($4|0);
 if (!($5)) {
  STACKTOP = sp;return;
 }
 $6 = $0;
 HEAP8[29152>>0] = $6;
 STACKTOP = sp;return;
}
function _Car_Spawn($aiType,$x,$y,$angle) {
 $aiType = $aiType|0;
 $x = $x|0;
 $y = $y|0;
 $angle = $angle|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0;
 var $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0;
 var $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0;
 var $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0;
 var $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0;
 var $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0;
 var $98 = 0, $99 = 0, $car = 0, $forwardX = 0, $forwardY = 0, $n = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $forwardX = sp + 15|0;
 $forwardY = sp + 16|0;
 $1 = $aiType;
 $2 = $x;
 $3 = $y;
 $4 = $angle;
 $car = 0;
 $n = 0;
 while(1) {
  $5 = $n;
  $6 = $5&255;
  $7 = ($6|0)<(5);
  if (!($7)) {
   break;
  }
  $8 = $n;
  $9 = $8&255;
  $10 = (29160 + (($9*30)|0)|0);
  $11 = (($10) + 29|0);
  $12 = HEAP8[$11>>0]|0;
  $13 = $12&255;
  $14 = ($13|0)==(0);
  if ($14) {
   label = 4;
   break;
  }
  $18 = $n;
  $19 = (($18) + 1)<<24>>24;
  $n = $19;
 }
 if ((label|0) == 4) {
  $15 = $n;
  $16 = $15&255;
  $17 = (29160 + (($16*30)|0)|0);
  $car = $17;
 }
 $20 = $car;
 $21 = ($20|0)!=(0|0);
 if (!($21)) {
  $n = 0;
  while(1) {
   $22 = $n;
   $23 = $22&255;
   $24 = ($23|0)<(5);
   if (!($24)) {
    break;
   }
   $25 = $n;
   $26 = $25&255;
   $27 = (29160 + (($26*30)|0)|0);
   $28 = (($27) + 29|0);
   $29 = HEAP8[$28>>0]|0;
   $30 = $29&255;
   $31 = ($30|0)==(4);
   if ($31) {
    label = 12;
   } else {
    $32 = $n;
    $33 = $32&255;
    $34 = (29160 + (($33*30)|0)|0);
    $35 = (($34) + 29|0);
    $36 = HEAP8[$35>>0]|0;
    $37 = $36&255;
    $38 = ($37|0)==(5);
    if ($38) {
     label = 12;
    }
   }
   if ((label|0) == 12) {
    label = 0;
    $39 = $n;
    $40 = $39&255;
    $41 = (29160 + (($40*30)|0)|0);
    $42 = (($41) + 27|0);
    $43 = (($42) + 1|0);
    $44 = HEAP8[$43>>0]|0;
    $45 = ($44&255) >>> 5;
    $46 = $45 & 1;
    $47 = ($46<<24>>24)!=(0);
    if (!($47)) {
     label = 13;
     break;
    }
   }
   $51 = $n;
   $52 = (($51) + 1)<<24>>24;
   $n = $52;
  }
  if ((label|0) == 13) {
   $48 = $n;
   $49 = $48&255;
   $50 = (29160 + (($49*30)|0)|0);
   $car = $50;
  }
 }
 $53 = $car;
 $54 = ($53|0)!=(0|0);
 if (!($54)) {
  $n = 0;
  while(1) {
   $55 = $n;
   $56 = $55&255;
   $57 = ($56|0)<(5);
   if (!($57)) {
    break;
   }
   $58 = $n;
   $59 = $58&255;
   $60 = (29160 + (($59*30)|0)|0);
   $61 = (($60) + 29|0);
   $62 = HEAP8[$61>>0]|0;
   $63 = $62&255;
   $64 = ($63|0)==(4);
   if ($64) {
    label = 22;
    break;
   }
   $65 = $n;
   $66 = $65&255;
   $67 = (29160 + (($66*30)|0)|0);
   $68 = (($67) + 29|0);
   $69 = HEAP8[$68>>0]|0;
   $70 = $69&255;
   $71 = ($70|0)==(5);
   if ($71) {
    label = 22;
    break;
   }
   $75 = $n;
   $76 = (($75) + 1)<<24>>24;
   $n = $76;
  }
  if ((label|0) == 22) {
   $72 = $n;
   $73 = $72&255;
   $74 = (29160 + (($73*30)|0)|0);
   $car = $74;
  }
 }
 $77 = $car;
 $78 = ($77|0)!=(0|0);
 if (!($78)) {
  $0 = 0;
  $181 = $0;
  STACKTOP = sp;return ($181|0);
 }
 $79 = $car;
 $80 = (($79) + 22|0);
 HEAP16[$80>>1] = 0;
 $81 = $1;
 $82 = $car;
 $83 = (($82) + 29|0);
 HEAP8[$83>>0] = $81;
 $84 = $2;
 $85 = $car;
 HEAP16[$85>>1] = $84;
 $86 = $3;
 $87 = $car;
 $88 = (($87) + 2|0);
 HEAP16[$88>>1] = $86;
 $89 = $4;
 $90 = $car;
 $91 = (($90) + 21|0);
 HEAP8[$91>>0] = $89;
 $92 = $4;
 _Math_GetForwardVector($92,$forwardX,$forwardY);
 $93 = $2;
 $94 = $93 << 16 >> 16;
 $95 = HEAP8[$forwardX>>0]|0;
 $96 = $95 << 24 >> 24;
 $97 = (($94) + ($96))|0;
 $98 = $97&65535;
 $99 = $car;
 $100 = (($99) + 4|0);
 $101 = (($100) + 4|0);
 HEAP16[$101>>1] = $98;
 $102 = $car;
 $103 = (($102) + 4|0);
 HEAP16[$103>>1] = $98;
 $104 = $3;
 $105 = $104 << 16 >> 16;
 $106 = HEAP8[$forwardY>>0]|0;
 $107 = $106 << 24 >> 24;
 $108 = (($105) + ($107))|0;
 $109 = $108&65535;
 $110 = $car;
 $111 = (($110) + 4|0);
 $112 = (($111) + 6|0);
 HEAP16[$112>>1] = $109;
 $113 = $car;
 $114 = (($113) + 4|0);
 $115 = (($114) + 2|0);
 HEAP16[$115>>1] = $109;
 $116 = $2;
 $117 = $116 << 16 >> 16;
 $118 = HEAP8[$forwardX>>0]|0;
 $119 = $118 << 24 >> 24;
 $120 = (($117) - ($119))|0;
 $121 = $120&65535;
 $122 = $car;
 $123 = (($122) + 12|0);
 $124 = (($123) + 4|0);
 HEAP16[$124>>1] = $121;
 $125 = $car;
 $126 = (($125) + 12|0);
 HEAP16[$126>>1] = $121;
 $127 = $3;
 $128 = $127 << 16 >> 16;
 $129 = HEAP8[$forwardY>>0]|0;
 $130 = $129 << 24 >> 24;
 $131 = (($128) - ($130))|0;
 $132 = $131&65535;
 $133 = $car;
 $134 = (($133) + 12|0);
 $135 = (($134) + 6|0);
 HEAP16[$135>>1] = $132;
 $136 = $car;
 $137 = (($136) + 12|0);
 $138 = (($137) + 2|0);
 HEAP16[$138>>1] = $132;
 $139 = $car;
 $140 = (($139) + 4|0);
 $141 = $car;
 $142 = (($141) + 12|0);
 _Particle_Constrain($140,$142,0);
 $143 = $car;
 $144 = (($143) + 25|0);
 HEAP8[$144>>0] = 0;
 $145 = $car;
 $146 = (($145) + 25|0);
 $147 = (($146) + 1|0);
 $148 = HEAP8[$147>>0]|0;
 $149 = $148 & 15;
 $150 = $149 | 16;
 HEAP8[$147>>0] = $150;
 $151 = $car;
 $152 = (($151) + 25|0);
 $153 = (($152) + 1|0);
 $154 = HEAP8[$153>>0]|0;
 $155 = $154 & -16;
 HEAP8[$153>>0] = $155;
 $156 = $car;
 $157 = (($156) + 24|0);
 HEAP8[$157>>0] = -1;
 $158 = $car;
 $159 = (($158) + 29|0);
 $160 = HEAP8[$159>>0]|0;
 $161 = $160&255;
 $162 = ($161|0)==(1);
 if ($162) {
  $163 = $n;
  HEAP8[29120>>0] = $163;
 }
 $164 = $car;
 $165 = (($164) + 29|0);
 $166 = HEAP8[$165>>0]|0;
 $167 = $166&255;
 $168 = ($167|0)==(2);
 if ($168) {
  $169 = $n;
  HEAP8[29128>>0] = $169;
 }
 $170 = $car;
 $171 = (($170) + 29|0);
 $172 = HEAP8[$171>>0]|0;
 $173 = $172&255;
 $174 = ($173|0)==(6);
 if ($174) {
  $175 = $n;
  $176 = HEAP8[((36904 + 14|0))>>0]|0;
  $177 = $175 & 15;
  $178 = $176 & -16;
  $179 = $178 | $177;
  HEAP8[((36904 + 14|0))>>0] = $179;
 }
 $180 = $car;
 $0 = $180;
 $181 = $0;
 STACKTOP = sp;return ($181|0);
}
function _Car_Init() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $n = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 HEAP8[29112>>0] = 8;
 HEAP8[29104>>0] = 0;
 HEAP8[29120>>0] = -1;
 HEAP8[29128>>0] = -1;
 HEAP8[29136>>0] = 0;
 HEAP8[29144>>0] = 0;
 $n = 0;
 while(1) {
  $0 = $n;
  $1 = ($0|0)<(5);
  if (!($1)) {
   break;
  }
  $2 = $n;
  $3 = (29160 + (($2*30)|0)|0);
  $4 = (($3) + 29|0);
  HEAP8[$4>>0] = 0;
  $5 = $n;
  $6 = (($5) + 1)|0;
  $n = $6;
 }
 STACKTOP = sp;return;
}
function _Car_GetRandomDirection($mask) {
 $mask = $mask|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $direction = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $mask;
 $2 = $1;
 $3 = ($2<<24>>24)!=(0);
 if (!($3)) {
  $0 = 0;
  $16 = $0;
  STACKTOP = sp;return ($16|0);
 }
 while(1) {
  $4 = (_Math_Random()|0);
  $5 = $4&65535;
  $6 = $5 & 3;
  $7 = $6&255;
  $direction = $7;
  $8 = $direction;
  $9 = $8&255;
  $10 = 1 << $9;
  $11 = $1;
  $12 = $11&255;
  $13 = $10 & $12;
  $14 = ($13|0)!=(0);
  if ($14) {
   break;
  }
 }
 $15 = $direction;
 $0 = $15;
 $16 = $0;
 STACKTOP = sp;return ($16|0);
}
function _Car_SpawnNPC($aiType) {
 $aiType = $aiType|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0;
 var $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0;
 var $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0, $202 = 0, $203 = 0, $204 = 0, $205 = 0;
 var $206 = 0, $207 = 0, $208 = 0, $209 = 0, $21 = 0, $210 = 0, $211 = 0, $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0, $219 = 0, $22 = 0, $220 = 0, $221 = 0, $222 = 0, $223 = 0;
 var $224 = 0, $225 = 0, $226 = 0, $227 = 0, $228 = 0, $229 = 0, $23 = 0, $230 = 0, $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0, $241 = 0;
 var $242 = 0, $243 = 0, $244 = 0, $245 = 0, $246 = 0, $247 = 0, $248 = 0, $249 = 0, $25 = 0, $250 = 0, $251 = 0, $252 = 0, $253 = 0, $254 = 0, $255 = 0, $256 = 0, $257 = 0, $258 = 0, $259 = 0, $26 = 0;
 var $260 = 0, $261 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0;
 var $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0;
 var $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0;
 var $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0;
 var $98 = 0, $99 = 0, $carDirection = 0, $direction = 0, $exits = 0, $i = 0, $macroTileX = 0, $macroTileY = 0, $newCar = 0, $offset = 0, $spawnX = 0, $spawnY = 0, $tileX = 0, $tileY = 0, $x = 0, $y = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $aiType;
 $1 = HEAP16[28952>>1]|0;
 $2 = $1 << 16 >> 16;
 $3 = (($2) + 112)|0;
 $4 = $3&65535;
 $x = $4;
 $5 = HEAP16[((28952 + 2|0))>>1]|0;
 $6 = $5 << 16 >> 16;
 $7 = (($6) + 96)|0;
 $8 = $7&65535;
 $y = $8;
 $offset = 0;
 $9 = (_Math_Random()|0);
 $10 = $9&65535;
 $11 = $10 & 3;
 if ((($11|0) == 0)) {
  $offset = -150;
 } else if ((($11|0) == 1)) {
  $offset = 150;
 } else {
 }
 $12 = (_Math_Random()|0);
 $13 = $12&65535;
 $14 = $13 & 3;
 $15 = $14&255;
 $direction = $15;
 $16 = $direction;
 $17 = $16&255;
 if ((($17|0) == 2)) {
  $48 = $x;
  $49 = $48 << 16 >> 16;
  $50 = (($49) - 150)|0;
  $51 = $50&65535;
  $x = $51;
  $52 = $offset;
  $53 = $52 << 16 >> 16;
  $54 = $y;
  $55 = $54 << 16 >> 16;
  $56 = (($55) + ($53))|0;
  $57 = $56&65535;
  $y = $57;
 } else if ((($17|0) == 3)) {
  $18 = $offset;
  $19 = $18 << 16 >> 16;
  $20 = $x;
  $21 = $20 << 16 >> 16;
  $22 = (($21) + ($19))|0;
  $23 = $22&65535;
  $x = $23;
  $24 = $y;
  $25 = $24 << 16 >> 16;
  $26 = (($25) - 150)|0;
  $27 = $26&65535;
  $y = $27;
 } else if ((($17|0) == 0)) {
  $28 = $x;
  $29 = $28 << 16 >> 16;
  $30 = (($29) + 150)|0;
  $31 = $30&65535;
  $x = $31;
  $32 = $offset;
  $33 = $32 << 16 >> 16;
  $34 = $y;
  $35 = $34 << 16 >> 16;
  $36 = (($35) + ($33))|0;
  $37 = $36&65535;
  $y = $37;
 } else if ((($17|0) == 1)) {
  $38 = $offset;
  $39 = $38 << 16 >> 16;
  $40 = $x;
  $41 = $40 << 16 >> 16;
  $42 = (($41) + ($39))|0;
  $43 = $42&65535;
  $x = $43;
  $44 = $y;
  $45 = $44 << 16 >> 16;
  $46 = (($45) + 150)|0;
  $47 = $46&65535;
  $y = $47;
 }
 $58 = $x;
 $59 = $58 << 16 >> 16;
 $60 = $59 >> 7;
 $61 = $60&255;
 $macroTileX = $61;
 $62 = $y;
 $63 = $62 << 16 >> 16;
 $64 = $63 >> 7;
 $65 = $64&255;
 $macroTileY = $65;
 $66 = $macroTileX;
 $67 = $macroTileY;
 $68 = (_World_GetMacroTileExits($66,$67)|0);
 $exits = $68;
 $69 = $direction;
 $70 = $69&255;
 $71 = (($70) + 2)|0;
 $72 = $71 & 3;
 $73 = $72&255;
 $carDirection = $73;
 $74 = $exits;
 $75 = $74&255;
 $76 = $carDirection;
 $77 = $76&255;
 $78 = 1 << $77;
 $79 = $direction;
 $80 = $79&255;
 $81 = 1 << $80;
 $82 = $78 | $81;
 $83 = $75 & $82;
 $84 = ($83|0)!=(0);
 if (!($84)) {
  STACKTOP = sp;return;
 }
 $85 = $x;
 $86 = $85 << 16 >> 16;
 $87 = $86 >> 3;
 $88 = $87 & 15;
 $89 = $88&255;
 $tileX = $89;
 $90 = $y;
 $91 = $90 << 16 >> 16;
 $92 = $91 >> 3;
 $93 = $92 & 15;
 $94 = $93&255;
 $tileY = $94;
 $95 = $carDirection;
 $96 = $95&255;
 if ((($96|0) == 3)) {
  $tileX = 9;
 } else if ((($96|0) == 0)) {
  $tileY = 9;
 } else if ((($96|0) == 1)) {
  $tileX = 7;
 } else if ((($96|0) == 2)) {
  $tileY = 7;
 }
 $97 = $tileX;
 $98 = $97&255;
 $99 = ($98|0)<(7);
 do {
  if ($99) {
   $100 = $exits;
   $101 = $100&255;
   $102 = $101 & 4;
   $103 = ($102|0)!=(0);
   if ($103) {
    break;
   }
   STACKTOP = sp;return;
  } else {
   $104 = $tileX;
   $105 = $104&255;
   $106 = ($105|0)>(9);
   do {
    if ($106) {
     $107 = $exits;
     $108 = $107&255;
     $109 = $108 & 1;
     $110 = ($109|0)!=(0);
     if ($110) {
      break;
     }
     STACKTOP = sp;return;
    }
   } while(0);
  }
 } while(0);
 $111 = $tileY;
 $112 = $111&255;
 $113 = ($112|0)<(7);
 do {
  if ($113) {
   $114 = $exits;
   $115 = $114&255;
   $116 = $115 & 8;
   $117 = ($116|0)!=(0);
   if ($117) {
    break;
   }
   STACKTOP = sp;return;
  } else {
   $118 = $tileY;
   $119 = $118&255;
   $120 = ($119|0)>(9);
   do {
    if ($120) {
     $121 = $exits;
     $122 = $121&255;
     $123 = $122 & 2;
     $124 = ($123|0)!=(0);
     if ($124) {
      break;
     }
     STACKTOP = sp;return;
    }
   } while(0);
  }
 } while(0);
 $125 = $macroTileX;
 $126 = $125&255;
 $127 = $126 << 4;
 $128 = $tileX;
 $129 = $128&255;
 $130 = (($127) + ($129))|0;
 $131 = $130 << 3;
 $132 = $131 << 5;
 $133 = $132&65535;
 $spawnX = $133;
 $134 = $macroTileY;
 $135 = $134&255;
 $136 = $135 << 4;
 $137 = $tileY;
 $138 = $137&255;
 $139 = (($136) + ($138))|0;
 $140 = $139 << 3;
 $141 = $140 << 5;
 $142 = $141&65535;
 $spawnY = $142;
 $i = 0;
 while(1) {
  $143 = $i;
  $144 = $143&255;
  $145 = ($144|0)<(5);
  if (!($145)) {
   break;
  }
  $146 = $i;
  $147 = $146&255;
  $148 = (29160 + (($147*30)|0)|0);
  $149 = (($148) + 29|0);
  $150 = HEAP8[$149>>0]|0;
  $151 = $150&255;
  $152 = ($151|0)!=(0);
  if ($152) {
   $153 = $spawnX;
   $154 = $153&65535;
   $155 = $i;
   $156 = $155&255;
   $157 = (29160 + (($156*30)|0)|0);
   $158 = HEAP16[$157>>1]|0;
   $159 = $158&65535;
   $160 = (($159) - 1024)|0;
   $161 = ($154|0)>=($160|0);
   if ($161) {
    $162 = $spawnY;
    $163 = $162&65535;
    $164 = $i;
    $165 = $164&255;
    $166 = (29160 + (($165*30)|0)|0);
    $167 = (($166) + 2|0);
    $168 = HEAP16[$167>>1]|0;
    $169 = $168&65535;
    $170 = (($169) - 1024)|0;
    $171 = ($163|0)>=($170|0);
    if ($171) {
     $172 = $spawnX;
     $173 = $172&65535;
     $174 = $i;
     $175 = $174&255;
     $176 = (29160 + (($175*30)|0)|0);
     $177 = HEAP16[$176>>1]|0;
     $178 = $177&65535;
     $179 = (($178) + 1024)|0;
     $180 = ($173|0)<=($179|0);
     if ($180) {
      $181 = $spawnY;
      $182 = $181&65535;
      $183 = $i;
      $184 = $183&255;
      $185 = (29160 + (($184*30)|0)|0);
      $186 = (($185) + 2|0);
      $187 = HEAP16[$186>>1]|0;
      $188 = $187&65535;
      $189 = (($188) + 1024)|0;
      $190 = ($182|0)<=($189|0);
      if ($190) {
       label = 42;
       break;
      }
     }
    }
   }
  }
  $191 = $i;
  $192 = (($191) + 1)<<24>>24;
  $i = $192;
 }
 if ((label|0) == 42) {
  STACKTOP = sp;return;
 }
 $193 = $0;
 $194 = $spawnX;
 $195 = $spawnY;
 $196 = $carDirection;
 $197 = $196&255;
 $198 = $197<<6;
 $199 = $198&255;
 $200 = (_Car_Spawn($193,$194,$195,$199)|0);
 $newCar = $200;
 $201 = $0;
 $202 = $201&255;
 $203 = ($202|0)==(5);
 if ($203) {
  $207 = 255;
 } else {
  $204 = (_Math_RandomColorMask()|0);
  $205 = $204&255;
  $207 = $205;
 }
 $206 = $207&255;
 $208 = $newCar;
 $209 = (($208) + 24|0);
 HEAP8[$209>>0] = $206;
 $210 = $macroTileX;
 $211 = $210&255;
 $212 = $211 << 4;
 $213 = $macroTileY;
 $214 = $213&255;
 $215 = (($212) + ($214))|0;
 $216 = $215&255;
 $217 = $newCar;
 $218 = (($217) + 27|0);
 HEAP8[$218>>0] = $216;
 $219 = $carDirection;
 $220 = $newCar;
 $221 = (($220) + 27|0);
 $222 = (($221) + 1|0);
 $223 = HEAP8[$222>>0]|0;
 $224 = $219 & 15;
 $225 = $223 & -16;
 $226 = $225 | $224;
 HEAP8[$222>>0] = $226;
 $227 = $exits;
 $228 = $227&255;
 $229 = $newCar;
 $230 = (($229) + 27|0);
 $231 = (($230) + 1|0);
 $232 = HEAP8[$231>>0]|0;
 $233 = $232 & 15;
 $234 = $233&255;
 $235 = 1 << $234;
 $236 = $228 & $235;
 $237 = ($236|0)==(0);
 if (!($237)) {
  STACKTOP = sp;return;
 }
 $238 = $direction;
 $239 = $238&255;
 $240 = 1 << $239;
 $241 = $240 ^ -1;
 $242 = $exits;
 $243 = $242&255;
 $244 = $243 & $241;
 $245 = $244&255;
 $exits = $245;
 $246 = $exits;
 $247 = ($246<<24>>24)!=(0);
 if ($247) {
  $248 = $exits;
  $249 = (_Car_GetRandomDirection($248)|0);
  $250 = $newCar;
  $251 = (($250) + 27|0);
  $252 = (($251) + 1|0);
  $253 = HEAP8[$252>>0]|0;
  $254 = $249 & 15;
  $255 = $253 & -16;
  $256 = $255 | $254;
  HEAP8[$252>>0] = $256;
 } else {
  $257 = $newCar;
  $258 = (($257) + 27|0);
  $259 = (($258) + 1|0);
  $260 = HEAP8[$259>>0]|0;
  $261 = $260 & -16;
  HEAP8[$259>>0] = $261;
 }
 STACKTOP = sp;return;
}
function _Car_Update() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0;
 var $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $aiType = 0, $carSlotsAvailable = 0, $n = 0, $numPolice = 0, $spawnFrequency = 0, label = 0;
 var sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $carSlotsAvailable = 0;
 $numPolice = 0;
 $n = 0;
 while(1) {
  $0 = $n;
  $1 = ($0|0)<(5);
  if (!($1)) {
   break;
  }
  $2 = $n;
  $3 = (29160 + (($2*30)|0)|0);
  $4 = (($3) + 29|0);
  $5 = HEAP8[$4>>0]|0;
  $6 = $5&255;
  $7 = ($6|0)==(0);
  if ($7) {
   $8 = $carSlotsAvailable;
   $9 = (($8) + 1)<<24>>24;
   $carSlotsAvailable = $9;
  } else {
   $10 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
   $11 = ($10&65535) >>> 9;
   $12 = $11 & 1;
   $13 = $12&1;
   if ($13) {
    $14 = $n;
    $15 = (29160 + (($14*30)|0)|0);
    $16 = (($15) + 29|0);
    $17 = HEAP8[$16>>0]|0;
    $18 = $17&255;
    $19 = ($18|0)==(1);
    if ($19) {
     label = 9;
    } else {
     $20 = $n;
     $21 = (29160 + (($20*30)|0)|0);
     $22 = (($21) + 29|0);
     $23 = HEAP8[$22>>0]|0;
     $24 = $23&255;
     $25 = ($24|0)==(3);
     if ($25) {
      label = 9;
     } else {
      $26 = $n;
      $27 = (29160 + (($26*30)|0)|0);
      $28 = (($27) + 29|0);
      $29 = HEAP8[$28>>0]|0;
      $30 = $29&255;
      $31 = ($30|0)==(6);
      if ($31) {
       label = 9;
      }
     }
    }
    if ((label|0) == 9) {
     label = 0;
     $32 = $n;
     _Car_UpdateRaceTrackPosition($32);
    }
   }
   $33 = $n;
   $34 = (29160 + (($33*30)|0)|0);
   $35 = (($34) + 29|0);
   $36 = HEAP8[$35>>0]|0;
   $37 = $36&255;
   switch ($37|0) {
   case 1:  {
    $38 = $n;
    $39 = (29160 + (($38*30)|0)|0);
    _Car_UpdatePlayer($39,0);
    break;
   }
   case 2:  {
    $40 = $n;
    $41 = (29160 + (($40*30)|0)|0);
    _Car_UpdatePlayer($41,1);
    break;
   }
   case 3:  {
    $42 = $n;
    $43 = (29160 + (($42*30)|0)|0);
    _Car_UpdateRaceAI($43);
    break;
   }
   case 4:  {
    $44 = $n;
    $45 = (29160 + (($44*30)|0)|0);
    _Car_UpdateNPCAI($45);
    break;
   }
   case 5:  {
    $46 = $n;
    $47 = (29160 + (($46*30)|0)|0);
    _Car_UpdatePoliceAI($47);
    $48 = $numPolice;
    $49 = (($48) + 1)<<24>>24;
    $numPolice = $49;
    break;
   }
   case 6:  {
    $50 = $n;
    $51 = (29160 + (($50*30)|0)|0);
    _Car_UpdateMissionAI($51);
    break;
   }
   default: {
   }
   }
   $52 = $n;
   $53 = (29160 + (($52*30)|0)|0);
   _Car_UpdatePhysics($53);
  }
  $54 = $n;
  $55 = (($54) + 1)|0;
  $n = $55;
 }
 _Car_CheckCollisions();
 $56 = $carSlotsAvailable;
 $57 = $56&255;
 $58 = ($57|0)>(0);
 if ($58) {
  $59 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
  $60 = ($59&65535) >>> 11;
  $61 = $60 & 1;
  $62 = $61&1;
  if (!($62)) {
   $spawnFrequency = 10;
   $63 = HEAP8[29312>>0]|0;
   $64 = ($63<<24>>24)!=(0);
   if ($64) {
    $82 = HEAP8[29312>>0]|0;
    $83 = (($82) + -1)<<24>>24;
    HEAP8[29312>>0] = $83;
   } else {
    $aiType = 4;
    $65 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
    $66 = ($65&65535) >>> 6;
    $67 = $66 & 1;
    $68 = $67&1;
    do {
     if (!($68)) {
      $69 = $numPolice;
      $70 = $69&255;
      $71 = HEAP8[29104>>0]|0;
      $72 = $71&255;
      $73 = ($70|0)<($72|0);
      if (!($73)) {
       $74 = $numPolice;
       $75 = $74&255;
       $76 = ($75|0)==(0);
       if (!($76)) {
        break;
       }
       $77 = (_Math_Random()|0);
       $78 = $77&65535;
       $79 = $78 & 7;
       $80 = ($79|0)==(0);
       if (!($80)) {
        break;
       }
      }
      $aiType = 5;
     }
    } while(0);
    $81 = $aiType;
    _Car_SpawnNPC($81);
    HEAP8[29312>>0] = 10;
   }
  }
 }
 _Camera_Update();
 $84 = HEAP8[29104>>0]|0;
 $85 = $84&255;
 $86 = ($85|0)>(0);
 if (!($86)) {
  STACKTOP = sp;return;
 }
 $87 = HEAP8[29144>>0]|0;
 $88 = $87&255;
 $89 = ($88|0)>(0);
 if (!($89)) {
  STACKTOP = sp;return;
 }
 $90 = HEAP8[35920>>0]|0;
 $91 = $90&255;
 $92 = $91 & 31;
 $93 = ($92|0)==(1);
 if (!($93)) {
  STACKTOP = sp;return;
 }
 _TriggerFx(38,31,1);
 STACKTOP = sp;return;
}
function _Car_UpdateRaceTrackPosition($n) {
 $n = $n|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0;
 var $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0;
 var $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0;
 var $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0;
 var $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0;
 var $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $carMacroTile = 0, $oldIndex = 0, $trackDirection = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $n;
 $1 = $0;
 $2 = (29160 + (($1*30)|0)|0);
 $3 = HEAP16[$2>>1]|0;
 $4 = $3&65535;
 $5 = $4 >> 5;
 $6 = $5 >> 7;
 $7 = $6 << 4;
 $8 = $0;
 $9 = (29160 + (($8*30)|0)|0);
 $10 = (($9) + 2|0);
 $11 = HEAP16[$10>>1]|0;
 $12 = $11&65535;
 $13 = $12 >> 5;
 $14 = $13 >> 7;
 $15 = (($7) + ($14))|0;
 $16 = $15&255;
 $carMacroTile = $16;
 $17 = $0;
 $18 = (29160 + (($17*30)|0)|0);
 $19 = (($18) + 25|0);
 $20 = HEAP8[$19>>0]|0;
 $21 = (_Track_GetCoordFromIndex($20)|0);
 $22 = $21&255;
 $23 = $carMacroTile;
 $24 = $23&255;
 $25 = ($22|0)!=($24|0);
 if ($25) {
  $26 = $0;
  $27 = (29160 + (($26*30)|0)|0);
  $28 = (($27) + 25|0);
  $29 = HEAP8[$28>>0]|0;
  $oldIndex = $29;
  $30 = $0;
  $31 = (29160 + (($30*30)|0)|0);
  $32 = (($31) + 25|0);
  $33 = HEAP8[$32>>0]|0;
  $34 = $carMacroTile;
  $35 = (_Track_GetNewIndex($33,$34)|0);
  $36 = $0;
  $37 = (29160 + (($36*30)|0)|0);
  $38 = (($37) + 25|0);
  HEAP8[$38>>0] = $35;
  $39 = $oldIndex;
  $40 = $39&255;
  $41 = (_Track_GetLength()|0);
  $42 = $41&255;
  $43 = (($42) - 1)|0;
  $44 = ($40|0)==($43|0);
  if ($44) {
   $45 = $0;
   $46 = (29160 + (($45*30)|0)|0);
   $47 = (($46) + 25|0);
   $48 = HEAP8[$47>>0]|0;
   $49 = $48&255;
   $50 = ($49|0)==(0);
   if ($50) {
    $51 = $0;
    $52 = (29160 + (($51*30)|0)|0);
    $53 = (($52) + 25|0);
    $54 = (($53) + 1|0);
    $55 = HEAP8[$54>>0]|0;
    $56 = ($55&255) >>> 4;
    $57 = (($56) + 1)<<24>>24;
    $58 = HEAP8[$54>>0]|0;
    $59 = $57 & 15;
    $60 = ($59 << 4)&255;
    $61 = $58 & 15;
    $62 = $61 | $60;
    HEAP8[$54>>0] = $62;
   }
  }
  $63 = $0;
  $64 = (29160 + (($63*30)|0)|0);
  $65 = (($64) + 25|0);
  $66 = (($65) + 1|0);
  $67 = HEAP8[$66>>0]|0;
  $68 = ($67&255) >>> 4;
  $69 = $68&255;
  $70 = ($69|0)>(0);
  if ($70) {
   $71 = $oldIndex;
   $72 = $71&255;
   $73 = ($72|0)==(0);
   if ($73) {
    $74 = $0;
    $75 = (29160 + (($74*30)|0)|0);
    $76 = (($75) + 25|0);
    $77 = HEAP8[$76>>0]|0;
    $78 = $77&255;
    $79 = (_Track_GetLength()|0);
    $80 = $79&255;
    $81 = (($80) - 1)|0;
    $82 = ($78|0)==($81|0);
    if ($82) {
     $83 = $0;
     $84 = (29160 + (($83*30)|0)|0);
     $85 = (($84) + 25|0);
     $86 = (($85) + 1|0);
     $87 = HEAP8[$86>>0]|0;
     $88 = ($87&255) >>> 4;
     $89 = (($88) + -1)<<24>>24;
     $90 = HEAP8[$86>>0]|0;
     $91 = $89 & 15;
     $92 = ($91 << 4)&255;
     $93 = $90 & 15;
     $94 = $93 | $92;
     HEAP8[$86>>0] = $94;
    }
   }
  }
 }
 $95 = $0;
 $96 = (29160 + (($95*30)|0)|0);
 $97 = (($96) + 25|0);
 $98 = HEAP8[$97>>0]|0;
 $99 = (_Track_GetDirection($98)|0);
 $trackDirection = $99;
 $100 = $trackDirection;
 $101 = $100&255;
 if ((($101|0) == 0)) {
  $102 = $0;
  $103 = (29160 + (($102*30)|0)|0);
  $104 = HEAP16[$103>>1]|0;
  $105 = $104&65535;
  $106 = $105 >> 5;
  $107 = $106 >> 3;
  $108 = $107 & 15;
  $109 = $108&255;
  $110 = $0;
  $111 = (29160 + (($110*30)|0)|0);
  $112 = (($111) + 25|0);
  $113 = (($112) + 1|0);
  $114 = HEAP8[$113>>0]|0;
  $115 = $109 & 15;
  $116 = $114 & -16;
  $117 = $116 | $115;
  HEAP8[$113>>0] = $117;
  STACKTOP = sp;return;
 } else if ((($101|0) == 1)) {
  $118 = $0;
  $119 = (29160 + (($118*30)|0)|0);
  $120 = (($119) + 2|0);
  $121 = HEAP16[$120>>1]|0;
  $122 = $121&65535;
  $123 = $122 >> 5;
  $124 = $123 >> 3;
  $125 = $124 & 15;
  $126 = $125&255;
  $127 = $0;
  $128 = (29160 + (($127*30)|0)|0);
  $129 = (($128) + 25|0);
  $130 = (($129) + 1|0);
  $131 = HEAP8[$130>>0]|0;
  $132 = $126 & 15;
  $133 = $131 & -16;
  $134 = $133 | $132;
  HEAP8[$130>>0] = $134;
  STACKTOP = sp;return;
 } else if ((($101|0) == 2)) {
  $135 = $0;
  $136 = (29160 + (($135*30)|0)|0);
  $137 = HEAP16[$136>>1]|0;
  $138 = $137&65535;
  $139 = $138 >> 5;
  $140 = $139 >> 3;
  $141 = $140 & 15;
  $142 = (15 - ($141))|0;
  $143 = $142&255;
  $144 = $0;
  $145 = (29160 + (($144*30)|0)|0);
  $146 = (($145) + 25|0);
  $147 = (($146) + 1|0);
  $148 = HEAP8[$147>>0]|0;
  $149 = $143 & 15;
  $150 = $148 & -16;
  $151 = $150 | $149;
  HEAP8[$147>>0] = $151;
  STACKTOP = sp;return;
 } else if ((($101|0) == 3)) {
  $152 = $0;
  $153 = (29160 + (($152*30)|0)|0);
  $154 = (($153) + 2|0);
  $155 = HEAP16[$154>>1]|0;
  $156 = $155&65535;
  $157 = $156 >> 5;
  $158 = $157 >> 3;
  $159 = $158 & 15;
  $160 = (15 - ($159))|0;
  $161 = $160&255;
  $162 = $0;
  $163 = (29160 + (($162*30)|0)|0);
  $164 = (($163) + 25|0);
  $165 = (($164) + 1|0);
  $166 = HEAP8[$165>>0]|0;
  $167 = $161 & 15;
  $168 = $166 & -16;
  $169 = $168 | $167;
  HEAP8[$165>>0] = $169;
  STACKTOP = sp;return;
 } else {
  STACKTOP = sp;return;
 }
}
function _Car_UpdatePlayer($car,$inputNumber) {
 $car = $car|0;
 $inputNumber = $inputNumber|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0;
 var $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0;
 var $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0;
 var $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0;
 var $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $desiredAngle = 0, $vel = 0, $velX = 0, $velY = 0, $xDir = 0;
 var $yDir = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = $inputNumber;
 $2 = HEAP8[29112>>0]|0;
 $3 = $2&255;
 $4 = ($3|0)==(0);
 if ($4) {
  $5 = $0;
  $6 = (($5) + 22|0);
  HEAP16[$6>>1] = 0;
  STACKTOP = sp;return;
 }
 $7 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $8 = ($7&65535) >>> 1;
 $9 = $8 & 1;
 $10 = $9&1;
 if ($10) {
  $11 = $0;
  $12 = (($11) + 4|0);
  $13 = HEAP16[$12>>1]|0;
  $14 = $0;
  $15 = (($14) + 4|0);
  $16 = (($15) + 4|0);
  HEAP16[$16>>1] = $13;
  $17 = $0;
  $18 = (($17) + 12|0);
  $19 = (($18) + 2|0);
  $20 = HEAP16[$19>>1]|0;
  $21 = $0;
  $22 = (($21) + 12|0);
  $23 = (($22) + 6|0);
  HEAP16[$23>>1] = $20;
  $24 = $0;
  $25 = (($24) + 22|0);
  HEAP16[$25>>1] = 0;
  STACKTOP = sp;return;
 }
 $26 = $1;
 $27 = $26&255;
 $28 = ($27|0)==(1);
 if ($28) {
  $29 = $0;
  $30 = (($29) + 27|0);
  $31 = (($30) + 1|0);
  $32 = HEAP8[$31>>0]|0;
  $33 = ($32&255) >>> 5;
  $34 = $33 & 1;
  $35 = ($34<<24>>24)!=(0);
  if (!($35)) {
   $36 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
   $37 = ($36&65535) >>> 1;
   $38 = $37 & 1;
   $39 = $38&1;
   if (!($39)) {
    $40 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
    $41 = $40 & -3;
    $42 = $41 | 2;
    HEAP8[((36904 + 16|0))>>0]=$42&255;HEAP8[((36904 + 16|0))+1>>0]=$42>>8;
    _Mission_Fail(36864);
    $43 = $0;
    $44 = (($43) + 29|0);
    HEAP8[$44>>0] = 0;
    HEAP8[29128>>0] = -1;
    STACKTOP = sp;return;
   }
  }
 }
 $45 = $1;
 $46 = (_ReadJoypad($45)|0);
 $47 = $46&65535;
 $48 = $0;
 $49 = (($48) + 22|0);
 HEAP16[$49>>1] = $47;
 $50 = $0;
 $51 = (($50) + 4|0);
 $52 = HEAP16[$51>>1]|0;
 $53 = $52&65535;
 $54 = $0;
 $55 = (($54) + 4|0);
 $56 = (($55) + 4|0);
 $57 = HEAP16[$56>>1]|0;
 $58 = $57&65535;
 $59 = (($53) - ($58))|0;
 $60 = $59&65535;
 $velX = $60;
 $61 = $0;
 $62 = (($61) + 4|0);
 $63 = (($62) + 2|0);
 $64 = HEAP16[$63>>1]|0;
 $65 = $64&65535;
 $66 = $0;
 $67 = (($66) + 4|0);
 $68 = (($67) + 6|0);
 $69 = HEAP16[$68>>1]|0;
 $70 = $69&65535;
 $71 = (($65) - ($70))|0;
 $72 = $71&65535;
 $velY = $72;
 $73 = $velX;
 $74 = $73 << 16 >> 16;
 $75 = ($74|0)<(0);
 if ($75) {
  $76 = $velX;
  $77 = $76 << 16 >> 16;
  $78 = (0 - ($77))|0;
  $90 = $78;
 } else {
  $79 = $velX;
  $80 = $79 << 16 >> 16;
  $90 = $80;
 }
 $81 = $velY;
 $82 = $81 << 16 >> 16;
 $83 = ($82|0)<(0);
 if ($83) {
  $84 = $velY;
  $85 = $84 << 16 >> 16;
  $86 = (0 - ($85))|0;
  $91 = $86;
 } else {
  $87 = $velY;
  $88 = $87 << 16 >> 16;
  $91 = $88;
 }
 $89 = (($90) + ($91))|0;
 $92 = $89 >> 2;
 $93 = $92&65535;
 $vel = $93;
 $94 = HEAP8[35920>>0]|0;
 $95 = $94&255;
 $96 = $95 & 3;
 $97 = ($96|0)==(0);
 if ($97) {
 }
 $98 = $vel;
 $99 = $98 << 16 >> 16;
 $100 = $99 & 63;
 $101 = (30 + ($100))|0;
 $102 = $101&255;
 _TriggerNote(0,39,$102,127);
 $103 = $0;
 $104 = (($103) + 22|0);
 $105 = HEAP16[$104>>1]|0;
 $106 = $105&65535;
 $107 = $106 & 512;
 $108 = ($107|0)!=(0);
 if (!($108)) {
  STACKTOP = sp;return;
 }
 $xDir = 0;
 $yDir = 0;
 $109 = $0;
 $110 = (($109) + 22|0);
 $111 = HEAP16[$110>>1]|0;
 $112 = $111&65535;
 $113 = $112 & 64;
 $114 = ($113|0)!=(0);
 if ($114) {
  $115 = $xDir;
  $116 = (($115) + -1)<<24>>24;
  $xDir = $116;
 }
 $117 = $0;
 $118 = (($117) + 22|0);
 $119 = HEAP16[$118>>1]|0;
 $120 = $119&65535;
 $121 = $120 & 128;
 $122 = ($121|0)!=(0);
 if ($122) {
  $123 = $xDir;
  $124 = (($123) + 1)<<24>>24;
  $xDir = $124;
 }
 $125 = $0;
 $126 = (($125) + 22|0);
 $127 = HEAP16[$126>>1]|0;
 $128 = $127&65535;
 $129 = $128 & 16;
 $130 = ($129|0)!=(0);
 if ($130) {
  $131 = $yDir;
  $132 = (($131) + -1)<<24>>24;
  $yDir = $132;
 }
 $133 = $0;
 $134 = (($133) + 22|0);
 $135 = HEAP16[$134>>1]|0;
 $136 = $135&65535;
 $137 = $136 & 32;
 $138 = ($137|0)!=(0);
 if ($138) {
  $139 = $yDir;
  $140 = (($139) + 1)<<24>>24;
  $yDir = $140;
 }
 $141 = $xDir;
 $142 = $141 << 24 >> 24;
 $143 = ($142|0)!=(0);
 if ($143) {
  label = 29;
 } else {
  $144 = $yDir;
  $145 = $144 << 24 >> 24;
  $146 = ($145|0)!=(0);
  if ($146) {
   label = 29;
  }
 }
 if ((label|0) == 29) {
  $147 = $yDir;
  $148 = $147 << 24 >> 24;
  $149 = $xDir;
  $150 = $149 << 24 >> 24;
  $151 = (_Math_ATan2($148,$150)|0);
  $152 = $151&255;
  $desiredAngle = $152;
  $153 = $0;
  $154 = $desiredAngle;
  _AI_TurnToAngle($153,$154);
 }
 STACKTOP = sp;return;
}
function _Car_UpdateRaceAI($car) {
 $car = $car|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $macroTileX = 0, $macroTileY = 0, $nextCoord = 0, $nextIndex = 0, $relativeToPlayer = 0, $trackDirection = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = $0;
 $2 = (($1) + 4|0);
 $3 = HEAP16[$2>>1]|0;
 $4 = $3&65535;
 $5 = $4 >> 5;
 $6 = $5 >> 7;
 $7 = $6&255;
 $macroTileX = $7;
 $8 = $0;
 $9 = (($8) + 4|0);
 $10 = (($9) + 2|0);
 $11 = HEAP16[$10>>1]|0;
 $12 = $11&65535;
 $13 = $12 >> 5;
 $14 = $13 >> 7;
 $15 = $14&255;
 $macroTileY = $15;
 $16 = $macroTileX;
 $17 = $16&255;
 $18 = $17 << 4;
 $19 = $macroTileY;
 $20 = $19&255;
 $21 = (($18) + ($20))|0;
 $22 = $21&255;
 $23 = $0;
 $24 = (($23) + 27|0);
 HEAP8[$24>>0] = $22;
 $25 = $0;
 $26 = (($25) + 25|0);
 $27 = HEAP8[$26>>0]|0;
 $28 = (_Track_GetNextIndex($27)|0);
 $nextIndex = $28;
 $29 = $nextIndex;
 $30 = (_Track_GetCoordFromIndex($29)|0);
 $nextCoord = $30;
 $31 = $0;
 $32 = (($31) + 27|0);
 $33 = HEAP8[$32>>0]|0;
 $34 = $nextCoord;
 $35 = (_AI_GetDirectionTo($33,-1,$34)|0);
 $trackDirection = $35;
 $36 = $0;
 $37 = $trackDirection;
 _AI_DriveInDirection($36,$37);
 $38 = HEAP8[29120>>0]|0;
 $39 = $38&255;
 $40 = ($39|0)<(5);
 if (!($40)) {
  STACKTOP = sp;return;
 }
 $41 = $0;
 $42 = HEAP8[29120>>0]|0;
 $43 = $42&255;
 $44 = (29160 + (($43*30)|0)|0);
 $45 = (_Car_CompareRacePosition($41,$44)|0);
 $46 = $45 << 24 >> 24;
 $relativeToPlayer = $46;
 $47 = $relativeToPlayer;
 $48 = ($47|0)>(0);
 if ($48) {
  $49 = $0;
  _AI_ThrottleSpeed($49,3);
 }
 STACKTOP = sp;return;
}
function _Car_UpdateNPCAI($car) {
 $car = $car|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = $0;
 $2 = (_Car_NPCOutOfRange($1)|0);
 if ($2) {
  $3 = $0;
  $4 = (($3) + 29|0);
  HEAP8[$4>>0] = 0;
  STACKTOP = sp;return;
 } else {
  $5 = $0;
  _AI_Wander($5);
  $6 = $0;
  _AI_ThrottleSpeed($6,1);
  STACKTOP = sp;return;
 }
}
function _Car_UpdatePoliceAI($car) {
 $car = $car|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = $0;
 $2 = (_Car_NPCOutOfRange($1)|0);
 if ($2) {
  $3 = $0;
  $4 = (($3) + 29|0);
  HEAP8[$4>>0] = 0;
  STACKTOP = sp;return;
 }
 $5 = HEAP8[29104>>0]|0;
 $6 = $5&255;
 $7 = ($6|0)>(0);
 if ($7) {
  $8 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
  $9 = ($8&65535) >>> 1;
  $10 = $9 & 1;
  $11 = $10&1;
  if (!($11)) {
   $12 = HEAP8[29120>>0]|0;
   $13 = $12&255;
   $14 = ($13|0)<(5);
   if ($14) {
    $15 = $0;
    $16 = HEAP8[29120>>0]|0;
    $17 = $16&255;
    $18 = (29160 + (($17*30)|0)|0);
    $19 = HEAP8[29136>>0]|0;
    $20 = $19&255;
    $21 = ($20|0)==(0);
    if ($21) {
     $22 = HEAP8[29112>>0]|0;
     $23 = $22&255;
     $24 = ($23|0)>(0);
     $25 = $24;
    } else {
     $25 = 0;
    }
    _AI_ChaseTarget($15,$18,$25);
    $26 = HEAP8[29104>>0]|0;
    $27 = $26&255;
    if ((($27|0) == 2) | (($27|0) == 1)) {
     $28 = $0;
     _AI_ThrottleSpeed($28,3);
    } else if ((($27|0) == 3)) {
     $29 = $0;
     _AI_ThrottleSpeed($29,7);
    } else {
    }
    STACKTOP = sp;return;
   }
  }
 }
 $30 = $0;
 _Car_UpdateNPCAI($30);
 STACKTOP = sp;return;
}
function _Car_UpdateMissionAI($car) {
 $car = $car|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0;
 var $81 = 0, $82 = 0, $83 = 0, $84 = 0, $9 = 0, $posX = 0, $posY = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $2 = ($1&65535) >>> 6;
 $3 = $2 & 1;
 $4 = $3&1;
 if ($4) {
  $5 = HEAP8[29112>>0]|0;
  $6 = $5&255;
  $7 = ($6|0)==(0);
  if ($7) {
   $8 = $0;
   $9 = (($8) + 22|0);
   HEAP16[$9>>1] = 0;
   STACKTOP = sp;return;
  }
 }
 $10 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $11 = ($10&65535) >>> 10;
 $12 = $11 & 1;
 $13 = $12&1;
 if ($13) {
  $14 = $0;
  $15 = (_Car_NPCOutOfRange($14)|0);
  if ($15) {
   $16 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
   $17 = $16 & -3;
   $18 = $17 | 2;
   HEAP8[((36904 + 16|0))>>0]=$18&255;HEAP8[((36904 + 16|0))+1>>0]=$18>>8;
   $19 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
   $20 = $19 & -5;
   HEAP8[((36904 + 16|0))>>0]=$20&255;HEAP8[((36904 + 16|0))+1>>0]=$20>>8;
   _Mission_Fail(36864);
   $21 = $0;
   $22 = (($21) + 29|0);
   HEAP8[$22>>0] = 0;
   STACKTOP = sp;return;
  }
 }
 $23 = HEAP8[((36904 + 14|0))>>0]|0;
 $24 = ($23&255) >>> 4;
 $25 = $24&255;
 if ((($25|0) == 7)) {
  $67 = $0;
  _AI_Wander($67);
  $68 = $0;
  _AI_ThrottleSpeed($68,3);
  STACKTOP = sp;return;
 } else if ((($25|0) == 3)) {
  $69 = $0;
  _Car_UpdateRaceAI($69);
  STACKTOP = sp;return;
 } else if ((($25|0) == 0)) {
  $70 = $0;
  $71 = (($70) + 4|0);
  $72 = HEAP16[$71>>1]|0;
  $73 = $0;
  $74 = (($73) + 4|0);
  $75 = (($74) + 4|0);
  HEAP16[$75>>1] = $72;
  $76 = $0;
  $77 = (($76) + 12|0);
  $78 = (($77) + 2|0);
  $79 = HEAP16[$78>>1]|0;
  $80 = $0;
  $81 = (($80) + 12|0);
  $82 = (($81) + 6|0);
  HEAP16[$82>>1] = $79;
  $83 = $0;
  $84 = (($83) + 22|0);
  HEAP16[$84>>1] = 0;
  STACKTOP = sp;return;
 } else if ((($25|0) == 8)) {
  $26 = $0;
  $27 = HEAP8[((36904 + 12|0))>>0]|0;
  $28 = HEAP8[((36904 + 13|0))>>0]|0;
  (_AI_DriveToTile($26,$27,$28)|0);
  $29 = $0;
  _AI_ThrottleSpeed($29,3);
  $30 = $0;
  $31 = HEAP16[$30>>1]|0;
  $32 = $31&65535;
  $33 = $32 >> 8;
  $34 = $33&65535;
  $posX = $34;
  $35 = $0;
  $36 = (($35) + 2|0);
  $37 = HEAP16[$36>>1]|0;
  $38 = $37&65535;
  $39 = $38 >> 8;
  $40 = $39&65535;
  $posY = $40;
  $41 = $posX;
  $42 = $41&65535;
  $43 = HEAP8[((36904 + 12|0))>>0]|0;
  $44 = $43&255;
  $45 = (($44) - 1)|0;
  $46 = ($42|0)>=($45|0);
  if ($46) {
   $47 = $posY;
   $48 = $47&65535;
   $49 = HEAP8[((36904 + 13|0))>>0]|0;
   $50 = $49&255;
   $51 = (($50) - 1)|0;
   $52 = ($48|0)>=($51|0);
   if ($52) {
    $53 = $posX;
    $54 = $53&65535;
    $55 = HEAP8[((36904 + 12|0))>>0]|0;
    $56 = $55&255;
    $57 = (($56) + 1)|0;
    $58 = ($54|0)<=($57|0);
    if ($58) {
     $59 = $posY;
     $60 = $59&65535;
     $61 = HEAP8[((36904 + 13|0))>>0]|0;
     $62 = $61&255;
     $63 = (($62) + 1)|0;
     $64 = ($60|0)<=($63|0);
     if ($64) {
      $65 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
      $66 = $65 & -17;
      HEAP8[((36904 + 16|0))>>0]=$66&255;HEAP8[((36904 + 16|0))+1>>0]=$66>>8;
     }
    }
   }
  }
  STACKTOP = sp;return;
 } else {
  STACKTOP = sp;return;
 }
}
function _Car_UpdatePhysics($car) {
 $car = $car|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0;
 var $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0;
 var $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0;
 var $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0;
 var $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0;
 var $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0;
 var $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $forwardX = 0;
 var $forwardY = 0, $quantizedAngle = 0, $turn = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $forwardX = sp + 6|0;
 $forwardY = sp + 5|0;
 $0 = $car;
 $1 = $0;
 $2 = (($1) + 21|0);
 $3 = HEAP8[$2>>0]|0;
 $4 = $3&255;
 $5 = (($4) + 8)|0;
 $6 = $5 & 240;
 $7 = $6&255;
 $quantizedAngle = $7;
 $turn = 0;
 $8 = $0;
 $9 = (($8) + 22|0);
 $10 = HEAP16[$9>>1]|0;
 $11 = $10&65535;
 $12 = $11 & 64;
 $13 = ($12|0)!=(0);
 if ($13) {
  $14 = $turn;
  $15 = $14 << 24 >> 24;
  $16 = (($15) - 1)|0;
  $17 = $16&255;
  $turn = $17;
 }
 $18 = $0;
 $19 = (($18) + 22|0);
 $20 = HEAP16[$19>>1]|0;
 $21 = $20&65535;
 $22 = $21 & 128;
 $23 = ($22|0)!=(0);
 if ($23) {
  $24 = $turn;
  $25 = $24 << 24 >> 24;
  $26 = (($25) + 1)|0;
  $27 = $26&255;
  $turn = $27;
 }
 $28 = $turn;
 $29 = $28 << 24 >> 24;
 $30 = ($29|0)==(0);
 if ($30) {
  $31 = $0;
  $32 = (($31) + 20|0);
  $33 = HEAP8[$32>>0]|0;
  $34 = $33 << 24 >> 24;
  $35 = $34 >> 1;
  $36 = $35&255;
  HEAP8[$32>>0] = $36;
 } else {
  $37 = $turn;
  $38 = $37 << 24 >> 24;
  $39 = $0;
  $40 = (($39) + 20|0);
  $41 = HEAP8[$40>>0]|0;
  $42 = $41 << 24 >> 24;
  $43 = (($42) + ($38))|0;
  $44 = $43&255;
  HEAP8[$40>>0] = $44;
  $45 = $0;
  $46 = (($45) + 20|0);
  $47 = HEAP8[$46>>0]|0;
  $48 = $47 << 24 >> 24;
  $49 = ($48|0)<(-10);
  if ($49) {
   $50 = $0;
   $51 = (($50) + 20|0);
   HEAP8[$51>>0] = -10;
  } else {
   $52 = $0;
   $53 = (($52) + 20|0);
   $54 = HEAP8[$53>>0]|0;
   $55 = $54 << 24 >> 24;
   $56 = ($55|0)>(10);
   if ($56) {
    $57 = $0;
    $58 = (($57) + 20|0);
    HEAP8[$58>>0] = 10;
   }
  }
 }
 $59 = $turn;
 $60 = $59 << 24 >> 24;
 $61 = ($60*10)|0;
 $62 = $61&255;
 $63 = $0;
 $64 = (($63) + 20|0);
 HEAP8[$64>>0] = $62;
 $65 = $0;
 $66 = (($65) + 4|0);
 $67 = $quantizedAngle;
 $68 = $67&255;
 $69 = $0;
 $70 = (($69) + 20|0);
 $71 = HEAP8[$70>>0]|0;
 $72 = $71 << 24 >> 24;
 $73 = $72 >> 0;
 $74 = (($68) + ($73))|0;
 $75 = $74&255;
 _Particle_Step($66,$75,1);
 $76 = $0;
 $77 = (($76) + 12|0);
 $78 = $quantizedAngle;
 _Particle_Step($77,$78,0);
 $79 = $quantizedAngle;
 $80 = $79&255;
 $81 = $0;
 $82 = (($81) + 20|0);
 $83 = HEAP8[$82>>0]|0;
 $84 = $83 << 24 >> 24;
 $85 = $84 >> 0;
 $86 = (($80) + ($85))|0;
 $87 = $86&255;
 _Math_GetForwardVector($87,$forwardX,$forwardY);
 $88 = $0;
 $89 = (($88) + 22|0);
 $90 = HEAP16[$89>>1]|0;
 $91 = $90&65535;
 $92 = $91 & 256;
 $93 = ($92|0)!=(0);
 if ($93) {
  $94 = HEAP8[$forwardX>>0]|0;
  $95 = $94 << 24 >> 24;
  $96 = $95 >> 2;
  $97 = $0;
  $98 = (($97) + 4|0);
  $99 = HEAP16[$98>>1]|0;
  $100 = $99&65535;
  $101 = (($100) + ($96))|0;
  $102 = $101&65535;
  HEAP16[$98>>1] = $102;
  $103 = HEAP8[$forwardY>>0]|0;
  $104 = $103 << 24 >> 24;
  $105 = $104 >> 2;
  $106 = $0;
  $107 = (($106) + 4|0);
  $108 = (($107) + 2|0);
  $109 = HEAP16[$108>>1]|0;
  $110 = $109&65535;
  $111 = (($110) + ($105))|0;
  $112 = $111&65535;
  HEAP16[$108>>1] = $112;
 }
 $113 = $0;
 $114 = (($113) + 22|0);
 $115 = HEAP16[$114>>1]|0;
 $116 = $115&65535;
 $117 = $116 & 1;
 $118 = ($117|0)!=(0);
 if ($118) {
  $119 = HEAP8[$forwardX>>0]|0;
  $120 = $119 << 24 >> 24;
  $121 = ($120*6)|0;
  $122 = $121 >> 5;
  $123 = $0;
  $124 = (($123) + 4|0);
  $125 = HEAP16[$124>>1]|0;
  $126 = $125&65535;
  $127 = (($126) - ($122))|0;
  $128 = $127&65535;
  HEAP16[$124>>1] = $128;
  $129 = HEAP8[$forwardY>>0]|0;
  $130 = $129 << 24 >> 24;
  $131 = ($130*6)|0;
  $132 = $131 >> 5;
  $133 = $0;
  $134 = (($133) + 4|0);
  $135 = (($134) + 2|0);
  $136 = HEAP16[$135>>1]|0;
  $137 = $136&65535;
  $138 = (($137) - ($132))|0;
  $139 = $138&65535;
  HEAP16[$135>>1] = $139;
 }
 $140 = $0;
 $141 = (($140) + 4|0);
 $142 = $0;
 $143 = (($142) + 12|0);
 _Particle_Constrain($141,$143,1568);
 $144 = $0;
 $145 = (($144) + 4|0);
 $146 = HEAP16[$145>>1]|0;
 $147 = $146&65535;
 $148 = $147 >> 1;
 $149 = $0;
 $150 = (($149) + 12|0);
 $151 = HEAP16[$150>>1]|0;
 $152 = $151&65535;
 $153 = $152 >> 1;
 $154 = (($148) + ($153))|0;
 $155 = $154&65535;
 $156 = $0;
 HEAP16[$156>>1] = $155;
 $157 = $0;
 $158 = (($157) + 4|0);
 $159 = (($158) + 2|0);
 $160 = HEAP16[$159>>1]|0;
 $161 = $160&65535;
 $162 = $161 >> 1;
 $163 = $0;
 $164 = (($163) + 12|0);
 $165 = (($164) + 2|0);
 $166 = HEAP16[$165>>1]|0;
 $167 = $166&65535;
 $168 = $167 >> 1;
 $169 = (($162) + ($168))|0;
 $170 = $169&65535;
 $171 = $0;
 $172 = (($171) + 2|0);
 HEAP16[$172>>1] = $170;
 $173 = $0;
 $174 = (($173) + 4|0);
 $175 = (($174) + 2|0);
 $176 = HEAP16[$175>>1]|0;
 $177 = $176&65535;
 $178 = $0;
 $179 = (($178) + 12|0);
 $180 = (($179) + 2|0);
 $181 = HEAP16[$180>>1]|0;
 $182 = $181&65535;
 $183 = (($177) - ($182))|0;
 $184 = $183&65535;
 $185 = $0;
 $186 = (($185) + 4|0);
 $187 = HEAP16[$186>>1]|0;
 $188 = $187&65535;
 $189 = $0;
 $190 = (($189) + 12|0);
 $191 = HEAP16[$190>>1]|0;
 $192 = $191&65535;
 $193 = (($188) - ($192))|0;
 $194 = $193&65535;
 $195 = (_Math_ATan2($184,$194)|0);
 $196 = $195&255;
 $197 = $0;
 $198 = (($197) + 21|0);
 HEAP8[$198>>0] = $196;
 STACKTOP = sp;return;
}
function _Camera_Update() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP8[29120>>0]|0;
 $1 = $0&255;
 $2 = ($1|0)<(5);
 if ($2) {
  $3 = HEAP8[29128>>0]|0;
  $4 = $3&255;
  $5 = ($4|0)<(5);
  if ($5) {
   $6 = HEAP8[29128>>0]|0;
   $7 = $6&255;
   $8 = (29160 + (($7*30)|0)|0);
   $9 = HEAP16[$8>>1]|0;
   $10 = $9&65535;
   $11 = $10 >> 5;
   $12 = HEAP8[29120>>0]|0;
   $13 = $12&255;
   $14 = (29160 + (($13*30)|0)|0);
   $15 = HEAP16[$14>>1]|0;
   $16 = $15&65535;
   $17 = $16 >> 5;
   $18 = (($11) + ($17))|0;
   $19 = $18 >> 1;
   $20 = (($19) - 112)|0;
   $21 = $20&65535;
   HEAP16[28952>>1] = $21;
   $22 = HEAP8[29128>>0]|0;
   $23 = $22&255;
   $24 = (29160 + (($23*30)|0)|0);
   $25 = (($24) + 2|0);
   $26 = HEAP16[$25>>1]|0;
   $27 = $26&65535;
   $28 = $27 >> 5;
   $29 = HEAP8[29120>>0]|0;
   $30 = $29&255;
   $31 = (29160 + (($30*30)|0)|0);
   $32 = (($31) + 2|0);
   $33 = HEAP16[$32>>1]|0;
   $34 = $33&65535;
   $35 = $34 >> 5;
   $36 = (($28) + ($35))|0;
   $37 = $36 >> 1;
   $38 = (($37) - 96)|0;
   $39 = $38&65535;
   HEAP16[((28952 + 2|0))>>1] = $39;
  } else {
   $40 = HEAP8[29120>>0]|0;
   $41 = $40&255;
   $42 = (29160 + (($41*30)|0)|0);
   $43 = HEAP16[$42>>1]|0;
   $44 = $43&65535;
   $45 = $44 >> 5;
   $46 = (($45) - 112)|0;
   $47 = $46&65535;
   HEAP16[28952>>1] = $47;
   $48 = HEAP8[29120>>0]|0;
   $49 = $48&255;
   $50 = (29160 + (($49*30)|0)|0);
   $51 = (($50) + 2|0);
   $52 = HEAP16[$51>>1]|0;
   $53 = $52&65535;
   $54 = $53 >> 5;
   $55 = (($54) - 96)|0;
   $56 = $55&65535;
   HEAP16[((28952 + 2|0))>>1] = $56;
  }
 }
 $57 = HEAP8[29152>>0]|0;
 $58 = ($57<<24>>24)!=(0);
 if (!($58)) {
  return;
 }
 $59 = (_Math_Random()|0);
 $60 = $59&65535;
 $61 = $60 & 3;
 $62 = (($61) - 2)|0;
 $63 = HEAP16[28952>>1]|0;
 $64 = $63 << 16 >> 16;
 $65 = (($64) + ($62))|0;
 $66 = $65&65535;
 HEAP16[28952>>1] = $66;
 $67 = HEAP8[29152>>0]|0;
 $68 = (($67) + -1)<<24>>24;
 HEAP8[29152>>0] = $68;
 return;
}
function _Particle_CheckParticleCollisions($a,$b) {
 $a = $a|0;
 $b = $b|0;
 var $$expand_i1_val = 0, $$expand_i1_val2 = 0, $$expand_i1_val4 = 0, $$pre_trunc = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0;
 var $112 = 0, $113 = 0, $114 = 0, $115 = 0, $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0;
 var $130 = 0, $131 = 0, $132 = 0, $133 = 0, $134 = 0, $135 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0;
 var $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0;
 var $deltaLengthSqr = 0, $deltaX = 0, $deltaY = 0, $mult = 0, $restLengthSqr = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $a;
 $2 = $b;
 $3 = $1;
 $4 = HEAP16[$3>>1]|0;
 $5 = $4&65535;
 $6 = $2;
 $7 = HEAP16[$6>>1]|0;
 $8 = $7&65535;
 $9 = (($8) - 192)|0;
 $10 = ($5|0)<($9|0);
 if (!($10)) {
  $11 = $1;
  $12 = HEAP16[$11>>1]|0;
  $13 = $12&65535;
  $14 = $2;
  $15 = HEAP16[$14>>1]|0;
  $16 = $15&65535;
  $17 = (($16) + 192)|0;
  $18 = ($13|0)>($17|0);
  if (!($18)) {
   $19 = $1;
   $20 = (($19) + 2|0);
   $21 = HEAP16[$20>>1]|0;
   $22 = $21&65535;
   $23 = $2;
   $24 = (($23) + 2|0);
   $25 = HEAP16[$24>>1]|0;
   $26 = $25&65535;
   $27 = (($26) - 192)|0;
   $28 = ($22|0)<($27|0);
   if (!($28)) {
    $29 = $1;
    $30 = (($29) + 2|0);
    $31 = HEAP16[$30>>1]|0;
    $32 = $31&65535;
    $33 = $2;
    $34 = (($33) + 2|0);
    $35 = HEAP16[$34>>1]|0;
    $36 = $35&65535;
    $37 = (($36) + 192)|0;
    $38 = ($32|0)>($37|0);
    if (!($38)) {
     $39 = $2;
     $40 = HEAP16[$39>>1]|0;
     $41 = $40&65535;
     $42 = $1;
     $43 = HEAP16[$42>>1]|0;
     $44 = $43&65535;
     $45 = (($41) - ($44))|0;
     $46 = $45&65535;
     $deltaX = $46;
     $47 = $2;
     $48 = (($47) + 2|0);
     $49 = HEAP16[$48>>1]|0;
     $50 = $49&65535;
     $51 = $1;
     $52 = (($51) + 2|0);
     $53 = HEAP16[$52>>1]|0;
     $54 = $53&65535;
     $55 = (($50) - ($54))|0;
     $56 = $55&65535;
     $deltaY = $56;
     $57 = $deltaX;
     $58 = $57 << 16 >> 16;
     $59 = $58 >> 5;
     $60 = $59&65535;
     $deltaX = $60;
     $61 = $deltaY;
     $62 = $61 << 16 >> 16;
     $63 = $62 >> 5;
     $64 = $63&65535;
     $deltaY = $64;
     $restLengthSqr = 36;
     $65 = $deltaX;
     $66 = $65 << 16 >> 16;
     $67 = $deltaX;
     $68 = $67 << 16 >> 16;
     $69 = Math_imul($66, $68)|0;
     $70 = $deltaY;
     $71 = $70 << 16 >> 16;
     $72 = $deltaY;
     $73 = $72 << 16 >> 16;
     $74 = Math_imul($71, $73)|0;
     $75 = (($69) + ($74))|0;
     $76 = $75&65535;
     $deltaLengthSqr = $76;
     $77 = $deltaLengthSqr;
     $78 = $77 << 16 >> 16;
     $79 = $restLengthSqr;
     $80 = $79 << 16 >> 16;
     $81 = ($78|0)>($80|0);
     if ($81) {
      $$expand_i1_val2 = 0;
      $0 = $$expand_i1_val2;
      $$pre_trunc = $0;
      $135 = $$pre_trunc&1;
      STACKTOP = sp;return ($135|0);
     } else {
      $82 = $restLengthSqr;
      $83 = $82 << 16 >> 16;
      $84 = $83 << 5;
      $85 = $deltaLengthSqr;
      $86 = $85 << 16 >> 16;
      $87 = $restLengthSqr;
      $88 = $87 << 16 >> 16;
      $89 = (($86) + ($88))|0;
      $90 = (($84|0) / ($89|0))&-1;
      $91 = (($90) - 16)|0;
      $92 = $91&65535;
      $mult = $92;
      $93 = $mult;
      $94 = $93 << 16 >> 16;
      $95 = $deltaX;
      $96 = $95 << 16 >> 16;
      $97 = Math_imul($96, $94)|0;
      $98 = $97&65535;
      $deltaX = $98;
      $99 = $mult;
      $100 = $99 << 16 >> 16;
      $101 = $deltaY;
      $102 = $101 << 16 >> 16;
      $103 = Math_imul($102, $100)|0;
      $104 = $103&65535;
      $deltaY = $104;
      $105 = $deltaX;
      $106 = $105 << 16 >> 16;
      $107 = $1;
      $108 = HEAP16[$107>>1]|0;
      $109 = $108&65535;
      $110 = (($109) - ($106))|0;
      $111 = $110&65535;
      HEAP16[$107>>1] = $111;
      $112 = $deltaY;
      $113 = $112 << 16 >> 16;
      $114 = $1;
      $115 = (($114) + 2|0);
      $116 = HEAP16[$115>>1]|0;
      $117 = $116&65535;
      $118 = (($117) - ($113))|0;
      $119 = $118&65535;
      HEAP16[$115>>1] = $119;
      $120 = $deltaX;
      $121 = $120 << 16 >> 16;
      $122 = $2;
      $123 = HEAP16[$122>>1]|0;
      $124 = $123&65535;
      $125 = (($124) + ($121))|0;
      $126 = $125&65535;
      HEAP16[$122>>1] = $126;
      $127 = $deltaY;
      $128 = $127 << 16 >> 16;
      $129 = $2;
      $130 = (($129) + 2|0);
      $131 = HEAP16[$130>>1]|0;
      $132 = $131&65535;
      $133 = (($132) + ($128))|0;
      $134 = $133&65535;
      HEAP16[$130>>1] = $134;
      $$expand_i1_val4 = 1;
      $0 = $$expand_i1_val4;
      $$pre_trunc = $0;
      $135 = $$pre_trunc&1;
      STACKTOP = sp;return ($135|0);
     }
    }
   }
  }
 }
 $$expand_i1_val = 0;
 $0 = $$expand_i1_val;
 $$pre_trunc = $0;
 $135 = $$pre_trunc&1;
 STACKTOP = sp;return ($135|0);
}
function _Particle_CheckCollisionWithOffset($p,$offX,$offY) {
 $p = $p|0;
 $offX = $offX|0;
 $offY = $offY|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0;
 var $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $19 = 0, $2 = 0;
 var $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0;
 var $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0;
 var $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0;
 var $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0;
 var $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $collided = 0, $prevTileX = 0, $prevTileY = 0, $tile = 0, $tileX = 0, $tileY = 0, $xDiff = 0, $yDiff = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $p;
 $1 = $offX;
 $2 = $offY;
 $3 = $0;
 $4 = (($3) + 4|0);
 $5 = HEAP16[$4>>1]|0;
 $6 = $5&65535;
 $7 = $6 >> 5;
 $8 = $7 >> 3;
 $9 = $8&65535;
 $prevTileX = $9;
 $10 = $0;
 $11 = (($10) + 6|0);
 $12 = HEAP16[$11>>1]|0;
 $13 = $12&65535;
 $14 = $13 >> 5;
 $15 = $14 >> 3;
 $16 = $15&65535;
 $prevTileY = $16;
 $17 = $0;
 $18 = HEAP16[$17>>1]|0;
 $19 = $18&65535;
 $20 = $1;
 $21 = $20 << 16 >> 16;
 $22 = (($19) + ($21))|0;
 $23 = $22 >> 5;
 $24 = $23 >> 3;
 $25 = $24&65535;
 $tileX = $25;
 $26 = $0;
 $27 = (($26) + 2|0);
 $28 = HEAP16[$27>>1]|0;
 $29 = $28&65535;
 $30 = $2;
 $31 = $30 << 16 >> 16;
 $32 = (($29) + ($31))|0;
 $33 = $32 >> 5;
 $34 = $33 >> 3;
 $35 = $34&65535;
 $tileY = $35;
 $36 = $tileX;
 $37 = $tileY;
 $38 = (_World_GetTile($36,$37)|0);
 $tile = $38;
 $collided = 0;
 $xDiff = 255;
 $yDiff = 255;
 $39 = $tile;
 $40 = $39&255;
 $41 = ($40|0)>=(19);
 if (!($41)) {
  $185 = $collided;
  $186 = $185&1;
  STACKTOP = sp;return ($186|0);
 }
 $42 = $tileY;
 $43 = $42 << 16 >> 16;
 $44 = $prevTileY;
 $45 = $44 << 16 >> 16;
 $46 = ($43|0)<($45|0);
 if ($46) {
  $47 = $tileX;
  $48 = $tileY;
  $49 = $48 << 16 >> 16;
  $50 = (($49) + 1)|0;
  $51 = $50&65535;
  $52 = (_World_GetTile($47,$51)|0);
  $53 = $52&255;
  $54 = ($53|0)<(19);
  if ($54) {
   $55 = $tileY;
   $56 = $55 << 16 >> 16;
   $57 = (($56) + 1)|0;
   $58 = $57 << 3;
   $59 = $58 << 5;
   $60 = (($59) + 96)|0;
   $61 = $0;
   $62 = (($61) + 2|0);
   $63 = HEAP16[$62>>1]|0;
   $64 = $63&65535;
   $65 = (($60) - ($64))|0;
   $66 = $65&65535;
   $yDiff = $66;
   $collided = 1;
  } else {
   label = 5;
  }
 } else {
  label = 5;
 }
 if ((label|0) == 5) {
  $67 = $tileY;
  $68 = $67 << 16 >> 16;
  $69 = $prevTileY;
  $70 = $69 << 16 >> 16;
  $71 = ($68|0)>($70|0);
  if ($71) {
   $72 = $tileX;
   $73 = $tileY;
   $74 = $73 << 16 >> 16;
   $75 = (($74) - 1)|0;
   $76 = $75&65535;
   $77 = (_World_GetTile($72,$76)|0);
   $78 = $77&255;
   $79 = ($78|0)<(19);
   if ($79) {
    $80 = $0;
    $81 = (($80) + 2|0);
    $82 = HEAP16[$81>>1]|0;
    $83 = $82&65535;
    $84 = $tileY;
    $85 = $84 << 16 >> 16;
    $86 = $85 << 3;
    $87 = $86 << 5;
    $88 = (($87) - 96)|0;
    $89 = (($83) - ($88))|0;
    $90 = $89&65535;
    $yDiff = $90;
    $collided = 1;
   }
  }
 }
 $91 = $tileX;
 $92 = $91 << 16 >> 16;
 $93 = $prevTileX;
 $94 = $93 << 16 >> 16;
 $95 = ($92|0)<($94|0);
 if ($95) {
  $96 = $tileX;
  $97 = $96 << 16 >> 16;
  $98 = (($97) + 1)|0;
  $99 = $98&65535;
  $100 = $tileY;
  $101 = (_World_GetTile($99,$100)|0);
  $102 = $101&255;
  $103 = ($102|0)<(19);
  if ($103) {
   $104 = $tileX;
   $105 = $104 << 16 >> 16;
   $106 = (($105) + 1)|0;
   $107 = $106 << 3;
   $108 = $107 << 5;
   $109 = (($108) + 96)|0;
   $110 = $0;
   $111 = HEAP16[$110>>1]|0;
   $112 = $111&65535;
   $113 = (($109) - ($112))|0;
   $114 = $113&65535;
   $xDiff = $114;
   $collided = 1;
  } else {
   label = 12;
  }
 } else {
  label = 12;
 }
 if ((label|0) == 12) {
  $115 = $tileX;
  $116 = $115 << 16 >> 16;
  $117 = $prevTileX;
  $118 = $117 << 16 >> 16;
  $119 = ($116|0)>($118|0);
  if ($119) {
   $120 = $tileX;
   $121 = $120 << 16 >> 16;
   $122 = (($121) - 1)|0;
   $123 = $122&65535;
   $124 = $tileY;
   $125 = (_World_GetTile($123,$124)|0);
   $126 = $125&255;
   $127 = ($126|0)<(19);
   if ($127) {
    $128 = $0;
    $129 = HEAP16[$128>>1]|0;
    $130 = $129&65535;
    $131 = $tileX;
    $132 = $131 << 16 >> 16;
    $133 = $132 << 3;
    $134 = $133 << 5;
    $135 = (($134) - 96)|0;
    $136 = (($130) - ($135))|0;
    $137 = $136&65535;
    $xDiff = $137;
    $collided = 1;
   }
  }
 }
 $138 = $collided;
 $139 = $138&1;
 if ($139) {
  $140 = $xDiff;
  $141 = $140&65535;
  $142 = $yDiff;
  $143 = $142&65535;
  $144 = ($141|0)<($143|0);
  if ($144) {
   $145 = $tileX;
   $146 = $145 << 16 >> 16;
   $147 = $prevTileX;
   $148 = $147 << 16 >> 16;
   $149 = ($146|0)<($148|0);
   if ($149) {
    $150 = $xDiff;
    $151 = $150&65535;
    $152 = $0;
    $153 = HEAP16[$152>>1]|0;
    $154 = $153&65535;
    $155 = (($154) + ($151))|0;
    $156 = $155&65535;
    HEAP16[$152>>1] = $156;
   } else {
    $157 = $xDiff;
    $158 = $157&65535;
    $159 = $0;
    $160 = HEAP16[$159>>1]|0;
    $161 = $160&65535;
    $162 = (($161) - ($158))|0;
    $163 = $162&65535;
    HEAP16[$159>>1] = $163;
   }
  } else {
   $164 = $tileY;
   $165 = $164 << 16 >> 16;
   $166 = $prevTileY;
   $167 = $166 << 16 >> 16;
   $168 = ($165|0)<($167|0);
   if ($168) {
    $169 = $yDiff;
    $170 = $169&65535;
    $171 = $0;
    $172 = (($171) + 2|0);
    $173 = HEAP16[$172>>1]|0;
    $174 = $173&65535;
    $175 = (($174) + ($170))|0;
    $176 = $175&65535;
    HEAP16[$172>>1] = $176;
   } else {
    $177 = $yDiff;
    $178 = $177&65535;
    $179 = $0;
    $180 = (($179) + 2|0);
    $181 = HEAP16[$180>>1]|0;
    $182 = $181&65535;
    $183 = (($182) - ($178))|0;
    $184 = $183&65535;
    HEAP16[$180>>1] = $184;
   }
  }
 }
 $185 = $collided;
 $186 = $185&1;
 STACKTOP = sp;return ($186|0);
}
function _Particle_CheckCollision($p) {
 $p = $p|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0;
 var $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0;
 var $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0;
 var $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0;
 var $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0;
 var $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $collided = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $p;
 $collided = 0;
 $1 = $0;
 $2 = HEAP16[$1>>1]|0;
 $3 = $2&65535;
 $4 = $0;
 $5 = (($4) + 4|0);
 $6 = HEAP16[$5>>1]|0;
 $7 = $6&65535;
 $8 = ($3|0)<($7|0);
 if ($8) {
  $9 = $0;
  $10 = (_Particle_CheckCollisionWithOffset($9,-96,0)|0);
  if ($10) {
   $14 = 1;
  } else {
   $11 = $collided;
   $12 = $11&1;
   $14 = $12;
  }
  $13 = $14&1;
  $collided = $13;
  $15 = $0;
  $16 = (($15) + 2|0);
  $17 = HEAP16[$16>>1]|0;
  $18 = $17&65535;
  $19 = $0;
  $20 = (($19) + 6|0);
  $21 = HEAP16[$20>>1]|0;
  $22 = $21&65535;
  $23 = ($18|0)<($22|0);
  if ($23) {
   $24 = $0;
   $25 = (_Particle_CheckCollisionWithOffset($24,-96,-96)|0);
   if ($25) {
    $29 = 1;
   } else {
    $26 = $collided;
    $27 = $26&1;
    $29 = $27;
   }
   $28 = $29&1;
   $collided = $28;
  } else {
   $30 = $0;
   $31 = (($30) + 2|0);
   $32 = HEAP16[$31>>1]|0;
   $33 = $32&65535;
   $34 = $0;
   $35 = (($34) + 6|0);
   $36 = HEAP16[$35>>1]|0;
   $37 = $36&65535;
   $38 = ($33|0)>($37|0);
   if ($38) {
    $39 = $0;
    $40 = (_Particle_CheckCollisionWithOffset($39,-96,96)|0);
    if ($40) {
     $44 = 1;
    } else {
     $41 = $collided;
     $42 = $41&1;
     $44 = $42;
    }
    $43 = $44&1;
    $collided = $43;
   }
  }
 } else {
  $45 = $0;
  $46 = HEAP16[$45>>1]|0;
  $47 = $46&65535;
  $48 = $0;
  $49 = (($48) + 4|0);
  $50 = HEAP16[$49>>1]|0;
  $51 = $50&65535;
  $52 = ($47|0)>($51|0);
  if ($52) {
   $53 = $0;
   $54 = (_Particle_CheckCollisionWithOffset($53,96,0)|0);
   if ($54) {
    $58 = 1;
   } else {
    $55 = $collided;
    $56 = $55&1;
    $58 = $56;
   }
   $57 = $58&1;
   $collided = $57;
   $59 = $0;
   $60 = (($59) + 2|0);
   $61 = HEAP16[$60>>1]|0;
   $62 = $61&65535;
   $63 = $0;
   $64 = (($63) + 6|0);
   $65 = HEAP16[$64>>1]|0;
   $66 = $65&65535;
   $67 = ($62|0)<($66|0);
   if ($67) {
    $68 = $0;
    $69 = (_Particle_CheckCollisionWithOffset($68,96,-96)|0);
    if ($69) {
     $73 = 1;
    } else {
     $70 = $collided;
     $71 = $70&1;
     $73 = $71;
    }
    $72 = $73&1;
    $collided = $72;
   } else {
    $74 = $0;
    $75 = (($74) + 2|0);
    $76 = HEAP16[$75>>1]|0;
    $77 = $76&65535;
    $78 = $0;
    $79 = (($78) + 6|0);
    $80 = HEAP16[$79>>1]|0;
    $81 = $80&65535;
    $82 = ($77|0)>($81|0);
    if ($82) {
     $83 = $0;
     $84 = (_Particle_CheckCollisionWithOffset($83,96,96)|0);
     if ($84) {
      $88 = 1;
     } else {
      $85 = $collided;
      $86 = $85&1;
      $88 = $86;
     }
     $87 = $88&1;
     $collided = $87;
    }
   }
  }
 }
 $89 = $0;
 $90 = (($89) + 2|0);
 $91 = HEAP16[$90>>1]|0;
 $92 = $91&65535;
 $93 = $0;
 $94 = (($93) + 6|0);
 $95 = HEAP16[$94>>1]|0;
 $96 = $95&65535;
 $97 = ($92|0)<($96|0);
 if ($97) {
  $98 = $0;
  $99 = (_Particle_CheckCollisionWithOffset($98,0,-96)|0);
  if ($99) {
   $103 = 1;
  } else {
   $100 = $collided;
   $101 = $100&1;
   $103 = $101;
  }
  $102 = $103&1;
  $collided = $102;
  $110 = $collided;
  $111 = $110&1;
  STACKTOP = sp;return ($111|0);
 } else {
  $104 = $0;
  $105 = (_Particle_CheckCollisionWithOffset($104,0,96)|0);
  if ($105) {
   $109 = 1;
  } else {
   $106 = $collided;
   $107 = $106&1;
   $109 = $107;
  }
  $108 = $109&1;
  $collided = $108;
  $110 = $collided;
  $111 = $110&1;
  STACKTOP = sp;return ($111|0);
 }
 return (0)|0;
}
function _Car_CheckCollisions() {
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0;
 var $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0;
 var $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0, $202 = 0, $203 = 0, $204 = 0, $205 = 0;
 var $206 = 0, $207 = 0, $208 = 0, $209 = 0, $21 = 0, $210 = 0, $211 = 0, $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0, $219 = 0, $22 = 0, $220 = 0, $221 = 0, $222 = 0, $223 = 0;
 var $224 = 0, $225 = 0, $226 = 0, $227 = 0, $228 = 0, $229 = 0, $23 = 0, $230 = 0, $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0, $241 = 0;
 var $242 = 0, $243 = 0, $244 = 0, $245 = 0, $246 = 0, $247 = 0, $248 = 0, $249 = 0, $25 = 0, $250 = 0, $251 = 0, $252 = 0, $253 = 0, $254 = 0, $255 = 0, $256 = 0, $257 = 0, $258 = 0, $259 = 0, $26 = 0;
 var $260 = 0, $261 = 0, $262 = 0, $263 = 0, $264 = 0, $265 = 0, $266 = 0, $267 = 0, $268 = 0, $269 = 0, $27 = 0, $270 = 0, $271 = 0, $272 = 0, $273 = 0, $274 = 0, $275 = 0, $276 = 0, $277 = 0, $278 = 0;
 var $279 = 0, $28 = 0, $280 = 0, $281 = 0, $282 = 0, $283 = 0, $284 = 0, $285 = 0, $286 = 0, $287 = 0, $288 = 0, $289 = 0, $29 = 0, $290 = 0, $291 = 0, $292 = 0, $293 = 0, $294 = 0, $3 = 0, $30 = 0;
 var $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0;
 var $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0;
 var $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0;
 var $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $collided = 0, $collided1 = 0, $i = 0, $j = 0, label = 0;
 var sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $i = 0;
 while(1) {
  $0 = $i;
  $1 = $0 << 24 >> 24;
  $2 = ($1|0)<(5);
  if (!($2)) {
   break;
  }
  $3 = $i;
  $4 = $3 << 24 >> 24;
  $5 = (29160 + (($4*30)|0)|0);
  $6 = (($5) + 29|0);
  $7 = HEAP8[$6>>0]|0;
  $8 = $7&255;
  $9 = ($8|0)!=(0);
  if ($9) {
   $10 = $i;
   $11 = $10 << 24 >> 24;
   $12 = (($11) + 1)|0;
   $13 = $12&255;
   $j = $13;
   while(1) {
    $14 = $j;
    $15 = $14 << 24 >> 24;
    $16 = ($15|0)<(5);
    if (!($16)) {
     break;
    }
    $17 = $j;
    $18 = $17 << 24 >> 24;
    $19 = (29160 + (($18*30)|0)|0);
    $20 = (($19) + 29|0);
    $21 = HEAP8[$20>>0]|0;
    $22 = $21&255;
    $23 = ($22|0)!=(0);
    if ($23) {
     $24 = $i;
     $25 = $24 << 24 >> 24;
     $26 = (29160 + (($25*30)|0)|0);
     $27 = HEAP16[$26>>1]|0;
     $28 = $27&65535;
     $29 = $j;
     $30 = $29 << 24 >> 24;
     $31 = (29160 + (($30*30)|0)|0);
     $32 = HEAP16[$31>>1]|0;
     $33 = $32&65535;
     $34 = (($33) - 512)|0;
     $35 = ($28|0)>($34|0);
     if ($35) {
      $36 = $i;
      $37 = $36 << 24 >> 24;
      $38 = (29160 + (($37*30)|0)|0);
      $39 = HEAP16[$38>>1]|0;
      $40 = $39&65535;
      $41 = $j;
      $42 = $41 << 24 >> 24;
      $43 = (29160 + (($42*30)|0)|0);
      $44 = HEAP16[$43>>1]|0;
      $45 = $44&65535;
      $46 = (($45) + 512)|0;
      $47 = ($40|0)<($46|0);
      if ($47) {
       $48 = $i;
       $49 = $48 << 24 >> 24;
       $50 = (29160 + (($49*30)|0)|0);
       $51 = (($50) + 2|0);
       $52 = HEAP16[$51>>1]|0;
       $53 = $52&65535;
       $54 = $j;
       $55 = $54 << 24 >> 24;
       $56 = (29160 + (($55*30)|0)|0);
       $57 = (($56) + 2|0);
       $58 = HEAP16[$57>>1]|0;
       $59 = $58&65535;
       $60 = (($59) - 512)|0;
       $61 = ($53|0)>($60|0);
       if ($61) {
        $62 = $i;
        $63 = $62 << 24 >> 24;
        $64 = (29160 + (($63*30)|0)|0);
        $65 = (($64) + 2|0);
        $66 = HEAP16[$65>>1]|0;
        $67 = $66&65535;
        $68 = $j;
        $69 = $68 << 24 >> 24;
        $70 = (29160 + (($69*30)|0)|0);
        $71 = (($70) + 2|0);
        $72 = HEAP16[$71>>1]|0;
        $73 = $72&65535;
        $74 = (($73) + 512)|0;
        $75 = ($67|0)<($74|0);
        if ($75) {
         $collided = 0;
         $76 = $i;
         $77 = $76 << 24 >> 24;
         $78 = (29160 + (($77*30)|0)|0);
         $79 = (($78) + 4|0);
         $80 = $j;
         $81 = $80 << 24 >> 24;
         $82 = (29160 + (($81*30)|0)|0);
         $83 = (($82) + 4|0);
         $84 = (_Particle_CheckParticleCollisions($79,$83)|0);
         $85 = $84&1;
         $collided = $85;
         $86 = $i;
         $87 = $86 << 24 >> 24;
         $88 = (29160 + (($87*30)|0)|0);
         $89 = (($88) + 12|0);
         $90 = $j;
         $91 = $90 << 24 >> 24;
         $92 = (29160 + (($91*30)|0)|0);
         $93 = (($92) + 12|0);
         $94 = (_Particle_CheckParticleCollisions($89,$93)|0);
         if ($94) {
          $98 = 1;
         } else {
          $95 = $collided;
          $96 = $95&1;
          $98 = $96;
         }
         $97 = $98&1;
         $collided = $97;
         $99 = $i;
         $100 = $99 << 24 >> 24;
         $101 = (29160 + (($100*30)|0)|0);
         $102 = (($101) + 4|0);
         $103 = $j;
         $104 = $103 << 24 >> 24;
         $105 = (29160 + (($104*30)|0)|0);
         $106 = (($105) + 12|0);
         $107 = (_Particle_CheckParticleCollisions($102,$106)|0);
         if ($107) {
          $111 = 1;
         } else {
          $108 = $collided;
          $109 = $108&1;
          $111 = $109;
         }
         $110 = $111&1;
         $collided = $110;
         $112 = $i;
         $113 = $112 << 24 >> 24;
         $114 = (29160 + (($113*30)|0)|0);
         $115 = (($114) + 12|0);
         $116 = $j;
         $117 = $116 << 24 >> 24;
         $118 = (29160 + (($117*30)|0)|0);
         $119 = (($118) + 4|0);
         $120 = (_Particle_CheckParticleCollisions($115,$119)|0);
         if ($120) {
          $124 = 1;
         } else {
          $121 = $collided;
          $122 = $121&1;
          $124 = $122;
         }
         $123 = $124&1;
         $collided = $123;
         $125 = $collided;
         $126 = $125&1;
         if ($126) {
          $127 = $i;
          $128 = $127 << 24 >> 24;
          $129 = (29160 + (($128*30)|0)|0);
          $130 = (($129) + 29|0);
          $131 = HEAP8[$130>>0]|0;
          $132 = $131&255;
          $133 = ($132|0)==(1);
          if ($133) {
           label = 20;
          } else {
           $134 = $j;
           $135 = $134 << 24 >> 24;
           $136 = (29160 + (($135*30)|0)|0);
           $137 = (($136) + 29|0);
           $138 = HEAP8[$137>>0]|0;
           $139 = $138&255;
           $140 = ($139|0)==(1);
           if ($140) {
            label = 20;
           }
          }
          if ((label|0) == 20) {
           label = 0;
           $141 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
           $142 = ($141&65535) >>> 1;
           $143 = $142 & 1;
           $144 = $143&1;
           L31: do {
            if (!($144)) {
             $145 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
             $146 = ($145&65535) >>> 13;
             $147 = $146 & 1;
             $148 = $147&1;
             if ($148) {
              break;
             }
             $149 = HEAP8[29136>>0]|0;
             $150 = ($149<<24>>24)!=(0);
             if ($150) {
              break;
             }
             $151 = HEAP8[29112>>0]|0;
             $152 = $151&255;
             $153 = ($152|0)>(0);
             if (!($153)) {
              break;
             }
             $154 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
             $155 = ($154&65535) >>> 6;
             $156 = $155 & 1;
             $157 = $156&1;
             do {
              if ($157) {
               $158 = $i;
               $159 = $158 << 24 >> 24;
               $160 = (29160 + (($159*30)|0)|0);
               $161 = (($160) + 29|0);
               $162 = HEAP8[$161>>0]|0;
               $163 = $162&255;
               $164 = ($163|0)==(6);
               if ($164) {
                break;
               }
               $165 = $j;
               $166 = $165 << 24 >> 24;
               $167 = (29160 + (($166*30)|0)|0);
               $168 = (($167) + 29|0);
               $169 = HEAP8[$168>>0]|0;
               $170 = $169&255;
               $171 = ($170|0)==(6);
               if ($171) {
                break;
               }
               $172 = $i;
               $173 = $172 << 24 >> 24;
               $174 = (29160 + (($173*30)|0)|0);
               $175 = (($174) + 29|0);
               $176 = HEAP8[$175>>0]|0;
               $177 = $176&255;
               $178 = ($177|0)==(2);
               if ($178) {
                break;
               }
               $179 = $j;
               $180 = $179 << 24 >> 24;
               $181 = (29160 + (($180*30)|0)|0);
               $182 = (($181) + 29|0);
               $183 = HEAP8[$182>>0]|0;
               $184 = $183&255;
               $185 = ($184|0)==(2);
               if (!($185)) {
                break L31;
               }
              }
             } while(0);
             _TriggerFx(31,-1,0);
             _Camera_Shake(5);
             $186 = HEAP8[29112>>0]|0;
             $187 = (($186) + -1)<<24>>24;
             HEAP8[29112>>0] = $187;
             _UI_UpdateHealth();
             HEAP8[29136>>0] = 64;
             $188 = HEAP8[29112>>0]|0;
             $189 = $188&255;
             $190 = ($189|0)==(0);
             if ($190) {
              $191 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
              $192 = ($191&65535) >>> 6;
              $193 = $192 & 1;
              $194 = $193&1;
              if ($194) {
               _Mission_Complete();
              } else {
               _Mission_Fail(36840);
              }
             }
             $195 = $i;
             $196 = $195 << 24 >> 24;
             $197 = (29160 + (($196*30)|0)|0);
             $198 = (($197) + 29|0);
             $199 = HEAP8[$198>>0]|0;
             $200 = $199&255;
             $201 = ($200|0)==(1);
             if ($201) {
              $202 = $i;
              $203 = $202 << 24 >> 24;
              $204 = (29160 + (($203*30)|0)|0);
              $205 = (($204) + 22|0);
              $206 = HEAP16[$205>>1]|0;
              $207 = $206&65535;
              $208 = ($207|0)!=(0);
              if ($208) {
               label = 38;
              } else {
               label = 36;
              }
             } else {
              label = 36;
             }
             do {
              if ((label|0) == 36) {
               label = 0;
               $209 = $j;
               $210 = $209 << 24 >> 24;
               $211 = (29160 + (($210*30)|0)|0);
               $212 = (($211) + 29|0);
               $213 = HEAP8[$212>>0]|0;
               $214 = $213&255;
               $215 = ($214|0)==(1);
               if (!($215)) {
                break;
               }
               $216 = $j;
               $217 = $216 << 24 >> 24;
               $218 = (29160 + (($217*30)|0)|0);
               $219 = (($218) + 22|0);
               $220 = HEAP16[$219>>1]|0;
               $221 = $220&65535;
               $222 = ($221|0)!=(0);
               if ($222) {
                label = 38;
               }
              }
             } while(0);
             if ((label|0) == 38) {
              label = 0;
              $223 = $i;
              $224 = $223 << 24 >> 24;
              $225 = (29160 + (($224*30)|0)|0);
              $226 = (($225) + 29|0);
              $227 = HEAP8[$226>>0]|0;
              $228 = $227&255;
              $229 = ($228|0)==(4);
              if ($229) {
               label = 40;
              } else {
               $230 = $j;
               $231 = $230 << 24 >> 24;
               $232 = (29160 + (($231*30)|0)|0);
               $233 = (($232) + 29|0);
               $234 = HEAP8[$233>>0]|0;
               $235 = $234&255;
               $236 = ($235|0)==(4);
               if ($236) {
                label = 40;
               }
              }
              if ((label|0) == 40) {
               label = 0;
               _Player_CommitOffence(3);
              }
              $237 = $i;
              $238 = $237 << 24 >> 24;
              $239 = (29160 + (($238*30)|0)|0);
              $240 = (($239) + 29|0);
              $241 = HEAP8[$240>>0]|0;
              $242 = $241&255;
              $243 = ($242|0)==(5);
              if ($243) {
               label = 43;
              } else {
               $244 = $j;
               $245 = $244 << 24 >> 24;
               $246 = (29160 + (($245*30)|0)|0);
               $247 = (($246) + 29|0);
               $248 = HEAP8[$247>>0]|0;
               $249 = $248&255;
               $250 = ($249|0)==(5);
               if ($250) {
                label = 43;
               }
              }
              if ((label|0) == 43) {
               label = 0;
               $251 = HEAP8[29104>>0]|0;
               $252 = $251&255;
               $253 = ($252|0)==(0);
               if ($253) {
                _Player_CommitOffence(0);
               }
              }
             }
            }
           } while(0);
          }
         }
        }
       }
      }
     }
    }
    $254 = $j;
    $255 = (($254) + 1)<<24>>24;
    $j = $255;
   }
  }
  $256 = $i;
  $257 = (($256) + 1)<<24>>24;
  $i = $257;
 }
 $i = 0;
 while(1) {
  $258 = $i;
  $259 = $258 << 24 >> 24;
  $260 = ($259|0)<(5);
  if (!($260)) {
   break;
  }
  $261 = $i;
  $262 = $261 << 24 >> 24;
  $263 = (29160 + (($262*30)|0)|0);
  $264 = (($263) + 29|0);
  $265 = HEAP8[$264>>0]|0;
  $266 = $265&255;
  $267 = ($266|0)!=(0);
  if ($267) {
   $collided1 = 0;
   $268 = $i;
   $269 = $268 << 24 >> 24;
   $270 = (29160 + (($269*30)|0)|0);
   $271 = (($270) + 4|0);
   $272 = (_Particle_CheckCollision($271)|0);
   $273 = $272&1;
   $collided1 = $273;
   $274 = $i;
   $275 = $274 << 24 >> 24;
   $276 = (29160 + (($275*30)|0)|0);
   $277 = (($276) + 12|0);
   $278 = (_Particle_CheckCollision($277)|0);
   if ($278) {
    $282 = 1;
   } else {
    $279 = $collided1;
    $280 = $279&1;
    $282 = $280;
   }
   $281 = $282&1;
   $collided1 = $281;
   $283 = $collided1;
   $284 = $283&1;
   if ($284) {
    $285 = $i;
    $286 = $285 << 24 >> 24;
    $287 = (29160 + (($286*30)|0)|0);
    $288 = (($287) + 4|0);
    $289 = $i;
    $290 = $289 << 24 >> 24;
    $291 = (29160 + (($290*30)|0)|0);
    $292 = (($291) + 12|0);
    _Particle_Constrain($288,$292,1568);
   }
  }
  $293 = $i;
  $294 = (($293) + 1)<<24>>24;
  $i = $294;
 }
 STACKTOP = sp;return;
}
function _Car_SetSprites() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $n = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 HEAP8[29144>>0] = 0;
 $n = 0;
 while(1) {
  $0 = $n;
  $1 = ($0|0)<(5);
  if (!($1)) {
   break;
  }
  $2 = $n;
  $3 = (29160 + (($2*30)|0)|0);
  $4 = (($3) + 29|0);
  $5 = HEAP8[$4>>0]|0;
  $6 = $5&255;
  $7 = ($6|0)!=(0);
  if ($7) {
   $8 = $n;
   $9 = (29160 + (($8*30)|0)|0);
   _Car_SetSprite($9);
  }
  $10 = $n;
  $11 = (($10) + 1)|0;
  $n = $11;
 }
 STACKTOP = sp;return;
}
function _Car_CompareRacePosition($first,$second) {
 $first = $first|0;
 $second = $second|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $first;
 $2 = $second;
 $3 = $1;
 $4 = (($3) + 25|0);
 $5 = (($4) + 1|0);
 $6 = HEAP8[$5>>0]|0;
 $7 = ($6&255) >>> 4;
 $8 = $7&255;
 $9 = $2;
 $10 = (($9) + 25|0);
 $11 = (($10) + 1|0);
 $12 = HEAP8[$11>>0]|0;
 $13 = ($12&255) >>> 4;
 $14 = $13&255;
 $15 = ($8|0)>($14|0);
 if ($15) {
  $0 = 1;
  $73 = $0;
  STACKTOP = sp;return ($73|0);
 }
 $16 = $1;
 $17 = (($16) + 25|0);
 $18 = (($17) + 1|0);
 $19 = HEAP8[$18>>0]|0;
 $20 = ($19&255) >>> 4;
 $21 = $20&255;
 $22 = $2;
 $23 = (($22) + 25|0);
 $24 = (($23) + 1|0);
 $25 = HEAP8[$24>>0]|0;
 $26 = ($25&255) >>> 4;
 $27 = $26&255;
 $28 = ($21|0)==($27|0);
 if ($28) {
  $29 = $1;
  $30 = (($29) + 25|0);
  $31 = HEAP8[$30>>0]|0;
  $32 = $31&255;
  $33 = $2;
  $34 = (($33) + 25|0);
  $35 = HEAP8[$34>>0]|0;
  $36 = $35&255;
  $37 = ($32|0)>($36|0);
  if ($37) {
   $0 = 1;
   $73 = $0;
   STACKTOP = sp;return ($73|0);
  }
  $38 = $1;
  $39 = (($38) + 25|0);
  $40 = HEAP8[$39>>0]|0;
  $41 = $40&255;
  $42 = $2;
  $43 = (($42) + 25|0);
  $44 = HEAP8[$43>>0]|0;
  $45 = $44&255;
  $46 = ($41|0)==($45|0);
  do {
   if ($46) {
    $47 = $1;
    $48 = (($47) + 25|0);
    $49 = (($48) + 1|0);
    $50 = HEAP8[$49>>0]|0;
    $51 = $50 & 15;
    $52 = $51&255;
    $53 = $2;
    $54 = (($53) + 25|0);
    $55 = (($54) + 1|0);
    $56 = HEAP8[$55>>0]|0;
    $57 = $56 & 15;
    $58 = $57&255;
    $59 = ($52|0)>($58|0);
    if ($59) {
     $0 = 1;
     $73 = $0;
     STACKTOP = sp;return ($73|0);
    }
    $60 = $1;
    $61 = (($60) + 25|0);
    $62 = (($61) + 1|0);
    $63 = HEAP8[$62>>0]|0;
    $64 = $63 & 15;
    $65 = $64&255;
    $66 = $2;
    $67 = (($66) + 25|0);
    $68 = (($67) + 1|0);
    $69 = HEAP8[$68>>0]|0;
    $70 = $69 & 15;
    $71 = $70&255;
    $72 = ($65|0)==($71|0);
    if ($72) {
     $0 = 0;
     $73 = $0;
     STACKTOP = sp;return ($73|0);
    } else {
     break;
    }
   }
  } while(0);
 }
 $0 = -1;
 $73 = $0;
 STACKTOP = sp;return ($73|0);
}
function _Particle_Constrain($a,$b,$unused) {
 $a = $a|0;
 $b = $b|0;
 $unused = $unused|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0;
 var $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $deltaLengthSqr = 0, $deltaX = 0, $deltaY = 0, $mult = 0, $restLengthSqr = 0, label = 0;
 var sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $a;
 $1 = $b;
 $2 = $unused;
 $3 = $1;
 $4 = HEAP16[$3>>1]|0;
 $5 = $4&65535;
 $6 = $0;
 $7 = HEAP16[$6>>1]|0;
 $8 = $7&65535;
 $9 = (($5) - ($8))|0;
 $10 = $9&65535;
 $deltaX = $10;
 $11 = $1;
 $12 = (($11) + 2|0);
 $13 = HEAP16[$12>>1]|0;
 $14 = $13&65535;
 $15 = $0;
 $16 = (($15) + 2|0);
 $17 = HEAP16[$16>>1]|0;
 $18 = $17&65535;
 $19 = (($14) - ($18))|0;
 $20 = $19&65535;
 $deltaY = $20;
 $21 = $deltaY;
 $22 = $21 << 16 >> 16;
 $23 = $22 >> 5;
 $24 = $23&65535;
 $deltaY = $24;
 $25 = $deltaX;
 $26 = $25 << 16 >> 16;
 $27 = $26 >> 5;
 $28 = $27&65535;
 $deltaX = $28;
 $restLengthSqr = 49;
 $29 = $deltaX;
 $30 = $29 << 16 >> 16;
 $31 = $deltaX;
 $32 = $31 << 16 >> 16;
 $33 = Math_imul($30, $32)|0;
 $34 = $deltaY;
 $35 = $34 << 16 >> 16;
 $36 = $deltaY;
 $37 = $36 << 16 >> 16;
 $38 = Math_imul($35, $37)|0;
 $39 = (($33) + ($38))|0;
 $40 = $39&65535;
 $deltaLengthSqr = $40;
 $41 = $restLengthSqr;
 $42 = $41 << 16 >> 16;
 $43 = $42 << 5;
 $44 = $deltaLengthSqr;
 $45 = $44 << 16 >> 16;
 $46 = $restLengthSqr;
 $47 = $46 << 16 >> 16;
 $48 = (($45) + ($47))|0;
 $49 = (($43|0) / ($48|0))&-1;
 $50 = (($49) - 16)|0;
 $51 = $50&65535;
 $mult = $51;
 $52 = $mult;
 $53 = $52 << 16 >> 16;
 $54 = $deltaX;
 $55 = $54 << 16 >> 16;
 $56 = Math_imul($55, $53)|0;
 $57 = $56&65535;
 $deltaX = $57;
 $58 = $mult;
 $59 = $58 << 16 >> 16;
 $60 = $deltaY;
 $61 = $60 << 16 >> 16;
 $62 = Math_imul($61, $59)|0;
 $63 = $62&65535;
 $deltaY = $63;
 $64 = $deltaX;
 $65 = $64 << 16 >> 16;
 $66 = $0;
 $67 = HEAP16[$66>>1]|0;
 $68 = $67&65535;
 $69 = (($68) - ($65))|0;
 $70 = $69&65535;
 HEAP16[$66>>1] = $70;
 $71 = $deltaY;
 $72 = $71 << 16 >> 16;
 $73 = $0;
 $74 = (($73) + 2|0);
 $75 = HEAP16[$74>>1]|0;
 $76 = $75&65535;
 $77 = (($76) - ($72))|0;
 $78 = $77&65535;
 HEAP16[$74>>1] = $78;
 $79 = $deltaX;
 $80 = $79 << 16 >> 16;
 $81 = $1;
 $82 = HEAP16[$81>>1]|0;
 $83 = $82&65535;
 $84 = (($83) + ($80))|0;
 $85 = $84&65535;
 HEAP16[$81>>1] = $85;
 $86 = $deltaY;
 $87 = $86 << 16 >> 16;
 $88 = $1;
 $89 = (($88) + 2|0);
 $90 = HEAP16[$89>>1]|0;
 $91 = $90&65535;
 $92 = (($91) + ($87))|0;
 $93 = $92&65535;
 HEAP16[$89>>1] = $93;
 STACKTOP = sp;return;
}
function _Car_NPCOutOfRange($car) {
 $car = $car|0;
 var $$expand_i1_val = 0, $$expand_i1_val2 = 0, $$pre_trunc = 0, $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0;
 var $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $5 = 0, $6 = 0;
 var $7 = 0, $8 = 0, $9 = 0, $shiftedX = 0, $shiftedY = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $car;
 $2 = $1;
 $3 = HEAP16[$2>>1]|0;
 $4 = $3&65535;
 $5 = $4 >> 5;
 $6 = $5&65535;
 $shiftedX = $6;
 $7 = $1;
 $8 = (($7) + 2|0);
 $9 = HEAP16[$8>>1]|0;
 $10 = $9&65535;
 $11 = $10 >> 5;
 $12 = $11&65535;
 $shiftedY = $12;
 $13 = $shiftedX;
 $14 = $13 << 16 >> 16;
 $15 = HEAP16[28952>>1]|0;
 $16 = $15 << 16 >> 16;
 $17 = (($16) + 292)|0;
 $18 = ($14|0)>($17|0);
 if (!($18)) {
  $19 = $shiftedX;
  $20 = $19 << 16 >> 16;
  $21 = HEAP16[28952>>1]|0;
  $22 = $21 << 16 >> 16;
  $23 = (($22) + 112)|0;
  $24 = (($23) - 180)|0;
  $25 = ($20|0)<($24|0);
  if (!($25)) {
   $26 = $shiftedY;
   $27 = $26 << 16 >> 16;
   $28 = HEAP16[((28952 + 2|0))>>1]|0;
   $29 = $28 << 16 >> 16;
   $30 = (($29) + 276)|0;
   $31 = ($27|0)>($30|0);
   if (!($31)) {
    $32 = $shiftedY;
    $33 = $32 << 16 >> 16;
    $34 = HEAP16[((28952 + 2|0))>>1]|0;
    $35 = $34 << 16 >> 16;
    $36 = (($35) + 96)|0;
    $37 = (($36) - 180)|0;
    $38 = ($33|0)<($37|0);
    if (!($38)) {
     $$expand_i1_val2 = 0;
     $0 = $$expand_i1_val2;
     $$pre_trunc = $0;
     $39 = $$pre_trunc&1;
     STACKTOP = sp;return ($39|0);
    }
   }
  }
 }
 $$expand_i1_val = 1;
 $0 = $$expand_i1_val;
 $$pre_trunc = $0;
 $39 = $$pre_trunc&1;
 STACKTOP = sp;return ($39|0);
}
function _Particle_Step($particle,$angle,$lateralFriction) {
 $particle = $particle|0;
 $angle = $angle|0;
 $lateralFriction = $lateralFriction|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0;
 var $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0;
 var $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0;
 var $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0;
 var $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0;
 var $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $dot = 0, $latVelX = 0, $latVelY = 0, $normalX = 0, $normalY = 0, $tempX = 0, $tempY = 0, $velX = 0, $velY = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $normalX = sp + 20|0;
 $normalY = sp + 21|0;
 $0 = $particle;
 $1 = $angle;
 $3 = $lateralFriction&1;
 $2 = $3;
 $4 = $0;
 $5 = HEAP16[$4>>1]|0;
 $tempX = $5;
 $6 = $0;
 $7 = (($6) + 2|0);
 $8 = HEAP16[$7>>1]|0;
 $tempY = $8;
 $9 = $0;
 $10 = HEAP16[$9>>1]|0;
 $11 = $10&65535;
 $12 = $0;
 $13 = (($12) + 4|0);
 $14 = HEAP16[$13>>1]|0;
 $15 = $14&65535;
 $16 = (($11) - ($15))|0;
 $17 = $16&65535;
 $velX = $17;
 $18 = $0;
 $19 = (($18) + 2|0);
 $20 = HEAP16[$19>>1]|0;
 $21 = $20&65535;
 $22 = $0;
 $23 = (($22) + 6|0);
 $24 = HEAP16[$23>>1]|0;
 $25 = $24&65535;
 $26 = (($21) - ($25))|0;
 $27 = $26&65535;
 $velY = $27;
 $28 = $1;
 $29 = $28&255;
 $30 = (($29) + 64)|0;
 $31 = $30&255;
 _Math_GetForwardVector($31,$normalX,$normalY);
 $32 = HEAP8[$normalX>>0]|0;
 $33 = $32 << 24 >> 24;
 $34 = $velX;
 $35 = $34 << 16 >> 16;
 $36 = Math_imul($33, $35)|0;
 $37 = (($36) + 16)|0;
 $38 = $37 >> 5;
 $39 = HEAP8[$normalY>>0]|0;
 $40 = $39 << 24 >> 24;
 $41 = $velY;
 $42 = $41 << 16 >> 16;
 $43 = Math_imul($40, $42)|0;
 $44 = (($43) + 16)|0;
 $45 = $44 >> 5;
 $46 = (($38) + ($45))|0;
 $47 = $46&65535;
 $dot = $47;
 $48 = HEAP8[$normalX>>0]|0;
 $49 = $48 << 24 >> 24;
 $50 = $dot;
 $51 = $50 << 16 >> 16;
 $52 = Math_imul($49, $51)|0;
 $53 = (($52) + 16)|0;
 $54 = $53 >> 5;
 $55 = $54&65535;
 $latVelX = $55;
 $56 = HEAP8[$normalY>>0]|0;
 $57 = $56 << 24 >> 24;
 $58 = $dot;
 $59 = $58 << 16 >> 16;
 $60 = Math_imul($57, $59)|0;
 $61 = (($60) + 16)|0;
 $62 = $61 >> 5;
 $63 = $62&65535;
 $latVelY = $63;
 $64 = $latVelX;
 $65 = $64 << 16 >> 16;
 $66 = $65 >> 2;
 $67 = $velX;
 $68 = $67 << 16 >> 16;
 $69 = (($68) - ($66))|0;
 $70 = $69&65535;
 $velX = $70;
 $71 = $latVelY;
 $72 = $71 << 16 >> 16;
 $73 = $72 >> 2;
 $74 = $velY;
 $75 = $74 << 16 >> 16;
 $76 = (($75) - ($73))|0;
 $77 = $76&65535;
 $velY = $77;
 $78 = $velX;
 $79 = $78 << 16 >> 16;
 $80 = ($79|0)<(0);
 if ($80) {
  $81 = $velX;
  $82 = $81 << 16 >> 16;
  $83 = (0 - ($82))|0;
  $84 = $83 >> 5;
  $85 = (($84) + 1)|0;
  $86 = $velX;
  $87 = $86 << 16 >> 16;
  $88 = (($87) + ($85))|0;
  $89 = $88&65535;
  $velX = $89;
 } else {
  $90 = $velX;
  $91 = $90 << 16 >> 16;
  $92 = ($91|0)>(0);
  if ($92) {
   $93 = $velX;
   $94 = $93 << 16 >> 16;
   $95 = $94 >> 5;
   $96 = (($95) + 1)|0;
   $97 = $velX;
   $98 = $97 << 16 >> 16;
   $99 = (($98) - ($96))|0;
   $100 = $99&65535;
   $velX = $100;
  }
 }
 $101 = $velY;
 $102 = $101 << 16 >> 16;
 $103 = ($102|0)<(0);
 if ($103) {
  $104 = $velY;
  $105 = $104 << 16 >> 16;
  $106 = (0 - ($105))|0;
  $107 = $106 >> 5;
  $108 = (($107) + 1)|0;
  $109 = $velY;
  $110 = $109 << 16 >> 16;
  $111 = (($110) + ($108))|0;
  $112 = $111&65535;
  $velY = $112;
  $124 = $velX;
  $125 = $124 << 16 >> 16;
  $126 = $0;
  $127 = HEAP16[$126>>1]|0;
  $128 = $127&65535;
  $129 = (($128) + ($125))|0;
  $130 = $129&65535;
  HEAP16[$126>>1] = $130;
  $131 = $velY;
  $132 = $131 << 16 >> 16;
  $133 = $0;
  $134 = (($133) + 2|0);
  $135 = HEAP16[$134>>1]|0;
  $136 = $135&65535;
  $137 = (($136) + ($132))|0;
  $138 = $137&65535;
  HEAP16[$134>>1] = $138;
  $139 = $tempX;
  $140 = $0;
  $141 = (($140) + 4|0);
  HEAP16[$141>>1] = $139;
  $142 = $tempY;
  $143 = $0;
  $144 = (($143) + 6|0);
  HEAP16[$144>>1] = $142;
  STACKTOP = sp;return;
 }
 $113 = $velY;
 $114 = $113 << 16 >> 16;
 $115 = ($114|0)>(0);
 if ($115) {
  $116 = $velY;
  $117 = $116 << 16 >> 16;
  $118 = $117 >> 5;
  $119 = (($118) + 1)|0;
  $120 = $velY;
  $121 = $120 << 16 >> 16;
  $122 = (($121) - ($119))|0;
  $123 = $122&65535;
  $velY = $123;
 }
 $124 = $velX;
 $125 = $124 << 16 >> 16;
 $126 = $0;
 $127 = HEAP16[$126>>1]|0;
 $128 = $127&65535;
 $129 = (($128) + ($125))|0;
 $130 = $129&65535;
 HEAP16[$126>>1] = $130;
 $131 = $velY;
 $132 = $131 << 16 >> 16;
 $133 = $0;
 $134 = (($133) + 2|0);
 $135 = HEAP16[$134>>1]|0;
 $136 = $135&65535;
 $137 = (($136) + ($132))|0;
 $138 = $137&65535;
 HEAP16[$134>>1] = $138;
 $139 = $tempX;
 $140 = $0;
 $141 = (($140) + 4|0);
 HEAP16[$141>>1] = $139;
 $142 = $tempY;
 $143 = $0;
 $144 = (($143) + 6|0);
 HEAP16[$144>>1] = $142;
 STACKTOP = sp;return;
}
function _Car_SetSprite($car) {
 $car = $car|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0;
 var $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0;
 var $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0, $202 = 0, $203 = 0, $204 = 0, $205 = 0;
 var $206 = 0, $207 = 0, $208 = 0, $209 = 0, $21 = 0, $210 = 0, $211 = 0, $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0, $219 = 0, $22 = 0, $220 = 0, $221 = 0, $222 = 0, $223 = 0;
 var $224 = 0, $225 = 0, $226 = 0, $227 = 0, $228 = 0, $229 = 0, $23 = 0, $230 = 0, $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0, $241 = 0;
 var $242 = 0, $243 = 0, $244 = 0, $245 = 0, $246 = 0, $247 = 0, $248 = 0, $249 = 0, $25 = 0, $250 = 0, $251 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0;
 var $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0;
 var $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0;
 var $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0;
 var $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $angleIndex = 0, $colorMask = 0, $screenX = 0, $screenY = 0, $shiftedX = 0, $shiftedY = 0, $spriteIndex = 0, $typeOffset = 0;
 var label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = $0;
 $2 = HEAP16[$1>>1]|0;
 $3 = $2&65535;
 $4 = $3 >> 5;
 $5 = $4&65535;
 $shiftedX = $5;
 $6 = $0;
 $7 = (($6) + 2|0);
 $8 = HEAP16[$7>>1]|0;
 $9 = $8&65535;
 $10 = $9 >> 5;
 $11 = $10&65535;
 $shiftedY = $11;
 $12 = $shiftedX;
 $13 = $12 << 16 >> 16;
 $14 = HEAP16[28952>>1]|0;
 $15 = $14 << 16 >> 16;
 $16 = (($15) + 224)|0;
 $17 = ($13|0)>($16|0);
 if (!($17)) {
  $18 = $shiftedX;
  $19 = $18 << 16 >> 16;
  $20 = HEAP16[28952>>1]|0;
  $21 = $20 << 16 >> 16;
  $22 = ($19|0)<($21|0);
  if (!($22)) {
   $23 = $shiftedY;
   $24 = $23 << 16 >> 16;
   $25 = HEAP16[((28952 + 2|0))>>1]|0;
   $26 = $25 << 16 >> 16;
   $27 = (($26) + 192)|0;
   $28 = ($24|0)>($27|0);
   if (!($28)) {
    $29 = $shiftedY;
    $30 = $29 << 16 >> 16;
    $31 = HEAP16[((28952 + 2|0))>>1]|0;
    $32 = $31 << 16 >> 16;
    $33 = (($32) + 16)|0;
    $34 = ($30|0)<($33|0);
    if (!($34)) {
     $40 = $0;
     $41 = (($40) + 27|0);
     $42 = (($41) + 1|0);
     $43 = HEAP8[$42>>0]|0;
     $44 = $43 & -33;
     $45 = $44 | 32;
     HEAP8[$42>>0] = $45;
     $46 = $0;
     $47 = (($46) + 21|0);
     $48 = HEAP8[$47>>0]|0;
     $49 = $48&255;
     $50 = (($49) + 8)|0;
     $51 = $50 & 255;
     $52 = $51 >> 4;
     $53 = $52&255;
     $angleIndex = $53;
     $54 = $shiftedX;
     $55 = $54 << 16 >> 16;
     $56 = HEAP16[28952>>1]|0;
     $57 = $56 << 16 >> 16;
     $58 = (($55) - ($57))|0;
     $59 = $58&255;
     $screenX = $59;
     $60 = $shiftedY;
     $61 = $60 << 16 >> 16;
     $62 = HEAP16[((28952 + 2|0))>>1]|0;
     $63 = $62 << 16 >> 16;
     $64 = (($61) - ($63))|0;
     $65 = $64&255;
     $screenY = $65;
     $typeOffset = 0;
     $66 = $0;
     $67 = (($66) + 24|0);
     $68 = HEAP8[$67>>0]|0;
     $colorMask = $68;
     $69 = $0;
     $70 = (($69) + 29|0);
     $71 = HEAP8[$70>>0]|0;
     $72 = $71&255;
     $73 = ($72|0)==(5);
     do {
      if ($73) {
       label = 9;
      } else {
       $74 = $0;
       $75 = (($74) + 29|0);
       $76 = HEAP8[$75>>0]|0;
       $77 = $76&255;
       $78 = ($77|0)==(1);
       if ($78) {
        $79 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
        $80 = ($79&65535) >>> 6;
        $81 = $80 & 1;
        $82 = $81&1;
        if ($82) {
         label = 9;
         break;
        }
       }
       $100 = $0;
       $101 = (($100) + 29|0);
       $102 = HEAP8[$101>>0]|0;
       $103 = $102&255;
       $104 = ($103|0)==(1);
       do {
        if ($104) {
         label = 19;
        } else {
         $105 = $0;
         $106 = (($105) + 29|0);
         $107 = HEAP8[$106>>0]|0;
         $108 = $107&255;
         $109 = ($108|0)==(2);
         if ($109) {
          $110 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
          $111 = ($110&65535) >>> 6;
          $112 = $111 & 1;
          $113 = $112&1;
          if ($113) {
           label = 19;
           break;
          }
         }
         $114 = $0;
         $115 = (($114) + 29|0);
         $116 = HEAP8[$115>>0]|0;
         $117 = $116&255;
         $118 = ($117|0)==(6);
         if ($118) {
          $119 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
          $120 = ($119&65535) >>> 6;
          $121 = $120 & 1;
          $122 = $121&1;
          if ($122) {
           label = 19;
          }
         }
        }
       } while(0);
       if ((label|0) == 19) {
        $123 = HEAP8[29136>>0]|0;
        $124 = $123&255;
        $125 = ($124|0)>(0);
        if ($125) {
         $126 = HEAP8[29136>>0]|0;
         $127 = (($126) + -1)<<24>>24;
         HEAP8[29136>>0] = $127;
         $128 = HEAP8[29136>>0]|0;
         $129 = $128&255;
         $130 = $129 & 4;
         $131 = ($130|0)!=(0);
         if ($131) {
          $colorMask = -83;
         }
        }
       }
      }
     } while(0);
     if ((label|0) == 9) {
      $83 = HEAP8[29144>>0]|0;
      $84 = (($83) + 1)<<24>>24;
      HEAP8[29144>>0] = $84;
      $85 = HEAP8[29104>>0]|0;
      $86 = $85&255;
      $87 = ($86|0)>(0);
      if ($87) {
       label = 11;
      } else {
       $88 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
       $89 = ($88&65535) >>> 6;
       $90 = $89 & 1;
       $91 = $90&1;
       if ($91) {
        label = 11;
       } else {
        $typeOffset = 10;
       }
      }
      if ((label|0) == 11) {
       $92 = HEAP8[35920>>0]|0;
       $93 = $92&255;
       $94 = $93 >> 3;
       $95 = $94 & 1;
       $96 = (1 + ($95))|0;
       $97 = ($96*10)|0;
       $98 = (10 + ($97))|0;
       $99 = $98&255;
       $typeOffset = $99;
      }
     }
     $132 = (_Sprites_GetIndex()|0);
     $spriteIndex = $132;
     $133 = $spriteIndex;
     $134 = $133&255;
     $135 = ($134|0)<(18);
     if ($135) {
      $136 = $angleIndex;
      $137 = $136&255;
      $138 = (28976 + ($137<<3)|0);
      $139 = (($138) + 2|0);
      $140 = HEAP8[$139>>0]|0;
      $141 = $140&255;
      $142 = $typeOffset;
      $143 = $142&255;
      $144 = (($141) + ($143))|0;
      $145 = $144&255;
      $146 = $spriteIndex;
      $147 = $146&255;
      $148 = (2304 + (($147*5)|0)|0);
      $149 = (($148) + 2|0);
      HEAP8[$149>>0] = $145;
      $150 = $angleIndex;
      $151 = $150&255;
      $152 = (28976 + ($151<<3)|0);
      $153 = (($152) + 3|0);
      $154 = HEAP8[$153>>0]|0;
      $155 = $spriteIndex;
      $156 = $155&255;
      $157 = (2304 + (($156*5)|0)|0);
      $158 = (($157) + 3|0);
      HEAP8[$158>>0] = $154;
      $159 = $screenX;
      $160 = $159&255;
      $161 = $angleIndex;
      $162 = $161&255;
      $163 = (28976 + ($162<<3)|0);
      $164 = HEAP8[$163>>0]|0;
      $165 = $164 << 24 >> 24;
      $166 = (($160) + ($165))|0;
      $167 = $166&255;
      $168 = $spriteIndex;
      $169 = $168&255;
      $170 = (2304 + (($169*5)|0)|0);
      HEAP8[$170>>0] = $167;
      $171 = $screenY;
      $172 = $171&255;
      $173 = $angleIndex;
      $174 = $173&255;
      $175 = (28976 + ($174<<3)|0);
      $176 = (($175) + 1|0);
      $177 = HEAP8[$176>>0]|0;
      $178 = $177 << 24 >> 24;
      $179 = (($172) + ($178))|0;
      $180 = $179&255;
      $181 = $spriteIndex;
      $182 = $181&255;
      $183 = (2304 + (($182*5)|0)|0);
      $184 = (($183) + 1|0);
      HEAP8[$184>>0] = $180;
      $185 = $colorMask;
      $186 = $spriteIndex;
      $187 = $186&255;
      $188 = (2304 + (($187*5)|0)|0);
      $189 = (($188) + 4|0);
      HEAP8[$189>>0] = $185;
     }
     $190 = (_Sprites_GetIndex()|0);
     $spriteIndex = $190;
     $191 = $spriteIndex;
     $192 = $191&255;
     $193 = ($192|0)<(18);
     if (!($193)) {
      STACKTOP = sp;return;
     }
     $194 = $angleIndex;
     $195 = $194&255;
     $196 = (28976 + ($195<<3)|0);
     $197 = (($196) + 4|0);
     $198 = (($197) + 2|0);
     $199 = HEAP8[$198>>0]|0;
     $200 = $199&255;
     $201 = $typeOffset;
     $202 = $201&255;
     $203 = (($200) + ($202))|0;
     $204 = $203&255;
     $205 = $spriteIndex;
     $206 = $205&255;
     $207 = (2304 + (($206*5)|0)|0);
     $208 = (($207) + 2|0);
     HEAP8[$208>>0] = $204;
     $209 = $angleIndex;
     $210 = $209&255;
     $211 = (28976 + ($210<<3)|0);
     $212 = (($211) + 4|0);
     $213 = (($212) + 3|0);
     $214 = HEAP8[$213>>0]|0;
     $215 = $spriteIndex;
     $216 = $215&255;
     $217 = (2304 + (($216*5)|0)|0);
     $218 = (($217) + 3|0);
     HEAP8[$218>>0] = $214;
     $219 = $screenX;
     $220 = $219&255;
     $221 = $angleIndex;
     $222 = $221&255;
     $223 = (28976 + ($222<<3)|0);
     $224 = (($223) + 4|0);
     $225 = HEAP8[$224>>0]|0;
     $226 = $225 << 24 >> 24;
     $227 = (($220) + ($226))|0;
     $228 = $227&255;
     $229 = $spriteIndex;
     $230 = $229&255;
     $231 = (2304 + (($230*5)|0)|0);
     HEAP8[$231>>0] = $228;
     $232 = $screenY;
     $233 = $232&255;
     $234 = $angleIndex;
     $235 = $234&255;
     $236 = (28976 + ($235<<3)|0);
     $237 = (($236) + 4|0);
     $238 = (($237) + 1|0);
     $239 = HEAP8[$238>>0]|0;
     $240 = $239 << 24 >> 24;
     $241 = (($233) + ($240))|0;
     $242 = $241&255;
     $243 = $spriteIndex;
     $244 = $243&255;
     $245 = (2304 + (($244*5)|0)|0);
     $246 = (($245) + 1|0);
     HEAP8[$246>>0] = $242;
     $247 = $colorMask;
     $248 = $spriteIndex;
     $249 = $248&255;
     $250 = (2304 + (($249*5)|0)|0);
     $251 = (($250) + 4|0);
     HEAP8[$251>>0] = $247;
     STACKTOP = sp;return;
    }
   }
  }
 }
 $35 = $0;
 $36 = (($35) + 27|0);
 $37 = (($36) + 1|0);
 $38 = HEAP8[$37>>0]|0;
 $39 = $38 & -33;
 HEAP8[$37>>0] = $39;
 STACKTOP = sp;return;
}
function _Track_GetLength() {
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP8[29352>>0]|0;
 return ($0|0);
}
function _Track_Set($trackNumber) {
 $trackNumber = $trackNumber|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $cond = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $trackNumber;
 $1 = $0;
 HEAP8[29360>>0] = $1;
 $2 = $0;
 $3 = $2&255;
 $cond = ($3|0)==(0);
 if ($cond) {
  HEAP32[29368>>2] = 29320;
  HEAP8[29352>>0] = 32;
  STACKTOP = sp;return;
 } else {
  STACKTOP = sp;return;
 }
}
function _Track_GetCoordFromIndex($index) {
 $index = $index|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $index;
 $1 = $0;
 $2 = $1&255;
 $3 = HEAP32[29368>>2]|0;
 $4 = (($3) + ($2)|0);
 $5 = HEAP8[$4>>0]|0;
 STACKTOP = sp;return ($5|0);
}
function _Track_GetNewIndex($index,$coord) {
 $index = $index|0;
 $coord = $coord|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $n = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $index;
 $2 = $coord;
 $3 = $1;
 $4 = $3&255;
 $5 = (($4) + 1)|0;
 $n = $5;
 while(1) {
  $6 = $n;
  $7 = $1;
  $8 = $7&255;
  $9 = ($6|0)!=($8|0);
  if (!($9)) {
   label = 9;
   break;
  }
  $10 = $n;
  $11 = HEAP8[29352>>0]|0;
  $12 = $11&255;
  $13 = ($10|0)>=($12|0);
  if ($13) {
   $n = 0;
  }
  $14 = $n;
  $15 = HEAP32[29368>>2]|0;
  $16 = (($15) + ($14)|0);
  $17 = HEAP8[$16>>0]|0;
  $18 = $17&255;
  $19 = $2;
  $20 = $19&255;
  $21 = ($18|0)==($20|0);
  if ($21) {
   label = 6;
   break;
  }
  $24 = $n;
  $25 = (($24) + 1)|0;
  $n = $25;
 }
 if ((label|0) == 6) {
  $22 = $n;
  $23 = $22&255;
  $0 = $23;
  $27 = $0;
  STACKTOP = sp;return ($27|0);
 }
 else if ((label|0) == 9) {
  $26 = $1;
  $0 = $26;
  $27 = $0;
  STACKTOP = sp;return ($27|0);
 }
 return (0)|0;
}
function _Track_GetNextIndex($currentIndex) {
 $currentIndex = $currentIndex|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $currentIndex;
 $2 = $1;
 $3 = $2&255;
 $4 = HEAP8[29352>>0]|0;
 $5 = $4&255;
 $6 = (($5) - 1)|0;
 $7 = ($3|0)==($6|0);
 if ($7) {
  $0 = 0;
  $12 = $0;
  STACKTOP = sp;return ($12|0);
 } else {
  $8 = $1;
  $9 = $8&255;
  $10 = (($9) + 1)|0;
  $11 = $10&255;
  $0 = $11;
  $12 = $0;
  STACKTOP = sp;return ($12|0);
 }
 return (0)|0;
}
function _Track_GetDirection($index) {
 $index = $index|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $5 = 0, $6 = 0;
 var $7 = 0, $8 = 0, $9 = 0, $current = 0, $next = 0, $nextIndex = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $index;
 $2 = $1;
 $3 = $2&255;
 $4 = (($3) + 1)|0;
 $nextIndex = $4;
 $5 = $nextIndex;
 $6 = HEAP8[29352>>0]|0;
 $7 = $6&255;
 $8 = ($5|0)>=($7|0);
 if ($8) {
  $nextIndex = 0;
 }
 $9 = $1;
 $10 = $9&255;
 $11 = HEAP32[29368>>2]|0;
 $12 = (($11) + ($10)|0);
 $13 = HEAP8[$12>>0]|0;
 $current = $13;
 $14 = $nextIndex;
 $15 = HEAP32[29368>>2]|0;
 $16 = (($15) + ($14)|0);
 $17 = HEAP8[$16>>0]|0;
 $next = $17;
 $18 = $current;
 $19 = $18&255;
 $20 = $next;
 $21 = $20&255;
 $22 = (($21) + 1)|0;
 $23 = ($19|0)==($22|0);
 if ($23) {
  $0 = 3;
  $42 = $0;
  STACKTOP = sp;return ($42|0);
 }
 $24 = $current;
 $25 = $24&255;
 $26 = $next;
 $27 = $26&255;
 $28 = (($27) - 1)|0;
 $29 = ($25|0)==($28|0);
 if ($29) {
  $0 = 1;
  $42 = $0;
  STACKTOP = sp;return ($42|0);
 }
 $30 = $current;
 $31 = $30&255;
 $32 = $next;
 $33 = $32&255;
 $34 = (($33) + 16)|0;
 $35 = ($31|0)==($34|0);
 if ($35) {
  $0 = 2;
  $42 = $0;
  STACKTOP = sp;return ($42|0);
 }
 $36 = $current;
 $37 = $36&255;
 $38 = $next;
 $39 = $38&255;
 $40 = (($39) - 16)|0;
 $41 = ($37|0)==($40|0);
 if ($41) {
  $0 = 0;
  $42 = $0;
  STACKTOP = sp;return ($42|0);
 } else {
  $0 = -1;
  $42 = $0;
  STACKTOP = sp;return ($42|0);
 }
 return (0)|0;
}
function _MenuFn_StartMission($arg) {
 $arg = $arg|0;
 var $0 = 0, $1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $arg;
 $1 = $0;
 _Mission_Set($1);
 _Game_SetState(2);
 STACKTOP = sp;return;
}
function _MenuFn_NavigateTo($menu) {
 $menu = $menu|0;
 var $0 = 0, $1 = 0, $2 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $menu;
 $1 = $0;
 HEAP32[29752>>2] = $1;
 HEAP8[29744>>0] = 0;
 $2 = HEAP32[29752>>2]|0;
 _Menu_Display($2);
 STACKTOP = sp;return;
}
function _MenuFn_Continue($arg) {
 $arg = $arg|0;
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $arg;
 _Game_SetState(2);
 STACKTOP = sp;return;
}
function _MenuFn_RestartMission($arg) {
 $arg = $arg|0;
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $arg;
 _Game_SetState(0);
 _Game_SetState(2);
 STACKTOP = sp;return;
}
function _MenuFn_ExitMission($arg) {
 $arg = $arg|0;
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $arg;
 _Game_SetState(1);
 STACKTOP = sp;return;
}
function _Menu_Display($menu) {
 $menu = $menu|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $ptr = 0, $y = 0, label = 0;
 var sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $menu;
 $1 = $0;
 $ptr = $1;
 $y = 17;
 $2 = (_Game_GetState()|0);
 $3 = $2&255;
 $4 = ($3|0)!=(3);
 if ($4) {
  _Fill(0,0,28,29,25);
  $5 = (_World_GetLogoLayout()|0);
  _DrawMap2(6,4,$5);
  _Print(10,11,29760);
 } else {
  $y = 20;
 }
 while(1) {
  $6 = $y;
  $7 = (($6) + 1)<<24>>24;
  $y = $7;
  $8 = $6&255;
  $9 = $ptr;
  $10 = HEAP32[$9>>2]|0;
  $11 = $10;
  _Print(10,$8,$11);
  $12 = $ptr;
  $13 = (($12) + 12|0);
  $ptr = $13;
  $14 = $ptr;
  $15 = HEAP32[$14>>2]|0;
  $16 = ($15|0)==((((65535))&65535)|0);
  if ($16) {
   break;
  }
 }
 STACKTOP = sp;return;
}
function _Menu_Init() {
 var $0 = 0, $1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 HEAP8[((2288 + 10|0))>>0] = 32;
 HEAP8[2288>>0] = 0;
 HEAP8[((2288 + 8|0))>>0] = 0;
 HEAP8[((2288 + 9|0))>>0] = 0;
 HEAP8[29744>>0] = 0;
 HEAP32[29752>>2] = 29664;
 $0 = (_World_GetLayoutTiles()|0);
 _SetTileTable($0);
 _Fill(0,0,28,29,25);
 _SetFontTilesIndex(25);
 $1 = HEAP32[29752>>2]|0;
 _Menu_Display($1);
 return;
}
function _PauseMenu_Init() {
 var $0 = 0, $1 = 0, $2 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 HEAP8[((2288 + 10|0))>>0] = 32;
 HEAP8[2288>>0] = 0;
 HEAP8[((2288 + 8|0))>>0] = 0;
 HEAP8[((2288 + 9|0))>>0] = 0;
 HEAP8[29744>>0] = 0;
 $0 = (_World_GetLayoutTiles()|0);
 _SetTileTable($0);
 _Fill(0,0,28,29,25);
 $1 = (_World_GetLayout()|0);
 _DrawMap2(6,2,$1);
 _SetFontTilesIndex(25);
 HEAP32[29752>>2] = 29704;
 $2 = HEAP32[29752>>2]|0;
 _Menu_Display($2);
 return;
}
function _PauseMenu_Update() {
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0;
 var $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0;
 var $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0;
 var $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0;
 var $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $spriteIndex = 0, $spriteIndex1 = 0, $spriteIndex2 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = HEAP8[29120>>0]|0;
 $1 = $0&255;
 $2 = ($1|0)<(5);
 if ($2) {
  $3 = (_Sprites_GetIndex()|0);
  $spriteIndex = $3;
  $4 = $spriteIndex;
  $5 = $4&255;
  $6 = ($5|0)<(18);
  if ($6) {
   $7 = HEAP8[35920>>0]|0;
   $8 = $7&255;
   $9 = $8 & 8;
   $10 = ($9|0)!=(0);
   $11 = $10 ? 51 : 52;
   $12 = $11&255;
   $13 = $spriteIndex;
   $14 = $13&255;
   $15 = (2304 + (($14*5)|0)|0);
   $16 = (($15) + 2|0);
   HEAP8[$16>>0] = $12;
   $17 = $spriteIndex;
   $18 = $17&255;
   $19 = (2304 + (($18*5)|0)|0);
   $20 = (($19) + 4|0);
   HEAP8[$20>>0] = -1;
   $21 = $spriteIndex;
   $22 = $21&255;
   $23 = (2304 + (($22*5)|0)|0);
   $24 = (($23) + 3|0);
   HEAP8[$24>>0] = 0;
   $25 = HEAP8[29120>>0]|0;
   $26 = $25&255;
   $27 = (29160 + (($26*30)|0)|0);
   $28 = HEAP16[$27>>1]|0;
   $29 = $28&65535;
   $30 = $29 >> 9;
   $31 = (48 + ($30))|0;
   $32 = (($31) - 4)|0;
   $33 = $32&255;
   $34 = $spriteIndex;
   $35 = $34&255;
   $36 = (2304 + (($35*5)|0)|0);
   HEAP8[$36>>0] = $33;
   $37 = HEAP8[29120>>0]|0;
   $38 = $37&255;
   $39 = (29160 + (($38*30)|0)|0);
   $40 = (($39) + 2|0);
   $41 = HEAP16[$40>>1]|0;
   $42 = $41&65535;
   $43 = $42 >> 9;
   $44 = (16 + ($43))|0;
   $45 = (($44) - 4)|0;
   $46 = $45&255;
   $47 = $spriteIndex;
   $48 = $47&255;
   $49 = (2304 + (($48*5)|0)|0);
   $50 = (($49) + 1|0);
   HEAP8[$50>>0] = $46;
  }
 }
 $51 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $52 = ($51&65535) >>> 3;
 $53 = $52 & 1;
 $54 = $53&1;
 if ($54) {
  $55 = (_Sprites_GetIndex()|0);
  $spriteIndex1 = $55;
  $56 = $spriteIndex1;
  $57 = $56&255;
  $58 = ($57|0)<(18);
  if ($58) {
   $59 = $spriteIndex1;
   $60 = $59&255;
   $61 = (2304 + (($60*5)|0)|0);
   $62 = (($61) + 2|0);
   HEAP8[$62>>0] = 50;
   $63 = $spriteIndex1;
   $64 = $63&255;
   $65 = (2304 + (($64*5)|0)|0);
   $66 = (($65) + 4|0);
   HEAP8[$66>>0] = -1;
   $67 = $spriteIndex1;
   $68 = $67&255;
   $69 = (2304 + (($68*5)|0)|0);
   $70 = (($69) + 3|0);
   HEAP8[$70>>0] = 0;
   $71 = HEAP8[((36904 + 12|0))>>0]|0;
   $72 = $71&255;
   $73 = $72 >> 1;
   $74 = (48 + ($73))|0;
   $75 = (($74) - 4)|0;
   $76 = $75&255;
   $77 = $spriteIndex1;
   $78 = $77&255;
   $79 = (2304 + (($78*5)|0)|0);
   HEAP8[$79>>0] = $76;
   $80 = HEAP8[((36904 + 13|0))>>0]|0;
   $81 = $80&255;
   $82 = $81 >> 1;
   $83 = (16 + ($82))|0;
   $84 = (($83) - 7)|0;
   $85 = HEAP8[35920>>0]|0;
   $86 = $85&255;
   $87 = $86 >> 3;
   $88 = $87 & 1;
   $89 = (($84) + ($88))|0;
   $90 = $89&255;
   $91 = $spriteIndex1;
   $92 = $91&255;
   $93 = (2304 + (($92*5)|0)|0);
   $94 = (($93) + 1|0);
   HEAP8[$94>>0] = $90;
  }
  STACKTOP = sp;return;
 }
 $95 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $96 = ($95&65535) >>> 2;
 $97 = $96 & 1;
 $98 = $97&1;
 if ($98) {
  $99 = HEAP8[((36904 + 14|0))>>0]|0;
  $100 = $99 & 15;
  $101 = $100&255;
  $102 = ($101|0)<(5);
  if ($102) {
   $103 = (_Sprites_GetIndex()|0);
   $spriteIndex2 = $103;
   $104 = $spriteIndex2;
   $105 = $104&255;
   $106 = ($105|0)<(18);
   if ($106) {
    $107 = $spriteIndex2;
    $108 = $107&255;
    $109 = (2304 + (($108*5)|0)|0);
    $110 = (($109) + 2|0);
    HEAP8[$110>>0] = 50;
    $111 = $spriteIndex2;
    $112 = $111&255;
    $113 = (2304 + (($112*5)|0)|0);
    $114 = (($113) + 4|0);
    HEAP8[$114>>0] = -1;
    $115 = $spriteIndex2;
    $116 = $115&255;
    $117 = (2304 + (($116*5)|0)|0);
    $118 = (($117) + 3|0);
    HEAP8[$118>>0] = 0;
    $119 = HEAP8[((36904 + 14|0))>>0]|0;
    $120 = $119 & 15;
    $121 = $120&255;
    $122 = (29160 + (($121*30)|0)|0);
    $123 = HEAP16[$122>>1]|0;
    $124 = $123&65535;
    $125 = $124 >> 9;
    $126 = (48 + ($125))|0;
    $127 = (($126) - 4)|0;
    $128 = $127&255;
    $129 = $spriteIndex2;
    $130 = $129&255;
    $131 = (2304 + (($130*5)|0)|0);
    HEAP8[$131>>0] = $128;
    $132 = HEAP8[((36904 + 14|0))>>0]|0;
    $133 = $132 & 15;
    $134 = $133&255;
    $135 = (29160 + (($134*30)|0)|0);
    $136 = (($135) + 2|0);
    $137 = HEAP16[$136>>1]|0;
    $138 = $137&65535;
    $139 = $138 >> 9;
    $140 = (16 + ($139))|0;
    $141 = (($140) - 7)|0;
    $142 = HEAP8[35920>>0]|0;
    $143 = $142&255;
    $144 = $143 >> 3;
    $145 = $144 & 1;
    $146 = (($141) + ($145))|0;
    $147 = $146&255;
    $148 = $spriteIndex2;
    $149 = $148&255;
    $150 = (2304 + (($149*5)|0)|0);
    $151 = (($150) + 1|0);
    HEAP8[$151>>0] = $147;
   }
  }
 }
 STACKTOP = sp;return;
}
function _Menu_GetNumEntries($menu) {
 $menu = $menu|0;
 var $0 = 0, $1 = 0, $10 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $count = 0, $ptr = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $menu;
 $1 = $0;
 $ptr = $1;
 $count = 0;
 while(1) {
  $2 = $ptr;
  $3 = HEAP16[$2>>1]|0;
  $4 = $3&65535;
  $5 = ($4|0)!=((((65535))&65535)|0);
  if (!($5)) {
   break;
  }
  $6 = $count;
  $7 = (($6) + 1)<<24>>24;
  $count = $7;
  $8 = $ptr;
  $9 = (($8) + 12|0);
  $ptr = $9;
 }
 $10 = $count;
 STACKTOP = sp;return ($10|0);
}
function _Menu_UpdateSelection($delta) {
 $delta = $delta|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $y = 0;
 var label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $delta;
 $1 = (_Game_GetState()|0);
 $2 = $1&255;
 $3 = ($2|0)==(3);
 $4 = $3 ? 20 : 17;
 $5 = $4&255;
 $y = $5;
 $6 = $0;
 $7 = $6 << 24 >> 24;
 $8 = ($7|0)!=(0);
 if (!($8)) {
  $34 = $y;
  $35 = $34&255;
  $36 = HEAP8[29744>>0]|0;
  $37 = $36 << 24 >> 24;
  $38 = (($35) + ($37))|0;
  _PrintChar(8,$38,62);
  STACKTOP = sp;return;
 }
 $9 = $y;
 $10 = $9&255;
 $11 = HEAP8[29744>>0]|0;
 $12 = $11 << 24 >> 24;
 $13 = (($10) + ($12))|0;
 _PrintChar(8,$13,32);
 $14 = $0;
 $15 = $14 << 24 >> 24;
 $16 = HEAP8[29744>>0]|0;
 $17 = $16 << 24 >> 24;
 $18 = (($17) + ($15))|0;
 $19 = $18&255;
 HEAP8[29744>>0] = $19;
 $20 = HEAP8[29744>>0]|0;
 $21 = $20 << 24 >> 24;
 $22 = ($21|0)<(0);
 if ($22) {
  $23 = HEAP32[29752>>2]|0;
  $24 = (_Menu_GetNumEntries($23)|0);
  $25 = $24 << 24 >> 24;
  $26 = (($25) - 1)|0;
  $27 = $26&255;
  HEAP8[29744>>0] = $27;
 } else {
  $28 = HEAP8[29744>>0]|0;
  $29 = $28 << 24 >> 24;
  $30 = HEAP32[29752>>2]|0;
  $31 = (_Menu_GetNumEntries($30)|0);
  $32 = $31 << 24 >> 24;
  $33 = ($29|0)>=($32|0);
  if ($33) {
   HEAP8[29744>>0] = 0;
  }
 }
 _TriggerFx(35,-1,1);
 $34 = $y;
 $35 = $34&255;
 $36 = HEAP8[29744>>0]|0;
 $37 = $36 << 24 >> 24;
 $38 = (($35) + ($37))|0;
 _PrintChar(8,$38,62);
 STACKTOP = sp;return;
}
function _Menu_ExecuteSelection($menu,$selection) {
 $menu = $menu|0;
 $selection = $selection|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 var $arg = 0, $fn = 0, $ptr = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $menu;
 $1 = $selection;
 _TriggerFx(36,-1,1);
 $2 = $0;
 $ptr = $2;
 $3 = $1;
 $4 = $3 << 24 >> 24;
 $5 = ($4*3)|0;
 $6 = $ptr;
 $7 = (($6) + ($5<<2)|0);
 $ptr = $7;
 $8 = $ptr;
 $9 = (($8) + 4|0);
 $10 = HEAP32[$9>>2]|0;
 $11 = $10;
 $fn = $11;
 $12 = $ptr;
 $13 = (($12) + 8|0);
 $14 = HEAP32[$13>>2]|0;
 $15 = $14;
 $arg = $15;
 $16 = $fn;
 $17 = ($16|0)!=(0|0);
 if (!($17)) {
  STACKTOP = sp;return;
 }
 $18 = $fn;
 $19 = $arg;
 FUNCTION_TABLE_vi[$18 & 7]($19);
 STACKTOP = sp;return;
}
function _Menu_Update() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $delta = 0, $joypad = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = (_ReadJoypad(0)|0);
 $1 = $0&65535;
 $joypad = $1;
 $2 = $joypad;
 $3 = $2 << 16 >> 16;
 $4 = HEAP16[29776>>1]|0;
 $5 = $4 << 16 >> 16;
 $6 = ($3|0)==($5|0);
 if ($6) {
  _Menu_UpdateSelection(0);
  STACKTOP = sp;return;
 }
 $7 = $joypad;
 HEAP16[29776>>1] = $7;
 $delta = 0;
 $8 = $joypad;
 $9 = $8 << 16 >> 16;
 $10 = $9 & 16;
 $11 = ($10|0)!=(0);
 if ($11) {
  $12 = $delta;
  $13 = (($12) + -1)<<24>>24;
  $delta = $13;
 }
 $14 = $joypad;
 $15 = $14 << 16 >> 16;
 $16 = $15 & 32;
 $17 = ($16|0)!=(0);
 if ($17) {
  $18 = $delta;
  $19 = (($18) + 1)<<24>>24;
  $delta = $19;
 }
 $20 = $delta;
 _Menu_UpdateSelection($20);
 $21 = $joypad;
 $22 = $21 << 16 >> 16;
 $23 = $22 & 264;
 $24 = ($23|0)!=(0);
 if ($24) {
  $25 = HEAP32[29752>>2]|0;
  $26 = HEAP8[29744>>0]|0;
  _Menu_ExecuteSelection($25,$26);
 }
 $27 = $joypad;
 $28 = $27 << 16 >> 16;
 $29 = $28 & 1;
 $30 = ($29|0)!=(0);
 if (!($30)) {
  STACKTOP = sp;return;
 }
 $31 = HEAP32[29752>>2]|0;
 $32 = ($31|0)!=(29664|0);
 if (!($32)) {
  STACKTOP = sp;return;
 }
 $33 = (_Game_GetState()|0);
 $34 = $33&255;
 $35 = ($34|0)==(1);
 if (!($35)) {
  STACKTOP = sp;return;
 }
 _TriggerFx(36,-1,1);
 HEAP32[29752>>2] = 29664;
 _Menu_Display(29664);
 STACKTOP = sp;return;
}
function _VsyncCallBack() {
 var $0 = 0, $1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP8[31992>>0]|0;
 $1 = (($0) + 1)<<24>>24;
 HEAP8[31992>>0] = $1;
 return;
}
function _Game_Vsync() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 _WaitVsync(1);
 HEAP8[31992>>0] = 0;
 return;
}
function _Game_Init() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 _SetUserPreVsyncCallback(7);
 _InitMusicPlayer(31168);
 _SetMasterVolume(64);
 _ClearVram();
 _Track_Set(0);
 _Game_SetState(1);
 _Mission_Set(36328);
 _Game_SetState(2);
 return;
}
function _Game_SetState($newState) {
 $newState = $newState|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $oldState = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $newState;
 $1 = HEAP8[32000>>0]|0;
 $2 = $1 << 24 >> 24;
 $3 = ($2|0)!=(0);
 if ($3) {
  _FadeOut(1,1);
 }
 _Sprites_Reset();
 $4 = HEAP8[32000>>0]|0;
 $oldState = $4;
 $5 = $0;
 HEAP8[32000>>0] = $5;
 $6 = $0;
 $7 = $6&255;
 if ((($7|0) == 3)) {
  _PauseMenu_Init();
 } else if ((($7|0) == 1)) {
  _Menu_Init();
 } else if ((($7|0) == 2)) {
  $8 = $oldState;
  $9 = $8&255;
  $10 = ($9|0)!=(3);
  if ($10) {
   _Sprites_Init();
   _Car_Init();
   _Ped_Init();
   _Mission_Init();
   _UI_Init();
   _Camera_Update();
  }
  _World_Init();
  _Car_SetSprites();
  _Ped_SetSprites();
 } else {
 }
 $11 = $oldState;
 $12 = $11&255;
 $13 = ($12|0)!=(0);
 if (!($13)) {
  STACKTOP = sp;return;
 }
 _FadeIn(1,1);
 STACKTOP = sp;return;
}
function _Game_Update() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 _Game_Vsync();
 _Sprites_Reset();
 $0 = HEAP8[32000>>0]|0;
 $1 = $0 << 24 >> 24;
 if ((($1|0) == 2)) {
  _World_UpdateTiles();
  _Ped_SetSprites();
  _Car_SetSprites();
  _Mission_SetSprites();
  _Mission_Update();
  _Ped_Update();
  _Car_Update();
  _UI_Update();
  $2 = (_ReadJoypad(0)|0);
  $3 = $2 & 8;
  $4 = ($3|0)!=(0);
  if ($4) {
   $5 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
   $6 = ($5&65535) >>> 7;
   $7 = $6 & 1;
   $8 = $7&1;
   if (!($8)) {
    _TriggerFx(37,-1,1);
    _Game_SetState(3);
   }
  }
  return;
 } else if ((($1|0) == 3)) {
  _PauseMenu_Update();
  _Menu_Update();
  return;
 } else if ((($1|0) == 1)) {
  _Menu_Update();
  return;
 } else {
  return;
 }
}
function _Game_Reset() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 _Game_SetState(2);
 return;
}
function _Game_GetState() {
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP8[32000>>0]|0;
 return ($0|0);
}
function _Math_Cos($angle) {
 $angle = $angle|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $angle;
 $1 = $0;
 $2 = $1&255;
 $3 = (($2) + 64)|0;
 $4 = $3 & 255;
 $5 = (32008 + ($4)|0);
 $6 = HEAP8[$5>>0]|0;
 STACKTOP = sp;return ($6|0);
}
function _Math_Sin($angle) {
 $angle = $angle|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $angle;
 $1 = $0;
 $2 = $1&255;
 $3 = (32008 + ($2)|0);
 $4 = HEAP8[$3>>0]|0;
 STACKTOP = sp;return ($4|0);
}
function _Math_ATan($x) {
 $x = $x|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $a = 0, $b = 0, $c = 0, $d = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $x;
 $2 = $1;
 $3 = $2 << 16 >> 16;
 $4 = ($3|0)>=(0);
 if ($4) {
  $a = 0;
  $b = 63;
 } else {
  $a = 64;
  $b = 127;
 }
 while(1) {
  $5 = $a;
  $6 = $5&255;
  $7 = $b;
  $8 = $7&255;
  $9 = (($6) + ($8))|0;
  $10 = $9 >> 1;
  $11 = $10&255;
  $c = $11;
  $12 = $1;
  $13 = $12 << 16 >> 16;
  $14 = $c;
  $15 = $14&255;
  $16 = (32264 + ($15<<1)|0);
  $17 = HEAP16[$16>>1]|0;
  $18 = $17 << 16 >> 16;
  $19 = (($13) - ($18))|0;
  $20 = $19&65535;
  $d = $20;
  $21 = $d;
  $22 = $21 << 16 >> 16;
  $23 = ($22|0)>(0);
  if ($23) {
   $24 = $c;
   $25 = $24&255;
   $26 = (($25) + 1)|0;
   $27 = $26&255;
   $a = $27;
  } else {
   $28 = $d;
   $29 = $28 << 16 >> 16;
   $30 = ($29|0)<(0);
   if ($30) {
    $31 = $c;
    $32 = $31&255;
    $33 = (($32) - 1)|0;
    $34 = $33&255;
    $b = $34;
   }
  }
  $35 = $a;
  $36 = $35&255;
  $37 = $b;
  $38 = $37&255;
  $39 = ($36|0)<=($38|0);
  if ($39) {
   $40 = $d;
   $41 = $40 << 16 >> 16;
   $42 = ($41|0)!=(0);
   $53 = $42;
  } else {
   $53 = 0;
  }
  if (!($53)) {
   break;
  }
 }
 $43 = $1;
 $44 = $43 << 16 >> 16;
 $45 = ($44|0)>=(0);
 if ($45) {
  $46 = $c;
  $47 = $46&255;
  $0 = $47;
  $52 = $0;
  STACKTOP = sp;return ($52|0);
 } else {
  $48 = $c;
  $49 = $48&255;
  $50 = (($49) - 128)|0;
  $51 = $50&65535;
  $0 = $51;
  $52 = $0;
  STACKTOP = sp;return ($52|0);
 }
 return (0)|0;
}
function _Math_ATan2($y,$x) {
 $y = $y|0;
 $x = $x|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $r = 0;
 var label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $y;
 $2 = $x;
 $3 = $2;
 $4 = $3 << 16 >> 16;
 $5 = ($4|0)==(0);
 do {
  if ($5) {
   $6 = $1;
   $7 = $6 << 16 >> 16;
   $8 = ($7|0)==(0);
   if ($8) {
    $0 = 0;
    break;
   } else {
    $9 = $1;
    $10 = $9 << 16 >> 16;
    $11 = ($10|0)<(0);
    $12 = $11 ? -64 : 64;
    $13 = $12&65535;
    $0 = $13;
    break;
   }
  } else {
   $14 = $1;
   $15 = $14 << 16 >> 16;
   $16 = $15 << 5;
   $17 = $2;
   $18 = $17 << 16 >> 16;
   $19 = (($16|0) / ($18|0))&-1;
   $20 = $19&65535;
   $r = $20;
   $21 = $r;
   $22 = (_Math_ATan($21)|0);
   $r = $22;
   $23 = $2;
   $24 = $23 << 16 >> 16;
   $25 = ($24|0)>=(0);
   if ($25) {
    $26 = $r;
    $0 = $26;
    break;
   }
   $27 = $1;
   $28 = $27 << 16 >> 16;
   $29 = ($28|0)>=(0);
   if ($29) {
    $30 = $r;
    $31 = $30 << 16 >> 16;
    $32 = (128 + ($31))|0;
    $33 = $32&65535;
    $0 = $33;
    break;
   } else {
    $34 = $r;
    $35 = $34 << 16 >> 16;
    $36 = (($35) - 128)|0;
    $37 = $36&65535;
    $0 = $37;
    break;
   }
  }
 } while(0);
 $38 = $0;
 STACKTOP = sp;return ($38|0);
}
function _Math_Random() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $lsb = 0;
 var label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = HEAP16[32520>>1]|0;
 $1 = $0&65535;
 $2 = $1 & 1;
 $3 = $2&65535;
 $lsb = $3;
 $4 = HEAP16[32520>>1]|0;
 $5 = $4&65535;
 $6 = $5 >> 1;
 $7 = $6&65535;
 HEAP16[32520>>1] = $7;
 $8 = $lsb;
 $9 = $8&65535;
 $10 = ($9|0)==(1);
 if ($10) {
  $11 = HEAP16[32520>>1]|0;
  $12 = $11&65535;
  $13 = $12 ^ 46080;
  $14 = $13&65535;
  HEAP16[32520>>1] = $14;
 }
 $15 = HEAP16[32520>>1]|0;
 $16 = $15&65535;
 $17 = (($16) - 1)|0;
 $18 = $17&65535;
 STACKTOP = sp;return ($18|0);
}
function _Math_RandomColorMask() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $mask = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $mask = 0;
 while(1) {
  $0 = $mask;
  $1 = $0&255;
  $2 = ($1|0)==(0);
  if ($2) {
   $11 = 1;
  } else {
   $3 = $mask;
   $4 = $3&255;
   $5 = ($4|0)==(254);
   $11 = $5;
  }
  if (!($11)) {
   break;
  }
  $6 = (_Math_Random()|0);
  $7 = $6&65535;
  $8 = $7 & 255;
  $9 = $8&255;
  $mask = $9;
 }
 $10 = $mask;
 STACKTOP = sp;return ($10|0);
}
function _Math_GetForwardVector($angle,$x,$y) {
 $angle = $angle|0;
 $x = $x|0;
 $y = $y|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $angle;
 $1 = $x;
 $2 = $y;
 $3 = $0;
 $4 = (_Math_Cos($3)|0);
 $5 = $1;
 HEAP8[$5>>0] = $4;
 $6 = $0;
 $7 = (_Math_Sin($6)|0);
 $8 = $2;
 HEAP8[$8>>0] = $7;
 STACKTOP = sp;return;
}
function _Sprites_Init() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 _SetSpritesTileTable(32528);
 HEAP8[35920>>0] = 0;
 return;
}
function _Sprites_Reset() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $n = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $n = 0;
 while(1) {
  $0 = $n;
  $1 = ($0|0)<(18);
  if (!($1)) {
   break;
  }
  $2 = $n;
  $3 = (2304 + (($2*5)|0)|0);
  HEAP8[$3>>0] = -32;
  $4 = $n;
  $5 = (($4) + 1)|0;
  $n = $5;
 }
 HEAP8[35928>>0] = 0;
 HEAP8[35936>>0] = 0;
 $6 = HEAP8[35920>>0]|0;
 $7 = (($6) + 1)<<24>>24;
 HEAP8[35920>>0] = $7;
 STACKTOP = sp;return;
}
function _Sprites_GetIndex() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 var $result = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = HEAP8[35936>>0]|0;
 $result = $1;
 $2 = HEAP8[35944>>0]|0;
 $3 = (($2) + 1)<<24>>24;
 HEAP8[35944>>0] = $3;
 $4 = HEAP8[35936>>0]|0;
 $5 = (($4) + 1)<<24>>24;
 HEAP8[35936>>0] = $5;
 $6 = HEAP8[35936>>0]|0;
 $7 = $6&255;
 $8 = ($7|0)==(18);
 if ($8) {
  HEAP8[35936>>0] = 0;
 }
 $9 = $result;
 $10 = $9&255;
 $11 = ($10|0)>=(18);
 if (!($11)) {
  $12 = $result;
  $13 = $12&255;
  $14 = (2304 + (($13*5)|0)|0);
  $15 = HEAP8[$14>>0]|0;
  $16 = $15&255;
  $17 = ($16|0)!=(224);
  if (!($17)) {
   $18 = $result;
   $0 = $18;
   $19 = $0;
   STACKTOP = sp;return ($19|0);
  }
 }
 $0 = 18;
 $19 = $0;
 STACKTOP = sp;return ($19|0);
}
function _AI_GetExitFromTo($from,$to) {
 $from = $from|0;
 $to = $to|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $from;
 $2 = $to;
 $3 = $2;
 $4 = $3&255;
 $5 = $1;
 $6 = $5&255;
 $7 = (($6) + 1)|0;
 $8 = ($4|0)==($7|0);
 do {
  if ($8) {
   $0 = 3;
  } else {
   $9 = $2;
   $10 = $9&255;
   $11 = $1;
   $12 = $11&255;
   $13 = (($12) - 1)|0;
   $14 = ($10|0)==($13|0);
   if ($14) {
    $0 = 1;
    break;
   }
   $15 = $2;
   $16 = $15&255;
   $17 = $1;
   $18 = $17&255;
   $19 = (($18) - 16)|0;
   $20 = ($16|0)==($19|0);
   if ($20) {
    $0 = 0;
    break;
   }
   $21 = $2;
   $22 = $21&255;
   $23 = $1;
   $24 = $23&255;
   $25 = (($24) + 16)|0;
   $26 = ($22|0)==($25|0);
   if ($26) {
    $0 = 2;
    break;
   }
   $0 = -1;
  }
 } while(0);
 $27 = $0;
 STACKTOP = sp;return ($27|0);
}
function _AI_GetPathScore($start,$exit,$target) {
 $start = $start|0;
 $exit = $exit|0;
 $target = $target|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0;
 var $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0;
 var $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0;
 var $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0;
 var $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0;
 var $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $current = 0, $exits = 0, $targetX = 0, $targetY = 0, $x = 0, $y = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $start;
 $2 = $exit;
 $3 = $target;
 $4 = $1;
 $current = $4;
 $5 = $3;
 $6 = $5&255;
 $7 = $6 >> 4;
 $8 = $7&255;
 $targetX = $8;
 $9 = $3;
 $10 = $9&255;
 $11 = $10 & 15;
 $12 = $11&255;
 $targetY = $12;
 $13 = $1;
 $14 = $13&255;
 $15 = $14 >> 4;
 $16 = $15&255;
 $x = $16;
 $17 = $1;
 $18 = $17&255;
 $19 = $18 & 15;
 $20 = $19&255;
 $y = $20;
 while(1) {
  $21 = $2;
  $22 = $21&255;
  switch ($22|0) {
  case 2:  {
   $29 = $x;
   $30 = (($29) + -1)<<24>>24;
   $x = $30;
   break;
  }
  case 6: case 4: case 3:  {
   $23 = $y;
   $24 = (($23) + -1)<<24>>24;
   $y = $24;
   break;
  }
  case 0:  {
   $25 = $x;
   $26 = (($25) + 1)<<24>>24;
   $x = $26;
   break;
  }
  case 7: case 5: case 1:  {
   $27 = $y;
   $28 = (($27) + 1)<<24>>24;
   $y = $28;
   break;
  }
  default: {
  }
  }
  $31 = $x;
  $32 = $31&255;
  $33 = $targetX;
  $34 = $33&255;
  $35 = ($32|0)==($34|0);
  if ($35) {
   $36 = $y;
   $37 = $36&255;
   $38 = $targetY;
   $39 = $38&255;
   $40 = ($37|0)==($39|0);
   if ($40) {
    label = 10;
    break;
   }
  }
  $41 = $x;
  $42 = $41&255;
  $43 = $42 << 4;
  $44 = $y;
  $45 = $44&255;
  $46 = (($43) + ($45))|0;
  $47 = $1;
  $48 = $47&255;
  $49 = ($46|0)==($48|0);
  if ($49) {
   label = 12;
   break;
  }
  $50 = $current;
  $51 = $x;
  $52 = $y;
  $53 = (_AI_GetExitsFromTo($50,$51,$52)|0);
  $exits = $53;
  $54 = $exits;
  $55 = $54&255;
  $56 = ($55|0)==(0);
  if ($56) {
   label = 14;
   break;
  }
  $2 = 0;
  while(1) {
   $57 = $exits;
   $58 = ($57<<24>>24)!=(0);
   if (!($58)) {
    break;
   }
   $59 = $exits;
   $60 = $59&255;
   $61 = $60 & 1;
   $62 = ($61|0)!=(0);
   if ($62) {
    label = 18;
    break;
   }
   $100 = $2;
   $101 = (($100) + 1)<<24>>24;
   $2 = $101;
   $102 = $exits;
   $103 = $102&255;
   $104 = $103 >> 1;
   $105 = $104&255;
   $exits = $105;
  }
  if ((label|0) == 18) {
   label = 0;
   $63 = $exits;
   $64 = $63&255;
   $65 = ($64|0)!=(1);
   if ($65) {
    label = 19;
    break;
   }
  }
 }
 if ((label|0) == 10) {
  $0 = 0;
  $106 = $0;
  STACKTOP = sp;return ($106|0);
 }
 else if ((label|0) == 12) {
  $0 = -1;
  $106 = $0;
  STACKTOP = sp;return ($106|0);
 }
 else if ((label|0) == 14) {
  $0 = -1;
  $106 = $0;
  STACKTOP = sp;return ($106|0);
 }
 else if ((label|0) == 19) {
  $66 = $x;
  $67 = $66&255;
  $68 = $targetX;
  $69 = $68&255;
  $70 = ($67|0)>($69|0);
  if ($70) {
   $71 = $x;
   $72 = $71&255;
   $73 = $targetX;
   $74 = $73&255;
   $75 = (($72) - ($74))|0;
   $97 = $75;
  } else {
   $76 = $targetX;
   $77 = $76&255;
   $78 = $x;
   $79 = $78&255;
   $80 = (($77) - ($79))|0;
   $97 = $80;
  }
  $81 = $y;
  $82 = $81&255;
  $83 = $targetY;
  $84 = $83&255;
  $85 = ($82|0)>($84|0);
  if ($85) {
   $86 = $y;
   $87 = $86&255;
   $88 = $targetY;
   $89 = $88&255;
   $90 = (($87) - ($89))|0;
   $98 = $90;
  } else {
   $91 = $targetY;
   $92 = $91&255;
   $93 = $y;
   $94 = $93&255;
   $95 = (($92) - ($94))|0;
   $98 = $95;
  }
  $96 = (($97) + ($98))|0;
  $99 = $96&255;
  $0 = $99;
  $106 = $0;
  STACKTOP = sp;return ($106|0);
 }
 return (0)|0;
}
function _AI_GetExitsFromTo($from,$macroTileX,$macroTileY) {
 $from = $from|0;
 $macroTileX = $macroTileX|0;
 $macroTileY = $macroTileY|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $exits = 0, $to = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $from;
 $1 = $macroTileX;
 $2 = $macroTileY;
 $3 = $1;
 $4 = $2;
 $5 = (_World_GetAllMacroTileExits($3,$4)|0);
 $exits = $5;
 $6 = $1;
 $7 = $6&255;
 $8 = $7 << 4;
 $9 = $2;
 $10 = $9&255;
 $11 = (($8) + ($10))|0;
 $12 = $11&255;
 $to = $12;
 $13 = $to;
 $14 = $13&255;
 $15 = $0;
 $16 = $15&255;
 $17 = (($16) + 1)|0;
 $18 = ($14|0)==($17|0);
 if ($18) {
  $19 = $exits;
  $20 = $19&255;
  $21 = $20 & -89;
  $22 = $21&255;
  $exits = $22;
  $53 = $exits;
  STACKTOP = sp;return ($53|0);
 }
 $23 = $to;
 $24 = $23&255;
 $25 = $0;
 $26 = $25&255;
 $27 = (($26) - 1)|0;
 $28 = ($24|0)==($27|0);
 if ($28) {
  $29 = $exits;
  $30 = $29&255;
  $31 = $30 & -163;
  $32 = $31&255;
  $exits = $32;
 } else {
  $33 = $to;
  $34 = $33&255;
  $35 = $0;
  $36 = $35&255;
  $37 = (($36) - 16)|0;
  $38 = ($34|0)==($37|0);
  if ($38) {
   $39 = $exits;
   $40 = $39&255;
   $41 = $40 & -2;
   $42 = $41&255;
   $exits = $42;
  } else {
   $43 = $to;
   $44 = $43&255;
   $45 = $0;
   $46 = $45&255;
   $47 = (($46) + 16)|0;
   $48 = ($44|0)==($47|0);
   if ($48) {
    $49 = $exits;
    $50 = $49&255;
    $51 = $50 & -5;
    $52 = $51&255;
    $exits = $52;
   }
  }
 }
 $53 = $exits;
 STACKTOP = sp;return ($53|0);
}
function _AI_ChaseTarget($car,$target,$ram) {
 $car = $car|0;
 $target = $target|0;
 $ram = $ram|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0;
 var $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0;
 var $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0, $202 = 0, $203 = 0, $204 = 0, $205 = 0;
 var $206 = 0, $207 = 0, $208 = 0, $209 = 0, $21 = 0, $210 = 0, $211 = 0, $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0, $219 = 0, $22 = 0, $220 = 0, $221 = 0, $222 = 0, $223 = 0;
 var $224 = 0, $225 = 0, $226 = 0, $227 = 0, $228 = 0, $229 = 0, $23 = 0, $230 = 0, $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0, $25 = 0;
 var $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0;
 var $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0;
 var $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0;
 var $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0;
 var $99 = 0, $desiredAngle = 0, $desiredAngle1 = 0, $diffX = 0, $diffY = 0, $exits = 0, $lastExit = 0, $macroTileIndex = 0, $macroTileX = 0, $macroTileY = 0, $targetMacroX = 0, $targetMacroY = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = $target;
 $3 = $ram&1;
 $2 = $3;
 $4 = $1;
 $5 = (($4) + 4|0);
 $6 = HEAP16[$5>>1]|0;
 $7 = $6&65535;
 $8 = $7 >> 5;
 $9 = $8 >> 7;
 $10 = $9&255;
 $targetMacroX = $10;
 $11 = $1;
 $12 = (($11) + 4|0);
 $13 = (($12) + 2|0);
 $14 = HEAP16[$13>>1]|0;
 $15 = $14&65535;
 $16 = $15 >> 5;
 $17 = $16 >> 7;
 $18 = $17&255;
 $targetMacroY = $18;
 $19 = $0;
 $20 = (($19) + 4|0);
 $21 = HEAP16[$20>>1]|0;
 $22 = $21&65535;
 $23 = $22 >> 5;
 $24 = $23 >> 7;
 $25 = $24&255;
 $macroTileX = $25;
 $26 = $0;
 $27 = (($26) + 4|0);
 $28 = (($27) + 2|0);
 $29 = HEAP16[$28>>1]|0;
 $30 = $29&65535;
 $31 = $30 >> 5;
 $32 = $31 >> 7;
 $33 = $32&255;
 $macroTileY = $33;
 $34 = $macroTileX;
 $35 = $34&255;
 $36 = $35 << 4;
 $37 = $macroTileY;
 $38 = $37&255;
 $39 = (($36) + ($38))|0;
 $40 = $39&255;
 $macroTileIndex = $40;
 $41 = $0;
 $42 = (($41) + 4|0);
 $43 = HEAP16[$42>>1]|0;
 $44 = $43&65535;
 $45 = $44 >> 5;
 $46 = $1;
 $47 = (($46) + 4|0);
 $48 = HEAP16[$47>>1]|0;
 $49 = $48&65535;
 $50 = $49 >> 5;
 $51 = (($45) - ($50))|0;
 $52 = $51&65535;
 $diffX = $52;
 $53 = $0;
 $54 = (($53) + 4|0);
 $55 = (($54) + 2|0);
 $56 = HEAP16[$55>>1]|0;
 $57 = $56&65535;
 $58 = $57 >> 5;
 $59 = $1;
 $60 = (($59) + 4|0);
 $61 = (($60) + 2|0);
 $62 = HEAP16[$61>>1]|0;
 $63 = $62&65535;
 $64 = $63 >> 5;
 $65 = (($58) - ($64))|0;
 $66 = $65&65535;
 $diffY = $66;
 $67 = $diffX;
 $68 = $67 << 16 >> 16;
 $69 = ($68|0)<(0);
 if ($69) {
  $70 = $diffX;
  $71 = $70 << 16 >> 16;
  $72 = (0 - ($71))|0;
  $76 = $72;
 } else {
  $73 = $diffX;
  $74 = $73 << 16 >> 16;
  $76 = $74;
 }
 $75 = $76&65535;
 $diffX = $75;
 $77 = $diffY;
 $78 = $77 << 16 >> 16;
 $79 = ($78|0)<(0);
 if ($79) {
  $80 = $diffY;
  $81 = $80 << 16 >> 16;
  $82 = (0 - ($81))|0;
  $86 = $82;
 } else {
  $83 = $diffY;
  $84 = $83 << 16 >> 16;
  $86 = $84;
 }
 $85 = $86&65535;
 $diffY = $85;
 $87 = $2;
 $88 = $87&1;
 if (!($88)) {
  $89 = $diffX;
  $90 = $89 << 16 >> 16;
  $91 = ($90|0)<(16);
  if ($91) {
   $92 = $diffY;
   $93 = $92 << 16 >> 16;
   $94 = ($93|0)<(16);
   if ($94) {
    $95 = $1;
    $96 = (($95) + 4|0);
    $97 = (($96) + 2|0);
    $98 = HEAP16[$97>>1]|0;
    $99 = $98&65535;
    $100 = $0;
    $101 = (($100) + 4|0);
    $102 = (($101) + 2|0);
    $103 = HEAP16[$102>>1]|0;
    $104 = $103&65535;
    $105 = (($99) - ($104))|0;
    $106 = $105&65535;
    $107 = $1;
    $108 = (($107) + 4|0);
    $109 = HEAP16[$108>>1]|0;
    $110 = $109&65535;
    $111 = $0;
    $112 = (($111) + 4|0);
    $113 = HEAP16[$112>>1]|0;
    $114 = $113&65535;
    $115 = (($110) - ($114))|0;
    $116 = $115&65535;
    $117 = (_Math_ATan2($106,$116)|0);
    $118 = $117&255;
    $desiredAngle = $118;
    $119 = $0;
    $120 = $desiredAngle;
    _AI_ReverseToAngle($119,$120);
    $121 = $0;
    $122 = (($121) + 27|0);
    HEAP8[$122>>0] = 0;
    STACKTOP = sp;return;
   }
  }
 }
 $123 = $2;
 $124 = $123&1;
 if ($124) {
  label = 15;
 } else {
  $125 = $diffX;
  $126 = $125 << 16 >> 16;
  $127 = ($126|0)<(18);
  if ($127) {
   $128 = $diffY;
   $129 = $128 << 16 >> 16;
   $130 = ($129|0)<(18);
   if ($130) {
    $131 = $0;
    $132 = (($131) + 22|0);
    HEAP16[$132>>1] = 0;
   } else {
    label = 15;
   }
  } else {
   label = 15;
  }
 }
 if ((label|0) == 15) {
  $133 = $diffX;
  $134 = $133 << 16 >> 16;
  $135 = ($134|0)<(32);
  if ($135) {
   $136 = $diffY;
   $137 = $136 << 16 >> 16;
   $138 = ($137|0)<(32);
   if ($138) {
    label = 19;
   } else {
    label = 17;
   }
  } else {
   label = 17;
  }
  do {
   if ((label|0) == 17) {
    $139 = $macroTileX;
    $140 = $139&255;
    $141 = $targetMacroX;
    $142 = $141&255;
    $143 = ($140|0)==($142|0);
    if ($143) {
     $144 = $macroTileY;
     $145 = $144&255;
     $146 = $targetMacroY;
     $147 = $146&255;
     $148 = ($145|0)==($147|0);
     if ($148) {
      label = 19;
      break;
     }
    }
    $177 = $0;
    $178 = (($177) + 27|0);
    $179 = HEAP8[$178>>0]|0;
    $180 = $179&255;
    $181 = $macroTileIndex;
    $182 = $181&255;
    $183 = ($180|0)!=($182|0);
    if ($183) {
     $lastExit = -1;
     $184 = $macroTileX;
     $185 = $macroTileY;
     $186 = (_World_GetAllMacroTileExits($184,$185)|0);
     $exits = $186;
     $187 = $0;
     $188 = (($187) + 27|0);
     $189 = HEAP8[$188>>0]|0;
     $190 = $macroTileIndex;
     $191 = (_AI_GetExitFromTo($189,$190)|0);
     $lastExit = $191;
     $192 = $lastExit;
     $193 = $192&255;
     $194 = ($193|0)!=(255);
     if ($194) {
      $195 = $macroTileIndex;
      $196 = $lastExit;
      $197 = $targetMacroX;
      $198 = $197&255;
      $199 = $198 << 4;
      $200 = $targetMacroY;
      $201 = $200&255;
      $202 = (($199) + ($201))|0;
      $203 = $202&255;
      $204 = (_AI_GetPathScore($195,$196,$203)|0);
      $205 = $204&255;
      $206 = ($205|0)!=(0);
      if ($206) {
       $207 = $lastExit;
       $208 = $207&255;
       $209 = 1 << $208;
       $210 = $209 ^ -1;
       $211 = $exits;
       $212 = $211&255;
       $213 = $212 & $210;
       $214 = $213&255;
       $exits = $214;
      }
     }
     $215 = $macroTileIndex;
     $216 = $0;
     $217 = (($216) + 27|0);
     HEAP8[$217>>0] = $215;
     $218 = $macroTileIndex;
     $219 = $exits;
     $220 = $targetMacroX;
     $221 = $220&255;
     $222 = $221 << 4;
     $223 = $targetMacroY;
     $224 = $223&255;
     $225 = (($222) + ($224))|0;
     $226 = $225&255;
     $227 = (_AI_GetDirectionTo($218,$219,$226)|0);
     $228 = $0;
     $229 = (($228) + 27|0);
     $230 = (($229) + 1|0);
     $231 = HEAP8[$230>>0]|0;
     $232 = $227 & 15;
     $233 = $231 & -16;
     $234 = $233 | $232;
     HEAP8[$230>>0] = $234;
    }
    $235 = $0;
    $236 = $0;
    $237 = (($236) + 27|0);
    $238 = (($237) + 1|0);
    $239 = HEAP8[$238>>0]|0;
    $240 = $239 & 15;
    _AI_DriveInDirection($235,$240);
   }
  } while(0);
  if ((label|0) == 19) {
   $149 = $1;
   $150 = (($149) + 4|0);
   $151 = (($150) + 2|0);
   $152 = HEAP16[$151>>1]|0;
   $153 = $152&65535;
   $154 = $0;
   $155 = (($154) + 4|0);
   $156 = (($155) + 2|0);
   $157 = HEAP16[$156>>1]|0;
   $158 = $157&65535;
   $159 = (($153) - ($158))|0;
   $160 = $159&65535;
   $161 = $1;
   $162 = (($161) + 4|0);
   $163 = HEAP16[$162>>1]|0;
   $164 = $163&65535;
   $165 = $0;
   $166 = (($165) + 4|0);
   $167 = HEAP16[$166>>1]|0;
   $168 = $167&65535;
   $169 = (($164) - ($168))|0;
   $170 = $169&65535;
   $171 = (_Math_ATan2($160,$170)|0);
   $172 = $171&255;
   $desiredAngle1 = $172;
   $173 = $0;
   $174 = $desiredAngle1;
   _AI_TurnToAngle($173,$174);
   $175 = $0;
   $176 = (($175) + 27|0);
   HEAP8[$176>>0] = 0;
  }
 }
 STACKTOP = sp;return;
}
function _AI_ReverseToAngle($car,$desiredAngle) {
 $car = $car|0;
 $desiredAngle = $desiredAngle|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $diff = 0, $quantizedAngle = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = $desiredAngle;
 $2 = $0;
 $3 = (($2) + 21|0);
 $4 = HEAP8[$3>>0]|0;
 $5 = $4&255;
 $6 = (($5) + 8)|0;
 $7 = $6 & 240;
 $8 = $7&255;
 $quantizedAngle = $8;
 $9 = $1;
 $10 = $9&255;
 $11 = $quantizedAngle;
 $12 = $11&255;
 $13 = (($10) - ($12))|0;
 $14 = $13&255;
 $diff = $14;
 $15 = $diff;
 $16 = $15 << 24 >> 24;
 $17 = ($16|0)<(0);
 if ($17) {
  $18 = $0;
  $19 = (($18) + 22|0);
  HEAP16[$19>>1] = 129;
  STACKTOP = sp;return;
 }
 $20 = $diff;
 $21 = $20 << 24 >> 24;
 $22 = ($21|0)>(0);
 if ($22) {
  $23 = $0;
  $24 = (($23) + 22|0);
  HEAP16[$24>>1] = 65;
 } else {
  $25 = $0;
  $26 = (($25) + 22|0);
  HEAP16[$26>>1] = 1;
 }
 STACKTOP = sp;return;
}
function _AI_TurnToAngle($car,$desiredAngle) {
 $car = $car|0;
 $desiredAngle = $desiredAngle|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $diff = 0, $quantizedAngle = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = $desiredAngle;
 $2 = $0;
 $3 = (($2) + 21|0);
 $4 = HEAP8[$3>>0]|0;
 $5 = $4&255;
 $6 = (($5) + 8)|0;
 $7 = $6 & 240;
 $8 = $7&255;
 $quantizedAngle = $8;
 $9 = $1;
 $10 = $9&255;
 $11 = $quantizedAngle;
 $12 = $11&255;
 $13 = (($10) - ($12))|0;
 $14 = $13&255;
 $diff = $14;
 $15 = $diff;
 $16 = $15 << 24 >> 24;
 $17 = ($16|0)<(0);
 if ($17) {
  $18 = $diff;
  $19 = $18 << 24 >> 24;
  $20 = ($19|0)<(-80);
  if ($20) {
   $21 = $0;
   $22 = (($21) + 22|0);
   HEAP16[$22>>1] = 129;
  } else {
   $23 = $0;
   $24 = (($23) + 22|0);
   HEAP16[$24>>1] = 320;
   $25 = $diff;
   $26 = $25 << 24 >> 24;
   $27 = ($26|0)<(-32);
   if ($27) {
    $28 = $0;
    _AI_ThrottleSpeed($28,3);
   }
  }
  STACKTOP = sp;return;
 }
 $29 = $diff;
 $30 = $29 << 24 >> 24;
 $31 = ($30|0)>(0);
 if ($31) {
  $32 = $diff;
  $33 = $32 << 24 >> 24;
  $34 = ($33|0)>(80);
  if ($34) {
   $35 = $0;
   $36 = (($35) + 22|0);
   HEAP16[$36>>1] = 65;
  } else {
   $37 = $0;
   $38 = (($37) + 22|0);
   HEAP16[$38>>1] = 384;
   $39 = $diff;
   $40 = $39 << 24 >> 24;
   $41 = ($40|0)>(32);
   if ($41) {
    $42 = $0;
    _AI_ThrottleSpeed($42,3);
   }
  }
 } else {
  $43 = $0;
  $44 = (($43) + 22|0);
  HEAP16[$44>>1] = 256;
 }
 STACKTOP = sp;return;
}
function _AI_GetDirectionTo($current,$exits,$targetCoord) {
 $current = $current|0;
 $exits = $exits|0;
 $targetCoord = $targetCoord|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $bestExit = 0, $bestScore = 0, $currentExit = 0, $score = 0, label = 0;
 var sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $current;
 $2 = $exits;
 $3 = $targetCoord;
 $4 = $2;
 $5 = $4&255;
 $6 = ($5|0)==(0);
 if ($6) {
  $0 = 0;
  $34 = $0;
  STACKTOP = sp;return ($34|0);
 }
 $bestExit = -1;
 $currentExit = 0;
 $bestScore = -1;
 while(1) {
  $7 = $2;
  $8 = ($7<<24>>24)!=(0);
  if (!($8)) {
   break;
  }
  $9 = $2;
  $10 = $9&255;
  $11 = $10 & 1;
  $12 = ($11|0)!=(0);
  if ($12) {
   $13 = $1;
   $14 = $currentExit;
   $15 = $3;
   $16 = (_AI_GetPathScore($13,$14,$15)|0);
   $score = $16;
   $17 = $bestExit;
   $18 = $17&255;
   $19 = ($18|0)==(255);
   if ($19) {
    label = 8;
   } else {
    $20 = $score;
    $21 = $20&255;
    $22 = $bestScore;
    $23 = $22&255;
    $24 = ($21|0)<($23|0);
    if ($24) {
     label = 8;
    }
   }
   if ((label|0) == 8) {
    label = 0;
    $25 = $currentExit;
    $bestExit = $25;
    $26 = $score;
    $bestScore = $26;
   }
  }
  $27 = $currentExit;
  $28 = (($27) + 1)<<24>>24;
  $currentExit = $28;
  $29 = $2;
  $30 = $29&255;
  $31 = $30 >> 1;
  $32 = $31&255;
  $2 = $32;
 }
 $33 = $bestExit;
 $0 = $33;
 $34 = $0;
 STACKTOP = sp;return ($34|0);
}
function _AI_DriveInDirection($car,$trackDirection) {
 $car = $car|0;
 $trackDirection = $trackDirection|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0;
 var $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0;
 var $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0;
 var $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0;
 var $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0;
 var $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $desiredAngle = 0, $tileX = 0, $tileY = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = $trackDirection;
 $2 = $0;
 $3 = (($2) + 4|0);
 $4 = HEAP16[$3>>1]|0;
 $5 = $4&65535;
 $6 = $0;
 $7 = (($6) + 4|0);
 $8 = HEAP16[$7>>1]|0;
 $9 = $8&65535;
 $10 = $0;
 $11 = (($10) + 4|0);
 $12 = (($11) + 4|0);
 $13 = HEAP16[$12>>1]|0;
 $14 = $13&65535;
 $15 = (($9) - ($14))|0;
 $16 = $15 << 2;
 $17 = (($5) + ($16))|0;
 $18 = $17 >> 8;
 $19 = $18 & 15;
 $20 = $19&255;
 $tileX = $20;
 $21 = $0;
 $22 = (($21) + 4|0);
 $23 = (($22) + 2|0);
 $24 = HEAP16[$23>>1]|0;
 $25 = $24&65535;
 $26 = $0;
 $27 = (($26) + 4|0);
 $28 = (($27) + 2|0);
 $29 = HEAP16[$28>>1]|0;
 $30 = $29&65535;
 $31 = $0;
 $32 = (($31) + 4|0);
 $33 = (($32) + 6|0);
 $34 = HEAP16[$33>>1]|0;
 $35 = $34&65535;
 $36 = (($30) - ($35))|0;
 $37 = $36 << 2;
 $38 = (($25) + ($37))|0;
 $39 = $38 >> 8;
 $40 = $39 & 15;
 $41 = $40&255;
 $tileY = $41;
 $42 = $0;
 $43 = (($42) + 21|0);
 $44 = HEAP8[$43>>0]|0;
 $desiredAngle = $44;
 $45 = $1;
 $46 = $45&255;
 $47 = ($46|0)==(1);
 if ($47) {
  $48 = $tileX;
  $49 = $48&255;
  $50 = ($49|0)<(4);
  if ($50) {
   $desiredAngle = 0;
  } else {
   $51 = $tileX;
   $52 = $51&255;
   $53 = ($52|0)<(7);
   if ($53) {
    $desiredAngle = 32;
   } else {
    $54 = $tileX;
    $55 = $54&255;
    $56 = ($55|0)<(8);
    if ($56) {
     $desiredAngle = 64;
    } else {
     $57 = $tileX;
     $58 = $57&255;
     $59 = ($58|0)<(12);
     if ($59) {
      $desiredAngle = 96;
     } else {
      $desiredAngle = -128;
     }
    }
   }
  }
  $165 = $0;
  $166 = $desiredAngle;
  _AI_TurnToAngle($165,$166);
  STACKTOP = sp;return;
 }
 $60 = $1;
 $61 = $60&255;
 $62 = ($61|0)==(3);
 if ($62) {
  $63 = $tileX;
  $64 = $63&255;
  $65 = ($64|0)<(4);
  if ($65) {
   $desiredAngle = 0;
  } else {
   $66 = $tileX;
   $67 = $66&255;
   $68 = ($67|0)<(8);
   if ($68) {
    $desiredAngle = -32;
   } else {
    $69 = $tileX;
    $70 = $69&255;
    $71 = ($70|0)<(9);
    if ($71) {
     $desiredAngle = -64;
    } else {
     $72 = $tileX;
     $73 = $72&255;
     $74 = ($73|0)<(10);
     if ($74) {
      $desiredAngle = -96;
     } else {
      $desiredAngle = -128;
     }
    }
   }
  }
 } else {
  $75 = $1;
  $76 = $75&255;
  $77 = ($76|0)==(2);
  if ($77) {
   $78 = $tileY;
   $79 = $78&255;
   $80 = ($79|0)<(4);
   if ($80) {
    $desiredAngle = 64;
   } else {
    $81 = $tileY;
    $82 = $81&255;
    $83 = ($82|0)<(7);
    if ($83) {
     $desiredAngle = 96;
    } else {
     $84 = $tileY;
     $85 = $84&255;
     $86 = ($85|0)<(8);
     if ($86) {
      $desiredAngle = -128;
     } else {
      $87 = $tileY;
      $88 = $87&255;
      $89 = ($88|0)<(12);
      if ($89) {
       $desiredAngle = -96;
      } else {
       $desiredAngle = -64;
      }
     }
    }
   }
  } else {
   $90 = $1;
   $91 = $90&255;
   $92 = ($91|0)==(0);
   if ($92) {
    $93 = $tileY;
    $94 = $93&255;
    $95 = ($94|0)<(4);
    if ($95) {
     $desiredAngle = 64;
    } else {
     $96 = $tileY;
     $97 = $96&255;
     $98 = ($97|0)<(9);
     if ($98) {
      $desiredAngle = 32;
     } else {
      $99 = $tileY;
      $100 = $99&255;
      $101 = ($100|0)<(10);
      if ($101) {
       $desiredAngle = 0;
      } else {
       $102 = $tileY;
       $103 = $102&255;
       $104 = ($103|0)<(11);
       if ($104) {
        $desiredAngle = -32;
       } else {
        $desiredAngle = -64;
       }
      }
     }
    }
   } else {
    $105 = $1;
    $106 = $105&255;
    $107 = ($106|0)==(5);
    if ($107) {
     $108 = $tileX;
     $109 = $108&255;
     $110 = ($109|0)<(6);
     if ($110) {
      $desiredAngle = 0;
     } else {
      $111 = $tileX;
      $112 = $111&255;
      $113 = ($112|0)<(7);
      if ($113) {
       $desiredAngle = 32;
      } else {
       $114 = $tileX;
       $115 = $114&255;
       $116 = ($115|0)<(9);
       if ($116) {
        $desiredAngle = 64;
       } else {
        $117 = $tileX;
        $118 = $117&255;
        $119 = ($118|0)<(10);
        if ($119) {
         $desiredAngle = 96;
        } else {
         $desiredAngle = -128;
        }
       }
      }
     }
    } else {
     $120 = $1;
     $121 = $120&255;
     $122 = ($121|0)==(4);
     if ($122) {
      $123 = $tileX;
      $124 = $123&255;
      $125 = ($124|0)<(6);
      if ($125) {
       $desiredAngle = 0;
      } else {
       $126 = $tileX;
       $127 = $126&255;
       $128 = ($127|0)<(7);
       if ($128) {
        $desiredAngle = -32;
       } else {
        $129 = $tileX;
        $130 = $129&255;
        $131 = ($130|0)<(9);
        if ($131) {
         $desiredAngle = -64;
        } else {
         $132 = $tileX;
         $133 = $132&255;
         $134 = ($133|0)<(10);
         if ($134) {
          $desiredAngle = -96;
         } else {
          $desiredAngle = -128;
         }
        }
       }
      }
     } else {
      $135 = $1;
      $136 = $135&255;
      $137 = ($136|0)==(7);
      if ($137) {
       $138 = $tileX;
       $139 = $138&255;
       $140 = ($139|0)<(1);
       if ($140) {
        $desiredAngle = 0;
       } else {
        $141 = $tileX;
        $142 = $141&255;
        $143 = ($142|0)<(2);
        if ($143) {
         $desiredAngle = 32;
        } else {
         $144 = $tileX;
         $145 = $144&255;
         $146 = ($145|0)<(5);
         if ($146) {
          $desiredAngle = 64;
         } else {
          $147 = $tileX;
          $148 = $147&255;
          $149 = ($148|0)<(7);
          if ($149) {
           $desiredAngle = 96;
          } else {
           $desiredAngle = -128;
          }
         }
        }
       }
      } else {
       $150 = $1;
       $151 = $150&255;
       $152 = ($151|0)==(6);
       if ($152) {
        $153 = $tileX;
        $154 = $153&255;
        $155 = ($154|0)<(1);
        if ($155) {
         $desiredAngle = 0;
        } else {
         $156 = $tileX;
         $157 = $156&255;
         $158 = ($157|0)<(2);
         if ($158) {
          $desiredAngle = -32;
         } else {
          $159 = $tileX;
          $160 = $159&255;
          $161 = ($160|0)<(5);
          if ($161) {
           $desiredAngle = -64;
          } else {
           $162 = $tileX;
           $163 = $162&255;
           $164 = ($163|0)<(7);
           if ($164) {
            $desiredAngle = -96;
           } else {
            $desiredAngle = -128;
           }
          }
         }
        }
       }
      }
     }
    }
   }
  }
 }
 $165 = $0;
 $166 = $desiredAngle;
 _AI_TurnToAngle($165,$166);
 STACKTOP = sp;return;
}
function _AI_ThrottleSpeed($car,$amount) {
 $car = $car|0;
 $amount = $amount|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 var label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = $amount;
 $2 = $0;
 $3 = (($2) + 22|0);
 $4 = HEAP16[$3>>1]|0;
 $5 = $4&65535;
 $6 = $5 & 256;
 $7 = ($6|0)!=(0);
 if (!($7)) {
  STACKTOP = sp;return;
 }
 $8 = HEAP8[35920>>0]|0;
 $9 = $8&255;
 $10 = $1;
 $11 = $10&255;
 $12 = $9 & $11;
 $13 = ($12|0)!=(0);
 if ($13) {
  STACKTOP = sp;return;
 }
 $14 = $0;
 $15 = (($14) + 22|0);
 $16 = HEAP16[$15>>1]|0;
 $17 = $16&65535;
 $18 = $17 & -257;
 $19 = $18&65535;
 HEAP16[$15>>1] = $19;
 STACKTOP = sp;return;
}
function _AI_GetExitCone($tileX,$tileY) {
 $tileX = $tileX|0;
 $tileY = $tileY|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $tileX;
 $2 = $tileY;
 $3 = $1;
 $4 = $3&255;
 $5 = $4 & 15;
 $6 = $5&255;
 $1 = $6;
 $7 = $2;
 $8 = $7&255;
 $9 = $8 & 15;
 $10 = $9&255;
 $2 = $10;
 $11 = $2;
 $12 = $11&255;
 $13 = ($12|0)<(8);
 do {
  if ($13) {
   $14 = $2;
   $15 = $14&255;
   $16 = $1;
   $17 = $16&255;
   $18 = ($15|0)>($17|0);
   if ($18) {
    $0 = 2;
    break;
   }
   $19 = $2;
   $20 = $19&255;
   $21 = $1;
   $22 = $21&255;
   $23 = (15 - ($22))|0;
   $24 = ($20|0)>($23|0);
   if ($24) {
    $0 = 0;
    break;
   } else {
    $0 = 3;
    break;
   }
  } else {
   $25 = $2;
   $26 = $25&255;
   $27 = $1;
   $28 = $27&255;
   $29 = ($26|0)<($28|0);
   if ($29) {
    $0 = 2;
    break;
   }
   $30 = $2;
   $31 = $30&255;
   $32 = $1;
   $33 = $32&255;
   $34 = (15 - ($33))|0;
   $35 = ($31|0)<($34|0);
   if ($35) {
    $0 = 0;
    break;
   } else {
    $0 = 1;
    break;
   }
  }
 } while(0);
 $36 = $0;
 STACKTOP = sp;return ($36|0);
}
function _AI_DriveToTile($car,$targetX,$targetY) {
 $car = $car|0;
 $targetX = $targetX|0;
 $targetY = $targetY|0;
 var $$expand_i1_val = 0, $$expand_i1_val2 = 0, $$expand_i1_val4 = 0, $$expand_i1_val6 = 0, $$pre_trunc = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0;
 var $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0, $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0;
 var $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0, $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0;
 var $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0, $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0;
 var $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0, $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0;
 var $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0, $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0;
 var $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0;
 var $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0;
 var $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0;
 var $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0;
 var $99 = 0, $currentCone = 0, $desiredAngle = 0, $exits = 0, $lastExit = 0, $macroTileIndex = 0, $macroTileX = 0, $macroTileY = 0, $posX = 0, $posY = 0, $targetCone = 0, $targetMacroX = 0, $targetMacroY = 0, $tileX = 0, $tileY = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $car;
 $2 = $targetX;
 $3 = $targetY;
 $4 = $2;
 $5 = $4&255;
 $6 = $5 >> 4;
 $7 = $6&255;
 $targetMacroX = $7;
 $8 = $3;
 $9 = $8&255;
 $10 = $9 >> 4;
 $11 = $10&255;
 $targetMacroY = $11;
 $12 = $1;
 $13 = (($12) + 4|0);
 $14 = HEAP16[$13>>1]|0;
 $15 = $14&65535;
 $16 = $15 >> 5;
 $17 = $16 >> 7;
 $18 = $17&255;
 $macroTileX = $18;
 $19 = $1;
 $20 = (($19) + 4|0);
 $21 = (($20) + 2|0);
 $22 = HEAP16[$21>>1]|0;
 $23 = $22&65535;
 $24 = $23 >> 5;
 $25 = $24 >> 7;
 $26 = $25&255;
 $macroTileY = $26;
 $27 = $macroTileX;
 $28 = $27&255;
 $29 = $28 << 4;
 $30 = $macroTileY;
 $31 = $30&255;
 $32 = (($29) + ($31))|0;
 $33 = $32&255;
 $macroTileIndex = $33;
 $34 = $macroTileX;
 $35 = $34&255;
 $36 = $targetMacroX;
 $37 = $36&255;
 $38 = ($35|0)!=($37|0);
 if (!($38)) {
  $39 = $macroTileY;
  $40 = $39&255;
  $41 = $targetMacroY;
  $42 = $41&255;
  $43 = ($40|0)!=($42|0);
  if (!($43)) {
   $96 = $1;
   $97 = (($96) + 4|0);
   $98 = HEAP16[$97>>1]|0;
   $99 = $98&65535;
   $100 = $99 >> 5;
   $101 = $100 >> 3;
   $102 = $101&255;
   $tileX = $102;
   $103 = $1;
   $104 = (($103) + 4|0);
   $105 = (($104) + 2|0);
   $106 = HEAP16[$105>>1]|0;
   $107 = $106&65535;
   $108 = $107 >> 5;
   $109 = $108 >> 3;
   $110 = $109&255;
   $tileY = $110;
   $111 = $tileX;
   $112 = $111&255;
   $113 = $2;
   $114 = $113&255;
   $115 = ($112|0)==($114|0);
   if ($115) {
    $116 = $tileY;
    $117 = $116&255;
    $118 = $3;
    $119 = $118&255;
    $120 = ($117|0)==($119|0);
    if ($120) {
     $121 = $1;
     $122 = (($121) + 22|0);
     HEAP16[$122>>1] = 0;
     $$expand_i1_val2 = 1;
     $0 = $$expand_i1_val2;
     $$pre_trunc = $0;
     $195 = $$pre_trunc&1;
     STACKTOP = sp;return ($195|0);
    }
   }
   $123 = $tileX;
   $124 = $123&255;
   $125 = $2;
   $126 = $125&255;
   $127 = (($126) - 3)|0;
   $128 = ($124|0)<($127|0);
   if ($128) {
    label = 15;
   } else {
    $129 = $tileX;
    $130 = $129&255;
    $131 = $2;
    $132 = $131&255;
    $133 = (($132) + 3)|0;
    $134 = ($130|0)>($133|0);
    if ($134) {
     label = 15;
    } else {
     $135 = $tileY;
     $136 = $135&255;
     $137 = $3;
     $138 = $137&255;
     $139 = (($138) - 3)|0;
     $140 = ($136|0)<($139|0);
     if ($140) {
      label = 15;
     } else {
      $141 = $tileY;
      $142 = $141&255;
      $143 = $3;
      $144 = $143&255;
      $145 = (($144) + 3)|0;
      $146 = ($142|0)>($145|0);
      if ($146) {
       label = 15;
      }
     }
    }
   }
   do {
    if ((label|0) == 15) {
     $147 = $2;
     $148 = $3;
     $149 = (_AI_GetExitCone($147,$148)|0);
     $targetCone = $149;
     $150 = $tileX;
     $151 = $tileY;
     $152 = (_AI_GetExitCone($150,$151)|0);
     $currentCone = $152;
     $153 = $targetCone;
     $154 = $153&255;
     $155 = $currentCone;
     $156 = $155&255;
     $157 = ($154|0)!=($156|0);
     if (!($157)) {
      break;
     }
     $158 = $1;
     $159 = $targetCone;
     _AI_DriveInDirection($158,$159);
     $$expand_i1_val4 = 0;
     $0 = $$expand_i1_val4;
     $$pre_trunc = $0;
     $195 = $$pre_trunc&1;
     STACKTOP = sp;return ($195|0);
    }
   } while(0);
   $160 = $2;
   $161 = $160&255;
   $162 = $161 << 3;
   $163 = (($162) + 4)|0;
   $164 = $163&65535;
   $posX = $164;
   $165 = $3;
   $166 = $165&255;
   $167 = $166 << 3;
   $168 = (($167) + 4)|0;
   $169 = $168&65535;
   $posY = $169;
   $170 = $1;
   $171 = (($170) + 4|0);
   $172 = HEAP16[$171>>1]|0;
   $173 = $172&65535;
   $174 = $173 >> 5;
   $175 = $posX;
   $176 = $175 << 16 >> 16;
   $177 = (($176) - ($174))|0;
   $178 = $177&65535;
   $posX = $178;
   $179 = $1;
   $180 = (($179) + 4|0);
   $181 = (($180) + 2|0);
   $182 = HEAP16[$181>>1]|0;
   $183 = $182&65535;
   $184 = $183 >> 5;
   $185 = $posY;
   $186 = $185 << 16 >> 16;
   $187 = (($186) - ($184))|0;
   $188 = $187&65535;
   $posY = $188;
   $189 = $posY;
   $190 = $posX;
   $191 = (_Math_ATan2($189,$190)|0);
   $192 = $191&255;
   $desiredAngle = $192;
   $193 = $1;
   $194 = $desiredAngle;
   _AI_TurnToAngle($193,$194);
   $$expand_i1_val6 = 0;
   $0 = $$expand_i1_val6;
   $$pre_trunc = $0;
   $195 = $$pre_trunc&1;
   STACKTOP = sp;return ($195|0);
  }
 }
 $44 = $1;
 $45 = (($44) + 27|0);
 $46 = HEAP8[$45>>0]|0;
 $47 = $46&255;
 $48 = $macroTileIndex;
 $49 = $48&255;
 $50 = ($47|0)!=($49|0);
 if ($50) {
  $lastExit = -1;
  $51 = $macroTileX;
  $52 = $macroTileY;
  $53 = (_World_GetAllMacroTileExits($51,$52)|0);
  $exits = $53;
  $54 = $1;
  $55 = (($54) + 27|0);
  $56 = HEAP8[$55>>0]|0;
  $57 = $macroTileIndex;
  $58 = (_AI_GetExitFromTo($56,$57)|0);
  $lastExit = $58;
  $59 = $lastExit;
  $60 = $59&255;
  $61 = ($60|0)!=(255);
  if ($61) {
   $62 = $lastExit;
   $63 = $62&255;
   $64 = 1 << $63;
   $65 = $64 ^ -1;
   $66 = $exits;
   $67 = $66&255;
   $68 = $67 & $65;
   $69 = $68&255;
   $exits = $69;
  }
  $70 = $macroTileIndex;
  $71 = $1;
  $72 = (($71) + 27|0);
  HEAP8[$72>>0] = $70;
  $73 = $macroTileIndex;
  $74 = $exits;
  $75 = $targetMacroX;
  $76 = $75&255;
  $77 = $76 << 4;
  $78 = $targetMacroY;
  $79 = $78&255;
  $80 = (($77) + ($79))|0;
  $81 = $80&255;
  $82 = (_AI_GetDirectionTo($73,$74,$81)|0);
  $83 = $1;
  $84 = (($83) + 27|0);
  $85 = (($84) + 1|0);
  $86 = HEAP8[$85>>0]|0;
  $87 = $82 & 15;
  $88 = $86 & -16;
  $89 = $88 | $87;
  HEAP8[$85>>0] = $89;
 }
 $90 = $1;
 $91 = $1;
 $92 = (($91) + 27|0);
 $93 = (($92) + 1|0);
 $94 = HEAP8[$93>>0]|0;
 $95 = $94 & 15;
 _AI_DriveInDirection($90,$95);
 $$expand_i1_val = 0;
 $0 = $$expand_i1_val;
 $$pre_trunc = $0;
 $195 = $$pre_trunc&1;
 STACKTOP = sp;return ($195|0);
}
function _AI_Wander($car) {
 $car = $car|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $12 = 0;
 var $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0;
 var $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0;
 var $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0;
 var $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0;
 var $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $exits = 0, $facingDirection = 0, $macroTileIndex = 0, $macroTileX = 0, $macroTileY = 0;
 var label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $car;
 $1 = $0;
 $2 = HEAP16[$1>>1]|0;
 $3 = $2&65535;
 $4 = $3 >> 12;
 $5 = $4&255;
 $macroTileX = $5;
 $6 = $0;
 $7 = (($6) + 2|0);
 $8 = HEAP16[$7>>1]|0;
 $9 = $8&65535;
 $10 = $9 >> 12;
 $11 = $10&255;
 $macroTileY = $11;
 $12 = $macroTileX;
 $13 = $12&255;
 $14 = $13 << 4;
 $15 = $macroTileY;
 $16 = $15&255;
 $17 = (($14) + ($16))|0;
 $18 = $17&255;
 $macroTileIndex = $18;
 $19 = $0;
 $20 = (($19) + 27|0);
 $21 = HEAP8[$20>>0]|0;
 $22 = $21&255;
 $23 = $macroTileIndex;
 $24 = $23&255;
 $25 = ($22|0)!=($24|0);
 if (!($25)) {
  $109 = $0;
  $110 = $0;
  $111 = (($110) + 27|0);
  $112 = (($111) + 1|0);
  $113 = HEAP8[$112>>0]|0;
  $114 = $113 & 15;
  _AI_DriveInDirection($109,$114);
  STACKTOP = sp;return;
 }
 $26 = $macroTileX;
 $27 = $macroTileY;
 $28 = (_World_GetMacroTileExits($26,$27)|0);
 $exits = $28;
 $29 = $macroTileIndex;
 $30 = $29&255;
 $31 = $0;
 $32 = (($31) + 27|0);
 $33 = HEAP8[$32>>0]|0;
 $34 = $33&255;
 $35 = (($34) + 1)|0;
 $36 = ($30|0)==($35|0);
 if ($36) {
  $37 = $exits;
  $38 = $37&255;
  $39 = $38 & -9;
  $40 = $39&255;
  $exits = $40;
 } else {
  $41 = $macroTileIndex;
  $42 = $41&255;
  $43 = $0;
  $44 = (($43) + 27|0);
  $45 = HEAP8[$44>>0]|0;
  $46 = $45&255;
  $47 = (($46) - 1)|0;
  $48 = ($42|0)==($47|0);
  if ($48) {
   $49 = $exits;
   $50 = $49&255;
   $51 = $50 & -3;
   $52 = $51&255;
   $exits = $52;
  } else {
   $53 = $macroTileIndex;
   $54 = $53&255;
   $55 = $0;
   $56 = (($55) + 27|0);
   $57 = HEAP8[$56>>0]|0;
   $58 = $57&255;
   $59 = (($58) - 16)|0;
   $60 = ($54|0)==($59|0);
   if ($60) {
    $61 = $exits;
    $62 = $61&255;
    $63 = $62 & -2;
    $64 = $63&255;
    $exits = $64;
   } else {
    $65 = $macroTileIndex;
    $66 = $65&255;
    $67 = $0;
    $68 = (($67) + 27|0);
    $69 = HEAP8[$68>>0]|0;
    $70 = $69&255;
    $71 = (($70) + 16)|0;
    $72 = ($66|0)==($71|0);
    if ($72) {
     $73 = $exits;
     $74 = $73&255;
     $75 = $74 & -5;
     $76 = $75&255;
     $exits = $76;
    } else {
     $77 = $0;
     $78 = (($77) + 21|0);
     $79 = HEAP8[$78>>0]|0;
     $80 = $79&255;
     $81 = (($80) + 16)|0;
     $82 = $81 >> 6;
     $83 = $82&255;
     $facingDirection = $83;
     $84 = $facingDirection;
     $85 = $84&255;
     $86 = (($85) + 2)|0;
     $87 = $86 & 3;
     $88 = $87&255;
     $facingDirection = $88;
     $89 = $facingDirection;
     $90 = $89&255;
     $91 = 1 << $90;
     $92 = $91 ^ -1;
     $93 = $exits;
     $94 = $93&255;
     $95 = $94 & $92;
     $96 = $95&255;
     $exits = $96;
    }
   }
  }
 }
 $97 = $macroTileIndex;
 $98 = $0;
 $99 = (($98) + 27|0);
 HEAP8[$99>>0] = $97;
 $100 = $exits;
 $101 = (_Car_GetRandomDirection($100)|0);
 $102 = $0;
 $103 = (($102) + 27|0);
 $104 = (($103) + 1|0);
 $105 = HEAP8[$104>>0]|0;
 $106 = $101 & 15;
 $107 = $105 & -16;
 $108 = $107 | $106;
 HEAP8[$104>>0] = $108;
 $109 = $0;
 $110 = $0;
 $111 = (($110) + 27|0);
 $112 = (($111) + 1|0);
 $113 = HEAP8[$112>>0]|0;
 $114 = $113 & 15;
 _AI_DriveInDirection($109,$114);
 STACKTOP = sp;return;
}
function _Ped_CheckCarCollision($ped) {
 $ped = $ped|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $n = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $ped;
 $n = 0;
 while(1) {
  $2 = $n;
  $3 = $2&255;
  $4 = ($3|0)<(5);
  if (!($4)) {
   label = 10;
   break;
  }
  $5 = $n;
  $6 = $5&255;
  $7 = (29160 + (($6*30)|0)|0);
  $8 = (($7) + 29|0);
  $9 = HEAP8[$8>>0]|0;
  $10 = $9&255;
  $11 = ($10|0)!=(0);
  if ($11) {
   $12 = $1;
   $13 = $n;
   $14 = $13&255;
   $15 = (29160 + (($14*30)|0)|0);
   $16 = (($15) + 4|0);
   $17 = (_Ped_CheckParticleCollision($12,$16)|0);
   if ($17) {
    label = 6;
    break;
   }
   $18 = $1;
   $19 = $n;
   $20 = $19&255;
   $21 = (29160 + (($20*30)|0)|0);
   $22 = (($21) + 12|0);
   $23 = (_Ped_CheckParticleCollision($18,$22)|0);
   if ($23) {
    label = 6;
    break;
   }
  }
  $27 = $n;
  $28 = (($27) + 1)<<24>>24;
  $n = $28;
 }
 if ((label|0) == 6) {
  $24 = $n;
  $25 = $24&255;
  $26 = (29160 + (($25*30)|0)|0);
  $0 = $26;
  $29 = $0;
  STACKTOP = sp;return ($29|0);
 }
 else if ((label|0) == 10) {
  $0 = 0;
  $29 = $0;
  STACKTOP = sp;return ($29|0);
 }
 return (0)|0;
}
function _Ped_Spawn($pedType,$x,$y,$direction,$colorMask) {
 $pedType = $pedType|0;
 $x = $x|0;
 $y = $y|0;
 $direction = $direction|0;
 $colorMask = $colorMask|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $i = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $1 = $pedType;
 $2 = $x;
 $3 = $y;
 $4 = $direction;
 $5 = $colorMask;
 $i = 0;
 while(1) {
  $6 = $i;
  $7 = $6 << 24 >> 24;
  $8 = ($7|0)<(5);
  if (!($8)) {
   break;
  }
  $9 = $i;
  $10 = $9 << 24 >> 24;
  $11 = (35952 + ($10<<3)|0);
  $12 = HEAP8[$11>>0]|0;
  $13 = $12&255;
  $14 = ($13|0)==(0);
  if ($14) {
   label = 4;
   break;
  }
  $15 = $i;
  $16 = (($15) + 1)<<24>>24;
  $i = $16;
 }
 if ((label|0) == 4) {
 }
 $17 = $i;
 $18 = $17 << 24 >> 24;
 $19 = ($18|0)==(5);
 if ($19) {
  $0 = 0;
  $51 = $0;
  STACKTOP = sp;return ($51|0);
 } else {
  $20 = $2;
  $21 = $i;
  $22 = $21 << 24 >> 24;
  $23 = (35952 + ($22<<3)|0);
  $24 = (($23) + 2|0);
  HEAP16[$24>>1] = $20;
  $25 = $3;
  $26 = $i;
  $27 = $26 << 24 >> 24;
  $28 = (35952 + ($27<<3)|0);
  $29 = (($28) + 4|0);
  HEAP16[$29>>1] = $25;
  $30 = $4;
  $31 = $i;
  $32 = $31 << 24 >> 24;
  $33 = (35952 + ($32<<3)|0);
  $34 = (($33) + 7|0);
  $35 = HEAP8[$34>>0]|0;
  $36 = $30 & 3;
  $37 = $35 & -4;
  $38 = $37 | $36;
  HEAP8[$34>>0] = $38;
  $39 = $5;
  $40 = $i;
  $41 = $40 << 24 >> 24;
  $42 = (35952 + ($41<<3)|0);
  $43 = (($42) + 6|0);
  HEAP8[$43>>0] = $39;
  $44 = $1;
  $45 = $i;
  $46 = $45 << 24 >> 24;
  $47 = (35952 + ($46<<3)|0);
  HEAP8[$47>>0] = $44;
  $48 = $i;
  $49 = $48 << 24 >> 24;
  $50 = (35952 + ($49<<3)|0);
  $0 = $50;
  $51 = $0;
  STACKTOP = sp;return ($51|0);
 }
 return (0)|0;
}
function _Ped_SpawnNPC() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0;
 var $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $direction = 0, $tile = 0, $tileX = 0, $tileY = 0, $x = 0, $y = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = (_Math_Random()|0);
 $1 = $0&65535;
 $2 = $1 & 3;
 $3 = $2&255;
 $direction = $3;
 $4 = $direction;
 $5 = $4&255;
 if ((($5|0) == 0)) {
  $18 = HEAP16[28952>>1]|0;
  $19 = $18 << 16 >> 16;
  $20 = (($19) + 112)|0;
  $21 = (($20) + 150)|0;
  $22 = $21&65535;
  $x = $22;
  $23 = HEAP16[((28952 + 2|0))>>1]|0;
  $24 = $23 << 16 >> 16;
  $25 = (_Math_Random()|0);
  $26 = $25&65535;
  $27 = (($26|0) % 112)&-1;
  $28 = (($24) + ($27))|0;
  $29 = $28&65535;
  $y = $29;
 } else if ((($5|0) == 2)) {
  $42 = HEAP16[28952>>1]|0;
  $43 = $42 << 16 >> 16;
  $44 = (($43) + 112)|0;
  $45 = (($44) - 150)|0;
  $46 = $45&65535;
  $x = $46;
  $47 = HEAP16[((28952 + 2|0))>>1]|0;
  $48 = $47 << 16 >> 16;
  $49 = (_Math_Random()|0);
  $50 = $49&65535;
  $51 = (($50|0) % 112)&-1;
  $52 = (($48) + ($51))|0;
  $53 = $52&65535;
  $y = $53;
 } else if ((($5|0) == 1)) {
  $30 = HEAP16[28952>>1]|0;
  $31 = $30 << 16 >> 16;
  $32 = (_Math_Random()|0);
  $33 = $32&65535;
  $34 = (($33|0) % 112)&-1;
  $35 = (($31) + ($34))|0;
  $36 = $35&65535;
  $x = $36;
  $37 = HEAP16[((28952 + 2|0))>>1]|0;
  $38 = $37 << 16 >> 16;
  $39 = (($38) + 112)|0;
  $40 = (($39) + 150)|0;
  $41 = $40&65535;
  $y = $41;
 } else if ((($5|0) == 3)) {
  $6 = HEAP16[28952>>1]|0;
  $7 = $6 << 16 >> 16;
  $8 = (_Math_Random()|0);
  $9 = $8&65535;
  $10 = (($9|0) % 112)&-1;
  $11 = (($7) + ($10))|0;
  $12 = $11&65535;
  $x = $12;
  $13 = HEAP16[((28952 + 2|0))>>1]|0;
  $14 = $13 << 16 >> 16;
  $15 = (($14) + 112)|0;
  $16 = (($15) - 150)|0;
  $17 = $16&65535;
  $y = $17;
 } else {
  STACKTOP = sp;return;
 }
 $54 = $x;
 $55 = $54 << 16 >> 16;
 $56 = $55 >> 3;
 $57 = $56&65535;
 $tileX = $57;
 $58 = $y;
 $59 = $58 << 16 >> 16;
 $60 = $59 >> 3;
 $61 = $60&65535;
 $tileY = $61;
 $62 = $tileX;
 $63 = $tileY;
 $64 = (_World_GetTile($62,$63)|0);
 $tile = $64;
 $65 = $tile;
 $66 = $65&255;
 $67 = ($66|0)>=(1);
 if (!($67)) {
  STACKTOP = sp;return;
 }
 $68 = $tile;
 $69 = $68&255;
 $70 = ($69|0)<=(7);
 if (!($70)) {
  STACKTOP = sp;return;
 }
 $71 = $tileX;
 $72 = $71&65535;
 $73 = $72 << 3;
 $74 = (($73) + 4)|0;
 $75 = $74 << 5;
 $76 = $75&65535;
 $x = $76;
 $77 = $tileY;
 $78 = $77&65535;
 $79 = $78 << 3;
 $80 = (($79) + 4)|0;
 $81 = $80 << 5;
 $82 = $81&65535;
 $y = $82;
 $83 = $x;
 $84 = $y;
 $85 = (_Math_Random()|0);
 $86 = $85&65535;
 $87 = $86 & 3;
 $88 = $87&255;
 $89 = (_Math_RandomColorMask()|0);
 (_Ped_Spawn(1,$83,$84,$88,$89)|0);
 STACKTOP = sp;return;
}
function _Ped_UpdateMovement($ped) {
 $ped = $ped|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0;
 var $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0;
 var $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0;
 var $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0;
 var $currentTile = 0, $forwardX = 0, $forwardY = 0, $moveSpeed = 0, $nextTile = 0, $shiftedX = 0, $shiftedY = 0, $tileX = 0, $tileY = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $ped;
 $1 = $0;
 $2 = (($1) + 2|0);
 $3 = HEAP16[$2>>1]|0;
 $4 = $3&65535;
 $5 = $4 >> 8;
 $6 = $5&255;
 $tileX = $6;
 $7 = $0;
 $8 = (($7) + 4|0);
 $9 = HEAP16[$8>>1]|0;
 $10 = $9&65535;
 $11 = $10 >> 8;
 $12 = $11&255;
 $tileY = $12;
 $13 = $tileX;
 $14 = $13&255;
 $15 = $tileY;
 $16 = $15&255;
 $17 = (_World_GetTile($14,$16)|0);
 $currentTile = $17;
 $forwardX = 0;
 $forwardY = 0;
 $moveSpeed = 8;
 $18 = $0;
 $19 = HEAP8[$18>>0]|0;
 $20 = $19&255;
 $21 = ($20|0)==(2);
 if ($21) {
  $22 = $moveSpeed;
  $23 = $22 << 24 >> 24;
  $24 = $23 << 1;
  $25 = $24&255;
  $moveSpeed = $25;
 }
 $26 = (_ReadJoypad(0)|0);
 $27 = $26 & 4;
 $28 = ($27|0)!=(0);
 if (!($28)) {
  $29 = $0;
  $30 = (($29) + 7|0);
  $31 = HEAP8[$30>>0]|0;
  $32 = $31 & 3;
  $33 = $32&255;
  if ((($33|0) == 3)) {
   $58 = $moveSpeed;
   $59 = $58 << 24 >> 24;
   $60 = $0;
   $61 = (($60) + 4|0);
   $62 = HEAP16[$61>>1]|0;
   $63 = $62&65535;
   $64 = (($63) - ($59))|0;
   $65 = $64&65535;
   HEAP16[$61>>1] = $65;
   $forwardY = -128;
  } else if ((($33|0) == 1)) {
   $42 = $moveSpeed;
   $43 = $42 << 24 >> 24;
   $44 = $0;
   $45 = (($44) + 4|0);
   $46 = HEAP16[$45>>1]|0;
   $47 = $46&65535;
   $48 = (($47) + ($43))|0;
   $49 = $48&65535;
   HEAP16[$45>>1] = $49;
   $forwardY = 128;
  } else if ((($33|0) == 2)) {
   $50 = $moveSpeed;
   $51 = $50 << 24 >> 24;
   $52 = $0;
   $53 = (($52) + 2|0);
   $54 = HEAP16[$53>>1]|0;
   $55 = $54&65535;
   $56 = (($55) - ($51))|0;
   $57 = $56&65535;
   HEAP16[$53>>1] = $57;
   $forwardX = -128;
  } else if ((($33|0) == 0)) {
   $34 = $moveSpeed;
   $35 = $34 << 24 >> 24;
   $36 = $0;
   $37 = (($36) + 2|0);
   $38 = HEAP16[$37>>1]|0;
   $39 = $38&65535;
   $40 = (($39) + ($35))|0;
   $41 = $40&65535;
   HEAP16[$37>>1] = $41;
   $forwardX = 128;
  }
 }
 $66 = $0;
 $67 = HEAP8[$66>>0]|0;
 $68 = $67&255;
 $69 = ($68|0)!=(1);
 if ($69) {
  STACKTOP = sp;return;
 }
 $70 = $0;
 $71 = (($70) + 2|0);
 $72 = HEAP16[$71>>1]|0;
 $73 = $72&65535;
 $74 = $forwardX;
 $75 = $74 << 16 >> 16;
 $76 = (($73) + ($75))|0;
 $77 = $76 >> 8;
 $78 = $77&65535;
 $79 = $0;
 $80 = (($79) + 4|0);
 $81 = HEAP16[$80>>1]|0;
 $82 = $81&65535;
 $83 = $forwardY;
 $84 = $83 << 16 >> 16;
 $85 = (($82) + ($84))|0;
 $86 = $85 >> 8;
 $87 = $86&65535;
 $88 = (_World_GetTile($78,$87)|0);
 $nextTile = $88;
 $89 = $currentTile;
 $90 = $89&255;
 $91 = ($90|0)>=(1);
 do {
  if ($91) {
   $92 = $currentTile;
   $93 = $92&255;
   $94 = ($93|0)<=(7);
   if ($94) {
    $95 = $nextTile;
    $96 = $95&255;
    $97 = ($96|0)<(1);
    if (!($97)) {
     $98 = $nextTile;
     $99 = $98&255;
     $100 = ($99|0)>(7);
     if (!($100)) {
      break;
     }
    }
    $101 = (_Math_Random()|0);
    $102 = $101&65535;
    $103 = $102 & 1;
    $104 = ($103|0)==(0);
    if ($104) {
     $105 = $0;
     $106 = (($105) + 7|0);
     $107 = HEAP8[$106>>0]|0;
     $108 = $107 & 3;
     $109 = $108&255;
     $110 = (($109) - 1)|0;
     $111 = $110 & 3;
     $112 = $111&255;
     $113 = $0;
     $114 = (($113) + 7|0);
     $115 = HEAP8[$114>>0]|0;
     $116 = $112 & 3;
     $117 = $115 & -4;
     $118 = $117 | $116;
     HEAP8[$114>>0] = $118;
    } else {
     $119 = $0;
     $120 = (($119) + 7|0);
     $121 = HEAP8[$120>>0]|0;
     $122 = $121 & 3;
     $123 = $122&255;
     $124 = (($123) + 1)|0;
     $125 = $124 & 3;
     $126 = $125&255;
     $127 = $0;
     $128 = (($127) + 7|0);
     $129 = HEAP8[$128>>0]|0;
     $130 = $126 & 3;
     $131 = $129 & -4;
     $132 = $131 | $130;
     HEAP8[$128>>0] = $132;
    }
    $133 = $0;
    $134 = (($133) + 7|0);
    $135 = HEAP8[$134>>0]|0;
    $136 = $135 & 3;
    $137 = $136&255;
    $138 = ($137|0)==(0);
    if ($138) {
     label = 21;
    } else {
     $139 = $0;
     $140 = (($139) + 7|0);
     $141 = HEAP8[$140>>0]|0;
     $142 = $141 & 3;
     $143 = $142&255;
     $144 = ($143|0)==(2);
     if ($144) {
      label = 21;
     } else {
      $153 = $tileX;
      $154 = $153&255;
      $155 = $154 << 3;
      $156 = (($155) + 4)|0;
      $157 = $156 << 5;
      $158 = $157&65535;
      $159 = $0;
      $160 = (($159) + 2|0);
      HEAP16[$160>>1] = $158;
     }
    }
    if ((label|0) == 21) {
     $145 = $tileY;
     $146 = $145&255;
     $147 = $146 << 3;
     $148 = (($147) + 4)|0;
     $149 = $148 << 5;
     $150 = $149&65535;
     $151 = $0;
     $152 = (($151) + 4|0);
     HEAP16[$152>>1] = $150;
    }
   }
  }
 } while(0);
 $161 = $0;
 $162 = (($161) + 2|0);
 $163 = HEAP16[$162>>1]|0;
 $164 = $163&65535;
 $165 = $164 >> 5;
 $166 = $165&65535;
 $shiftedX = $166;
 $167 = $0;
 $168 = (($167) + 4|0);
 $169 = HEAP16[$168>>1]|0;
 $170 = $169&65535;
 $171 = $170 >> 5;
 $172 = $171&65535;
 $shiftedY = $172;
 $173 = $shiftedX;
 $174 = $173 << 16 >> 16;
 $175 = HEAP16[28952>>1]|0;
 $176 = $175 << 16 >> 16;
 $177 = (($176) + 292)|0;
 $178 = ($174|0)>($177|0);
 if (!($178)) {
  $179 = $shiftedX;
  $180 = $179 << 16 >> 16;
  $181 = HEAP16[28952>>1]|0;
  $182 = $181 << 16 >> 16;
  $183 = (($182) + 112)|0;
  $184 = (($183) - 180)|0;
  $185 = ($180|0)<($184|0);
  if (!($185)) {
   $186 = $shiftedY;
   $187 = $186 << 16 >> 16;
   $188 = HEAP16[((28952 + 2|0))>>1]|0;
   $189 = $188 << 16 >> 16;
   $190 = (($189) + 292)|0;
   $191 = ($187|0)>($190|0);
   if (!($191)) {
    $192 = $shiftedY;
    $193 = $192 << 16 >> 16;
    $194 = HEAP16[((28952 + 2|0))>>1]|0;
    $195 = $194 << 16 >> 16;
    $196 = (($195) + 112)|0;
    $197 = (($196) - 180)|0;
    $198 = ($193|0)<($197|0);
    if (!($198)) {
     STACKTOP = sp;return;
    }
   }
  }
 }
 $199 = $0;
 HEAP8[$199>>0] = 0;
 STACKTOP = sp;return;
}
function _Ped_Splat($ped) {
 $ped = $ped|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $ped;
 _TriggerFx(32,-1,0);
 $1 = $0;
 HEAP8[$1>>0] = 3;
 $2 = $0;
 $3 = (($2) + 7|0);
 $4 = HEAP8[$3>>0]|0;
 $5 = $4 & 3;
 HEAP8[$3>>0] = $5;
 STACKTOP = sp;return;
}
function _Ped_Update() {
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0;
 var $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0;
 var $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0;
 var $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0;
 var $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $hitCar = 0, $i = 0, $missionPedVisible = 0;
 var $numPeds = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $numPeds = 0;
 $missionPedVisible = 0;
 $i = 0;
 while(1) {
  $0 = $i;
  $1 = $0 << 24 >> 24;
  $2 = ($1|0)<(5);
  if (!($2)) {
   break;
  }
  $3 = $i;
  $4 = $3 << 24 >> 24;
  $5 = (35952 + ($4<<3)|0);
  $6 = HEAP8[$5>>0]|0;
  $7 = $6&255;
  $8 = ($7|0)==(3);
  if ($8) {
   $9 = $i;
   $10 = $9 << 24 >> 24;
   $11 = (35952 + ($10<<3)|0);
   $12 = (($11) + 7|0);
   $13 = HEAP8[$12>>0]|0;
   $14 = ($13&255) >>> 2;
   $15 = $14&255;
   $16 = ($15|0)>(60);
   if ($16) {
    $17 = $i;
    $18 = $17 << 24 >> 24;
    $19 = (35952 + ($18<<3)|0);
    HEAP8[$19>>0] = 0;
   } else {
    $20 = $i;
    $21 = $20 << 24 >> 24;
    $22 = (35952 + ($21<<3)|0);
    $23 = (($22) + 7|0);
    $24 = HEAP8[$23>>0]|0;
    $25 = ($24&255) >>> 2;
    $26 = (($25) + 1)<<24>>24;
    $27 = HEAP8[$23>>0]|0;
    $28 = $26 & 63;
    $29 = ($28 << 2)&255;
    $30 = $27 & 3;
    $31 = $30 | $29;
    HEAP8[$23>>0] = $31;
   }
  } else {
   $32 = $i;
   $33 = $32 << 24 >> 24;
   $34 = (35952 + ($33<<3)|0);
   $35 = HEAP8[$34>>0]|0;
   $36 = $35&255;
   $37 = ($36|0)!=(0);
   if ($37) {
    $38 = $i;
    $39 = $38 << 24 >> 24;
    $40 = (35952 + ($39<<3)|0);
    _Ped_UpdateMovement($40);
    $41 = $numPeds;
    $42 = (($41) + 1)<<24>>24;
    $numPeds = $42;
    $43 = $i;
    $44 = $43 << 24 >> 24;
    $45 = (35952 + ($44<<3)|0);
    $46 = HEAP8[$45>>0]|0;
    $47 = $46&255;
    $48 = ($47|0)==(2);
    if ($48) {
     $missionPedVisible = 1;
    } else {
     $49 = $i;
     $50 = $49 << 24 >> 24;
     $51 = (35952 + ($50<<3)|0);
     $52 = (_Ped_CheckCarCollision($51)|0);
     $hitCar = $52;
     $53 = $hitCar;
     $54 = ($53|0)!=(0|0);
     if ($54) {
      $55 = $hitCar;
      $56 = (($55) + 4|0);
      $57 = HEAP16[$56>>1]|0;
      $58 = $57&65535;
      $59 = $hitCar;
      $60 = (($59) + 4|0);
      $61 = (($60) + 4|0);
      $62 = HEAP16[$61>>1]|0;
      $63 = $62&65535;
      $64 = ($58|0)==($63|0);
      if ($64) {
       $65 = $hitCar;
       $66 = (($65) + 4|0);
       $67 = (($66) + 2|0);
       $68 = HEAP16[$67>>1]|0;
       $69 = $68&65535;
       $70 = $hitCar;
       $71 = (($70) + 4|0);
       $72 = (($71) + 6|0);
       $73 = HEAP16[$72>>1]|0;
       $74 = $73&65535;
       $75 = ($69|0)==($74|0);
       if ($75) {
        $76 = $i;
        $77 = $76 << 24 >> 24;
        $78 = (35952 + ($77<<3)|0);
        $79 = (($78) + 7|0);
        $80 = HEAP8[$79>>0]|0;
        $81 = $80 & 3;
        $82 = $81&255;
        $83 = (($82) + 2)|0;
        $84 = $83 & 3;
        $85 = $84&255;
        $86 = $i;
        $87 = $86 << 24 >> 24;
        $88 = (35952 + ($87<<3)|0);
        $89 = (($88) + 7|0);
        $90 = HEAP8[$89>>0]|0;
        $91 = $85 & 3;
        $92 = $90 & -4;
        $93 = $92 | $91;
        HEAP8[$89>>0] = $93;
       } else {
        label = 15;
       }
      } else {
       label = 15;
      }
      if ((label|0) == 15) {
       label = 0;
       $94 = $i;
       $95 = $94 << 24 >> 24;
       $96 = (35952 + ($95<<3)|0);
       _Ped_Splat($96);
       $97 = $hitCar;
       $98 = (($97) + 29|0);
       $99 = HEAP8[$98>>0]|0;
       $100 = $99&255;
       $101 = ($100|0)==(1);
       if ($101) {
        _Player_CommitOffence(3);
       }
      }
     }
    }
   }
  }
  $102 = $i;
  $103 = (($102) + 1)<<24>>24;
  $i = $103;
 }
 $104 = $missionPedVisible;
 $105 = $104&1;
 if ($105) {
  $106 = HEAP8[35920>>0]|0;
  $107 = $106&255;
  $108 = $107 & 7;
  $109 = ($108|0)==(1);
  if ($109) {
   _TriggerFx(7,15,1);
   $110 = HEAP8[35920>>0]|0;
   $111 = $110&255;
   $112 = $111 & 15;
   $113 = ($112|0)==(1);
   if ($113) {
   } else {
   }
  }
 }
 $114 = $numPeds;
 $115 = $114 << 24 >> 24;
 $116 = ($115|0)<(3);
 if (!($116)) {
  STACKTOP = sp;return;
 }
 _Ped_SpawnNPC();
 STACKTOP = sp;return;
}
function _Ped_Init() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $i = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $i = 0;
 while(1) {
  $0 = $i;
  $1 = $0 << 24 >> 24;
  $2 = ($1|0)<(5);
  if (!($2)) {
   break;
  }
  $3 = $i;
  $4 = $3 << 24 >> 24;
  $5 = (35952 + ($4<<3)|0);
  HEAP8[$5>>0] = 0;
  $6 = $i;
  $7 = $6 << 24 >> 24;
  $8 = (35952 + ($7<<3)|0);
  $9 = (($8) + 2|0);
  HEAP16[$9>>1] = 0;
  $10 = $i;
  $11 = $10 << 24 >> 24;
  $12 = (35952 + ($11<<3)|0);
  $13 = (($12) + 4|0);
  HEAP16[$13>>1] = 0;
  $14 = $i;
  $15 = (($14) + 1)<<24>>24;
  $i = $15;
 }
 STACKTOP = sp;return;
}
function _Ped_SetSprites() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $3 = 0;
 var $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $currentAnimation = 0, $i = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = HEAP8[35920>>0]|0;
 $1 = $0&255;
 $2 = $1 >> 3;
 $3 = $2 & 3;
 $4 = $3&255;
 $currentAnimation = $4;
 $5 = $currentAnimation;
 $6 = $5 << 24 >> 24;
 $7 = ($6|0)==(2);
 if ($7) {
  $currentAnimation = 0;
 }
 $8 = $currentAnimation;
 $9 = $8 << 24 >> 24;
 $10 = ($9|0)==(3);
 if ($10) {
  $currentAnimation = 2;
 }
 $i = 0;
 while(1) {
  $11 = $i;
  $12 = $11 << 24 >> 24;
  $13 = ($12|0)<(5);
  if (!($13)) {
   break;
  }
  $14 = $i;
  $15 = $14 << 24 >> 24;
  $16 = (35952 + ($15<<3)|0);
  $17 = HEAP8[$16>>0]|0;
  $18 = $17&255;
  $19 = ($18|0)!=(0);
  if ($19) {
   $20 = $i;
   $21 = $20 << 24 >> 24;
   $22 = (35952 + ($21<<3)|0);
   $23 = $currentAnimation;
   _Ped_SetupSprite($22,$23);
  }
  $24 = $i;
  $25 = (($24) + 1)<<24>>24;
  $i = $25;
 }
 STACKTOP = sp;return;
}
function _Ped_CheckParticleCollision($ped,$particle) {
 $ped = $ped|0;
 $particle = $particle|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0;
 var $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $ped;
 $1 = $particle;
 $2 = $0;
 $3 = (($2) + 2|0);
 $4 = HEAP16[$3>>1]|0;
 $5 = $4&65535;
 $6 = $1;
 $7 = HEAP16[$6>>1]|0;
 $8 = $7&65535;
 $9 = (($8) - 128)|0;
 $10 = ($5|0)>=($9|0);
 if (!($10)) {
  $40 = 0;
  STACKTOP = sp;return ($40|0);
 }
 $11 = $0;
 $12 = (($11) + 4|0);
 $13 = HEAP16[$12>>1]|0;
 $14 = $13&65535;
 $15 = $1;
 $16 = (($15) + 2|0);
 $17 = HEAP16[$16>>1]|0;
 $18 = $17&65535;
 $19 = (($18) - 128)|0;
 $20 = ($14|0)>=($19|0);
 if (!($20)) {
  $40 = 0;
  STACKTOP = sp;return ($40|0);
 }
 $21 = $0;
 $22 = (($21) + 2|0);
 $23 = HEAP16[$22>>1]|0;
 $24 = $23&65535;
 $25 = $1;
 $26 = HEAP16[$25>>1]|0;
 $27 = $26&65535;
 $28 = (($27) + 128)|0;
 $29 = ($24|0)<=($28|0);
 if (!($29)) {
  $40 = 0;
  STACKTOP = sp;return ($40|0);
 }
 $30 = $0;
 $31 = (($30) + 4|0);
 $32 = HEAP16[$31>>1]|0;
 $33 = $32&65535;
 $34 = $1;
 $35 = (($34) + 2|0);
 $36 = HEAP16[$35>>1]|0;
 $37 = $36&65535;
 $38 = (($37) + 128)|0;
 $39 = ($33|0)<=($38|0);
 $40 = $39;
 STACKTOP = sp;return ($40|0);
}
function _Ped_SetupSprite($ped,$currentAnimation) {
 $ped = $ped|0;
 $currentAnimation = $currentAnimation|0;
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $16 = 0;
 var $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0;
 var $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0;
 var $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0;
 var $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0;
 var $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $pedX = 0, $pedY = 0, $spriteIndex = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $ped;
 $1 = $currentAnimation;
 $2 = $0;
 $3 = (($2) + 2|0);
 $4 = HEAP16[$3>>1]|0;
 $5 = $4&65535;
 $6 = $5 >> 5;
 $7 = $6&65535;
 $pedX = $7;
 $8 = $0;
 $9 = (($8) + 4|0);
 $10 = HEAP16[$9>>1]|0;
 $11 = $10&65535;
 $12 = $11 >> 5;
 $13 = $12&65535;
 $pedY = $13;
 $14 = $pedX;
 $15 = $14 << 16 >> 16;
 $16 = HEAP16[28952>>1]|0;
 $17 = $16 << 16 >> 16;
 $18 = ($15|0)<($17|0);
 if (!($18)) {
  $19 = $pedY;
  $20 = $19 << 16 >> 16;
  $21 = HEAP16[((28952 + 2|0))>>1]|0;
  $22 = $21 << 16 >> 16;
  $23 = (($22) + 8)|0;
  $24 = ($20|0)<($23|0);
  if (!($24)) {
   $25 = $pedX;
   $26 = $25 << 16 >> 16;
   $27 = HEAP16[28952>>1]|0;
   $28 = $27 << 16 >> 16;
   $29 = (($28) + 224)|0;
   $30 = ($26|0)>($29|0);
   if (!($30)) {
    $31 = $pedY;
    $32 = $31 << 16 >> 16;
    $33 = HEAP16[((28952 + 2|0))>>1]|0;
    $34 = $33 << 16 >> 16;
    $35 = (($34) + 224)|0;
    $36 = ($32|0)>($35|0);
    if (!($36)) {
     $37 = (_Sprites_GetIndex()|0);
     $spriteIndex = $37;
     $38 = $spriteIndex;
     $39 = $38&255;
     $40 = ($39|0)!=(18);
     if (!($40)) {
      STACKTOP = sp;return;
     }
     $41 = $0;
     $42 = (($41) + 6|0);
     $43 = HEAP8[$42>>0]|0;
     $44 = $spriteIndex;
     $45 = $44&255;
     $46 = (2304 + (($45*5)|0)|0);
     $47 = (($46) + 4|0);
     HEAP8[$47>>0] = $43;
     $48 = $pedX;
     $49 = $48 << 16 >> 16;
     $50 = HEAP16[28952>>1]|0;
     $51 = $50 << 16 >> 16;
     $52 = (($49) - ($51))|0;
     $53 = (($52) - 4)|0;
     $54 = $53&255;
     $55 = $spriteIndex;
     $56 = $55&255;
     $57 = (2304 + (($56*5)|0)|0);
     HEAP8[$57>>0] = $54;
     $58 = $pedY;
     $59 = $58 << 16 >> 16;
     $60 = HEAP16[((28952 + 2|0))>>1]|0;
     $61 = $60 << 16 >> 16;
     $62 = (($59) - ($61))|0;
     $63 = (($62) - 4)|0;
     $64 = $63&255;
     $65 = $spriteIndex;
     $66 = $65&255;
     $67 = (2304 + (($66*5)|0)|0);
     $68 = (($67) + 1|0);
     HEAP8[$68>>0] = $64;
     $69 = $0;
     $70 = HEAP8[$69>>0]|0;
     $71 = $70&255;
     $72 = ($71|0)==(3);
     if ($72) {
      $73 = $spriteIndex;
      $74 = $73&255;
      $75 = (2304 + (($74*5)|0)|0);
      $76 = (($75) + 2|0);
      HEAP8[$76>>0] = 46;
      $77 = $0;
      $78 = (($77) + 7|0);
      $79 = HEAP8[$78>>0]|0;
      $80 = $79 & 3;
      $81 = $80&255;
      if ((($81|0) == 0)) {
       $82 = $spriteIndex;
       $83 = $82&255;
       $84 = (2304 + (($83*5)|0)|0);
       $85 = (($84) + 3|0);
       HEAP8[$85>>0] = 0;
      } else if ((($81|0) == 1)) {
       $86 = $spriteIndex;
       $87 = $86&255;
       $88 = (2304 + (($87*5)|0)|0);
       $89 = (($88) + 3|0);
       HEAP8[$89>>0] = 2;
      } else if ((($81|0) == 2)) {
       $90 = $spriteIndex;
       $91 = $90&255;
       $92 = (2304 + (($91*5)|0)|0);
       $93 = (($92) + 3|0);
       HEAP8[$93>>0] = 1;
      } else if ((($81|0) == 3)) {
       $94 = $spriteIndex;
       $95 = $94&255;
       $96 = (2304 + (($95*5)|0)|0);
       $97 = (($96) + 3|0);
       HEAP8[$97>>0] = 3;
      }
     } else {
      $98 = $0;
      $99 = (($98) + 7|0);
      $100 = HEAP8[$99>>0]|0;
      $101 = $100 & 3;
      $102 = $101&255;
      if ((($102|0) == 2)) {
       $127 = $1;
       $128 = $127 << 24 >> 24;
       $129 = (43 + ($128))|0;
       $130 = $129&255;
       $131 = $spriteIndex;
       $132 = $131&255;
       $133 = (2304 + (($132*5)|0)|0);
       $134 = (($133) + 2|0);
       HEAP8[$134>>0] = $130;
       $135 = $spriteIndex;
       $136 = $135&255;
       $137 = (2304 + (($136*5)|0)|0);
       $138 = (($137) + 3|0);
       HEAP8[$138>>0] = 1;
      } else if ((($102|0) == 3)) {
       $139 = $1;
       $140 = $139 << 24 >> 24;
       $141 = (40 + ($140))|0;
       $142 = $141&255;
       $143 = $spriteIndex;
       $144 = $143&255;
       $145 = (2304 + (($144*5)|0)|0);
       $146 = (($145) + 2|0);
       HEAP8[$146>>0] = $142;
       $147 = $spriteIndex;
       $148 = $147&255;
       $149 = (2304 + (($148*5)|0)|0);
       $150 = (($149) + 3|0);
       HEAP8[$150>>0] = 0;
      } else if ((($102|0) == 0)) {
       $103 = $1;
       $104 = $103 << 24 >> 24;
       $105 = (43 + ($104))|0;
       $106 = $105&255;
       $107 = $spriteIndex;
       $108 = $107&255;
       $109 = (2304 + (($108*5)|0)|0);
       $110 = (($109) + 2|0);
       HEAP8[$110>>0] = $106;
       $111 = $spriteIndex;
       $112 = $111&255;
       $113 = (2304 + (($112*5)|0)|0);
       $114 = (($113) + 3|0);
       HEAP8[$114>>0] = 0;
      } else if ((($102|0) == 1)) {
       $115 = $1;
       $116 = $115 << 24 >> 24;
       $117 = (40 + ($116))|0;
       $118 = $117&255;
       $119 = $spriteIndex;
       $120 = $119&255;
       $121 = (2304 + (($120*5)|0)|0);
       $122 = (($121) + 2|0);
       HEAP8[$122>>0] = $118;
       $123 = $spriteIndex;
       $124 = $123&255;
       $125 = (2304 + (($124*5)|0)|0);
       $126 = (($125) + 3|0);
       HEAP8[$126>>0] = 2;
      }
     }
     STACKTOP = sp;return;
    }
   }
  }
 }
 STACKTOP = sp;return;
}
function _Mission_GetParameterU8() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $param = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = HEAP32[((36904 + 8|0))>>2]|0;
 $1 = HEAP8[$0>>0]|0;
 $param = $1;
 $2 = HEAP32[((36904 + 8|0))>>2]|0;
 $3 = (($2) + 1|0);
 HEAP32[((36904 + 8|0))>>2] = $3;
 $4 = $param;
 STACKTOP = sp;return ($4|0);
}
function _Mission_Yield() {
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP32[((36904 + 4|0))>>2]|0;
 HEAP32[((36904 + 8|0))>>2] = $0;
 return;
}
function _Mission_Init() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $1 = $0 & -2;
 HEAP8[((36904 + 16|0))>>0]=$1&255;HEAP8[((36904 + 16|0))+1>>0]=$1>>8;
 $2 = HEAP32[36904>>2]|0;
 HEAP32[((36904 + 8|0))>>2] = $2;
 $3 = HEAP32[36904>>2]|0;
 HEAP32[((36904 + 4|0))>>2] = $3;
 $4 = HEAP8[((36904 + 14|0))>>0]|0;
 $5 = $4 & -16;
 $6 = $5 | 5;
 HEAP8[((36904 + 14|0))>>0] = $6;
 $7 = HEAP8[((36904 + 14|0))>>0]|0;
 $8 = $7 & 15;
 $9 = $8 | 96;
 HEAP8[((36904 + 14|0))>>0] = $9;
 HEAP8[((36904 + 12|0))>>0] = 0;
 HEAP16[((36904 + 16|0))>>1] = 0;
 _SetFontTilesIndex(25);
 _Mission_Update();
 return;
}
function _Mission_Update() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $3 = 0;
 var $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 while(1) {
  $0 = HEAP32[((36904 + 8|0))>>2]|0;
  HEAP32[((36904 + 4|0))>>2] = $0;
  $1 = (_Mission_GetParameterU8()|0);
  $2 = $1&255;
  do {
   switch ($2|0) {
   case 18:  {
    $3 = (_Mission_GetParameterU8()|0);
    _Camera_Shake($3);
    break;
   }
   case 14:  {
    _Mission_SetAIType();
    break;
   }
   case 3:  {
    _UI_ClearMessage();
    break;
   }
   case 7:  {
    _Mission_SetWanted();
    break;
   }
   case 15:  {
    _Mission_WaitForMissionCar();
    break;
   }
   case 1:  {
    _Mission_DisplayMessage(0);
    break;
   }
   case 10:  {
    _Mission_PedXYToCar();
    break;
   }
   case 0:  {
    _Mission_Wait();
    break;
   }
   case 16:  {
    _Mission_RaceForLaps();
    break;
   }
   case 6:  {
    _Mission_SpawnCar();
    break;
   }
   case 5:  {
    HEAP32[((36904 + 8|0))>>2] = 36304;
    break;
   }
   case 9:  {
    _Mission_PedCarToXY();
    break;
   }
   case 13:  {
    _Mission_SetMarkerXY();
    break;
   }
   case 11:  {
    _Mission_ObjectiveXY();
    break;
   }
   case 4:  {
    _Mission_Yield();
    break;
   }
   case 8:  {
    _Mission_SetFlag();
    break;
   }
   case 17:  {
    _Mission_PlaySound();
    break;
   }
   case 2:  {
    _Mission_DisplayMessage(1);
    break;
   }
   case 12:  {
    _Mission_ObjectiveCar();
    break;
   }
   default: {
   }
   }
  } while(0);
  $4 = HEAP32[((36904 + 8|0))>>2]|0;
  $5 = HEAP32[((36904 + 4|0))>>2]|0;
  $6 = ($4|0)==($5|0);
  $7 = $6&1;
  $8 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
  $9 = $8 & -2;
  $10 = $9 | $7;
  HEAP8[((36904 + 16|0))>>0]=$10&255;HEAP8[((36904 + 16|0))+1>>0]=$10>>8;
  $11 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
  $12 = $11 & 1;
  $13 = $12&1;
  $14 = $13 ^ 1;
  if (!($14)) {
   break;
  }
 }
 $15 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $16 = ($15&65535) >>> 7;
 $17 = $16 & 1;
 $18 = $17&1;
 if (!($18)) {
  return;
 }
 $19 = (_ReadJoypad(0)|0);
 $20 = $19 & 264;
 $21 = ($20|0)!=(0);
 if ($21) {
  $22 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
  $23 = ($22&65535) >>> 8;
  $24 = $23 & 1;
  $25 = $24&1;
  if ($25) {
   _Mission_Advance();
  } else {
   _Game_Reset();
  }
 }
 return;
}
function _Mission_SetSprites() {
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0;
 var $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0;
 var $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0;
 var $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0, $202 = 0, $203 = 0, $204 = 0, $205 = 0;
 var $206 = 0, $207 = 0, $208 = 0, $209 = 0, $21 = 0, $210 = 0, $211 = 0, $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0, $219 = 0, $22 = 0, $220 = 0, $221 = 0, $222 = 0, $223 = 0;
 var $224 = 0, $225 = 0, $226 = 0, $227 = 0, $228 = 0, $229 = 0, $23 = 0, $230 = 0, $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0, $241 = 0;
 var $242 = 0, $243 = 0, $244 = 0, $245 = 0, $246 = 0, $247 = 0, $248 = 0, $249 = 0, $25 = 0, $250 = 0, $251 = 0, $252 = 0, $253 = 0, $254 = 0, $255 = 0, $256 = 0, $257 = 0, $258 = 0, $259 = 0, $26 = 0;
 var $260 = 0, $261 = 0, $262 = 0, $263 = 0, $264 = 0, $265 = 0, $266 = 0, $267 = 0, $268 = 0, $269 = 0, $27 = 0, $270 = 0, $271 = 0, $272 = 0, $273 = 0, $274 = 0, $275 = 0, $276 = 0, $277 = 0, $278 = 0;
 var $279 = 0, $28 = 0, $280 = 0, $281 = 0, $282 = 0, $283 = 0, $284 = 0, $285 = 0, $286 = 0, $287 = 0, $288 = 0, $289 = 0, $29 = 0, $290 = 0, $291 = 0, $292 = 0, $293 = 0, $294 = 0, $295 = 0, $296 = 0;
 var $297 = 0, $298 = 0, $299 = 0, $3 = 0, $30 = 0, $300 = 0, $301 = 0, $302 = 0, $303 = 0, $304 = 0, $305 = 0, $306 = 0, $307 = 0, $308 = 0, $309 = 0, $31 = 0, $310 = 0, $311 = 0, $312 = 0, $313 = 0;
 var $314 = 0, $315 = 0, $316 = 0, $317 = 0, $318 = 0, $319 = 0, $32 = 0, $320 = 0, $321 = 0, $322 = 0, $323 = 0, $324 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0;
 var $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0;
 var $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0;
 var $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0;
 var $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $animation = 0, $markerAnimation = 0, $markerX = 0, $markerX1 = 0, $markerY = 0, $markerY2 = 0, $nextCoord = 0, $posX = 0, $posY = 0, $raceIndex = 0, $showArrow = 0, $spriteIndex = 0, $spriteIndex3 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 32|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $posX = 0;
 $posY = 0;
 $showArrow = 0;
 $0 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $1 = ($0&65535) >>> 3;
 $2 = $1 & 1;
 $3 = $2&1;
 if ($3) {
  $4 = HEAP8[35920>>0]|0;
  $5 = $4&255;
  $6 = $5 >> 3;
  $7 = $6&255;
  $markerAnimation = $7;
  $8 = HEAP8[((36904 + 12|0))>>0]|0;
  $9 = HEAP8[((36904 + 13|0))>>0]|0;
  $10 = $markerAnimation;
  $11 = $10&255;
  $12 = $11 & 3;
  $13 = (21 + ($12))|0;
  $14 = $13&255;
  _World_SetCustomTile($8,$9,$14);
  $15 = HEAP8[((36904 + 12|0))>>0]|0;
  $16 = $15&255;
  $17 = $16 << 16 >> 16;
  $18 = $17 << 3;
  $19 = $18&65535;
  $markerX = $19;
  $20 = HEAP8[((36904 + 13|0))>>0]|0;
  $21 = $20&255;
  $22 = $21 << 16 >> 16;
  $23 = $22 << 3;
  $24 = $23&65535;
  $markerY = $24;
  $25 = $markerX;
  $26 = $25 << 16 >> 16;
  $27 = HEAP16[28952>>1]|0;
  $28 = $27 << 16 >> 16;
  $29 = (($26) - ($28))|0;
  $30 = $29&65535;
  $posX = $30;
  $31 = $markerY;
  $32 = $31 << 16 >> 16;
  $33 = HEAP16[((28952 + 2|0))>>1]|0;
  $34 = $33 << 16 >> 16;
  $35 = (($32) - ($34))|0;
  $36 = (($35) - 4)|0;
  $37 = $36&65535;
  $posY = $37;
  $showArrow = 1;
 }
 $38 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $39 = ($38&65535) >>> 2;
 $40 = $39 & 1;
 $41 = $40&1;
 if ($41) {
  $42 = HEAP8[((36904 + 14|0))>>0]|0;
  $43 = $42 & 15;
  $44 = $43&255;
  $45 = ($44|0)<(5);
  if ($45) {
   $46 = HEAP8[((36904 + 14|0))>>0]|0;
   $47 = $46 & 15;
   $48 = $47&255;
   $49 = (29160 + (($48*30)|0)|0);
   $50 = HEAP16[$49>>1]|0;
   $51 = $50&65535;
   $52 = $51 >> 5;
   $53 = HEAP16[28952>>1]|0;
   $54 = $53 << 16 >> 16;
   $55 = (($52) - ($54))|0;
   $56 = (($55) - 4)|0;
   $57 = $56&65535;
   $posX = $57;
   $58 = HEAP8[((36904 + 14|0))>>0]|0;
   $59 = $58 & 15;
   $60 = $59&255;
   $61 = (29160 + (($60*30)|0)|0);
   $62 = (($61) + 2|0);
   $63 = HEAP16[$62>>1]|0;
   $64 = $63&65535;
   $65 = $64 >> 5;
   $66 = HEAP16[((28952 + 2|0))>>1]|0;
   $67 = $66 << 16 >> 16;
   $68 = (($65) - ($67))|0;
   $69 = (($68) - 8)|0;
   $70 = $69&65535;
   $posY = $70;
   $showArrow = 1;
  }
 }
 $71 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $72 = ($71&65535) >>> 9;
 $73 = $72 & 1;
 $74 = $73&1;
 if ($74) {
  $75 = HEAP8[29120>>0]|0;
  $76 = $75&255;
  $77 = ($76|0)<(5);
  if ($77) {
   $78 = HEAP8[29120>>0]|0;
   $79 = $78&255;
   $80 = (29160 + (($79*30)|0)|0);
   $81 = (($80) + 25|0);
   $82 = HEAP8[$81>>0]|0;
   $raceIndex = $82;
   while(1) {
    $83 = $raceIndex;
    $84 = (_Track_GetCoordFromIndex($83)|0);
    $nextCoord = $84;
    $85 = $raceIndex;
    $86 = (_Track_GetNextIndex($85)|0);
    $raceIndex = $86;
    $87 = $nextCoord;
    $88 = $87&255;
    $89 = $88 >> 4;
    $90 = $89&65535;
    $91 = $90 << 16 >> 16;
    $92 = $91 << 7;
    $93 = (($92) + 64)|0;
    $94 = $93&65535;
    $markerX1 = $94;
    $95 = $nextCoord;
    $96 = $95&255;
    $97 = $96 & 15;
    $98 = $97&65535;
    $99 = $98 << 16 >> 16;
    $100 = $99 << 7;
    $101 = (($100) + 64)|0;
    $102 = $101&65535;
    $markerY2 = $102;
    $103 = $markerX1;
    $104 = $103 << 16 >> 16;
    $105 = HEAP16[28952>>1]|0;
    $106 = $105 << 16 >> 16;
    $107 = (($104) - ($106))|0;
    $108 = $107&65535;
    $posX = $108;
    $109 = $markerY2;
    $110 = $109 << 16 >> 16;
    $111 = HEAP16[((28952 + 2|0))>>1]|0;
    $112 = $111 << 16 >> 16;
    $113 = (($110) - ($112))|0;
    $114 = (($113) - 4)|0;
    $115 = $114&65535;
    $posY = $115;
    $116 = $posX;
    $117 = $116 << 16 >> 16;
    $118 = ($117|0)>(0);
    if ($118) {
     $119 = $posY;
     $120 = $119 << 16 >> 16;
     $121 = ($120|0)>(8);
     if ($121) {
      $122 = $posX;
      $123 = $122 << 16 >> 16;
      $124 = ($123|0)<(224);
      if ($124) {
       $125 = $posY;
       $126 = $125 << 16 >> 16;
       $127 = ($126|0)<(224);
       $324 = $127;
      } else {
       $324 = 0;
      }
     } else {
      $324 = 0;
     }
    } else {
     $324 = 0;
    }
    if (!($324)) {
     break;
    }
   }
   $showArrow = 1;
  }
 }
 $128 = $showArrow;
 $129 = $128&1;
 if (!($129)) {
  STACKTOP = sp;return;
 }
 $130 = $posX;
 $131 = $130 << 16 >> 16;
 $132 = ($131|0)>(0);
 if ($132) {
  $133 = $posY;
  $134 = $133 << 16 >> 16;
  $135 = ($134|0)>(8);
  if ($135) {
   $136 = $posX;
   $137 = $136 << 16 >> 16;
   $138 = ($137|0)<(224);
   if ($138) {
    $139 = $posY;
    $140 = $139 << 16 >> 16;
    $141 = ($140|0)<(224);
    if ($141) {
     $142 = (_Sprites_GetIndex()|0);
     $spriteIndex = $142;
     $143 = $spriteIndex;
     $144 = $143&255;
     $145 = ($144|0)<(18);
     if ($145) {
      $146 = $spriteIndex;
      $147 = $146&255;
      $148 = (2304 + (($147*5)|0)|0);
      $149 = (($148) + 4|0);
      HEAP8[$149>>0] = -1;
      $150 = $posX;
      $151 = $150&255;
      $152 = $spriteIndex;
      $153 = $152&255;
      $154 = (2304 + (($153*5)|0)|0);
      HEAP8[$154>>0] = $151;
      $155 = $posY;
      $156 = $155&255;
      $157 = $156&255;
      $158 = HEAP8[35920>>0]|0;
      $159 = $158&255;
      $160 = $159 >> 4;
      $161 = $160 & 1;
      $162 = (($157) + ($161))|0;
      $163 = $162&255;
      $164 = $spriteIndex;
      $165 = $164&255;
      $166 = (2304 + (($165*5)|0)|0);
      $167 = (($166) + 1|0);
      HEAP8[$167>>0] = $163;
      $168 = $spriteIndex;
      $169 = $168&255;
      $170 = (2304 + (($169*5)|0)|0);
      $171 = (($170) + 2|0);
      HEAP8[$171>>0] = 50;
      $172 = $spriteIndex;
      $173 = $172&255;
      $174 = (2304 + (($173*5)|0)|0);
      $175 = (($174) + 3|0);
      HEAP8[$175>>0] = 0;
     }
    } else {
     label = 25;
    }
   } else {
    label = 25;
   }
  } else {
   label = 25;
  }
 } else {
  label = 25;
 }
 if ((label|0) == 25) {
  $176 = (_Sprites_GetIndex()|0);
  $spriteIndex3 = $176;
  $177 = $spriteIndex3;
  $178 = $177&255;
  $179 = ($178|0)<(18);
  if ($179) {
   $180 = HEAP8[35920>>0]|0;
   $181 = $180&255;
   $182 = $181 >> 4;
   $183 = $182 & 1;
   $184 = $183&255;
   $animation = $184;
   $185 = $posX;
   $186 = $185 << 16 >> 16;
   $187 = ($186|0)<(0);
   if ($187) {
    $188 = $animation;
    $189 = $188&255;
    $posX = $189;
    $190 = $spriteIndex3;
    $191 = $190&255;
    $192 = (2304 + (($191*5)|0)|0);
    $193 = (($192) + 2|0);
    HEAP8[$193>>0] = 48;
    $194 = $spriteIndex3;
    $195 = $194&255;
    $196 = (2304 + (($195*5)|0)|0);
    $197 = (($196) + 3|0);
    HEAP8[$197>>0] = 1;
   } else {
    $198 = $posX;
    $199 = $198 << 16 >> 16;
    $200 = ($199|0)>(216);
    if ($200) {
     $201 = $animation;
     $202 = $201&255;
     $203 = (216 - ($202))|0;
     $204 = $203&65535;
     $posX = $204;
     $205 = $spriteIndex3;
     $206 = $205&255;
     $207 = (2304 + (($206*5)|0)|0);
     $208 = (($207) + 2|0);
     HEAP8[$208>>0] = 48;
     $209 = $spriteIndex3;
     $210 = $209&255;
     $211 = (2304 + (($210*5)|0)|0);
     $212 = (($211) + 3|0);
     HEAP8[$212>>0] = 0;
    }
   }
   $213 = $posY;
   $214 = $213 << 16 >> 16;
   $215 = ($214|0)<(0);
   if ($215) {
    $216 = $animation;
    $217 = $216&255;
    $posY = $217;
    $218 = $posX;
    $219 = $218 << 16 >> 16;
    $220 = ($219|0)<=(1);
    if ($220) {
     $221 = $spriteIndex3;
     $222 = $221&255;
     $223 = (2304 + (($222*5)|0)|0);
     $224 = (($223) + 2|0);
     HEAP8[$224>>0] = 49;
     $225 = $spriteIndex3;
     $226 = $225&255;
     $227 = (2304 + (($226*5)|0)|0);
     $228 = (($227) + 3|0);
     HEAP8[$228>>0] = 3;
     $229 = $animation;
     $230 = $229&255;
     $231 = $posX;
     $232 = $231 << 16 >> 16;
     $233 = (($232) + ($230))|0;
     $234 = $233&65535;
     $posX = $234;
    } else {
     $235 = $posX;
     $236 = $235 << 16 >> 16;
     $237 = ($236|0)>=(215);
     if ($237) {
      $238 = $spriteIndex3;
      $239 = $238&255;
      $240 = (2304 + (($239*5)|0)|0);
      $241 = (($240) + 2|0);
      HEAP8[$241>>0] = 49;
      $242 = $spriteIndex3;
      $243 = $242&255;
      $244 = (2304 + (($243*5)|0)|0);
      $245 = (($244) + 3|0);
      HEAP8[$245>>0] = 2;
      $246 = $animation;
      $247 = $246&255;
      $248 = $posX;
      $249 = $248 << 16 >> 16;
      $250 = (($249) - ($247))|0;
      $251 = $250&65535;
      $posX = $251;
     } else {
      $252 = $spriteIndex3;
      $253 = $252&255;
      $254 = (2304 + (($253*5)|0)|0);
      $255 = (($254) + 2|0);
      HEAP8[$255>>0] = 50;
      $256 = $spriteIndex3;
      $257 = $256&255;
      $258 = (2304 + (($257*5)|0)|0);
      $259 = (($258) + 3|0);
      HEAP8[$259>>0] = 3;
     }
    }
   } else {
    $260 = $posY;
    $261 = $260 << 16 >> 16;
    $262 = ($261|0)>(184);
    if ($262) {
     $263 = $animation;
     $264 = $263&255;
     $265 = (184 - ($264))|0;
     $266 = $265&65535;
     $posY = $266;
     $267 = $posX;
     $268 = $267 << 16 >> 16;
     $269 = ($268|0)<=(1);
     if ($269) {
      $270 = $spriteIndex3;
      $271 = $270&255;
      $272 = (2304 + (($271*5)|0)|0);
      $273 = (($272) + 2|0);
      HEAP8[$273>>0] = 49;
      $274 = $spriteIndex3;
      $275 = $274&255;
      $276 = (2304 + (($275*5)|0)|0);
      $277 = (($276) + 3|0);
      HEAP8[$277>>0] = 1;
      $278 = $animation;
      $279 = $278&255;
      $280 = $posX;
      $281 = $280 << 16 >> 16;
      $282 = (($281) + ($279))|0;
      $283 = $282&65535;
      $posX = $283;
     } else {
      $284 = $posX;
      $285 = $284 << 16 >> 16;
      $286 = ($285|0)>=(215);
      if ($286) {
       $287 = $spriteIndex3;
       $288 = $287&255;
       $289 = (2304 + (($288*5)|0)|0);
       $290 = (($289) + 2|0);
       HEAP8[$290>>0] = 49;
       $291 = $spriteIndex3;
       $292 = $291&255;
       $293 = (2304 + (($292*5)|0)|0);
       $294 = (($293) + 3|0);
       HEAP8[$294>>0] = 0;
       $295 = $animation;
       $296 = $295&255;
       $297 = $posX;
       $298 = $297 << 16 >> 16;
       $299 = (($298) - ($296))|0;
       $300 = $299&65535;
       $posX = $300;
      } else {
       $301 = $spriteIndex3;
       $302 = $301&255;
       $303 = (2304 + (($302*5)|0)|0);
       $304 = (($303) + 2|0);
       HEAP8[$304>>0] = 50;
       $305 = $spriteIndex3;
       $306 = $305&255;
       $307 = (2304 + (($306*5)|0)|0);
       $308 = (($307) + 3|0);
       HEAP8[$308>>0] = 1;
      }
     }
    }
   }
   $309 = $spriteIndex3;
   $310 = $309&255;
   $311 = (2304 + (($310*5)|0)|0);
   $312 = (($311) + 4|0);
   HEAP8[$312>>0] = -1;
   $313 = $posX;
   $314 = $313&255;
   $315 = $spriteIndex3;
   $316 = $315&255;
   $317 = (2304 + (($316*5)|0)|0);
   HEAP8[$317>>0] = $314;
   $318 = $posY;
   $319 = $318&255;
   $320 = $spriteIndex3;
   $321 = $320&255;
   $322 = (2304 + (($321*5)|0)|0);
   $323 = (($322) + 1|0);
   HEAP8[$323>>0] = $319;
  }
 }
 STACKTOP = sp;return;
}
function _Mission_Set($mission) {
 $mission = $mission|0;
 var $0 = 0, $1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $mission;
 $1 = $0;
 HEAP32[36904>>2] = $1;
 STACKTOP = sp;return;
}
function _Mission_Fail($reason) {
 $reason = $reason|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $reason;
 $1 = $0;
 _UI_DisplayMessage($1,0);
 HEAP32[((36904 + 8|0))>>2] = 36320;
 $2 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $3 = $2 & -2;
 HEAP8[((36904 + 16|0))>>0]=$3&255;HEAP8[((36904 + 16|0))+1>>0]=$3>>8;
 STACKTOP = sp;return;
}
function _Mission_Complete() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 HEAP32[((36904 + 8|0))>>2] = 36304;
 return;
}
function _Mission_Advance() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAP32[36904>>2]|0;
 $1 = ($0|0)==(36472|0);
 if ($1) {
  _Mission_Set(36608);
  _Game_Reset();
  return;
 }
 $2 = HEAP32[36904>>2]|0;
 $3 = ($2|0)==(36608|0);
 if ($3) {
  _Mission_Set(36792);
  _Game_Reset();
 } else {
  _Game_SetState(1);
 }
 return;
}
function _Mission_Wait() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $time = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = (_Mission_GetParameterU8()|0);
 $time = $0;
 $1 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $2 = $1 & 1;
 $3 = $2&1;
 do {
  if ($3) {
   $5 = HEAP8[36944>>0]|0;
   $6 = $5&255;
   $7 = ($6|0)==(0);
   if (!($7)) {
    $8 = HEAP8[36944>>0]|0;
    $9 = (($8) + -1)<<24>>24;
    HEAP8[36944>>0] = $9;
    break;
   }
   STACKTOP = sp;return;
  } else {
   $4 = $time;
   HEAP8[36944>>0] = $4;
  }
 } while(0);
 _Mission_Yield();
 STACKTOP = sp;return;
}
function _Mission_DisplayMessage($offset) {
 $offset = $offset|0;
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $param = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $offset;
 $1 = (_Mission_GetParameterU8()|0);
 $param = $1;
 $2 = $param;
 $3 = $2&255;
 $4 = (36248 + ($3<<2)|0);
 $5 = HEAP32[$4>>2]|0;
 $6 = $5;
 $7 = $0;
 _UI_DisplayMessage($6,$7);
 STACKTOP = sp;return;
}
function _Mission_SpawnCar() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0;
 var $8 = 0, $9 = 0, $aiType = 0, $angle = 0, $car = 0, $x = 0, $y = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = (_Mission_GetParameterU8()|0);
 $aiType = $0;
 $1 = (_Mission_GetParameterU8()|0);
 $2 = $1&255;
 $3 = $2&65535;
 $4 = $3 << 8;
 $5 = $4&65535;
 $x = $5;
 $6 = (_Mission_GetParameterU8()|0);
 $7 = $6&255;
 $8 = $7&65535;
 $9 = $8 << 8;
 $10 = $9&65535;
 $y = $10;
 $11 = (_Mission_GetParameterU8()|0);
 $angle = $11;
 $12 = $aiType;
 $13 = $x;
 $14 = $y;
 $15 = $angle;
 $16 = (_Car_Spawn($12,$13,$14,$15)|0);
 $car = $16;
 $17 = $car;
 $18 = ($17|0)!=(0|0);
 if (!($18)) {
  STACKTOP = sp;return;
 }
 $19 = (_Mission_GetParameterU8()|0);
 $20 = $car;
 $21 = (($20) + 24|0);
 HEAP8[$21>>0] = $19;
 STACKTOP = sp;return;
}
function _Mission_SetWanted() {
 var $0 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (_Mission_GetParameterU8()|0);
 HEAP8[29104>>0] = $0;
 _UI_UpdateWanted();
 return;
}
function _Mission_SetFlag() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $flag = 0;
 var $set = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = (_Mission_GetParameterU8()|0);
 $flag = $0;
 $1 = (_Mission_GetParameterU8()|0);
 $set = $1;
 $2 = $set;
 $3 = ($2<<24>>24)!=(0);
 if ($3) {
  $4 = $flag;
  $5 = $4&255;
  $6 = 1 << $5;
  $7 = HEAP16[((36904 + 16|0))>>1]|0;
  $8 = $7&65535;
  $9 = $8 | $6;
  $10 = $9&65535;
  HEAP16[((36904 + 16|0))>>1] = $10;
  STACKTOP = sp;return;
 } else {
  $11 = $flag;
  $12 = $11&255;
  $13 = 1 << $12;
  $14 = $13 ^ -1;
  $15 = HEAP16[((36904 + 16|0))>>1]|0;
  $16 = $15&65535;
  $17 = $16 & $14;
  $18 = $17&65535;
  HEAP16[((36904 + 16|0))>>1] = $18;
  STACKTOP = sp;return;
 }
}
function _Mission_PedCarToXY() {
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0;
 var $26 = 0, $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0;
 var $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0;
 var $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0;
 var $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0;
 var $99 = 0, $carType = 0, $colorMask = 0, $toX = 0, $toY = 0, $x = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = (_Mission_GetParameterU8()|0);
 $carType = $0;
 $1 = (_Mission_GetParameterU8()|0);
 $2 = $1&255;
 $3 = $2&65535;
 $4 = $3 << 8;
 $5 = (($4) + 128)|0;
 $6 = $5&65535;
 $toX = $6;
 $7 = (_Mission_GetParameterU8()|0);
 $8 = $7&255;
 $9 = $8&65535;
 $10 = $9 << 8;
 $11 = (($10) + 128)|0;
 $12 = $11&65535;
 $toY = $12;
 $13 = (_Mission_GetParameterU8()|0);
 $colorMask = $13;
 $14 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $15 = $14 & 1;
 $16 = $15&1;
 if (!($16)) {
  $x = 0;
  while(1) {
   $17 = $x;
   $18 = $17&255;
   $19 = ($18|0)<(5);
   if (!($19)) {
    break;
   }
   $20 = $x;
   $21 = $20&255;
   $22 = (29160 + (($21*30)|0)|0);
   $23 = (($22) + 29|0);
   $24 = HEAP8[$23>>0]|0;
   $25 = $24&255;
   $26 = $carType;
   $27 = $26&255;
   $28 = ($25|0)==($27|0);
   if ($28) {
    label = 5;
    break;
   }
   $30 = $x;
   $31 = (($30) + 1)<<24>>24;
   $x = $31;
  }
  if ((label|0) == 5) {
   $29 = $x;
   HEAP8[36928>>0] = $29;
  }
  $32 = $x;
  $33 = $32&255;
  $34 = ($33|0)==(5);
  if ($34) {
   STACKTOP = sp;return;
  }
  $35 = HEAP8[36928>>0]|0;
  $36 = $35&255;
  $37 = (29160 + (($36*30)|0)|0);
  $38 = HEAP16[$37>>1]|0;
  $39 = $38&65535;
  $40 = $toX;
  $41 = $40&65535;
  $42 = ($39|0)<($41|0);
  if ($42) {
   $43 = HEAP8[36928>>0]|0;
   $44 = $43&255;
   $45 = (29160 + (($44*30)|0)|0);
   $46 = HEAP16[$45>>1]|0;
   $47 = $46&65535;
   $48 = (($47) + 128)|0;
   $49 = $48&65535;
   $50 = HEAP8[36928>>0]|0;
   $51 = $50&255;
   $52 = (29160 + (($51*30)|0)|0);
   $53 = (($52) + 2|0);
   $54 = HEAP16[$53>>1]|0;
   $55 = $colorMask;
   $56 = (_Ped_Spawn(2,$49,$54,0,$55)|0);
   HEAP32[36936>>2] = $56;
  } else {
   $57 = HEAP8[36928>>0]|0;
   $58 = $57&255;
   $59 = (29160 + (($58*30)|0)|0);
   $60 = HEAP16[$59>>1]|0;
   $61 = $60&65535;
   $62 = (($61) - 128)|0;
   $63 = $62&65535;
   $64 = HEAP8[36928>>0]|0;
   $65 = $64&255;
   $66 = (29160 + (($65*30)|0)|0);
   $67 = (($66) + 2|0);
   $68 = HEAP16[$67>>1]|0;
   $69 = $colorMask;
   $70 = (_Ped_Spawn(2,$63,$68,2,$69)|0);
   HEAP32[36936>>2] = $70;
  }
 }
 $71 = HEAP32[36936>>2]|0;
 $72 = (($71) + 7|0);
 $73 = HEAP8[$72>>0]|0;
 $74 = $73 & 3;
 $75 = $74&255;
 $76 = ($75|0)==(2);
 if ($76) {
  $77 = HEAP32[36936>>2]|0;
  $78 = (($77) + 2|0);
  $79 = HEAP16[$78>>1]|0;
  $80 = $79&65535;
  $81 = $toX;
  $82 = $81&65535;
  $83 = ($80|0)<=($82|0);
  if ($83) {
   label = 18;
  } else {
   label = 16;
  }
 } else {
  label = 16;
 }
 if ((label|0) == 16) {
  $84 = HEAP32[36936>>2]|0;
  $85 = (($84) + 7|0);
  $86 = HEAP8[$85>>0]|0;
  $87 = $86 & 3;
  $88 = $87&255;
  $89 = ($88|0)==(0);
  if ($89) {
   $90 = HEAP32[36936>>2]|0;
   $91 = (($90) + 2|0);
   $92 = HEAP16[$91>>1]|0;
   $93 = $92&65535;
   $94 = $toX;
   $95 = $94&65535;
   $96 = ($93|0)>=($95|0);
   if ($96) {
    label = 18;
   }
  }
 }
 if ((label|0) == 18) {
  $97 = HEAP32[36936>>2]|0;
  $98 = (($97) + 4|0);
  $99 = HEAP16[$98>>1]|0;
  $100 = $99&65535;
  $101 = $toY;
  $102 = $101&65535;
  $103 = ($100|0)<($102|0);
  if ($103) {
   $104 = HEAP32[36936>>2]|0;
   $105 = (($104) + 7|0);
   $106 = HEAP8[$105>>0]|0;
   $107 = $106 & -4;
   $108 = $107 | 1;
   HEAP8[$105>>0] = $108;
  } else {
   $109 = HEAP32[36936>>2]|0;
   $110 = (($109) + 7|0);
   $111 = HEAP8[$110>>0]|0;
   $112 = $111 & -4;
   $113 = $112 | 3;
   HEAP8[$110>>0] = $113;
  }
 }
 $114 = HEAP32[36936>>2]|0;
 $115 = (($114) + 7|0);
 $116 = HEAP8[$115>>0]|0;
 $117 = $116 & 3;
 $118 = $117&255;
 $119 = ($118|0)==(3);
 if ($119) {
  $120 = HEAP32[36936>>2]|0;
  $121 = (($120) + 4|0);
  $122 = HEAP16[$121>>1]|0;
  $123 = $122&65535;
  $124 = $toY;
  $125 = $124&65535;
  $126 = ($123|0)<=($125|0);
  if (!($126)) {
   label = 24;
  }
 } else {
  label = 24;
 }
 do {
  if ((label|0) == 24) {
   $127 = HEAP32[36936>>2]|0;
   $128 = (($127) + 7|0);
   $129 = HEAP8[$128>>0]|0;
   $130 = $129 & 3;
   $131 = $130&255;
   $132 = ($131|0)==(1);
   if ($132) {
    $133 = HEAP32[36936>>2]|0;
    $134 = (($133) + 4|0);
    $135 = HEAP16[$134>>1]|0;
    $136 = $135&65535;
    $137 = $toY;
    $138 = $137&65535;
    $139 = ($136|0)>=($138|0);
    if ($139) {
     break;
    }
   }
   _Mission_Yield();
   STACKTOP = sp;return;
  }
 } while(0);
 $140 = HEAP32[36936>>2]|0;
 HEAP8[$140>>0] = 0;
 STACKTOP = sp;return;
}
function _Mission_PedXYToCar() {
 var $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0, $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0;
 var $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0;
 var $134 = 0, $135 = 0, $136 = 0, $137 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0, $27 = 0, $28 = 0;
 var $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0;
 var $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0;
 var $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0;
 var $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $carType = 0, $colorMask = 0;
 var $fromX = 0, $fromY = 0, $x = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = (_Mission_GetParameterU8()|0);
 $1 = $0&255;
 $2 = $1&65535;
 $3 = $2 << 8;
 $4 = (($3) + 128)|0;
 $5 = $4&65535;
 $fromX = $5;
 $6 = (_Mission_GetParameterU8()|0);
 $7 = $6&255;
 $8 = $7&65535;
 $9 = $8 << 8;
 $10 = (($9) + 128)|0;
 $11 = $10&65535;
 $fromY = $11;
 $12 = (_Mission_GetParameterU8()|0);
 $carType = $12;
 $13 = (_Mission_GetParameterU8()|0);
 $colorMask = $13;
 $14 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $15 = $14 & 1;
 $16 = $15&1;
 do {
  if (!($16)) {
   $x = 0;
   while(1) {
    $17 = $x;
    $18 = $17&255;
    $19 = ($18|0)<(5);
    if (!($19)) {
     break;
    }
    $20 = $x;
    $21 = $20&255;
    $22 = (29160 + (($21*30)|0)|0);
    $23 = (($22) + 29|0);
    $24 = HEAP8[$23>>0]|0;
    $25 = $24&255;
    $26 = $carType;
    $27 = $26&255;
    $28 = ($25|0)==($27|0);
    if ($28) {
     label = 5;
     break;
    }
    $30 = $x;
    $31 = (($30) + 1)<<24>>24;
    $x = $31;
   }
   if ((label|0) == 5) {
    $29 = $x;
    HEAP8[36928>>0] = $29;
   }
   $32 = $x;
   $33 = $32&255;
   $34 = ($33|0)==(5);
   if (!($34)) {
    $35 = $fromX;
    $36 = $fromY;
    $37 = HEAP8[36928>>0]|0;
    $38 = $37&255;
    $39 = (29160 + (($38*30)|0)|0);
    $40 = (($39) + 2|0);
    $41 = HEAP16[$40>>1]|0;
    $42 = $41&65535;
    $43 = $fromY;
    $44 = $43&65535;
    $45 = ($42|0)<($44|0);
    $46 = $45 ? 3 : 1;
    $47 = $46&255;
    $48 = $colorMask;
    $49 = (_Ped_Spawn(2,$35,$36,$47,$48)|0);
    HEAP32[36936>>2] = $49;
    break;
   }
   STACKTOP = sp;return;
  }
 } while(0);
 $50 = HEAP32[36936>>2]|0;
 $51 = (($50) + 7|0);
 $52 = HEAP8[$51>>0]|0;
 $53 = $52 & 3;
 $54 = $53&255;
 $55 = ($54|0)==(3);
 if ($55) {
  $56 = HEAP32[36936>>2]|0;
  $57 = (($56) + 4|0);
  $58 = HEAP16[$57>>1]|0;
  $59 = $58&65535;
  $60 = HEAP8[36928>>0]|0;
  $61 = $60&255;
  $62 = (29160 + (($61*30)|0)|0);
  $63 = (($62) + 2|0);
  $64 = HEAP16[$63>>1]|0;
  $65 = $64&65535;
  $66 = ($59|0)<=($65|0);
  if ($66) {
   label = 15;
  } else {
   label = 13;
  }
 } else {
  label = 13;
 }
 if ((label|0) == 13) {
  $67 = HEAP32[36936>>2]|0;
  $68 = (($67) + 7|0);
  $69 = HEAP8[$68>>0]|0;
  $70 = $69 & 3;
  $71 = $70&255;
  $72 = ($71|0)==(1);
  if ($72) {
   $73 = HEAP32[36936>>2]|0;
   $74 = (($73) + 4|0);
   $75 = HEAP16[$74>>1]|0;
   $76 = $75&65535;
   $77 = HEAP8[36928>>0]|0;
   $78 = $77&255;
   $79 = (29160 + (($78*30)|0)|0);
   $80 = (($79) + 2|0);
   $81 = HEAP16[$80>>1]|0;
   $82 = $81&65535;
   $83 = ($76|0)>=($82|0);
   if ($83) {
    label = 15;
   }
  }
 }
 if ((label|0) == 15) {
  $84 = HEAP32[36936>>2]|0;
  $85 = (($84) + 2|0);
  $86 = HEAP16[$85>>1]|0;
  $87 = $86&65535;
  $88 = HEAP8[36928>>0]|0;
  $89 = $88&255;
  $90 = (29160 + (($89*30)|0)|0);
  $91 = HEAP16[$90>>1]|0;
  $92 = $91&65535;
  $93 = ($87|0)<($92|0);
  if ($93) {
   $94 = HEAP32[36936>>2]|0;
   $95 = (($94) + 7|0);
   $96 = HEAP8[$95>>0]|0;
   $97 = $96 & -4;
   HEAP8[$95>>0] = $97;
  } else {
   $98 = HEAP32[36936>>2]|0;
   $99 = (($98) + 7|0);
   $100 = HEAP8[$99>>0]|0;
   $101 = $100 & -4;
   $102 = $101 | 2;
   HEAP8[$99>>0] = $102;
  }
 }
 $103 = HEAP32[36936>>2]|0;
 $104 = (($103) + 7|0);
 $105 = HEAP8[$104>>0]|0;
 $106 = $105 & 3;
 $107 = $106&255;
 $108 = ($107|0)==(2);
 if ($108) {
  $109 = HEAP32[36936>>2]|0;
  $110 = (($109) + 2|0);
  $111 = HEAP16[$110>>1]|0;
  $112 = $111&65535;
  $113 = HEAP8[36928>>0]|0;
  $114 = $113&255;
  $115 = (29160 + (($114*30)|0)|0);
  $116 = HEAP16[$115>>1]|0;
  $117 = $116&65535;
  $118 = (($117) + 128)|0;
  $119 = ($112|0)<=($118|0);
  if (!($119)) {
   label = 21;
  }
 } else {
  label = 21;
 }
 do {
  if ((label|0) == 21) {
   $120 = HEAP32[36936>>2]|0;
   $121 = (($120) + 7|0);
   $122 = HEAP8[$121>>0]|0;
   $123 = $122 & 3;
   $124 = $123&255;
   $125 = ($124|0)==(0);
   if ($125) {
    $126 = HEAP32[36936>>2]|0;
    $127 = (($126) + 2|0);
    $128 = HEAP16[$127>>1]|0;
    $129 = $128&65535;
    $130 = HEAP8[36928>>0]|0;
    $131 = $130&255;
    $132 = (29160 + (($131*30)|0)|0);
    $133 = HEAP16[$132>>1]|0;
    $134 = $133&65535;
    $135 = (($134) - 128)|0;
    $136 = ($129|0)>=($135|0);
    if ($136) {
     break;
    }
   }
   _Mission_Yield();
   STACKTOP = sp;return;
  }
 } while(0);
 $137 = HEAP32[36936>>2]|0;
 HEAP8[$137>>0] = 0;
 STACKTOP = sp;return;
}
function _Mission_ObjectiveXY() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $posX = 0, $posY = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $1 = $0 & -9;
 $2 = $1 | 8;
 HEAP8[((36904 + 16|0))>>0]=$2&255;HEAP8[((36904 + 16|0))+1>>0]=$2>>8;
 $3 = (_Mission_GetParameterU8()|0);
 HEAP8[((36904 + 12|0))>>0] = $3;
 $4 = (_Mission_GetParameterU8()|0);
 HEAP8[((36904 + 13|0))>>0] = $4;
 $5 = HEAP8[29120>>0]|0;
 $6 = $5&255;
 $7 = ($6|0)<(5);
 if (!($7)) {
  STACKTOP = sp;return;
 }
 $8 = HEAP8[29120>>0]|0;
 $9 = $8&255;
 $10 = (29160 + (($9*30)|0)|0);
 $11 = HEAP16[$10>>1]|0;
 $12 = $11&65535;
 $13 = $12 >> 8;
 $14 = $13&65535;
 $posX = $14;
 $15 = HEAP8[29120>>0]|0;
 $16 = $15&255;
 $17 = (29160 + (($16*30)|0)|0);
 $18 = (($17) + 2|0);
 $19 = HEAP16[$18>>1]|0;
 $20 = $19&65535;
 $21 = $20 >> 8;
 $22 = $21&65535;
 $posY = $22;
 $23 = $posX;
 $24 = $23&65535;
 $25 = HEAP8[((36904 + 12|0))>>0]|0;
 $26 = $25&255;
 $27 = (($26) - 1)|0;
 $28 = ($24|0)>=($27|0);
 if ($28) {
  $29 = $posY;
  $30 = $29&65535;
  $31 = HEAP8[((36904 + 13|0))>>0]|0;
  $32 = $31&255;
  $33 = (($32) - 1)|0;
  $34 = ($30|0)>=($33|0);
  if ($34) {
   $35 = $posX;
   $36 = $35&65535;
   $37 = HEAP8[((36904 + 12|0))>>0]|0;
   $38 = $37&255;
   $39 = (($38) + 1)|0;
   $40 = ($36|0)<=($39|0);
   if ($40) {
    $41 = $posY;
    $42 = $41&65535;
    $43 = HEAP8[((36904 + 13|0))>>0]|0;
    $44 = $43&255;
    $45 = (($44) + 1)|0;
    $46 = ($42|0)<=($45|0);
    if ($46) {
     $47 = HEAP8[((36904 + 12|0))>>0]|0;
     $48 = HEAP8[((36904 + 13|0))>>0]|0;
     _World_ResetCustomTile($47,$48);
     $49 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
     $50 = $49 & -9;
     HEAP8[((36904 + 16|0))>>0]=$50&255;HEAP8[((36904 + 16|0))+1>>0]=$50>>8;
     STACKTOP = sp;return;
    }
   }
  }
 }
 _Mission_Yield();
 STACKTOP = sp;return;
}
function _Mission_ObjectiveCar() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $1 = $0 & 1;
 $2 = $1&1;
 if (!($2)) {
  $3 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
  $4 = $3 & -5;
  $5 = $4 | 4;
  HEAP8[((36904 + 16|0))>>0]=$5&255;HEAP8[((36904 + 16|0))+1>>0]=$5>>8;
 }
 $6 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $7 = ($6&65535) >>> 2;
 $8 = $7 & 1;
 $9 = $8&1;
 if ($9) {
  _Mission_Yield();
  return;
 } else {
  return;
 }
}
function _Mission_SetMarkerXY() {
 var $0 = 0, $1 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (_Mission_GetParameterU8()|0);
 HEAP8[((36904 + 12|0))>>0] = $0;
 $1 = (_Mission_GetParameterU8()|0);
 HEAP8[((36904 + 13|0))>>0] = $1;
 return;
}
function _Mission_SetAIType() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = (_Mission_GetParameterU8()|0);
 $1 = HEAP8[((36904 + 14|0))>>0]|0;
 $2 = $0 & 15;
 $3 = ($2 << 4)&255;
 $4 = $1 & 15;
 $5 = $4 | $3;
 HEAP8[((36904 + 14|0))>>0] = $5;
 return;
}
function _Mission_WaitForMissionCar() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $1 = $0 & 1;
 $2 = $1&1;
 do {
  if ($2) {
   $6 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
   $7 = ($6&65535) >>> 4;
   $8 = $7 & 1;
   $9 = $8&1;
   if ($9) {
    break;
   }
   return;
  } else {
   $3 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
   $4 = $3 & -17;
   $5 = $4 | 16;
   HEAP8[((36904 + 16|0))>>0]=$5&255;HEAP8[((36904 + 16|0))+1>>0]=$5>>8;
  }
 } while(0);
 _Mission_Yield();
 return;
}
function _Mission_RaceForLaps() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $lapLimit = 0, $x = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = (_Mission_GetParameterU8()|0);
 $lapLimit = $0;
 $x = 0;
 while(1) {
  $1 = $x;
  $2 = $1&255;
  $3 = ($2|0)<(5);
  if (!($3)) {
   label = 15;
   break;
  }
  $4 = $x;
  $5 = $4&255;
  $6 = (29160 + (($5*30)|0)|0);
  $7 = (($6) + 29|0);
  $8 = HEAP8[$7>>0]|0;
  $9 = $8&255;
  $10 = ($9|0)==(1);
  if ($10) {
   $11 = $x;
   $12 = $11&255;
   $13 = (29160 + (($12*30)|0)|0);
   $14 = (($13) + 25|0);
   $15 = (($14) + 1|0);
   $16 = HEAP8[$15>>0]|0;
   $17 = ($16&255) >>> 4;
   $18 = $17&255;
   $19 = $lapLimit;
   $20 = $19&255;
   $21 = ($18|0)>($20|0);
   if ($21) {
    label = 5;
    break;
   }
  } else {
   $22 = $x;
   $23 = $22&255;
   $24 = (29160 + (($23*30)|0)|0);
   $25 = (($24) + 29|0);
   $26 = HEAP8[$25>>0]|0;
   $27 = $26&255;
   $28 = ($27|0)==(6);
   if ($28) {
    label = 9;
   } else {
    $29 = $x;
    $30 = $29&255;
    $31 = (29160 + (($30*30)|0)|0);
    $32 = (($31) + 29|0);
    $33 = HEAP8[$32>>0]|0;
    $34 = $33&255;
    $35 = ($34|0)==(3);
    if ($35) {
     label = 9;
    }
   }
   if ((label|0) == 9) {
    label = 0;
    $36 = $x;
    $37 = $36&255;
    $38 = (29160 + (($37*30)|0)|0);
    $39 = (($38) + 25|0);
    $40 = (($39) + 1|0);
    $41 = HEAP8[$40>>0]|0;
    $42 = ($41&255) >>> 4;
    $43 = $42&255;
    $44 = $lapLimit;
    $45 = $44&255;
    $46 = ($43|0)>($45|0);
    if ($46) {
     label = 10;
     break;
    }
   }
  }
  $50 = $x;
  $51 = (($50) + 1)<<24>>24;
  $x = $51;
 }
 if ((label|0) == 5) {
  STACKTOP = sp;return;
 }
 else if ((label|0) == 10) {
  $47 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
  $48 = $47 & -3;
  $49 = $48 | 2;
  HEAP8[((36904 + 16|0))>>0]=$49&255;HEAP8[((36904 + 16|0))+1>>0]=$49>>8;
  _Mission_Fail(36880);
  STACKTOP = sp;return;
 }
 else if ((label|0) == 15) {
  _Mission_Yield();
  STACKTOP = sp;return;
 }
}
function _Mission_PlaySound() {
 var $0 = 0, $1 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $patch = 0, $retrig = 0, $volume = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = (_Mission_GetParameterU8()|0);
 $patch = $0;
 $1 = (_Mission_GetParameterU8()|0);
 $volume = $1;
 $2 = (_Mission_GetParameterU8()|0);
 $3 = ($2<<24>>24)!=(0);
 $4 = $3&1;
 $retrig = $4;
 $5 = $patch;
 $6 = $volume;
 $7 = $retrig;
 $8 = $7&1;
 _TriggerFx($5,$6,$8);
 STACKTOP = sp;return;
}
function _UI_PrintInt($x,$y,$val,$zeropad) {
 $x = $x|0;
 $y = $y|0;
 $val = $val|0;
 $zeropad = $zeropad|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $c = 0, $i = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $x;
 $1 = $y;
 $2 = $val;
 $4 = $zeropad&1;
 $3 = $4;
 $i = 0;
 while(1) {
  $5 = $i;
  $6 = $5&255;
  $7 = ($6|0)<(5);
  if (!($7)) {
   label = 12;
   break;
  }
  $8 = $2;
  $9 = (($8>>>0) % 10)&-1;
  $10 = $9&255;
  $c = $10;
  $11 = $2;
  $12 = ($11>>>0)>(0);
  if ($12) {
   label = 5;
  } else {
   $13 = $i;
   $14 = $13&255;
   $15 = ($14|0)==(0);
   if ($15) {
    label = 5;
   } else {
    $25 = $3;
    $26 = $25&1;
    if (!($26)) {
     break;
    }
    $27 = $0;
    $28 = (($27) + -1)|0;
    $0 = $28;
    $29 = $27&255;
    $30 = $1;
    $31 = $30&255;
    _SetFont($29,$31,16);
   }
  }
  if ((label|0) == 5) {
   label = 0;
   $16 = $0;
   $17 = (($16) + -1)|0;
   $0 = $17;
   $18 = $16&255;
   $19 = $1;
   $20 = $19&255;
   $21 = $c;
   $22 = $21&255;
   $23 = (($22) + 16)|0;
   $24 = $23&255;
   _SetFont($18,$20,$24);
  }
  $32 = $2;
  $33 = (($32>>>0) / 10)&-1;
  $2 = $33;
  $34 = $i;
  $35 = (($34) + 1)<<24>>24;
  $i = $35;
 }
 if ((label|0) == 12) {
  STACKTOP = sp;return;
 }
 STACKTOP = sp;return;
}
function _UI_UpdateTimer() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $minutes = 0, $seconds = 0;
 var label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $minutes = 0;
 $0 = HEAP16[36968>>1]|0;
 $1 = $0&255;
 $seconds = $1;
 while(1) {
  $2 = $seconds;
  $3 = $2&255;
  $4 = ($3|0)>=(60);
  if (!($4)) {
   break;
  }
  $5 = $seconds;
  $6 = $5&255;
  $7 = (($6) - 60)|0;
  $8 = $7&255;
  $seconds = $8;
  $9 = $minutes;
  $10 = (($9) + 1)<<24>>24;
  $minutes = $10;
 }
 $11 = $seconds;
 $12 = $11&255;
 _UI_PrintInt(15,29,$12,0);
 $13 = $seconds;
 $14 = $13&255;
 $15 = ($14|0)<(10);
 if ($15) {
  _PrintChar(14,29,48);
 }
 _PrintChar(13,29,58);
 $16 = $minutes;
 $17 = $16&255;
 _UI_PrintInt(12,29,$17,0);
 STACKTOP = sp;return;
}
function _UI_Init() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $x = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 HEAP8[36952>>0] = 0;
 HEAP8[36960>>0] = 0;
 _Fill(0,30,28,2,25);
 $0 = (_World_GetHUDLayout()|0);
 _DrawMap2(0,29,$0);
 $x = 0;
 while(1) {
  $1 = $x;
  $2 = $1 << 24 >> 24;
  $3 = ($2|0)<(8);
  if (!($3)) {
   break;
  }
  $4 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
  $5 = ($4&65535) >>> 6;
  $6 = $5 & 1;
  $7 = $6&1;
  if ($7) {
   label = 5;
  } else {
   $8 = HEAP8[29128>>0]|0;
   $9 = $8&255;
   $10 = ($9|0)<(5);
   if ($10) {
    label = 5;
   } else {
    $15 = $x;
    $16 = $15 << 24 >> 24;
    $17 = (1 + ($16))|0;
    $18 = $17&255;
    _SetTile($18,29,90);
   }
  }
  if ((label|0) == 5) {
   label = 0;
   $11 = $x;
   $12 = $11 << 24 >> 24;
   $13 = (1 + ($12))|0;
   $14 = $13&255;
   _SetTile($14,29,93);
  }
  $19 = $x;
  $20 = (($19) + 1)<<24>>24;
  $x = $20;
 }
 $x = 0;
 while(1) {
  $21 = $x;
  $22 = $21 << 24 >> 24;
  $23 = ($22|0)<(4);
  if (!($23)) {
   break;
  }
  $24 = $x;
  $25 = $24 << 24 >> 24;
  $26 = (23 + ($25))|0;
  $27 = $26&255;
  _SetTile($27,29,91);
  $28 = $x;
  $29 = (($28) + 1)<<24>>24;
  $x = $29;
 }
 HEAP16[36968>>1] = 0;
 STACKTOP = sp;return;
}
function _UI_Update() {
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $19 = 0, $2 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0, $24 = 0, $25 = 0, $26 = 0;
 var $27 = 0, $28 = 0, $29 = 0, $3 = 0, $30 = 0, $31 = 0, $32 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0;
 var $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0, $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0;
 var $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0, $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0;
 var $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0, $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0;
 var $flash = 0, $flash1 = 0, $tileType = 0, $x = 0, $x2 = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = HEAP8[36952>>0]|0;
 $1 = $0&255;
 $2 = ($1|0)>(0);
 if ($2) {
  $3 = HEAP8[36952>>0]|0;
  $4 = $3&255;
  $5 = $4 & 8;
  $6 = $5&255;
  $flash = $6;
  $7 = HEAP8[36952>>0]|0;
  $8 = (($7) + -1)<<24>>24;
  HEAP8[36952>>0] = $8;
  $9 = $flash;
  $10 = $9&255;
  $11 = HEAP8[36952>>0]|0;
  $12 = $11&255;
  $13 = $12 & 8;
  $14 = ($10|0)!=($13|0);
  if ($14) {
   $tileType = 89;
   $15 = $flash;
   $16 = ($15<<24>>24)!=(0);
   if ($16) {
    $17 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
    $18 = ($17&65535) >>> 6;
    $19 = $18 & 1;
    $20 = $19&1;
    if ($20) {
     label = 6;
    } else {
     $21 = HEAP8[29128>>0]|0;
     $22 = $21&255;
     $23 = ($22|0)<(5);
     if ($23) {
      label = 6;
     } else {
      $tileType = 90;
     }
    }
    if ((label|0) == 6) {
     $tileType = 93;
    }
   }
   $x = 0;
   while(1) {
    $24 = $x;
    $25 = $24 << 24 >> 24;
    $26 = ($25|0)<(8);
    if (!($26)) {
     break;
    }
    $27 = $x;
    $28 = $27 << 24 >> 24;
    $29 = (1 + ($28))|0;
    $30 = $29&255;
    $31 = $flash;
    $32 = $31&255;
    $33 = ($32|0)!=(0);
    if ($33) {
     $34 = $x;
     $35 = $34 << 24 >> 24;
     $36 = HEAP8[29112>>0]|0;
     $37 = $36&255;
     $38 = ($35|0)<($37|0);
     if ($38) {
      $39 = $tileType;
      $40 = $39&255;
      $41 = $40;
     } else {
      label = 14;
     }
    } else {
     label = 14;
    }
    if ((label|0) == 14) {
     label = 0;
     $41 = 89;
    }
    _SetTile($30,29,$41);
    $42 = $x;
    $43 = (($42) + 1)<<24>>24;
    $x = $43;
   }
  }
 }
 $44 = HEAP8[36960>>0]|0;
 $45 = $44&255;
 $46 = ($45|0)>(0);
 if ($46) {
  $47 = HEAP8[36960>>0]|0;
  $48 = $47&255;
  $49 = $48 & 8;
  $50 = $49&255;
  $flash1 = $50;
  $51 = HEAP8[36960>>0]|0;
  $52 = (($51) + -1)<<24>>24;
  HEAP8[36960>>0] = $52;
  $53 = $flash1;
  $54 = $53&255;
  $55 = HEAP8[36960>>0]|0;
  $56 = $55&255;
  $57 = $56 & 8;
  $58 = ($54|0)!=($57|0);
  if ($58) {
   $x2 = 0;
   while(1) {
    $59 = $x2;
    $60 = $59 << 24 >> 24;
    $61 = ($60|0)<(4);
    if (!($61)) {
     break;
    }
    $62 = $x2;
    $63 = $62 << 24 >> 24;
    $64 = (23 + ($63))|0;
    $65 = $64&255;
    $66 = $flash1;
    $67 = $66&255;
    $68 = ($67|0)!=(0);
    if ($68) {
     $69 = $x2;
     $70 = $69 << 24 >> 24;
     $71 = HEAP8[29104>>0]|0;
     $72 = $71&255;
     $73 = ($70|0)<($72|0);
     $75 = $73;
    } else {
     $75 = 0;
    }
    $74 = $75 ? 92 : 91;
    _SetTile($65,29,$74);
    $76 = $x2;
    $77 = (($76) + 1)<<24>>24;
    $x2 = $77;
   }
  }
 }
 $78 = HEAP8[29112>>0]|0;
 $79 = $78&255;
 $80 = ($79|0)>(0);
 if (!($80)) {
  STACKTOP = sp;return;
 }
 $81 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $82 = ($81&65535) >>> 1;
 $83 = $82 & 1;
 $84 = $83&1;
 if ($84) {
  STACKTOP = sp;return;
 }
 $85 = HEAP8[35920>>0]|0;
 $86 = $85&255;
 $87 = $86 & 63;
 $88 = ($87|0)==(0);
 if (!($88)) {
  STACKTOP = sp;return;
 }
 $89 = HEAPU8[((36904 + 16|0))>>0]|(HEAPU8[((36904 + 16|0))+1>>0]<<8);
 $90 = ($89&65535) >>> 5;
 $91 = $90 & 1;
 $92 = $91&1;
 if ($92) {
  $93 = HEAP16[36968>>1]|0;
  $94 = (($93) + 1)<<16>>16;
  HEAP16[36968>>1] = $94;
  _UI_UpdateTimer();
 } else {
  $95 = HEAP16[36968>>1]|0;
  $96 = $95&65535;
  $97 = ($96|0)>(0);
  if ($97) {
   $98 = HEAP16[36968>>1]|0;
   $99 = (($98) + -1)<<16>>16;
   HEAP16[36968>>1] = $99;
   _UI_UpdateTimer();
  }
 }
 STACKTOP = sp;return;
}
function _UI_UpdateWanted() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 HEAP8[36960>>0] = 60;
 return;
}
function _UI_UpdateHealth() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 HEAP8[36952>>0] = 60;
 return;
}
function _UI_ClearMessage() {
 var label = 0, sp = 0;
 sp = STACKTOP;
 _Fill(0,30,28,1,25);
 return;
}
function _UI_DisplayMessage($message,$offset) {
 $message = $message|0;
 $offset = $offset|0;
 var $0 = 0, $1 = 0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $2 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0, $x = 0, label = 0, sp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16|0; if ((STACKTOP|0) >= (STACK_MAX|0)) abort();
 $0 = $message;
 $1 = $offset;
 $2 = $1;
 $3 = $2 << 24 >> 24;
 $4 = (30 + ($3))|0;
 _Fill(0,$4,28,1,25);
 $5 = $0;
 $6 = (_strlen(($5|0))|0);
 $7 = $6 >>> 1;
 $8 = (14 - ($7))|0;
 $9 = $8&255;
 $x = $9;
 $10 = $x;
 $11 = $10 << 24 >> 24;
 $12 = $1;
 $13 = $12 << 24 >> 24;
 $14 = (30 + ($13))|0;
 $15 = $0;
 _Print($11,$14,$15);
 STACKTOP = sp;return;
}
function _malloc($bytes) {
 $bytes = $bytes|0;
 var $$lcssa = 0, $$lcssa110 = 0, $$lcssa112 = 0, $$lcssa115 = 0, $$lcssa116 = 0, $$lcssa117 = 0, $$lcssa118 = 0, $$lcssa120 = 0, $$lcssa123 = 0, $$lcssa125 = 0, $$lcssa127 = 0, $$lcssa130 = 0, $$lcssa132 = 0, $$lcssa134 = 0, $$lcssa137 = 0, $$pre = 0, $$pre$i = 0, $$pre$i$i = 0, $$pre$i23$i = 0, $$pre$i25 = 0;
 var $$pre$phi$i$iZ2D = 0, $$pre$phi$i24$iZ2D = 0, $$pre$phi$i26Z2D = 0, $$pre$phi$iZ2D = 0, $$pre$phi59$i$iZ2D = 0, $$pre$phiZ2D = 0, $$pre105 = 0, $$pre58$i$i = 0, $$rsize$0$i = 0, $$rsize$3$i = 0, $$rsize$3$i$lcssa = 0, $$sum = 0, $$sum$i$i = 0, $$sum$i$i$i = 0, $$sum$i12$i = 0, $$sum$i13$i = 0, $$sum$i16$i = 0, $$sum$i19$i = 0, $$sum$i2338 = 0, $$sum$i32 = 0;
 var $$sum$i39 = 0, $$sum1 = 0, $$sum1$i = 0, $$sum1$i$i = 0, $$sum1$i14$i = 0, $$sum1$i20$i = 0, $$sum1$i24 = 0, $$sum10 = 0, $$sum10$i = 0, $$sum10$i$i = 0, $$sum10$pre$i$i = 0, $$sum102$i = 0, $$sum103$i = 0, $$sum104$i = 0, $$sum105$i = 0, $$sum106$i = 0, $$sum107$i = 0, $$sum108$i = 0, $$sum109$i = 0, $$sum11$i = 0;
 var $$sum11$i$i = 0, $$sum11$i22$i = 0, $$sum110$i = 0, $$sum111$i = 0, $$sum1112 = 0, $$sum112$i = 0, $$sum113$i = 0, $$sum114$i = 0, $$sum115$i = 0, $$sum12$i = 0, $$sum12$i$i = 0, $$sum13$i = 0, $$sum13$i$i = 0, $$sum14$i$i = 0, $$sum14$pre$i = 0, $$sum15$i = 0, $$sum15$i$i = 0, $$sum16$i = 0, $$sum16$i$i = 0, $$sum17$i = 0;
 var $$sum17$i$i = 0, $$sum18$i = 0, $$sum1819$i$i = 0, $$sum2 = 0, $$sum2$i = 0, $$sum2$i$i = 0, $$sum2$i$i$i = 0, $$sum2$i15$i = 0, $$sum2$i17$i = 0, $$sum2$i21$i = 0, $$sum2$pre$i = 0, $$sum20$i$i = 0, $$sum21$i$i = 0, $$sum22$i$i = 0, $$sum23$i$i = 0, $$sum24$i$i = 0, $$sum25$i$i = 0, $$sum26$pre$i$i = 0, $$sum27$i$i = 0, $$sum28$i$i = 0;
 var $$sum29$i$i = 0, $$sum3$i = 0, $$sum3$i$i = 0, $$sum3$i27 = 0, $$sum30$i$i = 0, $$sum3132$i$i = 0, $$sum34$i$i = 0, $$sum3536$i$i = 0, $$sum3738$i$i = 0, $$sum39$i$i = 0, $$sum4 = 0, $$sum4$i = 0, $$sum4$i28 = 0, $$sum40$i$i = 0, $$sum41$i$i = 0, $$sum42$i$i = 0, $$sum5$i = 0, $$sum5$i$i = 0, $$sum56 = 0, $$sum6$i = 0;
 var $$sum67$i$i = 0, $$sum7$i = 0, $$sum8$i = 0, $$sum8$pre = 0, $$sum9 = 0, $$sum9$i = 0, $$sum9$i$i = 0, $$tsize$1$i = 0, $$v$0$i = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $1000 = 0, $1001 = 0, $1002 = 0, $1003 = 0, $1004 = 0, $1005 = 0, $1006 = 0;
 var $1007 = 0, $1008 = 0, $1009 = 0, $101 = 0, $1010 = 0, $1011 = 0, $1012 = 0, $1013 = 0, $1014 = 0, $1015 = 0, $1016 = 0, $1017 = 0, $1018 = 0, $1019 = 0, $102 = 0, $1020 = 0, $1021 = 0, $1022 = 0, $1023 = 0, $1024 = 0;
 var $1025 = 0, $1026 = 0, $1027 = 0, $1028 = 0, $1029 = 0, $103 = 0, $1030 = 0, $1031 = 0, $1032 = 0, $1033 = 0, $1034 = 0, $1035 = 0, $1036 = 0, $1037 = 0, $1038 = 0, $1039 = 0, $104 = 0, $1040 = 0, $1041 = 0, $1042 = 0;
 var $1043 = 0, $1044 = 0, $1045 = 0, $1046 = 0, $1047 = 0, $1048 = 0, $1049 = 0, $105 = 0, $1050 = 0, $1051 = 0, $1052 = 0, $1053 = 0, $1054 = 0, $1055 = 0, $1056 = 0, $1057 = 0, $1058 = 0, $1059 = 0, $106 = 0, $1060 = 0;
 var $1061 = 0, $1062 = 0, $1063 = 0, $1064 = 0, $1065 = 0, $1066 = 0, $1067 = 0, $1068 = 0, $1069 = 0, $107 = 0, $1070 = 0, $1071 = 0, $1072 = 0, $1073 = 0, $1074 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0;
 var $112 = 0, $113 = 0, $114 = 0, $115 = 0, $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0, $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0;
 var $130 = 0, $131 = 0, $132 = 0, $133 = 0, $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0, $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0;
 var $149 = 0, $15 = 0, $150 = 0, $151 = 0, $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0, $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0;
 var $167 = 0, $168 = 0, $169 = 0, $17 = 0, $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0, $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0;
 var $185 = 0, $186 = 0, $187 = 0, $188 = 0, $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0, $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0;
 var $202 = 0, $203 = 0, $204 = 0, $205 = 0, $206 = 0, $207 = 0, $208 = 0, $209 = 0, $21 = 0, $210 = 0, $211 = 0, $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0, $219 = 0, $22 = 0;
 var $220 = 0, $221 = 0, $222 = 0, $223 = 0, $224 = 0, $225 = 0, $226 = 0, $227 = 0, $228 = 0, $229 = 0, $23 = 0, $230 = 0, $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0;
 var $239 = 0, $24 = 0, $240 = 0, $241 = 0, $242 = 0, $243 = 0, $244 = 0, $245 = 0, $246 = 0, $247 = 0, $248 = 0, $249 = 0, $25 = 0, $250 = 0, $251 = 0, $252 = 0, $253 = 0, $254 = 0, $255 = 0, $256 = 0;
 var $257 = 0, $258 = 0, $259 = 0, $26 = 0, $260 = 0, $261 = 0, $262 = 0, $263 = 0, $264 = 0, $265 = 0, $266 = 0, $267 = 0, $268 = 0, $269 = 0, $27 = 0, $270 = 0, $271 = 0, $272 = 0, $273 = 0, $274 = 0;
 var $275 = 0, $276 = 0, $277 = 0, $278 = 0, $279 = 0, $28 = 0, $280 = 0, $281 = 0, $282 = 0, $283 = 0, $284 = 0, $285 = 0, $286 = 0, $287 = 0, $288 = 0, $289 = 0, $29 = 0, $290 = 0, $291 = 0, $292 = 0;
 var $293 = 0, $294 = 0, $295 = 0, $296 = 0, $297 = 0, $298 = 0, $299 = 0, $3 = 0, $30 = 0, $300 = 0, $301 = 0, $302 = 0, $303 = 0, $304 = 0, $305 = 0, $306 = 0, $307 = 0, $308 = 0, $309 = 0, $31 = 0;
 var $310 = 0, $311 = 0, $312 = 0, $313 = 0, $314 = 0, $315 = 0, $316 = 0, $317 = 0, $318 = 0, $319 = 0, $32 = 0, $320 = 0, $321 = 0, $322 = 0, $323 = 0, $324 = 0, $325 = 0, $326 = 0, $327 = 0, $328 = 0;
 var $329 = 0, $33 = 0, $330 = 0, $331 = 0, $332 = 0, $333 = 0, $334 = 0, $335 = 0, $336 = 0, $337 = 0, $338 = 0, $339 = 0, $34 = 0, $340 = 0, $341 = 0, $342 = 0, $343 = 0, $344 = 0, $345 = 0, $346 = 0;
 var $347 = 0, $348 = 0, $349 = 0, $35 = 0, $350 = 0, $351 = 0, $352 = 0, $353 = 0, $354 = 0, $355 = 0, $356 = 0, $357 = 0, $358 = 0, $359 = 0, $36 = 0, $360 = 0, $361 = 0, $362 = 0, $363 = 0, $364 = 0;
 var $365 = 0, $366 = 0, $367 = 0, $368 = 0, $369 = 0, $37 = 0, $370 = 0, $371 = 0, $372 = 0, $373 = 0, $374 = 0, $375 = 0, $376 = 0, $377 = 0, $378 = 0, $379 = 0, $38 = 0, $380 = 0, $381 = 0, $382 = 0;
 var $383 = 0, $384 = 0, $385 = 0, $386 = 0, $387 = 0, $388 = 0, $389 = 0, $39 = 0, $390 = 0, $391 = 0, $392 = 0, $393 = 0, $394 = 0, $395 = 0, $396 = 0, $397 = 0, $398 = 0, $399 = 0, $4 = 0, $40 = 0;
 var $400 = 0, $401 = 0, $402 = 0, $403 = 0, $404 = 0, $405 = 0, $406 = 0, $407 = 0, $408 = 0, $409 = 0, $41 = 0, $410 = 0, $411 = 0, $412 = 0, $413 = 0, $414 = 0, $415 = 0, $416 = 0, $417 = 0, $418 = 0;
 var $419 = 0, $42 = 0, $420 = 0, $421 = 0, $422 = 0, $423 = 0, $424 = 0, $425 = 0, $426 = 0, $427 = 0, $428 = 0, $429 = 0, $43 = 0, $430 = 0, $431 = 0, $432 = 0, $433 = 0, $434 = 0, $435 = 0, $436 = 0;
 var $437 = 0, $438 = 0, $439 = 0, $44 = 0, $440 = 0, $441 = 0, $442 = 0, $443 = 0, $444 = 0, $445 = 0, $446 = 0, $447 = 0, $448 = 0, $449 = 0, $45 = 0, $450 = 0, $451 = 0, $452 = 0, $453 = 0, $454 = 0;
 var $455 = 0, $456 = 0, $457 = 0, $458 = 0, $459 = 0, $46 = 0, $460 = 0, $461 = 0, $462 = 0, $463 = 0, $464 = 0, $465 = 0, $466 = 0, $467 = 0, $468 = 0, $469 = 0, $47 = 0, $470 = 0, $471 = 0, $472 = 0;
 var $473 = 0, $474 = 0, $475 = 0, $476 = 0, $477 = 0, $478 = 0, $479 = 0, $48 = 0, $480 = 0, $481 = 0, $482 = 0, $483 = 0, $484 = 0, $485 = 0, $486 = 0, $487 = 0, $488 = 0, $489 = 0, $49 = 0, $490 = 0;
 var $491 = 0, $492 = 0, $493 = 0, $494 = 0, $495 = 0, $496 = 0, $497 = 0, $498 = 0, $499 = 0, $5 = 0, $50 = 0, $500 = 0, $501 = 0, $502 = 0, $503 = 0, $504 = 0, $505 = 0, $506 = 0, $507 = 0, $508 = 0;
 var $509 = 0, $51 = 0, $510 = 0, $511 = 0, $512 = 0, $513 = 0, $514 = 0, $515 = 0, $516 = 0, $517 = 0, $518 = 0, $519 = 0, $52 = 0, $520 = 0, $521 = 0, $522 = 0, $523 = 0, $524 = 0, $525 = 0, $526 = 0;
 var $527 = 0, $528 = 0, $529 = 0, $53 = 0, $530 = 0, $531 = 0, $532 = 0, $533 = 0, $534 = 0, $535 = 0, $536 = 0, $537 = 0, $538 = 0, $539 = 0, $54 = 0, $540 = 0, $541 = 0, $542 = 0, $543 = 0, $544 = 0;
 var $545 = 0, $546 = 0, $547 = 0, $548 = 0, $549 = 0, $55 = 0, $550 = 0, $551 = 0, $552 = 0, $553 = 0, $554 = 0, $555 = 0, $556 = 0, $557 = 0, $558 = 0, $559 = 0, $56 = 0, $560 = 0, $561 = 0, $562 = 0;
 var $563 = 0, $564 = 0, $565 = 0, $566 = 0, $567 = 0, $568 = 0, $569 = 0, $57 = 0, $570 = 0, $571 = 0, $572 = 0, $573 = 0, $574 = 0, $575 = 0, $576 = 0, $577 = 0, $578 = 0, $579 = 0, $58 = 0, $580 = 0;
 var $581 = 0, $582 = 0, $583 = 0, $584 = 0, $585 = 0, $586 = 0, $587 = 0, $588 = 0, $589 = 0, $59 = 0, $590 = 0, $591 = 0, $592 = 0, $593 = 0, $594 = 0, $595 = 0, $596 = 0, $597 = 0, $598 = 0, $599 = 0;
 var $6 = 0, $60 = 0, $600 = 0, $601 = 0, $602 = 0, $603 = 0, $604 = 0, $605 = 0, $606 = 0, $607 = 0, $608 = 0, $609 = 0, $61 = 0, $610 = 0, $611 = 0, $612 = 0, $613 = 0, $614 = 0, $615 = 0, $616 = 0;
 var $617 = 0, $618 = 0, $619 = 0, $62 = 0, $620 = 0, $621 = 0, $622 = 0, $623 = 0, $624 = 0, $625 = 0, $626 = 0, $627 = 0, $628 = 0, $629 = 0, $63 = 0, $630 = 0, $631 = 0, $632 = 0, $633 = 0, $634 = 0;
 var $635 = 0, $636 = 0, $637 = 0, $638 = 0, $639 = 0, $64 = 0, $640 = 0, $641 = 0, $642 = 0, $643 = 0, $644 = 0, $645 = 0, $646 = 0, $647 = 0, $648 = 0, $649 = 0, $65 = 0, $650 = 0, $651 = 0, $652 = 0;
 var $653 = 0, $654 = 0, $655 = 0, $656 = 0, $657 = 0, $658 = 0, $659 = 0, $66 = 0, $660 = 0, $661 = 0, $662 = 0, $663 = 0, $664 = 0, $665 = 0, $666 = 0, $667 = 0, $668 = 0, $669 = 0, $67 = 0, $670 = 0;
 var $671 = 0, $672 = 0, $673 = 0, $674 = 0, $675 = 0, $676 = 0, $677 = 0, $678 = 0, $679 = 0, $68 = 0, $680 = 0, $681 = 0, $682 = 0, $683 = 0, $684 = 0, $685 = 0, $686 = 0, $687 = 0, $688 = 0, $689 = 0;
 var $69 = 0, $690 = 0, $691 = 0, $692 = 0, $693 = 0, $694 = 0, $695 = 0, $696 = 0, $697 = 0, $698 = 0, $699 = 0, $7 = 0, $70 = 0, $700 = 0, $701 = 0, $702 = 0, $703 = 0, $704 = 0, $705 = 0, $706 = 0;
 var $707 = 0, $708 = 0, $709 = 0, $71 = 0, $710 = 0, $711 = 0, $712 = 0, $713 = 0, $714 = 0, $715 = 0, $716 = 0, $717 = 0, $718 = 0, $719 = 0, $72 = 0, $720 = 0, $721 = 0, $722 = 0, $723 = 0, $724 = 0;
 var $725 = 0, $726 = 0, $727 = 0, $728 = 0, $729 = 0, $73 = 0, $730 = 0, $731 = 0, $732 = 0, $733 = 0, $734 = 0, $735 = 0, $736 = 0, $737 = 0, $738 = 0, $739 = 0, $74 = 0, $740 = 0, $741 = 0, $742 = 0;
 var $743 = 0, $744 = 0, $745 = 0, $746 = 0, $747 = 0, $748 = 0, $749 = 0, $75 = 0, $750 = 0, $751 = 0, $752 = 0, $753 = 0, $754 = 0, $755 = 0, $756 = 0, $757 = 0, $758 = 0, $759 = 0, $76 = 0, $760 = 0;
 var $761 = 0, $762 = 0, $763 = 0, $764 = 0, $765 = 0, $766 = 0, $767 = 0, $768 = 0, $769 = 0, $77 = 0, $770 = 0, $771 = 0, $772 = 0, $773 = 0, $774 = 0, $775 = 0, $776 = 0, $777 = 0, $778 = 0, $779 = 0;
 var $78 = 0, $780 = 0, $781 = 0, $782 = 0, $783 = 0, $784 = 0, $785 = 0, $786 = 0, $787 = 0, $788 = 0, $789 = 0, $79 = 0, $790 = 0, $791 = 0, $792 = 0, $793 = 0, $794 = 0, $795 = 0, $796 = 0, $797 = 0;
 var $798 = 0, $799 = 0, $8 = 0, $80 = 0, $800 = 0, $801 = 0, $802 = 0, $803 = 0, $804 = 0, $805 = 0, $806 = 0, $807 = 0, $808 = 0, $809 = 0, $81 = 0, $810 = 0, $811 = 0, $812 = 0, $813 = 0, $814 = 0;
 var $815 = 0, $816 = 0, $817 = 0, $818 = 0, $819 = 0, $82 = 0, $820 = 0, $821 = 0, $822 = 0, $823 = 0, $824 = 0, $825 = 0, $826 = 0, $827 = 0, $828 = 0, $829 = 0, $83 = 0, $830 = 0, $831 = 0, $832 = 0;
 var $833 = 0, $834 = 0, $835 = 0, $836 = 0, $837 = 0, $838 = 0, $839 = 0, $84 = 0, $840 = 0, $841 = 0, $842 = 0, $843 = 0, $844 = 0, $845 = 0, $846 = 0, $847 = 0, $848 = 0, $849 = 0, $85 = 0, $850 = 0;
 var $851 = 0, $852 = 0, $853 = 0, $854 = 0, $855 = 0, $856 = 0, $857 = 0, $858 = 0, $859 = 0, $86 = 0, $860 = 0, $861 = 0, $862 = 0, $863 = 0, $864 = 0, $865 = 0, $866 = 0, $867 = 0, $868 = 0, $869 = 0;
 var $87 = 0, $870 = 0, $871 = 0, $872 = 0, $873 = 0, $874 = 0, $875 = 0, $876 = 0, $877 = 0, $878 = 0, $879 = 0, $88 = 0, $880 = 0, $881 = 0, $882 = 0, $883 = 0, $884 = 0, $885 = 0, $886 = 0, $887 = 0;
 var $888 = 0, $889 = 0, $89 = 0, $890 = 0, $891 = 0, $892 = 0, $893 = 0, $894 = 0, $895 = 0, $896 = 0, $897 = 0, $898 = 0, $899 = 0, $9 = 0, $90 = 0, $900 = 0, $901 = 0, $902 = 0, $903 = 0, $904 = 0;
 var $905 = 0, $906 = 0, $907 = 0, $908 = 0, $909 = 0, $91 = 0, $910 = 0, $911 = 0, $912 = 0, $913 = 0, $914 = 0, $915 = 0, $916 = 0, $917 = 0, $918 = 0, $919 = 0, $92 = 0, $920 = 0, $921 = 0, $922 = 0;
 var $923 = 0, $924 = 0, $925 = 0, $926 = 0, $927 = 0, $928 = 0, $929 = 0, $93 = 0, $930 = 0, $931 = 0, $932 = 0, $933 = 0, $934 = 0, $935 = 0, $936 = 0, $937 = 0, $938 = 0, $939 = 0, $94 = 0, $940 = 0;
 var $941 = 0, $942 = 0, $943 = 0, $944 = 0, $945 = 0, $946 = 0, $947 = 0, $948 = 0, $949 = 0, $95 = 0, $950 = 0, $951 = 0, $952 = 0, $953 = 0, $954 = 0, $955 = 0, $956 = 0, $957 = 0, $958 = 0, $959 = 0;
 var $96 = 0, $960 = 0, $961 = 0, $962 = 0, $963 = 0, $964 = 0, $965 = 0, $966 = 0, $967 = 0, $968 = 0, $969 = 0, $97 = 0, $970 = 0, $971 = 0, $972 = 0, $973 = 0, $974 = 0, $975 = 0, $976 = 0, $977 = 0;
 var $978 = 0, $979 = 0, $98 = 0, $980 = 0, $981 = 0, $982 = 0, $983 = 0, $984 = 0, $985 = 0, $986 = 0, $987 = 0, $988 = 0, $989 = 0, $99 = 0, $990 = 0, $991 = 0, $992 = 0, $993 = 0, $994 = 0, $995 = 0;
 var $996 = 0, $997 = 0, $998 = 0, $999 = 0, $F$0$i$i = 0, $F1$0$i = 0, $F4$0 = 0, $F4$0$i$i = 0, $F5$0$i = 0, $I1$0$c$i$i = 0, $I1$0$i$i = 0, $I7$0$i = 0, $I7$0$i$i = 0, $K12$029$i = 0, $K2$015$i$i = 0, $K8$053$i$i = 0, $R$0$i = 0, $R$0$i$be = 0, $R$0$i$i = 0, $R$0$i$i$be = 0;
 var $R$0$i$i$lcssa = 0, $R$0$i$i$ph = 0, $R$0$i$lcssa = 0, $R$0$i$ph = 0, $R$0$i18 = 0, $R$0$i18$be = 0, $R$0$i18$lcssa = 0, $R$0$i18$ph = 0, $R$1$i = 0, $R$1$i$i = 0, $R$1$i20 = 0, $RP$0$i = 0, $RP$0$i$be = 0, $RP$0$i$i = 0, $RP$0$i$i$be = 0, $RP$0$i$i$lcssa = 0, $RP$0$i$i$ph = 0, $RP$0$i$lcssa = 0, $RP$0$i$ph = 0, $RP$0$i17 = 0;
 var $RP$0$i17$be = 0, $RP$0$i17$lcssa = 0, $RP$0$i17$ph = 0, $T$0$lcssa$i = 0, $T$0$lcssa$i$i = 0, $T$0$lcssa$i26$i = 0, $T$014$i$i = 0, $T$014$i$i$lcssa = 0, $T$028$i = 0, $T$028$i$lcssa = 0, $T$052$i$i = 0, $T$052$i$i$lcssa = 0, $br$0$i = 0, $br$030$i = 0, $cond$i = 0, $cond$i$i = 0, $cond$i21 = 0, $exitcond$i$i = 0, $i$02$i$i = 0, $idx$0$i = 0;
 var $mem$0 = 0, $nb$0 = 0, $oldfirst$0$i$i = 0, $or$cond$i = 0, $or$cond$i$i = 0, $or$cond$i27$i = 0, $or$cond$i29 = 0, $or$cond1$i = 0, $or$cond19$i = 0, $or$cond2$i = 0, $or$cond24$i = 0, $or$cond3$i = 0, $or$cond4$i = 0, $or$cond47$i = 0, $or$cond5$i = 0, $or$cond6$i = 0, $or$cond8$i = 0, $qsize$0$i$i = 0, $rsize$0$i = 0, $rsize$0$i$lcssa = 0;
 var $rsize$0$i15 = 0, $rsize$1$i = 0, $rsize$2$i = 0, $rsize$2$i$ph = 0, $rsize$3$lcssa$i = 0, $rsize$331$i = 0, $rst$0$i = 0, $rst$1$i = 0, $sizebits$0$i = 0, $sp$0$i$i = 0, $sp$0$i$i$i = 0, $sp$0$i$i$lcssa = 0, $sp$074$i = 0, $sp$074$i$lcssa = 0, $sp$173$i = 0, $sp$173$i$lcssa = 0, $ssize$0$i = 0, $ssize$1$i = 0, $ssize$129$i = 0, $ssize$2$i = 0;
 var $t$0$i = 0, $t$0$i14 = 0, $t$1$i = 0, $t$1$i$ph = 0, $t$2$ph$i = 0, $t$2$v$3$i = 0, $t$2$v$3$i$lcssa = 0, $t$230$i = 0, $t$230$i$be = 0, $tbase$245$i = 0, $tsize$03141$i = 0, $tsize$1$i = 0, $tsize$244$i = 0, $v$0$i = 0, $v$0$i$lcssa = 0, $v$0$i16 = 0, $v$1$i = 0, $v$2$i = 0, $v$2$i$ph = 0, $v$3$lcssa$i = 0;
 var $v$332$i = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($bytes>>>0)<(245);
 do {
  if ($0) {
   $1 = ($bytes>>>0)<(11);
   if ($1) {
    $5 = 16;
   } else {
    $2 = (($bytes) + 11)|0;
    $3 = $2 & -8;
    $5 = $3;
   }
   $4 = $5 >>> 3;
   $6 = HEAP32[36976>>2]|0;
   $7 = $6 >>> $4;
   $8 = $7 & 3;
   $9 = ($8|0)==(0);
   if (!($9)) {
    $10 = $7 & 1;
    $11 = $10 ^ 1;
    $12 = (($11) + ($4))|0;
    $13 = $12 << 1;
    $14 = ((36976 + ($13<<2)|0) + 40|0);
    $$sum10 = (($13) + 2)|0;
    $15 = ((36976 + ($$sum10<<2)|0) + 40|0);
    $16 = HEAP32[$15>>2]|0;
    $17 = (($16) + 8|0);
    $18 = HEAP32[$17>>2]|0;
    $19 = ($14|0)==($18|0);
    do {
     if ($19) {
      $20 = 1 << $12;
      $21 = $20 ^ -1;
      $22 = $6 & $21;
      HEAP32[36976>>2] = $22;
     } else {
      $23 = HEAP32[((36976 + 16|0))>>2]|0;
      $24 = ($18>>>0)<($23>>>0);
      if ($24) {
       _abort();
       // unreachable;
      }
      $25 = (($18) + 12|0);
      $26 = HEAP32[$25>>2]|0;
      $27 = ($26|0)==($16|0);
      if ($27) {
       HEAP32[$25>>2] = $14;
       HEAP32[$15>>2] = $18;
       break;
      } else {
       _abort();
       // unreachable;
      }
     }
    } while(0);
    $28 = $12 << 3;
    $29 = $28 | 3;
    $30 = (($16) + 4|0);
    HEAP32[$30>>2] = $29;
    $$sum1112 = $28 | 4;
    $31 = (($16) + ($$sum1112)|0);
    $32 = HEAP32[$31>>2]|0;
    $33 = $32 | 1;
    HEAP32[$31>>2] = $33;
    $mem$0 = $17;
    return ($mem$0|0);
   }
   $34 = HEAP32[((36976 + 8|0))>>2]|0;
   $35 = ($5>>>0)>($34>>>0);
   if ($35) {
    $36 = ($7|0)==(0);
    if (!($36)) {
     $37 = $7 << $4;
     $38 = 2 << $4;
     $39 = (0 - ($38))|0;
     $40 = $38 | $39;
     $41 = $37 & $40;
     $42 = (0 - ($41))|0;
     $43 = $41 & $42;
     $44 = (($43) + -1)|0;
     $45 = $44 >>> 12;
     $46 = $45 & 16;
     $47 = $44 >>> $46;
     $48 = $47 >>> 5;
     $49 = $48 & 8;
     $50 = $49 | $46;
     $51 = $47 >>> $49;
     $52 = $51 >>> 2;
     $53 = $52 & 4;
     $54 = $50 | $53;
     $55 = $51 >>> $53;
     $56 = $55 >>> 1;
     $57 = $56 & 2;
     $58 = $54 | $57;
     $59 = $55 >>> $57;
     $60 = $59 >>> 1;
     $61 = $60 & 1;
     $62 = $58 | $61;
     $63 = $59 >>> $61;
     $64 = (($62) + ($63))|0;
     $65 = $64 << 1;
     $66 = ((36976 + ($65<<2)|0) + 40|0);
     $$sum4 = (($65) + 2)|0;
     $67 = ((36976 + ($$sum4<<2)|0) + 40|0);
     $68 = HEAP32[$67>>2]|0;
     $69 = (($68) + 8|0);
     $70 = HEAP32[$69>>2]|0;
     $71 = ($66|0)==($70|0);
     do {
      if ($71) {
       $72 = 1 << $64;
       $73 = $72 ^ -1;
       $74 = $6 & $73;
       HEAP32[36976>>2] = $74;
       $89 = $34;
      } else {
       $75 = HEAP32[((36976 + 16|0))>>2]|0;
       $76 = ($70>>>0)<($75>>>0);
       if ($76) {
        _abort();
        // unreachable;
       }
       $77 = (($70) + 12|0);
       $78 = HEAP32[$77>>2]|0;
       $79 = ($78|0)==($68|0);
       if ($79) {
        HEAP32[$77>>2] = $66;
        HEAP32[$67>>2] = $70;
        $$pre = HEAP32[((36976 + 8|0))>>2]|0;
        $89 = $$pre;
        break;
       } else {
        _abort();
        // unreachable;
       }
      }
     } while(0);
     $80 = $64 << 3;
     $81 = (($80) - ($5))|0;
     $82 = $5 | 3;
     $83 = (($68) + 4|0);
     HEAP32[$83>>2] = $82;
     $84 = (($68) + ($5)|0);
     $85 = $81 | 1;
     $$sum56 = $5 | 4;
     $86 = (($68) + ($$sum56)|0);
     HEAP32[$86>>2] = $85;
     $87 = (($68) + ($80)|0);
     HEAP32[$87>>2] = $81;
     $88 = ($89|0)==(0);
     if (!($88)) {
      $90 = HEAP32[((36976 + 20|0))>>2]|0;
      $91 = $89 >>> 3;
      $92 = $91 << 1;
      $93 = ((36976 + ($92<<2)|0) + 40|0);
      $94 = HEAP32[36976>>2]|0;
      $95 = 1 << $91;
      $96 = $94 & $95;
      $97 = ($96|0)==(0);
      if ($97) {
       $98 = $94 | $95;
       HEAP32[36976>>2] = $98;
       $$sum8$pre = (($92) + 2)|0;
       $$pre105 = ((36976 + ($$sum8$pre<<2)|0) + 40|0);
       $$pre$phiZ2D = $$pre105;$F4$0 = $93;
      } else {
       $$sum9 = (($92) + 2)|0;
       $99 = ((36976 + ($$sum9<<2)|0) + 40|0);
       $100 = HEAP32[$99>>2]|0;
       $101 = HEAP32[((36976 + 16|0))>>2]|0;
       $102 = ($100>>>0)<($101>>>0);
       if ($102) {
        _abort();
        // unreachable;
       } else {
        $$pre$phiZ2D = $99;$F4$0 = $100;
       }
      }
      HEAP32[$$pre$phiZ2D>>2] = $90;
      $103 = (($F4$0) + 12|0);
      HEAP32[$103>>2] = $90;
      $104 = (($90) + 8|0);
      HEAP32[$104>>2] = $F4$0;
      $105 = (($90) + 12|0);
      HEAP32[$105>>2] = $93;
     }
     HEAP32[((36976 + 8|0))>>2] = $81;
     HEAP32[((36976 + 20|0))>>2] = $84;
     $mem$0 = $69;
     return ($mem$0|0);
    }
    $106 = HEAP32[((36976 + 4|0))>>2]|0;
    $107 = ($106|0)==(0);
    if ($107) {
     $nb$0 = $5;
    } else {
     $108 = (0 - ($106))|0;
     $109 = $106 & $108;
     $110 = (($109) + -1)|0;
     $111 = $110 >>> 12;
     $112 = $111 & 16;
     $113 = $110 >>> $112;
     $114 = $113 >>> 5;
     $115 = $114 & 8;
     $116 = $115 | $112;
     $117 = $113 >>> $115;
     $118 = $117 >>> 2;
     $119 = $118 & 4;
     $120 = $116 | $119;
     $121 = $117 >>> $119;
     $122 = $121 >>> 1;
     $123 = $122 & 2;
     $124 = $120 | $123;
     $125 = $121 >>> $123;
     $126 = $125 >>> 1;
     $127 = $126 & 1;
     $128 = $124 | $127;
     $129 = $125 >>> $127;
     $130 = (($128) + ($129))|0;
     $131 = ((36976 + ($130<<2)|0) + 304|0);
     $132 = HEAP32[$131>>2]|0;
     $133 = (($132) + 4|0);
     $134 = HEAP32[$133>>2]|0;
     $135 = $134 & -8;
     $136 = (($135) - ($5))|0;
     $rsize$0$i = $136;$t$0$i = $132;$v$0$i = $132;
     while(1) {
      $137 = (($t$0$i) + 16|0);
      $138 = HEAP32[$137>>2]|0;
      $139 = ($138|0)==(0|0);
      if ($139) {
       $140 = (($t$0$i) + 20|0);
       $141 = HEAP32[$140>>2]|0;
       $142 = ($141|0)==(0|0);
       if ($142) {
        $rsize$0$i$lcssa = $rsize$0$i;$v$0$i$lcssa = $v$0$i;
        break;
       } else {
        $144 = $141;
       }
      } else {
       $144 = $138;
      }
      $143 = (($144) + 4|0);
      $145 = HEAP32[$143>>2]|0;
      $146 = $145 & -8;
      $147 = (($146) - ($5))|0;
      $148 = ($147>>>0)<($rsize$0$i>>>0);
      $$rsize$0$i = $148 ? $147 : $rsize$0$i;
      $$v$0$i = $148 ? $144 : $v$0$i;
      $rsize$0$i = $$rsize$0$i;$t$0$i = $144;$v$0$i = $$v$0$i;
     }
     $149 = HEAP32[((36976 + 16|0))>>2]|0;
     $150 = ($v$0$i$lcssa>>>0)<($149>>>0);
     if ($150) {
      _abort();
      // unreachable;
     }
     $151 = (($v$0$i$lcssa) + ($5)|0);
     $152 = ($v$0$i$lcssa>>>0)<($151>>>0);
     if (!($152)) {
      _abort();
      // unreachable;
     }
     $153 = (($v$0$i$lcssa) + 24|0);
     $154 = HEAP32[$153>>2]|0;
     $155 = (($v$0$i$lcssa) + 12|0);
     $156 = HEAP32[$155>>2]|0;
     $157 = ($156|0)==($v$0$i$lcssa|0);
     do {
      if ($157) {
       $167 = (($v$0$i$lcssa) + 20|0);
       $168 = HEAP32[$167>>2]|0;
       $169 = ($168|0)==(0|0);
       if ($169) {
        $170 = (($v$0$i$lcssa) + 16|0);
        $171 = HEAP32[$170>>2]|0;
        $172 = ($171|0)==(0|0);
        if ($172) {
         $R$1$i = 0;
         break;
        } else {
         $R$0$i$ph = $171;$RP$0$i$ph = $170;
        }
       } else {
        $R$0$i$ph = $168;$RP$0$i$ph = $167;
       }
       $R$0$i = $R$0$i$ph;$RP$0$i = $RP$0$i$ph;
       while(1) {
        $173 = (($R$0$i) + 20|0);
        $174 = HEAP32[$173>>2]|0;
        $175 = ($174|0)==(0|0);
        if ($175) {
         $176 = (($R$0$i) + 16|0);
         $177 = HEAP32[$176>>2]|0;
         $178 = ($177|0)==(0|0);
         if ($178) {
          $R$0$i$lcssa = $R$0$i;$RP$0$i$lcssa = $RP$0$i;
          break;
         } else {
          $R$0$i$be = $177;$RP$0$i$be = $176;
         }
        } else {
         $R$0$i$be = $174;$RP$0$i$be = $173;
        }
        $R$0$i = $R$0$i$be;$RP$0$i = $RP$0$i$be;
       }
       $179 = ($RP$0$i$lcssa>>>0)<($149>>>0);
       if ($179) {
        _abort();
        // unreachable;
       } else {
        HEAP32[$RP$0$i$lcssa>>2] = 0;
        $R$1$i = $R$0$i$lcssa;
        break;
       }
      } else {
       $158 = (($v$0$i$lcssa) + 8|0);
       $159 = HEAP32[$158>>2]|0;
       $160 = ($159>>>0)<($149>>>0);
       if ($160) {
        _abort();
        // unreachable;
       }
       $161 = (($159) + 12|0);
       $162 = HEAP32[$161>>2]|0;
       $163 = ($162|0)==($v$0$i$lcssa|0);
       if (!($163)) {
        _abort();
        // unreachable;
       }
       $164 = (($156) + 8|0);
       $165 = HEAP32[$164>>2]|0;
       $166 = ($165|0)==($v$0$i$lcssa|0);
       if ($166) {
        HEAP32[$161>>2] = $156;
        HEAP32[$164>>2] = $159;
        $R$1$i = $156;
        break;
       } else {
        _abort();
        // unreachable;
       }
      }
     } while(0);
     $180 = ($154|0)==(0|0);
     do {
      if (!($180)) {
       $181 = (($v$0$i$lcssa) + 28|0);
       $182 = HEAP32[$181>>2]|0;
       $183 = ((36976 + ($182<<2)|0) + 304|0);
       $184 = HEAP32[$183>>2]|0;
       $185 = ($v$0$i$lcssa|0)==($184|0);
       if ($185) {
        HEAP32[$183>>2] = $R$1$i;
        $cond$i = ($R$1$i|0)==(0|0);
        if ($cond$i) {
         $186 = 1 << $182;
         $187 = $186 ^ -1;
         $188 = HEAP32[((36976 + 4|0))>>2]|0;
         $189 = $188 & $187;
         HEAP32[((36976 + 4|0))>>2] = $189;
         break;
        }
       } else {
        $190 = HEAP32[((36976 + 16|0))>>2]|0;
        $191 = ($154>>>0)<($190>>>0);
        if ($191) {
         _abort();
         // unreachable;
        }
        $192 = (($154) + 16|0);
        $193 = HEAP32[$192>>2]|0;
        $194 = ($193|0)==($v$0$i$lcssa|0);
        if ($194) {
         HEAP32[$192>>2] = $R$1$i;
        } else {
         $195 = (($154) + 20|0);
         HEAP32[$195>>2] = $R$1$i;
        }
        $196 = ($R$1$i|0)==(0|0);
        if ($196) {
         break;
        }
       }
       $197 = HEAP32[((36976 + 16|0))>>2]|0;
       $198 = ($R$1$i>>>0)<($197>>>0);
       if ($198) {
        _abort();
        // unreachable;
       }
       $199 = (($R$1$i) + 24|0);
       HEAP32[$199>>2] = $154;
       $200 = (($v$0$i$lcssa) + 16|0);
       $201 = HEAP32[$200>>2]|0;
       $202 = ($201|0)==(0|0);
       do {
        if (!($202)) {
         $203 = ($201>>>0)<($197>>>0);
         if ($203) {
          _abort();
          // unreachable;
         } else {
          $204 = (($R$1$i) + 16|0);
          HEAP32[$204>>2] = $201;
          $205 = (($201) + 24|0);
          HEAP32[$205>>2] = $R$1$i;
          break;
         }
        }
       } while(0);
       $206 = (($v$0$i$lcssa) + 20|0);
       $207 = HEAP32[$206>>2]|0;
       $208 = ($207|0)==(0|0);
       if (!($208)) {
        $209 = HEAP32[((36976 + 16|0))>>2]|0;
        $210 = ($207>>>0)<($209>>>0);
        if ($210) {
         _abort();
         // unreachable;
        } else {
         $211 = (($R$1$i) + 20|0);
         HEAP32[$211>>2] = $207;
         $212 = (($207) + 24|0);
         HEAP32[$212>>2] = $R$1$i;
         break;
        }
       }
      }
     } while(0);
     $213 = ($rsize$0$i$lcssa>>>0)<(16);
     if ($213) {
      $214 = (($rsize$0$i$lcssa) + ($5))|0;
      $215 = $214 | 3;
      $216 = (($v$0$i$lcssa) + 4|0);
      HEAP32[$216>>2] = $215;
      $$sum4$i = (($214) + 4)|0;
      $217 = (($v$0$i$lcssa) + ($$sum4$i)|0);
      $218 = HEAP32[$217>>2]|0;
      $219 = $218 | 1;
      HEAP32[$217>>2] = $219;
     } else {
      $220 = $5 | 3;
      $221 = (($v$0$i$lcssa) + 4|0);
      HEAP32[$221>>2] = $220;
      $222 = $rsize$0$i$lcssa | 1;
      $$sum$i39 = $5 | 4;
      $223 = (($v$0$i$lcssa) + ($$sum$i39)|0);
      HEAP32[$223>>2] = $222;
      $$sum1$i = (($rsize$0$i$lcssa) + ($5))|0;
      $224 = (($v$0$i$lcssa) + ($$sum1$i)|0);
      HEAP32[$224>>2] = $rsize$0$i$lcssa;
      $225 = HEAP32[((36976 + 8|0))>>2]|0;
      $226 = ($225|0)==(0);
      if (!($226)) {
       $227 = HEAP32[((36976 + 20|0))>>2]|0;
       $228 = $225 >>> 3;
       $229 = $228 << 1;
       $230 = ((36976 + ($229<<2)|0) + 40|0);
       $231 = HEAP32[36976>>2]|0;
       $232 = 1 << $228;
       $233 = $231 & $232;
       $234 = ($233|0)==(0);
       if ($234) {
        $235 = $231 | $232;
        HEAP32[36976>>2] = $235;
        $$sum2$pre$i = (($229) + 2)|0;
        $$pre$i = ((36976 + ($$sum2$pre$i<<2)|0) + 40|0);
        $$pre$phi$iZ2D = $$pre$i;$F1$0$i = $230;
       } else {
        $$sum3$i = (($229) + 2)|0;
        $236 = ((36976 + ($$sum3$i<<2)|0) + 40|0);
        $237 = HEAP32[$236>>2]|0;
        $238 = HEAP32[((36976 + 16|0))>>2]|0;
        $239 = ($237>>>0)<($238>>>0);
        if ($239) {
         _abort();
         // unreachable;
        } else {
         $$pre$phi$iZ2D = $236;$F1$0$i = $237;
        }
       }
       HEAP32[$$pre$phi$iZ2D>>2] = $227;
       $240 = (($F1$0$i) + 12|0);
       HEAP32[$240>>2] = $227;
       $241 = (($227) + 8|0);
       HEAP32[$241>>2] = $F1$0$i;
       $242 = (($227) + 12|0);
       HEAP32[$242>>2] = $230;
      }
      HEAP32[((36976 + 8|0))>>2] = $rsize$0$i$lcssa;
      HEAP32[((36976 + 20|0))>>2] = $151;
     }
     $243 = (($v$0$i$lcssa) + 8|0);
     $mem$0 = $243;
     return ($mem$0|0);
    }
   } else {
    $nb$0 = $5;
   }
  } else {
   $244 = ($bytes>>>0)>(4294967231);
   if ($244) {
    $nb$0 = -1;
   } else {
    $245 = (($bytes) + 11)|0;
    $246 = $245 & -8;
    $247 = HEAP32[((36976 + 4|0))>>2]|0;
    $248 = ($247|0)==(0);
    if ($248) {
     $nb$0 = $246;
    } else {
     $249 = (0 - ($246))|0;
     $250 = $245 >>> 8;
     $251 = ($250|0)==(0);
     if ($251) {
      $idx$0$i = 0;
     } else {
      $252 = ($246>>>0)>(16777215);
      if ($252) {
       $idx$0$i = 31;
      } else {
       $253 = (($250) + 1048320)|0;
       $254 = $253 >>> 16;
       $255 = $254 & 8;
       $256 = $250 << $255;
       $257 = (($256) + 520192)|0;
       $258 = $257 >>> 16;
       $259 = $258 & 4;
       $260 = $259 | $255;
       $261 = $256 << $259;
       $262 = (($261) + 245760)|0;
       $263 = $262 >>> 16;
       $264 = $263 & 2;
       $265 = $260 | $264;
       $266 = (14 - ($265))|0;
       $267 = $261 << $264;
       $268 = $267 >>> 15;
       $269 = (($266) + ($268))|0;
       $270 = $269 << 1;
       $271 = (($269) + 7)|0;
       $272 = $246 >>> $271;
       $273 = $272 & 1;
       $274 = $273 | $270;
       $idx$0$i = $274;
      }
     }
     $275 = ((36976 + ($idx$0$i<<2)|0) + 304|0);
     $276 = HEAP32[$275>>2]|0;
     $277 = ($276|0)==(0|0);
     if ($277) {
      $rsize$2$i = $249;$t$1$i = 0;$v$2$i = 0;
     } else {
      $278 = ($idx$0$i|0)==(31);
      if ($278) {
       $282 = 0;
      } else {
       $279 = $idx$0$i >>> 1;
       $280 = (25 - ($279))|0;
       $282 = $280;
      }
      $281 = $246 << $282;
      $rsize$0$i15 = $249;$rst$0$i = 0;$sizebits$0$i = $281;$t$0$i14 = $276;$v$0$i16 = 0;
      while(1) {
       $283 = (($t$0$i14) + 4|0);
       $284 = HEAP32[$283>>2]|0;
       $285 = $284 & -8;
       $286 = (($285) - ($246))|0;
       $287 = ($286>>>0)<($rsize$0$i15>>>0);
       if ($287) {
        $288 = ($285|0)==($246|0);
        if ($288) {
         $rsize$2$i$ph = $286;$t$1$i$ph = $t$0$i14;$v$2$i$ph = $t$0$i14;
         break;
        } else {
         $rsize$1$i = $286;$v$1$i = $t$0$i14;
        }
       } else {
        $rsize$1$i = $rsize$0$i15;$v$1$i = $v$0$i16;
       }
       $289 = (($t$0$i14) + 20|0);
       $290 = HEAP32[$289>>2]|0;
       $291 = $sizebits$0$i >>> 31;
       $292 = ((($t$0$i14) + ($291<<2)|0) + 16|0);
       $293 = HEAP32[$292>>2]|0;
       $294 = ($290|0)==(0|0);
       $295 = ($290|0)==($293|0);
       $or$cond19$i = $294 | $295;
       $rst$1$i = $or$cond19$i ? $rst$0$i : $290;
       $296 = ($293|0)==(0|0);
       $297 = $sizebits$0$i << 1;
       if ($296) {
        $rsize$2$i$ph = $rsize$1$i;$t$1$i$ph = $rst$1$i;$v$2$i$ph = $v$1$i;
        break;
       } else {
        $rsize$0$i15 = $rsize$1$i;$rst$0$i = $rst$1$i;$sizebits$0$i = $297;$t$0$i14 = $293;$v$0$i16 = $v$1$i;
       }
      }
      $rsize$2$i = $rsize$2$i$ph;$t$1$i = $t$1$i$ph;$v$2$i = $v$2$i$ph;
     }
     $298 = ($t$1$i|0)==(0|0);
     $299 = ($v$2$i|0)==(0|0);
     $or$cond$i = $298 & $299;
     if ($or$cond$i) {
      $300 = 2 << $idx$0$i;
      $301 = (0 - ($300))|0;
      $302 = $300 | $301;
      $303 = $247 & $302;
      $304 = ($303|0)==(0);
      if ($304) {
       $nb$0 = $246;
       break;
      }
      $305 = (0 - ($303))|0;
      $306 = $303 & $305;
      $307 = (($306) + -1)|0;
      $308 = $307 >>> 12;
      $309 = $308 & 16;
      $310 = $307 >>> $309;
      $311 = $310 >>> 5;
      $312 = $311 & 8;
      $313 = $312 | $309;
      $314 = $310 >>> $312;
      $315 = $314 >>> 2;
      $316 = $315 & 4;
      $317 = $313 | $316;
      $318 = $314 >>> $316;
      $319 = $318 >>> 1;
      $320 = $319 & 2;
      $321 = $317 | $320;
      $322 = $318 >>> $320;
      $323 = $322 >>> 1;
      $324 = $323 & 1;
      $325 = $321 | $324;
      $326 = $322 >>> $324;
      $327 = (($325) + ($326))|0;
      $328 = ((36976 + ($327<<2)|0) + 304|0);
      $329 = HEAP32[$328>>2]|0;
      $t$2$ph$i = $329;
     } else {
      $t$2$ph$i = $t$1$i;
     }
     $330 = ($t$2$ph$i|0)==(0|0);
     if ($330) {
      $rsize$3$lcssa$i = $rsize$2$i;$v$3$lcssa$i = $v$2$i;
     } else {
      $rsize$331$i = $rsize$2$i;$t$230$i = $t$2$ph$i;$v$332$i = $v$2$i;
      while(1) {
       $331 = (($t$230$i) + 4|0);
       $332 = HEAP32[$331>>2]|0;
       $333 = $332 & -8;
       $334 = (($333) - ($246))|0;
       $335 = ($334>>>0)<($rsize$331$i>>>0);
       $$rsize$3$i = $335 ? $334 : $rsize$331$i;
       $t$2$v$3$i = $335 ? $t$230$i : $v$332$i;
       $336 = (($t$230$i) + 16|0);
       $337 = HEAP32[$336>>2]|0;
       $338 = ($337|0)==(0|0);
       if ($338) {
        $339 = (($t$230$i) + 20|0);
        $340 = HEAP32[$339>>2]|0;
        $341 = ($340|0)==(0|0);
        if ($341) {
         $$rsize$3$i$lcssa = $$rsize$3$i;$t$2$v$3$i$lcssa = $t$2$v$3$i;
         break;
        } else {
         $t$230$i$be = $340;
        }
       } else {
        $t$230$i$be = $337;
       }
       $rsize$331$i = $$rsize$3$i;$t$230$i = $t$230$i$be;$v$332$i = $t$2$v$3$i;
      }
      $rsize$3$lcssa$i = $$rsize$3$i$lcssa;$v$3$lcssa$i = $t$2$v$3$i$lcssa;
     }
     $342 = ($v$3$lcssa$i|0)==(0|0);
     if ($342) {
      $nb$0 = $246;
     } else {
      $343 = HEAP32[((36976 + 8|0))>>2]|0;
      $344 = (($343) - ($246))|0;
      $345 = ($rsize$3$lcssa$i>>>0)<($344>>>0);
      if ($345) {
       $346 = HEAP32[((36976 + 16|0))>>2]|0;
       $347 = ($v$3$lcssa$i>>>0)<($346>>>0);
       if ($347) {
        _abort();
        // unreachable;
       }
       $348 = (($v$3$lcssa$i) + ($246)|0);
       $349 = ($v$3$lcssa$i>>>0)<($348>>>0);
       if (!($349)) {
        _abort();
        // unreachable;
       }
       $350 = (($v$3$lcssa$i) + 24|0);
       $351 = HEAP32[$350>>2]|0;
       $352 = (($v$3$lcssa$i) + 12|0);
       $353 = HEAP32[$352>>2]|0;
       $354 = ($353|0)==($v$3$lcssa$i|0);
       do {
        if ($354) {
         $364 = (($v$3$lcssa$i) + 20|0);
         $365 = HEAP32[$364>>2]|0;
         $366 = ($365|0)==(0|0);
         if ($366) {
          $367 = (($v$3$lcssa$i) + 16|0);
          $368 = HEAP32[$367>>2]|0;
          $369 = ($368|0)==(0|0);
          if ($369) {
           $R$1$i20 = 0;
           break;
          } else {
           $R$0$i18$ph = $368;$RP$0$i17$ph = $367;
          }
         } else {
          $R$0$i18$ph = $365;$RP$0$i17$ph = $364;
         }
         $R$0$i18 = $R$0$i18$ph;$RP$0$i17 = $RP$0$i17$ph;
         while(1) {
          $370 = (($R$0$i18) + 20|0);
          $371 = HEAP32[$370>>2]|0;
          $372 = ($371|0)==(0|0);
          if ($372) {
           $373 = (($R$0$i18) + 16|0);
           $374 = HEAP32[$373>>2]|0;
           $375 = ($374|0)==(0|0);
           if ($375) {
            $R$0$i18$lcssa = $R$0$i18;$RP$0$i17$lcssa = $RP$0$i17;
            break;
           } else {
            $R$0$i18$be = $374;$RP$0$i17$be = $373;
           }
          } else {
           $R$0$i18$be = $371;$RP$0$i17$be = $370;
          }
          $R$0$i18 = $R$0$i18$be;$RP$0$i17 = $RP$0$i17$be;
         }
         $376 = ($RP$0$i17$lcssa>>>0)<($346>>>0);
         if ($376) {
          _abort();
          // unreachable;
         } else {
          HEAP32[$RP$0$i17$lcssa>>2] = 0;
          $R$1$i20 = $R$0$i18$lcssa;
          break;
         }
        } else {
         $355 = (($v$3$lcssa$i) + 8|0);
         $356 = HEAP32[$355>>2]|0;
         $357 = ($356>>>0)<($346>>>0);
         if ($357) {
          _abort();
          // unreachable;
         }
         $358 = (($356) + 12|0);
         $359 = HEAP32[$358>>2]|0;
         $360 = ($359|0)==($v$3$lcssa$i|0);
         if (!($360)) {
          _abort();
          // unreachable;
         }
         $361 = (($353) + 8|0);
         $362 = HEAP32[$361>>2]|0;
         $363 = ($362|0)==($v$3$lcssa$i|0);
         if ($363) {
          HEAP32[$358>>2] = $353;
          HEAP32[$361>>2] = $356;
          $R$1$i20 = $353;
          break;
         } else {
          _abort();
          // unreachable;
         }
        }
       } while(0);
       $377 = ($351|0)==(0|0);
       do {
        if (!($377)) {
         $378 = (($v$3$lcssa$i) + 28|0);
         $379 = HEAP32[$378>>2]|0;
         $380 = ((36976 + ($379<<2)|0) + 304|0);
         $381 = HEAP32[$380>>2]|0;
         $382 = ($v$3$lcssa$i|0)==($381|0);
         if ($382) {
          HEAP32[$380>>2] = $R$1$i20;
          $cond$i21 = ($R$1$i20|0)==(0|0);
          if ($cond$i21) {
           $383 = 1 << $379;
           $384 = $383 ^ -1;
           $385 = HEAP32[((36976 + 4|0))>>2]|0;
           $386 = $385 & $384;
           HEAP32[((36976 + 4|0))>>2] = $386;
           break;
          }
         } else {
          $387 = HEAP32[((36976 + 16|0))>>2]|0;
          $388 = ($351>>>0)<($387>>>0);
          if ($388) {
           _abort();
           // unreachable;
          }
          $389 = (($351) + 16|0);
          $390 = HEAP32[$389>>2]|0;
          $391 = ($390|0)==($v$3$lcssa$i|0);
          if ($391) {
           HEAP32[$389>>2] = $R$1$i20;
          } else {
           $392 = (($351) + 20|0);
           HEAP32[$392>>2] = $R$1$i20;
          }
          $393 = ($R$1$i20|0)==(0|0);
          if ($393) {
           break;
          }
         }
         $394 = HEAP32[((36976 + 16|0))>>2]|0;
         $395 = ($R$1$i20>>>0)<($394>>>0);
         if ($395) {
          _abort();
          // unreachable;
         }
         $396 = (($R$1$i20) + 24|0);
         HEAP32[$396>>2] = $351;
         $397 = (($v$3$lcssa$i) + 16|0);
         $398 = HEAP32[$397>>2]|0;
         $399 = ($398|0)==(0|0);
         do {
          if (!($399)) {
           $400 = ($398>>>0)<($394>>>0);
           if ($400) {
            _abort();
            // unreachable;
           } else {
            $401 = (($R$1$i20) + 16|0);
            HEAP32[$401>>2] = $398;
            $402 = (($398) + 24|0);
            HEAP32[$402>>2] = $R$1$i20;
            break;
           }
          }
         } while(0);
         $403 = (($v$3$lcssa$i) + 20|0);
         $404 = HEAP32[$403>>2]|0;
         $405 = ($404|0)==(0|0);
         if (!($405)) {
          $406 = HEAP32[((36976 + 16|0))>>2]|0;
          $407 = ($404>>>0)<($406>>>0);
          if ($407) {
           _abort();
           // unreachable;
          } else {
           $408 = (($R$1$i20) + 20|0);
           HEAP32[$408>>2] = $404;
           $409 = (($404) + 24|0);
           HEAP32[$409>>2] = $R$1$i20;
           break;
          }
         }
        }
       } while(0);
       $410 = ($rsize$3$lcssa$i>>>0)<(16);
       L215: do {
        if ($410) {
         $411 = (($rsize$3$lcssa$i) + ($246))|0;
         $412 = $411 | 3;
         $413 = (($v$3$lcssa$i) + 4|0);
         HEAP32[$413>>2] = $412;
         $$sum18$i = (($411) + 4)|0;
         $414 = (($v$3$lcssa$i) + ($$sum18$i)|0);
         $415 = HEAP32[$414>>2]|0;
         $416 = $415 | 1;
         HEAP32[$414>>2] = $416;
        } else {
         $417 = $246 | 3;
         $418 = (($v$3$lcssa$i) + 4|0);
         HEAP32[$418>>2] = $417;
         $419 = $rsize$3$lcssa$i | 1;
         $$sum$i2338 = $246 | 4;
         $420 = (($v$3$lcssa$i) + ($$sum$i2338)|0);
         HEAP32[$420>>2] = $419;
         $$sum1$i24 = (($rsize$3$lcssa$i) + ($246))|0;
         $421 = (($v$3$lcssa$i) + ($$sum1$i24)|0);
         HEAP32[$421>>2] = $rsize$3$lcssa$i;
         $422 = $rsize$3$lcssa$i >>> 3;
         $423 = ($rsize$3$lcssa$i>>>0)<(256);
         if ($423) {
          $424 = $422 << 1;
          $425 = ((36976 + ($424<<2)|0) + 40|0);
          $426 = HEAP32[36976>>2]|0;
          $427 = 1 << $422;
          $428 = $426 & $427;
          $429 = ($428|0)==(0);
          do {
           if ($429) {
            $430 = $426 | $427;
            HEAP32[36976>>2] = $430;
            $$sum14$pre$i = (($424) + 2)|0;
            $$pre$i25 = ((36976 + ($$sum14$pre$i<<2)|0) + 40|0);
            $$pre$phi$i26Z2D = $$pre$i25;$F5$0$i = $425;
           } else {
            $$sum17$i = (($424) + 2)|0;
            $431 = ((36976 + ($$sum17$i<<2)|0) + 40|0);
            $432 = HEAP32[$431>>2]|0;
            $433 = HEAP32[((36976 + 16|0))>>2]|0;
            $434 = ($432>>>0)<($433>>>0);
            if (!($434)) {
             $$pre$phi$i26Z2D = $431;$F5$0$i = $432;
             break;
            }
            _abort();
            // unreachable;
           }
          } while(0);
          HEAP32[$$pre$phi$i26Z2D>>2] = $348;
          $435 = (($F5$0$i) + 12|0);
          HEAP32[$435>>2] = $348;
          $$sum15$i = (($246) + 8)|0;
          $436 = (($v$3$lcssa$i) + ($$sum15$i)|0);
          HEAP32[$436>>2] = $F5$0$i;
          $$sum16$i = (($246) + 12)|0;
          $437 = (($v$3$lcssa$i) + ($$sum16$i)|0);
          HEAP32[$437>>2] = $425;
          break;
         }
         $438 = $rsize$3$lcssa$i >>> 8;
         $439 = ($438|0)==(0);
         if ($439) {
          $I7$0$i = 0;
         } else {
          $440 = ($rsize$3$lcssa$i>>>0)>(16777215);
          if ($440) {
           $I7$0$i = 31;
          } else {
           $441 = (($438) + 1048320)|0;
           $442 = $441 >>> 16;
           $443 = $442 & 8;
           $444 = $438 << $443;
           $445 = (($444) + 520192)|0;
           $446 = $445 >>> 16;
           $447 = $446 & 4;
           $448 = $447 | $443;
           $449 = $444 << $447;
           $450 = (($449) + 245760)|0;
           $451 = $450 >>> 16;
           $452 = $451 & 2;
           $453 = $448 | $452;
           $454 = (14 - ($453))|0;
           $455 = $449 << $452;
           $456 = $455 >>> 15;
           $457 = (($454) + ($456))|0;
           $458 = $457 << 1;
           $459 = (($457) + 7)|0;
           $460 = $rsize$3$lcssa$i >>> $459;
           $461 = $460 & 1;
           $462 = $461 | $458;
           $I7$0$i = $462;
          }
         }
         $463 = ((36976 + ($I7$0$i<<2)|0) + 304|0);
         $$sum2$i = (($246) + 28)|0;
         $464 = (($v$3$lcssa$i) + ($$sum2$i)|0);
         HEAP32[$464>>2] = $I7$0$i;
         $$sum3$i27 = (($246) + 16)|0;
         $465 = (($v$3$lcssa$i) + ($$sum3$i27)|0);
         $$sum4$i28 = (($246) + 20)|0;
         $466 = (($v$3$lcssa$i) + ($$sum4$i28)|0);
         HEAP32[$466>>2] = 0;
         HEAP32[$465>>2] = 0;
         $467 = HEAP32[((36976 + 4|0))>>2]|0;
         $468 = 1 << $I7$0$i;
         $469 = $467 & $468;
         $470 = ($469|0)==(0);
         if ($470) {
          $471 = $467 | $468;
          HEAP32[((36976 + 4|0))>>2] = $471;
          HEAP32[$463>>2] = $348;
          $$sum5$i = (($246) + 24)|0;
          $472 = (($v$3$lcssa$i) + ($$sum5$i)|0);
          HEAP32[$472>>2] = $463;
          $$sum6$i = (($246) + 12)|0;
          $473 = (($v$3$lcssa$i) + ($$sum6$i)|0);
          HEAP32[$473>>2] = $348;
          $$sum7$i = (($246) + 8)|0;
          $474 = (($v$3$lcssa$i) + ($$sum7$i)|0);
          HEAP32[$474>>2] = $348;
          break;
         }
         $475 = HEAP32[$463>>2]|0;
         $476 = ($I7$0$i|0)==(31);
         if ($476) {
          $484 = 0;
         } else {
          $477 = $I7$0$i >>> 1;
          $478 = (25 - ($477))|0;
          $484 = $478;
         }
         $479 = (($475) + 4|0);
         $480 = HEAP32[$479>>2]|0;
         $481 = $480 & -8;
         $482 = ($481|0)==($rsize$3$lcssa$i|0);
         do {
          if ($482) {
           $T$0$lcssa$i = $475;
          } else {
           $483 = $rsize$3$lcssa$i << $484;
           $K12$029$i = $483;$T$028$i = $475;
           while(1) {
            $491 = $K12$029$i >>> 31;
            $492 = ((($T$028$i) + ($491<<2)|0) + 16|0);
            $487 = HEAP32[$492>>2]|0;
            $493 = ($487|0)==(0|0);
            if ($493) {
             $$lcssa134 = $492;$T$028$i$lcssa = $T$028$i;
             break;
            }
            $485 = $K12$029$i << 1;
            $486 = (($487) + 4|0);
            $488 = HEAP32[$486>>2]|0;
            $489 = $488 & -8;
            $490 = ($489|0)==($rsize$3$lcssa$i|0);
            if ($490) {
             $$lcssa137 = $487;
             label = 163;
             break;
            } else {
             $K12$029$i = $485;$T$028$i = $487;
            }
           }
           if ((label|0) == 163) {
            $T$0$lcssa$i = $$lcssa137;
            break;
           }
           $494 = HEAP32[((36976 + 16|0))>>2]|0;
           $495 = ($$lcssa134>>>0)<($494>>>0);
           if ($495) {
            _abort();
            // unreachable;
           } else {
            HEAP32[$$lcssa134>>2] = $348;
            $$sum11$i = (($246) + 24)|0;
            $496 = (($v$3$lcssa$i) + ($$sum11$i)|0);
            HEAP32[$496>>2] = $T$028$i$lcssa;
            $$sum12$i = (($246) + 12)|0;
            $497 = (($v$3$lcssa$i) + ($$sum12$i)|0);
            HEAP32[$497>>2] = $348;
            $$sum13$i = (($246) + 8)|0;
            $498 = (($v$3$lcssa$i) + ($$sum13$i)|0);
            HEAP32[$498>>2] = $348;
            break L215;
           }
          }
         } while(0);
         $499 = (($T$0$lcssa$i) + 8|0);
         $500 = HEAP32[$499>>2]|0;
         $501 = HEAP32[((36976 + 16|0))>>2]|0;
         $502 = ($T$0$lcssa$i>>>0)>=($501>>>0);
         $503 = ($500>>>0)>=($501>>>0);
         $or$cond24$i = $502 & $503;
         if ($or$cond24$i) {
          $504 = (($500) + 12|0);
          HEAP32[$504>>2] = $348;
          HEAP32[$499>>2] = $348;
          $$sum8$i = (($246) + 8)|0;
          $505 = (($v$3$lcssa$i) + ($$sum8$i)|0);
          HEAP32[$505>>2] = $500;
          $$sum9$i = (($246) + 12)|0;
          $506 = (($v$3$lcssa$i) + ($$sum9$i)|0);
          HEAP32[$506>>2] = $T$0$lcssa$i;
          $$sum10$i = (($246) + 24)|0;
          $507 = (($v$3$lcssa$i) + ($$sum10$i)|0);
          HEAP32[$507>>2] = 0;
          break;
         } else {
          _abort();
          // unreachable;
         }
        }
       } while(0);
       $508 = (($v$3$lcssa$i) + 8|0);
       $mem$0 = $508;
       return ($mem$0|0);
      } else {
       $nb$0 = $246;
      }
     }
    }
   }
  }
 } while(0);
 $509 = HEAP32[((36976 + 8|0))>>2]|0;
 $510 = ($509>>>0)<($nb$0>>>0);
 if (!($510)) {
  $511 = (($509) - ($nb$0))|0;
  $512 = HEAP32[((36976 + 20|0))>>2]|0;
  $513 = ($511>>>0)>(15);
  if ($513) {
   $514 = (($512) + ($nb$0)|0);
   HEAP32[((36976 + 20|0))>>2] = $514;
   HEAP32[((36976 + 8|0))>>2] = $511;
   $515 = $511 | 1;
   $$sum2 = (($nb$0) + 4)|0;
   $516 = (($512) + ($$sum2)|0);
   HEAP32[$516>>2] = $515;
   $517 = (($512) + ($509)|0);
   HEAP32[$517>>2] = $511;
   $518 = $nb$0 | 3;
   $519 = (($512) + 4|0);
   HEAP32[$519>>2] = $518;
  } else {
   HEAP32[((36976 + 8|0))>>2] = 0;
   HEAP32[((36976 + 20|0))>>2] = 0;
   $520 = $509 | 3;
   $521 = (($512) + 4|0);
   HEAP32[$521>>2] = $520;
   $$sum1 = (($509) + 4)|0;
   $522 = (($512) + ($$sum1)|0);
   $523 = HEAP32[$522>>2]|0;
   $524 = $523 | 1;
   HEAP32[$522>>2] = $524;
  }
  $525 = (($512) + 8|0);
  $mem$0 = $525;
  return ($mem$0|0);
 }
 $526 = HEAP32[((36976 + 12|0))>>2]|0;
 $527 = ($526>>>0)>($nb$0>>>0);
 if ($527) {
  $528 = (($526) - ($nb$0))|0;
  HEAP32[((36976 + 12|0))>>2] = $528;
  $529 = HEAP32[((36976 + 24|0))>>2]|0;
  $530 = (($529) + ($nb$0)|0);
  HEAP32[((36976 + 24|0))>>2] = $530;
  $531 = $528 | 1;
  $$sum = (($nb$0) + 4)|0;
  $532 = (($529) + ($$sum)|0);
  HEAP32[$532>>2] = $531;
  $533 = $nb$0 | 3;
  $534 = (($529) + 4|0);
  HEAP32[$534>>2] = $533;
  $535 = (($529) + 8|0);
  $mem$0 = $535;
  return ($mem$0|0);
 }
 $536 = HEAP32[37448>>2]|0;
 $537 = ($536|0)==(0);
 do {
  if ($537) {
   $538 = (_sysconf(30)|0);
   $539 = (($538) + -1)|0;
   $540 = $539 & $538;
   $541 = ($540|0)==(0);
   if ($541) {
    HEAP32[((37448 + 8|0))>>2] = $538;
    HEAP32[((37448 + 4|0))>>2] = $538;
    HEAP32[((37448 + 12|0))>>2] = -1;
    HEAP32[((37448 + 16|0))>>2] = -1;
    HEAP32[((37448 + 20|0))>>2] = 0;
    HEAP32[((36976 + 444|0))>>2] = 0;
    $542 = (_time((0|0))|0);
    $543 = $542 & -16;
    $544 = $543 ^ 1431655768;
    HEAP32[37448>>2] = $544;
    break;
   } else {
    _abort();
    // unreachable;
   }
  }
 } while(0);
 $545 = (($nb$0) + 48)|0;
 $546 = HEAP32[((37448 + 8|0))>>2]|0;
 $547 = (($nb$0) + 47)|0;
 $548 = (($546) + ($547))|0;
 $549 = (0 - ($546))|0;
 $550 = $548 & $549;
 $551 = ($550>>>0)>($nb$0>>>0);
 if (!($551)) {
  $mem$0 = 0;
  return ($mem$0|0);
 }
 $552 = HEAP32[((36976 + 440|0))>>2]|0;
 $553 = ($552|0)==(0);
 if (!($553)) {
  $554 = HEAP32[((36976 + 432|0))>>2]|0;
  $555 = (($554) + ($550))|0;
  $556 = ($555>>>0)<=($554>>>0);
  $557 = ($555>>>0)>($552>>>0);
  $or$cond1$i = $556 | $557;
  if ($or$cond1$i) {
   $mem$0 = 0;
   return ($mem$0|0);
  }
 }
 $558 = HEAP32[((36976 + 444|0))>>2]|0;
 $559 = $558 & 4;
 $560 = ($559|0)==(0);
 L279: do {
  if ($560) {
   $561 = HEAP32[((36976 + 24|0))>>2]|0;
   $562 = ($561|0)==(0|0);
   do {
    if ($562) {
     label = 191;
    } else {
     $sp$0$i$i = ((36976 + 448|0));
     while(1) {
      $563 = HEAP32[$sp$0$i$i>>2]|0;
      $564 = ($563>>>0)>($561>>>0);
      if (!($564)) {
       $565 = (($sp$0$i$i) + 4|0);
       $566 = HEAP32[$565>>2]|0;
       $567 = (($563) + ($566)|0);
       $568 = ($567>>>0)>($561>>>0);
       if ($568) {
        $$lcssa130 = $sp$0$i$i;$$lcssa132 = $565;$sp$0$i$i$lcssa = $sp$0$i$i;
        break;
       }
      }
      $569 = (($sp$0$i$i) + 8|0);
      $570 = HEAP32[$569>>2]|0;
      $571 = ($570|0)==(0|0);
      if ($571) {
       label = 190;
       break;
      } else {
       $sp$0$i$i = $570;
      }
     }
     if ((label|0) == 190) {
      label = 191;
      break;
     }
     $572 = ($sp$0$i$i$lcssa|0)==(0|0);
     if ($572) {
      label = 191;
     } else {
      $595 = HEAP32[((36976 + 12|0))>>2]|0;
      $596 = (($548) - ($595))|0;
      $597 = $596 & $549;
      $598 = ($597>>>0)<(2147483647);
      if ($598) {
       $599 = (_sbrk(($597|0))|0);
       $600 = HEAP32[$$lcssa130>>2]|0;
       $601 = HEAP32[$$lcssa132>>2]|0;
       $602 = (($600) + ($601)|0);
       $603 = ($599|0)==($602|0);
       if ($603) {
        $br$0$i = $599;$ssize$1$i = $597;
        label = 200;
       } else {
        $br$030$i = $599;$ssize$129$i = $597;
        label = 201;
       }
      } else {
       $tsize$03141$i = 0;
      }
     }
    }
   } while(0);
   do {
    if ((label|0) == 191) {
     $573 = (_sbrk(0)|0);
     $574 = ($573|0)==((-1)|0);
     if ($574) {
      $tsize$03141$i = 0;
     } else {
      $575 = $573;
      $576 = HEAP32[((37448 + 4|0))>>2]|0;
      $577 = (($576) + -1)|0;
      $578 = $577 & $575;
      $579 = ($578|0)==(0);
      if ($579) {
       $ssize$0$i = $550;
      } else {
       $580 = (($577) + ($575))|0;
       $581 = (0 - ($576))|0;
       $582 = $580 & $581;
       $583 = (($550) - ($575))|0;
       $584 = (($583) + ($582))|0;
       $ssize$0$i = $584;
      }
      $585 = HEAP32[((36976 + 432|0))>>2]|0;
      $586 = (($585) + ($ssize$0$i))|0;
      $587 = ($ssize$0$i>>>0)>($nb$0>>>0);
      $588 = ($ssize$0$i>>>0)<(2147483647);
      $or$cond$i29 = $587 & $588;
      if ($or$cond$i29) {
       $589 = HEAP32[((36976 + 440|0))>>2]|0;
       $590 = ($589|0)==(0);
       if (!($590)) {
        $591 = ($586>>>0)<=($585>>>0);
        $592 = ($586>>>0)>($589>>>0);
        $or$cond2$i = $591 | $592;
        if ($or$cond2$i) {
         $tsize$03141$i = 0;
         break;
        }
       }
       $593 = (_sbrk(($ssize$0$i|0))|0);
       $594 = ($593|0)==($573|0);
       if ($594) {
        $br$0$i = $573;$ssize$1$i = $ssize$0$i;
        label = 200;
       } else {
        $br$030$i = $593;$ssize$129$i = $ssize$0$i;
        label = 201;
       }
      } else {
       $tsize$03141$i = 0;
      }
     }
    }
   } while(0);
   L303: do {
    if ((label|0) == 200) {
     $604 = ($br$0$i|0)==((-1)|0);
     if ($604) {
      $tsize$03141$i = $ssize$1$i;
     } else {
      $tbase$245$i = $br$0$i;$tsize$244$i = $ssize$1$i;
      label = 211;
      break L279;
     }
    }
    else if ((label|0) == 201) {
     $605 = (0 - ($ssize$129$i))|0;
     $606 = ($br$030$i|0)!=((-1)|0);
     $607 = ($ssize$129$i>>>0)<(2147483647);
     $or$cond5$i = $606 & $607;
     $608 = ($545>>>0)>($ssize$129$i>>>0);
     $or$cond4$i = $or$cond5$i & $608;
     do {
      if ($or$cond4$i) {
       $609 = HEAP32[((37448 + 8|0))>>2]|0;
       $610 = (($547) - ($ssize$129$i))|0;
       $611 = (($610) + ($609))|0;
       $612 = (0 - ($609))|0;
       $613 = $611 & $612;
       $614 = ($613>>>0)<(2147483647);
       if ($614) {
        $615 = (_sbrk(($613|0))|0);
        $616 = ($615|0)==((-1)|0);
        if ($616) {
         (_sbrk(($605|0))|0);
         $tsize$03141$i = 0;
         break L303;
        } else {
         $617 = (($613) + ($ssize$129$i))|0;
         $ssize$2$i = $617;
         break;
        }
       } else {
        $ssize$2$i = $ssize$129$i;
       }
      } else {
       $ssize$2$i = $ssize$129$i;
      }
     } while(0);
     $618 = ($br$030$i|0)==((-1)|0);
     if ($618) {
      $tsize$03141$i = 0;
     } else {
      $tbase$245$i = $br$030$i;$tsize$244$i = $ssize$2$i;
      label = 211;
      break L279;
     }
    }
   } while(0);
   $619 = HEAP32[((36976 + 444|0))>>2]|0;
   $620 = $619 | 4;
   HEAP32[((36976 + 444|0))>>2] = $620;
   $tsize$1$i = $tsize$03141$i;
   label = 208;
  } else {
   $tsize$1$i = 0;
   label = 208;
  }
 } while(0);
 if ((label|0) == 208) {
  $621 = ($550>>>0)<(2147483647);
  if ($621) {
   $622 = (_sbrk(($550|0))|0);
   $623 = (_sbrk(0)|0);
   $624 = ($622|0)!=((-1)|0);
   $625 = ($623|0)!=((-1)|0);
   $or$cond3$i = $624 & $625;
   $626 = ($622>>>0)<($623>>>0);
   $or$cond6$i = $or$cond3$i & $626;
   if ($or$cond6$i) {
    $627 = $623;
    $628 = $622;
    $629 = (($627) - ($628))|0;
    $630 = (($nb$0) + 40)|0;
    $631 = ($629>>>0)>($630>>>0);
    $$tsize$1$i = $631 ? $629 : $tsize$1$i;
    if ($631) {
     $tbase$245$i = $622;$tsize$244$i = $$tsize$1$i;
     label = 211;
    }
   }
  }
 }
 if ((label|0) == 211) {
  $632 = HEAP32[((36976 + 432|0))>>2]|0;
  $633 = (($632) + ($tsize$244$i))|0;
  HEAP32[((36976 + 432|0))>>2] = $633;
  $634 = HEAP32[((36976 + 436|0))>>2]|0;
  $635 = ($633>>>0)>($634>>>0);
  if ($635) {
   HEAP32[((36976 + 436|0))>>2] = $633;
  }
  $636 = HEAP32[((36976 + 24|0))>>2]|0;
  $637 = ($636|0)==(0|0);
  L323: do {
   if ($637) {
    $638 = HEAP32[((36976 + 16|0))>>2]|0;
    $639 = ($638|0)==(0|0);
    $640 = ($tbase$245$i>>>0)<($638>>>0);
    $or$cond8$i = $639 | $640;
    if ($or$cond8$i) {
     HEAP32[((36976 + 16|0))>>2] = $tbase$245$i;
    }
    HEAP32[((36976 + 448|0))>>2] = $tbase$245$i;
    HEAP32[((36976 + 452|0))>>2] = $tsize$244$i;
    HEAP32[((36976 + 460|0))>>2] = 0;
    $641 = HEAP32[37448>>2]|0;
    HEAP32[((36976 + 36|0))>>2] = $641;
    HEAP32[((36976 + 32|0))>>2] = -1;
    $i$02$i$i = 0;
    while(1) {
     $642 = $i$02$i$i << 1;
     $643 = ((36976 + ($642<<2)|0) + 40|0);
     $$sum$i$i = (($642) + 3)|0;
     $644 = ((36976 + ($$sum$i$i<<2)|0) + 40|0);
     HEAP32[$644>>2] = $643;
     $$sum1$i$i = (($642) + 2)|0;
     $645 = ((36976 + ($$sum1$i$i<<2)|0) + 40|0);
     HEAP32[$645>>2] = $643;
     $646 = (($i$02$i$i) + 1)|0;
     $exitcond$i$i = ($646|0)==(32);
     if ($exitcond$i$i) {
      break;
     } else {
      $i$02$i$i = $646;
     }
    }
    $647 = (($tsize$244$i) + -40)|0;
    $648 = (($tbase$245$i) + 8|0);
    $649 = $648;
    $650 = $649 & 7;
    $651 = ($650|0)==(0);
    if ($651) {
     $655 = 0;
    } else {
     $652 = (0 - ($649))|0;
     $653 = $652 & 7;
     $655 = $653;
    }
    $654 = (($tbase$245$i) + ($655)|0);
    $656 = (($647) - ($655))|0;
    HEAP32[((36976 + 24|0))>>2] = $654;
    HEAP32[((36976 + 12|0))>>2] = $656;
    $657 = $656 | 1;
    $$sum$i12$i = (($655) + 4)|0;
    $658 = (($tbase$245$i) + ($$sum$i12$i)|0);
    HEAP32[$658>>2] = $657;
    $$sum2$i$i = (($tsize$244$i) + -36)|0;
    $659 = (($tbase$245$i) + ($$sum2$i$i)|0);
    HEAP32[$659>>2] = 40;
    $660 = HEAP32[((37448 + 16|0))>>2]|0;
    HEAP32[((36976 + 28|0))>>2] = $660;
   } else {
    $sp$074$i = ((36976 + 448|0));
    while(1) {
     $661 = HEAP32[$sp$074$i>>2]|0;
     $662 = (($sp$074$i) + 4|0);
     $663 = HEAP32[$662>>2]|0;
     $664 = (($661) + ($663)|0);
     $665 = ($tbase$245$i|0)==($664|0);
     if ($665) {
      $$lcssa123 = $661;$$lcssa125 = $662;$$lcssa127 = $663;$sp$074$i$lcssa = $sp$074$i;
      label = 224;
      break;
     }
     $666 = (($sp$074$i) + 8|0);
     $667 = HEAP32[$666>>2]|0;
     $668 = ($667|0)==(0|0);
     if ($668) {
      label = 229;
      break;
     } else {
      $sp$074$i = $667;
     }
    }
    if ((label|0) == 224) {
     $669 = (($sp$074$i$lcssa) + 12|0);
     $670 = HEAP32[$669>>2]|0;
     $671 = $670 & 8;
     $672 = ($671|0)==(0);
     if ($672) {
      $673 = ($636>>>0)>=($$lcssa123>>>0);
      $674 = ($636>>>0)<($tbase$245$i>>>0);
      $or$cond47$i = $673 & $674;
      if ($or$cond47$i) {
       $675 = (($$lcssa127) + ($tsize$244$i))|0;
       HEAP32[$$lcssa125>>2] = $675;
       $676 = HEAP32[((36976 + 12|0))>>2]|0;
       $677 = (($676) + ($tsize$244$i))|0;
       $678 = (($636) + 8|0);
       $679 = $678;
       $680 = $679 & 7;
       $681 = ($680|0)==(0);
       if ($681) {
        $685 = 0;
       } else {
        $682 = (0 - ($679))|0;
        $683 = $682 & 7;
        $685 = $683;
       }
       $684 = (($636) + ($685)|0);
       $686 = (($677) - ($685))|0;
       HEAP32[((36976 + 24|0))>>2] = $684;
       HEAP32[((36976 + 12|0))>>2] = $686;
       $687 = $686 | 1;
       $$sum$i16$i = (($685) + 4)|0;
       $688 = (($636) + ($$sum$i16$i)|0);
       HEAP32[$688>>2] = $687;
       $$sum2$i17$i = (($677) + 4)|0;
       $689 = (($636) + ($$sum2$i17$i)|0);
       HEAP32[$689>>2] = 40;
       $690 = HEAP32[((37448 + 16|0))>>2]|0;
       HEAP32[((36976 + 28|0))>>2] = $690;
       break;
      }
     }
    }
    else if ((label|0) == 229) {
    }
    $691 = HEAP32[((36976 + 16|0))>>2]|0;
    $692 = ($tbase$245$i>>>0)<($691>>>0);
    if ($692) {
     HEAP32[((36976 + 16|0))>>2] = $tbase$245$i;
     $756 = $tbase$245$i;
    } else {
     $756 = $691;
    }
    $693 = (($tbase$245$i) + ($tsize$244$i)|0);
    $sp$173$i = ((36976 + 448|0));
    while(1) {
     $694 = HEAP32[$sp$173$i>>2]|0;
     $695 = ($694|0)==($693|0);
     if ($695) {
      $$lcssa120 = $sp$173$i;$sp$173$i$lcssa = $sp$173$i;
      label = 235;
      break;
     }
     $696 = (($sp$173$i) + 8|0);
     $697 = HEAP32[$696>>2]|0;
     $698 = ($697|0)==(0|0);
     if ($698) {
      label = 319;
      break;
     } else {
      $sp$173$i = $697;
     }
    }
    if ((label|0) == 235) {
     $699 = (($sp$173$i$lcssa) + 12|0);
     $700 = HEAP32[$699>>2]|0;
     $701 = $700 & 8;
     $702 = ($701|0)==(0);
     if ($702) {
      HEAP32[$$lcssa120>>2] = $tbase$245$i;
      $703 = (($sp$173$i$lcssa) + 4|0);
      $704 = HEAP32[$703>>2]|0;
      $705 = (($704) + ($tsize$244$i))|0;
      HEAP32[$703>>2] = $705;
      $706 = (($tbase$245$i) + 8|0);
      $707 = $706;
      $708 = $707 & 7;
      $709 = ($708|0)==(0);
      if ($709) {
       $713 = 0;
      } else {
       $710 = (0 - ($707))|0;
       $711 = $710 & 7;
       $713 = $711;
      }
      $712 = (($tbase$245$i) + ($713)|0);
      $$sum102$i = (($tsize$244$i) + 8)|0;
      $714 = (($tbase$245$i) + ($$sum102$i)|0);
      $715 = $714;
      $716 = $715 & 7;
      $717 = ($716|0)==(0);
      if ($717) {
       $720 = 0;
      } else {
       $718 = (0 - ($715))|0;
       $719 = $718 & 7;
       $720 = $719;
      }
      $$sum103$i = (($720) + ($tsize$244$i))|0;
      $721 = (($tbase$245$i) + ($$sum103$i)|0);
      $722 = $721;
      $723 = $712;
      $724 = (($722) - ($723))|0;
      $$sum$i19$i = (($713) + ($nb$0))|0;
      $725 = (($tbase$245$i) + ($$sum$i19$i)|0);
      $726 = (($724) - ($nb$0))|0;
      $727 = $nb$0 | 3;
      $$sum1$i20$i = (($713) + 4)|0;
      $728 = (($tbase$245$i) + ($$sum1$i20$i)|0);
      HEAP32[$728>>2] = $727;
      $729 = ($721|0)==($636|0);
      L352: do {
       if ($729) {
        $730 = HEAP32[((36976 + 12|0))>>2]|0;
        $731 = (($730) + ($726))|0;
        HEAP32[((36976 + 12|0))>>2] = $731;
        HEAP32[((36976 + 24|0))>>2] = $725;
        $732 = $731 | 1;
        $$sum42$i$i = (($$sum$i19$i) + 4)|0;
        $733 = (($tbase$245$i) + ($$sum42$i$i)|0);
        HEAP32[$733>>2] = $732;
       } else {
        $734 = HEAP32[((36976 + 20|0))>>2]|0;
        $735 = ($721|0)==($734|0);
        if ($735) {
         $736 = HEAP32[((36976 + 8|0))>>2]|0;
         $737 = (($736) + ($726))|0;
         HEAP32[((36976 + 8|0))>>2] = $737;
         HEAP32[((36976 + 20|0))>>2] = $725;
         $738 = $737 | 1;
         $$sum40$i$i = (($$sum$i19$i) + 4)|0;
         $739 = (($tbase$245$i) + ($$sum40$i$i)|0);
         HEAP32[$739>>2] = $738;
         $$sum41$i$i = (($737) + ($$sum$i19$i))|0;
         $740 = (($tbase$245$i) + ($$sum41$i$i)|0);
         HEAP32[$740>>2] = $737;
         break;
        }
        $$sum2$i21$i = (($tsize$244$i) + 4)|0;
        $$sum104$i = (($$sum2$i21$i) + ($720))|0;
        $741 = (($tbase$245$i) + ($$sum104$i)|0);
        $742 = HEAP32[$741>>2]|0;
        $743 = $742 & 3;
        $744 = ($743|0)==(1);
        if ($744) {
         $745 = $742 & -8;
         $746 = $742 >>> 3;
         $747 = ($742>>>0)<(256);
         L360: do {
          if ($747) {
           $$sum3738$i$i = $720 | 8;
           $$sum114$i = (($$sum3738$i$i) + ($tsize$244$i))|0;
           $748 = (($tbase$245$i) + ($$sum114$i)|0);
           $749 = HEAP32[$748>>2]|0;
           $$sum39$i$i = (($tsize$244$i) + 12)|0;
           $$sum115$i = (($$sum39$i$i) + ($720))|0;
           $750 = (($tbase$245$i) + ($$sum115$i)|0);
           $751 = HEAP32[$750>>2]|0;
           $752 = $746 << 1;
           $753 = ((36976 + ($752<<2)|0) + 40|0);
           $754 = ($749|0)==($753|0);
           do {
            if (!($754)) {
             $755 = ($749>>>0)<($756>>>0);
             if ($755) {
              _abort();
              // unreachable;
             }
             $757 = (($749) + 12|0);
             $758 = HEAP32[$757>>2]|0;
             $759 = ($758|0)==($721|0);
             if ($759) {
              break;
             }
             _abort();
             // unreachable;
            }
           } while(0);
           $760 = ($751|0)==($749|0);
           if ($760) {
            $761 = 1 << $746;
            $762 = $761 ^ -1;
            $763 = HEAP32[36976>>2]|0;
            $764 = $763 & $762;
            HEAP32[36976>>2] = $764;
            break;
           }
           $765 = ($751|0)==($753|0);
           do {
            if ($765) {
             $$pre58$i$i = (($751) + 8|0);
             $$pre$phi59$i$iZ2D = $$pre58$i$i;
            } else {
             $766 = ($751>>>0)<($756>>>0);
             if ($766) {
              _abort();
              // unreachable;
             }
             $767 = (($751) + 8|0);
             $768 = HEAP32[$767>>2]|0;
             $769 = ($768|0)==($721|0);
             if ($769) {
              $$pre$phi59$i$iZ2D = $767;
              break;
             }
             _abort();
             // unreachable;
            }
           } while(0);
           $770 = (($749) + 12|0);
           HEAP32[$770>>2] = $751;
           HEAP32[$$pre$phi59$i$iZ2D>>2] = $749;
          } else {
           $$sum34$i$i = $720 | 24;
           $$sum105$i = (($$sum34$i$i) + ($tsize$244$i))|0;
           $771 = (($tbase$245$i) + ($$sum105$i)|0);
           $772 = HEAP32[$771>>2]|0;
           $$sum5$i$i = (($tsize$244$i) + 12)|0;
           $$sum106$i = (($$sum5$i$i) + ($720))|0;
           $773 = (($tbase$245$i) + ($$sum106$i)|0);
           $774 = HEAP32[$773>>2]|0;
           $775 = ($774|0)==($721|0);
           do {
            if ($775) {
             $$sum67$i$i = $720 | 16;
             $$sum112$i = (($$sum2$i21$i) + ($$sum67$i$i))|0;
             $785 = (($tbase$245$i) + ($$sum112$i)|0);
             $786 = HEAP32[$785>>2]|0;
             $787 = ($786|0)==(0|0);
             if ($787) {
              $$sum113$i = (($$sum67$i$i) + ($tsize$244$i))|0;
              $788 = (($tbase$245$i) + ($$sum113$i)|0);
              $789 = HEAP32[$788>>2]|0;
              $790 = ($789|0)==(0|0);
              if ($790) {
               $R$1$i$i = 0;
               break;
              } else {
               $R$0$i$i$ph = $789;$RP$0$i$i$ph = $788;
              }
             } else {
              $R$0$i$i$ph = $786;$RP$0$i$i$ph = $785;
             }
             $R$0$i$i = $R$0$i$i$ph;$RP$0$i$i = $RP$0$i$i$ph;
             while(1) {
              $791 = (($R$0$i$i) + 20|0);
              $792 = HEAP32[$791>>2]|0;
              $793 = ($792|0)==(0|0);
              if ($793) {
               $794 = (($R$0$i$i) + 16|0);
               $795 = HEAP32[$794>>2]|0;
               $796 = ($795|0)==(0|0);
               if ($796) {
                $R$0$i$i$lcssa = $R$0$i$i;$RP$0$i$i$lcssa = $RP$0$i$i;
                break;
               } else {
                $R$0$i$i$be = $795;$RP$0$i$i$be = $794;
               }
              } else {
               $R$0$i$i$be = $792;$RP$0$i$i$be = $791;
              }
              $R$0$i$i = $R$0$i$i$be;$RP$0$i$i = $RP$0$i$i$be;
             }
             $797 = ($RP$0$i$i$lcssa>>>0)<($756>>>0);
             if ($797) {
              _abort();
              // unreachable;
             } else {
              HEAP32[$RP$0$i$i$lcssa>>2] = 0;
              $R$1$i$i = $R$0$i$i$lcssa;
              break;
             }
            } else {
             $$sum3536$i$i = $720 | 8;
             $$sum107$i = (($$sum3536$i$i) + ($tsize$244$i))|0;
             $776 = (($tbase$245$i) + ($$sum107$i)|0);
             $777 = HEAP32[$776>>2]|0;
             $778 = ($777>>>0)<($756>>>0);
             if ($778) {
              _abort();
              // unreachable;
             }
             $779 = (($777) + 12|0);
             $780 = HEAP32[$779>>2]|0;
             $781 = ($780|0)==($721|0);
             if (!($781)) {
              _abort();
              // unreachable;
             }
             $782 = (($774) + 8|0);
             $783 = HEAP32[$782>>2]|0;
             $784 = ($783|0)==($721|0);
             if ($784) {
              HEAP32[$779>>2] = $774;
              HEAP32[$782>>2] = $777;
              $R$1$i$i = $774;
              break;
             } else {
              _abort();
              // unreachable;
             }
            }
           } while(0);
           $798 = ($772|0)==(0|0);
           if ($798) {
            break;
           }
           $$sum30$i$i = (($tsize$244$i) + 28)|0;
           $$sum108$i = (($$sum30$i$i) + ($720))|0;
           $799 = (($tbase$245$i) + ($$sum108$i)|0);
           $800 = HEAP32[$799>>2]|0;
           $801 = ((36976 + ($800<<2)|0) + 304|0);
           $802 = HEAP32[$801>>2]|0;
           $803 = ($721|0)==($802|0);
           do {
            if ($803) {
             HEAP32[$801>>2] = $R$1$i$i;
             $cond$i$i = ($R$1$i$i|0)==(0|0);
             if (!($cond$i$i)) {
              break;
             }
             $804 = 1 << $800;
             $805 = $804 ^ -1;
             $806 = HEAP32[((36976 + 4|0))>>2]|0;
             $807 = $806 & $805;
             HEAP32[((36976 + 4|0))>>2] = $807;
             break L360;
            } else {
             $808 = HEAP32[((36976 + 16|0))>>2]|0;
             $809 = ($772>>>0)<($808>>>0);
             if ($809) {
              _abort();
              // unreachable;
             }
             $810 = (($772) + 16|0);
             $811 = HEAP32[$810>>2]|0;
             $812 = ($811|0)==($721|0);
             if ($812) {
              HEAP32[$810>>2] = $R$1$i$i;
             } else {
              $813 = (($772) + 20|0);
              HEAP32[$813>>2] = $R$1$i$i;
             }
             $814 = ($R$1$i$i|0)==(0|0);
             if ($814) {
              break L360;
             }
            }
           } while(0);
           $815 = HEAP32[((36976 + 16|0))>>2]|0;
           $816 = ($R$1$i$i>>>0)<($815>>>0);
           if ($816) {
            _abort();
            // unreachable;
           }
           $817 = (($R$1$i$i) + 24|0);
           HEAP32[$817>>2] = $772;
           $$sum3132$i$i = $720 | 16;
           $$sum109$i = (($$sum3132$i$i) + ($tsize$244$i))|0;
           $818 = (($tbase$245$i) + ($$sum109$i)|0);
           $819 = HEAP32[$818>>2]|0;
           $820 = ($819|0)==(0|0);
           do {
            if (!($820)) {
             $821 = ($819>>>0)<($815>>>0);
             if ($821) {
              _abort();
              // unreachable;
             } else {
              $822 = (($R$1$i$i) + 16|0);
              HEAP32[$822>>2] = $819;
              $823 = (($819) + 24|0);
              HEAP32[$823>>2] = $R$1$i$i;
              break;
             }
            }
           } while(0);
           $$sum110$i = (($$sum2$i21$i) + ($$sum3132$i$i))|0;
           $824 = (($tbase$245$i) + ($$sum110$i)|0);
           $825 = HEAP32[$824>>2]|0;
           $826 = ($825|0)==(0|0);
           if ($826) {
            break;
           }
           $827 = HEAP32[((36976 + 16|0))>>2]|0;
           $828 = ($825>>>0)<($827>>>0);
           if ($828) {
            _abort();
            // unreachable;
           } else {
            $829 = (($R$1$i$i) + 20|0);
            HEAP32[$829>>2] = $825;
            $830 = (($825) + 24|0);
            HEAP32[$830>>2] = $R$1$i$i;
            break;
           }
          }
         } while(0);
         $$sum9$i$i = $745 | $720;
         $$sum111$i = (($$sum9$i$i) + ($tsize$244$i))|0;
         $831 = (($tbase$245$i) + ($$sum111$i)|0);
         $832 = (($745) + ($726))|0;
         $oldfirst$0$i$i = $831;$qsize$0$i$i = $832;
        } else {
         $oldfirst$0$i$i = $721;$qsize$0$i$i = $726;
        }
        $833 = (($oldfirst$0$i$i) + 4|0);
        $834 = HEAP32[$833>>2]|0;
        $835 = $834 & -2;
        HEAP32[$833>>2] = $835;
        $836 = $qsize$0$i$i | 1;
        $$sum10$i$i = (($$sum$i19$i) + 4)|0;
        $837 = (($tbase$245$i) + ($$sum10$i$i)|0);
        HEAP32[$837>>2] = $836;
        $$sum11$i22$i = (($qsize$0$i$i) + ($$sum$i19$i))|0;
        $838 = (($tbase$245$i) + ($$sum11$i22$i)|0);
        HEAP32[$838>>2] = $qsize$0$i$i;
        $839 = $qsize$0$i$i >>> 3;
        $840 = ($qsize$0$i$i>>>0)<(256);
        if ($840) {
         $841 = $839 << 1;
         $842 = ((36976 + ($841<<2)|0) + 40|0);
         $843 = HEAP32[36976>>2]|0;
         $844 = 1 << $839;
         $845 = $843 & $844;
         $846 = ($845|0)==(0);
         do {
          if ($846) {
           $847 = $843 | $844;
           HEAP32[36976>>2] = $847;
           $$sum26$pre$i$i = (($841) + 2)|0;
           $$pre$i23$i = ((36976 + ($$sum26$pre$i$i<<2)|0) + 40|0);
           $$pre$phi$i24$iZ2D = $$pre$i23$i;$F4$0$i$i = $842;
          } else {
           $$sum29$i$i = (($841) + 2)|0;
           $848 = ((36976 + ($$sum29$i$i<<2)|0) + 40|0);
           $849 = HEAP32[$848>>2]|0;
           $850 = HEAP32[((36976 + 16|0))>>2]|0;
           $851 = ($849>>>0)<($850>>>0);
           if (!($851)) {
            $$pre$phi$i24$iZ2D = $848;$F4$0$i$i = $849;
            break;
           }
           _abort();
           // unreachable;
          }
         } while(0);
         HEAP32[$$pre$phi$i24$iZ2D>>2] = $725;
         $852 = (($F4$0$i$i) + 12|0);
         HEAP32[$852>>2] = $725;
         $$sum27$i$i = (($$sum$i19$i) + 8)|0;
         $853 = (($tbase$245$i) + ($$sum27$i$i)|0);
         HEAP32[$853>>2] = $F4$0$i$i;
         $$sum28$i$i = (($$sum$i19$i) + 12)|0;
         $854 = (($tbase$245$i) + ($$sum28$i$i)|0);
         HEAP32[$854>>2] = $842;
         break;
        }
        $855 = $qsize$0$i$i >>> 8;
        $856 = ($855|0)==(0);
        do {
         if ($856) {
          $I7$0$i$i = 0;
         } else {
          $857 = ($qsize$0$i$i>>>0)>(16777215);
          if ($857) {
           $I7$0$i$i = 31;
           break;
          }
          $858 = (($855) + 1048320)|0;
          $859 = $858 >>> 16;
          $860 = $859 & 8;
          $861 = $855 << $860;
          $862 = (($861) + 520192)|0;
          $863 = $862 >>> 16;
          $864 = $863 & 4;
          $865 = $864 | $860;
          $866 = $861 << $864;
          $867 = (($866) + 245760)|0;
          $868 = $867 >>> 16;
          $869 = $868 & 2;
          $870 = $865 | $869;
          $871 = (14 - ($870))|0;
          $872 = $866 << $869;
          $873 = $872 >>> 15;
          $874 = (($871) + ($873))|0;
          $875 = $874 << 1;
          $876 = (($874) + 7)|0;
          $877 = $qsize$0$i$i >>> $876;
          $878 = $877 & 1;
          $879 = $878 | $875;
          $I7$0$i$i = $879;
         }
        } while(0);
        $880 = ((36976 + ($I7$0$i$i<<2)|0) + 304|0);
        $$sum12$i$i = (($$sum$i19$i) + 28)|0;
        $881 = (($tbase$245$i) + ($$sum12$i$i)|0);
        HEAP32[$881>>2] = $I7$0$i$i;
        $$sum13$i$i = (($$sum$i19$i) + 16)|0;
        $882 = (($tbase$245$i) + ($$sum13$i$i)|0);
        $$sum14$i$i = (($$sum$i19$i) + 20)|0;
        $883 = (($tbase$245$i) + ($$sum14$i$i)|0);
        HEAP32[$883>>2] = 0;
        HEAP32[$882>>2] = 0;
        $884 = HEAP32[((36976 + 4|0))>>2]|0;
        $885 = 1 << $I7$0$i$i;
        $886 = $884 & $885;
        $887 = ($886|0)==(0);
        if ($887) {
         $888 = $884 | $885;
         HEAP32[((36976 + 4|0))>>2] = $888;
         HEAP32[$880>>2] = $725;
         $$sum15$i$i = (($$sum$i19$i) + 24)|0;
         $889 = (($tbase$245$i) + ($$sum15$i$i)|0);
         HEAP32[$889>>2] = $880;
         $$sum16$i$i = (($$sum$i19$i) + 12)|0;
         $890 = (($tbase$245$i) + ($$sum16$i$i)|0);
         HEAP32[$890>>2] = $725;
         $$sum17$i$i = (($$sum$i19$i) + 8)|0;
         $891 = (($tbase$245$i) + ($$sum17$i$i)|0);
         HEAP32[$891>>2] = $725;
         break;
        }
        $892 = HEAP32[$880>>2]|0;
        $893 = ($I7$0$i$i|0)==(31);
        if ($893) {
         $901 = 0;
        } else {
         $894 = $I7$0$i$i >>> 1;
         $895 = (25 - ($894))|0;
         $901 = $895;
        }
        $896 = (($892) + 4|0);
        $897 = HEAP32[$896>>2]|0;
        $898 = $897 & -8;
        $899 = ($898|0)==($qsize$0$i$i|0);
        do {
         if ($899) {
          $T$0$lcssa$i26$i = $892;
         } else {
          $900 = $qsize$0$i$i << $901;
          $K8$053$i$i = $900;$T$052$i$i = $892;
          while(1) {
           $908 = $K8$053$i$i >>> 31;
           $909 = ((($T$052$i$i) + ($908<<2)|0) + 16|0);
           $904 = HEAP32[$909>>2]|0;
           $910 = ($904|0)==(0|0);
           if ($910) {
            $$lcssa = $909;$T$052$i$i$lcssa = $T$052$i$i;
            break;
           }
           $902 = $K8$053$i$i << 1;
           $903 = (($904) + 4|0);
           $905 = HEAP32[$903>>2]|0;
           $906 = $905 & -8;
           $907 = ($906|0)==($qsize$0$i$i|0);
           if ($907) {
            $$lcssa110 = $904;
            label = 314;
            break;
           } else {
            $K8$053$i$i = $902;$T$052$i$i = $904;
           }
          }
          if ((label|0) == 314) {
           $T$0$lcssa$i26$i = $$lcssa110;
           break;
          }
          $911 = HEAP32[((36976 + 16|0))>>2]|0;
          $912 = ($$lcssa>>>0)<($911>>>0);
          if ($912) {
           _abort();
           // unreachable;
          } else {
           HEAP32[$$lcssa>>2] = $725;
           $$sum23$i$i = (($$sum$i19$i) + 24)|0;
           $913 = (($tbase$245$i) + ($$sum23$i$i)|0);
           HEAP32[$913>>2] = $T$052$i$i$lcssa;
           $$sum24$i$i = (($$sum$i19$i) + 12)|0;
           $914 = (($tbase$245$i) + ($$sum24$i$i)|0);
           HEAP32[$914>>2] = $725;
           $$sum25$i$i = (($$sum$i19$i) + 8)|0;
           $915 = (($tbase$245$i) + ($$sum25$i$i)|0);
           HEAP32[$915>>2] = $725;
           break L352;
          }
         }
        } while(0);
        $916 = (($T$0$lcssa$i26$i) + 8|0);
        $917 = HEAP32[$916>>2]|0;
        $918 = HEAP32[((36976 + 16|0))>>2]|0;
        $919 = ($T$0$lcssa$i26$i>>>0)>=($918>>>0);
        $920 = ($917>>>0)>=($918>>>0);
        $or$cond$i27$i = $919 & $920;
        if ($or$cond$i27$i) {
         $921 = (($917) + 12|0);
         HEAP32[$921>>2] = $725;
         HEAP32[$916>>2] = $725;
         $$sum20$i$i = (($$sum$i19$i) + 8)|0;
         $922 = (($tbase$245$i) + ($$sum20$i$i)|0);
         HEAP32[$922>>2] = $917;
         $$sum21$i$i = (($$sum$i19$i) + 12)|0;
         $923 = (($tbase$245$i) + ($$sum21$i$i)|0);
         HEAP32[$923>>2] = $T$0$lcssa$i26$i;
         $$sum22$i$i = (($$sum$i19$i) + 24)|0;
         $924 = (($tbase$245$i) + ($$sum22$i$i)|0);
         HEAP32[$924>>2] = 0;
         break;
        } else {
         _abort();
         // unreachable;
        }
       }
      } while(0);
      $$sum1819$i$i = $713 | 8;
      $925 = (($tbase$245$i) + ($$sum1819$i$i)|0);
      $mem$0 = $925;
      return ($mem$0|0);
     }
    }
    else if ((label|0) == 319) {
    }
    $sp$0$i$i$i = ((36976 + 448|0));
    while(1) {
     $926 = HEAP32[$sp$0$i$i$i>>2]|0;
     $927 = ($926>>>0)>($636>>>0);
     if (!($927)) {
      $928 = (($sp$0$i$i$i) + 4|0);
      $929 = HEAP32[$928>>2]|0;
      $930 = (($926) + ($929)|0);
      $931 = ($930>>>0)>($636>>>0);
      if ($931) {
       $$lcssa116 = $926;$$lcssa117 = $929;$$lcssa118 = $930;
       break;
      }
     }
     $932 = (($sp$0$i$i$i) + 8|0);
     $933 = HEAP32[$932>>2]|0;
     $sp$0$i$i$i = $933;
    }
    $$sum$i13$i = (($$lcssa117) + -47)|0;
    $$sum1$i14$i = (($$lcssa117) + -39)|0;
    $934 = (($$lcssa116) + ($$sum1$i14$i)|0);
    $935 = $934;
    $936 = $935 & 7;
    $937 = ($936|0)==(0);
    if ($937) {
     $940 = 0;
    } else {
     $938 = (0 - ($935))|0;
     $939 = $938 & 7;
     $940 = $939;
    }
    $$sum2$i15$i = (($$sum$i13$i) + ($940))|0;
    $941 = (($$lcssa116) + ($$sum2$i15$i)|0);
    $942 = (($636) + 16|0);
    $943 = ($941>>>0)<($942>>>0);
    $944 = $943 ? $636 : $941;
    $945 = (($944) + 8|0);
    $946 = (($tsize$244$i) + -40)|0;
    $947 = (($tbase$245$i) + 8|0);
    $948 = $947;
    $949 = $948 & 7;
    $950 = ($949|0)==(0);
    if ($950) {
     $954 = 0;
    } else {
     $951 = (0 - ($948))|0;
     $952 = $951 & 7;
     $954 = $952;
    }
    $953 = (($tbase$245$i) + ($954)|0);
    $955 = (($946) - ($954))|0;
    HEAP32[((36976 + 24|0))>>2] = $953;
    HEAP32[((36976 + 12|0))>>2] = $955;
    $956 = $955 | 1;
    $$sum$i$i$i = (($954) + 4)|0;
    $957 = (($tbase$245$i) + ($$sum$i$i$i)|0);
    HEAP32[$957>>2] = $956;
    $$sum2$i$i$i = (($tsize$244$i) + -36)|0;
    $958 = (($tbase$245$i) + ($$sum2$i$i$i)|0);
    HEAP32[$958>>2] = 40;
    $959 = HEAP32[((37448 + 16|0))>>2]|0;
    HEAP32[((36976 + 28|0))>>2] = $959;
    $960 = (($944) + 4|0);
    HEAP32[$960>>2] = 27;
    ;HEAP32[$945+0>>2]=HEAP32[((36976 + 448|0))+0>>2]|0;HEAP32[$945+4>>2]=HEAP32[((36976 + 448|0))+4>>2]|0;HEAP32[$945+8>>2]=HEAP32[((36976 + 448|0))+8>>2]|0;HEAP32[$945+12>>2]=HEAP32[((36976 + 448|0))+12>>2]|0;
    HEAP32[((36976 + 448|0))>>2] = $tbase$245$i;
    HEAP32[((36976 + 452|0))>>2] = $tsize$244$i;
    HEAP32[((36976 + 460|0))>>2] = 0;
    HEAP32[((36976 + 456|0))>>2] = $945;
    $961 = (($944) + 28|0);
    HEAP32[$961>>2] = 7;
    $962 = (($944) + 32|0);
    $963 = ($962>>>0)<($$lcssa118>>>0);
    if ($963) {
     $965 = $961;
     while(1) {
      $964 = (($965) + 4|0);
      HEAP32[$964>>2] = 7;
      $966 = (($965) + 8|0);
      $967 = ($966>>>0)<($$lcssa118>>>0);
      if ($967) {
       $965 = $964;
      } else {
       break;
      }
     }
    }
    $968 = ($944|0)==($636|0);
    if (!($968)) {
     $969 = $944;
     $970 = $636;
     $971 = (($969) - ($970))|0;
     $972 = (($636) + ($971)|0);
     $$sum3$i$i = (($971) + 4)|0;
     $973 = (($636) + ($$sum3$i$i)|0);
     $974 = HEAP32[$973>>2]|0;
     $975 = $974 & -2;
     HEAP32[$973>>2] = $975;
     $976 = $971 | 1;
     $977 = (($636) + 4|0);
     HEAP32[$977>>2] = $976;
     HEAP32[$972>>2] = $971;
     $978 = $971 >>> 3;
     $979 = ($971>>>0)<(256);
     if ($979) {
      $980 = $978 << 1;
      $981 = ((36976 + ($980<<2)|0) + 40|0);
      $982 = HEAP32[36976>>2]|0;
      $983 = 1 << $978;
      $984 = $982 & $983;
      $985 = ($984|0)==(0);
      do {
       if ($985) {
        $986 = $982 | $983;
        HEAP32[36976>>2] = $986;
        $$sum10$pre$i$i = (($980) + 2)|0;
        $$pre$i$i = ((36976 + ($$sum10$pre$i$i<<2)|0) + 40|0);
        $$pre$phi$i$iZ2D = $$pre$i$i;$F$0$i$i = $981;
       } else {
        $$sum11$i$i = (($980) + 2)|0;
        $987 = ((36976 + ($$sum11$i$i<<2)|0) + 40|0);
        $988 = HEAP32[$987>>2]|0;
        $989 = HEAP32[((36976 + 16|0))>>2]|0;
        $990 = ($988>>>0)<($989>>>0);
        if (!($990)) {
         $$pre$phi$i$iZ2D = $987;$F$0$i$i = $988;
         break;
        }
        _abort();
        // unreachable;
       }
      } while(0);
      HEAP32[$$pre$phi$i$iZ2D>>2] = $636;
      $991 = (($F$0$i$i) + 12|0);
      HEAP32[$991>>2] = $636;
      $992 = (($636) + 8|0);
      HEAP32[$992>>2] = $F$0$i$i;
      $993 = (($636) + 12|0);
      HEAP32[$993>>2] = $981;
      break;
     }
     $994 = $971 >>> 8;
     $995 = ($994|0)==(0);
     if ($995) {
      $I1$0$i$i = 0;
     } else {
      $996 = ($971>>>0)>(16777215);
      if ($996) {
       $I1$0$i$i = 31;
      } else {
       $997 = (($994) + 1048320)|0;
       $998 = $997 >>> 16;
       $999 = $998 & 8;
       $1000 = $994 << $999;
       $1001 = (($1000) + 520192)|0;
       $1002 = $1001 >>> 16;
       $1003 = $1002 & 4;
       $1004 = $1003 | $999;
       $1005 = $1000 << $1003;
       $1006 = (($1005) + 245760)|0;
       $1007 = $1006 >>> 16;
       $1008 = $1007 & 2;
       $1009 = $1004 | $1008;
       $1010 = (14 - ($1009))|0;
       $1011 = $1005 << $1008;
       $1012 = $1011 >>> 15;
       $1013 = (($1010) + ($1012))|0;
       $1014 = $1013 << 1;
       $1015 = (($1013) + 7)|0;
       $1016 = $971 >>> $1015;
       $1017 = $1016 & 1;
       $1018 = $1017 | $1014;
       $I1$0$i$i = $1018;
      }
     }
     $1019 = ((36976 + ($I1$0$i$i<<2)|0) + 304|0);
     $1020 = (($636) + 28|0);
     $I1$0$c$i$i = $I1$0$i$i;
     HEAP32[$1020>>2] = $I1$0$c$i$i;
     $1021 = (($636) + 20|0);
     HEAP32[$1021>>2] = 0;
     $1022 = (($636) + 16|0);
     HEAP32[$1022>>2] = 0;
     $1023 = HEAP32[((36976 + 4|0))>>2]|0;
     $1024 = 1 << $I1$0$i$i;
     $1025 = $1023 & $1024;
     $1026 = ($1025|0)==(0);
     if ($1026) {
      $1027 = $1023 | $1024;
      HEAP32[((36976 + 4|0))>>2] = $1027;
      HEAP32[$1019>>2] = $636;
      $1028 = (($636) + 24|0);
      HEAP32[$1028>>2] = $1019;
      $1029 = (($636) + 12|0);
      HEAP32[$1029>>2] = $636;
      $1030 = (($636) + 8|0);
      HEAP32[$1030>>2] = $636;
      break;
     }
     $1031 = HEAP32[$1019>>2]|0;
     $1032 = ($I1$0$i$i|0)==(31);
     if ($1032) {
      $1040 = 0;
     } else {
      $1033 = $I1$0$i$i >>> 1;
      $1034 = (25 - ($1033))|0;
      $1040 = $1034;
     }
     $1035 = (($1031) + 4|0);
     $1036 = HEAP32[$1035>>2]|0;
     $1037 = $1036 & -8;
     $1038 = ($1037|0)==($971|0);
     do {
      if ($1038) {
       $T$0$lcssa$i$i = $1031;
      } else {
       $1039 = $971 << $1040;
       $K2$015$i$i = $1039;$T$014$i$i = $1031;
       while(1) {
        $1047 = $K2$015$i$i >>> 31;
        $1048 = ((($T$014$i$i) + ($1047<<2)|0) + 16|0);
        $1043 = HEAP32[$1048>>2]|0;
        $1049 = ($1043|0)==(0|0);
        if ($1049) {
         $$lcssa112 = $1048;$T$014$i$i$lcssa = $T$014$i$i;
         break;
        }
        $1041 = $K2$015$i$i << 1;
        $1042 = (($1043) + 4|0);
        $1044 = HEAP32[$1042>>2]|0;
        $1045 = $1044 & -8;
        $1046 = ($1045|0)==($971|0);
        if ($1046) {
         $$lcssa115 = $1043;
         label = 353;
         break;
        } else {
         $K2$015$i$i = $1041;$T$014$i$i = $1043;
        }
       }
       if ((label|0) == 353) {
        $T$0$lcssa$i$i = $$lcssa115;
        break;
       }
       $1050 = HEAP32[((36976 + 16|0))>>2]|0;
       $1051 = ($$lcssa112>>>0)<($1050>>>0);
       if ($1051) {
        _abort();
        // unreachable;
       } else {
        HEAP32[$$lcssa112>>2] = $636;
        $1052 = (($636) + 24|0);
        HEAP32[$1052>>2] = $T$014$i$i$lcssa;
        $1053 = (($636) + 12|0);
        HEAP32[$1053>>2] = $636;
        $1054 = (($636) + 8|0);
        HEAP32[$1054>>2] = $636;
        break L323;
       }
      }
     } while(0);
     $1055 = (($T$0$lcssa$i$i) + 8|0);
     $1056 = HEAP32[$1055>>2]|0;
     $1057 = HEAP32[((36976 + 16|0))>>2]|0;
     $1058 = ($T$0$lcssa$i$i>>>0)>=($1057>>>0);
     $1059 = ($1056>>>0)>=($1057>>>0);
     $or$cond$i$i = $1058 & $1059;
     if ($or$cond$i$i) {
      $1060 = (($1056) + 12|0);
      HEAP32[$1060>>2] = $636;
      HEAP32[$1055>>2] = $636;
      $1061 = (($636) + 8|0);
      HEAP32[$1061>>2] = $1056;
      $1062 = (($636) + 12|0);
      HEAP32[$1062>>2] = $T$0$lcssa$i$i;
      $1063 = (($636) + 24|0);
      HEAP32[$1063>>2] = 0;
      break;
     } else {
      _abort();
      // unreachable;
     }
    }
   }
  } while(0);
  $1064 = HEAP32[((36976 + 12|0))>>2]|0;
  $1065 = ($1064>>>0)>($nb$0>>>0);
  if ($1065) {
   $1066 = (($1064) - ($nb$0))|0;
   HEAP32[((36976 + 12|0))>>2] = $1066;
   $1067 = HEAP32[((36976 + 24|0))>>2]|0;
   $1068 = (($1067) + ($nb$0)|0);
   HEAP32[((36976 + 24|0))>>2] = $1068;
   $1069 = $1066 | 1;
   $$sum$i32 = (($nb$0) + 4)|0;
   $1070 = (($1067) + ($$sum$i32)|0);
   HEAP32[$1070>>2] = $1069;
   $1071 = $nb$0 | 3;
   $1072 = (($1067) + 4|0);
   HEAP32[$1072>>2] = $1071;
   $1073 = (($1067) + 8|0);
   $mem$0 = $1073;
   return ($mem$0|0);
  }
 }
 $1074 = (___errno_location()|0);
 HEAP32[$1074>>2] = 12;
 $mem$0 = 0;
 return ($mem$0|0);
}
function _free($mem) {
 $mem = $mem|0;
 var $$lcssa = 0, $$lcssa73 = 0, $$pre = 0, $$pre$phi66Z2D = 0, $$pre$phi68Z2D = 0, $$pre$phiZ2D = 0, $$pre65 = 0, $$pre67 = 0, $$sum = 0, $$sum16$pre = 0, $$sum17 = 0, $$sum18 = 0, $$sum19 = 0, $$sum2 = 0, $$sum20 = 0, $$sum2324 = 0, $$sum25 = 0, $$sum26 = 0, $$sum28 = 0, $$sum29 = 0;
 var $$sum3 = 0, $$sum30 = 0, $$sum31 = 0, $$sum32 = 0, $$sum33 = 0, $$sum34 = 0, $$sum35 = 0, $$sum36 = 0, $$sum37 = 0, $$sum5 = 0, $$sum67 = 0, $$sum8 = 0, $$sum9 = 0, $0 = 0, $1 = 0, $10 = 0, $100 = 0, $101 = 0, $102 = 0, $103 = 0;
 var $104 = 0, $105 = 0, $106 = 0, $107 = 0, $108 = 0, $109 = 0, $11 = 0, $110 = 0, $111 = 0, $112 = 0, $113 = 0, $114 = 0, $115 = 0, $116 = 0, $117 = 0, $118 = 0, $119 = 0, $12 = 0, $120 = 0, $121 = 0;
 var $122 = 0, $123 = 0, $124 = 0, $125 = 0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0, $130 = 0, $131 = 0, $132 = 0, $133 = 0, $134 = 0, $135 = 0, $136 = 0, $137 = 0, $138 = 0, $139 = 0, $14 = 0;
 var $140 = 0, $141 = 0, $142 = 0, $143 = 0, $144 = 0, $145 = 0, $146 = 0, $147 = 0, $148 = 0, $149 = 0, $15 = 0, $150 = 0, $151 = 0, $152 = 0, $153 = 0, $154 = 0, $155 = 0, $156 = 0, $157 = 0, $158 = 0;
 var $159 = 0, $16 = 0, $160 = 0, $161 = 0, $162 = 0, $163 = 0, $164 = 0, $165 = 0, $166 = 0, $167 = 0, $168 = 0, $169 = 0, $17 = 0, $170 = 0, $171 = 0, $172 = 0, $173 = 0, $174 = 0, $175 = 0, $176 = 0;
 var $177 = 0, $178 = 0, $179 = 0, $18 = 0, $180 = 0, $181 = 0, $182 = 0, $183 = 0, $184 = 0, $185 = 0, $186 = 0, $187 = 0, $188 = 0, $189 = 0, $19 = 0, $190 = 0, $191 = 0, $192 = 0, $193 = 0, $194 = 0;
 var $195 = 0, $196 = 0, $197 = 0, $198 = 0, $199 = 0, $2 = 0, $20 = 0, $200 = 0, $201 = 0, $202 = 0, $203 = 0, $204 = 0, $205 = 0, $206 = 0, $207 = 0, $208 = 0, $209 = 0, $21 = 0, $210 = 0, $211 = 0;
 var $212 = 0, $213 = 0, $214 = 0, $215 = 0, $216 = 0, $217 = 0, $218 = 0, $219 = 0, $22 = 0, $220 = 0, $221 = 0, $222 = 0, $223 = 0, $224 = 0, $225 = 0, $226 = 0, $227 = 0, $228 = 0, $229 = 0, $23 = 0;
 var $230 = 0, $231 = 0, $232 = 0, $233 = 0, $234 = 0, $235 = 0, $236 = 0, $237 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0, $241 = 0, $242 = 0, $243 = 0, $244 = 0, $245 = 0, $246 = 0, $247 = 0, $248 = 0;
 var $249 = 0, $25 = 0, $250 = 0, $251 = 0, $252 = 0, $253 = 0, $254 = 0, $255 = 0, $256 = 0, $257 = 0, $258 = 0, $259 = 0, $26 = 0, $260 = 0, $261 = 0, $262 = 0, $263 = 0, $264 = 0, $265 = 0, $266 = 0;
 var $267 = 0, $268 = 0, $269 = 0, $27 = 0, $270 = 0, $271 = 0, $272 = 0, $273 = 0, $274 = 0, $275 = 0, $276 = 0, $277 = 0, $278 = 0, $279 = 0, $28 = 0, $280 = 0, $281 = 0, $282 = 0, $283 = 0, $284 = 0;
 var $285 = 0, $286 = 0, $287 = 0, $288 = 0, $289 = 0, $29 = 0, $290 = 0, $291 = 0, $292 = 0, $293 = 0, $294 = 0, $295 = 0, $296 = 0, $297 = 0, $298 = 0, $299 = 0, $3 = 0, $30 = 0, $300 = 0, $301 = 0;
 var $302 = 0, $303 = 0, $304 = 0, $305 = 0, $306 = 0, $307 = 0, $308 = 0, $309 = 0, $31 = 0, $310 = 0, $311 = 0, $312 = 0, $313 = 0, $314 = 0, $315 = 0, $316 = 0, $317 = 0, $318 = 0, $319 = 0, $32 = 0;
 var $320 = 0, $321 = 0, $33 = 0, $34 = 0, $35 = 0, $36 = 0, $37 = 0, $38 = 0, $39 = 0, $4 = 0, $40 = 0, $41 = 0, $42 = 0, $43 = 0, $44 = 0, $45 = 0, $46 = 0, $47 = 0, $48 = 0, $49 = 0;
 var $5 = 0, $50 = 0, $51 = 0, $52 = 0, $53 = 0, $54 = 0, $55 = 0, $56 = 0, $57 = 0, $58 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $62 = 0, $63 = 0, $64 = 0, $65 = 0, $66 = 0, $67 = 0;
 var $68 = 0, $69 = 0, $7 = 0, $70 = 0, $71 = 0, $72 = 0, $73 = 0, $74 = 0, $75 = 0, $76 = 0, $77 = 0, $78 = 0, $79 = 0, $8 = 0, $80 = 0, $81 = 0, $82 = 0, $83 = 0, $84 = 0, $85 = 0;
 var $86 = 0, $87 = 0, $88 = 0, $89 = 0, $9 = 0, $90 = 0, $91 = 0, $92 = 0, $93 = 0, $94 = 0, $95 = 0, $96 = 0, $97 = 0, $98 = 0, $99 = 0, $F16$0 = 0, $I18$0 = 0, $I18$0$c = 0, $K19$060 = 0, $R$0 = 0;
 var $R$0$be = 0, $R$0$lcssa = 0, $R$0$ph = 0, $R$1 = 0, $R7$0 = 0, $R7$0$be = 0, $R7$0$lcssa = 0, $R7$0$ph = 0, $R7$1 = 0, $RP$0 = 0, $RP$0$be = 0, $RP$0$lcssa = 0, $RP$0$ph = 0, $RP9$0 = 0, $RP9$0$be = 0, $RP9$0$lcssa = 0, $RP9$0$ph = 0, $T$0$lcssa = 0, $T$059 = 0, $T$059$lcssa = 0;
 var $cond = 0, $cond54 = 0, $or$cond = 0, $p$0 = 0, $psize$0 = 0, $psize$1 = 0, $sp$0$i = 0, $sp$0$in$i = 0, label = 0, sp = 0;
 sp = STACKTOP;
 $0 = ($mem|0)==(0|0);
 if ($0) {
  return;
 }
 $1 = (($mem) + -8|0);
 $2 = HEAP32[((36976 + 16|0))>>2]|0;
 $3 = ($1>>>0)<($2>>>0);
 if ($3) {
  _abort();
  // unreachable;
 }
 $4 = (($mem) + -4|0);
 $5 = HEAP32[$4>>2]|0;
 $6 = $5 & 3;
 $7 = ($6|0)==(1);
 if ($7) {
  _abort();
  // unreachable;
 }
 $8 = $5 & -8;
 $$sum = (($8) + -8)|0;
 $9 = (($mem) + ($$sum)|0);
 $10 = $5 & 1;
 $11 = ($10|0)==(0);
 do {
  if ($11) {
   $12 = HEAP32[$1>>2]|0;
   $13 = ($6|0)==(0);
   if ($13) {
    return;
   }
   $$sum2 = (-8 - ($12))|0;
   $14 = (($mem) + ($$sum2)|0);
   $15 = (($12) + ($8))|0;
   $16 = ($14>>>0)<($2>>>0);
   if ($16) {
    _abort();
    // unreachable;
   }
   $17 = HEAP32[((36976 + 20|0))>>2]|0;
   $18 = ($14|0)==($17|0);
   if ($18) {
    $$sum3 = (($8) + -4)|0;
    $103 = (($mem) + ($$sum3)|0);
    $104 = HEAP32[$103>>2]|0;
    $105 = $104 & 3;
    $106 = ($105|0)==(3);
    if (!($106)) {
     $p$0 = $14;$psize$0 = $15;
     break;
    }
    HEAP32[((36976 + 8|0))>>2] = $15;
    $107 = $104 & -2;
    HEAP32[$103>>2] = $107;
    $108 = $15 | 1;
    $$sum26 = (($$sum2) + 4)|0;
    $109 = (($mem) + ($$sum26)|0);
    HEAP32[$109>>2] = $108;
    HEAP32[$9>>2] = $15;
    return;
   }
   $19 = $12 >>> 3;
   $20 = ($12>>>0)<(256);
   if ($20) {
    $$sum36 = (($$sum2) + 8)|0;
    $21 = (($mem) + ($$sum36)|0);
    $22 = HEAP32[$21>>2]|0;
    $$sum37 = (($$sum2) + 12)|0;
    $23 = (($mem) + ($$sum37)|0);
    $24 = HEAP32[$23>>2]|0;
    $25 = $19 << 1;
    $26 = ((36976 + ($25<<2)|0) + 40|0);
    $27 = ($22|0)==($26|0);
    if (!($27)) {
     $28 = ($22>>>0)<($2>>>0);
     if ($28) {
      _abort();
      // unreachable;
     }
     $29 = (($22) + 12|0);
     $30 = HEAP32[$29>>2]|0;
     $31 = ($30|0)==($14|0);
     if (!($31)) {
      _abort();
      // unreachable;
     }
    }
    $32 = ($24|0)==($22|0);
    if ($32) {
     $33 = 1 << $19;
     $34 = $33 ^ -1;
     $35 = HEAP32[36976>>2]|0;
     $36 = $35 & $34;
     HEAP32[36976>>2] = $36;
     $p$0 = $14;$psize$0 = $15;
     break;
    }
    $37 = ($24|0)==($26|0);
    if ($37) {
     $$pre67 = (($24) + 8|0);
     $$pre$phi68Z2D = $$pre67;
    } else {
     $38 = ($24>>>0)<($2>>>0);
     if ($38) {
      _abort();
      // unreachable;
     }
     $39 = (($24) + 8|0);
     $40 = HEAP32[$39>>2]|0;
     $41 = ($40|0)==($14|0);
     if ($41) {
      $$pre$phi68Z2D = $39;
     } else {
      _abort();
      // unreachable;
     }
    }
    $42 = (($22) + 12|0);
    HEAP32[$42>>2] = $24;
    HEAP32[$$pre$phi68Z2D>>2] = $22;
    $p$0 = $14;$psize$0 = $15;
    break;
   }
   $$sum28 = (($$sum2) + 24)|0;
   $43 = (($mem) + ($$sum28)|0);
   $44 = HEAP32[$43>>2]|0;
   $$sum29 = (($$sum2) + 12)|0;
   $45 = (($mem) + ($$sum29)|0);
   $46 = HEAP32[$45>>2]|0;
   $47 = ($46|0)==($14|0);
   do {
    if ($47) {
     $$sum31 = (($$sum2) + 20)|0;
     $57 = (($mem) + ($$sum31)|0);
     $58 = HEAP32[$57>>2]|0;
     $59 = ($58|0)==(0|0);
     if ($59) {
      $$sum30 = (($$sum2) + 16)|0;
      $60 = (($mem) + ($$sum30)|0);
      $61 = HEAP32[$60>>2]|0;
      $62 = ($61|0)==(0|0);
      if ($62) {
       $R$1 = 0;
       break;
      } else {
       $R$0$ph = $61;$RP$0$ph = $60;
      }
     } else {
      $R$0$ph = $58;$RP$0$ph = $57;
     }
     $R$0 = $R$0$ph;$RP$0 = $RP$0$ph;
     while(1) {
      $63 = (($R$0) + 20|0);
      $64 = HEAP32[$63>>2]|0;
      $65 = ($64|0)==(0|0);
      if ($65) {
       $66 = (($R$0) + 16|0);
       $67 = HEAP32[$66>>2]|0;
       $68 = ($67|0)==(0|0);
       if ($68) {
        $R$0$lcssa = $R$0;$RP$0$lcssa = $RP$0;
        break;
       } else {
        $R$0$be = $67;$RP$0$be = $66;
       }
      } else {
       $R$0$be = $64;$RP$0$be = $63;
      }
      $R$0 = $R$0$be;$RP$0 = $RP$0$be;
     }
     $69 = ($RP$0$lcssa>>>0)<($2>>>0);
     if ($69) {
      _abort();
      // unreachable;
     } else {
      HEAP32[$RP$0$lcssa>>2] = 0;
      $R$1 = $R$0$lcssa;
      break;
     }
    } else {
     $$sum35 = (($$sum2) + 8)|0;
     $48 = (($mem) + ($$sum35)|0);
     $49 = HEAP32[$48>>2]|0;
     $50 = ($49>>>0)<($2>>>0);
     if ($50) {
      _abort();
      // unreachable;
     }
     $51 = (($49) + 12|0);
     $52 = HEAP32[$51>>2]|0;
     $53 = ($52|0)==($14|0);
     if (!($53)) {
      _abort();
      // unreachable;
     }
     $54 = (($46) + 8|0);
     $55 = HEAP32[$54>>2]|0;
     $56 = ($55|0)==($14|0);
     if ($56) {
      HEAP32[$51>>2] = $46;
      HEAP32[$54>>2] = $49;
      $R$1 = $46;
      break;
     } else {
      _abort();
      // unreachable;
     }
    }
   } while(0);
   $70 = ($44|0)==(0|0);
   if ($70) {
    $p$0 = $14;$psize$0 = $15;
   } else {
    $$sum32 = (($$sum2) + 28)|0;
    $71 = (($mem) + ($$sum32)|0);
    $72 = HEAP32[$71>>2]|0;
    $73 = ((36976 + ($72<<2)|0) + 304|0);
    $74 = HEAP32[$73>>2]|0;
    $75 = ($14|0)==($74|0);
    if ($75) {
     HEAP32[$73>>2] = $R$1;
     $cond = ($R$1|0)==(0|0);
     if ($cond) {
      $76 = 1 << $72;
      $77 = $76 ^ -1;
      $78 = HEAP32[((36976 + 4|0))>>2]|0;
      $79 = $78 & $77;
      HEAP32[((36976 + 4|0))>>2] = $79;
      $p$0 = $14;$psize$0 = $15;
      break;
     }
    } else {
     $80 = HEAP32[((36976 + 16|0))>>2]|0;
     $81 = ($44>>>0)<($80>>>0);
     if ($81) {
      _abort();
      // unreachable;
     }
     $82 = (($44) + 16|0);
     $83 = HEAP32[$82>>2]|0;
     $84 = ($83|0)==($14|0);
     if ($84) {
      HEAP32[$82>>2] = $R$1;
     } else {
      $85 = (($44) + 20|0);
      HEAP32[$85>>2] = $R$1;
     }
     $86 = ($R$1|0)==(0|0);
     if ($86) {
      $p$0 = $14;$psize$0 = $15;
      break;
     }
    }
    $87 = HEAP32[((36976 + 16|0))>>2]|0;
    $88 = ($R$1>>>0)<($87>>>0);
    if ($88) {
     _abort();
     // unreachable;
    }
    $89 = (($R$1) + 24|0);
    HEAP32[$89>>2] = $44;
    $$sum33 = (($$sum2) + 16)|0;
    $90 = (($mem) + ($$sum33)|0);
    $91 = HEAP32[$90>>2]|0;
    $92 = ($91|0)==(0|0);
    do {
     if (!($92)) {
      $93 = ($91>>>0)<($87>>>0);
      if ($93) {
       _abort();
       // unreachable;
      } else {
       $94 = (($R$1) + 16|0);
       HEAP32[$94>>2] = $91;
       $95 = (($91) + 24|0);
       HEAP32[$95>>2] = $R$1;
       break;
      }
     }
    } while(0);
    $$sum34 = (($$sum2) + 20)|0;
    $96 = (($mem) + ($$sum34)|0);
    $97 = HEAP32[$96>>2]|0;
    $98 = ($97|0)==(0|0);
    if ($98) {
     $p$0 = $14;$psize$0 = $15;
    } else {
     $99 = HEAP32[((36976 + 16|0))>>2]|0;
     $100 = ($97>>>0)<($99>>>0);
     if ($100) {
      _abort();
      // unreachable;
     } else {
      $101 = (($R$1) + 20|0);
      HEAP32[$101>>2] = $97;
      $102 = (($97) + 24|0);
      HEAP32[$102>>2] = $R$1;
      $p$0 = $14;$psize$0 = $15;
      break;
     }
    }
   }
  } else {
   $p$0 = $1;$psize$0 = $8;
  }
 } while(0);
 $110 = ($p$0>>>0)<($9>>>0);
 if (!($110)) {
  _abort();
  // unreachable;
 }
 $$sum25 = (($8) + -4)|0;
 $111 = (($mem) + ($$sum25)|0);
 $112 = HEAP32[$111>>2]|0;
 $113 = $112 & 1;
 $114 = ($113|0)==(0);
 if ($114) {
  _abort();
  // unreachable;
 }
 $115 = $112 & 2;
 $116 = ($115|0)==(0);
 if ($116) {
  $117 = HEAP32[((36976 + 24|0))>>2]|0;
  $118 = ($9|0)==($117|0);
  if ($118) {
   $119 = HEAP32[((36976 + 12|0))>>2]|0;
   $120 = (($119) + ($psize$0))|0;
   HEAP32[((36976 + 12|0))>>2] = $120;
   HEAP32[((36976 + 24|0))>>2] = $p$0;
   $121 = $120 | 1;
   $122 = (($p$0) + 4|0);
   HEAP32[$122>>2] = $121;
   $123 = HEAP32[((36976 + 20|0))>>2]|0;
   $124 = ($p$0|0)==($123|0);
   if (!($124)) {
    return;
   }
   HEAP32[((36976 + 20|0))>>2] = 0;
   HEAP32[((36976 + 8|0))>>2] = 0;
   return;
  }
  $125 = HEAP32[((36976 + 20|0))>>2]|0;
  $126 = ($9|0)==($125|0);
  if ($126) {
   $127 = HEAP32[((36976 + 8|0))>>2]|0;
   $128 = (($127) + ($psize$0))|0;
   HEAP32[((36976 + 8|0))>>2] = $128;
   HEAP32[((36976 + 20|0))>>2] = $p$0;
   $129 = $128 | 1;
   $130 = (($p$0) + 4|0);
   HEAP32[$130>>2] = $129;
   $131 = (($p$0) + ($128)|0);
   HEAP32[$131>>2] = $128;
   return;
  }
  $132 = $112 & -8;
  $133 = (($132) + ($psize$0))|0;
  $134 = $112 >>> 3;
  $135 = ($112>>>0)<(256);
  do {
   if ($135) {
    $136 = (($mem) + ($8)|0);
    $137 = HEAP32[$136>>2]|0;
    $$sum2324 = $8 | 4;
    $138 = (($mem) + ($$sum2324)|0);
    $139 = HEAP32[$138>>2]|0;
    $140 = $134 << 1;
    $141 = ((36976 + ($140<<2)|0) + 40|0);
    $142 = ($137|0)==($141|0);
    if (!($142)) {
     $143 = HEAP32[((36976 + 16|0))>>2]|0;
     $144 = ($137>>>0)<($143>>>0);
     if ($144) {
      _abort();
      // unreachable;
     }
     $145 = (($137) + 12|0);
     $146 = HEAP32[$145>>2]|0;
     $147 = ($146|0)==($9|0);
     if (!($147)) {
      _abort();
      // unreachable;
     }
    }
    $148 = ($139|0)==($137|0);
    if ($148) {
     $149 = 1 << $134;
     $150 = $149 ^ -1;
     $151 = HEAP32[36976>>2]|0;
     $152 = $151 & $150;
     HEAP32[36976>>2] = $152;
     break;
    }
    $153 = ($139|0)==($141|0);
    if ($153) {
     $$pre65 = (($139) + 8|0);
     $$pre$phi66Z2D = $$pre65;
    } else {
     $154 = HEAP32[((36976 + 16|0))>>2]|0;
     $155 = ($139>>>0)<($154>>>0);
     if ($155) {
      _abort();
      // unreachable;
     }
     $156 = (($139) + 8|0);
     $157 = HEAP32[$156>>2]|0;
     $158 = ($157|0)==($9|0);
     if ($158) {
      $$pre$phi66Z2D = $156;
     } else {
      _abort();
      // unreachable;
     }
    }
    $159 = (($137) + 12|0);
    HEAP32[$159>>2] = $139;
    HEAP32[$$pre$phi66Z2D>>2] = $137;
   } else {
    $$sum5 = (($8) + 16)|0;
    $160 = (($mem) + ($$sum5)|0);
    $161 = HEAP32[$160>>2]|0;
    $$sum67 = $8 | 4;
    $162 = (($mem) + ($$sum67)|0);
    $163 = HEAP32[$162>>2]|0;
    $164 = ($163|0)==($9|0);
    do {
     if ($164) {
      $$sum9 = (($8) + 12)|0;
      $175 = (($mem) + ($$sum9)|0);
      $176 = HEAP32[$175>>2]|0;
      $177 = ($176|0)==(0|0);
      if ($177) {
       $$sum8 = (($8) + 8)|0;
       $178 = (($mem) + ($$sum8)|0);
       $179 = HEAP32[$178>>2]|0;
       $180 = ($179|0)==(0|0);
       if ($180) {
        $R7$1 = 0;
        break;
       } else {
        $R7$0$ph = $179;$RP9$0$ph = $178;
       }
      } else {
       $R7$0$ph = $176;$RP9$0$ph = $175;
      }
      $R7$0 = $R7$0$ph;$RP9$0 = $RP9$0$ph;
      while(1) {
       $181 = (($R7$0) + 20|0);
       $182 = HEAP32[$181>>2]|0;
       $183 = ($182|0)==(0|0);
       if ($183) {
        $184 = (($R7$0) + 16|0);
        $185 = HEAP32[$184>>2]|0;
        $186 = ($185|0)==(0|0);
        if ($186) {
         $R7$0$lcssa = $R7$0;$RP9$0$lcssa = $RP9$0;
         break;
        } else {
         $R7$0$be = $185;$RP9$0$be = $184;
        }
       } else {
        $R7$0$be = $182;$RP9$0$be = $181;
       }
       $R7$0 = $R7$0$be;$RP9$0 = $RP9$0$be;
      }
      $187 = HEAP32[((36976 + 16|0))>>2]|0;
      $188 = ($RP9$0$lcssa>>>0)<($187>>>0);
      if ($188) {
       _abort();
       // unreachable;
      } else {
       HEAP32[$RP9$0$lcssa>>2] = 0;
       $R7$1 = $R7$0$lcssa;
       break;
      }
     } else {
      $165 = (($mem) + ($8)|0);
      $166 = HEAP32[$165>>2]|0;
      $167 = HEAP32[((36976 + 16|0))>>2]|0;
      $168 = ($166>>>0)<($167>>>0);
      if ($168) {
       _abort();
       // unreachable;
      }
      $169 = (($166) + 12|0);
      $170 = HEAP32[$169>>2]|0;
      $171 = ($170|0)==($9|0);
      if (!($171)) {
       _abort();
       // unreachable;
      }
      $172 = (($163) + 8|0);
      $173 = HEAP32[$172>>2]|0;
      $174 = ($173|0)==($9|0);
      if ($174) {
       HEAP32[$169>>2] = $163;
       HEAP32[$172>>2] = $166;
       $R7$1 = $163;
       break;
      } else {
       _abort();
       // unreachable;
      }
     }
    } while(0);
    $189 = ($161|0)==(0|0);
    if (!($189)) {
     $$sum18 = (($8) + 20)|0;
     $190 = (($mem) + ($$sum18)|0);
     $191 = HEAP32[$190>>2]|0;
     $192 = ((36976 + ($191<<2)|0) + 304|0);
     $193 = HEAP32[$192>>2]|0;
     $194 = ($9|0)==($193|0);
     if ($194) {
      HEAP32[$192>>2] = $R7$1;
      $cond54 = ($R7$1|0)==(0|0);
      if ($cond54) {
       $195 = 1 << $191;
       $196 = $195 ^ -1;
       $197 = HEAP32[((36976 + 4|0))>>2]|0;
       $198 = $197 & $196;
       HEAP32[((36976 + 4|0))>>2] = $198;
       break;
      }
     } else {
      $199 = HEAP32[((36976 + 16|0))>>2]|0;
      $200 = ($161>>>0)<($199>>>0);
      if ($200) {
       _abort();
       // unreachable;
      }
      $201 = (($161) + 16|0);
      $202 = HEAP32[$201>>2]|0;
      $203 = ($202|0)==($9|0);
      if ($203) {
       HEAP32[$201>>2] = $R7$1;
      } else {
       $204 = (($161) + 20|0);
       HEAP32[$204>>2] = $R7$1;
      }
      $205 = ($R7$1|0)==(0|0);
      if ($205) {
       break;
      }
     }
     $206 = HEAP32[((36976 + 16|0))>>2]|0;
     $207 = ($R7$1>>>0)<($206>>>0);
     if ($207) {
      _abort();
      // unreachable;
     }
     $208 = (($R7$1) + 24|0);
     HEAP32[$208>>2] = $161;
     $$sum19 = (($8) + 8)|0;
     $209 = (($mem) + ($$sum19)|0);
     $210 = HEAP32[$209>>2]|0;
     $211 = ($210|0)==(0|0);
     do {
      if (!($211)) {
       $212 = ($210>>>0)<($206>>>0);
       if ($212) {
        _abort();
        // unreachable;
       } else {
        $213 = (($R7$1) + 16|0);
        HEAP32[$213>>2] = $210;
        $214 = (($210) + 24|0);
        HEAP32[$214>>2] = $R7$1;
        break;
       }
      }
     } while(0);
     $$sum20 = (($8) + 12)|0;
     $215 = (($mem) + ($$sum20)|0);
     $216 = HEAP32[$215>>2]|0;
     $217 = ($216|0)==(0|0);
     if (!($217)) {
      $218 = HEAP32[((36976 + 16|0))>>2]|0;
      $219 = ($216>>>0)<($218>>>0);
      if ($219) {
       _abort();
       // unreachable;
      } else {
       $220 = (($R7$1) + 20|0);
       HEAP32[$220>>2] = $216;
       $221 = (($216) + 24|0);
       HEAP32[$221>>2] = $R7$1;
       break;
      }
     }
    }
   }
  } while(0);
  $222 = $133 | 1;
  $223 = (($p$0) + 4|0);
  HEAP32[$223>>2] = $222;
  $224 = (($p$0) + ($133)|0);
  HEAP32[$224>>2] = $133;
  $225 = HEAP32[((36976 + 20|0))>>2]|0;
  $226 = ($p$0|0)==($225|0);
  if ($226) {
   HEAP32[((36976 + 8|0))>>2] = $133;
   return;
  } else {
   $psize$1 = $133;
  }
 } else {
  $227 = $112 & -2;
  HEAP32[$111>>2] = $227;
  $228 = $psize$0 | 1;
  $229 = (($p$0) + 4|0);
  HEAP32[$229>>2] = $228;
  $230 = (($p$0) + ($psize$0)|0);
  HEAP32[$230>>2] = $psize$0;
  $psize$1 = $psize$0;
 }
 $231 = $psize$1 >>> 3;
 $232 = ($psize$1>>>0)<(256);
 if ($232) {
  $233 = $231 << 1;
  $234 = ((36976 + ($233<<2)|0) + 40|0);
  $235 = HEAP32[36976>>2]|0;
  $236 = 1 << $231;
  $237 = $235 & $236;
  $238 = ($237|0)==(0);
  if ($238) {
   $239 = $235 | $236;
   HEAP32[36976>>2] = $239;
   $$sum16$pre = (($233) + 2)|0;
   $$pre = ((36976 + ($$sum16$pre<<2)|0) + 40|0);
   $$pre$phiZ2D = $$pre;$F16$0 = $234;
  } else {
   $$sum17 = (($233) + 2)|0;
   $240 = ((36976 + ($$sum17<<2)|0) + 40|0);
   $241 = HEAP32[$240>>2]|0;
   $242 = HEAP32[((36976 + 16|0))>>2]|0;
   $243 = ($241>>>0)<($242>>>0);
   if ($243) {
    _abort();
    // unreachable;
   } else {
    $$pre$phiZ2D = $240;$F16$0 = $241;
   }
  }
  HEAP32[$$pre$phiZ2D>>2] = $p$0;
  $244 = (($F16$0) + 12|0);
  HEAP32[$244>>2] = $p$0;
  $245 = (($p$0) + 8|0);
  HEAP32[$245>>2] = $F16$0;
  $246 = (($p$0) + 12|0);
  HEAP32[$246>>2] = $234;
  return;
 }
 $247 = $psize$1 >>> 8;
 $248 = ($247|0)==(0);
 if ($248) {
  $I18$0 = 0;
 } else {
  $249 = ($psize$1>>>0)>(16777215);
  if ($249) {
   $I18$0 = 31;
  } else {
   $250 = (($247) + 1048320)|0;
   $251 = $250 >>> 16;
   $252 = $251 & 8;
   $253 = $247 << $252;
   $254 = (($253) + 520192)|0;
   $255 = $254 >>> 16;
   $256 = $255 & 4;
   $257 = $256 | $252;
   $258 = $253 << $256;
   $259 = (($258) + 245760)|0;
   $260 = $259 >>> 16;
   $261 = $260 & 2;
   $262 = $257 | $261;
   $263 = (14 - ($262))|0;
   $264 = $258 << $261;
   $265 = $264 >>> 15;
   $266 = (($263) + ($265))|0;
   $267 = $266 << 1;
   $268 = (($266) + 7)|0;
   $269 = $psize$1 >>> $268;
   $270 = $269 & 1;
   $271 = $270 | $267;
   $I18$0 = $271;
  }
 }
 $272 = ((36976 + ($I18$0<<2)|0) + 304|0);
 $273 = (($p$0) + 28|0);
 $I18$0$c = $I18$0;
 HEAP32[$273>>2] = $I18$0$c;
 $274 = (($p$0) + 20|0);
 HEAP32[$274>>2] = 0;
 $275 = (($p$0) + 16|0);
 HEAP32[$275>>2] = 0;
 $276 = HEAP32[((36976 + 4|0))>>2]|0;
 $277 = 1 << $I18$0;
 $278 = $276 & $277;
 $279 = ($278|0)==(0);
 L205: do {
  if ($279) {
   $280 = $276 | $277;
   HEAP32[((36976 + 4|0))>>2] = $280;
   HEAP32[$272>>2] = $p$0;
   $281 = (($p$0) + 24|0);
   HEAP32[$281>>2] = $272;
   $282 = (($p$0) + 12|0);
   HEAP32[$282>>2] = $p$0;
   $283 = (($p$0) + 8|0);
   HEAP32[$283>>2] = $p$0;
  } else {
   $284 = HEAP32[$272>>2]|0;
   $285 = ($I18$0|0)==(31);
   if ($285) {
    $293 = 0;
   } else {
    $286 = $I18$0 >>> 1;
    $287 = (25 - ($286))|0;
    $293 = $287;
   }
   $288 = (($284) + 4|0);
   $289 = HEAP32[$288>>2]|0;
   $290 = $289 & -8;
   $291 = ($290|0)==($psize$1|0);
   do {
    if ($291) {
     $T$0$lcssa = $284;
    } else {
     $292 = $psize$1 << $293;
     $K19$060 = $292;$T$059 = $284;
     while(1) {
      $300 = $K19$060 >>> 31;
      $301 = ((($T$059) + ($300<<2)|0) + 16|0);
      $296 = HEAP32[$301>>2]|0;
      $302 = ($296|0)==(0|0);
      if ($302) {
       $$lcssa = $301;$T$059$lcssa = $T$059;
       break;
      }
      $294 = $K19$060 << 1;
      $295 = (($296) + 4|0);
      $297 = HEAP32[$295>>2]|0;
      $298 = $297 & -8;
      $299 = ($298|0)==($psize$1|0);
      if ($299) {
       $$lcssa73 = $296;
       label = 137;
       break;
      } else {
       $K19$060 = $294;$T$059 = $296;
      }
     }
     if ((label|0) == 137) {
      $T$0$lcssa = $$lcssa73;
      break;
     }
     $303 = HEAP32[((36976 + 16|0))>>2]|0;
     $304 = ($$lcssa>>>0)<($303>>>0);
     if ($304) {
      _abort();
      // unreachable;
     } else {
      HEAP32[$$lcssa>>2] = $p$0;
      $305 = (($p$0) + 24|0);
      HEAP32[$305>>2] = $T$059$lcssa;
      $306 = (($p$0) + 12|0);
      HEAP32[$306>>2] = $p$0;
      $307 = (($p$0) + 8|0);
      HEAP32[$307>>2] = $p$0;
      break L205;
     }
    }
   } while(0);
   $308 = (($T$0$lcssa) + 8|0);
   $309 = HEAP32[$308>>2]|0;
   $310 = HEAP32[((36976 + 16|0))>>2]|0;
   $311 = ($T$0$lcssa>>>0)>=($310>>>0);
   $312 = ($309>>>0)>=($310>>>0);
   $or$cond = $311 & $312;
   if ($or$cond) {
    $313 = (($309) + 12|0);
    HEAP32[$313>>2] = $p$0;
    HEAP32[$308>>2] = $p$0;
    $314 = (($p$0) + 8|0);
    HEAP32[$314>>2] = $309;
    $315 = (($p$0) + 12|0);
    HEAP32[$315>>2] = $T$0$lcssa;
    $316 = (($p$0) + 24|0);
    HEAP32[$316>>2] = 0;
    break;
   } else {
    _abort();
    // unreachable;
   }
  }
 } while(0);
 $317 = HEAP32[((36976 + 32|0))>>2]|0;
 $318 = (($317) + -1)|0;
 HEAP32[((36976 + 32|0))>>2] = $318;
 $319 = ($318|0)==(0);
 if (!($319)) {
  return;
 }
 $sp$0$in$i = ((36976 + 456|0));
 while(1) {
  $sp$0$i = HEAP32[$sp$0$in$i>>2]|0;
  $320 = ($sp$0$i|0)==(0|0);
  $321 = (($sp$0$i) + 8|0);
  if ($320) {
   break;
  } else {
   $sp$0$in$i = $321;
  }
 }
 HEAP32[((36976 + 32|0))>>2] = -1;
 return;
}
function runPostSets() {
 
}
function _memset(ptr, value, num) {
    ptr = ptr|0; value = value|0; num = num|0;
    var stop = 0, value4 = 0, stop4 = 0, unaligned = 0;
    stop = (ptr + num)|0;
    if ((num|0) >= 20) {
      // This is unaligned, but quite large, so work hard to get to aligned settings
      value = value & 0xff;
      unaligned = ptr & 3;
      value4 = value | (value << 8) | (value << 16) | (value << 24);
      stop4 = stop & ~3;
      if (unaligned) {
        unaligned = (ptr + 4 - unaligned)|0;
        while ((ptr|0) < (unaligned|0)) { // no need to check for stop, since we have large num
          HEAP8[((ptr)>>0)]=value;
          ptr = (ptr+1)|0;
        }
      }
      while ((ptr|0) < (stop4|0)) {
        HEAP32[((ptr)>>2)]=value4;
        ptr = (ptr+4)|0;
      }
    }
    while ((ptr|0) < (stop|0)) {
      HEAP8[((ptr)>>0)]=value;
      ptr = (ptr+1)|0;
    }
    return (ptr-num)|0;
}
function _strlen(ptr) {
    ptr = ptr|0;
    var curr = 0;
    curr = ptr;
    while (((HEAP8[((curr)>>0)])|0)) {
      curr = (curr + 1)|0;
    }
    return (curr - ptr)|0;
}
function _memcpy(dest, src, num) {

    dest = dest|0; src = src|0; num = num|0;
    var ret = 0;
    if ((num|0) >= 4096) return _emscripten_memcpy_big(dest|0, src|0, num|0)|0;
    ret = dest|0;
    if ((dest&3) == (src&3)) {
      while (dest & 3) {
        if ((num|0) == 0) return ret|0;
        HEAP8[((dest)>>0)]=((HEAP8[((src)>>0)])|0);
        dest = (dest+1)|0;
        src = (src+1)|0;
        num = (num-1)|0;
      }
      while ((num|0) >= 4) {
        HEAP32[((dest)>>2)]=((HEAP32[((src)>>2)])|0);
        dest = (dest+4)|0;
        src = (src+4)|0;
        num = (num-4)|0;
      }
    }
    while ((num|0) > 0) {
      HEAP8[((dest)>>0)]=((HEAP8[((src)>>0)])|0);
      dest = (dest+1)|0;
      src = (src+1)|0;
      num = (num-1)|0;
    }
    return ret|0;
}

  
function dynCall_vi(index,a1) {
  index = index|0;
  a1=a1|0;
  FUNCTION_TABLE_vi[index&7](a1|0);
}


function dynCall_v(index) {
  index = index|0;
  
  FUNCTION_TABLE_v[index&7]();
}

function b0(p0) { p0 = p0|0; nullFunc_vi(0); }
function b1() { ; nullFunc_v(1); }

// EMSCRIPTEN_END_FUNCS
var FUNCTION_TABLE_vi = [b0,_MenuFn_StartMission,_MenuFn_NavigateTo,_MenuFn_Continue,_MenuFn_RestartMission,_MenuFn_ExitMission,b0,b0];
var FUNCTION_TABLE_v = [b1,b1,b1,b1,b1,b1,_Kernel_Update,_VsyncCallBack];

  return { _strlen: _strlen, _free: _free, _main: _main, _memset: _memset, _malloc: _malloc, _memcpy: _memcpy, runPostSets: runPostSets, stackAlloc: stackAlloc, stackSave: stackSave, stackRestore: stackRestore, setThrew: setThrew, setTempRet0: setTempRet0, getTempRet0: getTempRet0, dynCall_vi: dynCall_vi, dynCall_v: dynCall_v };
})
// EMSCRIPTEN_END_ASM
(Module.asmGlobalArg, Module.asmLibraryArg, buffer);
var real__strlen = asm["_strlen"]; asm["_strlen"] = function() {
assert(runtimeInitialized, 'you need to wait for the runtime to be ready (e.g. wait for main() to be called)');
assert(!runtimeExited, 'the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)');
return real__strlen.apply(null, arguments);
};

var real__main = asm["_main"]; asm["_main"] = function() {
assert(runtimeInitialized, 'you need to wait for the runtime to be ready (e.g. wait for main() to be called)');
assert(!runtimeExited, 'the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)');
return real__main.apply(null, arguments);
};

var real_runPostSets = asm["runPostSets"]; asm["runPostSets"] = function() {
assert(runtimeInitialized, 'you need to wait for the runtime to be ready (e.g. wait for main() to be called)');
assert(!runtimeExited, 'the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)');
return real_runPostSets.apply(null, arguments);
};
var _strlen = Module["_strlen"] = asm["_strlen"];
var _free = Module["_free"] = asm["_free"];
var _main = Module["_main"] = asm["_main"];
var _memset = Module["_memset"] = asm["_memset"];
var _malloc = Module["_malloc"] = asm["_malloc"];
var _memcpy = Module["_memcpy"] = asm["_memcpy"];
var runPostSets = Module["runPostSets"] = asm["runPostSets"];
var dynCall_vi = Module["dynCall_vi"] = asm["dynCall_vi"];
var dynCall_v = Module["dynCall_v"] = asm["dynCall_v"];

Runtime.stackAlloc = asm['stackAlloc'];
Runtime.stackSave = asm['stackSave'];
Runtime.stackRestore = asm['stackRestore'];
Runtime.setTempRet0 = asm['setTempRet0'];
Runtime.getTempRet0 = asm['getTempRet0'];


// Warning: printing of i64 values may be slightly rounded! No deep i64 math used, so precise i64 code not included
var i64Math = null;

// === Auto-generated postamble setup entry stuff ===

if (memoryInitializer) {
  if (typeof Module['locateFile'] === 'function') {
    memoryInitializer = Module['locateFile'](memoryInitializer);
  } else if (Module['memoryInitializerPrefixURL']) {
    memoryInitializer = Module['memoryInitializerPrefixURL'] + memoryInitializer;
  }
  if (ENVIRONMENT_IS_NODE || ENVIRONMENT_IS_SHELL) {
    var data = Module['readBinary'](memoryInitializer);
    HEAPU8.set(data, STATIC_BASE);
  } else {
    addRunDependency('memory initializer');
    function applyMemoryInitializer(data) {
      if (data.byteLength) data = new Uint8Array(data);
      for (var i = 0; i < data.length; i++) {
        assert(HEAPU8[STATIC_BASE + i] === 0, "area for memory initializer should not have been touched before it's loaded");
      }
      HEAPU8.set(data, STATIC_BASE);
      removeRunDependency('memory initializer');
    }
    var request = Module['memoryInitializerRequest'];
    if (request) {
      // a network request has already been created, just use that
      if (request.response) {
        setTimeout(function() {
          applyMemoryInitializer(request.response);
        }, 0); // it's already here; but, apply it asynchronously
      } else {
        request.addEventListener('load', function() { // wait for it
          if (request.status !== 200 && request.status !== 0) {
            console.warn('a problem seems to have happened with Module.memoryInitializerRequest, status: ' + request.status);
          }
          if (!request.response || typeof request.response !== 'object' || !request.response.byteLength) {
            console.warn('a problem seems to have happened with Module.memoryInitializerRequest response (expected ArrayBuffer): ' + request.response);
          }
          applyMemoryInitializer(request.response);
        });
      }
    } else {
      // fetch it from the network ourselves
      Browser.asyncLoad(memoryInitializer, applyMemoryInitializer, function() {
        throw 'could not load memory initializer ' + memoryInitializer;
      });
    }
  }
}

function ExitStatus(status) {
  this.name = "ExitStatus";
  this.message = "Program terminated with exit(" + status + ")";
  this.status = status;
};
ExitStatus.prototype = new Error();
ExitStatus.prototype.constructor = ExitStatus;

var initialStackTop;
var preloadStartTime = null;
var calledMain = false;

dependenciesFulfilled = function runCaller() {
  // If run has never been called, and we should call run (INVOKE_RUN is true, and Module.noInitialRun is not false)
  if (!Module['calledRun']) run();
  if (!Module['calledRun']) dependenciesFulfilled = runCaller; // try this again later, after new deps are fulfilled
}

Module['callMain'] = Module.callMain = function callMain(args) {
  assert(runDependencies == 0, 'cannot call main when async dependencies remain! (listen on __ATMAIN__)');
  assert(__ATPRERUN__.length == 0, 'cannot call main when preRun functions remain to be called');

  args = args || [];

  ensureInitRuntime();

  var argc = args.length+1;
  function pad() {
    for (var i = 0; i < 4-1; i++) {
      argv.push(0);
    }
  }
  var argv = [allocate(intArrayFromString(Module['thisProgram']), 'i8', ALLOC_NORMAL) ];
  pad();
  for (var i = 0; i < argc-1; i = i + 1) {
    argv.push(allocate(intArrayFromString(args[i]), 'i8', ALLOC_NORMAL));
    pad();
  }
  argv.push(0);
  argv = allocate(argv, 'i32', ALLOC_NORMAL);

  initialStackTop = STACKTOP;

  try {

    var ret = Module['_main'](argc, argv, 0);


    // if we're not running an evented main loop, it's time to exit
    exit(ret);
  }
  catch(e) {
    if (e instanceof ExitStatus) {
      // exit() throws this once it's done to make sure execution
      // has been stopped completely
      return;
    } else if (e == 'SimulateInfiniteLoop') {
      // running an evented main loop, don't immediately exit
      Module['noExitRuntime'] = true;
      return;
    } else {
      if (e && typeof e === 'object' && e.stack) Module.printErr('exception thrown: ' + [e, e.stack]);
      throw e;
    }
  } finally {
    calledMain = true;
  }
}




function run(args) {
  args = args || Module['arguments'];

  if (preloadStartTime === null) preloadStartTime = Date.now();

  if (runDependencies > 0) {
    Module.printErr('run() called, but dependencies remain, so not running');
    return;
  }

  preRun();

  if (runDependencies > 0) return; // a preRun added a dependency, run will be called later
  if (Module['calledRun']) return; // run may have just been called through dependencies being fulfilled just in this very frame

  function doRun() {
    if (Module['calledRun']) return; // run may have just been called while the async setStatus time below was happening
    Module['calledRun'] = true;

    if (ABORT) return; 

    ensureInitRuntime();

    preMain();

    if (ENVIRONMENT_IS_WEB && preloadStartTime !== null) {
      Module.printErr('pre-main prep time: ' + (Date.now() - preloadStartTime) + ' ms');
    }

    if (Module['onRuntimeInitialized']) Module['onRuntimeInitialized']();

    if (Module['_main'] && shouldRunNow) Module['callMain'](args);

    postRun();
  }

  if (Module['setStatus']) {
    Module['setStatus']('Running...');
    setTimeout(function() {
      setTimeout(function() {
        Module['setStatus']('');
      }, 1);
      doRun();
    }, 1);
  } else {
    doRun();
  }
}
Module['run'] = Module.run = run;

function exit(status) {
  if (Module['noExitRuntime']) {
    Module.printErr('exit(' + status + ') called, but noExitRuntime, so not exiting (you can use emscripten_force_exit, if you want to force a true shutdown)');
    return;
  }

  ABORT = true;
  EXITSTATUS = status;
  STACKTOP = initialStackTop;

  // exit the runtime
  exitRuntime();

  if (Module['onExit']) Module['onExit'](status);

  if (ENVIRONMENT_IS_NODE) {
    // Work around a node.js bug where stdout buffer is not flushed at process exit:
    // Instead of process.exit() directly, wait for stdout flush event.
    // See https://github.com/joyent/node/issues/1669 and https://github.com/kripken/emscripten/issues/2582
    // Workaround is based on https://github.com/RReverser/acorn/commit/50ab143cecc9ed71a2d66f78b4aec3bb2e9844f6
    process['stdout']['once']('drain', function () {
      process['exit'](status);
    });
    console.log(' '); // Make sure to print something to force the drain event to occur, in case the stdout buffer was empty.
    // Work around another node bug where sometimes 'drain' is never fired - make another effort
    // to emit the exit status, after a significant delay (if node hasn't fired drain by then, give up)
    setTimeout(function() {
      process['exit'](status);
    }, 500);
  } else
  if (ENVIRONMENT_IS_SHELL && typeof quit === 'function') {
    quit(status);
  }
  // if we reach here, we must throw an exception to halt the current execution
  throw new ExitStatus(status);
}
Module['exit'] = Module.exit = exit;

var abortDecorators = [];

function abort(what) {
  if (what !== undefined) {
    Module.print(what);
    Module.printErr(what);
    what = JSON.stringify(what)
  } else {
    what = '';
  }

  ABORT = true;
  EXITSTATUS = 1;

  var extra = '';

  var output = 'abort(' + what + ') at ' + stackTrace() + extra;
  abortDecorators.forEach(function(decorator) {
    output = decorator(output, what);
  });
  throw output;
}
Module['abort'] = Module.abort = abort;

// {{PRE_RUN_ADDITIONS}}

if (Module['preInit']) {
  if (typeof Module['preInit'] == 'function') Module['preInit'] = [Module['preInit']];
  while (Module['preInit'].length > 0) {
    Module['preInit'].pop()();
  }
}

// shouldRunNow refers to calling main(), not run().
var shouldRunNow = true;
if (Module['noInitialRun']) {
  shouldRunNow = false;
}


run();

// {{POST_RUN_ADDITIONS}}






// {{MODULE_ADDITIONS}}



