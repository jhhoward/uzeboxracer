###############################################################################
# Makefile for the project Joyrider (Emscripten SDL port)
###############################################################################

## General Flags
PROJECT = UzeboxRacer
GAME = Joyrider
TARGET = joyrider.html
CC = emcc
INFO=../gameinfo.properties
#UZEBIN_DIR=c:/uzebox/bin/

## Kernel settings
KERNEL_DIR = ../SDLkernel/

KERNEL_OPTIONS  = -DVIDEO_MODE=3 -DINTRO_LOGO=1 -DSOUND_CHANNEL_4_ENABLE=1 -DSCROLLING=1 -DSOUND_MIXER=1
KERNEL_OPTIONS += -DMAX_SPRITES=18 -DRAM_TILES_COUNT=34 -DSCREEN_TILES_V=27 -DFIRST_RENDER_LINE=24 
KERNEL_OPTIONS += -DVRAM_TILES_V=32

KERNEL_OPTIONS += -DSPRITE_COLORMASK_ENABLE=1
KERNEL_OPTIONS += -DENABLE_SPRITE_CYCLING=0

## Options common to compile, link and assembly rules
COMMON = -O3 --memory-init-file 0

## Compile options common for all C compilation units.
CFLAGS = $(COMMON)
CFLAGS += -Wall -std=gnu99 -fsigned-char -ffunction-sections -fno-inline -g 
CFLAGS += -MD -MP -MT $(*F).o -MF dep/$(@F).d 
#CFLAGS += -O3
CFLAGS += $(KERNEL_OPTIONS)


## For debugging
#CFLAGS += -DDEBUG=1

## Assembly specific flags
#ASMFLAGS = $(COMMON)
#ASMFLAGS += $(CFLAGS)
#ASMFLAGS += -x assembler-with-cpp -Wa,-gdwarf2

## Debug options
#ASMFLAGS += -g3 -Wa,-gdwarf2
#CFLAGS += -g3 -gdwarf-2

## Linker flags
LDFLAGS = $(COMMON)
#LDFLAGS += -Wl,-Map=$(GAME).map 
#LDFLAGS += -Wl,-gc-sections 
#LDFLAGS += -Wl,--section-start,.noinit=0x800100 -Wl,--section-start,.data=0x800520

## Link time optimisation
#LDFLAGS += -flto

## Intel Hex file production flags
HEX_FLASH_FLAGS = -R .eeprom

HEX_EEPROM_FLAGS = -j .eeprom
HEX_EEPROM_FLAGS += --set-section-flags=.eeprom="alloc,load"
HEX_EEPROM_FLAGS += --change-section-lma .eeprom=0 --no-change-warnings


## Objects that must be built in order to link
SDL_OBJECTS = 
KERNEL_OBJECTS = uzeboxSDL.o kernel.o uzeboxVideoEngine.o uzeboxCore.o
GAME_OBJECTS = World.o Car.o Track.o Menu.o Main.o FixedMath.o Sprites.o AI.o Ped.o Mission.o UI.o
OBJECTS = $(SDL_OBJECTS) $(KERNEL_OBJECTS) $(GAME_OBJECTS)

## Objects explicitly added by the user
LINKONLYOBJECTS = -lSDL

## Include Directories
#SDL_INCLUDES = -I/Library/Frameworks/SDL.framework/Headers
SDL_INCLUDES = 
INCLUDES = $(SDL_INCLUDES) -I"$(KERNEL_DIR)" -I".."

## Build
all: $(TARGET)

## Rebuild graphics resources
../data/graphics.inc.h: ../data/template.png ../data/graphics.xml
	$(UZEBIN_DIR)gconvert ../data/graphics.xml

../data/sprites.inc.h: ../data/sprites.png ../data/sprites.xml
	$(UZEBIN_DIR)gconvert ../data/sprites.xml

../data/worldLayout.inc.h: ../data/layout.png ../data/worldLayout.xml
	$(UZEBIN_DIR)gconvert ../data/worldLayout.xml

## Compile Kernel files
uzeboxSDL.o: $(KERNEL_DIR)/uzeboxSDL.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
uzeboxCore.o: $(KERNEL_DIR)/uzeboxCore.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
uzeboxVideoEngine.o: $(KERNEL_DIR)/uzeboxVideoEngine.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
kernel.o: $(KERNEL_DIR)/kernel.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

## Compile game sources
Main.o: ../src/Main.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
World.o: ../src/World.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
Car.o: ../src/Car.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
Track.o: ../src/Track.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
Menu.o: ../src/Menu.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
FixedMath.o: ../src/FixedMath.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
Sprites.o: ../src/Sprites.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
AI.o: ../src/AI.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
Ped.o: ../src/Ped.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
Mission.o: ../src/Mission.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<
UI.o: ../src/UI.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

##Link
$(TARGET): $(OBJECTS)
	 $(CC) $(LDFLAGS) $(OBJECTS) $(LINKONLYOBJECTS) $(LIBDIRS) $(LIBS) -o $(TARGET)

%.hex: $(TARGET)
	avr-objcopy -O ihex $(HEX_FLASH_FLAGS)  $< $@

%.eep: $(TARGET)
	-avr-objcopy $(HEX_EEPROM_FLAGS) -O ihex $< $@ || exit 0

%.lss: $(TARGET)
	avr-objdump -h -S $< > $@

%.uze: $(TARGET)
	-$(UZEBIN_DIR)packrom $(GAME).hex $@ $(INFO)

UNAME := $(shell sh -c 'uname -s 2>/dev/null || echo not')
AVRSIZEFLAGS := -A ${TARGET}
ifneq (,$(findstring MINGW,$(UNAME)))
AVRSIZEFLAGS := -C --mcu=${MCU} ${TARGET}
endif

size: ${TARGET}
	@echo
	@avr-size ${AVRSIZEFLAGS}

## Clean target
.PHONY: clean
clean:
	-rm -rf $(OBJECTS) $(GAME).* dep/* *.uze


## Other dependencies
-include $(shell mkdir dep 2>/dev/null) $(wildcard dep/*)

