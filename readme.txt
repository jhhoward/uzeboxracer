       __                 _     __         
      / /___  __  _______(_)___/ /__  _____
 __  / / __ \/ / / / ___/ / __  / _ \/ ___/
/ /_/ / /_/ / /_/ / /  / / /_/ /  __/ /    
\____/\____/\__, /_/  /_/\__,_/\___/_/     
           /____/                          

Overview
--------
Joyrider is an action / driving game for the Uzebox, in the style of the 
original Grand Theft Auto and Driver games, created by James Howard for
the Uzebox Coding Competition 2014.

Game Modes
----------
There are several game modes available:

Story      : Features three missions with specific objectives
Free roam  : Freely explore the city at your own leisure
Survival [Arcade] : With the full force of the police after you, 
                    survive as long as you can
Pursuit [Arcade]  : Play as the police and chase down the suspect,
                    ramming them off the road
Pursuit [2 Player]: Player 1 plays as the police, whilst Player 2
                    tries to escape

Controls
--------
There are two control schemes available for playing with:
Standard:
[Dpad] : Steer car left or right
[A] : Accelerate
[B] : Brake

Alternate / Casual:
[Dpad] : Steer the car North, East, South or West
[X] : Drive in direction of dpad input

[Start] : Pause game / view map

Compilation
-----------
Joyrider uses a custom version of the Uzebox kernel which is included
with the source code. The main difference is the custom sprite blitting
code which tints the colours of the sprites.

The Makefile found in the 'default' folder should handle all the
compilation, including the building of the kernel and tile / sprite data.

An assumption is made that the Uzebox tools gconvert and packrom are
installed at the location C:\uzebox\bin
This can be changed in the Makefile by editing the UZEBIN_DIR variable

Compilation has only be tested on Windows, but it should be relatively
easy to compile on other platforms.

License
-------
Code is released under the GNU General Public License (GPLv3)
For further information please refer to gpl.txt

Contact
-------
James Howard: jhhoward@gmail.com
