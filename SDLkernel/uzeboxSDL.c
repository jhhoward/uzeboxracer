#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif
#include "uzebox.h"

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

void Initialize();

/**
* Fills the entire VRAM with tile #0.
*
* Before calling this function, you must invoke SetTileTable() to specify the tile set to use.
*/
void ClearVram(void) {}

void ClearVsyncFlag(void) {}
u8 GetVsyncFlag(void) { return 0; }


/*
* Sound Engine functions
*/	
void SetMasterVolume(unsigned char vol) {}
u8 GetMasterVolume() { return 0; }
void TriggerNote(unsigned char channel,unsigned char patch,unsigned char note,unsigned char volume) {}
void TriggerFx(unsigned char patch,unsigned char volume, bool retrig) {}
void StopSong() {}
void StartSong(const char *midiSong) {}
void ResumeSong() {}
void InitMusicPlayer(const struct PatchStruct *patchPointersParam) {}
void EnableSoundEngine() {}
void DisableSoundEngine() {}
void SetSongSpeed(u8 speed) {}
u8 GetSongSpeed() { return 0; }

unsigned int ReadJoypadExt(unsigned char joypadNo) { return 0; }
char EnableSnesMouse(unsigned char spriteIndex,const char *spriteMap) { return 0; }
bool SetMouseSensitivity(unsigned char value) { return false; }
unsigned char GetMouseSensitivity() { return 0; }
unsigned char GetMouseX() { return 0; }
unsigned char GetMouseY() { return 0; }
unsigned int GetActionButton() { return 0; }
unsigned char DetectControllers() { return 0; }


/*
* UART RX buffer
*/
void UartInitRxBuffer() {}
void UartGoBack(unsigned char count) {}
unsigned char UartUnreadCount() { return 0; }
unsigned char UartReadChar() { return 0; }

/*
* Misc functions
*/
void WaitClocks(u16 clocks) {}
void WaitUs(unsigned int microseconds) {}
bool IsRunningInEmulator(void) { return false; }
void SetUserPreVsyncCallback(VsyncCallBackFunc func) {}
void SetUserPostVsyncCallback(VsyncCallBackFunc func) {}

#define MAX_FRAME_SKIP 15 
#define TARGET_FPS 60
#define TARGET_FRAME_TIME (16)

SDL_Surface* Screen_Surface = NULL;
bool Program_Running = true;
Uint32 Kernel_LastTime = 0;
int Kernel_TicksBehind = 0;

void Game_Init();
void Game_Update();

void Video_Render();

int SDL_KeyboardButtonMappings[] = 
{
	SDLK_x, BTN_A, 
	SDLK_z, BTN_B, 
	SDLK_s, BTN_X, 
	SDLK_a, BTN_Y, 
	SDLK_q, BTN_SL, 
	SDLK_w, BTN_SR, 
	SDLK_LEFT, BTN_LEFT,
	SDLK_RIGHT, BTN_RIGHT,
	SDLK_UP, BTN_UP,
	SDLK_DOWN, BTN_DOWN,
	SDLK_RETURN, BTN_START,
	SDLK_TAB, BTN_SELECT
};

unsigned int JoypadState = 0;

unsigned int ReadJoypad(unsigned char joypadNo) 
{ 
	return JoypadState; 
}

void UpdateJoypad(unsigned int* state, int* mappings, int numMappings, int eventType, bool pressed)
{
	for(int n = 0; n < numMappings; n++)
	{
		if(mappings[n * 2] == eventType)
		{
			if(pressed)
			{
				*state |= mappings[n * 2 + 1];
			}
			else
			{
				*state &= ~(mappings[n * 2 + 1]);
			}
		}
	}
}

void Kernel_Init()
{
	SDL_Init(SDL_INIT_EVERYTHING);
	Screen_Surface = SDL_SetVideoMode( SCREEN_TILES_H * TILE_WIDTH, SCREEN_TILES_V * TILE_HEIGHT, 32, SDL_SWSURFACE );
	
	Initialize();
	Game_Init();
	
	Kernel_LastTime = SDL_GetTicks();
}

void Kernel_Update()
{
	SDL_Event event;

	Uint32 ticks = SDL_GetTicks();
	int ticksPassed = ticks - Kernel_LastTime;
	Kernel_LastTime = ticks;
	
	Kernel_TicksBehind += ticksPassed;

	if(Kernel_TicksBehind > MAX_FRAME_SKIP * TARGET_FRAME_TIME)
	{
		Kernel_TicksBehind = MAX_FRAME_SKIP * TARGET_FRAME_TIME;
	}	

	while(Kernel_TicksBehind > 0)
	{
		Game_Update();
		Kernel_TicksBehind -= TARGET_FRAME_TIME;
	}
	
    while(SDL_PollEvent(&event))
    {
        switch(event.type) 
        {
            case SDL_QUIT:
			Program_Running = false;
			break;
			case SDL_KEYDOWN:
			UpdateJoypad(&JoypadState, SDL_KeyboardButtonMappings, sizeof(SDL_KeyboardButtonMappings) / 2, event.key.keysym.sym, true);
			break;
			case SDL_KEYUP:
			UpdateJoypad(&JoypadState, SDL_KeyboardButtonMappings, sizeof(SDL_KeyboardButtonMappings) / 2, event.key.keysym.sym, false);
			break;
			default:
			break;
		}
	}
	
	Video_Render();
	SDL_Flip(Screen_Surface);
}

int main()
{
	Kernel_Init();

#ifdef __EMSCRIPTEN__
	emscripten_set_main_loop(Kernel_Update, 0, 1);
#else
	while(Program_Running)
	{
		Kernel_Update();
		SDL_Delay(0);
		//Uint32 ticks = SDL_GetTicks();

//		Kernel_Update();		

		/*float tickDiff = SDL_GetTicks() - ticks;
		float frameTime = 1000.0f / 60.0f;
		
		if(tickDiff < frameTime)
		{
			SDL_Delay((Uint32)(frameTime - tickDiff));
		}*/			
	}
#endif
	
	return 0;
}
