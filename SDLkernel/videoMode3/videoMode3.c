/*
 *  Uzebox Kernel - Mode 3
 *  Copyright (C) 2008  Alec Bourque
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Uzebox is a reserved trade mark
*/
#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif

	#include <stdbool.h>
	#include <stdlib.h>
	#include <avr/pgmspace.h>
	#include "uzebox.h"
	#include "intro.h"
	

	#if INTRO_LOGO !=0
		#include "videoMode3/uzeboxlogo_8x8.pic.inc"
		#include "videoMode3/uzeboxlogo_8x8.map.inc"
	#endif

	u8 vram[VRAM_TILES_H * VRAM_TILES_V];
	u8* tile_table = NULL;

	struct SpriteStruct sprites[MAX_SPRITES];
	unsigned char *sprites_tiletable_lo;
	extern unsigned int sprites_tile_banks[];

	ScreenType Screen;
	bool spritesOn = true;

	void SetSpritesTileBank(u8 bank,const char* tileData) {}

	void CopyTileToRam(unsigned char romTile,unsigned char ramTile)
	{
	}
	void BlitSprite(unsigned char spriteNo,unsigned char ramTileNo,unsigned int xy,unsigned int dxdy)
	{
		
	}

	void SetSpriteVisibility(bool visible){
		spritesOn=visible;
	}

	/*
	//
	// This C function is the direct equivalent of the assembly
	// function of the same same.
	//
	void BlitSprite(u8 sprNo,u8 ramTileIndex,u16 yx,u16 dydx){
		u8 dy=dydx>>8;
		u8 dx=dydx &0xff;
		u8 flags=sprites[sprNo].flags;
		u8 destXdiff,ydiff,px,x2,y2;
		s8 step=1,srcXdiff;

		u16 src=(sprites[sprNo].tileIndex*TILE_HEIGHT*TILE_WIDTH)
				+sprites_tile_banks[flags>>6];	//add bank adress		

		u8* dest=&ram_tiles[ramTileIndex*TILE_HEIGHT*TILE_WIDTH];
	
		if((yx&1)==0){
			dest+=dx;	
			destXdiff=dx;
			srcXdiff=dx;
					
			if(flags&SPRITE_FLIP_X){
				src+=(TILE_WIDTH-1);
				srcXdiff=((TILE_WIDTH*2)-dx);
			}
		}else{
			destXdiff=(TILE_WIDTH-dx);

			if(flags&SPRITE_FLIP_X){
				srcXdiff=TILE_WIDTH+dx;
				src+=dx;
				src--;
			}else{
				srcXdiff=destXdiff;
				src+=destXdiff;
			}
		}
	

		if((yx&0x0100)==0){
			dest+=(dy*TILE_WIDTH);
			ydiff=dy;
			if(flags&SPRITE_FLIP_Y){
				src+=(TILE_WIDTH*(TILE_HEIGHT-1));
			}
		}else{			
			ydiff=(TILE_HEIGHT-dy);
			if(flags&SPRITE_FLIP_Y){
				src+=((dy-1)*TILE_WIDTH); 
			}else{
				src+=(ydiff*TILE_WIDTH);
			}
		}

		if(flags&SPRITE_FLIP_X){
			step=-1;
		}
		
		if(flags&SPRITE_FLIP_Y){
			srcXdiff-=(TILE_WIDTH*2);
		}

		for(y2=0;y2<(TILE_HEIGHT-ydiff);y2++){
			for(x2=0;x2<(TILE_WIDTH-destXdiff);x2++){
							
				px=pgm_read_byte(src);
				if(px!=TRANSLUCENT_COLOR){
					*dest=px;
				}
				dest++;
				src+=step;
			}		
			src+=srcXdiff;
			dest+=destXdiff;
		}

	}
	*/

	#if SCROLLING == 1
		//Scroll the screen by the relative amount specified (+/-)
		//This function handles screen wrapping on the Y axis if VRAM_TILES_V is less than 32
		void Scroll(char dx,char dy){
		Screen.scrollY+=dy;
		Screen.scrollX+=dx;

		if(Screen.scrollHeight<32){

			if(Screen.scrollY>=(Screen.scrollHeight*TILE_HEIGHT)){
				if(dy>=0){	
					Screen.scrollY=(Screen.scrollY-(Screen.scrollHeight*TILE_HEIGHT));
				}else{
					Screen.scrollY=((Screen.scrollHeight*TILE_HEIGHT)-1)-(0xff-Screen.scrollY);
				}			
			}
	
		}

		}

		//position the scrolling is absolute value
		void SetScrolling(char sx,char sy){

			Screen.scrollX=sx;

			if(Screen.scrollHeight<32){
				if(sy<(Screen.scrollHeight*TILE_HEIGHT)){
					Screen.scrollY=sy;
				}
			}else{
				Screen.scrollY=sy;
			}
		}
	#endif

	
	void MapSprite(unsigned char startSprite,const char *map){
		unsigned char tile;
		unsigned char mapWidth=pgm_read_byte(&(map[0]));
		unsigned char mapHeight=pgm_read_byte(&(map[1]));

		for(unsigned char dy=0;dy<mapHeight;dy++){
			for(unsigned char dx=0;dx<mapWidth;dx++){
		
			 	tile=pgm_read_byte(&(map[(dy*mapWidth)+dx+2]));		
				sprites[startSprite++].tileIndex=tile ;
			}
		}

	}


	void MapSprite2(unsigned char startSprite,const char *map,u8 spriteFlags){	
		unsigned char tile;
		unsigned char mapWidth=pgm_read_byte(&(map[0]));
		unsigned char mapHeight=pgm_read_byte(&(map[1]));

		
		
		if(spriteFlags & SPRITE_FLIP_X){
			for(unsigned char dy=0;dy<mapHeight;dy++){
				for(s8 dx=(mapWidth-1);dx>=0;dx--){
				 	tile=pgm_read_byte(&(map[(dy*mapWidth)+dx+2]));		
					sprites[startSprite].tileIndex=tile ;
					sprites[startSprite++].flags=spriteFlags;
				}			
			}
		}else{
			for(unsigned char dy=0;dy<mapHeight;dy++){
				for(unsigned char dx=0;dx<mapWidth;dx++){
				 	tile=pgm_read_byte(&(map[(dy*mapWidth)+dx+2]));		
					sprites[startSprite].tileIndex=tile;
					sprites[startSprite++].flags=spriteFlags;
				}			
			}
		}
	

	}


	void MoveSprite(unsigned char startSprite,unsigned char x,unsigned char y,unsigned char width,unsigned char height){

		for(unsigned char dy=0;dy<height;dy++){
			for(unsigned char dx=0;dx<width;dx++){
			
				sprites[startSprite].x=x+(TILE_WIDTH*dx);
			
				#if SCROLLING == 1
					if((Screen.scrollHeight<32) && (y+(TILE_HEIGHT*dy))>(Screen.scrollHeight*TILE_HEIGHT)){
						unsigned char tmp=(y+(TILE_HEIGHT*dy))-(Screen.scrollHeight*TILE_HEIGHT);
						sprites[startSprite].y=tmp;
					}else{
						sprites[startSprite].y=y+(TILE_HEIGHT*dy);
					}
				#else
					if((VRAM_TILES_V<32) && (y+(TILE_HEIGHT*dy))>(VRAM_TILES_V*TILE_HEIGHT)){
						unsigned char tmp=(y+(TILE_HEIGHT*dy))-(VRAM_TILES_V*TILE_HEIGHT);
						sprites[startSprite].y=tmp;
					}else{
						sprites[startSprite].y=y+(TILE_HEIGHT*dy);
					}
				#endif

				startSprite++;
			}
		}	

	}

	//Callback invoked by UzeboxCore.Initialize()
	void DisplayLogo(){
	
		#if INTRO_LOGO !=0
			#define LOGO_X_POS 12
			
			InitMusicPlayer(logoInitPatches);
			SetTileTable(logo_tileset);
			
			//draw logo
			ClearVram();
			WaitVsync(15);		


			#if INTRO_LOGO == 1 
				TriggerFx(0,0xff,true);
			#endif

			DrawMap2(LOGO_X_POS,12,map_uzeboxlogo);
			WaitVsync(3);
			DrawMap2(LOGO_X_POS,12,map_uzeboxlogo2);
			WaitVsync(2);
			DrawMap2(LOGO_X_POS,12,map_uzeboxlogo);

			#if INTRO_LOGO == 2
				SetMasterVolume(0xc0);
				TriggerNote(3,0,16,0xff);
			#endif 
		
			WaitVsync(65);
			ClearVram();
			WaitVsync(20);
		#endif	
	}


	//Callback invoked during hsync
	void VideoModeVsync(){
	}
	
unsigned char Video_FontTilesIndex = 0;
	
void SetFont(char x,char y, unsigned char tileId) 
{
	SetTile(x, y, tileId + Video_FontTilesIndex);
}

void SetFontTilesIndex(unsigned char index) 
{
	Video_FontTilesIndex = index;
}

void SetFontTable(const char *data) 
{
	
}

void SetTile(char x,char y, unsigned int tileId) 
{
	vram[y * VRAM_TILES_H + x] = (u8) tileId;
}

#define TILE_WIDTH 8
#define TILE_HEIGHT 8

#define NUM_TILE_ORIENTATIONS 4
#define NUM_COLOR_VARIATIONS 256

typedef struct
{
	u8* srcData;
	SDL_Surface** surfaces;
	int numSurfaces;
	bool spriteBank;
} SurfaceBank;

SurfaceBank Video_TileBank;
SurfaceBank Video_OverlayTileBank;
SurfaceBank Video_SpriteBank;

SDL_Surface* Video_Buffer;
extern SDL_Surface* Screen_Surface;

void Video_SetBank(SurfaceBank* bank, u8* data)
{
	bank->srcData = data;
	
	for(int n = 0; n < bank->numSurfaces; n++)
	{
		if(bank->surfaces[n])
		{
			SDL_FreeSurface(bank->surfaces[n]);
			bank->surfaces[n] = NULL;
		}
	}
}

void Video_InitBank(SurfaceBank* bank, bool spriteBank)
{
	bank->numSurfaces = spriteBank ? 256 * NUM_TILE_ORIENTATIONS * NUM_COLOR_VARIATIONS : 256;
	bank->surfaces = (SDL_Surface**)malloc(sizeof(SDL_Surface*) * bank->numSurfaces);
	memset(bank->surfaces, 0, sizeof(SDL_Surface*) * bank->numSurfaces);
	bank->srcData = NULL;
	bank->spriteBank = spriteBank;
}

void SetSpritesTileTable(const char *data)
{
	Video_SetBank(&Video_SpriteBank, (u8*)data);
}

void SetTileTable(const char *data) 
{
	Video_SetBank(&Video_TileBank, (u8*)data);
}

//Callback invoked by UzeboxCore.Initialize()
void InitializeVideoMode(){
	//disable sprites
	for(int i=0;i<MAX_SPRITES;i++){
		sprites[i].x=(SCREEN_TILES_H*TILE_WIDTH);		
	}
	
	#if SCROLLING == 1
		Screen.scrollHeight=VRAM_TILES_V;
		Screen.overlayHeight=0;
	#endif

	Video_InitBank(&Video_TileBank, false);
	Video_InitBank(&Video_OverlayTileBank, false);
	
	Video_InitBank(&Video_SpriteBank, true);
	
	Video_Buffer = Screen_Surface;
	//Video_Buffer = SDL_CreateRGBSurface(SDL_SWSURFACE, SCREEN_TILES_H * TILE_WIDTH, SCREEN_TILES_V * TILE_HEIGHT, 32, 
	//				Screen_Surface->format->Rmask, Screen_Surface->format->Gmask, Screen_Surface->format->Bmask, Screen_Surface->format->Amask);
}


void Video_PutPixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    *(Uint32 *)p = pixel;
}

Uint32 Video_GetColour(SDL_Surface* surface, int i)
{
	int red = (((i >> 0) & 7) * 255) / 7;
	int green = (((i >> 3) & 7) * 255) / 7;
	int blue = (((i >> 6) & 3) * 255) / 3;
	return SDL_MapRGBA(surface->format, red, green, blue, 255);
}

SDL_Surface* Video_CreateTileSurface(u8* srcData, int index, bool transparent, int flip, uint8_t colorMask)
{
	Uint32 rmask, gmask, bmask, amask;
	SDL_Surface* surface;
	u8* tileData = srcData + ( index * 64 );


    /* SDL interprets each pixel as a 32-bit number, so our masks must depend
       on the endianness (byte order) of the machine */
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

	/*rmask = Screen_Surface->format->Rmask;
	gmask = Screen_Surface->format->Gmask;
	bmask = Screen_Surface->format->Bmask;
	amask = Screen_Surface->format->Amask;
*/
	surface = SDL_CreateRGBSurface(SDL_SWSURFACE, 8, 8, 32, rmask, gmask, bmask, amask);
	if (SDL_MUSTLOCK(surface)) SDL_LockSurface(surface);

	for(int y = 0; y < 8; y++)
	{
		for(int x = 0; x < 8; x++)
		{
			int i = x;
			int j = y;
			u8 pixel = ( *tileData );
			
			if(flip & SPRITE_FLIP_X)
			{
				i = 7 - x;
			}
			if(flip & SPRITE_FLIP_Y)
			{
				j = 7 - y;
			}
			
			if(pixel == TRANSLUCENT_COLOR && transparent)
			{
				Video_PutPixel(surface, i, j, 0);
			}
			else
			{
				Video_PutPixel(surface, i, j, Video_GetColour(surface, pixel & colorMask));
			}
			tileData++;
		}
	}
	if (SDL_MUSTLOCK(surface)) SDL_UnlockSurface(surface);

	return surface;
}

SDL_Surface* Video_GetBankSurface(SurfaceBank* bank, int index, int flip, uint8_t colorMask)
{
	if(bank->srcData == NULL)
	{
		return NULL;
	}
	
	int lookupIndex = index;
	
	if(bank->spriteBank)
	{
		lookupIndex = (index & 0xFF) | (colorMask << 8) | ((flip & 0x3) << 16);
	}
	
	if(lookupIndex >= bank->numSurfaces)
	{
		return NULL;
	}
	
	if(!bank->surfaces[lookupIndex])
	{
		bank->surfaces[lookupIndex] = Video_CreateTileSurface(bank->srcData, index, bank->spriteBank, flip, colorMask);
	}

	return bank->surfaces[lookupIndex];
}

void Video_DrawTile(SDL_Surface* targetSurface, SurfaceBank* bank, int index, int x, int y)
{
	SDL_Surface* tileSurface = Video_GetBankSurface(bank, index, 0, 0xFF);
	
	if(tileSurface)
	{
		SDL_Rect tileRect;
		tileRect.x = 0;
		tileRect.y = 0;
		tileRect.w = 8;
		tileRect.h = 8;
		SDL_Rect targetRect;
		targetRect.x = x;
		targetRect.y = y;
		targetRect.w = 8;
		targetRect.h = 8;
		SDL_BlitSurface(tileSurface, &tileRect, targetSurface, &targetRect);
	}
}

void Video_DrawSprite(SDL_Surface* targetSurface, SurfaceBank* bank, int index, int x, int y, int flip, uint8_t colorMask)
{
	SDL_Surface* tileSurface = Video_GetBankSurface(bank, index, flip, colorMask);
	
	if(tileSurface)
	{
		SDL_Rect tileRect;
		tileRect.x = 0;
		tileRect.y = 0;
		tileRect.w = 8;
		tileRect.h = 8;
		SDL_Rect targetRect;
		targetRect.x = x;
		targetRect.y = y;
		targetRect.w = 8;
		targetRect.h = 8;
		SDL_BlitSurface(tileSurface, &tileRect, targetSurface, &targetRect);
	}
}

void Video_Render()
{
	int tileX = Screen.scrollX / TILE_WIDTH;
	int tileY = (Screen.scrollY / TILE_HEIGHT) % Screen.scrollHeight;
	int offsetX = Screen.scrollX % TILE_WIDTH;
	int offsetY = Screen.scrollY % TILE_HEIGHT;
	int x = -offsetX;
	int y = Screen.overlayHeight * TILE_HEIGHT - offsetY;
	
	while(y < SCREEN_TILES_V * TILE_HEIGHT)
	{
		x = -offsetX;
		tileX = Screen.scrollX / TILE_WIDTH;
		
		while(x < SCREEN_TILES_H * TILE_WIDTH)
		{
			u8 tile = vram[tileY * VRAM_TILES_H + tileX];

			Video_DrawTile(Video_Buffer, &Video_TileBank, tile, x, y);
						
			x += TILE_WIDTH;
			tileX++;
			if(tileX >= VRAM_TILES_H)
			{
				tileX = 0;
			}
		}
		
		y += TILE_HEIGHT;
		tileY ++;
		if(tileY >= Screen.scrollHeight)
		{
			tileY = 0;
		}
	}
	
	for(int n = 0; n < MAX_SPRITES; n++)
	{
		if(sprites[n].x != OFF_SCREEN)
		{
			uint8_t colorMask = sprites[n].colorMask;
			int flip = sprites[n].flags & 0x3;
			Video_DrawSprite(Video_Buffer, &Video_SpriteBank, sprites[n].tileIndex, sprites[n].x, sprites[n].y + Screen.overlayHeight * TILE_HEIGHT, flip, colorMask);
		}
	}
	
	if((u8*)Screen.overlayTileTable != Video_OverlayTileBank.srcData)
	{
		Video_SetBank(&Video_OverlayTileBank, (u8*) Screen.overlayTileTable);
	}
	
	for(int y = 0; y < Screen.overlayHeight; y++)
	{
		for(int x = 0; x < VRAM_TILES_H; x++)
		{
			u8 tile = vram[(y + Screen.scrollHeight) * VRAM_TILES_H + x];
	
			Video_DrawTile(Video_Buffer, &Video_OverlayTileBank, tile, x * TILE_WIDTH, y * TILE_HEIGHT);
		}
	}
	
/*	for(int y = 0; y < VRAM_TILES_V; y++)
	{
		for(int x = 0; x < VRAM_TILES_H; x++)
		{
			u8 tile = vram[y * VRAM_TILES_H + x];

			Video_DrawTile(Screen_Surface, &Video_TileBank, tile, x * 8, y * 8);			
		}
	}
*/	
	/*SDL_Rect rect;
	rect.x = 0;
	rect.y = 0;
	rect.w = Video_Buffer->w;
	rect.h = Video_Buffer->h;
	SDL_BlitSurface(Video_Buffer, &rect, Screen_Surface, &rect);	
*/

}