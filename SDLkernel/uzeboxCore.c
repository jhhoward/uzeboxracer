/*
 *  Uzebox Kernel
 *  Copyright (C) 2008-2009 Alec Bourque
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Uzebox is a reserved trade mark
*/

#include <stdbool.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include "uzebox.h"

//Callbacks defined in each video modes module
extern void DisplayLogo(); 
extern void InitializeVideoMode();
extern void InitSoundPort();

unsigned char sync_phase;
unsigned char sync_pulse;
unsigned char sync_flags;
struct TrackStruct tracks[CHANNELS];
volatile unsigned int joypad1_status_lo,joypad2_status_lo;
volatile unsigned int joypad1_status_hi,joypad2_status_hi;
unsigned char tileheight, textheight;
unsigned char render_start;
unsigned char playback_start;
unsigned char render_lines_count;
unsigned char render_lines_count_tmp;
unsigned char first_render_line;
unsigned char first_render_line_tmp;
unsigned char sound_enabled;


u8 joypadsConnectionStatus;

const u8 eeprom_format_table[] PROGMEM ={(u8)EEPROM_SIGNATURE,		//(u16)
								   (u8)(EEPROM_SIGNATURE>>8),	//
								   EEPROM_HEADER_VER,			//(u8)				
								   EEPROM_BLOCK_SIZE,			//(u8) 
								   EEPROM_HEADER_SIZE,			//(u8) 
								   1,							//(u8) hardwareVersion
								   0,							//(u8) hardwareRevision
								   0x38,0x8, 					//(u16)  standard uzebox & fuzebox features
								   0,0,							//(u16)  extended features
								   0,0,0,0,0,0, 				//(u8[8])MAC
								   0,							//(u8)colorCorrectionType
								   0,0,0,0, 					//(u32)game CRC
								   0,							//(u8)bootloader flags
								   0,0,0,0,0,0,0,0,0 			//(u8[9])reserved
								   };



/**
 * Performs a software reset
 */
void SoftReset(void){        
}

/**
 * Dynamically sets the rasterizer parameters:
 * firstScanlineToRender = First scanline to render
 * scanlinesToRender     = Total number of vertical lines to render. 
 */
void SetRenderingParameters(u8 firstScanlineToRender, u8 scanlinesToRender){        
	render_lines_count_tmp=scanlinesToRender;
	first_render_line_tmp=firstScanlineToRender;
}

void GeneratePalette();

void Initialize(void){
	if(!isEepromFormatted()) FormatEeprom();

	//InitSoundPort(); //ramp-up sound to avoid click

	//set rendering parameters
	render_lines_count_tmp=FRAME_LINES;
	render_lines_count=FRAME_LINES;
	first_render_line_tmp=FIRST_RENDER_LINE;
	first_render_line=FIRST_RENDER_LINE;

	GeneratePalette();

	InitializeVideoMode();

	DisplayLogo();
}

void ReadButtons(){
}

void ReadControllers(){
	ReadButtons();
}








	
// Format eeprom, wiping all data to zero
void FormatEeprom(void) {
}

// Format eeprom, saving data specified in ids
void FormatEeprom2(u16 *ids, u8 count) {
}
	
//returns true if the EEPROM has been setup to work with the kernel.
bool isEepromFormatted(){
	return true;
}

/*
 * Reads the power button status. Works for all consoles. 
 *
 * Returns: true if pressed.
 */
u8 ReadPowerSwitch(){
	return false;
}

/*
 * Write a data block in the specified block id. If the block does not exist, it is created.
 *
 * Returns: 0 on success or error codes
 */
char EepromWriteBlock(struct EepromBlockStruct *block){
	return 0;
}

/*
 * Reads a data block in the specified structure.
 *
 * Returns: 
 *  0x00 = Success
 * 	0x01 = EEPROM_ERROR_INVALID_BLOCK
 *	0x02 = EEPROM_ERROR_FULL
 *	0x03 = EEPROM_ERROR_BLOCK_NOT_FOUND
 *	0x04 = EEPROM_ERROR_NOT_FORMATTED
 */
char EepromReadBlock(unsigned int blockId,struct EepromBlockStruct *block){
	return 0;
}


