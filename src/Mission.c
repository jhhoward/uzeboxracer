/*
 * Mission.c
 *
 *  Created on: 8 Jul 2014
 *      Author: James
 */

#include <uzebox.h>
#include <avr/pgmspace.h>
#include "Sprites.h"
#include "Mission.h"
#include "Car.h"
#include "Main.h"
#include "UI.h"
#include "Track.h"
#include "Ped.h"

#include "MissionData.inc.h"

#define MISSION_ARROW_RIGHT 48
#define MISSION_ARROW_DOWN_RIGHT 49
#define MISSION_ARROW_DOWN 50

#define MISSION_MARKER_TILE 21

const char FailReason_WreckedCar[] PROGMEM = "YOU WRECKED YOUR CAR!";
const char FailReason_TheyEscaped[] PROGMEM = "THEY GOT AWAY!";
const char FailReason_LostRace[] PROGMEM = "YOU LOST THE RACE!";

MissionState Mission_State;

void Mission_Advance();

uint8_t Mission_GetParameterU8()
{
	uint8_t param = pgm_read_byte(Mission_State.currentPtr);
	Mission_State.currentPtr++;
	return param;
}

void Mission_Yield()
{
	Mission_State.currentPtr = Mission_State.lastPtr;
}

void Mission_Init()
{
	Mission_State.didYield = false;
	Mission_State.currentPtr = (uint8_t*) Mission_State.startPtr;
	Mission_State.lastPtr = (uint8_t*) Mission_State.startPtr;
	Mission_State.carIndex = NUM_CARS;
	Mission_State.aiType = AIType_Mission;
	Mission_State.markerX = 0;
	Mission_State.flags = 0;

	SetFontTilesIndex(25);

	Mission_Update();
}

// Mission instructions
static void Mission_Wait()
{
	uint8_t time = Mission_GetParameterU8();
	static uint8_t timer = 0;

	if(!Mission_State.didYield)
	{
		timer = time;
	}
	else
	{
		if(timer == 0)
		{
			return;
		}
		timer --;
	}

	Mission_Yield();
}

static void Mission_DisplayMessage(int8_t offset)
{
	uint8_t param = Mission_GetParameterU8();

	UI_DisplayMessage((char*) pgm_read_ptr(&(Mission_Strings[param])), offset);
}

static void Mission_SpawnCar()
{
	uint8_t aiType = Mission_GetParameterU8();
	uint16_t x = ((uint16_t) Mission_GetParameterU8()) << (FIXED_SHIFT + 3);
	uint16_t y = ((uint16_t) Mission_GetParameterU8()) << (FIXED_SHIFT + 3);
	uint8_t angle = Mission_GetParameterU8();

	Car* car = Car_Spawn(aiType, x, y, angle);
	if(car != NULL)
	{
		car->colorMask = Mission_GetParameterU8();
	}

}

static void Mission_SetWanted()
{
	Player_Wanted = Mission_GetParameterU8();
	UI_UpdateWanted();
}

static void Mission_SetFlag()
{
	uint8_t flag = Mission_GetParameterU8();
	uint8_t set = Mission_GetParameterU8();

	if(set)
	{
		Mission_State.flags |= (0x1 << flag);
	}
	else
	{
		Mission_State.flags &= ~(0x1 << flag);
	}
}

static void Mission_SetAIType()
{
	Mission_State.aiType = Mission_GetParameterU8();
}

static void Mission_WaitForMissionCar()
{
	if(!Mission_State.didYield)
	{
		Mission_State.waitingForMissionCar = true;
	}
	else
	{
		if(!Mission_State.waitingForMissionCar)
		{
			return;
		}
	}
	Mission_Yield();
}

static void Mission_RaceForLaps()
{
	uint8_t lapLimit = Mission_GetParameterU8();

	uint8_t x;
	for(x = 0; x < NUM_CARS; x++)
	{
		if(cars[x].aiType == AIType_Player)
		{
			if(cars[x].lap > lapLimit)
			{
				return;
			}
		}
		else if(cars[x].aiType == AIType_Mission || cars[x].aiType == AIType_RacingOpponent)
		{
			if(cars[x].lap > lapLimit)
			{
				Mission_State.inputBlocked = true;
				Mission_Fail(FailReason_LostRace);
				return;
			}
		}
	}

	Mission_Yield();
}

static uint8_t Mission_CarIndex = 0;
static Ped* Mission_Ped = NULL;

#define CAR_POSITION_OFFSET INT_TO_FIXED(4)

static void Mission_PedCarToXY()
{
	uint8_t carType = Mission_GetParameterU8();
	uint16_t toX = (((uint16_t) Mission_GetParameterU8()) << (FIXED_SHIFT + 3)) + INT_TO_FIXED(4);
	uint16_t toY = (((uint16_t) Mission_GetParameterU8()) << (FIXED_SHIFT + 3)) + INT_TO_FIXED(4);
	uint8_t colorMask = Mission_GetParameterU8();

	if(!Mission_State.didYield)
	{
		uint8_t x;
		for(x = 0; x < NUM_CARS; x++)
		{
			if(cars[x].aiType == carType)
			{
				Mission_CarIndex = x;
				break;
			}
		}

		if(x == NUM_CARS)
			return;

		if(cars[Mission_CarIndex].x < toX)
		{
			Mission_Ped = Ped_Spawn(PedType_Mission, cars[Mission_CarIndex].x + CAR_POSITION_OFFSET, cars[Mission_CarIndex].y, EXIT_EAST, colorMask);
		}
		else
		{
			Mission_Ped = Ped_Spawn(PedType_Mission, cars[Mission_CarIndex].x - CAR_POSITION_OFFSET, cars[Mission_CarIndex].y, EXIT_WEST, colorMask);
		}
	}

	if((Mission_Ped->direction == EXIT_WEST && Mission_Ped->x <= toX)
	|| (Mission_Ped->direction == EXIT_EAST && Mission_Ped->x >= toX))
	{
		if(Mission_Ped->y < toY)
		{
			Mission_Ped->direction = EXIT_SOUTH;
		}
		else
		{
			Mission_Ped->direction = EXIT_NORTH;
		}
	}

	if((Mission_Ped->direction == EXIT_NORTH && Mission_Ped->y <= toY)
	|| (Mission_Ped->direction == EXIT_SOUTH && Mission_Ped->y >= toY))
	{
		Mission_Ped->pedType = PedType_EmptySlot;
		return;
	}

	Mission_Yield();

}

static void Mission_PedXYToCar()
{
	uint16_t fromX = (((uint16_t) Mission_GetParameterU8()) << (FIXED_SHIFT + 3)) + INT_TO_FIXED(4);
	uint16_t fromY = (((uint16_t) Mission_GetParameterU8()) << (FIXED_SHIFT + 3)) + INT_TO_FIXED(4);
	uint8_t carType = Mission_GetParameterU8();
	uint8_t colorMask = Mission_GetParameterU8();

	if(!Mission_State.didYield)
	{
		uint8_t x;
		for(x = 0; x < NUM_CARS; x++)
		{
			if(cars[x].aiType == carType)
			{
				Mission_CarIndex = x;
				break;
			}
		}

		if(x == NUM_CARS)
			return;

		Mission_Ped = Ped_Spawn(PedType_Mission, fromX, fromY, cars[Mission_CarIndex].y < fromY ? EXIT_NORTH : EXIT_SOUTH, colorMask);
	}

	if((Mission_Ped->direction == EXIT_NORTH && Mission_Ped->y <= cars[Mission_CarIndex].y)
	|| (Mission_Ped->direction == EXIT_SOUTH && Mission_Ped->y >= cars[Mission_CarIndex].y))
	{
		if(Mission_Ped->x < cars[Mission_CarIndex].x)
		{
			Mission_Ped->direction = EXIT_EAST;
		}
		else
		{
			Mission_Ped->direction = EXIT_WEST;
		}
	}

	if((Mission_Ped->direction == EXIT_WEST && Mission_Ped->x <= cars[Mission_CarIndex].x + CAR_POSITION_OFFSET)
	|| (Mission_Ped->direction == EXIT_EAST && Mission_Ped->x >= cars[Mission_CarIndex].x - CAR_POSITION_OFFSET))
	{
		Mission_Ped->pedType = PedType_EmptySlot;
		return;
	}

	Mission_Yield();
}

static void Mission_ObjectiveCar()
{
	if(!Mission_State.didYield)
	{
		Mission_State.carIsObjective = true;
	}

	if(!Mission_State.carIsObjective)
	{
		return;
	}

	Mission_Yield();
}

static void Mission_SetMarkerXY()
{
	Mission_State.markerX = Mission_GetParameterU8();
	Mission_State.markerY = Mission_GetParameterU8();
}

static void Mission_ObjectiveXY()
{
	Mission_State.markerIsObjective = true;
	Mission_State.markerX = Mission_GetParameterU8();
	Mission_State.markerY = Mission_GetParameterU8();

	if(Player_Index < NUM_CARS)
	{
		uint16_t posX = cars[Player_Index].x >> (FIXED_SHIFT + 3);
		uint16_t posY = cars[Player_Index].y >> (FIXED_SHIFT + 3);

		if(posX >= Mission_State.markerX - 1 && posY >= Mission_State.markerY - 1
		&& posX <= Mission_State.markerX + 1 && posY <= Mission_State.markerY + 1)
		{
			World_ResetCustomTile(Mission_State.markerX, Mission_State.markerY);
			Mission_State.markerIsObjective = false;
			return;
		}

		Mission_Yield();
	}
}

static void Mission_PlaySound()
{
	uint8_t patch = Mission_GetParameterU8();
	uint8_t volume = Mission_GetParameterU8();
	bool retrig  = Mission_GetParameterU8();

	TriggerFx(patch, volume, retrig);
}

void Mission_Update()
{
	do
	{
		Mission_State.lastPtr = Mission_State.currentPtr;

		switch(Mission_GetParameterU8())
		{
		case MI_Wait:
			Mission_Wait();
			break;
		case MI_DisplayMessage:
			Mission_DisplayMessage(0);
			break;
		case MI_DisplayMessage2:
			Mission_DisplayMessage(1);
			break;
		case MI_ClearMessage:
			UI_ClearMessage();
			break;
		case MI_YieldForever:
			Mission_Yield();
			break;
		case MI_Finish:
			Mission_State.currentPtr = (uint8_t*) Mission_Completed;
			break;
		case MI_SpawnCar:
			Mission_SpawnCar();
			break;
		case MI_SetWanted:
			Mission_SetWanted();
			break;
		case MI_SetFlag:
			Mission_SetFlag();
			break;
		case MI_PedCarToXY:
			Mission_PedCarToXY();
			break;
		case MI_PedXYToCar:
			Mission_PedXYToCar();
			break;
		case MI_ObjectiveXY:
			Mission_ObjectiveXY();
			break;
		case MI_ObjectiveCar:
			Mission_ObjectiveCar();
			break;
		case MI_SetMarkerXY:
			Mission_SetMarkerXY();
			break;
		case MI_SetAIType:
			Mission_SetAIType();
			break;
		case MI_WaitForMissionCar:
			Mission_WaitForMissionCar();
			break;
		case MI_RaceForLaps:
			Mission_RaceForLaps();
			break;
		case MI_ShakeCamera:
			Camera_Shake(Mission_GetParameterU8());
			break;
		case MI_PlaySound:
			Mission_PlaySound();
			break;
		default:
			break;
		}

		Mission_State.didYield = Mission_State.currentPtr == Mission_State.lastPtr;
	}
	while(!Mission_State.didYield);

	if(Mission_State.inEndMenu)
	{
		if(ReadJoypad(0) & (BTN_START | BTN_A))
		{
			if(Mission_State.missionComplete)
			{
				Mission_Advance();
			}
			else
			{
				Game_Reset();
			}
		}
	}
}

void Mission_SetSprites()
{
	int16_t posX = 0, posY = 0;
	bool showArrow = false;

	if(Mission_State.markerIsObjective)
	{
		uint8_t markerAnimation = Sprites_FrameAnimation >> 3;
		World_SetCustomTile(Mission_State.markerX, Mission_State.markerY, MISSION_MARKER_TILE + (markerAnimation & 0x3));

		int16_t markerX = (((int16_t)Mission_State.markerX) << 3);
		int16_t markerY = (((int16_t)Mission_State.markerY) << 3);
		posX = markerX - MainCamera.x;
		posY = markerY - MainCamera.y - 4;

		showArrow = true;
	}
	if(Mission_State.carIsObjective && Mission_State.carIndex < NUM_CARS)
	{
		posX = FIXED_TO_INT(cars[Mission_State.carIndex].x) - MainCamera.x - 4;
		posY = FIXED_TO_INT(cars[Mission_State.carIndex].y) - MainCamera.y - 8;

		showArrow = true;
	}
	if(Mission_State.isRace)
	{
		if(Player_Index < NUM_CARS)
		{
			uint8_t raceIndex = cars[Player_Index].raceTrackIndex;

			do
			{
				MacroTileCoord nextCoord = Track_GetCoordFromIndex(raceIndex);
				raceIndex = Track_GetNextIndex(raceIndex);

				int16_t markerX = (((int16_t)(nextCoord >> 4)) << 7) + (8 * 8);
				int16_t markerY = (((int16_t)(nextCoord & 0xF)) << 7) + (8 * 8);
				posX = markerX - MainCamera.x;
				posY = markerY - MainCamera.y - 4;
			}
			while (posX > 0 && posY > 8 && posX < 224 && posY < 224);
			showArrow = true;
		}
	}

	if(showArrow)
	{
		if(posX > 0 && posY > 8 && posX < 224 && posY < 224)
		{
			uint8_t spriteIndex = Sprites_GetIndex();

			if(spriteIndex < MAX_SPRITES)
			{
				sprites[spriteIndex].colorMask = 0xFF;
				sprites[spriteIndex].x = (uint8_t) posX;
				sprites[spriteIndex].y = (uint8_t) posY + ((Sprites_FrameAnimation >> 4) & 0x1);
				sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN;
				sprites[spriteIndex].flags = 0;
			}
		}
		else
		{
			uint8_t spriteIndex = Sprites_GetIndex();
			if(spriteIndex < MAX_SPRITES)
			{
#if 1
				uint8_t animation = (Sprites_FrameAnimation >> 4) & 0x1;
				if(posX < 0)
				{
					posX = animation;
					sprites[spriteIndex].tileIndex = MISSION_ARROW_RIGHT;
					sprites[spriteIndex].flags = SPRITE_FLIP_X;
				}
				else if(posX > 224 - 8)
				{
					posX = 224 - 8 - animation;
					sprites[spriteIndex].tileIndex = MISSION_ARROW_RIGHT;
					sprites[spriteIndex].flags = 0;
				}
				if(posY < 0)
				{
					posY = animation;
					if(posX <= 1)
					{
						sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN_RIGHT;
						sprites[spriteIndex].flags = SPRITE_FLIP_X | SPRITE_FLIP_Y;
						posX += animation;
					}
					else if(posX >= 224 - 9)
					{
						sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN_RIGHT;
						sprites[spriteIndex].flags = SPRITE_FLIP_Y;
						posX -= animation;
					}
					else
					{
						sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN;
						sprites[spriteIndex].flags = SPRITE_FLIP_X | SPRITE_FLIP_Y;
					}
				}
				else if(posY > 224 - 40)
				{
					posY = 224 - 40 - animation;
					if(posX <= 1)
					{
						sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN_RIGHT;
						sprites[spriteIndex].flags = SPRITE_FLIP_X;
						posX += animation;
					}
					else if(posX >= 224 - 9)
					{
						sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN_RIGHT;
						sprites[spriteIndex].flags = 0;
						posX -= animation;
					}
					else
					{
						sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN;
						sprites[spriteIndex].flags = SPRITE_FLIP_X;
					}
				}

				sprites[spriteIndex].colorMask = 0xFF;
				sprites[spriteIndex].x = (uint8_t)posX;
				sprites[spriteIndex].y = (uint8_t)posY;
#else

				posX = markerX - (MainCamera.x + HALF_DISPLAY_WIDTH);
				posY = markerY - (MainCamera.y + HALF_DISPLAY_HEIGHT);
				uint8_t angle = Math_ATan2(posY, posX);
				posX = Math_Cos(angle) >> 1;
				posY = Math_Sin(angle) >> 1;

				switch(((angle + 16) & 0xFF) >> 5)
				{
				case 0:
					sprites[spriteIndex].tileIndex = MISSION_ARROW_RIGHT;
					sprites[spriteIndex].flags = 0;
					break;
				case 1:
					sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN_RIGHT;
					sprites[spriteIndex].flags = 0;
					break;
				case 2:
					sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN;
					sprites[spriteIndex].flags = 0;
					break;
				case 3:
					sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN_RIGHT;
					sprites[spriteIndex].flags = SPRITE_FLIP_X;
					break;
				case 4:
					sprites[spriteIndex].tileIndex = MISSION_ARROW_RIGHT;
					sprites[spriteIndex].flags = SPRITE_FLIP_X;
					break;
				case 5:
					sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN_RIGHT;
					sprites[spriteIndex].flags = SPRITE_FLIP_X | SPRITE_FLIP_Y;
					break;
				case 6:
					sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN;
					sprites[spriteIndex].flags = SPRITE_FLIP_X | SPRITE_FLIP_Y;
					break;
				case 7:
					sprites[spriteIndex].tileIndex = MISSION_ARROW_DOWN_RIGHT;
					sprites[spriteIndex].flags = SPRITE_FLIP_Y;
					break;
				default:
					break;
				}

				sprites[spriteIndex].colorMask = 0xFF;
				sprites[spriteIndex].x = HALF_DISPLAY_WIDTH + posX - 4;
				sprites[spriteIndex].y = HALF_DISPLAY_HEIGHT + posY - 4;
#endif
			}
		}
	}
}

void Mission_Set(const uint8_t* mission)
{
	Mission_State.startPtr = (uint8_t*) mission;
}

void Mission_Fail(const char* reason)
{
	UI_DisplayMessage(reason, 0);
	Mission_State.currentPtr = (uint8_t*) Mission_Failed;
	Mission_State.didYield = false;
}

void Mission_Complete()
{
	Mission_State.currentPtr = (uint8_t*) Mission_Completed;
}

void Mission_Advance()
{
	if(Mission_State.startPtr == Mission_Story1)
	{
		Mission_Set(Mission_Story2);
		Game_Reset();
	}
	else if(Mission_State.startPtr == Mission_Story2)
	{
		Mission_Set(Mission_Story3);
		Game_Reset();
	}
	else
	{
		Game_SetState(GameState_Menu);
	}
}
