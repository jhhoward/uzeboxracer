/*
 * Ped.c
 *
 *  Created on: 5 Jul 2014
 *      Author: James
 */

#include <uzebox.h>
#include <avr/pgmspace.h>
#include "Main.h"
#include "Ped.h"
#include "Sprites.h"
#include "World.h"
#include "Car.h"
#include "Track.h"

#define NUM_PEDS 5
#define NUM_NPCS 3
#define FIRST_NORTH_SPRITE_PED 40
#define FIRST_EAST_SPRITE_PED 43
#define DEAD_SPRITE_PED 46

#define FIRST_PAVEMENT_TILE 1
#define LAST_PAVEMENT_TILE 7

#define HALF_DISPLAY_SIZE 112
#define DISPLAY_SIZE 224

#define NPC_MIN_SPAWN_DISTANCE 150
#define NPC_DESTROY_DISTANCE 180

#define PED_DEAD_TIME 60
#define PED_SPLAT_WANTED_CHANCE 0x3

Ped peds[NUM_PEDS];

#define PED_CAR_COLLISION_SIZE INT_TO_FIXED(4)

STATIC_INLINE bool Ped_CheckParticleCollision(Ped* ped, Particle* particle)
{
	return (ped->x >= particle->x - PED_CAR_COLLISION_SIZE)
		&& (ped->y >= particle->y - PED_CAR_COLLISION_SIZE)
		&& (ped->x <= particle->x + PED_CAR_COLLISION_SIZE)
		&& (ped->y <= particle->y + PED_CAR_COLLISION_SIZE);
}

Car* Ped_CheckCarCollision(Ped* ped)
{
	uint8_t n;

	for(n = 0; n < NUM_CARS; n++)
	{
		if(cars[n].aiType != AIType_None)
		{
			if(Ped_CheckParticleCollision(ped, &cars[n].front)
					|| Ped_CheckParticleCollision(ped, &cars[n].back))
			{
				return &cars[n];
			}
		}
	}

	return NULL;
}

Ped* Ped_Spawn(uint8_t pedType, uint16_t x, uint16_t y, uint8_t direction, uint8_t colorMask)
{
	int8_t i;
	for(i = 0; i < NUM_PEDS; i++)
	{
		if(peds[i].pedType == PedType_EmptySlot)
		{
			break;
		}
	}

	if(i == NUM_PEDS)
	{
		return NULL;
	}

	peds[i].x = x;
	peds[i].y = y;
	peds[i].direction = direction;
	peds[i].colorMask = colorMask;
	peds[i].pedType = pedType;

	return &peds[i];
}

void Ped_SpawnNPC()
{
	uint16_t tileX;
	uint16_t tileY;

	int16_t x;
	int16_t y;
	uint8_t direction = Math_Random() & 0x3;

	switch(direction)
	{
	case EXIT_NORTH:
		x = MainCamera.x + Math_Random() % HALF_DISPLAY_SIZE;
		y = MainCamera.y + HALF_DISPLAY_SIZE - NPC_MIN_SPAWN_DISTANCE;
		break;
	case EXIT_EAST:
		x = MainCamera.x + HALF_DISPLAY_SIZE + NPC_MIN_SPAWN_DISTANCE;
		y = MainCamera.y + Math_Random() % HALF_DISPLAY_SIZE;
		break;
	case EXIT_SOUTH:
		x = MainCamera.x + Math_Random() % HALF_DISPLAY_SIZE;
		y = MainCamera.y + HALF_DISPLAY_SIZE + NPC_MIN_SPAWN_DISTANCE;
		break;
	case EXIT_WEST:
		x = MainCamera.x + HALF_DISPLAY_SIZE - NPC_MIN_SPAWN_DISTANCE;
		y = MainCamera.y + Math_Random() % HALF_DISPLAY_SIZE;
		break;
	default:
		return;
	}

	tileX = x >> 3;
	tileY = y >> 3;

	uint8_t tile = World_GetTile(tileX, tileY);
	if(tile >= FIRST_PAVEMENT_TILE && tile <= LAST_PAVEMENT_TILE)
	{
		x = INT_TO_FIXED((tileX << 3) + 4);
		y = INT_TO_FIXED((tileY << 3) + 4);
		Ped_Spawn(PedType_NPC, x, y, Math_Random() & 0x3, Math_RandomColorMask());
	}
}

#define PED_MOVEMENT_SPEED 8

void Ped_UpdateMovement(Ped* ped)
{
	uint8_t tileX = (ped->x) >> (FIXED_SHIFT + 3);
	uint8_t tileY = (ped->y) >> (FIXED_SHIFT + 3);
	uint8_t currentTile = World_GetTile(tileX, tileY);
	int16_t forwardX = 0;
	int16_t forwardY = 0;
	int8_t moveSpeed = PED_MOVEMENT_SPEED;
	uint8_t nextTile;

	if(ped->pedType == PedType_Mission)
	{
		moveSpeed <<= 1;
	}

	if(!(ReadJoypad(0) & BTN_SELECT))
	switch(ped->direction)
	{
	case 0: // EAST:
		ped->x += moveSpeed;
		forwardX = INT_TO_FIXED(4);
		break;
	case 1: // SOUTH:
		ped->y += moveSpeed;
		forwardY = INT_TO_FIXED(4);
		break;
	case 2: // WEST:
		ped->x -= moveSpeed;
		forwardX = INT_TO_FIXED(-4);
		break;
	case 3: // NORTH:
		ped->y -= moveSpeed;
		forwardY = INT_TO_FIXED(-4);
		break;
	}

	if(ped->pedType != PedType_NPC)
		return;

	nextTile = World_GetTile((ped->x + forwardX) >> (FIXED_SHIFT + 3), (ped->y + forwardY) >> (FIXED_SHIFT + 3));

	if(currentTile >= FIRST_PAVEMENT_TILE && currentTile <= LAST_PAVEMENT_TILE
			&& (nextTile < FIRST_PAVEMENT_TILE || nextTile > LAST_PAVEMENT_TILE))
	{
		if((Math_Random() & 0x1) == 0)
		{
			ped->direction = (ped->direction - 1) & 0x3;
		}
		else
		{
			ped->direction = (ped->direction + 1) & 0x3;
		}

		// Snap to grid to avoid ram tile usage
		if(ped->direction == 0 || ped->direction == 2)
		{
			ped->y = INT_TO_FIXED((tileY << 3) + 4);
		}
		else
		{
			ped->x = INT_TO_FIXED((tileX << 3) + 4);
		}

	}

	int16_t shiftedX = ped->x >> FIXED_SHIFT;
	int16_t shiftedY = ped->y >> FIXED_SHIFT;

	if(shiftedX > MainCamera.x + (HALF_DISPLAY_SIZE + NPC_DESTROY_DISTANCE) || shiftedX < MainCamera.x + HALF_DISPLAY_SIZE - NPC_DESTROY_DISTANCE ||
			shiftedY > MainCamera.y + (HALF_DISPLAY_SIZE + NPC_DESTROY_DISTANCE) || shiftedY < MainCamera.y + HALF_DISPLAY_SIZE - NPC_DESTROY_DISTANCE)
	{
		ped->pedType = PedType_EmptySlot;
		return;
	}

}

void Ped_Splat(Ped* ped)
{
	TriggerFx(SFX_SPLAT, 0xFF, false);
	ped->pedType = PedType_Killed;
	ped->animation = 0;
}

void Ped_Update()
{
	int8_t i;
	int8_t numPeds = 0;
	bool missionPedVisible = false;

	for(i = 0; i < NUM_PEDS; i++)
	{
		if(peds[i].pedType == PedType_Killed)
		{
			if(peds[i].animation > PED_DEAD_TIME)
			{
				peds[i].pedType = PedType_EmptySlot;
			}
			else peds[i].animation ++;
		}
		else if(peds[i].pedType != PedType_EmptySlot)
		{
			Ped_UpdateMovement(&peds[i]);
			numPeds++;

			if(peds[i].pedType == PedType_Mission)
			{
				missionPedVisible = true;
			}
			else
			{
				Car* hitCar = Ped_CheckCarCollision(&peds[i]);
				if(hitCar)
				{
					if(hitCar->front.x == hitCar->front.prevX && hitCar->front.y == hitCar->front.prevY)
					{
						peds[i].direction = (peds[i].direction + 2) & 0x3;
					}
					else
					{
						Ped_Splat(&peds[i]);

						if(hitCar->aiType == AIType_Player)
						{
							Player_CommitOffence(PED_SPLAT_WANTED_CHANCE);
						}
					}
				}
			}
		}
	}

	if(missionPedVisible)
	{
		if((Sprites_FrameAnimation & 0x7) == 0x1)
		{
			TriggerFx(7, 0xF, true);
			if((Sprites_FrameAnimation & 0xF) == 0x1)
			{
	//			TriggerFx(SFX_WALK1, 0xF, true);
			}
			else
			{
		//		TriggerFx(SFX_WALK2, 0xF, true);
			}
		}
	}

	if(numPeds < NUM_NPCS)
	{
		Ped_SpawnNPC();
	}
}

void Ped_Init()
{
	int8_t i;
	for(i = 0; i < NUM_PEDS; i++)
	{
		peds[i].pedType = PedType_EmptySlot;
		peds[i].x = 0;
		peds[i].y = 0;
	}
}

STATIC_INLINE void Ped_SetupSprite(Ped* ped, int8_t currentAnimation)
{
	int16_t pedX = FIXED_TO_INT(ped->x);
	int16_t pedY = FIXED_TO_INT(ped->y);
	uint8_t spriteIndex;
	if(pedX < MainCamera.x || pedY < MainCamera.y + 8
	|| pedX > MainCamera.x + 224 || pedY > MainCamera.y + 224)
		return;

	spriteIndex = Sprites_GetIndex();
	if(spriteIndex != MAX_SPRITES)
	{
		sprites[spriteIndex].colorMask = ped->colorMask;
		sprites[spriteIndex].x = pedX - MainCamera.x - 4;
		sprites[spriteIndex].y = pedY - MainCamera.y - 4;

		if(ped->pedType == PedType_Killed)
		{
			sprites[spriteIndex].tileIndex = DEAD_SPRITE_PED;

			switch(ped->direction)
			{
			case 0:
				sprites[spriteIndex].flags = 0;
				break;
			case 1:
				sprites[spriteIndex].flags = SPRITE_FLIP_Y;
				break;
			case 2:
				sprites[spriteIndex].flags = SPRITE_FLIP_X;
				break;
			case 3:
				sprites[spriteIndex].flags = SPRITE_FLIP_Y | SPRITE_FLIP_X;
				break;
			}
		}
		else
		{
			switch(ped->direction)
			{
				case 0: // EAST
					sprites[spriteIndex].tileIndex = FIRST_EAST_SPRITE_PED + currentAnimation;
					sprites[spriteIndex].flags = 0;
					break;
				case 1: // SOUTH:
					sprites[spriteIndex].tileIndex = FIRST_NORTH_SPRITE_PED + currentAnimation;
					sprites[spriteIndex].flags = SPRITE_FLIP_Y;
					break;
				case 2: // WEST:
					sprites[spriteIndex].tileIndex = FIRST_EAST_SPRITE_PED + currentAnimation;
					sprites[spriteIndex].flags = SPRITE_FLIP_X;
					break;
				case 3: // SOUTH:
					sprites[spriteIndex].tileIndex = FIRST_NORTH_SPRITE_PED + currentAnimation;
					sprites[spriteIndex].flags = 0;
					break;
			}
		}
	}
}

void Ped_SetSprites()
{
	int8_t i;
	int8_t currentAnimation;

	currentAnimation = (Sprites_FrameAnimation >> 3) & 0x3;
	if(currentAnimation == 2) currentAnimation = 0;
	if(currentAnimation == 3) currentAnimation = 2;

	for(i = 0; i < NUM_PEDS; i++)
	{
		if(peds[i].pedType != PedType_EmptySlot)
		{
			Ped_SetupSprite(&peds[i], currentAnimation);
		}
	}
}
