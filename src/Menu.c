/*
 * Menu.c
 *
 *  Created on: 9 Mar 2014
 *      Author: James
 */

#include <avr/pgmspace.h>
#include <uzebox.h>
#include <stdint.h>
#include "Menu.h"
#include "Main.h"
#include "World.h"
#include "Track.h"
#include "Sprites.h"
#include "Car.h"
#include "Mission.h"
#include "data/fonts_8x8.pic.inc"
#include "MenuData.inc.h"

#define MAP_POSITION_X 6
#define MAP_POSITION_Y 2

#define PLAYER_INDICATOR_1 51
#define PLAYER_INDICATOR_2 52
#define MISSION_INDICATOR 50

#define MENU_POSITION_X 10
#define MENU_POSITION_Y 17
#define PAUSEMENU_POSITION_Y 20

#define LOGO_POSITION_X 6
#define LOGO_POSITION_Y 4

#define SECONDS_TO_DEMO 15

typedef const void* Menu;
typedef void (*MenuFn)(void*);
Menu* Menu_Current;
int8_t Menu_CurrentIndex = 0;
int8_t Menu_SecondsToDemo = SECONDS_TO_DEMO;

void Menu_Display(Menu* menu);
void Menu_DrawTrack(int8_t x, int8_t y);

void MenuFn_NavigateTo(const void* menu)
{
	Menu_Current = (Menu*) menu;
	Menu_CurrentIndex = 0;
	Menu_Display(Menu_Current);
}

void MenuFn_Continue(const void* arg)
{
	Game_SetState(GameState_Play);
}

void MenuFn_StartMission(const void* arg)
{
	Mission_Set((const uint8_t*) arg);
	Game_SetState(GameState_Play);
}

void MenuFn_RestartMission(const void* arg)
{
	Game_SetState(GameState_Init);
	Game_SetState(GameState_Play);
}

void MenuFn_ExitMission(const void* arg)
{
	Game_SetState(GameState_Menu);
}

void Menu_Display(Menu* menu)
{
	uintptr_t* ptr = (uintptr_t*)menu;
	uint8_t y = MENU_POSITION_Y;

	if(Game_GetState() != GameState_Paused)
	{
		Fill(0, 0, SCREEN_TILES_H, SCREEN_SCROLL_HEIGHT, FONT_START);
		DrawMap2(LOGO_POSITION_X, LOGO_POSITION_Y, World_GetLogoLayout());
		Print(LOGO_POSITION_X + 4, LOGO_POSITION_Y + 7, PSTR("JOYRIDER"));
	}
	else
	{
		y = PAUSEMENU_POSITION_Y;
	}

	while(1)
	{
		Print(MENU_POSITION_X, y++, (char*)pgm_read_ptr(ptr));
//		ptr += sizeof(void*) * MENU_COMMAND_SIZE;
		ptr += MENU_COMMAND_SIZE;
		if(pgm_read_ptr(ptr) == (uint16_t)MENU_ENTRY_END)
		{
			break;
		}
	}
}

void Menu_Init()
{
	Screen.scrollHeight = 32;
	Screen.overlayHeight = 0;
	Screen.scrollX = 0;
	Screen.scrollY = 0;

	Menu_CurrentIndex = 0;
	Menu_Current = Menu_MainMenu;

	SetTileTable(World_GetLayoutTiles());

	Fill(0, 0, SCREEN_TILES_H, SCREEN_SCROLL_HEIGHT, FONT_START);

	SetFontTilesIndex(FONT_START);
	//PrintRam(0, 0, ">RACE");
	Menu_Display(Menu_Current);
}

void PauseMenu_Init()
{
	Screen.scrollHeight = 32;
	Screen.overlayHeight = 0;
	Screen.scrollX = 0;
	Screen.scrollY = 0;
	Menu_CurrentIndex = 0;

	SetTileTable(World_GetLayoutTiles());

	Fill(0, 0, SCREEN_TILES_H, SCREEN_SCROLL_HEIGHT, FONT_START);

	DrawMap2(MAP_POSITION_X, MAP_POSITION_Y, World_GetLayout());

	SetFontTilesIndex(FONT_START);
	Menu_Current = Menu_PauseMenu;
	Menu_Display(Menu_Current);
}

void PauseMenu_Update()
{
	if(Player_Index < NUM_CARS)
	{
		uint8_t spriteIndex = Sprites_GetIndex();
		if(spriteIndex < MAX_SPRITES)
		{
			sprites[spriteIndex].tileIndex = (Sprites_FrameAnimation & 0x8) ? PLAYER_INDICATOR_1 : PLAYER_INDICATOR_2;
			sprites[spriteIndex].colorMask = 0xFF;
			sprites[spriteIndex].flags = 0;
			sprites[spriteIndex].x = (MAP_POSITION_X << 3) + (cars[Player_Index].x >> (FIXED_SHIFT + 4)) - 4;
			sprites[spriteIndex].y = (MAP_POSITION_Y << 3) + (cars[Player_Index].y >> (FIXED_SHIFT + 4)) - 4;
		}
	}

	if(Mission_State.markerIsObjective)
	{
		uint8_t spriteIndex = Sprites_GetIndex();
		if(spriteIndex < MAX_SPRITES)
		{
			sprites[spriteIndex].tileIndex = MISSION_INDICATOR;
			sprites[spriteIndex].colorMask = 0xFF;
			sprites[spriteIndex].flags = 0;
			sprites[spriteIndex].x = (MAP_POSITION_X << 3) + (Mission_State.markerX >> 1) - 4;
			sprites[spriteIndex].y = (MAP_POSITION_Y << 3) + (Mission_State.markerY >> 1) - 7 + ((Sprites_FrameAnimation >> 3) & 0x1);
		}
	}
	else if(Mission_State.carIsObjective && Mission_State.carIndex < NUM_CARS)
	{
		uint8_t spriteIndex = Sprites_GetIndex();
		if(spriteIndex < MAX_SPRITES)
		{
			sprites[spriteIndex].tileIndex = MISSION_INDICATOR;
			sprites[spriteIndex].colorMask = 0xFF;
			sprites[spriteIndex].flags = 0;
			sprites[spriteIndex].x = (MAP_POSITION_X << 3) + (cars[Mission_State.carIndex].x >> (FIXED_SHIFT + 4)) - 4;
			sprites[spriteIndex].y = (MAP_POSITION_Y << 3) + (cars[Mission_State.carIndex].y >> (FIXED_SHIFT + 4)) - 7 +  + ((Sprites_FrameAnimation >> 3) & 0x1);
		}
	}
}

int8_t Menu_GetNumEntries(Menu* menu)
{
	uintptr_t* ptr = (uintptr_t*)menu;
	int8_t count = 0;

	while(pgm_read_word(ptr) != (uint16_t)MENU_ENTRY_END)
	{
		count++;
		ptr += MENU_COMMAND_SIZE;
//		ptr += sizeof(void*) * MENU_COMMAND_SIZE;
	}

	return count;
}

void Menu_UpdateSelection(int8_t delta)
{
	uint8_t y = (Game_GetState() == GameState_Paused) ? PAUSEMENU_POSITION_Y : MENU_POSITION_Y;

	if(delta != 0)
	{
		PrintChar(MENU_POSITION_X - 2, y +  Menu_CurrentIndex, ' ');
		Menu_CurrentIndex += delta;

		if(Menu_CurrentIndex < 0)
		{
			Menu_CurrentIndex = Menu_GetNumEntries(Menu_Current) - 1;
		}
		else if(Menu_CurrentIndex >= Menu_GetNumEntries(Menu_Current))
		{
			Menu_CurrentIndex = 0;
		}

		TriggerFx(SFX_MENU_MOVE, 0xff, true);
	}
	PrintChar(MENU_POSITION_X - 2, y +  Menu_CurrentIndex, '>');
}

void Menu_ExecuteSelection(Menu* menu, int8_t selection)
{
	TriggerFx(SFX_MENU_SELECT, 0xff, true);

	/*void* ptr = menu + selection * MENU_COMMAND_SIZE;
	MenuFn fn = (MenuFn)(pgm_read_ptr(ptr + sizeof(void*)));
	void* arg = (void*) pgm_read_ptr((ptr + sizeof(void*) * 2));
*/
	intptr_t* ptr = (intptr_t*) menu;
	ptr += selection * MENU_COMMAND_SIZE;
	MenuFn fn = (MenuFn)pgm_read_ptr(&ptr[1]);
	void* arg = (void*) pgm_read_ptr(&ptr[2]);

	if(fn)
	{
		fn(arg);
	}
}

void Menu_Update()
{
	static int16_t joypadLastFrame = 0;
	int16_t joypad = ReadJoypad(0);

	if(Menu_Current == Menu_MainMenu)
	{
		if(Menu_SecondsToDemo == 0)
		{
			Menu_SecondsToDemo = SECONDS_TO_DEMO;
			if(Math_Random() & 0x1)
			{
				Mission_Set(Mission_Demo);
			}
			else
			{
				Mission_Set(Mission_Demo2);
			}
			Game_SetState(GameState_Play);
			return;
		}
		else if((Sprites_FrameAnimation & 0x3F) == 0)
		{
			Menu_SecondsToDemo --;
		}
	}

	if(joypad == joypadLastFrame)
	{
		Menu_UpdateSelection(0);
		return;
	}
	
	Menu_SecondsToDemo = SECONDS_TO_DEMO;
	
	joypadLastFrame = joypad;

	int8_t delta = 0;

	if(joypad & BTN_UP)
	{
		delta --;
	}
	if(joypad & BTN_DOWN)
	{
		delta ++;
	}

	Menu_UpdateSelection(delta);

	//Menu_DrawTrack(MAP_POSITION_X, MAP_POSITION_Y);

	/*if(joypad)
	{
		while(ReadJoypad(0));
	}*/

	if(joypad & (BTN_A | BTN_START))
	{
		//Game_SetState(GameState_Play);
		Menu_ExecuteSelection(Menu_Current, Menu_CurrentIndex);
	}

	if((joypad & BTN_B) && Menu_Current != Menu_MainMenu && Game_GetState() == GameState_Menu)
	{
		TriggerFx(SFX_MENU_SELECT, 0xff, true);
		Menu_Current = Menu_MainMenu;
		Menu_Display(Menu_MainMenu);
	}
}

void Menu_DrawTrack(int8_t x, int8_t y)
{
	uint8_t trackLength = Track_GetLength();
	static uint8_t offset = 0;

	offset++;

	uint8_t counter = (offset / 8) % 4;

	const char* worldLayout = World_GetLayout();

	for(uint8_t n = 0; n < trackLength; n++)
	{
		MacroTileCoord coord = Track_GetCoordFromIndex(n);
		uint8_t coordX = coord >> 4;
		uint8_t coordY = coord & 0xF;

		if(counter == 0)
		{
			SetTile(x + coordX, y + coordY, 20);
			counter = 4;
		}
		else
		{
			SetTile(x + coordX, y + coordY, pgm_read_byte(&worldLayout[2 + coordY * 16 + coordX]));
		}

		counter --;
	}
}

void Menu_ClearTrack(int8_t x, int8_t y)
{
	uint8_t trackLength = Track_GetLength();
	const char* worldLayout = World_GetLayout();

	for(uint8_t n = 0; n < trackLength; n++)
	{
		MacroTileCoord coord = Track_GetCoordFromIndex(n);
		uint8_t coordX = coord >> 4;
		uint8_t coordY = coord & 0xF;

		SetTile(x + coordX, y + coordY, pgm_read_byte(&worldLayout[2 + y * 16 + x]));
	}
}

void Menu_DrawOverlayHUD()
{

}
