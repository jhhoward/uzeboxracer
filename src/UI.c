/*
 * UI.c
 *
 *  Created on: 8 Jul 2014
 *      Author: James
 */

#include <avr/pgmspace.h>
#include <uzebox.h>
#include "Main.h"
#include "UI.h"
#include "Car.h"
#include "Sprites.h"
#include "Mission.h"

#define UI_HEALTH_FILLED 90
#define UI_HEALTH_EMPTY 89
#define UI_WANTED_FILLED 92
#define UI_WANTED_EMPTY 91
#define UI_HEALTH_FILLED2 93

#define UI_HEALTH_LENGTH 8
#define UI_WANTED_LENGTH 4

#define OVERLAY_TOP (VRAM_TILES_V - SCREEN_OVERLAY_HEIGHT)

#define UI_HEALTH_X 1
#define UI_HEALTH_Y (OVERLAY_TOP)

#define UI_WANTED_X 23
#define UI_WANTED_Y (OVERLAY_TOP)

#define UI_FLASHING_DURATION 60

#define UI_TIMER_X 12
#define UI_TIMER_Y 29

#define UI_CHAR_ZERO 16

uint8_t UI_FlashingHealth = 0;
uint8_t UI_FlashingWanted = 0;

uint16_t UI_Timer = 0;

void UI_SetBase()
{

}

void UI_PrintInt(int x, int y, unsigned int val, bool zeropad)
{
	unsigned char c,i;

	for(i=0;i<5;i++)
	{
		c=val%10;
		if(val>0 || i==0)
		{
			SetFont(x--,y,c+UI_CHAR_ZERO);
		}
		else
		{
			if(zeropad)
			{
				SetFont(x--,y,UI_CHAR_ZERO);
			}
			else
			{
				return;
			}
		}
		val=val/10;
	}
}


void UI_UpdateTimer()
{
	uint8_t minutes = 0;
	uint8_t seconds = (uint8_t)UI_Timer;
	while(seconds >= 60)
	{
		seconds -= 60;
		minutes ++;
	}


	UI_PrintInt(UI_TIMER_X + 3, UI_TIMER_Y, seconds, false);
	if(seconds < 10)
	{
		PrintChar(UI_TIMER_X + 2, UI_TIMER_Y, '0');
	}
	PrintChar(UI_TIMER_X + 1, UI_TIMER_Y, ':');
	UI_PrintInt(UI_TIMER_X, UI_TIMER_Y, minutes, false);
}

void UI_Init()
{
	UI_FlashingHealth = 0;
	UI_FlashingWanted = 0;

	Fill(0, 30, 28, 2, FONT_START);
	DrawMap2(0, 29, World_GetHUDLayout());

	// Reset health and wanted
	int8_t x;
	for(x = 0; x < UI_HEALTH_LENGTH; x++)
	{
		if(Mission_State.playerIsCop || Player2_Index < NUM_CARS)
		{
			SetTile(UI_HEALTH_X + x, UI_HEALTH_Y, UI_HEALTH_FILLED2);
		}
		else
		{
			SetTile(UI_HEALTH_X + x, UI_HEALTH_Y, UI_HEALTH_FILLED);
		}
	}
	for(x = 0; x < UI_WANTED_LENGTH; x++)
	{
		SetTile(UI_WANTED_X + x, UI_WANTED_Y, UI_WANTED_EMPTY);
	}

	UI_Timer = 0;
}

void UI_Update()
{
	if(UI_FlashingHealth > 0)
	{
		uint8_t flash = (UI_FlashingHealth & 0x8);
		UI_FlashingHealth --;

		if(flash != (UI_FlashingHealth & 0x8))
		{
			uint8_t tileType = UI_HEALTH_EMPTY;

			if(flash)
			{
				if(Mission_State.playerIsCop || Player2_Index < NUM_CARS)
				{
					tileType = UI_HEALTH_FILLED2;
				}
				else
				{
					tileType = UI_HEALTH_FILLED;
				}
			}

			int8_t x;
			for(x = 0; x < UI_HEALTH_LENGTH; x++)
			{
				SetTile(UI_HEALTH_X + x, UI_HEALTH_Y, flash && (x < Player_Health) ?  tileType : UI_HEALTH_EMPTY);
			}
		}

	}

	if(UI_FlashingWanted > 0)
	{
		uint8_t flash = (UI_FlashingWanted & 0x8);
		UI_FlashingWanted --;

		if(flash != (UI_FlashingWanted & 0x8))
		{
			int8_t x;
			for(x = 0; x < UI_WANTED_LENGTH; x++)
			{
				SetTile(UI_WANTED_X + x, UI_WANTED_Y, flash && (x < Player_Wanted) ? UI_WANTED_FILLED : UI_WANTED_EMPTY);
			}
		}

	}

	if(Player_Health > 0 && !Mission_State.inputBlocked && ((Sprites_FrameAnimation & 0x3F) == 0))
	{
		if(Mission_State.timerCountingUp)
		{
			UI_Timer ++;
			UI_UpdateTimer();
		}
		else if(UI_Timer > 0)
		{
			UI_Timer --;
			UI_UpdateTimer();
		}
	}
}

void UI_UpdateWanted()
{
	UI_FlashingWanted = UI_FLASHING_DURATION;
}

void UI_UpdateHealth()
{
	UI_FlashingHealth = UI_FLASHING_DURATION;
}

void UI_ClearMessage()
{
	Fill(0, 30, 28, 1, FONT_START);
}

void UI_DisplayMessage(const char* message, int8_t offset)
{
	Fill(0, 30 + offset, 28, 1, FONT_START);
//	UI_ClearMessage();

	int8_t x = 14 - (strlen_P(message) >> 1);
	Print(x, 30 + offset, message);
}

void UI_FlashMessage(const char* message)
{

}
