/*
 * Main.h
 *
 *  Created on: 9 Mar 2014
 *      Author: James
 */

#ifndef MAIN_H_
#define MAIN_H_

#ifndef NULL
#define NULL 0
#endif

#ifdef PLATFORM_AVR
#define pgm_read_ptr(x) pgm_read_word(x)
#endif

#define SCREEN_OVERLAY_HEIGHT 3
#define SCREEN_SCROLL_HEIGHT 29

#ifdef WIN32
#define STATIC_INLINE
#else
#define STATIC_INLINE static inline
#endif

// Overall screen dimensions are 28x27 tiles, or 224x216 pixels
#define DISPLAY_WIDTH 224
#define DISPLAY_HEIGHT (216 - SCREEN_OVERLAY_HEIGHT * 8)
#define HALF_DISPLAY_WIDTH (DISPLAY_WIDTH >> 1)
#define HALF_DISPLAY_HEIGHT (DISPLAY_HEIGHT >> 1)

#define FONT_START 25

enum
{
	GameState_Init,
	GameState_Menu,
	GameState_Play,
	GameState_Paused
};

void Game_SetState(uint8_t newState);
uint8_t Game_GetState();
void Game_Reset();

void Profile_Start(char* block);
void Profile_End();

enum
{
	SFX_BUMP = 31,
	SFX_SPLAT,
	SFX_WALK1,
	SFX_WALK2,
	SFX_MENU_MOVE,
	SFX_MENU_SELECT,
	SFX_PAUSE,
	SFX_SIREN,
	SFX_TYRES
};

extern uint8_t Game_Frame;

#endif /* MAIN_H_ */
