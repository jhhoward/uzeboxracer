/*
 * Menu.h
 *
 *  Created on: 9 Mar 2014
 *      Author: James
 */

#ifndef MENU_H_
#define MENU_H_

#define MENU_ENTRY_END (void*)(0xffff)
#define MENU_COMMAND_SIZE 3		// string ptr, function ptr, arg

void Menu_Init();
void Menu_Update();

void PauseMenu_Init();
void PauseMenu_Update();

void MenuFn_NavigateTo(const void* menu);
void MenuFn_Continue(const void* arg);
void MenuFn_StartMission(const void* arg);
void MenuFn_RestartMission(const void* arg);
void MenuFn_ExitMission(const void* arg);


#endif /* MENU_H_ */
