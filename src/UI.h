/*
 * UI.h
 *
 *  Created on: 8 Jul 2014
 *      Author: James
 */

#ifndef UI_H_
#define UI_H_

void UI_Init();
void UI_Update();
void UI_UpdateHealth();
void UI_UpdateWanted();

void UI_ClearMessage();
void UI_DisplayMessage(const char* message, int8_t offset);
void UI_FlashMessage(const char* message);

#endif /* UI_H_ */
