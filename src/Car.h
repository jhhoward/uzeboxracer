/*
 * Car.h
 *
 *  Created on: 24 Jan 2014
 *      Author: James
 */

#ifndef CAR_H_
#define CAR_H_

#include "FixedMath.h"
#include "World.h"
#define CAR_LENGTH INT_TO_FIXED(8)

#define NUM_CARS 5

typedef struct
{
	uint16_t x, y;
	uint16_t prevX, prevY;
} Particle;

typedef struct
{
	uint16_t x, y;				// Position
	Particle front;
	Particle back;
	int8_t wheelTurn;
	uint8_t angle;				// Rotation angle
	uint16_t input;				// Joypad input
	uint8_t colorMask;			// Sprite color mask

	//union
	//{
		struct // Racing mode
		{
			uint8_t raceTrackIndex;			// Index into race track tile array
			uint8_t positionTile : 4;	// Tiles to next part
			uint8_t lap : 4;			// How many laps in race
		};
		struct // Open world
		{
			uint8_t aiMacroTileIndex;
			uint8_t aiDirection : 4;
			uint8_t speedThrottle : 1;
			uint8_t onScreen : 1;
			uint8_t other : 2;
		};
	//};

	uint8_t aiType;
} Car;

#define COLORMASK_GREEN 0x30
#define COLORMASK_RED 0x6
#define COLORMASK_YELLOW 0x36
#define COLORMASK_BLUE 0x80

#define COLORMASK_RGB(r, g, b) ((r >> 5) | ((g >> 5) << 3) | ((b >> 6) << 6))

enum AIType
{
	AIType_None,
	AIType_Player,
	AIType_Player2,
	AIType_RacingOpponent,
	AIType_NPC,
	AIType_Police,
	AIType_Mission,
	AIType_Flee,
	AIType_FleeFast,
	AIType_DriveToMarker
};

void Car_SetSprites();
void Car_Update();
void Car_Init();
Car* Car_Spawn(uint8_t aiType, int16_t x, int16_t y, int8_t angle);
void Player_CommitOffence(uint8_t wantedRaiseChance);

void Camera_Update();
void Camera_Shake(uint8_t duration);

extern uint8_t Player_Wanted;
extern uint8_t Player_Health;
extern uint8_t Player_Index;
extern uint8_t Player2_Index;
extern Car cars[NUM_CARS];

#endif /* CAR_H_ */
