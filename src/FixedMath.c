/*
 * FixedMath.c
 *
 *  Created on: 8 Mar 2014
 *      Author: James
 */

#include <avr/pgmspace.h>
#include <stdint.h>
#include "FixedMath.h"
#include "MathTable.inc.h"

#ifndef M_1_PI
#define M_1_PI 0.3183098861422280369261393109942
#endif

/*const int8_t forwardVectorLUT[] PROGMEM =
{
	FLOAT_TO_FIXED(1.0000f), FLOAT_TO_FIXED(0.0000f),  // 0
	FLOAT_TO_FIXED(0.9239f), FLOAT_TO_FIXED(0.3827f),  // 22.5
	FLOAT_TO_FIXED(0.7071f), FLOAT_TO_FIXED(0.7071f),  // 45
	FLOAT_TO_FIXED(0.3827f), FLOAT_TO_FIXED(0.9239f),  // 67.5
	FLOAT_TO_FIXED(0.0000f), FLOAT_TO_FIXED(1.0000f),  // 90
	FLOAT_TO_FIXED(-0.3827f), FLOAT_TO_FIXED(0.9239f),  // 112.5
	FLOAT_TO_FIXED(-0.7071f), FLOAT_TO_FIXED(0.7071f),  // 135
	FLOAT_TO_FIXED(-0.9239f), FLOAT_TO_FIXED(0.3827f),  // 157.5
	FLOAT_TO_FIXED(-1.0000f), FLOAT_TO_FIXED(0.0000f),  // 180
	FLOAT_TO_FIXED(-0.9239f), FLOAT_TO_FIXED(-0.3827f),  // 202.5
	FLOAT_TO_FIXED(-0.7071f), FLOAT_TO_FIXED(-0.7071f),  // 225
	FLOAT_TO_FIXED(-0.3827f), FLOAT_TO_FIXED(-0.9239f),  // 247.5
	FLOAT_TO_FIXED(0.0000f), FLOAT_TO_FIXED(-1.0000f),  // 270
	FLOAT_TO_FIXED(0.3827f), FLOAT_TO_FIXED(-0.9239f),  // 292.5
	FLOAT_TO_FIXED(0.7071f), FLOAT_TO_FIXED(-0.7071f),  // 315
	FLOAT_TO_FIXED(0.9239f), FLOAT_TO_FIXED(-0.3827f)   // 337.5
};*/

int8_t Math_Cos(uint8_t angle)
{
	return pgm_read_byte(&SinTable[(angle + 64) & 0xFF]);
}

int8_t Math_Sin(uint8_t angle)
{
	return pgm_read_byte(&SinTable[angle]);
}


int16_t Math_ATan(int16_t x)
{
	uint8_t a, b, c;            /* for binary search */
	int16_t d;                /* difference value for search */

	if (x >= 0) {           /* search the first part of tan table */
	  a = 0;
	 // b = 127;
	  b = 63;
	}
	else {                  /* search the second half instead */
	  //a = 128;
	  //b = 255;
	  a = 64;
	  b = 127;
	}

	do {
	  c = (a + b) >> 1;
	  d = x - (int16_t)pgm_read_word(&TanTable[c]);

	  if (d > 0)
		  a = c + 1;
	  else
		  if (d < 0)
			  b = c - 1;

	} while ((a <= b) && (d));

	/*if (x >= 0)
	  return ((int16_t)c) << FIXED_SHIFT;

	return -(((int16_t)c) << FIXED_SHIFT);*/

//	if(x >= 0)
//		return c >> 1;
//	else return (c >> 1) - 128; /// <--- this is wrong
	if(x >= 0)
		return c;
	else return (c) - 128; /// <--- this is wrong
}

int16_t Math_ATan2(int16_t y, int16_t x)
{
   int16_t r;

   if (x == 0) {
      if (y == 0) {
    	  return 0;
      }
      else
    	  return ((y < 0) ? -DEGREES_90 : DEGREES_90);
   }

   r = ((((int32_t)y) << FIXED_SHIFT) / x);

   r = Math_ATan(r);

   if (x >= 0)
      return r;

   if (y >= 0)
      return DEGREES_180 + r;

   return r - DEGREES_180;
}

int16_t FixedMul(int16_t a, int16_t b)
{
	return (((int32_t)a * b) + (0x1 << (FIXED_SHIFT - 1))) >> FIXED_SHIFT;
}

int16_t FixedDiv(int16_t a, int16_t b)
{
	return ((int32_t)a << FIXED_SHIFT) / b;
}

int16_t FixedDot(int16_t x, int16_t y, int16_t i, int16_t j)
{
	return FixedMul(x, i) + FixedMul(y, j);
}

uint16_t Math_Random()
{
	static uint16_t randVal = 0xABC;

    uint16_t lsb = randVal & 1;
    randVal >>= 1;
    if (lsb == 1)
    	randVal ^= 0xB400u;

	return randVal - 1;
}

uint8_t Math_RandomColorMask()
{
	uint8_t mask = 0;

	while(mask == 0 || mask == 254)
	{
		mask = (uint8_t) (Math_Random() & 0xFF);
	}

	return mask;
}

#if 0
static inline int16_t FixedNabs(const int16_t j)
{
#if (((int16_t)-1) >> 1) == ((int16_t)-1)
    // signed right shift sign-extends (arithmetic)
    const int16_t negSign = ~(j >> 15); // splat sign bit into all 16 and complement
    // if j is positive (negSign is -1), xor will invert j and sub will add 1
    // otherwise j is unchanged
    return (j ^ negSign) - negSign;
#else
    return (j < 0 ? j : -j);
#endif
}


uint8_t Math_ATan2(int16_t y, int16_t x) {
    if (x == y) { // x/y or y/x would return -1 since 1 isn't representable
        if (y > 0) { // 1/8
            return 8192 >> 8;
        } else if (y < 0) { // 5/8
            return 40960 >> 8;
        } else { // x = y = 0
            return 0;
        }
    }
    const int16_t nabs_y = FixedNabs(y), nabs_x = FixedNabs(x);
    if (nabs_x < nabs_y) { // octants 1, 4, 5, 8
        const int16_t y_over_x = FixedDiv(y, x);
        const int16_t correction = FixedMul(FLOAT_TO_FIXED(0.273f * M_1_PI), FixedNabs(y_over_x));
        const int16_t unrotated = FixedMul(FLOAT_TO_FIXED(0.25f + 0.273f * M_1_PI) + correction, y_over_x);
        if (x > 0) { // octants 1, 8
            return unrotated >> 8;
        } else { // octants 4, 5
            return (32768 + unrotated) >> 8;
        }
    } else { // octants 2, 3, 6, 7
        const int16_t x_over_y = FixedDiv(x, y);
        const int16_t correction = FixedMul(FLOAT_TO_FIXED(0.273f * M_1_PI), FixedNabs(x_over_y));
        const int16_t unrotated = FixedMul(FLOAT_TO_FIXED(0.25f + 0.273f * M_1_PI) + correction, x_over_y);
        if (y > 0) { // octants 2, 3
            return (16384 - unrotated) >> 8;
        } else { // octants 6, 7
            return (49152 - unrotated) >> 8;
        }
    }
}
#endif

/*
int16_t Math_Sqrt(int16_t x)
{
	int8_t shift = 0;

	while(x >= sizeof(SqrtTable))
	{
		x >>= 2;
		shift ++;
	}

	return pgm_read_word(&SqrtTable[x]) << shift;
}
*/

void Math_GetForwardVector(uint8_t angle, int8_t* x, int8_t* y)
{
	*x = Math_Cos(angle);
	*y = Math_Sin(angle);
	/*uint8_t angleIndex = ((angle + 8) & 0xFF) >> 4;

	*x = (int8_t)pgm_read_byte(&forwardVectorLUT[angleIndex << 1]);
	*y = (int8_t)pgm_read_byte(&forwardVectorLUT[(angleIndex << 1) + 1]);*/
}

