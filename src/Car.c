/*
 * Car.c
 *
 *  Created on: 24 Jan 2014
 *      Author: James
 */

#include <uzebox.h>
#include <avr/pgmspace.h>
#include <stdbool.h>
#include "World.h"
#include "Car.h"
#include "CarSprites.inc.h"
#include "FixedMath.h"
#include "Track.h"
#include "Main.h"
#include "Sprites.h"
#include "AI.h"
#include "UI.h"
#include "Mission.h"

#define CHEATS_ENABLED 0
#define NUM_NPCS 4
#define NUM_RACE_CARS 4
#define FRICTION_SHIFT 5
#define ROTATION_SPEED 2
#define POLICE_SPAWN_FREQUENCY 0x7
#define CAR_RAM_WANTED_CHANCE 0x3
#define POLICE_RAM_WANTED_CHANCE 0x0
#define MAX_WANTED 4

#define PLAYER_HEALTH_FLASH_TIME 64

Car cars[NUM_CARS];

uint8_t Car_RacePositions[NUM_RACE_CARS];

uint8_t Player_Wanted = 2;
uint8_t Player_Health = 6;
uint8_t Player_Index = 0xff;
uint8_t Player2_Index = 0xff;

uint8_t Player_HealthFlashing = 0;
uint8_t Police_OnScreen = 0;

uint8_t Camera_Shaking = 0;

void Car_UpdatePhysics(Car* car);
void Car_UpdateRaceAI(Car* car);
void Car_UpdateRaceTrackPosition(int n);
void Car_UpdateNPCAI(Car* car);
void Car_UpdatePoliceAI(Car* car);
void Car_UpdatePlayer(Car* car, uint8_t inputNumber);
void Car_UpdateMissionAI(Car* car);
void Car_CheckCollisions();

void AI_DriveInDirection(Car* car, uint8_t direction);
STATIC_INLINE void Particle_Constrain(Particle* a, Particle* b, int16_t unused);

void Player_CommitOffence(uint8_t wantedRaiseChance)
{
	if(Mission_State.playerIsCop || Mission_State.playerImmunity)
		return;

	if((Math_Random() & wantedRaiseChance) == 0)
	{
		if(Player_Wanted < MAX_WANTED && !Mission_State.inputBlocked)
		{
			Player_Wanted ++;
			UI_UpdateWanted();
		}
	}
}

void Camera_Shake(uint8_t duration)
{
	if(duration > Camera_Shaking)
	{
		Camera_Shaking = duration;
	}
}

uint8_t Car_GetColorMask(uint8_t index)
{
	switch(index)
	{
	default:
	case 0:
		return COLORMASK_RED;
	case 1:
		return COLORMASK_GREEN;
	case 2:
		return COLORMASK_YELLOW;
	case 3:
		return COLORMASK_BLUE;
	}
}

Car* Car_Spawn(uint8_t aiType, int16_t x, int16_t y, int8_t angle)
{
	Car* car = NULL;
	uint8_t n;

	// Find an empty slot
	for(n = 0; n < NUM_CARS; n++)
	{
		if(cars[n].aiType == AIType_None)
		{
			car = &cars[n];
			break;
		}
	}

	// Find something off screen
	if(!car)
	{
		for(n = 0; n < NUM_CARS; n++)
		{
			if((cars[n].aiType == AIType_NPC || cars[n].aiType == AIType_Police) && !cars[n].onScreen)
			{
				car = &cars[n];
				break;
			}
		}
	}

	// Just find any NPC to trample over
	if(!car)
	{
		for(n = 0; n < NUM_CARS; n++)
		{
			if((cars[n].aiType == AIType_NPC || cars[n].aiType == AIType_Police))
			{
				car = &cars[n];
				break;
			}
		}
	}

	if(!car)
	{
		return NULL;
	}

	car->input = 0;
	car->aiType = aiType;
	car->x = x;
	car->y = y;
	car->angle = angle;

	int8_t forwardX, forwardY;
	Math_GetForwardVector(angle, &forwardX, &forwardY);
	car->front.x = car->front.prevX = x + forwardX;
	car->front.y = car->front.prevY = y + forwardY;
	car->back.x = car->back.prevX = x - forwardX;
	car->back.y = car->back.prevY = y - forwardY;

	Particle_Constrain(&car->front, &car->back, 0);

	{
		car->raceTrackIndex = 0;
		car->lap = 1;
		car->positionTile = 0;
	}

	car->colorMask = 0xFF;

	if(car->aiType == AIType_Player)
	{
		Player_Index = n;
	}
	if(car->aiType == AIType_Player2)
	{
		Player2_Index = n;
	}
	if(car->aiType == AIType_Mission)
	{
		Mission_State.carIndex = n;
	}

	return car;
}




void Car_Init()
{
	Player_Health = 8;
	Player_Wanted = 0;
	Player_Index = 0xff;
	Player2_Index = 0xff;
	Player_HealthFlashing = 0;
	Police_OnScreen = 0;

	for(int n = 0; n < NUM_CARS; n++)
	{
		cars[n].aiType = AIType_None;
	}
}


void Car_UpdatePositionDisplays()
{
	for(int n = 0; n < NUM_CARS; n++)
	{
		int index = Sprites_GetIndex();
		if(index < MAX_SPRITES)
		{
			sprites[index].x = 10 + 10 * n;
			sprites[index].y = 10;
			sprites[index].tileIndex = 0;
			sprites[index].colorMask = Car_GetColorMask(Car_RacePositions[n]);
			sprites[index].flags = 0;
		}
	}
}

void Car_SortPositions()
{
	bool changed = true;

	while(changed)
	{
		changed = false;

		for(int n = 1; n < NUM_RACE_CARS; n++)
		{
			Car* first = &cars[Car_RacePositions[n]];
			Car* second = &cars[Car_RacePositions[n - 1]];
			bool swap = false;

			if(first->lap > second->lap)
			{
				swap = true;
			}
			else if(first->lap == second->lap)
			{
				if(first->raceTrackIndex > second->raceTrackIndex)
				{
					swap = true;
				}
				else if(first->raceTrackIndex == second->raceTrackIndex)
				{
					if(first->positionTile > second->positionTile)
					{
						swap = true;
					}
				}
			}

			if(swap)
			{
				uint8_t temp = Car_RacePositions[n];
				Car_RacePositions[n] = Car_RacePositions[n - 1];
				Car_RacePositions[n - 1] = temp;
				changed = true;
			}
		}
	}
}

uint8_t Car_GetRacePosition(uint8_t carIndex)
{
	for(uint8_t n = 0; n < NUM_RACE_CARS; n++)
	{
		if(Car_RacePositions[n] == carIndex)
		{
			return n;
		}
	}

	return 0;
}

#define NPC_MIN_SPAWN_DISTANCE 150
#define NPC_DESTROY_DISTANCE 180

uint8_t Car_GetRandomDirection(uint8_t mask)
{
	if(!mask)
		return 0;
	else if((mask & 0xF) == 0)
	{
		uint8_t direction = 0;
		while(mask && (mask & 0x1) == 0)
		{
			direction ++;
			mask >>= 1;
		}
		return direction;
	}

	while(1)
	{
		uint8_t direction = Math_Random() & 0x3;

		if((0x1 << direction) & mask)
		{
			return direction;
		}
	}

	return 0;
}

#define MIN_CAR_TO_CAR_SPAWN_DISTANCE INT_TO_FIXED(32)

void Car_SpawnNPC(uint8_t aiType)
{
	uint8_t direction;
	uint8_t carDirection;
	uint8_t macroTileX, macroTileY;
	int16_t x = MainCamera.x + HALF_DISPLAY_WIDTH;
	int16_t y = MainCamera.y + HALF_DISPLAY_HEIGHT;
	uint8_t tileX;
	uint8_t tileY;
	uint8_t exits;
	int16_t offset = 0;

	switch(Math_Random() & 0x3)
	{
	case 0:
		offset = -NPC_MIN_SPAWN_DISTANCE;
		break;
	case 1:
		offset = NPC_MIN_SPAWN_DISTANCE;
		break;
	default:
		break;
	}

	direction = Math_Random() & 0x3;

	// Which direction is the car going to spawn in, relative to the camera?
	switch(direction)
	{
	case EXIT_NORTH:
		x += offset;
		y -= NPC_MIN_SPAWN_DISTANCE;
		break;
	case EXIT_EAST:
		x += NPC_MIN_SPAWN_DISTANCE;
		y += offset;
		break;
	case EXIT_SOUTH:
		x += offset;
		y += NPC_MIN_SPAWN_DISTANCE;
		break;
	case EXIT_WEST:
		x -= NPC_MIN_SPAWN_DISTANCE;
		y += offset;
		break;
	}

	// Find which macro tile this position falls on
	macroTileX = x >> 7;
	macroTileY = y >> 7;

	exits = World_GetMacroTileExits(macroTileX, macroTileY);

	// Car will be pointing towards camera
	carDirection = (direction + 2) & 0x3;

	// Check if there are valid exits here
	if(!(exits & ((0x1 << carDirection) | (0x1 << direction))))
		return;

	// Which tile on this macro tile:
	tileX = (x >> 3) & 0xF;
	tileY = (y >> 3) & 0xF;

	// Align to center of macro tile
	switch(carDirection)
	{
	case EXIT_NORTH:
		tileX = 9;
		break;
	case EXIT_EAST:
		tileY = 9;
		break;
	case EXIT_SOUTH:
		tileX = 7;
		break;
	case EXIT_WEST:
		tileY = 7;
		break;
	}

	// Check this is a valid position:
	if(tileX < 7)
	{
		if(!(exits & EXIT_WEST_MASK))
			return;
	}
	else if(tileX > 9)
	{
		if(!(exits & EXIT_EAST_MASK))
			return;
	}
	if(tileY < 7)
	{
		if(!(exits & EXIT_NORTH_MASK))
			return;
	}
	else if(tileY > 9)
	{
		if(!(exits & EXIT_SOUTH_MASK))
			return;
	}

	// Calculate fixed point position
	uint16_t spawnX = INT_TO_FIXED(((macroTileX << 4) + tileX) << 3);
	uint16_t spawnY = INT_TO_FIXED(((macroTileY << 4) + tileY) << 3);

	// Check this doesn't collide with any other cars:
	for(uint8_t i = 0; i < NUM_CARS; i++)
	{
		if(cars[i].aiType != AIType_None)
		{
			if(spawnX >= cars[i].x - MIN_CAR_TO_CAR_SPAWN_DISTANCE
			&& spawnY >= cars[i].y - MIN_CAR_TO_CAR_SPAWN_DISTANCE
			&& spawnX <= cars[i].x + MIN_CAR_TO_CAR_SPAWN_DISTANCE
			&& spawnY <= cars[i].y + MIN_CAR_TO_CAR_SPAWN_DISTANCE)
			{
				return;
			}
		}
	}

	// Spawn car
	Car* newCar = Car_Spawn(aiType, spawnX, spawnY, carDirection * DEGREES_90);
	newCar->colorMask = aiType == AIType_Police ? 0xFF : Math_RandomColorMask();
	newCar->aiMacroTileIndex = MT_COORD(macroTileX, macroTileY);

	// Check car direction is a valid place to go
	newCar->aiDirection = carDirection;
	if((exits & (0x1 << newCar->aiDirection)) == 0)
	{
		// Don't come from where we were:
		exits &= ~(0x1 << direction);
		if(exits)
		{
			newCar->aiDirection = Car_GetRandomDirection(exits);
		}
		else
		{
			newCar->aiDirection = 0;
		}
	}
}


void Car_Update()
{
	uint8_t carSlotsAvailable = 0;
	uint8_t numPolice = 0;

	/* Originally supposed to show what position each car was in for race mode:
	 * if(Game_GetMode() & GameMode_Race)
	{
		Car_UpdatePositionDisplays();
	}*/

	for(int n = 0; n < NUM_CARS; n++)
	{
		if(cars[n].aiType == AIType_None)
		{
			carSlotsAvailable++;
			continue;
		}

		if(Mission_State.isRace)
		{
			if(cars[n].aiType == AIType_Player || cars[n].aiType == AIType_RacingOpponent || cars[n].aiType == AIType_Mission)
			{
				Car_UpdateRaceTrackPosition(n);
			}
		}

		switch(cars[n].aiType)
		{
		case AIType_Player:
			Car_UpdatePlayer(&cars[n], 0);
			break;
		case AIType_Player2:
			Car_UpdatePlayer(&cars[n], 1);
			break;
		case AIType_RacingOpponent:
			Car_UpdateRaceAI(&cars[n]);
			break;
		case AIType_NPC:
			Car_UpdateNPCAI(&cars[n]);
			break;
		case AIType_Police:
			Car_UpdatePoliceAI(&cars[n]);
			numPolice ++;
			break;
		case AIType_Mission:
			Car_UpdateMissionAI(&cars[n]);
			break;
		default:
			break;
		}

		Car_UpdatePhysics(&cars[n]);
	}

	Car_CheckCollisions();

	/* This was to sort what position cars were at in a race
	 * if(Game_GetMode() & GameMode_Race)
	{
		Car_SortPositions();
	}*/
	if(carSlotsAvailable > 0 && !Mission_State.blockNPCSpawning)
	{
		const uint8_t spawnFrequency = 10;
		static uint8_t spawnCounter = 0;

		if(!spawnCounter)
		{
			uint8_t aiType = AIType_NPC;

			if(!Mission_State.playerIsCop && (numPolice < Player_Wanted || (numPolice == 0 && (Math_Random() & POLICE_SPAWN_FREQUENCY) == 0)))
			{
				aiType = AIType_Police;
			}
			Car_SpawnNPC(aiType);
			spawnCounter = spawnFrequency;
		}
		else
		{
			spawnCounter --;
		}
	}

	Camera_Update();

	if(Player_Wanted > 0 && Police_OnScreen > 0 && ((Sprites_FrameAnimation & 0x1F) == 0x1))
	{
		TriggerFx(SFX_SIREN, 0x1F, true);
	}

	//PrintInt(0, 31, carSlotsAvailable, false);
}

void Camera_Update()
{
	if(Player_Index < NUM_CARS)
	{
		if(Player2_Index < NUM_CARS)
		{
			MainCamera.x = ((FIXED_TO_INT(cars[Player2_Index].x) + FIXED_TO_INT(cars[Player_Index].x)) >> 1) - HALF_DISPLAY_WIDTH;
			MainCamera.y = ((FIXED_TO_INT(cars[Player2_Index].y) + FIXED_TO_INT(cars[Player_Index].y)) >> 1) - HALF_DISPLAY_HEIGHT;
		}
		else
		{
			MainCamera.x = FIXED_TO_INT(cars[Player_Index].x) - HALF_DISPLAY_WIDTH;
			MainCamera.y = FIXED_TO_INT(cars[Player_Index].y) - HALF_DISPLAY_HEIGHT;
		}
	}

	if(Camera_Shaking)
	{
		MainCamera.x += (Math_Random() & 0x3) - 2;
		Camera_Shaking--;
	}
}

#define HALF_CAR_WIDTH 5
#define HALF_CAR_HEIGHT 5

#define CAR_WIDTH 9
#define CAR_HEIGHT 9

STATIC_INLINE void Particle_Step(Particle* particle, uint8_t angle, bool lateralFriction)
{
	uint16_t tempX = particle->x;
	uint16_t tempY = particle->y;

	int16_t velX = particle->x - particle->prevX;
	int16_t velY = particle->y - particle->prevY;

	//if(0)
	{
	/*	This code is more accurate but takes a lot more cycles
	 *  int8_t normalX, normalY;
		Math_GetForwardVector(angle + DEGREES_90, &normalX, &normalY);
		int16_t dot = FixedDot(normalX, normalY, velX, velY);

		int16_t latVelX = FixedMul(normalX, dot);
		int16_t latVelY = FixedMul(normalY, dot);

		velX -= FixedMul(8, latVelX);
		velY -= FixedMul(8, latVelY);*/

		int8_t normalX, normalY;
		Math_GetForwardVector(angle + DEGREES_90, &normalX, &normalY);
		int16_t dot = ((normalX * velX + FIXED_HALF) >> FIXED_SHIFT) + ((normalY * velY + FIXED_HALF) >> FIXED_SHIFT);

		int16_t latVelX = (normalX * dot + FIXED_HALF) >> FIXED_SHIFT;
		int16_t latVelY = (normalY * dot + FIXED_HALF) >> FIXED_SHIFT;

		velX -= ((latVelX) >> 2);
		velY -= ((latVelY) >> 2);
	}

	if(velX < 0)
	{
		velX += ((-velX) >> FRICTION_SHIFT) + 1;
	}
	else if(velX > 0)
	{
		velX -= ((velX >> FRICTION_SHIFT) + 1);
	}
	if(velY < 0)
	{
		velY += ((-velY) >> FRICTION_SHIFT) + 1;
	}
	else if(velY > 0)
	{
		velY -= (velY >> FRICTION_SHIFT) + 1;
	}

	particle->x += velX;
	particle->y += velY;

	particle->prevX = tempX;
	particle->prevY = tempY;
}

#define PARTICLE_COLLISION_RADIUS 6
bool Particle_CheckParticleCollisions(Particle* a, Particle* b)
{
	int16_t deltaX, deltaY;
	int16_t restLengthSqr, deltaLengthSqr;
	int16_t mult;

	if(a->x < b->x - INT_TO_FIXED(PARTICLE_COLLISION_RADIUS)
	|| a->x > b->x + INT_TO_FIXED(PARTICLE_COLLISION_RADIUS)
	|| a->y < b->y - INT_TO_FIXED(PARTICLE_COLLISION_RADIUS)
	|| a->y > b->y + INT_TO_FIXED(PARTICLE_COLLISION_RADIUS))
		return false;


/*  More accurate but more cycles
 * 	int16_t deltaX = (b->x - a->x);
	int16_t deltaY = (b->y - a->y);
	int16_t restLengthSqr = CONSTRAINT_REST_LENGTH * CONSTRAINT_REST_LENGTH;
	int16_t deltaLengthSqr = ((deltaX * deltaX) + (deltaY * deltaY));
	int16_t mult = (INT_TO_FIXED(restLengthSqr) / (deltaLengthSqr + restLengthSqr)) - FIXED_HALF;

	deltaX *= mult;
	deltaY *= mult;

	a->x -= deltaX;
	a->y -= deltaY;
	b->x += deltaX;
	b->y += deltaY;
 *
 */

	deltaX = b->x - a->x;
	deltaY = b->y - a->y;
	deltaX = FIXED_TO_INT(deltaX);
	deltaY = FIXED_TO_INT(deltaY);

	restLengthSqr = PARTICLE_COLLISION_RADIUS * PARTICLE_COLLISION_RADIUS;
	deltaLengthSqr = (deltaX * deltaX) + (deltaY * deltaY);

	if(deltaLengthSqr > restLengthSqr)
		return false;

	mult = (INT_TO_FIXED(restLengthSqr) / (deltaLengthSqr + restLengthSqr)) - FIXED_HALF;


	deltaX *= mult;
	deltaY *= mult;

	a->x -= deltaX;
	a->y -= deltaY;
	b->x += deltaX;
	b->y += deltaY;

	return true;
}

#define PARTICLE_OFFSET INT_TO_FIXED(3)

bool Particle_CheckCollisionWithOffset(Particle* p, int16_t offX, int16_t offY)
{
	int16_t prevTileX = FIXED_TO_INT(p->prevX) >> 3;
	int16_t prevTileY = FIXED_TO_INT(p->prevY) >> 3;
	int16_t tileX = FIXED_TO_INT(p->x + offX) >> 3;
	int16_t tileY = FIXED_TO_INT(p->y + offY) >> 3;
	uint8_t tile = World_GetTile(tileX, tileY);
	bool collided = false;
	uint16_t xDiff = 0xFF;
	uint16_t yDiff = 0xFF;

	if(tile >= FIRST_COLLIDABLE_TILE)
	{
		if(tileY < prevTileY && World_GetTile(tileX, tileY + 1) < FIRST_COLLIDABLE_TILE)
		{
			yDiff = (INT_TO_FIXED((tileY + 1) << 3) + PARTICLE_OFFSET) - p->y;
			//p->y = INT_TO_FIXED((tileY + 1) << 3) + PARTICLE_OFFSET;
			collided = true;
		}
		else if(tileY > prevTileY && World_GetTile(tileX, tileY - 1) < FIRST_COLLIDABLE_TILE)
		{
			yDiff = p->y - (INT_TO_FIXED(tileY << 3) - PARTICLE_OFFSET);
			//p->y = INT_TO_FIXED(tileY << 3) - PARTICLE_OFFSET;
			collided = true;
		}
		if(tileX < prevTileX && World_GetTile(tileX + 1, tileY) < FIRST_COLLIDABLE_TILE)
		{
			xDiff = (INT_TO_FIXED((tileX + 1) << 3) + PARTICLE_OFFSET) - p->x;
			//p->x = INT_TO_FIXED((tileX + 1) << 3) + PARTICLE_OFFSET;
			collided = true;
		}
		else if(tileX > prevTileX && World_GetTile(tileX - 1, tileY) < FIRST_COLLIDABLE_TILE)
		{
			xDiff = p->x - (INT_TO_FIXED(tileX << 3) - PARTICLE_OFFSET);
			//p->x = INT_TO_FIXED(tileX << 3) - PARTICLE_OFFSET;
			collided = true;
		}

		if(collided)
		{
			if(xDiff < yDiff)
			{
				if(tileX < prevTileX)
				{
					p->x += xDiff;
				}
				else
				{
					p->x -= xDiff;
				}
			}
			else
			{
				if(tileY < prevTileY)
				{
					p->y += yDiff;
				}
				else
				{
					p->y -= yDiff;
				}
			}
		}
	}
	return collided;
}

bool Particle_CheckCollision(Particle* p)
{
	bool collided = false;

	if(p->x < p->prevX)
	{
		collided = Particle_CheckCollisionWithOffset(p, -PARTICLE_OFFSET, 0) || collided;

		if(p->y < p->prevY)
		{
			collided = Particle_CheckCollisionWithOffset(p, -PARTICLE_OFFSET, -PARTICLE_OFFSET) || collided;
		}
		else if(p->y > p->prevY)
		{
			collided = Particle_CheckCollisionWithOffset(p, -PARTICLE_OFFSET, PARTICLE_OFFSET) || collided;
		}
	}
	else if(p->x > p->prevX)
	{
		collided = Particle_CheckCollisionWithOffset(p, PARTICLE_OFFSET, 0) || collided;

		if(p->y < p->prevY)
		{
			collided = Particle_CheckCollisionWithOffset(p, PARTICLE_OFFSET, -PARTICLE_OFFSET) || collided;
		}
		else if(p->y > p->prevY)
		{
			collided = Particle_CheckCollisionWithOffset(p, PARTICLE_OFFSET, PARTICLE_OFFSET) || collided;
		}
	}

	if(p->y < p->prevY)
	{
		collided = Particle_CheckCollisionWithOffset(p, 0, -PARTICLE_OFFSET) || collided;
	}
	else
	{
		collided = Particle_CheckCollisionWithOffset(p, 0, PARTICLE_OFFSET) || collided;
	}

	return collided;
}

#define CONSTRAINT_REST_LENGTH 7
STATIC_INLINE void Particle_Constrain(Particle* a, Particle* b, int16_t unused)
{
	int16_t deltaX = (b->x - a->x);
	int16_t deltaY = (b->y - a->y);
	deltaY = FIXED_TO_INT(deltaY);
	deltaX = FIXED_TO_INT(deltaX);
	int16_t restLengthSqr = CONSTRAINT_REST_LENGTH * CONSTRAINT_REST_LENGTH;
	int16_t deltaLengthSqr = ((deltaX * deltaX) + (deltaY * deltaY));
	int16_t mult = (INT_TO_FIXED(restLengthSqr) / (deltaLengthSqr + restLengthSqr)) - FIXED_HALF;

	deltaX *= mult;
	deltaY *= mult;

	a->x -= deltaX;
	a->y -= deltaY;
	b->x += deltaX;
	b->y += deltaY;
}

#define MAX_WHEEL_TURN 10

#define CAR_BOUNDING_BOX INT_TO_FIXED(16)

void Car_CheckCollisions()
{
	int8_t i, j;

	for(i = 0; i < NUM_CARS; i++)
	{
		if(cars[i].aiType != AIType_None)
		{
			for(j = i + 1; j < NUM_CARS; j++)
			{
				if(cars[j].aiType != AIType_None)
				{
					if(cars[i].x > cars[j].x - CAR_BOUNDING_BOX && cars[i].x < cars[j].x + CAR_BOUNDING_BOX
					&& cars[i].y > cars[j].y - CAR_BOUNDING_BOX && cars[i].y < cars[j].y + CAR_BOUNDING_BOX)
					{
						bool collided = false;
						collided = Particle_CheckParticleCollisions(&cars[i].front, &cars[j].front);
						collided = Particle_CheckParticleCollisions(&cars[i].back, &cars[j].back) || collided;
						collided = Particle_CheckParticleCollisions(&cars[i].front, &cars[j].back) || collided;
						collided = Particle_CheckParticleCollisions(&cars[i].back, &cars[j].front) || collided;

						if(collided && !Mission_State.isDemo)
						{
							if(cars[i].aiType == AIType_Player || cars[j].aiType == AIType_Player)
							{
								if(!Mission_State.inputBlocked && !Mission_State.playerInvincible && !Player_HealthFlashing && Player_Health > 0
								&& (!Mission_State.playerIsCop || cars[i].aiType == AIType_Mission || cars[j].aiType == AIType_Mission || cars[i].aiType == AIType_Player2 || cars[j].aiType == AIType_Player2))
								{
									TriggerFx(SFX_BUMP, 0xff, false);
									Camera_Shake(5);
									Player_Health --;
									UI_UpdateHealth();
									Player_HealthFlashing = PLAYER_HEALTH_FLASH_TIME;

									if(Player_Health == 0)
									{
										if(Mission_State.playerIsCop)
										{
											Mission_Complete();
										}
										else
										{
											Mission_Fail(FailReason_WreckedCar);
										}
									}

									// Raise the wanted level
									if((cars[i].aiType == AIType_Player && cars[i].input)
									|| (cars[j].aiType == AIType_Player && cars[j].input))
									{
										if(cars[i].aiType == AIType_NPC || cars[j].aiType == AIType_NPC)
										{
											Player_CommitOffence(CAR_RAM_WANTED_CHANCE);
										}
										if(cars[i].aiType == AIType_Police || cars[j].aiType == AIType_Police)
										{
											if(Player_Wanted == 0)
											{
												Player_CommitOffence(POLICE_RAM_WANTED_CHANCE);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	for(i = 0; i < NUM_CARS; i++)
	{
		if(cars[i].aiType != AIType_None)
		{
			bool collided = false;
			collided = Particle_CheckCollision(&cars[i].front);
			collided = Particle_CheckCollision(&cars[i].back) || collided;

			if(collided)
			{
				Particle_Constrain(&cars[i].front, &cars[i].back, INT_TO_FIXED(7 * 7));
			}
		}
	}
}

void Car_UpdatePhysics(Car* car)
{
	uint8_t quantizedAngle = ((car->angle + 8) & 0xF0);
	int8_t forwardX, forwardY;

	int8_t turn = 0;
	if(car->input & BTN_LEFT)
	{
		//if(car->forward > 0)
			turn -= 1; //ROTATION_SPEED;
	}
	if(car->input & BTN_RIGHT)
	{
		//if(car->forward > 0)
			turn += 1; //ROTATION_SPEED;
	}

	if(turn == 0)
	{
		car->wheelTurn >>= 1;
	}
	else
	{
		car->wheelTurn += turn;

		if(car->wheelTurn < -MAX_WHEEL_TURN)
		{
			car->wheelTurn = -MAX_WHEEL_TURN;
		}
		else if(car->wheelTurn > MAX_WHEEL_TURN)
		{
			car->wheelTurn = MAX_WHEEL_TURN;
		}
	}

	car->wheelTurn = turn * MAX_WHEEL_TURN;

	Particle_Step(&car->front, quantizedAngle + (car->wheelTurn >> 0), true);
	Particle_Step(&car->back, quantizedAngle, false);

	Math_GetForwardVector(quantizedAngle + (car->wheelTurn >> 0), &forwardX, &forwardY);

	if(car->input & BTN_A)
	{
		car->front.x += forwardX >> 2;
		car->front.y += forwardY >> 2;
	}
	if(car->input & BTN_B)
	{
		car->front.x -= (forwardX * 6) >> FIXED_SHIFT;
		car->front.y -= (forwardY * 6) >> FIXED_SHIFT;
	}


	Particle_Constrain(&car->front, &car->back, INT_TO_FIXED(7 * 7));


	car->x = (car->front.x >> 1) + (car->back.x >> 1);
	car->y = (car->front.y >> 1) + (car->back.y >> 1);

	car->angle = (uint8_t) Math_ATan2(car->front.y - car->back.y, car->front.x - car->back.x);// + 0xFF) & 0xFF;
}

STATIC_INLINE bool Car_NPCOutOfRange(Car* car)
{
	int16_t shiftedX = car->x >> FIXED_SHIFT;
	int16_t shiftedY = car->y >> FIXED_SHIFT;

	if(shiftedX > MainCamera.x + (HALF_DISPLAY_WIDTH + NPC_DESTROY_DISTANCE) || shiftedX < MainCamera.x + HALF_DISPLAY_WIDTH - NPC_DESTROY_DISTANCE ||
			shiftedY > MainCamera.y + (HALF_DISPLAY_HEIGHT + NPC_DESTROY_DISTANCE) || shiftedY < MainCamera.y + HALF_DISPLAY_HEIGHT - NPC_DESTROY_DISTANCE)
	{
		return true;
	}
	return false;
}

void Car_UpdateNPCAI(Car* car)
{
	if(Car_NPCOutOfRange(car))
	{
		car->aiType = AIType_None;
		return;
	}

	AI_Wander(car);

	AI_ThrottleSpeed(car, THROTTLE_SLOW);
}

STATIC_INLINE void Car_SetSprite(Car* car)
{
	int16_t shiftedX = car->x >> FIXED_SHIFT;
	int16_t shiftedY = car->y >> FIXED_SHIFT;

	if(shiftedX > MainCamera.x + DISPLAY_WIDTH || shiftedX < MainCamera.x ||
			shiftedY > MainCamera.y + DISPLAY_HEIGHT || shiftedY < MainCamera.y + 16)
	{
		car->onScreen = false;
		return;
	}

	car->onScreen = true;

	uint8_t angleIndex = ((car->angle + 8) & 0xFF) >> 4;
	uint8_t screenX = shiftedX - MainCamera.x;
	uint8_t screenY = shiftedY - MainCamera.y;// - Screen.overlayHeight * 8;

	uint8_t typeOffset = 0;
	uint8_t colorMask = car->colorMask;

	if(car->aiType == AIType_Police || (car->aiType == AIType_Player && Mission_State.playerIsCop))
	{
		Police_OnScreen ++;

		if(Player_Wanted > 0 || Mission_State.playerIsCop)
		{
			typeOffset = 10 + 10 * (1 + ((Sprites_FrameAnimation >> 3) & 0x1));
		}
		else typeOffset = 10;
	}
	else if(car->aiType == AIType_Player || (car->aiType == AIType_Player2 && Mission_State.playerIsCop) || (car->aiType == AIType_Mission && Mission_State.playerIsCop))
	{
		if(Player_HealthFlashing > 0)
		{
			Player_HealthFlashing --;

			if((Player_HealthFlashing & 0x4) != 0)
			{
				colorMask = COLORMASK_RGB(191, 191, 191);
			}
		}
	}

	uint8_t spriteIndex;
	spriteIndex = Sprites_GetIndex();

	if(spriteIndex < MAX_SPRITES)
	{
		sprites[spriteIndex].tileIndex = pgm_read_byte(&gfx_CarFrames[angleIndex].layout[0].spriteIndex) + typeOffset;
		sprites[spriteIndex].flags = pgm_read_byte(&gfx_CarFrames[angleIndex].layout[0].spriteFlags);
		sprites[spriteIndex].x = screenX + (int8_t)pgm_read_byte(&gfx_CarFrames[angleIndex].layout[0].offsetX);
		sprites[spriteIndex].y = screenY + (int8_t)pgm_read_byte(&gfx_CarFrames[angleIndex].layout[0].offsetY);
		sprites[spriteIndex].colorMask = colorMask;
	}

	spriteIndex = Sprites_GetIndex();

	if(spriteIndex < MAX_SPRITES)
	{
		sprites[spriteIndex].tileIndex = pgm_read_byte(&gfx_CarFrames[angleIndex].layout[1].spriteIndex) + typeOffset;
		sprites[spriteIndex].flags = pgm_read_byte(&gfx_CarFrames[angleIndex].layout[1].spriteFlags);
		sprites[spriteIndex].x = screenX + (int8_t)pgm_read_byte(&gfx_CarFrames[angleIndex].layout[1].offsetX);
		sprites[spriteIndex].y = screenY + (int8_t)pgm_read_byte(&gfx_CarFrames[angleIndex].layout[1].offsetY);
		sprites[spriteIndex].colorMask = colorMask;
	}
}

void Car_SetSprites()
{
	Police_OnScreen = 0;

	for(int n = 0; n < NUM_CARS; n++)
	{
		if(cars[n].aiType != AIType_None)
		{
			Car_SetSprite(&cars[n]);
		}
	}
}


void Car_UpdateRaceTrackPosition(int n)
{
	MacroTileCoord carMacroTile = MT_COORD(
			(FIXED_TO_INT(cars[n].x)) >> 7,
			(FIXED_TO_INT(cars[n].y)) >> 7);

	if(Track_GetCoordFromIndex(cars[n].raceTrackIndex) != carMacroTile)
	{
		uint8_t oldIndex = cars[n].raceTrackIndex;
		cars[n].raceTrackIndex = Track_GetNewIndex(cars[n].raceTrackIndex, carMacroTile);

		if(oldIndex == Track_GetLength() - 1 && cars[n].raceTrackIndex == 0)
		{
			cars[n].lap++;
		}
		if(cars[n].lap > 0 && oldIndex == 0 && cars[n].raceTrackIndex == Track_GetLength() - 1)
		{
			cars[n].lap--;
		}
	}

	uint8_t trackDirection = Track_GetDirection(cars[n].raceTrackIndex);

	switch(trackDirection)
	{
	case EXIT_EAST:
		cars[n].positionTile = (FIXED_TO_INT(cars[n].x) >> 3) & 0xF;
		break;
	case EXIT_SOUTH:
		cars[n].positionTile = (FIXED_TO_INT(cars[n].y) >> 3) & 0xF;
		break;
	case EXIT_WEST:
		cars[n].positionTile = 0xF - ((FIXED_TO_INT(cars[n].x) >> 3) & 0xF);
		break;
	case EXIT_NORTH:
		cars[n].positionTile = 0xF - ((FIXED_TO_INT(cars[n].y) >> 3) & 0xF);
		break;
	default:
		break;
	}

}

int16_t Clamp(int16_t a, int16_t min, int16_t max, int16_t i, int16_t j)
{
	if(a > i && a < j)
		return 0;

	if(a < min)
		a = min;
	if(a > max)
		a = max;

	return a;
}

int8_t Car_CompareRacePosition(Car* first, Car* second)
{
	if(first->lap > second->lap)
	{
		return 1;
	}
	else if(first->lap == second->lap)
	{
		if(first->raceTrackIndex > second->raceTrackIndex)
		{
			return 1;
		}
		else if(first->raceTrackIndex == second->raceTrackIndex)
		{
			if(first->positionTile > second->positionTile)
			{
				return 1;
			}
			else if(first->positionTile == second->positionTile)
			{
				return 0;
			}
		}
	}

	return -1;
}

void Car_UpdateRaceAI(Car* car)
{
	uint8_t macroTileX = (FIXED_TO_INT(car->front.x) >> 7);
	uint8_t macroTileY = (FIXED_TO_INT(car->front.y) >> 7);
	car->aiMacroTileIndex = MT_COORD(macroTileX, macroTileY);

	uint8_t nextIndex = Track_GetNextIndex(car->raceTrackIndex);
	MacroTileCoord nextCoord = Track_GetCoordFromIndex(nextIndex);
	uint8_t trackDirection = AI_GetDirectionTo(car->aiMacroTileIndex, 0xFF, nextCoord);

	AI_DriveInDirection(car, trackDirection);

	if(Player_Index < NUM_CARS)
	{
		int relativeToPlayer = Car_CompareRacePosition(car, &cars[Player_Index]);

		if(relativeToPlayer > 0)
		{
			AI_ThrottleSpeed(car, THROTTLE_MEDIUM);
		}
	}
}

void Car_UpdatePoliceAI(Car* car)
{
	if(Car_NPCOutOfRange(car))
	{
		car->aiType = AIType_None;
		return;
	}

	if(Player_Wanted > 0 && !Mission_State.inputBlocked && Player_Index < NUM_CARS)// && (ReadJoypad(0) & BTN_X))
	{
		AI_ChaseTarget(car, &cars[Player_Index], Player_HealthFlashing == 0 && Player_Health > 0);

		switch(Player_Wanted)
		{
		case 1:
		case 2:
			AI_ThrottleSpeed(car, THROTTLE_MEDIUM);
			break;
		case 3:
			AI_ThrottleSpeed(car, THROTTLE_FAST);
			break;
		default:
			break;
		}
	}
	else
	{
		Car_UpdateNPCAI(car);
	}
}

void Car_UpdateMissionAI(Car* car)
{
	if(Mission_State.playerIsCop && Player_Health == 0)
	{
		car->input = 0;
		return;
	}

	if(Mission_State.failIfLoseMissionCar && Car_NPCOutOfRange(car))
	{
		Mission_State.inputBlocked = true;
		Mission_State.carIsObjective = false;
		Mission_Fail(FailReason_TheyEscaped);
		car->aiType = AIType_None;
		return;
	}

	switch(Mission_State.aiType)
	{
	case AIType_DriveToMarker:
	{
		AI_DriveToTile(car, Mission_State.markerX, Mission_State.markerY);
		AI_ThrottleSpeed(car, THROTTLE_MEDIUM);

		uint16_t posX = car->x >> (FIXED_SHIFT + 3);
		uint16_t posY = car->y >> (FIXED_SHIFT + 3);

		if(posX >= Mission_State.markerX - 1 && posY >= Mission_State.markerY - 1
		&& posX <= Mission_State.markerX + 1 && posY <= Mission_State.markerY + 1)
		{
			Mission_State.waitingForMissionCar = false;
		}
	}
		break;
	case AIType_FleeFast:
		AI_Wander(car);
		break;

	case AIType_Flee:
		AI_Wander(car);
		AI_ThrottleSpeed(car, THROTTLE_MEDIUM);
		break;
	case AIType_RacingOpponent:
		Car_UpdateRaceAI(car);
		break;
	case AIType_None:
		car->front.prevX = car->front.x;
		car->back.prevY = car->back.y;
		car->input = 0;
		break;
	default:
		break;
	}
}

void Car_UpdatePlayerBot(Car* car, uint8_t inputNumber)
{
	if(Mission_State.carIsObjective && Mission_State.carIndex < NUM_CARS)
	{
		AI_ChaseTarget(car, &cars[Mission_State.carIndex], Mission_State.playerIsCop);
	}
	else if(Mission_State.markerIsObjective)
	{
		AI_DriveToTile(car, Mission_State.markerX, Mission_State.markerY);
	}
	else if(Mission_State.isRace)
	{
		Car_UpdateRaceAI(car);
	}
	else
	{
		AI_Wander(car);
	}
}

void Car_UpdatePlayer(Car* car, uint8_t inputNumber)
{
	if(Player_Health == 0)
	{
		car->input = 0;
		return;
	}
	
	if(Mission_State.isDemo)
	{
		Car_UpdatePlayerBot(car, inputNumber);
		return;
	}

	if(Mission_State.inputBlocked)
	{
		car->front.prevX = car->front.x;
		car->back.prevY = car->back.y;
		car->input = 0;
		return;
	}

	if(inputNumber == 1)
	{
		if(!car->onScreen && !Mission_State.inputBlocked)
		{
			Mission_State.inputBlocked = true;
			Mission_Fail(FailReason_TheyEscaped);
			car->aiType = AIType_None;
			Player2_Index = 0xff;
			return;
		}
	}

	car->input = ReadJoypad(inputNumber);

	int16_t velX = (car->front.x - car->front.prevX);
	int16_t velY = (car->front.y - car->front.prevY);
	int16_t vel = (ABS(velX) + ABS(velY)) >> 2;

	if((Sprites_FrameAnimation & 0x3) == 0)
	{
	/* Code for triggering tyre squeals - sounds a bit crap so took it out
	 * if(vel > 20 && (car->input & (BTN_LEFT | BTN_RIGHT)))
	{
		//TriggerNote(2, 40, 80, 127);
		TriggerFx(SFX_TYRES, 127, true);
	}*/
	}
	//else
	{
		TriggerNote(0, 39, 30 + (vel & 0x3F), 127);
	}

#if CHEATS_ENABLED
	if((car->input & BTN_Y))
	{
		Car_UpdatePlayerBot(car, inputNumber);
	}

	if((car->input & BTN_SL))
	{
		Car_UpdateNPCAI(car);
	}
#endif

	if((car->input & BTN_X))
	{
		int8_t xDir = 0;
		int8_t yDir = 0;

		if(car->input & BTN_LEFT)
		{
			xDir --;
		}
		if(car->input & BTN_RIGHT)
		{
			xDir ++;
		}
		if(car->input & BTN_UP)
		{
			yDir --;
		}
		if(car->input & BTN_DOWN)
		{
			yDir ++;
		}

		if(xDir || yDir)
		{
			uint8_t desiredAngle = (uint8_t)Math_ATan2(yDir, xDir);

			AI_TurnToAngle(car, desiredAngle);
		}
	}
}
