/*
 * FixedMath.h
 *
 *  Created on: 8 Mar 2014
 *      Author: James
 */

#ifndef FIXEDMATH_H_
#define FIXEDMATH_H_

#define DEGREES_0 0
#define DEGREES_45 32
#define DEGREES_90 64
#define DEGREES_112 80
#define DEGREES_135 96
#define DEGREES_180 128
#define DEGREES_225 160
#define DEGREES_270 192
#define DEGREES_315 224

#define FIXED_SHIFT 5
#define FIXED_TO_INT(x) ((x) >> FIXED_SHIFT)
#define INT_TO_FIXED(x) ((x) << FIXED_SHIFT)
#define FIXED_ONE (0x1 << FIXED_SHIFT)
#define FIXED_HALF (0x1 << (FIXED_SHIFT - 1))

#define FLOAT_TO_FIXED(x) ((int)((x) * (0x1 << FIXED_SHIFT)))

#define DIFF(x, y) (((x) > (y)) ? ((x) - (y)) : ((y) - (x)))

#define DOT_PRODUCT(x, y, i, j) (( ( (x) * (i) ) >> FIXED_SHIFT ) + ( ( (y) * (j) ) >> FIXED_SHIFT ) )
#define ABS(x) (((x) < 0) ? (-(x)) : (x))

void Math_GetForwardVector(uint8_t angle, int8_t* x, int8_t* y);

int8_t Math_Cos(uint8_t angle);
int8_t Math_Sin(uint8_t angle);
int16_t Math_ATan(int16_t x);
int16_t Math_ATan2(int16_t y, int16_t x);

int16_t Math_Sqrt(int16_t x);

int16_t FixedMul(int16_t a, int16_t b);
int16_t FixedDiv(int16_t a, int16_t b);
int16_t FixedDot(int16_t x, int16_t y, int16_t i, int16_t j);

uint16_t Math_Random();
uint8_t Math_RandomColorMask();


#endif /* FIXEDMATH_H_ */
