/*
 * Track.h
 *
 *  Created on: 8 Mar 2014
 *      Author: James
 */

#ifndef TRACK_H_
#define TRACK_H_

#include "World.h"
#include <stdint.h>
#include <stdbool.h>

#define EXIT_NORTH 3
#define EXIT_EAST 0
#define EXIT_SOUTH 1
#define EXIT_WEST 2
#define EXIT_ALLEYWAY_NORTH 4
#define EXIT_ALLEYWAY_SOUTH 5
#define EXIT_PARKINGLOT_NORTH 6
#define EXIT_PARKINGLOT_SOUTH 7

#define EXIT_NORTH_MASK (0x1 << (EXIT_NORTH))
#define EXIT_EAST_MASK (0x1 << (EXIT_EAST))
#define EXIT_SOUTH_MASK (0x1 << (EXIT_SOUTH))
#define EXIT_WEST_MASK (0x1 << (EXIT_WEST))
#define EXIT_ALLEYWAY_NORTH_MASK (0x1 << (EXIT_ALLEYWAY_NORTH))
#define EXIT_ALLEYWAY_SOUTH_MASK (0x1 << (EXIT_ALLEYWAY_SOUTH))
#define EXIT_PARKINGLOT_NORTH_MASK (0x1 << (EXIT_PARKINGLOT_NORTH))
#define EXIT_PARKINGLOT_SOUTH_MASK (0x1 << (EXIT_PARKINGLOT_SOUTH))

void Track_Set(uint8_t trackNumber);
uint8_t Track_GetDirection(uint8_t index);
MacroTileCoord Track_GetCoordFromIndex(uint8_t index);
uint8_t Track_GetIndexFromCoord(MacroTileCoord coord);
uint8_t Track_GetNewIndex(uint8_t index, MacroTileCoord coord);
int8_t Track_GetExitsAtCoord(MacroTileCoord inCoord);
int8_t Track_GetAllExitsAtCoord(MacroTileCoord inCoord);
uint8_t Track_GetLength();
uint8_t Track_GetNextIndex(uint8_t currentIndex);

#endif /* TRACK_H_ */
