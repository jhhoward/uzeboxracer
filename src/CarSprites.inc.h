/*
 * CarSprites.inc.h
 *
 *  Created on: 24 Jan 2014
 *      Author: James
 */

#ifndef CARSPRITES_INC_H_
#define CARSPRITES_INC_H_

typedef struct
{
	int8_t offsetX, offsetY;
	uint8_t spriteIndex;
	uint8_t spriteFlags;
} SpriteLayoutTile;

typedef struct
{
	SpriteLayoutTile layout[2];
} SpriteLayout;

const SpriteLayout gfx_CarFrames[] PROGMEM =
{
		{	// 0 degrees
				{
						{ -5, -4, 0, 0 },
						{ -2, -4, 1, 0 }
				}
		},
		{	// 22.5 degrees
				{
						{ -6, -5, 2, 0 },
						{ -2, -3, 3, 0 }
				}
		},
		{	// 45 degrees
				{
						{ -6, -6, 4, 0 },
						{ -2, -2, 5, 0 }
				}
		},
		{	// 67.5 degrees
				{
						{ -5, -6, 6, 0 },
						{ -3, -2, 7, 0 }
				}
		},
		{	// 90 degrees
				{
						{ -4, -5, 8, 0 },
						{ -4, -2, 9, 0 }
				}
		},
		{	// 112.5 degrees
				{
						{ -3, -6, 6, SPRITE_FLIP_X },
						{ -5, -2, 7, SPRITE_FLIP_X }
				}
		},
		{	// 135 degrees
				{
						{ -2, -6, 4, SPRITE_FLIP_X },
						{ -6, -2, 5, SPRITE_FLIP_X }
				}
		},
		{	// 157.5 degrees
				{
						{ -2, -5, 2, SPRITE_FLIP_X },
						{ -6, -3, 3, SPRITE_FLIP_X }
				}
		},
		{	// 180 degrees
				{
						{ -2, -5, 0, SPRITE_FLIP_X | SPRITE_FLIP_Y },
						{ -5, -5, 1, SPRITE_FLIP_X | SPRITE_FLIP_Y }
				}
		},
		{	// 202.5 degrees
				{
						{ -2, -3, 2, SPRITE_FLIP_X | SPRITE_FLIP_Y },
						{ -6, -5, 3, SPRITE_FLIP_X | SPRITE_FLIP_Y }
				}
		},
		{	// 225 degrees
				{
						{ -2, -2, 4, SPRITE_FLIP_X | SPRITE_FLIP_Y },
						{ -6, -6, 5, SPRITE_FLIP_X | SPRITE_FLIP_Y }
				}
		},
		{	// 247.5 degrees
				{
						{ -3, -2, 6, SPRITE_FLIP_X | SPRITE_FLIP_Y },
						{ -5, -6, 7, SPRITE_FLIP_X | SPRITE_FLIP_Y }
				}
		},
		{	// 270 degrees
				{
						{ -4, -3, 8, SPRITE_FLIP_X | SPRITE_FLIP_Y },
						{ -4, -6, 9, SPRITE_FLIP_X | SPRITE_FLIP_Y }
				}
		},
		{	// 292.5 degrees
				{
						{ -5, -2, 6, SPRITE_FLIP_Y },
						{ -3, -6, 7, SPRITE_FLIP_Y }
				}
		},
		{	// 315 degrees
				{
						{ -6, -3, 4, SPRITE_FLIP_Y },
						{ -2, -7, 5, SPRITE_FLIP_Y }
				}
		},
		{	// 337.5 degrees
				{
						{ -6, -4, 2, SPRITE_FLIP_Y },
						{ -2, -6, 3, SPRITE_FLIP_Y }
				}
		},

};


#endif /* CARSPRITES_INC_H_ */
