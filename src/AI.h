/*
 * AI.h
 *
 *  Created on: 3 Jul 2014
 *      Author: James
 */

#ifndef AI_H_
#define AI_H_

#include <stdint.h>
#include <stdbool.h>
#include "Car.h"

#define THROTTLE_SLOW 0x1
#define THROTTLE_MEDIUM 0x3
#define THROTTLE_FAST 0x7

void AI_ChaseTarget(Car* car, Car* target, bool ram);
uint8_t AI_GetDirectionTo(uint8_t current, uint8_t exits, uint8_t targetCoord);
uint8_t AI_GetPathScore(uint8_t start, uint8_t exit, uint8_t target);
uint8_t AI_GetExitsFromTo(uint8_t from, uint8_t macroTileX, uint8_t macroTileY);
uint8_t AI_GetExitFromTo(uint8_t from, uint8_t to);
void AI_DriveInDirection(Car* car, uint8_t trackDirection);
void AI_TurnToAngle(Car* car, uint8_t desiredAngle);
void AI_ReverseToAngle(Car* car, uint8_t desiredAngle);
bool AI_DriveToTile(Car* car, uint8_t x, uint8_t y);
void AI_ThrottleSpeed(Car* car, uint8_t amount);
void AI_Wander(Car* car);
uint8_t Car_GetRandomDirection(uint8_t exits);

#endif /* AI_H_ */
