/*
 * World.h
 *
 *  Created on: 24 Jan 2014
 *      Author: James
 */

#ifndef WORLD_H_
#define WORLD_H_

#include <stdint.h>

#define WORLD_WIDTH 16
#define WORLD_HEIGHT 16
#define FIRST_COLLIDABLE_TILE 19

typedef struct
{
	int16_t x, y;
} Vector2;

enum
{
	WORLD_TILE_BUILDINGS = 0,
	WORLD_TILE_ROAD_NS,
	WORLD_TILE_ROAD_EW,
	WORLD_TILE_ROAD_ES,
	WORLD_TILE_ROAD_SW,
	WORLD_TILE_ROAD_NE,
	WORLD_TILE_ROAD_NW,
	WORLD_TILE_ROAD_NESW,
	WORLD_TILE_ROAD_ESW,
	WORLD_TILE_ROAD_NSW,
	WORLD_TILE_ROAD_NEW,
	WORLD_TILE_ROAD_NES,
	WORLD_TILE_RIVER_NS,
	WORLD_TILE_RIVER_EW,
	WORLD_TILE_RIVER_ES,
	WORLD_TILE_RIVER_SW,
	WORLD_TILE_RIVER_NE,
	WORLD_TILE_RIVER_NW,
	WORLD_TILE_BRIDGE_EW,
	WORLD_TILE_BRIDGE_NS,
	WORLD_TILE_ROAD_EW2,
	WORLD_TILE_ROAD_EW_ALLEY_N,
	WORLD_TILE_ROAD_EW_ALLEY_S,
	WORLD_TILE_ROAD_EW3,
	WORLD_TILE_OPEN_AREA
};

#define MT_COORD(x, y) (((x) << 4) + (y))
typedef uint8_t MacroTileCoord;

extern Vector2 MainCamera;

void World_UpdateTiles();
void World_UpdateAllTiles();
void World_Init();
uint8_t World_GetTile(int16_t x, int16_t y);
const char* World_GetLayout();
const char* World_GetLayoutTiles();
const char* World_GetHUDLayout();
const char* World_GetLogoLayout();
uint8_t World_GetMacroTileExits(int8_t x, int8_t y);
uint8_t World_GetAllMacroTileExits(int8_t x, int8_t y);
void World_SetCustomTile(uint8_t worldX, uint8_t worldY, uint8_t tile);
void World_ResetCustomTile(uint8_t worldX, uint8_t worldY);


#endif /* WORLD_H_ */
