/*
 * Track.c
 *
 *  Created on: 8 Mar 2014
 *      Author: James
 */

#include <stdlib.h>
#include "Track.h"
#include "TrackData.inc.h"

static uint8_t Track_CurrentIndex = 0;
static const MacroTileCoord* Track_Current;
static uint8_t Track_Length;

uint8_t Track_GetLength()
{
	return Track_Length;
}

void Track_Set(uint8_t trackNumber)
{
	Track_CurrentIndex = trackNumber;

	switch(trackNumber)
	{
	case 0:
		Track_Current = TrackData_0;
		Track_Length = sizeof(TrackData_0);
		break;
	default:
		break;
	}
}

MacroTileCoord Track_GetCoordFromIndex(uint8_t index)
{
	return pgm_read_byte(&Track_Current[index]);
}

uint8_t Track_GetIndexFromCoord(MacroTileCoord coord)
{
	for(int n = 0; n < Track_Length; n++)
	{
		if(pgm_read_byte(&Track_Current[n]) == coord)
		{
			return n;
		}
	}

	return 0;
}

uint8_t Track_GetNewIndex(uint8_t index, MacroTileCoord coord)
{
	for(int n = index + 1; n != index; n++)
	{
		if(n >= Track_Length)
		{
			n = 0;
		}
		if(pgm_read_byte(&Track_Current[n]) == coord)
		{
			return n;
		}
	}

	return index;
}

uint8_t Track_GetNextIndex(uint8_t currentIndex)
{
	if(currentIndex == Track_Length - 1)
	{
		return 0;
	}
	else
	{
		return currentIndex + 1;
	}
}

int8_t Track_GetExitsAtCoord(MacroTileCoord inCoord)
{
	int8_t result = 0;

	for(int n = 0; n < Track_Length; n++)
	{
		if(pgm_read_byte(&Track_Current[n]) == inCoord)
		{
			uint8_t currentIndex = n;
			uint8_t nextIndex = n + 1;
			uint8_t prevIndex = n - 1;

			if(currentIndex == 0)
			{
				prevIndex = Track_Length - 1;
			}
			if(nextIndex == Track_Length)
			{
				nextIndex = 0;
			}

			MacroTileCoord prev = pgm_read_byte(&Track_Current[prevIndex]);
			MacroTileCoord current = pgm_read_byte(&Track_Current[currentIndex]);
			MacroTileCoord next = pgm_read_byte(&Track_Current[nextIndex]);

			if(current == prev + 1)
			{
				result |= (EXIT_NORTH_MASK);
			}
			else if(current == prev - 1)
			{
				result |= (EXIT_SOUTH_MASK);
			}
			else if(current == prev - 16)
			{
				result |= (EXIT_EAST_MASK);
			}
			else if(current == prev + 16)
			{
				result |= (EXIT_WEST_MASK);
			}

			if(next == current + 1)
			{
				result |= (EXIT_SOUTH_MASK);
			}
			else if(next == current - 1)
			{
				result |= (EXIT_NORTH_MASK);
			}
			else if(next == current + 16)
			{
				result |= (EXIT_EAST_MASK);
			}
			else if(next == current - 16)
			{
				result |= (EXIT_WEST_MASK);
			}


			return result;
		}
	}

	return 0xF;
}

uint8_t Track_GetDirection(uint8_t index)
{
	int nextIndex = index + 1;

	if(nextIndex >= Track_Length)
	{
		nextIndex = 0;
	}

	MacroTileCoord current = pgm_read_byte(&Track_Current[index]);
	MacroTileCoord next = pgm_read_byte(&Track_Current[nextIndex]);

	if(current == next + 1)
	{
		return EXIT_NORTH;
	}
	if(current == next - 1)
	{
		return EXIT_SOUTH;
	}
	if(current == next + 16)
	{
		return EXIT_WEST;
	}
	if(current == next - 16)
	{
		return EXIT_EAST;
	}

	return -1;
}

