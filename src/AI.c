#include <uzebox.h>
#include "AI.h"
#include "Car.h"
#include "World.h"
#include "Track.h"
#include "Sprites.h"

#define NEAR_THRESHOLD 32
#define RAM_THRESHOLD 16

uint8_t AI_GetExitFromTo(uint8_t from, uint8_t to)
{
	if(to == from + 1)
		return EXIT_NORTH;
	else if(to == from - 1)
		return EXIT_SOUTH;
	else if(to == from - WORLD_WIDTH)
		return EXIT_EAST;
	else if(to == from + WORLD_WIDTH)
		return EXIT_WEST;

	return 0xFF;
}

uint8_t AI_GetPathScore(uint8_t start, uint8_t exit, uint8_t target)
{
	uint8_t x, y;
	uint8_t current = start;
	uint8_t targetX = target >> 4;
	uint8_t targetY = target & 0xF;

	x = start >> 4;
	y = start & 0xF;

	while(1)
	{
		switch(exit)
		{
		case EXIT_NORTH:
		case EXIT_ALLEYWAY_NORTH:
		case EXIT_PARKINGLOT_NORTH:
			y --;
			break;
		case EXIT_EAST:
			x ++;
			break;
		case EXIT_SOUTH:
		case EXIT_ALLEYWAY_SOUTH:
		case EXIT_PARKINGLOT_SOUTH:
			y ++;
			break;
		case EXIT_WEST:
			x --;
			break;
		default:
			break;
		}

		// reached target - perfect score
		if(x == targetX && y == targetY)
		{
			return 0;
		}

		// looped - worst score
		if(MT_COORD(x, y) == start)
		{
			return 0xFF;
		}

		uint8_t exits = AI_GetExitsFromTo(current, x, y);

		// Dead end
		if(exits == 0)
		{
			return 0xFF;
		}

		exit = 0;
		while(exits)
		{
			if(exits & 0x1)
			{
				if(exits != 0x1)
				{
					// More than one direction to go in
					return DIFF(x, targetX) + DIFF(y, targetY);
				}
				break;
			}
			exit ++;
			exits >>= 1;
		}
	}

	return 0xFF;
}


void AI_ChaseTarget(Car* car, Car* target, bool ram)
{
	uint8_t targetMacroX = (FIXED_TO_INT(target->front.x) >> 7);
	uint8_t targetMacroY = (FIXED_TO_INT(target->front.y) >> 7);
	uint8_t macroTileX = (FIXED_TO_INT(car->front.x) >> 7);
	uint8_t macroTileY = (FIXED_TO_INT(car->front.y) >> 7);
	uint8_t macroTileIndex = MT_COORD(macroTileX, macroTileY);

	int16_t diffX = FIXED_TO_INT(car->front.x) - FIXED_TO_INT(target->front.x);
	int16_t diffY = FIXED_TO_INT(car->front.y) - FIXED_TO_INT(target->front.y);

	diffX = ABS(diffX);
	diffY = ABS(diffY);

	if(!ram && diffX < RAM_THRESHOLD && diffY < RAM_THRESHOLD)
	{
		uint8_t desiredAngle = (uint8_t)Math_ATan2(target->front.y - car->front.y, target->front.x - car->front.x);

		AI_ReverseToAngle(car, desiredAngle);
		car->aiMacroTileIndex = 0;
	}
	else if(!ram && diffX < RAM_THRESHOLD + 2 && diffY < RAM_THRESHOLD + 2)
	{
		car->input = 0;
	}
	else if((diffX < NEAR_THRESHOLD && diffY < NEAR_THRESHOLD)
			|| 	(macroTileX == targetMacroX && macroTileY == targetMacroY))
	{
		uint8_t desiredAngle = (uint8_t)Math_ATan2(target->front.y - car->front.y, target->front.x - car->front.x);

		AI_TurnToAngle(car, desiredAngle);
		car->aiMacroTileIndex = 0;
	}
	else
	{
/*		if(macroTileX == targetMacroX && macroTileY == targetMacroY)
		{
			car->input = 0;
			return;
		}
*/
		if(car->aiMacroTileIndex != macroTileIndex)
		{
			uint8_t lastExit = 0xFF;
			uint8_t exits;

			exits = World_GetAllMacroTileExits(macroTileX, macroTileY);
			lastExit = AI_GetExitFromTo(car->aiMacroTileIndex, macroTileIndex);

			// Only go back if it leads to the target
			if(lastExit != 0xFF && AI_GetPathScore(macroTileIndex, lastExit, MT_COORD(targetMacroX, targetMacroY)) != 0)
			{
				exits &= ~(0x1 << lastExit);
			}

			car->aiMacroTileIndex = macroTileIndex;

			car->aiDirection = AI_GetDirectionTo(macroTileIndex, exits, MT_COORD(targetMacroX, targetMacroY));
		}

		AI_DriveInDirection(car, car->aiDirection);

	}

}

uint8_t AI_GetDirectionTo(uint8_t current, uint8_t exits, uint8_t targetCoord)
{
	uint8_t bestExit = 0xFF;
	uint8_t currentExit = 0;
	uint8_t bestScore = 0xFF;

	if(exits == 0)
		return 0;

	while(exits)
	{
		if(exits & 0x1)
		{
			uint8_t score = AI_GetPathScore(current, currentExit, targetCoord);
			if(bestExit == 0xFF || score < bestScore)
			{
				bestExit = currentExit;
				bestScore = score;
			}
		}

		currentExit ++;
		exits >>= 1;
	}

	return bestExit;
}

uint8_t AI_GetExitsFromTo(uint8_t from, uint8_t macroTileX, uint8_t macroTileY)
{
	uint8_t exits = World_GetAllMacroTileExits(macroTileX, macroTileY);
	uint8_t to = MT_COORD(macroTileX, macroTileY);

	if(to == from + 1)
		exits &= ~(EXIT_NORTH_MASK | EXIT_ALLEYWAY_NORTH_MASK | EXIT_PARKINGLOT_NORTH_MASK);
	else if(to == from - 1)
		exits &= ~(EXIT_SOUTH_MASK | EXIT_ALLEYWAY_SOUTH_MASK | EXIT_PARKINGLOT_SOUTH_MASK);
	else if(to == from - WORLD_WIDTH)
		exits &= ~(EXIT_EAST_MASK);
	else if(to == from + WORLD_WIDTH)
		exits &= ~(EXIT_WEST_MASK);

	return exits;
}

void AI_DriveInDirection(Car* car, uint8_t trackDirection)
{
	uint8_t tileX = ((car->front.x + ((car->front.x - car->front.prevX) << 2)) >> (FIXED_SHIFT + 3)) & 0xF;
	uint8_t tileY = ((car->front.y + ((car->front.y - car->front.prevY) << 2)) >> (FIXED_SHIFT + 3)) & 0xF;

	uint8_t desiredAngle = car->angle;

	// down
	if(trackDirection == EXIT_SOUTH)
	{
		if(tileX < 4)
		{
			desiredAngle = DEGREES_0;
		}
		else if(tileX < 7)
		{
			desiredAngle = DEGREES_45;
		}
		else if(tileX < 8)
		{
			desiredAngle = DEGREES_90;
		}
		else if(tileX < 12)
		{
			desiredAngle = DEGREES_135;
		}
		else
		{
			desiredAngle = DEGREES_180;
		}
	}
	// up
	else if(trackDirection == EXIT_NORTH)
	{
		if(tileX < 4)
		{
			desiredAngle = DEGREES_0;
		}
		else if(tileX < 8)
		{
			desiredAngle = DEGREES_315;
		}
		else if(tileX < 9)
		{
			desiredAngle = DEGREES_270;
		}
		else if(tileX < 10)
		{
			desiredAngle = DEGREES_225;
		}
		else
		{
			desiredAngle = DEGREES_180;
		}
	}
	// left
	else if(trackDirection == EXIT_WEST)
	{
		if(tileY < 4)
		{
			desiredAngle = DEGREES_90;
		}
		else if(tileY < 7)
		{
			desiredAngle = DEGREES_135;
		}
		else if(tileY < 8)
		{
			desiredAngle = DEGREES_180;
		}
		else if(tileY < 12)
		{
			desiredAngle = DEGREES_225;
		}
		else
		{
			desiredAngle = DEGREES_270;
		}
	}
	// right
	else if(trackDirection == EXIT_EAST)
	{
		if(tileY < 4)
		{
			desiredAngle = DEGREES_90;
		}
		else if(tileY < 9)
		{
			desiredAngle = DEGREES_45;
		}
		else if(tileY < 10)
		{
			desiredAngle = DEGREES_0;
		}
		else if(tileY < 11)
		{
			desiredAngle = DEGREES_315;
		}
		else
		{
			desiredAngle = DEGREES_270;
		}
	}
	// alley down
	else if(trackDirection == EXIT_ALLEYWAY_SOUTH)
	{
		if(tileX < 6)
		{
			desiredAngle = DEGREES_0;
		}
		else if(tileX < 7)
		{
			desiredAngle = DEGREES_45;
		}
		else if(tileX < 9)
		{
			desiredAngle = DEGREES_90;
		}
		else if(tileX < 10)
		{
			desiredAngle = DEGREES_135;
		}
		else
		{
			desiredAngle = DEGREES_180;
		}
	}
	// alley up
	else if(trackDirection == EXIT_ALLEYWAY_NORTH)
	{
		if(tileX < 6)
		{
			desiredAngle = DEGREES_0;
		}
		else if(tileX < 7)
		{
			desiredAngle = DEGREES_315;
		}
		else if(tileX < 9)
		{
			desiredAngle = DEGREES_270;
		}
		else if(tileX < 10)
		{
			desiredAngle = DEGREES_225;
		}
		else
		{
			desiredAngle = DEGREES_180;
		}
	}
	// parking lot down
	else if(trackDirection == EXIT_PARKINGLOT_SOUTH)
	{
		if(tileX < 1)
		{
			desiredAngle = DEGREES_0;
		}
		else if(tileX < 2)
		{
			desiredAngle = DEGREES_45;
		}
		else if(tileX < 5)
		{
			desiredAngle = DEGREES_90;
		}
		else if(tileX < 7)
		{
			desiredAngle = DEGREES_135;
		}
		else
		{
			desiredAngle = DEGREES_180;
		}
	}
	// parking lot up
	else if(trackDirection == EXIT_PARKINGLOT_NORTH)
	{
		if(tileX < 1)
		{
			desiredAngle = DEGREES_0;
		}
		else if(tileX < 2)
		{
			desiredAngle = DEGREES_315;
		}
		else if(tileX < 5)
		{
			desiredAngle = DEGREES_270;
		}
		else if(tileX < 7)
		{
			desiredAngle = DEGREES_225;
		}
		else
		{
			desiredAngle = DEGREES_180;
		}
	}


	AI_TurnToAngle(car, desiredAngle);
}

void AI_ReverseToAngle(Car* car, uint8_t desiredAngle)
{
	uint8_t quantizedAngle = (car->angle + 8) & 0xF0;

	int8_t diff = desiredAngle - quantizedAngle;

	if(diff < 0)
	{
		car->input = BTN_B | BTN_RIGHT;
	}
	else if(diff > 0)
	{
		car->input = BTN_B | BTN_LEFT;
	}
	else
	{
		car->input = BTN_B;
	}
}


void AI_TurnToAngle(Car* car, uint8_t desiredAngle)
{
	uint8_t quantizedAngle = (car->angle + 8) & 0xF0;

	int8_t diff = desiredAngle - quantizedAngle;

	if(diff < 0)
	{
		if(diff < -DEGREES_112)
		{
//			car->input = BTN_A | BTN_LEFT;
//			AI_ThrottleSpeed(car, THROTTLE_SLOW);
			car->input = BTN_B | BTN_RIGHT;
		}
		else
		{
			car->input = BTN_A | BTN_LEFT;

			if(diff < -DEGREES_45)
			{
				AI_ThrottleSpeed(car, THROTTLE_MEDIUM);
			}
		}
	}
	else if(diff > 0)
	{
		if(diff > DEGREES_112)
		{
//			car->input = BTN_A | BTN_RIGHT;
//			AI_ThrottleSpeed(car, THROTTLE_SLOW);
			car->input = BTN_B | BTN_LEFT;
		}
		else
		{
			car->input = BTN_A | BTN_RIGHT;

			if(diff > DEGREES_45)
			{
				AI_ThrottleSpeed(car, THROTTLE_MEDIUM);
			}
		}
	}
	else
	{
		car->input = BTN_A;
	}
}

// Which exit are we closest to?
uint8_t AI_GetExitCone(uint8_t tileX, uint8_t tileY)
{
	tileX &= 0xF;
	tileY &= 0xF;

	if(tileY < 8)
	{
		if(tileY > tileX)
		{
			return EXIT_WEST;
		}
		if(tileY > (15 - tileX))
		{
			return EXIT_EAST;
		}
		return EXIT_NORTH;
	}
	else
	{
		if(tileY < tileX)
		{
			return EXIT_WEST;
		}
		if(tileY < (15 - tileX))
		{
			return EXIT_EAST;
		}
		return EXIT_SOUTH;
	}
}

bool AI_DriveToTile(Car* car, uint8_t targetX, uint8_t targetY)
{
	uint8_t targetMacroX = ((targetX) >> 4);
	uint8_t targetMacroY = ((targetY) >> 4);
	uint8_t macroTileX = (FIXED_TO_INT(car->front.x) >> 7);
	uint8_t macroTileY = (FIXED_TO_INT(car->front.y) >> 7);
	uint8_t macroTileIndex = MT_COORD(macroTileX, macroTileY);

	// In a different macro tile
	if(macroTileX != targetMacroX || macroTileY != targetMacroY)
	{
		if(car->aiMacroTileIndex != macroTileIndex)
		{
			uint8_t lastExit = 0xFF;
			uint8_t exits;

			exits = World_GetAllMacroTileExits(macroTileX, macroTileY);
			lastExit = AI_GetExitFromTo(car->aiMacroTileIndex, macroTileIndex);

			if(lastExit != 0xFF)
			{
				exits &= ~(0x1 << lastExit);
			}

			car->aiMacroTileIndex = macroTileIndex;

			car->aiDirection = AI_GetDirectionTo(macroTileIndex, exits, MT_COORD(targetMacroX, targetMacroY));
		}

		AI_DriveInDirection(car, car->aiDirection);
		return false;
	}
	else // Same macro tile
	{
		uint8_t tileX = (FIXED_TO_INT(car->front.x) >> 3);
		uint8_t tileY = (FIXED_TO_INT(car->front.y) >> 3);

		// At the target tile
		if(tileX == targetX && tileY == targetY)
		{
			car->input = 0;
			return true;
		}

		// Far enough away, check to see if in a different exit cone
		if(tileX < targetX - 3 || tileX > targetX + 3
		|| tileY < targetY - 3 || tileY > targetY + 3)
		{
			uint8_t targetCone = AI_GetExitCone(targetX, targetY);
			uint8_t currentCone = AI_GetExitCone(tileX, tileY);

			if(targetCone != currentCone)
			{
				AI_DriveInDirection(car, targetCone);
				return false;
			}
		}

		// Drive straight towards tile center
		int16_t posX = ((targetX << 3) + 4);
		int16_t posY = ((targetY << 3) + 4);
		posX -= FIXED_TO_INT(car->front.x);
		posY -= FIXED_TO_INT(car->front.y);
		uint8_t desiredAngle = (uint8_t)Math_ATan2(posY, posX);

		AI_TurnToAngle(car, desiredAngle);

		return false;
	}
}

void AI_ThrottleSpeed(Car* car, uint8_t amount)
{
	if((car->input & BTN_A) && !(Sprites_FrameAnimation & amount))
	{
		car->input &= (~BTN_A);
	}
}

void AI_Wander(Car* car)
{
	uint8_t macroTileX = (car->x >> (FIXED_SHIFT + 7));
	uint8_t macroTileY = (car->y >> (FIXED_SHIFT + 7));
	uint8_t macroTileIndex = MT_COORD(macroTileX, macroTileY);

	if(car->aiMacroTileIndex != macroTileIndex)
	{
		uint8_t exits = World_GetMacroTileExits(macroTileX, macroTileY);
		
		if(exits == 0)
		{
			exits = World_GetAllMacroTileExits(macroTileX, macroTileY);
		}
		
		if(macroTileIndex == car->aiMacroTileIndex + 1)
			exits &= ~(EXIT_NORTH_MASK);
		else if(macroTileIndex == car->aiMacroTileIndex - 1)
			exits &= ~(EXIT_SOUTH_MASK);
		else if(macroTileIndex == car->aiMacroTileIndex - WORLD_WIDTH)
			exits &= ~(EXIT_EAST_MASK);
		else if(macroTileIndex == car->aiMacroTileIndex + WORLD_WIDTH)
			exits &= ~(EXIT_WEST_MASK);
		else
		{
			uint8_t facingDirection = (car->angle + 16) >> 6;
			facingDirection = (facingDirection + 2) & 0x3;
			exits &= ~(0x1 << facingDirection);
		}

		car->aiMacroTileIndex = macroTileIndex;

		car->aiDirection = Car_GetRandomDirection(exits);
	}

	AI_DriveInDirection(car, car->aiDirection);
}
