#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include <avr/pgmspace.h>
#include <uzebox.h>

#include "World.h"
#include "Car.h"
#include "Track.h"
#include "Menu.h"
#include "Main.h"
#include "Sprites.h"
#include "Ped.h"
#include "Mission.h"
#include "UI.h"

#define SHOW_FRAMERATE 0

#include "data/patches.inc"

static int8_t Game_State = GameState_Init;
uint8_t Game_Frame = 0;

void VsyncCallBack(void)
{
	Game_Frame ++;
}

void SoundTest()
{
	if(ReadJoypad(0) & BTN_SL)
	{
		TriggerFx(31, 0xFF, true);
	}
	if(ReadJoypad(0) & BTN_SR)
	{
		TriggerFx(32, 0xFF, true);
	}

	while(ReadJoypad(0));
}

void Game_Vsync()
{
	WaitVsync(1);

#if SHOW_FRAMERATE
	switch(Game_Frame)
	{

	case 1:
		Print(0, 31, PSTR("60"));
		break;
	case 2:
		Print(0, 31, PSTR("30"));
		break;
	case 3:
		Print(0, 31, PSTR("20"));
		break;
	case 4:
		Print(0, 31, PSTR("15"));
		break;
	case 5:
		Print(0, 31, PSTR("12"));
		break;
	case 6:
		Print(0, 31, PSTR("10"));
		break;
	default:
		Print(0, 31, PSTR("--"));
		break;
	}
#endif

	Game_Frame = 0;
}

void Game_Init()
{
    SetUserPreVsyncCallback(&VsyncCallBack);
	InitMusicPlayer(patches);
	SetMasterVolume(0x40);

	ClearVram();

	Track_Set(0);

	Game_SetState(GameState_Menu);
}

void Game_Update()
{
	Game_Vsync();
	Sprites_Reset();

	switch(Game_State)
	{
	case GameState_Play:
		World_UpdateTiles();

		Ped_SetSprites();
		Car_SetSprites();
		Mission_SetSprites();

		Mission_Update();
		Ped_Update();
		Car_Update();
		UI_Update();
		
		if(Mission_State.isDemo && ReadJoypad(0))
		{
			Game_SetState(GameState_Menu);
		}

		if((ReadJoypad(0) & BTN_START) && !Mission_State.inEndMenu)
		{
			TriggerFx(SFX_PAUSE, 0xff, true);
			Game_SetState(GameState_Paused);
		}
		break;

	case GameState_Paused:
		PauseMenu_Update();
		Menu_Update();
		break;

	case GameState_Menu:
		Menu_Update();
		break;
	default:
		break;
	}
}

#ifdef PLATFORM_AVR
int main()
{
	Game_Init();
	
	while (1)
	{
		Game_Update();
	}

	return 0;
}
#endif

void Game_Reset()
{
	Game_SetState(GameState_Play);
}

void Game_SetState(uint8_t newState)
{
	if(Game_State != GameState_Init)
	{
		FadeOut(1, true);
	}

	Sprites_Reset();

	uint8_t oldState = Game_State;
	Game_State = newState;

	switch(newState)
	{
	case GameState_Menu:
		Menu_Init();
		break;
	case GameState_Play:
		if(oldState != GameState_Paused)
		{
			Sprites_Init();
			Car_Init();
			Ped_Init();
			Mission_Init();
			UI_Init();
			Camera_Update();
		}
		World_Init();

		Car_SetSprites();
		Ped_SetSprites();
		break;
	case GameState_Paused:
		PauseMenu_Init();
		break;
	default:
		break;
	}

	if(oldState != GameState_Init)
	{
		FadeIn(1, true);
	}
}

uint8_t Game_GetState()
{
	return Game_State;
}

