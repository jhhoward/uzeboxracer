/*
 * MenuData.inc.h
 *
 *  Created on: 12 Jul 2014
 *      Author: James
 */

#ifndef MENUDATA_INC_H_
#define MENUDATA_INC_H_

#include "Mission.h"

// Strings
const char MenuStr_Story[] PROGMEM = 		"STORY";
const char MenuStr_FreeRoam[] PROGMEM = 	"FREE ROAM";
const char MenuStr_Arcade[] PROGMEM = 		"ARCADE";
const char MenuStr_1Player[] PROGMEM = 		"1 PLAYER";
const char MenuStr_2Player[] PROGMEM = 		"2 PLAYER";
const char MenuStr_Survival[] PROGMEM = 	"SURVIVAL";
const char MenuStr_Pursuit[] PROGMEM = 		"PURSUIT";
const char MenuStr_Race[] PROGMEM = 		"RACE";
const char MenuStr_Back[] PROGMEM = 		"BACK";
const char MenuStr_Continue[] PROGMEM = 	"CONTINUE";
const char MenuStr_Restart[] PROGMEM = 		"RESTART";
const char MenuStr_Exit[] PROGMEM = 		"EXIT";

const char MenuStr_Mission1[] PROGMEM = 	"BANK JOB";
const char MenuStr_Mission2[] PROGMEM = 	"COLLECTOR";
const char MenuStr_Mission3[] PROGMEM = 	"STREET RACE";
const char MenuStr_Mission4[] PROGMEM = 	"MISSION 4";
const char MenuStr_Mission5[] PROGMEM = 	"MISSION 5";

const void* Menu_MissionMenu[] PROGMEM =
{
	MenuStr_Mission1,			&MenuFn_StartMission, Mission_Story1,
	MenuStr_Mission2,			&MenuFn_StartMission, Mission_Story2,
	MenuStr_Mission3,			&MenuFn_StartMission, Mission_Story3,
//	MenuStr_Mission4,			&MenuFn_StartMission, Mission_Story4,
//	MenuStr_Mission5,			&MenuFn_StartMission, Mission_Story5,
	MENU_ENTRY_END
};

const void* Menu_ModeSelectMenu[] PROGMEM =
{
	MenuStr_Survival,		&MenuFn_StartMission, Mission_Survival,
	MenuStr_Pursuit,		&MenuFn_StartMission, Mission_Pursuit,
//	MenuStr_Race,			&MenuFn_StartMission, Mission_Test,
	MENU_ENTRY_END
};

const void* Menu_ModeSelect2PMenu[] PROGMEM =
{
	MenuStr_Pursuit,		&MenuFn_StartMission, Mission_Pursuit2P,
	//MenuStr_Race,			&MenuFn_StartMission, Mission_Test,
	MENU_ENTRY_END
};

const void* Menu_PlayerSelectMenu[] PROGMEM =
{
	MenuStr_1Player,		&MenuFn_NavigateTo, Menu_ModeSelectMenu,
	MenuStr_2Player,		&MenuFn_NavigateTo, Menu_ModeSelect2PMenu,
	MENU_ENTRY_END
};

const void* Menu_MainMenu[] PROGMEM =
{
	MenuStr_Story,			&MenuFn_NavigateTo, Menu_MissionMenu,
	MenuStr_FreeRoam,		&MenuFn_StartMission, Mission_FreeRoam,
	MenuStr_Arcade,			&MenuFn_NavigateTo, Menu_PlayerSelectMenu,
	MENU_ENTRY_END
};

const void* Menu_PauseMenu[] PROGMEM =
{
	MenuStr_Continue,		&MenuFn_Continue, NULL,
	MenuStr_Restart,		&MenuFn_RestartMission, NULL,
	MenuStr_Exit,			&MenuFn_ExitMission, NULL,
	MENU_ENTRY_END
};


#endif /* MENUDATA_INC_H_ */
