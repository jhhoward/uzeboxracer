/*
 * TrackData.inc.h
 *
 *  Created on: 8 Mar 2014
 *      Author: James
 */

#ifndef TRACKDATA_INC_H_
#define TRACKDATA_INC_H_

#include <avr/pgmspace.h>
#include "World.h"

const MacroTileCoord TrackData_0[] PROGMEM =
{
	MT_COORD(8, 4),
	MT_COORD(9, 4),
	MT_COORD(10, 4),
	MT_COORD(11, 4),
	MT_COORD(12, 4),
	MT_COORD(12, 5),
	MT_COORD(12, 6),
	MT_COORD(12, 7),
	MT_COORD(12, 8),
	MT_COORD(13, 8),
	MT_COORD(13, 9),
	MT_COORD(13, 10),
	MT_COORD(12, 10),
	MT_COORD(11, 10),
	MT_COORD(10, 10),
	MT_COORD(9, 10),
	MT_COORD(9, 9),
	MT_COORD(8, 9),
	MT_COORD(7, 9),
	MT_COORD(6, 9),
	MT_COORD(5, 9),
	MT_COORD(4, 9),
	MT_COORD(3, 9),
	MT_COORD(3, 8),
	MT_COORD(3, 7),
	MT_COORD(4, 7),
	MT_COORD(4, 6),
	MT_COORD(5, 6),
	MT_COORD(6, 6),
	MT_COORD(7, 6),
	MT_COORD(7, 5),
	MT_COORD(7, 4),
};
/*const MacroTileCoord TrackData_0[] PROGMEM =
{
	MT_COORD(0, 1),
	MT_COORD(0, 0),
	MT_COORD(1, 0),
	MT_COORD(1, 1),
	MT_COORD(2, 1),
	MT_COORD(3, 1),
	MT_COORD(3, 2),
	MT_COORD(3, 3),
	MT_COORD(3, 4),
	MT_COORD(3, 5),
	MT_COORD(3, 6),
	MT_COORD(2, 6),
	MT_COORD(1, 6),
	MT_COORD(0, 6),
	MT_COORD(0, 5),
	MT_COORD(0, 4),
	MT_COORD(0, 3),
	MT_COORD(0, 2),
};*/

/*const MacroTileCoord TrackData_0[] PROGMEM =
{
	MT_COORD(0, 1),
	MT_COORD(0, 0),
	MT_COORD(1, 0),
	MT_COORD(1, 1),
	MT_COORD(2, 1),
	MT_COORD(3, 1),
	MT_COORD(3, 2),
	MT_COORD(3, 3),
	MT_COORD(3, 4),
	MT_COORD(3, 5),
	MT_COORD(3, 6),
	MT_COORD(2, 6),
	MT_COORD(1, 6),
	MT_COORD(1, 7),
	MT_COORD(2, 7),
	MT_COORD(3, 7),
	MT_COORD(4, 7),
	MT_COORD(4, 6),
	MT_COORD(4, 5),
	MT_COORD(4, 4),
	MT_COORD(4, 3),
	MT_COORD(5, 3),
	MT_COORD(5, 4),
	MT_COORD(6, 4),
	MT_COORD(7, 4),
	MT_COORD(7, 3),
	MT_COORD(7, 2),
	MT_COORD(7, 1),
	MT_COORD(6, 1),
	MT_COORD(5, 1),
	MT_COORD(5, 2),
	MT_COORD(4, 2),
	MT_COORD(3, 2),
	MT_COORD(2, 2),
	MT_COORD(1, 2),
	MT_COORD(0, 2)
};
*/
#endif /* TRACKDATA_INC_H_ */
