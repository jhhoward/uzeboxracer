/*
 * MissionData.inc.h
 *
 *  Created on: 16 Jul 2014
 *      Author: James
 */

#ifndef MISSIONDATA_INC_H_
#define MISSIONDATA_INC_H_

#include <avr/pgmspace.h>
#include "Mission.h"
#include "Car.h"

#define COLORMASK_PLAYER COLORMASK_RED
#define COLORMASK_PLAYER_PED COLORMASK_BLUE

const char Mission_String0[] PROGMEM = "PICK UP THE CREW";
const char Mission_String1[] PROGMEM = "GET TO THE BANK";
const char Mission_String2[] PROGMEM = "GET TO THE SAFEHOUSE";
const char Mission_String3[] PROGMEM = "MISSION COMPLETE!";
const char Mission_String4[] PROGMEM = "GO COLLECT THE PAYMENTS";
const char Mission_String5[] PROGMEM = "DON'T LET HIM GET AWAY!";
const char Mission_String6[] PROGMEM = "IT'S THE COPS! ESCAPE!";
const char Mission_String7[] PROGMEM = "GO!";
const char Mission_String8[] PROGMEM = "> RETRY <";
const char Mission_String9[] PROGMEM = "> CONTINUE <";
const char Mission_String10[] PROGMEM = "GET BACK TO THE BOSS";
const char Mission_String11[] PROGMEM = "FOLLOW HIM!";
const char Mission_String12[] PROGMEM = "GET READY";
const char Mission_String13[] PROGMEM = "DEMO";

const char* Mission_Strings[] PROGMEM =
{
	Mission_String0,
	Mission_String1,
	Mission_String2,
	Mission_String3,
	Mission_String4,
	Mission_String5,
	Mission_String6,
	Mission_String7,
	Mission_String8,
	Mission_String9,
	Mission_String10,
	Mission_String11,
	Mission_String12,
	Mission_String13
};

// Door locations		|  Parking location
// Parking lot:   4, 2  |  3, 5
// Stop off: 	  7, 1  |  8, 3
// Alleyway:      2, 2  |  4, 4
// Front of lot:  10,2  |  12, 4

// Starting offset: 3, 5

const uint8_t Mission_Completed[] PROGMEM =
{
		MI_SetFlag,				MFlag_MissionComplete, true,
		MI_SetFlag,				MFlag_InputBlocked, true,
		// "Mission complete!"
		MI_DisplayMessage,		3,
		MI_Wait,				90,
		MI_SetFlag,				MFlag_InEndMenu, true,
		MI_DisplayMessage2,		9,
		MI_YieldForever,
};

const uint8_t Mission_Failed[] PROGMEM =
{
		MI_Wait,				90,
		MI_SetFlag,				MFlag_InEndMenu, true,
		MI_DisplayMessage2,		8,
		MI_YieldForever,
};

const uint8_t Mission_FreeRoam[] PROGMEM =
{
//		MI_SetFlag,				MFlag_BlockNPCSpawning, true,
		MI_SpawnCar,			AIType_Player, 19, 53, DEGREES_90, COLORMASK_PLAYER,
		MI_SetFlag,				MFlag_InputBlocked, true,
		MI_PedXYToCar,			20, 50, AIType_Player, COLORMASK_PLAYER_PED,
		MI_SetFlag,				MFlag_InputBlocked, false,
		MI_YieldForever,
};

const uint8_t Mission_Demo[] PROGMEM =
{
		MI_SpawnCar,			AIType_Player, 140, 70, DEGREES_0, COLORMASK_PLAYER,
		MI_Wait,				1,
		MI_SetWanted,			2,
		MI_SetFlag,				MFlag_PlayerInvincible, true,
		MI_SetFlag,				MFlag_IsDemo, true,
		MI_DisplayMessage2,		13,
		MI_YieldForever,
};

const uint8_t Mission_Demo2[] PROGMEM =
{
		MI_SpawnCar,			AIType_Player, 140, 73, DEGREES_0, COLORMASK_RGB(255, 255, 255),
		MI_SpawnCar,			AIType_Mission, 145, 73, DEGREES_0, COLORMASK_GREEN,
		MI_SetFlag,				MFlag_PlayerIsCop, true,
		MI_SetFlag,				MFlag_CarIsObjective, true,
		MI_SetAIType,			AIType_FleeFast,
		MI_SetFlag,				MFlag_IsDemo, true,
		MI_Wait,				1,
		MI_DisplayMessage2,		13,
		MI_YieldForever,
};

const uint8_t Mission_Survival[] PROGMEM =
{
		MI_SpawnCar,			AIType_Player, 140, 70, DEGREES_0, COLORMASK_PLAYER,
		MI_SetFlag,				MFlag_InputBlocked, true,
		MI_PedXYToCar,			138, 66, AIType_Player, COLORMASK_PLAYER_PED,
		MI_SetFlag,				MFlag_InputBlocked, false,
		// "It's the cops!"
		MI_DisplayMessage,		6,
		MI_SetFlag,				MFlag_TimerCountingUp, true,
		MI_Wait,				60,
		MI_SetWanted,			4,
		MI_YieldForever,
};

const uint8_t Mission_Pursuit[] PROGMEM =
{
		MI_SpawnCar,			AIType_Player, 140, 73, DEGREES_0, COLORMASK_RGB(255, 255, 255),
		MI_SpawnCar,			AIType_Mission, 145, 73, DEGREES_0, COLORMASK_GREEN,
		MI_SetFlag,				MFlag_PlayerIsCop, true,
		MI_SetFlag,				MFlag_InputBlocked, true,
		MI_SetFlag,				MFlag_CarIsObjective, true,
		MI_SetFlag,				MFlag_BlockNPCSpawning, true,
		MI_Wait,				1,
		// "Don't let him get away!"
		MI_DisplayMessage,		5,
		MI_SetFlag,				MFlag_TimerCountingUp, true,
		MI_Wait,				60,
		MI_SetFlag,				MFlag_FailIfLoseMissionCar, true,
		MI_SetAIType,			AIType_Flee,
		MI_SetFlag,				MFlag_InputBlocked, false,
		MI_SetFlag,				MFlag_BlockNPCSpawning, false,
		MI_YieldForever,
};

const uint8_t Mission_Pursuit2P[] PROGMEM =
{
		MI_SpawnCar,			AIType_Player, 140, 73, DEGREES_0, COLORMASK_RGB(255, 255, 255),
		MI_SpawnCar,			AIType_Player2, 145, 73, DEGREES_0, COLORMASK_GREEN,
		MI_SetFlag,				MFlag_PlayerIsCop, true,
		MI_SetFlag,				MFlag_InputBlocked, true,
		MI_SetFlag,				MFlag_BlockNPCSpawning, true,
		MI_Wait,				1,
		//MI_SetFlag,				MFlag_CarIsObjective, true,
		// "Don't let him get away!"
		MI_DisplayMessage,		5,
		MI_SetFlag,				MFlag_TimerCountingUp, true,
		MI_Wait,				60,
		MI_SetFlag,				MFlag_InputBlocked, false,
		MI_SetFlag,				MFlag_BlockNPCSpawning, false,
		MI_YieldForever,
};

// "The bank job"
const uint8_t Mission_Story1[] PROGMEM =
{
	// Spawn player, get in car
	MI_SpawnCar,			AIType_Player, 19, 53, DEGREES_90, COLORMASK_PLAYER,
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_Wait,				5,
	MI_PedXYToCar,			20, 50, AIType_Player, COLORMASK_PLAYER_PED,
	MI_SetFlag,				MFlag_InputBlocked, false,
	// "Pick up the crew"
	MI_DisplayMessage,		0,
	MI_ObjectiveXY,			104, 99,
	// Crew get in car
	MI_ClearMessage,
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_PedXYToCar,			103, 97, AIType_Player, COLORMASK_RED,
	MI_PedXYToCar,			103, 97, AIType_Player, COLORMASK_GREEN,
	MI_PedXYToCar,			103, 97, AIType_Player, COLORMASK_YELLOW,
	MI_SetFlag,				MFlag_InputBlocked, false,
	// "Get to the bank"
	MI_DisplayMessage,		1,
	MI_ObjectiveXY,			138, 68,
	MI_ClearMessage,
	// Crew go into back and back into car
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_PedCarToXY,			AIType_Player, 138, 66, COLORMASK_YELLOW,
	MI_PedCarToXY,			AIType_Player, 138, 66, COLORMASK_RED,
	MI_PedCarToXY,			AIType_Player, 138, 66, COLORMASK_GREEN,
	MI_Wait,				40,
	MI_ShakeCamera,			5,
	MI_PlaySound,			SFX_SPLAT, 0xFF, true,
	MI_Wait,				20,
	MI_PedXYToCar,			138, 66, AIType_Player, COLORMASK_RED,
	MI_PedXYToCar,			138, 66, AIType_Player, COLORMASK_GREEN,
	MI_PedXYToCar,			138, 66, AIType_Player, COLORMASK_YELLOW,
	MI_SetFlag,				MFlag_InputBlocked, false,
	// "Get to the safe house"
	MI_DisplayMessage,		2,
	MI_Wait,				60,
	MI_SetWanted,			2,
	MI_ObjectiveXY,			211, 213,
	// Crew and player go into building
	MI_ClearMessage,
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_PedCarToXY,			AIType_Player, 212, 210, COLORMASK_YELLOW,
	MI_PedCarToXY,			AIType_Player, 212, 210, COLORMASK_RED,
	MI_PedCarToXY,			AIType_Player, 212, 210, COLORMASK_GREEN,
	MI_PedCarToXY,			AIType_Player, 212, 210, COLORMASK_PLAYER_PED,
	MI_Finish
};

#define COLORMASK_MISSION2_PED COLORMASK_RGB(10, 10, 10)

// "Protection racket"
const uint8_t Mission_Story2[] PROGMEM =
{
	// Spawn player, get in car
	MI_SpawnCar,			AIType_Player, 211, 213, DEGREES_90, COLORMASK_PLAYER,
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_PedXYToCar,			212, 210, AIType_Player, COLORMASK_PLAYER_PED,
	MI_SetFlag,				MFlag_InputBlocked, false,
	MI_Wait,				1,
	// "Go collect the payments"
	MI_DisplayMessage,		4,
	MI_ObjectiveXY,			59, 228,			// Tile 3, 14
	MI_ClearMessage,
	// Goes in to collect and comes back out
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_PedCarToXY,			AIType_Player, 58, 226, COLORMASK_PLAYER_PED,
	MI_Wait,				20,
	MI_ShakeCamera,			4,
	MI_PlaySound,			SFX_SPLAT, 0xFF, true,
	MI_Wait,				20,
	MI_ShakeCamera,			4,
	MI_PlaySound,			SFX_BUMP, 0xFF, true,
	MI_Wait,				20,
	MI_PedXYToCar,			58, 226, AIType_Player, COLORMASK_PLAYER_PED,
	MI_SetFlag,				MFlag_InputBlocked, false,
	// Set up the car that makes the getaway
	MI_SpawnCar,			AIType_Mission, 22, 181, DEGREES_90, COLORMASK_BLUE,
	MI_SetAIType,			AIType_None,
	// "Go collect the payments"
	MI_DisplayMessage,		4,
	MI_ObjectiveXY,			27, 180,			// Tile 1, 11
	MI_ClearMessage,
	// Goes in to collect and comes back out
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_PedCarToXY,			AIType_Player, 26, 178, COLORMASK_PLAYER_PED,
	MI_Wait,				20,
	MI_ShakeCamera,			4,
	MI_PlaySound,			SFX_SPLAT, 0xFF, true,
	MI_Wait,				40,
	MI_PedXYToCar,			26, 178, AIType_Mission, COLORMASK_MISSION2_PED,
	MI_PedXYToCar,			26, 178, AIType_Player, COLORMASK_PLAYER_PED,
	// Set getaway car destination
	MI_SetMarkerXY,			24, 36,			// Tile 1, 2
	MI_SetAIType,			AIType_DriveToMarker,
	MI_SetFlag,				MFlag_CarIsObjective, true,
	MI_SetFlag,				MFlag_InputBlocked, false,
	// "He's short on payment, follow him!"
	MI_DisplayMessage,		11,
	MI_SetFlag,				MFlag_FailIfLoseMissionCar, true,
	MI_WaitForMissionCar,
	// Arrived at destination
	MI_SetAIType,			AIType_None,
	MI_PedCarToXY,			AIType_Mission, 23, 33, COLORMASK_MISSION2_PED,
	MI_SetFlag,				MFlag_CarIsObjective, false,
	MI_SetFlag,				MFlag_FailIfLoseMissionCar, false,
	MI_ObjectiveXY,			22, 36,
	MI_ClearMessage,
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_PedCarToXY,			AIType_Player, 23, 33, COLORMASK_PLAYER_PED,
	MI_Wait,				20,
	MI_ShakeCamera,			4,
	MI_PlaySound,			SFX_SPLAT, 0xFF, true,
	MI_Wait,				20,
	MI_ShakeCamera,			4,
	MI_PlaySound,			SFX_BUMP, 0xFF, true,
	MI_Wait,				20,
	MI_PedXYToCar,			23, 33, AIType_Player, COLORMASK_PLAYER_PED,
	MI_SetFlag,				MFlag_InputBlocked, false,
	// "Get back to the boss!"
	MI_DisplayMessage,		10,
	MI_ObjectiveXY,			56, 19,		// Macro Tile 3, 1
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_PedCarToXY,			AIType_Player, 55, 17, COLORMASK_PLAYER_PED,
	MI_Finish
};

// "Street race"
const uint8_t Mission_Story3[] PROGMEM =
{
	// Spawn player, get in car
	MI_SetFlag,				MFlag_BlockNPCSpawning, true,
	MI_SpawnCar,			AIType_Player, 136, 71, DEGREES_0, COLORMASK_PLAYER,
	MI_SpawnCar,			AIType_Mission, 136, 73, DEGREES_0, COLORMASK_GREEN,
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_Wait,				1,
	// "Get ready!"
	MI_DisplayMessage,		12,
	MI_Wait,				180,
	// "Go!"
	MI_DisplayMessage,		7,
	MI_SetFlag,				MFlag_IsRace, true,
	MI_SetAIType,			AIType_RacingOpponent,
	MI_SetFlag,				MFlag_InputBlocked, false,
	MI_SetFlag,				MFlag_BlockNPCSpawning, false,
	MI_RaceForLaps,			1,
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_Finish
};

#if 0
// "The meet"
const uint8_t Mission_Story4[] PROGMEM =
{
	// Spawn player, get in car
	MI_SpawnCar,			AIType_Player, 19, 53, DEGREES_90, COLORMASK_PLAYER,
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_Wait,				5,
	MI_PedXYToCar,			20, 50, AIType_Player, COLORMASK_PLAYER_PED,
	MI_SetFlag,				MFlag_InputBlocked, false,
	MI_Finish
};

// "Take out the rat"
const uint8_t Mission_Story5[] PROGMEM =
{
	// Spawn player, get in car
	MI_SpawnCar,			AIType_Player, 19, 53, DEGREES_90, COLORMASK_PLAYER,
	MI_SetFlag,				MFlag_InputBlocked, true,
	MI_Wait,				5,
	MI_PedXYToCar,			20, 50, AIType_Player, COLORMASK_PLAYER_PED,
	MI_SetFlag,				MFlag_InputBlocked, false,
	MI_Finish
};
#endif

#endif /* MISSIONDATA_INC_H_ */
