/*
 * Mission.h
 *
 *  Created on: 8 Jul 2014
 *      Author: James
 */

#ifndef MISSION_H_
#define MISSION_H_

#include <stdint.h>
#include <stdbool.h>

#include "Car.h"

typedef struct
{
	uint8_t* startPtr;
	uint8_t* lastPtr;
	uint8_t* currentPtr;
	uint8_t markerX, markerY;
	uint8_t carIndex : 4;
	uint8_t aiType : 4;

	union
	{
		uint16_t flags;
		struct
		{
			bool didYield : 1;
			bool inputBlocked : 1;
			bool carIsObjective : 1;
			bool markerIsObjective : 1;
			bool waitingForMissionCar : 1;
			bool timerCountingUp : 1;
			bool playerIsCop : 1;
			bool inEndMenu : 1;
			bool missionComplete : 1;
			bool isRace : 1;
			bool failIfLoseMissionCar : 1;
			bool blockNPCSpawning : 1;
			bool playerImmunity : 1;
			bool playerInvincible : 1;
			bool isDemo : 1;
		};
	};
} MissionState;

// Mission flags
enum
{
	MFlag_DidYield,
	MFlag_InputBlocked,
	MFlag_CarIsObjective,
	MFlag_MarkerIsObjective,
	MFlag_WaitingForMissionCar,
	MFlag_TimerCountingUp,
	MFlag_PlayerIsCop,
	MFlag_InEndMenu,
	MFlag_MissionComplete,
	MFlag_IsRace,
	MFlag_FailIfLoseMissionCar,
	MFlag_BlockNPCSpawning,
	MFlag_PlayerImmunity,
	MFlag_PlayerInvincible,
	MFlag_IsDemo
};

// Mission instructions
enum
{
	MI_Wait = 0,
	MI_DisplayMessage,
	MI_DisplayMessage2,
	MI_ClearMessage,
	MI_YieldForever,
	MI_Finish,
	MI_SpawnCar,
	MI_SetWanted,
	MI_SetFlag,
	MI_PedCarToXY,
	MI_PedXYToCar,
	MI_ObjectiveXY,
	MI_ObjectiveCar,
	MI_SetMarkerXY,
	MI_SetAIType,
	MI_WaitForMissionCar,
	MI_RaceForLaps,
	MI_PlaySound,
	MI_ShakeCamera,
	MI_SetRaceTrack
};

extern MissionState Mission_State;

void Mission_Init();
void Mission_Update();
void Mission_SetSprites();
void Mission_Set(const uint8_t* mission);
void Mission_Fail(const char* reason);
void Mission_Complete();

extern const char FailReason_WreckedCar[];
extern const char FailReason_TheyEscaped[];
extern const char FailReason_LostRace[];

extern const uint8_t Mission_Failed[];
extern const uint8_t Mission_Completed[];
extern const uint8_t Mission_FreeRoam[];
extern const uint8_t Mission_Story1[];
extern const uint8_t Mission_Story2[];
extern const uint8_t Mission_Story3[];
extern const uint8_t Mission_Story4[];
extern const uint8_t Mission_Story5[];
extern const uint8_t Mission_Test[];
extern const uint8_t Mission_Survival[];
extern const uint8_t Mission_Pursuit[];
extern const uint8_t Mission_Pursuit2P[];
extern const uint8_t Mission_Demo[];
extern const uint8_t Mission_Demo2[];

#define Mission_Current Mission_State.startPtr

#endif /* MISSION_H_ */
