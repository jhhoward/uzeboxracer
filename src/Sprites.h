#ifndef SPRITES_H_
#define SPRITES_H_

#include <stdint.h>

void Sprites_Init();
void Sprites_Reset();
uint8_t Sprites_GetIndex();

extern uint8_t Sprites_FrameAnimation;

#endif
