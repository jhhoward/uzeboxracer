#include <uzebox.h>
#include <avr/pgmspace.h>
#include "Sprites.h"
#include "data/sprites.inc.h"

static uint8_t Sprites_CurrentIndex = 0;
static uint8_t Sprites_LastCount = 0;
static uint8_t Sprites_Cycle = 0;
uint8_t Sprites_FrameAnimation = 0;

void Sprites_Init()
{
	SetSpritesTileTable(gfx_Sprites);
	Sprites_FrameAnimation = 0;
}

void Sprites_Reset()
{
	for(int n = 0; n < MAX_SPRITES; n++)
	{
		sprites[n].x = OFF_SCREEN;
	}

#ifdef SPRITE_CYCLING_ENABLED
	if(Sprites_LastCount <= MAX_SPRITES)
	{
		if(Sprites_Cycle >= MAX_SPRITES)
		{
			Sprites_Cycle = 0;
		}
		Sprites_CurrentIndex = Sprites_Cycle;
	}
	else
	{
		if(Sprites_Cycle >= (Sprites_LastCount))
		{
			Sprites_Cycle = 0;
		}
		Sprites_CurrentIndex = (256 - (Sprites_Cycle));
	}
	Sprites_LastCount = 0;
	Sprites_Cycle++;

#else
	Sprites_Cycle = 0;
	Sprites_CurrentIndex = 0;
#endif

	Sprites_FrameAnimation ++;
}

uint8_t Sprites_GetIndex()
{
	uint8_t result = Sprites_CurrentIndex;

	Sprites_LastCount++;
	Sprites_CurrentIndex++;

	if(Sprites_CurrentIndex == MAX_SPRITES)
	{
		Sprites_CurrentIndex = 0;
	}

	if(result >= MAX_SPRITES || sprites[result].x != OFF_SCREEN)
	{
		return MAX_SPRITES;
	}
	return result;
}

