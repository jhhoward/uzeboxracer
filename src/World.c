/*
 * World.c
 *
 *  Created on: 24 Jan 2014
 *      Author: James
 */

#include <avr/pgmspace.h>
#include <uzebox.h>
#include "World.h"
#include "Track.h"
#include "Main.h"
#include "FixedMath.h"
#include "data/graphics.inc.h"
#include "data/worldLayout.inc.h"

Vector2 MainCamera;

const char* World_GetTileAddress(uint8_t tileIndex)
{
	switch(tileIndex)
	{
	case WORLD_TILE_BUILDINGS:
		return map_buildings;
	case WORLD_TILE_ROAD_NS:
		return map_road_ns;
	case WORLD_TILE_ROAD_EW:
		return map_road_ew;
	case WORLD_TILE_ROAD_ES:
		return map_road_es;
	case WORLD_TILE_ROAD_SW:
		return map_road_sw;
	case WORLD_TILE_ROAD_NE:
		return map_road_ne;
	case WORLD_TILE_ROAD_NW:
		return map_road_nw;
	case WORLD_TILE_ROAD_NESW:
		return map_road_nesw;
	case WORLD_TILE_ROAD_ESW:
		return map_road_esw;
	case WORLD_TILE_ROAD_NSW:
		return map_road_nsw;
	case WORLD_TILE_ROAD_NEW:
		return map_road_new;
	case WORLD_TILE_ROAD_NES:
		return map_road_nes;
	case WORLD_TILE_RIVER_NS:
		return map_river_ns;
	case WORLD_TILE_RIVER_EW:
		return map_river_ew;
	case WORLD_TILE_RIVER_ES:
		return map_river_es;
	case WORLD_TILE_RIVER_SW:
		return map_river_sw;
	case WORLD_TILE_RIVER_NE:
		return map_river_ne;
	case WORLD_TILE_RIVER_NW:
		return map_river_nw;
	case WORLD_TILE_BRIDGE_EW:
		return map_bridge_ew;
	case WORLD_TILE_BRIDGE_NS:
		return map_bridge_ns;
	case WORLD_TILE_ROAD_EW2:
		return map_road_ew2;
	case WORLD_TILE_ROAD_EW_ALLEY_N:
		return map_road_ew_alley_n;
	case WORLD_TILE_ROAD_EW_ALLEY_S:
		return map_road_ew_alley_s;
	case WORLD_TILE_ROAD_EW3:
		return map_road_ew3;
	case WORLD_TILE_OPEN_AREA:
		return map_open_area;

	default:
		return 0;
	}
}

const char* World_GetLayout()
{
	return world_layout;
}

const char* World_GetLayoutTiles()
{
	return world_layoutTiles;
}

const char* World_GetHUDLayout()
{
	return hud_layout;
}

const char* World_GetLogoLayout()
{
	return logo_layout;
}

void World_Init()
{
	SetTileTable(graphicsTiles);

	Screen.overlayTileTable = world_layoutTiles;
	Screen.scrollHeight = SCREEN_SCROLL_HEIGHT;
	Screen.overlayHeight = SCREEN_OVERLAY_HEIGHT;

	World_UpdateAllTiles();
}

uint8_t World_GetMacroTileExits(int8_t x, int8_t y)
{
	return World_GetAllMacroTileExits(x, y) & 0xF;
}

uint8_t World_GetAllMacroTileExits(int8_t x, int8_t y)
{
	uint8_t worldTile =
			pgm_read_byte(&world_layout[2 + y * WORLD_WIDTH + x]);

	switch(worldTile)
	{
	case WORLD_TILE_ROAD_NS:
		return (EXIT_NORTH_MASK | EXIT_SOUTH_MASK);
	case WORLD_TILE_ROAD_EW:
		return (EXIT_EAST_MASK | EXIT_WEST_MASK);
	case WORLD_TILE_ROAD_ES:
		return (EXIT_EAST_MASK | EXIT_SOUTH_MASK);
	case WORLD_TILE_ROAD_SW:
		return (EXIT_SOUTH_MASK | EXIT_WEST_MASK);
	case WORLD_TILE_ROAD_NE:
		return (EXIT_NORTH_MASK | EXIT_EAST_MASK);
	case WORLD_TILE_ROAD_NW:
		return (EXIT_NORTH_MASK | EXIT_WEST_MASK);
	case WORLD_TILE_ROAD_NESW:
		return (EXIT_NORTH_MASK | EXIT_EAST_MASK | EXIT_SOUTH_MASK | EXIT_WEST_MASK);
	case WORLD_TILE_ROAD_ESW:
		return (EXIT_EAST_MASK | EXIT_SOUTH_MASK | EXIT_WEST_MASK);
	case WORLD_TILE_ROAD_NSW:
		return (EXIT_NORTH_MASK | EXIT_SOUTH_MASK | EXIT_WEST_MASK);
	case WORLD_TILE_ROAD_NEW:
		return (EXIT_NORTH_MASK | EXIT_EAST_MASK | EXIT_WEST_MASK);
	case WORLD_TILE_ROAD_NES:
		return (EXIT_NORTH_MASK | EXIT_EAST_MASK | EXIT_SOUTH_MASK);
	case WORLD_TILE_BRIDGE_EW:
		return (EXIT_EAST_MASK | EXIT_WEST_MASK);
	case WORLD_TILE_BRIDGE_NS:
		return (EXIT_NORTH_MASK | EXIT_SOUTH_MASK);
	case WORLD_TILE_ROAD_EW2:
		return (EXIT_EAST_MASK | EXIT_WEST_MASK);
	case WORLD_TILE_ROAD_EW_ALLEY_N:
		return (EXIT_EAST_MASK | EXIT_WEST_MASK | EXIT_ALLEYWAY_NORTH_MASK);
	case WORLD_TILE_ROAD_EW_ALLEY_S:
		return (EXIT_EAST_MASK | EXIT_WEST_MASK | EXIT_ALLEYWAY_SOUTH_MASK);
	case WORLD_TILE_ROAD_EW3:
		return (EXIT_EAST_MASK | EXIT_WEST_MASK | EXIT_PARKINGLOT_NORTH_MASK);
	case WORLD_TILE_OPEN_AREA:
		return EXIT_PARKINGLOT_SOUTH_MASK;
	default:
		return 0;
	}
}


uint8_t World_GetTile(int16_t x, int16_t y)
{
	int16_t macroTileX = x >> 4;
	int16_t macroTileY = y >> 4;

	const char* address = 0;

	if(macroTileX < 0 || macroTileY < 0 || macroTileX >= WORLD_WIDTH || macroTileY >= WORLD_HEIGHT)
	{
		address = map_buildings;
	}

	if(!address)
	{
		address = World_GetTileAddress(
				pgm_read_byte(&world_layout[2 + macroTileY * WORLD_WIDTH + macroTileX]));
	}

	if(!address)
	{
		return 0;
	}

	uint8_t u = x & 0xF;
	uint8_t v = y & 0xF;

	uint8_t result = pgm_read_byte(&(address[(v<<4)+u+2]));

/*
 * This was to originally create barriers on the edge of the track:
	if((Game_GetMode() & GameMode_Race) && result < FIRST_COLLIDABLE_TILE)
	{
		int8_t exits = Track_GetExitsAtCoord(MT_COORD(macroTileX, macroTileY));
		uint8_t barrier = 19;

		if(!(exits & (0x1 << EXIT_NORTH)) && v == 3)
		{
			return barrier;
		}
		if(!(exits & (0x1 << EXIT_SOUTH)) && v == 12)
		{
			return barrier;
		}
		if(!(exits & (0x1 << EXIT_WEST)) && u == 3)
		{
			return barrier + 1;
		}
		if(!(exits & (0x1 << EXIT_EAST)) && u == 12)
		{
			return barrier + 1;
		}
	}
*/

	return result;
}

static int16_t oldScrollX = 0xFF;
static int16_t oldScrollY = 0xFF;
static int8_t vramScrollY = ((0xFF) % SCREEN_SCROLL_HEIGHT);

void World_UpdateAllTiles()
{
	oldScrollY = 0xFF;
	World_UpdateTiles();
}

void World_UpdateTiles()
{
#if 0
	uint16_t input = ReadJoypad(0);
	if(input & BTN_LEFT)
		MainCamera.x --;
	if(input & BTN_RIGHT)
		MainCamera.x ++;
	if(input & BTN_UP)
		MainCamera.y --;
	if(input & BTN_DOWN)
		MainCamera.y ++;
#endif


#if SCROLLING
	Screen.scrollX = (uint8_t) MainCamera.x;
	//Screen.scrollX = 0;
	//Screen.scrollY = (uint8_t) (test % (Screen.scrollHeight * 8));
#endif

	if(MainCamera.y < 0)
	{
		Screen.scrollY =  (uint8_t) ((MainCamera.y - 8 * (32 - SCREEN_SCROLL_HEIGHT)) % (SCREEN_SCROLL_HEIGHT * 8));
	}
	else
	{
		Screen.scrollY = (uint8_t) (MainCamera.y % (SCREEN_SCROLL_HEIGHT * 8));
	}

	int16_t scrollX = MainCamera.x >> 3;
	int16_t scrollY = MainCamera.y >> 3;

	// Haven't moved, early out
	if(scrollX == oldScrollX && scrollY == oldScrollY)
	{
		return;
	}


	// Moved so much its worth updating the whole world
	if(scrollX <= oldScrollX - 32 || scrollX >= oldScrollX + 32
	  || scrollY <= oldScrollY - 32 || scrollY >= oldScrollY + 32)
	{
		if(scrollY >= 0)
		{
			vramScrollY = ((scrollY) % SCREEN_SCROLL_HEIGHT);
		}
		else
		{
			vramScrollY = ((scrollY + SCREEN_SCROLL_HEIGHT) % SCREEN_SCROLL_HEIGHT);
		}

		uint8_t outY = vramScrollY;

		for(int16_t j = scrollY; j < scrollY + SCREEN_SCROLL_HEIGHT; j++)
		{
			for(int16_t i = scrollX; i < scrollX + 32; i++)
			{
				uint8_t outX = i & 0x1F;

				SetTile(outX, outY, World_GetTile(i, j));
			}

			outY++;
			if(outY >= SCREEN_SCROLL_HEIGHT) outY -= SCREEN_SCROLL_HEIGHT;
		}
	}
	else
	{
		int8_t diffX = 0;
		int8_t diffY = 0;

		// Moved up
		if(scrollY < oldScrollY)
		{
			diffY = (oldScrollY - scrollY);
			vramScrollY -= diffY;
			while(vramScrollY < 0)
				vramScrollY += SCREEN_SCROLL_HEIGHT;

			uint8_t outY = vramScrollY;

			for(int8_t j = 0; j < diffY; j++)
			{
				int16_t worldY = scrollY + j;

				for(uint8_t i = 0; i < 32; i++)
				{
					int16_t worldX = scrollX + i;
					uint8_t outX = worldX & 0x1F;
					SetTile(outX, outY, World_GetTile(worldX, worldY));
				}

				outY++;
				if(outY >= SCREEN_SCROLL_HEIGHT) outY -= SCREEN_SCROLL_HEIGHT;
			}
		}
		// Moved down
		else if(scrollY > oldScrollY)
		{
			diffY = (scrollY - oldScrollY) & 0x1F;
			vramScrollY += diffY;
			while(vramScrollY >= SCREEN_SCROLL_HEIGHT)
				vramScrollY -= SCREEN_SCROLL_HEIGHT;

			uint8_t outY = vramScrollY + SCREEN_SCROLL_HEIGHT - diffY;
			while(outY >= SCREEN_SCROLL_HEIGHT)
				outY -= SCREEN_SCROLL_HEIGHT;

			for(int8_t j = SCREEN_SCROLL_HEIGHT - diffY; j < SCREEN_SCROLL_HEIGHT; j++)
			{
				int16_t worldY = scrollY + j;

				for(uint8_t i = 0; i < 32; i++)
				{
					int16_t worldX = scrollX + i;
					uint8_t outX = worldX & 0x1F;
//					uint8_t outY = worldY & 0x1F;
					SetTile(outX, outY, World_GetTile(worldX, worldY));
				}
				outY++;
				if(outY >= SCREEN_SCROLL_HEIGHT) outY -= SCREEN_SCROLL_HEIGHT;
			}
		}

		// Moved left
		if(scrollX < oldScrollX)
		{
			diffX = oldScrollX - scrollX;

			for(uint8_t i = 0; i < diffX; i++)
			{
				int16_t worldX = scrollX + i;
				uint8_t outY = vramScrollY;

				for(int8_t j = 0; j < SCREEN_SCROLL_HEIGHT; j++)
				{
					int16_t worldY = scrollY + j;
					uint8_t outX = worldX & 0x1F;
					SetTile(outX, outY, World_GetTile(worldX, worldY));

					outY++;
					if(outY >= SCREEN_SCROLL_HEIGHT) outY -= SCREEN_SCROLL_HEIGHT;
				}
			}
		}
		// Moved right
		else if(scrollX > oldScrollX)
		{
			diffX = scrollX - oldScrollX;

			for(uint8_t i = 32 - diffX; i < 32; i++)
			{
				int16_t worldX = scrollX + i;
				uint8_t outY = vramScrollY;

				for(int8_t j = 0; j < SCREEN_SCROLL_HEIGHT; j++)
				{
					int16_t worldY = scrollY + j;
					uint8_t outX = worldX & 0x1F;

					SetTile(outX, outY, World_GetTile(worldX, worldY));
					outY++;
					if(outY >= SCREEN_SCROLL_HEIGHT) outY -= SCREEN_SCROLL_HEIGHT;
				}
			}
		}
	}

	oldScrollX = scrollX;
	oldScrollY = scrollY;
}

void World_SetCustomTile(uint8_t worldX, uint8_t worldY, uint8_t tile)
{
	int16_t scrollX = MainCamera.x >> 3;
	int16_t scrollY = MainCamera.y >> 3;

	if(worldX >= scrollX && worldY >= scrollY && worldX < scrollX + 32 && worldY < scrollY + SCREEN_SCROLL_HEIGHT)
	{
		uint8_t outX = worldX & 0x1F;
		uint8_t outY = vramScrollY + (worldY - scrollY);

		if(outY >= SCREEN_SCROLL_HEIGHT) outY -= SCREEN_SCROLL_HEIGHT;
		SetTile(outX, outY, tile);
	}
}

void World_ResetCustomTile(uint8_t worldX, uint8_t worldY)
{
	uint8_t tile = World_GetTile(worldX, worldY);
	World_SetCustomTile(worldX, worldY, tile);
}
