/*
 * Ped.h
 *
 *  Created on: 5 Jul 2014
 *      Author: James
 */

#ifndef PED_H_
#define PED_H_

#include <stdint.h>

enum
{
	PedType_EmptySlot,
	PedType_NPC,
	PedType_Mission,
	PedType_Killed
};

typedef struct
{
	uint8_t pedType;
	uint16_t x, y;
	uint8_t colorMask;

	uint8_t direction : 2;
	uint8_t animation : 6;
} Ped;

Ped* Ped_Spawn(uint8_t pedType, uint16_t x, uint16_t y, uint8_t direction, uint8_t colorMask);
void Ped_Update();
void Ped_Init();
void Ped_SetSprites();

#endif /* PED_H_ */
